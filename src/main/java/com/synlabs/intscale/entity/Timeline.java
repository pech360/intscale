
package com.synlabs.intscale.entity;

import com.synlabs.intscale.entity.user.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.criteria.CriteriaBuilder;

@Entity
public class Timeline extends BaseEntity
{
  private String heading;
  @Column(length = 2000)
  private String description;
  private String imageName;
  private String videoName;
  private String contentType;
  @ManyToOne
  private User   user;


  public String getHeading()
  {
    return heading;
  }

  public void setHeading(String heading)
  {
    this.heading = heading;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public String getImageName()
  {
    return imageName;
  }

  public void setImageName(String imageName)
  {
    this.imageName = imageName;
  }

  public String getContentType()
  {
    return contentType;
  }

  public void setContentType(String contentType)
  {
    this.contentType = contentType;
  }

  public String getVideoName()
  {
    return videoName;
  }

  public void setVideoName(String videoName)
  {
    this.videoName = videoName;
  }

  @Override
  public User getUser()
  {
    return user;
  }

  public void setUser(User user)
  {
    this.user = user;
  }


}
