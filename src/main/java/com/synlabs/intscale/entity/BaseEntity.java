package com.synlabs.intscale.entity;

import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.entity.user.User;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.*;

@MappedSuperclass
public abstract class BaseEntity extends AbstractAuditable<User, Long>
{
  @ManyToOne
  private Tenant tenant;

  @Column(name = "_version")
  private Long version;

  public Tenant getTenant()
  {
    return tenant;
  }

  public void setTenant(Tenant tenant)
  {
    this.tenant = tenant;
  }

  public Long getVersion()
  {
    return version;
  }

  public void setVersion(Long version)
  {
    this.version = version;
  }

  public User getUser()
  {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication == null || !authentication.isAuthenticated())
    {
      return null;
    }

    if (authentication.getPrincipal() instanceof CurrentUser)
    {
      return ((CurrentUser) authentication.getPrincipal()).getUser();
    }
    return null;
  }

  @PrePersist
  private void preCreate()
  {
    this.setCreatedDate(DateTime.now());

    User current = getUser();
    if (current != null)
    {
      this.setCreatedBy(current);
      this.setTenant(current.getTenant());
    }
  }

  @PreUpdate
  private void preUpdate()
  {
    this.setLastModifiedDate(DateTime.now());
    User current = getUser();
    if (current != null)
    {
      this.setLastModifiedBy(current);
    }

  }

  public void setId(Long Id)
  {
    super.setId(Id);
  }

}
