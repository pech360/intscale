package com.synlabs.intscale.entity.threesixty;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.entity.Report.Category;
import com.synlabs.intscale.entity.test.SubTag;
import com.synlabs.intscale.entity.test.Test;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class StakeholderQuestion extends BaseEntity
{
  @ManyToOne(optional = false)
  private Test   test;

  @Column(nullable = false, length = 600)
  private String description;

  @OneToMany(mappedBy = "question")
  private List<StakeholderQuestionOption> options = new ArrayList<>();

  @ManyToMany
  private List<SubTag> subTags = new ArrayList<>();

  @ManyToOne
  private Category subCategory;

  @ManyToOne
  private Category category;

  public Category getSubCategory()
  {
    return subCategory;
  }

  public void setSubCategory(Category subCategory)
  {
    this.subCategory = subCategory;
  }

  public Category getCategory()
  {
    return category;
  }

  public void setCategory(Category category)
  {
    this.category = category;
  }

  public List<SubTag> getSubTags()
  {
    return subTags;
  }

  public void setSubTags(List<SubTag> subTags)
  {
    this.subTags = subTags;
  }

  public Test getTest()
  {
    return test;
  }

  public void setTest(Test test)
  {
    this.test = test;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public List<StakeholderQuestionOption> getOptions()
  {
    return options;
  }

  public void setOptions(List<StakeholderQuestionOption> options)
  {
    this.options = options;
  }
}
