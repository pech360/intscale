package com.synlabs.intscale.entity.threesixty;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.entity.test.Test;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class StakeholderAnswer extends BaseEntity
{
  @ManyToOne
  private Stakeholder stakeholder;

  @ManyToOne
  private Test test;

  @ManyToOne
  private StakeholderQuestion question;

  @ManyToOne
  private StakeholderQuestionOption selectedOption;

  @Column(nullable = false, length = 600)
  private String questionDescription;

  @Column(nullable = false)
  private String ansDescription;

  @Column(nullable = false)
  private Integer marksObtained;

  public String getQuestionDescription()
  {
    return questionDescription;
  }

  public void setQuestionDescription(String questionDescription)
  {
    this.questionDescription = questionDescription;
  }

  public String getAnsDescription()
  {
    return ansDescription;
  }

  public void setAnsDescription(String ansDescription)
  {
    this.ansDescription = ansDescription;
  }

  public Integer getMarksObtained()
  {
    return marksObtained;
  }

  public void setMarksObtained(Integer marksObtained)
  {
    this.marksObtained = marksObtained;
  }

  public Test getTest()
  {
    return test;
  }

  public void setTest(Test test)
  {
    this.test = test;
  }

  public Stakeholder getStakeholder()
  {
    return stakeholder;
  }

  public void setStakeholder(Stakeholder stakeholder)
  {
    this.stakeholder = stakeholder;
  }

  public StakeholderQuestion getQuestion()
  {
    return question;
  }

  public void setQuestion(StakeholderQuestion question)
  {
    this.question = question;
  }

  public StakeholderQuestionOption getSelectedOption()
  {
    return selectedOption;
  }

  public void setSelectedOption(StakeholderQuestionOption selectedOption)
  {
    this.selectedOption = selectedOption;
  }
}
