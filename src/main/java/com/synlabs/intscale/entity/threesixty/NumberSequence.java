package com.synlabs.intscale.entity.threesixty;

import javax.persistence.*;

@Entity
public class NumberSequence
{
  @Id
  @GeneratedValue
  private Long id;

  @Column(nullable = false)
  private long current = 1L;

  @Column(nullable = false)
  private String type;

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public long getCurrent()
  {
    return current;
  }

  public void setCurrent(long current)
  {
    this.current = current;
  }

  public String getType()
  {
    return type;
  }

  public void setType(String type)
  {
    this.type = type;
  }

  @Transient
  public String getSequence()
  {
    return type + current;
  }
}
