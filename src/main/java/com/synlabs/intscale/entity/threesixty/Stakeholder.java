package com.synlabs.intscale.entity.threesixty;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.enums.StakeholderRelation;
import com.synlabs.intscale.enums.StakeholderTestStatus;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import java.util.*;

@Entity
public class Stakeholder extends BaseEntity
{
  @ManyToOne
  private User user;

  @OneToOne
  private User stakeholderUser;

  @Column(nullable = false)
  private String name;
  private String mobile;

  @Column(nullable = false)
  private String email;

  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private StakeholderRelation relation;

  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private StakeholderTestStatus status;

  @ManyToMany
  private Set<Test> tests = new HashSet<>();

  @Column(nullable = false)
  private String loginKey;

  @Temporal(TemporalType.TIMESTAMP)
  private Date   lastLogin;

  @Temporal(TemporalType.TIMESTAMP)
  private Date loginValid;

  @Temporal(TemporalType.TIMESTAMP)
  private Date lastSend;

  @Temporal(TemporalType.TIMESTAMP)
  private Date lastRemind;

  @Temporal(TemporalType.TIMESTAMP)
  private Date lastVerificationSend;

  @Temporal(TemporalType.TIMESTAMP)
  private Date finishedDate;

  @OneToMany(mappedBy = "stakeholder")
  private List<StakeholderAnswer> answers = new ArrayList<>();

  private Boolean verified;

  public Date getLastVerificationSend()
  {
    return lastVerificationSend;
  }

  public void setLastVerificationSend(Date lastVerificationSend)
  {
    this.lastVerificationSend = lastVerificationSend;
  }

  public Boolean getVerified()
  {
    return verified;
  }

  public void setVerified(Boolean verified)
  {
    this.verified = verified;
  }

  public List<StakeholderAnswer> getAnswers()
  {
    return answers;
  }

  public void setAnswers(List<StakeholderAnswer> answers)
  {
    this.answers = answers;
  }

  public Date getLastSend()
  {
    return lastSend;
  }

  public void setLastSend(Date lastSend)
  {
    this.lastSend = lastSend;
  }

  public Date getLastRemind()
  {
    return lastRemind;
  }

  public void setLastRemind(Date lastRemind)
  {
    this.lastRemind = lastRemind;
  }

  public Date getFinishedDate()
  {
    return finishedDate;
  }

  public void setFinishedDate(Date finishedDate)
  {
    this.finishedDate = finishedDate;
  }

  public User getStakeholderUser()
  {
    return stakeholderUser;
  }

  public void setStakeholderUser(User stakeholderUser)
  {
    this.stakeholderUser = stakeholderUser;
  }

  @Override
  public User getUser()
  {
    return user;
  }

  public void setUser(User user)
  {
    this.user = user;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getMobile()
  {
    return mobile;
  }

  public void setMobile(String mobile)
  {
    this.mobile = mobile;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public StakeholderRelation getRelation()
  {
    return relation;
  }

  public void setRelation(StakeholderRelation relation)
  {
    this.relation = relation;
  }

  public StakeholderTestStatus getStatus()
  {
    return status;
  }

  public void setStatus(StakeholderTestStatus status)
  {
    this.status = status;
  }

  public Set<Test> getTests()
  {
    return tests;
  }

  public void setTests(Set<Test> tests)
  {
    this.tests = tests;
  }

  public String getLoginKey()
  {
    return loginKey;
  }

  public void setLoginKey(String loginKey)
  {
    this.loginKey = loginKey;
  }

  public Date getLastLogin()
  {
    return lastLogin;
  }

  public void setLastLogin(Date lastLogin)
  {
    this.lastLogin = lastLogin;
  }

  public Date getLoginValid()
  {
    return loginValid;
  }

  public void setLoginValid(Date loginValid)
  {
    this.loginValid = loginValid;
  }

  public StakeholderAnswer getAnswer(Long questionId)
  {
    if (questionId == null)
    {
      return null;
    }

    if (!CollectionUtils.isEmpty(answers))
    {
      for (StakeholderAnswer answer : answers)
      {
        if (answer.getQuestion().getId().equals(questionId))
        {
          return answer;
        }
      }
    }

    return null;
  }
}
