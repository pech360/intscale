package com.synlabs.intscale.entity.threesixty;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.enums.NotificationType;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
public class NotificationEmail extends BaseEntity
{
  @Enumerated(EnumType.STRING)
  private NotificationType type;
  private String email;

  public NotificationType getType()
  {
    return type;
  }

  public void setType(NotificationType type)
  {
    this.type = type;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }
}
