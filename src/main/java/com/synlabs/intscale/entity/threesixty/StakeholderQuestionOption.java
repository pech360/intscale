package com.synlabs.intscale.entity.threesixty;

import com.synlabs.intscale.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class StakeholderQuestionOption extends BaseEntity
{
  @ManyToOne(optional = false)
  private StakeholderQuestion question;
  private String description;
  @Column(nullable = false)
  private Integer marks;

  public StakeholderQuestion getQuestion()
  {
    return question;
  }

  public void setQuestion(StakeholderQuestion question)
  {
    this.question = question;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public Integer getMarks()
  {
    return marks;
  }

  public void setMarks(Integer marks)
  {
    this.marks = marks;
  }
}
