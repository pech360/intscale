package com.synlabs.intscale.entity;


import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
//LNT
@Entity
public class PromoCode extends BaseEntity{

	@Column(unique=true)
	private String name;
	@Min(value = 0)
	@Max(value = 100)
	private BigDecimal discount;
	@Column(name="coupon_start_date")
	private String couponStartDate;
	@Column(name="coupon_end_date")
	private String couponEndDate;
	@Column(name="is_valid")
	private Boolean isValid;
	@Column(name="consumed_count")
	@Min(value = 0)
	private Integer consumedCount;
	@Column(name="total_count")
	@Min(value=1)
	private Integer totalCount;

	public String getCouponStartDate() {
		return couponStartDate;
	}

	public void setCouponStartDate(Date couponStartDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		this.couponStartDate = dateFormat.format(couponStartDate);	
	}
	public String getCouponEndDate() {
		return couponEndDate;
	}
	public void setCouponEndDate(Date couponEndDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		this.couponEndDate = dateFormat.format(couponEndDate);
	}
    public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public Boolean getIsValid() {
		return isValid;
	}
	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}
	public Integer getConsumedCount() {
		return consumedCount;
	}
	public void setConsumedCount(Integer consumedCount) {
		this.consumedCount = consumedCount;
	}
	public Integer getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	@PrePersist
	public void SetExpireAndConsumedCount() {		
		consumedCount = consumedCount == null ? 0 : consumedCount;	
	}
	public PromoCode() {
		
	}
	public PromoCode(String name, BigDecimal discount,  Date couponStartDate, Date couponEndDate,
			Boolean isValid, Integer consumedCount, Integer totalCount) {	
		this.name = name;
		this.discount = discount;
		this.isValid = isValid;
		this.consumedCount = consumedCount;
		this.totalCount = totalCount;
		 SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		this.couponStartDate = dateFormat.format(couponStartDate);
		this.couponEndDate = dateFormat.format(couponEndDate);
	}
	
}
