package com.synlabs.intscale.entity.Report;

import com.synlabs.intscale.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class InterestInventory extends BaseEntity
{

  private String  interestCombination;
  private Integer region;
  @Column(length = 1200)
  private String  description;
  @Column(length = 1200)
  private String  shortDescription;
  @Column(length = 1200)
  private String  learningStyle;
  @Column(length = 1200)
  private String learningStyleInference;
  @Column(length = 1200)
  private String learningStyleRecommendation;

  public String getInterestCombination()
  {
    return interestCombination;
  }

  public void setInterestCombination(String interestCombination)
  {
    this.interestCombination = interestCombination;
  }

  public Integer getRegion()
  {
    return region;
  }

  public void setRegion(Integer region)
  {
    this.region = region;
  }

  public String getShortDescription()
  {
    return shortDescription;
  }

  public void setShortDescription(String shortDescription)
  {
    this.shortDescription = shortDescription;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public String getLearningStyle()
  {
    return learningStyle;
  }

  public void setLearningStyle(String learningStyle)
  {
    this.learningStyle = learningStyle;
  }

  public String getLearningStyleInference()
  {
    return learningStyleInference;
  }

  public void setLearningStyleInference(String learningStyleInference)
  {
    this.learningStyleInference = learningStyleInference;
  }

  public String getLearningStyleRecommendation()
  {
    return learningStyleRecommendation;
  }

  public void setLearningStyleRecommendation(String learningStyleRecommendation)
  {
    this.learningStyleRecommendation = learningStyleRecommendation;
  }
}
