package com.synlabs.intscale.entity.Report;

import com.synlabs.intscale.entity.BaseEntity;

import javax.jdo.annotations.Unique;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class AimExpertInventory extends BaseEntity
{
  @Unique
  private String              name;
  private String              displayName;
  private String              type;
  private String              imageName;
  @Column(length = 1200)
  private String              description;
  @Column(length = 1200)
  private String              summary;
  @Column(length = 1200)
  private String              tasks;
  @Column(length = 1200)
  private String              scope;
  @Column(length = 1200)
  private String              topRecruiters;
  @Column(length = 1200)
  private String              education;
  @Column(length = 1200)
  private String              topCollegesOfIndia;
  @Column(length = 1200)
  private String              topCollegesOfAbroad;
  private String              streams;
  private String              leisures[];
  @OneToMany(mappedBy = "aimExpertInventory")
  private List<AimExpertItem> aimExpertItems = new ArrayList<>();
  @OneToMany
  private List<Subject>       subjects       = new ArrayList<>();
  @OneToMany
  private List<Subject>       hobbies       = new ArrayList<>();

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getDisplayName()
  {
    return displayName;
  }

  public void setDisplayName(String displayName)
  {
    this.displayName = displayName;
  }

  public String getType()
  {
    return type;
  }

  public void setType(String type)
  {
    this.type = type;
  }

  public String getImageName()
  {
    return imageName;
  }

  public void setImageName(String imageName)
  {
    this.imageName = imageName;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public String getSummary()
  {
    return summary;
  }

  public void setSummary(String summary)
  {
    this.summary = summary;
  }

  public String getTasks()
  {
    return tasks;
  }

  public void setTasks(String tasks)
  {
    this.tasks = tasks;
  }

  public String getScope()
  {
    return scope;
  }

  public void setScope(String scope)
  {
    this.scope = scope;
  }

  public String getTopRecruiters()
  {
    return topRecruiters;
  }

  public void setTopRecruiters(String topRecruiters)
  {
    this.topRecruiters = topRecruiters;
  }

  public String getEducation()
  {
    return education;
  }

  public void setEducation(String education)
  {
    this.education = education;
  }

  public String getTopCollegesOfIndia()
  {
    return topCollegesOfIndia;
  }

  public void setTopCollegesOfIndia(String topCollegesOfIndia)
  {
    this.topCollegesOfIndia = topCollegesOfIndia;
  }

  public String getTopCollegesOfAbroad()
  {
    return topCollegesOfAbroad;
  }

  public void setTopCollegesOfAbroad(String topCollegesOfAbroad)
  {
    this.topCollegesOfAbroad = topCollegesOfAbroad;
  }

  public String getStreams()
  {
    return streams;
  }

  public void setStreams(String streams)
  {
    this.streams = streams;
  }

  public String[] getLeisures()
  {
    return leisures;
  }

  public void setLeisures(String[] leisures)
  {
    this.leisures = leisures;
  }

  public List<AimExpertItem> getAimExpertItems()
  {
    return aimExpertItems;
  }

  public void setAimExpertItems(List<AimExpertItem> aimExpertItems)
  {
    this.aimExpertItems = aimExpertItems;
  }

  public List<Subject> getSubjects()
  {
    return subjects;
  }

  public void setSubjects(List<Subject> subjects)
  {
    this.subjects = subjects;
  }

  public List<Subject> getHobbies()
  {
    return hobbies;
  }

  public void setHobbies(List<Subject> hobbies)
  {
    this.hobbies = hobbies;
  }
}