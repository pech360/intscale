package com.synlabs.intscale.entity.Report;

import com.synlabs.intscale.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;

@Entity
public class Subject extends BaseEntity
{
  @Column(precision = 16, scale = 5)
  private BigDecimal weight;
  @ManyToOne
  private AimExpertInventory aimExpertInventory;

  public BigDecimal getWeight()
  {
    return weight;
  }

  public void setWeight(BigDecimal weight)
  {
    this.weight = weight;
  }

  public AimExpertInventory getAimExpertInventory()
  {
    return aimExpertInventory;
  }

  public void setAimExpertInventory(AimExpertInventory aimExpertInventory)
  {
    this.aimExpertInventory = aimExpertInventory;
  }

}
