package com.synlabs.intscale.entity.Report;

import com.synlabs.intscale.entity.BaseEntity;

import javax.jdo.annotations.Unique;
import javax.persistence.Entity;

@Entity
public class CareerInterestInventory  extends BaseEntity {

    @Unique
    private String careerName;
    private String subjects;  // comma separated subjects
    private int region;
    private int r;
    private int i;
    private int a;
    private int s;
    private int e;
    private int c;

    public String getCareerName() {
        return careerName;
    }

    public void setCareerName(String careerName) {
        this.careerName = careerName;
    }

    public String getSubjects() {
        return subjects;
    }

    public void setSubjects(String subjects) {
        this.subjects = subjects;
    }

    public int getRegion() {
        return region;
    }

    public void setRegion(int region) {
        this.region = region;
    }

    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getS() {
        return s;
    }

    public void setS(int s) {
        this.s = s;
    }

    public int getE() {
        return e;
    }

    public void setE(int e) {
        this.e = e;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }
}
