package com.synlabs.intscale.entity.Report;

import com.synlabs.intscale.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class AimExpertItem extends BaseEntity
{
  private int goalStandard;

  @ManyToOne
  private ReportSection reportSection;
  @ManyToOne
  private Category category;
  @ManyToOne
  private AimExpertInventory aimExpertInventory;
  @ManyToOne
  private Subject subject;

  public int getGoalStandard()
  {
    return goalStandard;
  }

  public void setGoalStandard(int goalStandard)
  {
    this.goalStandard = goalStandard;
  }

  public ReportSection getReportSection()
  {
    return reportSection;
  }

  public void setReportSection(ReportSection reportSection)
  {
    this.reportSection = reportSection;
  }

  public Category getCategory()
  {
    return category;
  }

  public void setCategory(Category category)
  {
    this.category = category;
  }

  public AimExpertInventory getAimExpertInventory()
  {
    return aimExpertInventory;
  }

  public void setAimExpertInventory(AimExpertInventory aimExpertInventory)
  {
    this.aimExpertInventory = aimExpertInventory;
  }

  public Subject getSubject()
  {
    return subject;
  }

  public void setSubject(Subject subject)
  {
    this.subject = subject;
  }
}
