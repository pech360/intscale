package com.synlabs.intscale.entity.Report;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.enums.RangeValue;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class ScoreRange extends BaseEntity {
    @Column(precision = 16, scale = 5)
    private BigDecimal startFrom;
    @Column(precision = 16, scale = 5)
    private BigDecimal endAt;
    @Enumerated(EnumType.STRING)
    private RangeValue rangeOf;
    private String text;
    @Column(length = 1200)
    private String innateTalent;
    @Column(length = 1200)
    private String strength;
    @Column(length = 1200)
    private String weakness;
    @Column(length = 1200)
    private String learningStyle;
    @Column(length = 1200)
    private String learningStyleInference;
    @Column(length = 1200)
    private String learningStyleRecommendation;
    @Column(length = 1200)
    private String developmentPlanForStrength;
    @Column(length = 1200)
    private String developmentPlanForWeakness;

    public BigDecimal getStartFrom() {
        return startFrom;
    }

    public void setStartFrom(BigDecimal startFrom) {
        this.startFrom = startFrom;
    }

    public BigDecimal getEndAt() {
        return endAt;
    }

    public void setEndAt(BigDecimal endAt) {
        this.endAt = endAt;
    }

    public RangeValue getRangeOf() {
        return rangeOf;
    }

    public void setRangeOf(RangeValue rangeOf) {
        this.rangeOf = rangeOf;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getInnateTalent() {
        return innateTalent;
    }

    public void setInnateTalent(String innateTalent) {
        this.innateTalent = innateTalent;
    }

    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    public String getWeakness() {
        return weakness;
    }

    public void setWeakness(String weakness) {
        this.weakness = weakness;
    }

    public String getLearningStyle() {
        return learningStyle;
    }

    public void setLearningStyle(String learningStyle) {
        this.learningStyle = learningStyle;
    }

    public String getLearningStyleInference()
    {
        return learningStyleInference;
    }

    public void setLearningStyleInference(String learningStyleInference)
    {
        this.learningStyleInference = learningStyleInference;
    }

    public String getLearningStyleRecommendation()
    {
        return learningStyleRecommendation;
    }

    public void setLearningStyleRecommendation(String learningStyleRecommendation)
    {
        this.learningStyleRecommendation = learningStyleRecommendation;
    }

    public String getDevelopmentPlanForStrength()
    {
        return developmentPlanForStrength;
    }

    public void setDevelopmentPlanForStrength(String developmentPlanForStrength)
    {
        this.developmentPlanForStrength = developmentPlanForStrength;
    }

    public String getDevelopmentPlanForWeakness()
    {
        return developmentPlanForWeakness;
    }

    public void setDevelopmentPlanForWeakness(String developmentPlanForWeakness)
    {
        this.developmentPlanForWeakness = developmentPlanForWeakness;
    }
}
