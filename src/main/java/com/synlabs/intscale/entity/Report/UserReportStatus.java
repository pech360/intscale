package com.synlabs.intscale.entity.Report;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.entity.user.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class UserReportStatus extends BaseEntity
{
  @ManyToOne
  private User                   user;
  private Long                   reportId;
  private String                 reportName;
  private String                 status;
  private String                 filename;
  private Boolean flag=false;
  @OneToMany(mappedBy = "userReportStatus")
  private List<AimReportContent> aimReportContents = new ArrayList<>();
  @Column(length = 1000)
  private String reason;

  @Override
  public User getUser()
  {
    return user;
  }

  public void setUser(User user)
  {
    this.user = user;
  }

  public Long getReportId()
  {
    return reportId;
  }

  public void setReportId(Long reportId)
  {
    this.reportId = reportId;
  }

  public String getReportName()
  {
    return reportName;
  }

  public void setReportName(String reportName)
  {
    this.reportName = reportName;
  }

  public String getStatus()
  {
    return status;
  }

  public void setStatus(String status)
  {
    this.status = status;
  }

  public String getFilename()
  {
    return filename;
  }

  public void setFilename(String filename)
  {
    this.filename = filename;
  }

  public List<AimReportContent> getAimReportContents()
  {
    return aimReportContents;
  }

  public void setAimReportContents(List<AimReportContent> aimReportContents)
  {
    this.aimReportContents = aimReportContents;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public Boolean getFlag() {
    return flag;
  }

  public void setFlag(Boolean flag) {
    this.flag = flag;
  }
}
