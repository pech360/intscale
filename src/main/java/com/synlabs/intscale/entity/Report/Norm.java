package com.synlabs.intscale.entity.Report;

import com.synlabs.intscale.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;

@Entity
public class Norm extends BaseEntity
{
  private String language;

  @Column(precision = 16, scale = 5)
  private BigDecimal juniorMeanScore;

  @Column(precision = 16, scale = 5)
  private BigDecimal juniorStandardDeviation;

  @Column(precision = 16, scale = 5)
  private BigDecimal seniorMeanScore;

  @Column(precision = 16, scale = 5)
  private BigDecimal seniorStandardDeviation;

  @Column(precision = 16, scale = 5)
  private BigDecimal secondaryMeanScore;

  @Column(precision = 16, scale = 5)
  private BigDecimal secondaryStandardDeviation;

  @ManyToOne
  private Category category;

  @ManyToOne
  private ReportSection reportSection;

  public String getLanguage()
  {
    return language;
  }

  public void setLanguage(String language)
  {
    this.language = language;
  }

  public BigDecimal getJuniorMeanScore()
  {
    return juniorMeanScore;
  }

  public void setJuniorMeanScore(BigDecimal juniorMeanScore)
  {
    this.juniorMeanScore = juniorMeanScore;
  }

  public BigDecimal getJuniorStandardDeviation()
  {
    return juniorStandardDeviation;
  }

  public void setJuniorStandardDeviation(BigDecimal juniorStandardDeviation)
  {
    this.juniorStandardDeviation = juniorStandardDeviation;
  }

  public BigDecimal getSeniorMeanScore()
  {
    return seniorMeanScore;
  }

  public void setSeniorMeanScore(BigDecimal seniorMeanScore)
  {
    this.seniorMeanScore = seniorMeanScore;
  }

  public BigDecimal getSeniorStandardDeviation()
  {
    return seniorStandardDeviation;
  }

  public void setSeniorStandardDeviation(BigDecimal seniorStandardDeviation)
  {
    this.seniorStandardDeviation = seniorStandardDeviation;
  }

  public BigDecimal getSecondaryMeanScore()
  {
    return secondaryMeanScore;
  }

  public void setSecondaryMeanScore(BigDecimal secondaryMeanScore)
  {
    this.secondaryMeanScore = secondaryMeanScore;
  }

  public BigDecimal getSecondaryStandardDeviation()
  {
    return secondaryStandardDeviation;
  }

  public void setSecondaryStandardDeviation(BigDecimal secondaryStandardDeviation)
  {
    this.secondaryStandardDeviation = secondaryStandardDeviation;
  }

  public Category getCategory()
  {
    return category;
  }

  public void setCategory(Category category)
  {
    this.category = category;
  }

  public ReportSection getReportSection()
  {
    return reportSection;
  }

  public void setReportSection(ReportSection reportSection)
  {
    this.reportSection = reportSection;
  }
}
