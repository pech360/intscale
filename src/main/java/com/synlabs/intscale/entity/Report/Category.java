package com.synlabs.intscale.entity.Report;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.entity.test.Feedback;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vikas kumar on 28-05-2017.
 */
@Entity
public class Category extends BaseEntity
{
  private String name;
  private String displayName;
  @Column(length = 1200)
  private String description;
  private String color;
  @Column(nullable = false)
  private boolean        active    = true;
  @OneToMany(mappedBy = "category")
  private List<Feedback> feedbacks = new ArrayList<>();
  @ManyToOne
  private Category parent;

  @Column(length = 1200)
  private String  highText;
  @Column(length = 1200)
  private String  lowText;
  private String  shortHighText;
  private String  shortLowText;
  private Integer sequence;
  private String  type;

  @Column(precision = 16, scale = 5)
  private BigDecimal juniorMeanScore;

  @Column(precision = 16, scale = 5)
  private BigDecimal juniorStandardDeviation;

  @Column(precision = 16, scale = 5)
  private BigDecimal seniorMeanScore;

  @Column(precision = 16, scale = 5)
  private BigDecimal seniorStandardDeviation;

  @Column(precision = 16, scale = 5)
  private BigDecimal secondaryMeanScore;

  @Column(precision = 16, scale = 5)
  private BigDecimal secondaryStandardDeviation;

  @OneToMany
  private List<ScoreRange> juniorScoreRanges = new ArrayList<>();

  @OneToMany
  private List<ScoreRange> seniorScoreRanges = new ArrayList<>();

  @OneToMany(mappedBy = "parent")
  private List<Category> subCategories = new ArrayList<>();

  @OneToMany(mappedBy = "category")
  private List<Norm> norms = new ArrayList<>();

  public List<Category> getSubCategories()
  {
    return subCategories;
  }

  public void setSubCategories(List<Category> subCategories)
  {
    this.subCategories = subCategories;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public String getColor()
  {
    return color;
  }

  public void setColor(String color)
  {
    this.color = color;
  }

  public Category getParent()
  {
    return parent;
  }

  public void setParent(Category parent)
  {
    this.parent = parent;
  }

  public List<Feedback> getFeedbacks()
  {
    return feedbacks;
  }

  public void setFeedbacks(List<Feedback> feedbacks)
  {
    this.feedbacks = feedbacks;
  }

  public boolean isActive()
  {
    return active;
  }

  public void setActive(boolean active)
  {
    this.active = active;
  }

  public String getHighText()
  {
    return highText;
  }

  public void setHighText(String highText)
  {
    this.highText = highText;
  }

  public String getLowText()
  {
    return lowText;
  }

  public void setLowText(String lowText)
  {
    this.lowText = lowText;
  }

  public List<ScoreRange> getJuniorScoreRanges()
  {
    return juniorScoreRanges;
  }

  public void setJuniorScoreRanges(List<ScoreRange> juniorScoreRanges)
  {
    this.juniorScoreRanges = juniorScoreRanges;
  }

  public List<ScoreRange> getSeniorScoreRanges()
  {
    return seniorScoreRanges;
  }

  public void setSeniorScoreRanges(List<ScoreRange> seniorScoreRanges)
  {
    this.seniorScoreRanges = seniorScoreRanges;
  }

  public BigDecimal getJuniorMeanScore()
  {
    return juniorMeanScore;
  }

  public void setJuniorMeanScore(BigDecimal juniorMeanScore)
  {
    this.juniorMeanScore = juniorMeanScore;
  }

  public BigDecimal getJuniorStandardDeviation()
  {
    return juniorStandardDeviation;
  }

  public void setJuniorStandardDeviation(BigDecimal juniorStandardDeviation)
  {
    this.juniorStandardDeviation = juniorStandardDeviation;
  }

  public String getDisplayName()
  {
    return displayName;
  }

  public void setDisplayName(String displayName)
  {
    this.displayName = displayName;
  }

  public Integer getSequence()
  {
    return sequence;
  }

  public void setSequence(Integer sequence)
  {
    this.sequence = sequence;
  }

  public String getShortHighText()
  {
    return shortHighText;
  }

  public void setShortHighText(String shortHighText)
  {
    this.shortHighText = shortHighText;
  }

  public String getShortLowText()
  {
    return shortLowText;
  }

  public void setShortLowText(String shortLowText)
  {
    this.shortLowText = shortLowText;
  }

  public String getType()
  {
    return type;
  }

  public void setType(String type)
  {
    this.type = type;
  }

  public BigDecimal getSeniorMeanScore()
  {
    return seniorMeanScore;
  }

  public void setSeniorMeanScore(BigDecimal seniorMeanScore)
  {
    this.seniorMeanScore = seniorMeanScore;
  }

  public BigDecimal getSeniorStandardDeviation()
  {
    return seniorStandardDeviation;
  }

  public void setSeniorStandardDeviation(BigDecimal seniorStandardDeviation)
  {
    this.seniorStandardDeviation = seniorStandardDeviation;
  }

  public BigDecimal getSecondaryMeanScore()
  {
    return secondaryMeanScore;
  }

  public void setSecondaryMeanScore(BigDecimal secondaryMeanScore)
  {
    this.secondaryMeanScore = secondaryMeanScore;
  }

  public BigDecimal getSecondaryStandardDeviation()
  {
    return secondaryStandardDeviation;
  }

  public void setSecondaryStandardDeviation(BigDecimal secondaryStandardDeviation)
  {
    this.secondaryStandardDeviation = secondaryStandardDeviation;
  }

  public List<Norm> getNorms()
  {
    return norms;
  }

  public void setNorms(List<Norm> norms)
  {
    this.norms = norms;
  }
}
