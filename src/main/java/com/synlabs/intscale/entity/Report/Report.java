package com.synlabs.intscale.entity.Report;

import com.synlabs.intscale.entity.BaseEntity;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Report extends BaseEntity {
    private String name;
    @Column(length = 600)
    private String description;

    @OneToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ReportSection> reportSections = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ReportSection> getReportSections() {
        return reportSections;
    }

    public void setReportSections(List<ReportSection> reportSections) {
        this.reportSections = reportSections;
    }
}
