package com.synlabs.intscale.entity.Report;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.entity.Report.Report;
import com.synlabs.intscale.entity.Report.ScoreRange;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.jdo.annotations.Unique;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
public class ReportSection extends BaseEntity {

    @Unique
    private String name;
    @Column(length = 600)
    private String description;
    private String idsOfTestForEvaluation;
    @Column(precision = 16, scale = 5)
    private BigDecimal juniorMeanScore;
    @Column(precision = 16, scale = 5)
    private BigDecimal juniorStandardDeviation;
    @Column(precision = 16, scale = 5)
    private BigDecimal seniorMeanScore;
    @Column(precision = 16, scale = 5)
    private BigDecimal seniorStandardDeviation;
    @Column(precision = 16, scale = 5)
    private BigDecimal secondaryMeanScore;
    @Column(precision = 16, scale = 5)
    private BigDecimal secondaryStandardDeviation;

    private Integer sequence;
    @OneToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ScoreRange> scoreRanges = new ArrayList<>();
    @OneToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ScoreRange> juniorScoreRanges = new ArrayList<>();
    @OneToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ScoreRange> seniorScoreRanges = new ArrayList<>();

    @OneToMany(mappedBy = "reportSection")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Norm> norms = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ScoreRange> getScoreRanges() {
        return scoreRanges;
    }

    public void setScoreRanges(List<ScoreRange> scoreRanges) {
        this.scoreRanges = scoreRanges;
    }

    public String getIdsOfTestForEvaluation() {
        return idsOfTestForEvaluation;
    }

    public void setIdsOfTestForEvaluation(String idsOfTestForEvaluation) {
        this.idsOfTestForEvaluation = idsOfTestForEvaluation;
    }

    public BigDecimal getJuniorMeanScore() {
        return juniorMeanScore;
    }

    public void setJuniorMeanScore(BigDecimal juniorMeanScore) {
        this.juniorMeanScore = juniorMeanScore;
    }

    public BigDecimal getJuniorStandardDeviation() {
        return juniorStandardDeviation;
    }

    public void setJuniorStandardDeviation(BigDecimal juniorStandardDeviation) {
        this.juniorStandardDeviation = juniorStandardDeviation;
    }

    public BigDecimal getSeniorMeanScore() {
        return seniorMeanScore;
    }

    public void setSeniorMeanScore(BigDecimal seniorMeanScore) {
        this.seniorMeanScore = seniorMeanScore;
    }

    public BigDecimal getSeniorStandardDeviation() {
        return seniorStandardDeviation;
    }

    public void setSeniorStandardDeviation(BigDecimal seniorStandardDeviation) {
        this.seniorStandardDeviation = seniorStandardDeviation;
    }

    public List<ScoreRange> getJuniorScoreRanges() {
        return juniorScoreRanges;
    }

    public void setJuniorScoreRanges(List<ScoreRange> juniorScoreRanges) {
        this.juniorScoreRanges = juniorScoreRanges;
    }

    public List<ScoreRange> getSeniorScoreRanges() {
        return seniorScoreRanges;
    }

    public void setSeniorScoreRanges(List<ScoreRange> seniorScoreRanges) {
        this.seniorScoreRanges = seniorScoreRanges;
    }

    public BigDecimal getSecondaryMeanScore()
    {
        return secondaryMeanScore;
    }

    public void setSecondaryMeanScore(BigDecimal secondaryMeanScore)
    {
        this.secondaryMeanScore = secondaryMeanScore;
    }

    public BigDecimal getSecondaryStandardDeviation()
    {
        return secondaryStandardDeviation;
    }

    public void setSecondaryStandardDeviation(BigDecimal secondaryStandardDeviation)
    {
        this.secondaryStandardDeviation = secondaryStandardDeviation;
    }

    public Integer getSequence()
    {
        return sequence;
    }

    public void setSequence(Integer sequence)
    {
        this.sequence = sequence;
    }

    public List<Norm> getNorms()
    {
        return norms;
    }

    public void setNorms(List<Norm> norms)
    {
        this.norms = norms;
    }
}
