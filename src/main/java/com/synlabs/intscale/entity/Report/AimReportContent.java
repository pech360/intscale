package com.synlabs.intscale.entity.Report;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.enums.AimReportContentType;
import com.synlabs.intscale.enums.RangeValue;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;

@Entity
public class AimReportContent extends BaseEntity
{
  @ManyToOne
  private UserReportStatus userReportStatus;

  @Enumerated(EnumType.STRING)
  private AimReportContentType type;

  private String name;
  private BigDecimal rawScore;
  private BigDecimal standardScore;

  @Enumerated(EnumType.STRING)
  private RangeValue scoreLabel;

  private Integer scoreLabelRating;
  private String reportSectionName;
  private Integer sequence;

  public UserReportStatus getUserReportStatus()
  {
    return userReportStatus;
  }

  public void setUserReportStatus(UserReportStatus userReportStatus)
  {
    this.userReportStatus = userReportStatus;
  }

  public AimReportContentType getType()
  {
    return type;
  }

  public void setType(AimReportContentType type)
  {
    this.type = type;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public BigDecimal getRawScore()
  {
    return rawScore;
  }

  public void setRawScore(BigDecimal rawScore)
  {
    this.rawScore = rawScore;
  }

  public BigDecimal getStandardScore()
  {
    return standardScore;
  }

  public void setStandardScore(BigDecimal standardScore)
  {
    this.standardScore = standardScore;
  }

  public RangeValue getScoreLabel()
  {
    return scoreLabel;
  }

  public void setScoreLabel(RangeValue scoreLabel)
  {
    this.scoreLabel = scoreLabel;
  }

  public Integer getScoreLabelRating()
  {
    return scoreLabelRating;
  }

  public void setScoreLabelRating(Integer scoreLabelRating)
  {
    this.scoreLabelRating = scoreLabelRating;
  }

  public String getReportSectionName()
  {
    return reportSectionName;
  }

  public void setReportSectionName(String reportSectionName)
  {
    this.reportSectionName = reportSectionName;
  }

  public Integer getSequence()
  {
    return sequence;
  }

  public void setSequence(Integer sequence)
  {
    this.sequence = sequence;
  }
}
