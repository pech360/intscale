package com.synlabs.intscale.entity.test;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.enums.TagType;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

@Entity
public class SubTag extends BaseEntity
{
  private String name;
  @ManyToOne
  private QuestionTag tag;

  @Enumerated(EnumType.STRING)
  private TagType type;


  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public QuestionTag getTag()
  {
    return tag;
  }

  public void setTag(QuestionTag tag)
  {
    this.tag = tag;
  }

  public TagType getType()
  {
    return type;
  }

  public void setType(TagType type)
  {
    this.type = type;
  }
}
