package com.synlabs.intscale.entity.test;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.entity.Report.Category;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by itrs on 8/23/201
 */
@Entity
public class Feedback extends BaseEntity
{
  private int      marksMin;
  private int      marksMax;
  @Column(length = 1500)
  private String   expertAnaylsis;
  @Column(length = 1500)
  private String   developementPlan;
  @ManyToOne
  private Category category;

  public int getMarksMin()
  {
    return marksMin;
  }

  public void setMarksMin(int marksMin)
  {
    this.marksMin = marksMin;
  }

  public int getMarksMax()
  {
    return marksMax;
  }

  public void setMarksMax(int marksMax)
  {
    this.marksMax = marksMax;
  }

  public String getExpertAnaylsis()
  {
    return expertAnaylsis;
  }

  public void setExpertAnaylsis(String expertAnaylsis)
  {
    this.expertAnaylsis = expertAnaylsis;
  }

  public String getDevelopementPlan()
  {
    return developementPlan;
  }

  public void setDevelopementPlan(String developementPlan)
  {
    this.developementPlan = developementPlan;
  }

  public Category getCategory()
  {
    return category;
  }

  public void setCategory(Category category)
  {
    this.category = category;
  }
}






