package com.synlabs.intscale.entity.test;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.enums.RangeValue;
import com.synlabs.intscale.enums.RecordType;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
public class TestResultSummary extends BaseEntity
{
  private Long  userId;
  private String studentName;
  private String grade;
  private String section;
  private Long subjectTeacherId;
  private Long constructId;
  private String constructName;
  private Long subConstructId;
  private String subConstructName;
  private Long testId;
  private String testName;
  private Long testResultId;
  private Long maxMarks;
  private Long rawScore;
  private Double percentageScore;
  @Enumerated(EnumType.STRING)
  private RangeValue scoreRange;
  private Double syllabusCompleted;
  @Enumerated(EnumType.STRING)
  private RecordType recordType;

  public Long getUserId()
  {
    return userId;
  }

  public void setUserId(Long userId)
  {
    this.userId = userId;
  }

  public String getStudentName()
  {
    return studentName;
  }

  public void setStudentName(String studentName)
  {
    this.studentName = studentName;
  }

  public String getGrade()
  {
    return grade;
  }

  public void setGrade(String grade)
  {
    this.grade = grade;
  }

  public String getSection()
  {
    return section;
  }

  public void setSection(String section)
  {
    this.section = section;
  }

  public Long getSubjectTeacherId()
  {
    return subjectTeacherId;
  }

  public void setSubjectTeacherId(Long subjectTeacherId)
  {
    this.subjectTeacherId = subjectTeacherId;
  }

  public Long getConstructId()
  {
    return constructId;
  }

  public void setConstructId(Long constructId)
  {
    this.constructId = constructId;
  }

  public String getConstructName()
  {
    return constructName;
  }

  public void setConstructName(String constructName)
  {
    this.constructName = constructName;
  }

  public Long getSubConstructId()
  {
    return subConstructId;
  }

  public void setSubConstructId(Long subConstructId)
  {
    this.subConstructId = subConstructId;
  }

  public String getSubConstructName()
  {
    return subConstructName;
  }

  public void setSubConstructName(String subConstructName)
  {
    this.subConstructName = subConstructName;
  }

  public Long getTestId()
  {
    return testId;
  }

  public void setTestId(Long testId)
  {
    this.testId = testId;
  }

  public String getTestName()
  {
    return testName;
  }

  public void setTestName(String testName)
  {
    this.testName = testName;
  }

  public Long getTestResultId()
  {
    return testResultId;
  }

  public void setTestResultId(Long testResultId)
  {
    this.testResultId = testResultId;
  }

  public Long getMaxMarks()
  {
    return maxMarks;
  }

  public void setMaxMarks(Long maxMarks)
  {
    this.maxMarks = maxMarks;
  }

  public Long getRawScore()
  {
    return rawScore;
  }

  public void setRawScore(Long rawScore)
  {
    this.rawScore = rawScore;
  }

  public Double getPercentageScore()
  {
    return percentageScore;
  }

  public void setPercentageScore(Double percentageScore)
  {
    this.percentageScore = percentageScore;
  }

  public RangeValue getScoreRange()
  {
    return scoreRange;
  }

  public void setScoreRange(RangeValue scoreRange)
  {
    this.scoreRange = scoreRange;
  }

  public Double getSyllabusCompleted()
  {
    return syllabusCompleted;
  }

  public void setSyllabusCompleted(Double syllabusCompleted)
  {
    this.syllabusCompleted = syllabusCompleted;
  }

  public RecordType getRecordType()
  {
    return recordType;
  }

  public void setRecordType(RecordType recordType)
  {
    this.recordType = recordType;
  }
}
