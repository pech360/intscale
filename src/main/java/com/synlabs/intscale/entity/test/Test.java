package com.synlabs.intscale.entity.test;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.entity.threesixty.StakeholderQuestion;
import com.synlabs.intscale.entity.user.Teacher;
import com.synlabs.intscale.entity.user.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Test extends BaseEntity
{
  private String name;

  private String logo;

  private String stakeholderImage;

  private String stakeholderVideo;

  @Column(length = 600)
  private String introduction;

  @Column(length = 600)
  private String participation;

  @Column(length = 600)
  private String disclaimer;

  @Column(name = "_procedure", length = 600)
  private String procedure;

  @Column(length = 600)
  private String description;

  @Column(length = 600)
  private String  notes;
  @ManyToOne
  private Test parent;

  @Column(length = 1000)
  private String  primaryReportText;

  @Column(length = 1000)
  private String  secondaryReportText;
  @ManyToOne
  private Teacher teacher;

  private int     ageMin;
  private int     ageMax;
  private boolean paid;
  private String  scoreCategory;
  private String  scoreSubCategory;
  private boolean showResults;
  private boolean random;
  private String  redirectTo;
  private boolean autoIncrementQuestion;
  private boolean navigationAllowed;
  private boolean skipQuestions;
  private boolean published;
  private String  userType;
  private String  studentType;
  private String  gender;
  private String grades[];
  private String  language;
  private boolean disableSubmit;
  private boolean discontinueTest;
  private int discontinueTestCriteria;
  @OneToMany(mappedBy = "test", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<TestQuestion> questions = new ArrayList<>();
  private String videoName;
  private int submitLimit;
  private Long versionId;
  private boolean latestVersion;
  private int sequence;
  private boolean testForce;
  private boolean goLive;
  private boolean leafNode;
  private boolean construct;
  @Column(length = 600)
  private String scoringFormula;
  private String reportType;
  private Boolean threeSixtyEnabled;
  @OneToMany(mappedBy = "test")
  private List<StakeholderQuestion> stakeholderQuestions = new ArrayList<>();

  public List<StakeholderQuestion> getStakeholderQuestions()
  {
    return stakeholderQuestions;
  }

  public void setStakeholderQuestions(List<StakeholderQuestion> stakeholderQuestions)
  {
    this.stakeholderQuestions = stakeholderQuestions;
  }

  public Boolean getThreeSixtyEnabled()
  {
    return threeSixtyEnabled;
  }

  public void setThreeSixtyEnabled(Boolean threeSixtyEnabled)
  {
    this.threeSixtyEnabled = threeSixtyEnabled;
  }

  public Teacher getTeacher() {
     return teacher;
  }

  public void setTeacher(Teacher teacher) {
    this.teacher = teacher;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getLogo()
  {
    return logo;
  }

  public void setLogo(String logo)
  {
    this.logo = logo;
  }

  public String getIntroduction()
  {
    return introduction;
  }

  public void setIntroduction(String introduction)
  {
    this.introduction = introduction;
  }

  public String getParticipation()
  {
    return participation;
  }

  public void setParticipation(String participation)
  {
    this.participation = participation;
  }

  public String getDisclaimer()
  {
    return disclaimer;
  }

  public void setDisclaimer(String disclaimer)
  {
    this.disclaimer = disclaimer;
  }

  public String getProcedure()
  {
    return procedure;
  }

  public void setProcedure(String procedure)
  {
    this.procedure = procedure;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public int getAgeMin()
  {
    return ageMin;
  }

  public void setAgeMin(int ageMin)
  {
    this.ageMin = ageMin;
  }

  public int getAgeMax()
  {
    return ageMax;
  }

  public void setAgeMax(int ageMax)
  {
    this.ageMax = ageMax;
  }

  public boolean isPaid()
  {
    return paid;
  }

  public void setPaid(boolean paid)
  {
    this.paid = paid;
  }

  public String getScoreCategory()
  {
    return scoreCategory;
  }

  public void setScoreCategory(String scoreCategory)
  {
    this.scoreCategory = scoreCategory;
  }

  public String getScoreSubCategory()
  {
    return scoreSubCategory;
  }

  public void setScoreSubCategory(String scoreSubCategory)
  {
    this.scoreSubCategory = scoreSubCategory;
  }

  public String getNotes()
  {
    return notes;
  }

  public void setNotes(String notes)
  {
    this.notes = notes;
  }

  public boolean isShowResults()
  {
    return showResults;
  }

  public void setShowResults(boolean showResults)
  {
    this.showResults = showResults;
  }

  public boolean isRandom()
  {
    return random;
  }

  public void setRandom(boolean random)
  {
    this.random = random;
  }

  public String getRedirectTo()
  {
    return redirectTo;
  }

  public void setRedirectTo(String redirectTo)
  {
    this.redirectTo = redirectTo;
  }

  public boolean isAutoIncrementQuestion()
  {
    return autoIncrementQuestion;
  }

  public void setAutoIncrementQuestion(boolean autoIncrementQuestion)
  {
    this.autoIncrementQuestion = autoIncrementQuestion;
  }

  public boolean isNavigationAllowed()
  {
    return navigationAllowed;
  }

  public void setNavigationAllowed(boolean navigationAllowed)
  {
    this.navigationAllowed = navigationAllowed;
  }

  public boolean isSkipQuestions()
  {
    return skipQuestions;
  }

  public void setSkipQuestions(boolean skipQuestions)
  {
    this.skipQuestions = skipQuestions;
  }

  public boolean isPublished()
  {
    return published;
  }

  public void setPublished(boolean published)
  {
    this.published = published;
  }

  public List<TestQuestion> getQuestions()
  {
    return questions;
  }

  public void setQuestions(List<TestQuestion> questions)
  {
    this.questions = questions;
  }

  public String getUserType()
  {
    return userType;
  }

  public void setUserType(String userType)
  {
    this.userType = userType;
  }

  public String getStudentType()
  {
    return studentType;
  }

  public void setStudentType(String studentType)
  {
    this.studentType = studentType;
  }

  public String getGender()
  {
    return gender;
  }

  public void setGender(String gender)
  {
    this.gender = gender;
  }

  public String[] getGrades() {
    return grades;
  }

  public void setGrades(String[] grades) {
    this.grades = grades;
  }

  public String getLanguage()
  {
    return language;
  }

  public void setLanguage(String language)
  {
    this.language = language;
  }

  public String getVideoName()
  {
    return videoName;
  }

  public void setVideoName(String videoName)
  {
    this.videoName = videoName;
  }

  public boolean isDisableSubmit()
  {
    return disableSubmit;
  }

  public void setDisableSubmit(boolean disableSubmit)
  {
    this.disableSubmit = disableSubmit;
  }

  public String getReportType() {
    return reportType;
  }

  public void setReportType(String reportType) {
    this.reportType = reportType;
  }

  public int getSubmitLimit()
  {
    return submitLimit;
  }

  public void setSubmitLimit(int submitLimit)
  {
    this.submitLimit = submitLimit;
  }

  public boolean isDiscontinueTest()
  {
    return discontinueTest;
  }

  public void setDiscontinueTest(boolean discontinueTest)
  {
    this.discontinueTest = discontinueTest;
  }

  public int getDiscontinueTestCriteria()
  {
    return discontinueTestCriteria;
  }

  public Long getVersionId() {
    return versionId;
  }

  public void setVersionId(Long versionId) {
    this.versionId = versionId;
  }

  public boolean isLatestVersion() {
    return latestVersion;
  }

  public void setLatestVersion(boolean latestVersion) {
    this.latestVersion = latestVersion;
  }

  public int getSequence() {
    return sequence;
  }

  public void setSequence(int sequence) {
    this.sequence = sequence;
  }

  public boolean isTestForce() {
    return testForce;
  }

  public void setTestForce(boolean testForce) {
    this.testForce = testForce;
  }

  public boolean isGoLive() {
    return goLive;
  }

  public void setGoLive(boolean goLive) {
    this.goLive = goLive;
  }

  public Test getParent() {
    return parent;
  }

  public void setParent(Test parent) {
    this.parent = parent;
  }

  public boolean isLeafNode() {
    return leafNode;
  }

  public void setLeafNode(boolean leafNode) {
    this.leafNode = leafNode;
  }

  public boolean isConstruct() {
    return construct;
  }

  public void setConstruct(boolean construct) {
    this.construct = construct;
  }

  public void setDiscontinueTestCriteria(int discontinueTestCriteria)
  {
    this.discontinueTestCriteria = discontinueTestCriteria;
  }

  public String getStakeholderImage() {
    return stakeholderImage;
  }

  public void setStakeholderImage(String stakeholderImage) {
    this.stakeholderImage = stakeholderImage;
  }

  public String getStakeholderVideo() {
    return stakeholderVideo;
  }

  public void setStakeholderVideo(String stakeholderVideo) {
    this.stakeholderVideo = stakeholderVideo;
  }

  public String getPrimaryReportText()
  {
    return primaryReportText;
  }

  public void setPrimaryReportText(String primaryReportText)
  {
    this.primaryReportText = primaryReportText;
  }

  public String getSecondaryReportText()
  {
    return secondaryReportText;
  }

  public void setSecondaryReportText(String secondaryReportText)
  {
    this.secondaryReportText = secondaryReportText;
  }

  public String getScoringFormula()
  {
    return scoringFormula;
  }

  public void setScoringFormula(String scoringFormula)
  {
    this.scoringFormula = scoringFormula;
  }

  public void addQuestions(TestQuestion question)
  {
    if (questions == null)
    {
      questions = new ArrayList<>();
    }
    questions.add(question);
  }

  public boolean equals(Object obj)
  {
    if (this == obj)
    {
      return true;
    }
    if (obj == null)
    {
      return false;
    }
    if (getClass() != obj.getClass())
    {
      return false;
    }
    BaseEntity other = (BaseEntity) obj;
    if (getId() == null)
    {
      return false;
    }
    else if (!getId().equals(other.getId()))
    {
      return false;
    }
    return true;
  }

  public int hashCode()
  {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
    return result;
  }

  @Override
  public String toString()
  {
    return "Test{" +
        "id=" + getId() +
        '}';
  }
}
