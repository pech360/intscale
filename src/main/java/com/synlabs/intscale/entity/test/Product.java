package com.synlabs.intscale.entity.test;

import com.synlabs.intscale.entity.BaseEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by India on 1/24/2018.
 */
@Entity
public class Product extends BaseEntity {
    private String name;
    private String description;
    @ManyToMany
    private List<Test> test=new ArrayList<>();
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Test> getTest() {
        return test;
    }

    public void setTest(List<Test> test) {
        this.test = test;
    }
}
