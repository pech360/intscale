package com.synlabs.intscale.entity.test;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.entity.Pilot;
import com.synlabs.intscale.entity.user.User;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class TestResult extends BaseEntity
{
  @ManyToOne
  private User user;
  @ManyToOne
  private Test test;
  @OneToMany(mappedBy = "testResult")
  private List<UserAnswer> userAnswers = new ArrayList<>();
  @ManyToOne
  private Pilot pilot;
  private String subConstructName;
  private Date testLoadedAt;
  private Date testBeginAt;
  private Date testFinishAt;


  public User getUser()
  {
    return user;
  }

  public void setUser(User user)
  {
    this.user = user;
  }

  public Test getTest()
  {
    return test;
  }

  public void setTest(Test test)
  {
    this.test = test;
  }

  public List<UserAnswer> getUserAnswers()
  {
    return userAnswers;
  }

  public void setUserAnswers(List<UserAnswer> userAnswers)
  {
    this.userAnswers = userAnswers;
  }

  public Pilot getPilot() {
    return pilot;
  }

  public void setPilot(Pilot pilot) {
    this.pilot = pilot;
  }

  public String getSubConstructName() {
    return subConstructName;
  }

  public void setSubConstructName(String subConstructName) {
    this.subConstructName = subConstructName;
  }

  public Date getTestLoadedAt() {
    return testLoadedAt;
  }

  public void setTestLoadedAt(Date testLoadedAt) {
    this.testLoadedAt = testLoadedAt;
  }

  public Date getTestBeginAt() {
    return testBeginAt;
  }

  public void setTestBeginAt(Date testBeginAt) {
    this.testBeginAt = testBeginAt;
  }

  public Date getTestFinishAt()
  {
    return testFinishAt;
  }

  public void setTestFinishAt(Date testFinishAt)
  {
    this.testFinishAt = testFinishAt;
  }

  public void addUserAnswers(UserAnswer userAnswer)
  {
    if (userAnswers == null)
    {
      userAnswers = new ArrayList<>();
    }
    userAnswers.add(userAnswer);
  }

  public boolean equals(Object obj)
  {
    if (this == obj)
    {
      return true;
    }
    if (obj == null)
    {
      return false;
    }
    if (getClass() != obj.getClass())
    {
      return false;
    }
    BaseEntity other = (BaseEntity) obj;
    if (getId() == null)
    {
      return false;
    }
    else if (!getId().equals(other.getId()))
    {
      return false;
    }
    return true;
  }

  public int hashCode()
  {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
    return result;
  }

  @Override
  public String toString()
  {
    return "TestResult{" +
        "id=" + getId() +
        '}';
  }
}