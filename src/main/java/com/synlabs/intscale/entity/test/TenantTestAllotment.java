package com.synlabs.intscale.entity.test;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.entity.user.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by itrs on 9/19/2017.
 */
@Entity
public class TenantTestAllotment extends BaseEntity
{
  private String userEmail;
  private int testCount;
  private int usedTestCount;
  private int availableTestCount;
  @Temporal(TemporalType.TIMESTAMP)
  private Date validTillDate;
  @ManyToMany
  private List<Test> tests =new ArrayList<>();
  @ManyToMany
  private List<Product> products=new ArrayList<>();

  public String getUserEmail() {
    return userEmail;
  }

  public void setUserEmail(String userEmail) {
    this.userEmail = userEmail;
  }

  public int getTestCount()
  {
    return testCount;
  }

  public void setTestCount(int testCount)
  {
    this.testCount = testCount;
  }

  public int getUsedTestCount()
  {
    return usedTestCount;
  }

  public void setUsedTestCount(int usedTestCount)
  {
    this.usedTestCount = usedTestCount;
  }

  public int getAvailableTestCount()
  {
    return availableTestCount;
  }

  public void setAvailableTestCount(int availableTestCount)
  {
    this.availableTestCount = availableTestCount;
  }

  public Date getValidTillDate() {
    return validTillDate;
  }

  public void setValidTillDate(Date validTillDate) {
    this.validTillDate = validTillDate;
  }

  public List<Test> getTests()
  {
    return tests;
  }

  public void setTests(List<Test> tests)
  {
    this.tests = tests;
  }

  public List<Product> getProducts() {
    return products;
  }

  public void setProducts(List<Product> products) {
    this.products = products;
  }
}
