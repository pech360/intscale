package com.synlabs.intscale.entity.test;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.entity.Report.Category;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class TestQuestion extends BaseEntity
{
  @ManyToOne
  private Test test;

  @ManyToOne
  private Question question;

  @ManyToMany
  @JoinTable
      (
          name = "test_question_category",
          joinColumns = { @JoinColumn(name = "TEST_QUESTION_ID", referencedColumnName = "ID") },
          inverseJoinColumns = { @JoinColumn(name = "CATEGORY_ID", referencedColumnName = "ID") }
      )
  private List<Category> category = new ArrayList<>();

  @ManyToMany
  @JoinTable
      (
          name = "test_question_subcategory",
          joinColumns = { @JoinColumn(name = "TEST_QUESTION_ID", referencedColumnName = "ID") },
          inverseJoinColumns = { @JoinColumn(name = "SUB_CATEGORY_ID", referencedColumnName = "ID") }
      )
  private List<Category> subCategory = new ArrayList<>();

  private int sequence;

  public Test getTest()
  {
    return test;
  }

  public void setTest(Test test)
  {
    this.test = test;
  }

  public Question getQuestion()
  {
    return question;
  }

  public void setQuestion(Question question)
  {
    this.question = question;
  }

  public List<Category> getCategory()
  {
    return category;
  }

  public void setCategory(List<Category> category)
  {
    this.category = category;
  }

  public int getSequence()
  {
    return sequence;
  }

  public void setSequence(int sequence)
  {
    this.sequence = sequence;
  }

  public List<Category> getSubCategory()
  {
    return subCategory;
  }

  public void setSubCategory(List<Category> subCategory)
  {
    this.subCategory = subCategory;
  }

  public TestQuestion(Test test, Question question, Category category, Category subCategory, int index)
  {
    this.test = test;
    this.question = question;
    this.getCategory().add(category);
    this.getSubCategory().add(subCategory);
    this.sequence = ++index;
  }

  public TestQuestion(Test test, Question question, List<Category> categories, List<Category> subCategories)
  {
    this.test = test;
    this.question = question;
    this.getCategory().addAll(categories);
    this.getSubCategory().addAll(subCategories);
  }

  public TestQuestion(Test test, Question question, List<Category> categories, List<Category> subCategories, int index)
  {
    this.test = test;
    this.question = question;
    if(categories!=null){
      this.getCategory().addAll(categories);
    }
    if(subCategories!=null){
      this.getSubCategory().addAll(subCategories);
    }
    this.sequence = ++index;
  }

  public TestQuestion(Test test, Question question, int index)
  {
    this.test = test;
    this.question = question;
    this.sequence = ++index;
  }

  public TestQuestion()
  {
  }
}
