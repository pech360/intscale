package com.synlabs.intscale.entity.test;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.entity.user.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by India on 10/5/2017.
 */
@Entity
public class UserTest extends BaseEntity {

    @OneToOne
    private User userEmail;
    @ManyToMany
    private List<Test> tests = new ArrayList<>();
    private String testStatus;
    private boolean used;
    @ManyToOne
    private User testAssigner;
    private String language;

    public User getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(User userEmail) {
        this.userEmail = userEmail;
    }

    public List<Test> getTests() {
        return tests;
    }

    public void setTests(List<Test> tests) {
        this.tests = tests;
    }

    public String getTestStatus() {
        return testStatus;
    }

    public void setTestStatus(String testStatus) {
        this.testStatus = testStatus;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public User getTestAssigner() {
        return testAssigner;
    }

    public void setTestAssigner(User testAssigner) {
        this.testAssigner = testAssigner;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public boolean equals(Object obj) {
        UserTest userTest = (UserTest) obj;
        if (userTest.getUserEmail().getId() == this.getUserEmail().getId())
            return true;
        return false;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.getUserEmail().getId() == null) ? 0 : this.getUserEmail().getId().hashCode());
        return result;
    }
}
