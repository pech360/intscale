package com.synlabs.intscale.entity.test;

import com.synlabs.intscale.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by vikas kumar on 29-05-2017.
 */
@Entity
public class Subscribers extends BaseEntity
{
  @Column(name="_key")
  private String   key;
  private boolean  used;
  @ManyToOne
  private Subscription subscription;

  public String getKey()
  {
    return key;
  }

  public void setKey(String key)
  {
    this.key = key;
  }

  public boolean isUsed()
  {
    return used;
  }

  public void setUsed(boolean used)
  {
    this.used = used;
  }

  public Subscription getSubscription()
  {
    return subscription;
  }

  public void setSubscription(Subscription subscription)
  {
    this.subscription = subscription;
  }
}
