package com.synlabs.intscale.entity.test;

import com.synlabs.intscale.entity.BaseEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
public class Question extends BaseEntity
{
  @Column(length = 600)
  private String description;
  private boolean hasAnimatedGif;
  private String videoName;
  private boolean hasDummy;
  private boolean hasAudio;
  private String audioName;
  private boolean hasImage;
  private String imageName;
  private boolean hasTimer;
  private int timeInSeconds;
  private boolean hasParagraph;
  private String paraWord;
  private String questionType;
  private int numberOfAnswers;
  private String answerAlignment;
  private String groupName;
  private boolean active=true;
  @Column(length = 600)
  private String hint;

  @OneToMany(mappedBy = "question")
  private List<Answers> answers=new ArrayList<>();

  @ManyToMany
  private List<SubTag> subTags=new ArrayList<>();

  @ManyToOne
  private GroupText groupText;
  private boolean hasValidation;
  private int minLength;
  private int maxLength;
  private boolean specialCharAllowed;
  private boolean alphabetAllowed;
  private boolean numericAllowed;
  private boolean ignoreQuestion = false;
  private boolean hideOptionsUntilVideoEnded;

  public GroupText getGroupText()
  {
    return groupText;
  }

  public void setGroupText(GroupText groupText)
  {
    this.groupText = groupText;
  }

  public List<SubTag> getSubTags()
  {
    return subTags;
  }

  public void setSubTags(List<SubTag> subTags)
  {
    this.subTags = subTags;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public boolean isHasAnimatedGif()
  {
    return hasAnimatedGif;
  }

  public void setHasAnimatedGif(boolean hasAnimatedGif)
  {
    this.hasAnimatedGif = hasAnimatedGif;
  }

  public String getVideoName() {
    return videoName;
  }

  public void setVideoName(String videoName) {
    this.videoName = videoName;
  }

  public boolean isHasDummy()
  {
    return hasDummy;
  }

  public void setHasDummy(boolean hasDummy)
  {
    this.hasDummy = hasDummy;
  }

  public boolean isHasAudio()
  {
    return hasAudio;
  }

  public void setHasAudio(boolean hasAudio)
  {
    this.hasAudio = hasAudio;
  }

  public String getAudioName()
  {
    return audioName;
  }

  public void setAudioName(String audioName)
  {
    this.audioName = audioName;
  }

  public boolean isHasImage()
  {
    return hasImage;
  }

  public void setHasImage(boolean hasImage)
  {
    this.hasImage = hasImage;
  }

  public String getImageName()
  {
    return imageName;
  }

  public void setImageName(String imageName)
  {
    this.imageName = imageName;
  }

  public boolean isHasTimer()
  {
    return hasTimer;
  }

  public void setHasTimer(boolean hasTimer)
  {
    this.hasTimer = hasTimer;
  }

  public int getTimeInSeconds()
  {
    return timeInSeconds;
  }

  public void setTimeInSeconds(int timeInSeconds)
  {
    this.timeInSeconds = timeInSeconds;
  }

  public boolean isHasParagraph()
  {
    return hasParagraph;
  }

  public void setHasParagraph(boolean hasParagraph)
  {
    this.hasParagraph = hasParagraph;
  }

  public String getParaWord()
  {
    return paraWord;
  }

  public void setParaWord(String paraWord)
  {
    this.paraWord = paraWord;
  }

  public String getQuestionType()
  {
    return questionType;
  }

  public void setQuestionType(String questionType)
  {
    this.questionType = questionType;
  }

  public int getNumberOfAnswers()
  {
    return numberOfAnswers;
  }

  public void setNumberOfAnswers(int numberOfAnswers)
  {
    this.numberOfAnswers = numberOfAnswers;
  }

  public String getAnswerAlignment()
  {
    return answerAlignment;
  }

  public void setAnswerAlignment(String answerAlignment)
  {
    this.answerAlignment = answerAlignment;
  }

  public String getGroupName()
  {
    return groupName;
  }

  public void setGroupName(String groupName)
  {
    this.groupName = groupName;
  }

  public List<Answers> getAnswers()
  {
    return answers;
  }

  public void setAnswers(List<Answers> answers)
  {
    this.answers = answers;
  }

  public boolean isActive()
  {
    return active;
  }

  public void setActive(boolean active)
  {
    this.active = active;
  }

  public String getHint()
  {
    return hint;
  }

  public void setHint(String hint)
  {
    this.hint = hint;
  }

  public boolean isHasValidation() {
    return hasValidation;
  }

  public void setHasValidation(boolean hasValidation) {
    this.hasValidation = hasValidation;
  }

  public int getMinLength() {
    return minLength;
  }

  public void setMinLength(int minLength) {
    this.minLength = minLength;
  }

  public int getMaxLength() {
    return maxLength;
  }

  public void setMaxLength(int maxLength) {
    this.maxLength = maxLength;
  }

  public boolean isSpecialCharAllowed() {
    return specialCharAllowed;
  }

  public void setSpecialCharAllowed(boolean specialCharAllowed) {
    this.specialCharAllowed = specialCharAllowed;
  }

  public boolean isAlphabetAllowed() {
    return alphabetAllowed;
  }

  public void setAlphabetAllowed(boolean alphabetAllowed) {
    this.alphabetAllowed = alphabetAllowed;
  }

  public boolean isNumericAllowed() {
    return numericAllowed;
  }

  public void setNumericAllowed(boolean numericAllowed) {
    this.numericAllowed = numericAllowed;
  }

  public boolean isIgnoreQuestion()
  {
    return ignoreQuestion;
  }

  public void setIgnoreQuestion(boolean ignoreQuestion)
  {
    this.ignoreQuestion = ignoreQuestion;
  }

  public boolean isHideOptionsUntilVideoEnded()
  {
    return hideOptionsUntilVideoEnded;
  }

  public void setHideOptionsUntilVideoEnded(boolean hideOptionsUntilVideoEnded)
  {
    this.hideOptionsUntilVideoEnded = hideOptionsUntilVideoEnded;
  }

  public void addTag(SubTag tag){
    if(subTags == null){
      subTags = new ArrayList<>();
    }
    subTags.add(tag);
  }
}