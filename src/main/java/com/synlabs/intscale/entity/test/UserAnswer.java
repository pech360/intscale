package com.synlabs.intscale.entity.test;

import com.synlabs.intscale.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class UserAnswer extends BaseEntity
{
  @ManyToOne
  private Question   question;
  private int        optionNumber;
  private boolean    hasText;
  private String     text;
  private String     description;
  private int        marks;
  private int        maxMarks;
  private int        minMarks;
  private boolean    hasImage;
  private boolean    hasAudio;
  private int        timeTaken;
  @ManyToOne
  private TestResult testResult;
  private Long       answerId;

  public Long getAnswerId()
  {
    return answerId;
  }

  public void setAnswerId(Long answerId)
  {
    this.answerId = answerId;
  }

  public TestResult getTestResult()
  {
    return testResult;
  }

  public void setTestResult(TestResult testResult)
  {
    this.testResult = testResult;
  }

  public boolean isHasText()
  {
    return hasText;
  }

  public void setHasText(boolean hasText)
  {
    this.hasText = hasText;
  }

  public int getMinMarks()
  {
    return minMarks;
  }

  public void setMinMarks(int minMarks)
  {
    this.minMarks = minMarks;
  }

  public Question getQuestion()
  {
    return question;
  }

  public void setQuestion(Question question)
  {
    this.question = question;
  }

  public int getTimeTaken()
  {
    return timeTaken;
  }

  public void setTimeTaken(int timeTaken)
  {
    this.timeTaken = timeTaken;
  }

  public int getOptionNumber()
  {
    return optionNumber;
  }

  public void setOptionNumber(int optionNumber)
  {
    this.optionNumber = optionNumber;
  }

  public String getText()
  {
    return text;
  }

  public void setText(String text)
  {
    this.text = text;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public int getMarks()
  {
    return marks;
  }

  public void setMarks(int marks)
  {
    this.marks = marks;
  }

  public boolean isHasImage()
  {
    return hasImage;
  }

  public void setHasImage(boolean hasImage)
  {
    this.hasImage = hasImage;
  }

  public boolean isHasAudio()
  {
    return hasAudio;
  }

  public void setHasAudio(boolean hasAudio)
  {
    this.hasAudio = hasAudio;
  }

  public int getMaxMarks()
  {
    return maxMarks;
  }

  public void setMaxMarks(int maxMarks)
  {
    this.maxMarks = maxMarks;
  }
}
