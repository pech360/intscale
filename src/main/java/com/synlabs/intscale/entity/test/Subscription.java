package com.synlabs.intscale.entity.test;

import com.synlabs.intscale.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.List;

/**
 * Created by vikas kumar on 29-05-2017.
 */
@Entity
public class Subscription extends BaseEntity
{
  private String userType;
  private int    keyUsesCount;
  private int usedKeysCount;
  private String language;
  private String studentType;
  private String gradeType;
  private String instituteName;
  private String testKey;
  @ManyToMany
  private List<Test> tests;

  public String getUserType()
  {
    return userType;
  }

  public void setUserType(String userType)
  {
    this.userType = userType;
  }

  public String getLanguage()
  {
    return language;
  }

  public void setLanguage(String language)
  {
    this.language = language;
  }

  public String getStudentType()
  {
    return studentType;
  }

  public void setStudentType(String studentType)
  {
    this.studentType = studentType;
  }

  public String getGradeType()
  {
    return gradeType;
  }

  public void setGradeType(String gradeType)
  {
    this.gradeType = gradeType;
  }

  public String getInstituteName()
  {
    return instituteName;
  }

  public void setInstituteName(String instituteName)
  {
    this.instituteName = instituteName;
  }

  public List<Test> getTests()
  {
    return tests;
  }

  public void setTests(List<Test> tests)
  {
    this.tests = tests;
  }

  public int getKeyUsesCount()
  {
    return keyUsesCount;
  }

  public void setKeyUsesCount(int keyUsesCount)
  {
    this.keyUsesCount = keyUsesCount;
  }

  public String getTestKey()
  {
    return testKey;
  }

  public void setTestKey(String testKey)
  {
    this.testKey = testKey;
  }

  public int getUsedKeysCount()
  {
    return usedKeysCount;
  }

  public void setUsedKeysCount(int usedKeysCount)
  {
    this.usedKeysCount = usedKeysCount;
  }
}
