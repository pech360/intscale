package com.synlabs.intscale.entity.test;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.enums.TagType;

import javax.jdo.annotations.Unique;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
public class QuestionTag extends BaseEntity
{
  private String name;
  @OneToMany(mappedBy = "tag")
  private Set<SubTag> subTags=new HashSet<>();

  @Enumerated(EnumType.STRING)
  private TagType type;

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public Set<SubTag> getSubTags()
  {
    return subTags;
  }

  public void setSubTags(Set<SubTag> subTags)
  {
    this.subTags = subTags;
  }

  public TagType getType()
  {
    return type;
  }

  public void setType(TagType type)
  {
    this.type = type;
  }
}
