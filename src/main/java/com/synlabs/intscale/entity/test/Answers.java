package com.synlabs.intscale.entity.test;


import com.synlabs.intscale.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by itrs on 7/31/2017.
 */
@Entity
public class Answers extends BaseEntity
{
  private String  description;
  private int     marks;
  private int     optionNumber;
  private boolean hasText;
  private boolean hasImage;
  private String  imageName;
  private boolean hasAudio;
  private String  audioName;
  private int startPoint;
  private int endPoint;
  private String sliderType;
  @ManyToOne
  private Question question;

  public Question getQuestion()
  {
    return question;
  }

  public void setQuestion(Question question)
  {
    this.question = question;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public int getMarks()
  {
    return marks;
  }

  public void setMarks(int marks)
  {
    this.marks = marks;
  }

  public boolean isHasText()
  {
    return hasText;
  }

  public void setHasText(boolean hasText)
  {
    this.hasText = hasText;
  }

  public boolean isHasImage()
  {
    return hasImage;
  }

  public void setHasImage(boolean hasImage)
  {
    this.hasImage = hasImage;
  }

  public String getImageName()
  {
    return imageName;
  }

  public void setImageName(String imageName)
  {
    this.imageName = imageName;
  }

  public boolean isHasAudio()
  {
    return hasAudio;
  }

  public void setHasAudio(boolean hasAudio)
  {
    this.hasAudio = hasAudio;
  }

  public String getAudioName()
  {
    return audioName;
  }

  public void setAudioName(String audioName)
  {
    this.audioName = audioName;
  }

  public int getOptionNumber()
  {
    return optionNumber;
  }

  public void setOptionNumber(int optionNumber)
  {
    this.optionNumber = optionNumber;
  }

  public int getStartPoint()
  {
    return startPoint;
  }

  public void setStartPoint(int startPoint)
  {
    this.startPoint = startPoint;
  }

  public int getEndPoint()
  {
    return endPoint;
  }

  public void setEndPoint(int endPoint)
  {
    this.endPoint = endPoint;
  }

  public String getSliderType()
  {
    return sliderType;
  }

  public void setSliderType(String sliderType)
  {
    this.sliderType = sliderType;
  }
}
