package com.synlabs.intscale.entity.test;

import com.synlabs.intscale.entity.BaseEntity;

import javax.persistence.Entity;

/**
 * Created by India on 5/9/2018.
 */
@Entity
public class TextquestionAnswers extends BaseEntity {
    private Long questionId;
    private Long testId;
    private String textAnswer;

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public Long getTestId() {
        return testId;
    }

    public void setTestId(Long testId) {
        this.testId = testId;
    }

    public String getTextAnswer() {
        return textAnswer;
    }

    public void setTextAnswer(String textAnswer) {
        this.textAnswer = textAnswer;
    }
}
