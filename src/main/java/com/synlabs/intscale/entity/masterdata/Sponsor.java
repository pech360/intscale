package com.synlabs.intscale.entity.masterdata;

import com.synlabs.intscale.entity.BaseEntity;

import javax.jdo.annotations.Unique;
import javax.persistence.Entity;

@Entity
public class Sponsor extends BaseEntity {
    @Unique
    private String name;
    private String description;
    private String logo;
    private String reportCoverFrontImage;
    private String reportCoverBackImage;
    private boolean active = true;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getReportCoverFrontImage() {
        return reportCoverFrontImage;
    }

    public void setReportCoverFrontImage(String reportCoverFrontImage) {
        this.reportCoverFrontImage = reportCoverFrontImage;
    }

    public String getReportCoverBackImage() {
        return reportCoverBackImage;
    }

    public void setReportCoverBackImage(String reportCoverBackImage) {
        this.reportCoverBackImage = reportCoverBackImage;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
