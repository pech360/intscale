package com.synlabs.intscale.entity.masterdata;

import com.synlabs.intscale.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class UserExperienceFeedback extends BaseEntity
{
  private String username;
  private Integer rating;
  @Column(length = 600)
  private String experienceDescription;

  public String getUsername()
  {
    return username;
  }

  public void setUsername(String username)
  {
    this.username = username;
  }

  public Integer getRating()
  {
    return rating;
  }

  public void setRating(Integer rating)
  {
    this.rating = rating;
  }

  public String getExperienceDescription()
  {
    return experienceDescription;
  }

  public void setExperienceDescription(String experienceDescription)
  {
    this.experienceDescription = experienceDescription;
  }
}
