package com.synlabs.intscale.entity.masterdata;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.entity.user.User;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity

public class LikedContent extends BaseEntity
{
  @OneToOne
  private User user;
  @OneToOne
  private Blog blog;

  private boolean isLiked;

  @Override
  public User getUser()
  {
    return user;
  }

  public void setUser(User user)
  {
    this.user = user;
  }

  public Blog getBlog()
  {
    return blog;
  }

  public void setBlog(Blog blog)
  {
    this.blog = blog;
  }

  public boolean isLiked()
  {
    return isLiked;
  }

  public void setLiked(boolean liked)
  {
    isLiked = liked;
  }
}
