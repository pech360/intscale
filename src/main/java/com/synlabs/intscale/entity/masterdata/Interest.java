package com.synlabs.intscale.entity.masterdata;

import com.synlabs.intscale.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Interest extends BaseEntity
{
  @Column(unique = true)
  private String   name;
  @ManyToOne
  private Interest parent;
  private String   image;

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public Interest getParent()
  {
    return parent;
  }

  public void setParent(Interest parent)
  {
    this.parent = parent;
  }

  public String getImage()
  {
    return image;
  }

  public void setImage(String image)
  {
    this.image = image;
  }
}
