package com.synlabs.intscale.entity.masterdata;

import com.synlabs.intscale.entity.BaseEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Blog extends BaseEntity
{
  @Column(length = 600)
  private String description;

  private String name;
  private String url;
  private Long   likes;
  private String author;
  private String title;
  private String logoUrl;
  private String imageUrl;
  private String videoUrl;

  @ManyToMany
  @JoinTable
      (
          name = "blog_interest",
          joinColumns = { @JoinColumn(name = "BLOG_ID") },
          inverseJoinColumns = { @JoinColumn(name = "INTEREST_ID") }
      )
  private List<Interest> interests = new ArrayList<>();

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getUrl()
  {
    return url;
  }

  public void setUrl(String url)
  {
    this.url = url;
  }

  public Long getLikes()
  {
    return likes;
  }

  public void setLikes(Long likes)
  {
    this.likes = likes;
  }

  public List<Interest> getInterests()
  {
    return interests;
  }

  public void setInterests(List<Interest> interests)
  {
    this.interests = interests;
  }

  public String getAuthor()
  {
    return author;
  }

  public void setAuthor(String author)
  {
    this.author = author;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public String getLogoUrl()
  {
    return logoUrl;
  }

  public void setLogoUrl(String logoUrl)
  {
    this.logoUrl = logoUrl;
  }

  public String getImageUrl()
  {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl)
  {
    this.imageUrl = imageUrl;
  }

  public String getVideoUrl()
  {
    return videoUrl;
  }

  public void setVideoUrl(String videoUrl)
  {
    this.videoUrl = videoUrl;
  }
}
