package com.synlabs.intscale.entity.masterdata;

import com.synlabs.intscale.entity.BaseEntity;

import javax.persistence.Entity;

/**
 * Created by itrs on 7/25/2017.
 */
@Entity
public class MasterDataDocument extends BaseEntity
{
  private String type;
  private String value;

  public String getType()
  {
    return type;
  }

  public void setType(String type)
  {
    this.type = type;
  }

  public String getValue()
  {
    return value;
  }

  public void setValue(String value)
  {
    this.value = value;
  }
}
