package com.synlabs.intscale.entity.masterdata;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.user.Teacher;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class GradeSectionSubject extends BaseEntity
{

  private Long   subject;
  private String subjectName;
  private String grade;
  private String section;
  @ManyToOne
  private Teacher teacher;

  public Long getSubject()
  {
    return subject;
  }

  public void setSubject(Long subject)
  {
    this.subject = subject;
  }

  public String getSubjectName()
  {
    return subjectName;
  }

  public void setSubjectName(String subjectName)
  {
    this.subjectName = subjectName;
  }

  public String getGrade()
  {
    return grade;
  }

  public void setGrade(String grade)
  {
    this.grade = grade;
  }

  public String getSection()
  {
    return section;
  }

  public void setSection(String section)
  {
    this.section = section;
  }

  public Teacher getTeacher()
  {
    return teacher;
  }

  public void setTeacher(Teacher teacher)
  {
    this.teacher = teacher;
  }
}
