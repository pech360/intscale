package com.synlabs.intscale.entity;

import com.synlabs.intscale.entity.test.Product;
import com.synlabs.intscale.entity.user.Tenant;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class ProductTenant extends BaseEntity {
    @ManyToOne
    private Tenant domain;
    @ManyToOne
    private Product product;

    public Tenant getDomain() {
        return domain;
    }

    public void setDomain(Tenant domain) {
        this.domain = domain;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
