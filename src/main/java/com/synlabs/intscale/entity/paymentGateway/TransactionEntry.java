package com.synlabs.intscale.entity.paymentGateway;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import com.synlabs.intscale.view.paymentGateway.TransactionEntryView;

import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;

@Entity
public class TransactionEntry {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id; 
	@Column(unique=true)
	private String payId;
	private String entity;
	private Long amount; //In Rs.
	private String currency;
	private String status;
	private String orderId;
	private String invoiceId;
	private Boolean international;
	private String method;
	private String amountRefunded;
	private String refundStatus;
	private Boolean captured;
	private String description;
	private String cardId;
	private String bank;
	private String wallet;
	private String vpa;
	private String email;
	private String contactNum;
	private String address;
	private Long userId;
	private String username;
	private Long fee;
	private Long tax;
	private String errorCode;
	private String errorDescription;
	private Date createdAt; //value give by payment gateway
	
	//Extra field
	private Date storedAt; //by coder
	private Date modifiedAt; //by coder
	
	//extra field for PromoCode
	//LNT
	private String promoCodeName;
	private String productName;
	private BigDecimal discount;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEntity() {
		return entity;
	}
	public void setEntity(String entity) {
		this.entity = entity;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getInternational() {
		return international;
	}
	public void setInternational(Boolean international) {
		this.international = international;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public Boolean getCaptured() {
		return captured;
	}
	public void setCaptured(Boolean captured) {
		this.captured = captured;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getWallet() {
		return wallet;
	}
	public void setWallet(String wallet) {
		this.wallet = wallet;
	}
	public String getVpa() {
		return vpa;
	}
	public void setVpa(String vpa) {
		this.vpa = vpa;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContactNum() {
		return contactNum;
	}
	public void setContactNum(String contactNum) {
		this.contactNum = contactNum;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Long getFee() {
		return fee;
	}
	public void setFee(Long fee) {
		this.fee = fee;
	}
	public Long getTax() {
		return tax;
	}
	public void setTax(Long tax) {
		this.tax = tax;
	}
	public String getPayId() {
		return payId;
	}
	public void setPayId(String payId) {
		this.payId = payId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}
	public String getAmountRefunded() {
		return amountRefunded;
	}
	public void setAmountRefunded(String amountRefunded) {
		this.amountRefunded = amountRefunded;
	}
	public String getRefundStatus() {
		return refundStatus;
	}
	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorDescription() {
		return errorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getStoredAt() {
		return storedAt;
	}
	public void setStoredAt(Date storedAt) {
		this.storedAt = storedAt;
	}
	public Date getModifiedAt() {
		return modifiedAt;
	}
	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}
	//LNT
	public String getPromoCodeName() {
		return promoCodeName;
	}
	public void setPromoCodeName(String promoCodeName) {
		this.promoCodeName = promoCodeName;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	@PrePersist
	private void preCreated() {
		this.setStoredAt(new Date());
	}
	@PreUpdate
	private void preUpdate() {
		this.setModifiedAt(new Date());
	}
	//LNT
	public TransactionEntryView toTransactionEntryView() {
		TransactionEntryView transactionEntryView = new TransactionEntryView();
		transactionEntryView.setPayId(this.payId);
		transactionEntryView.setAmount(this.amount);
		transactionEntryView.setCreatedAt(this.createdAt);
		transactionEntryView.setDescription(this.description);
		transactionEntryView.setEmail(this.email);
		transactionEntryView.setMethod(this.method);
		transactionEntryView.setPercentageDiscount(this.discount);
		transactionEntryView.setProduct(this.productName);
		transactionEntryView.setPromoCode(this.promoCodeName);
		transactionEntryView.setUsername(this.username);
		return transactionEntryView;
	}
}
