package com.synlabs.intscale.entity.counselling;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.enums.CounsellingSessionType;

import javax.persistence.*;

@Entity
public class CounsellingSessionNotes extends BaseEntity {
    @Enumerated(EnumType.STRING)
    private CounsellingSessionType counsellingSessionType;
    @Column(length = 600)
    private String statement;
    @Column(length = 1500)
    private String response;
    private boolean active = true;
    private boolean sample = false;
    private boolean timelinePost = false;

    @ManyToOne
    private CounsellingSession counsellingSession;

    public CounsellingSessionType getCounsellingSessionType() {
        return counsellingSessionType;
    }

    public void setCounsellingSessionType(CounsellingSessionType counsellingSessionType) {
        this.counsellingSessionType = counsellingSessionType;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isSample() {
        return sample;
    }

    public void setSample(boolean sample) {
        this.sample = sample;
    }

    public CounsellingSession getCounsellingSession() {
        return counsellingSession;
    }

    public void setCounsellingSession(CounsellingSession counsellingSession) {
        this.counsellingSession = counsellingSession;
    }

    public boolean isTimelinePost()
    {
        return timelinePost;
    }

    public void setTimelinePost(boolean timelinePost)
    {
        this.timelinePost = timelinePost;
    }
}
