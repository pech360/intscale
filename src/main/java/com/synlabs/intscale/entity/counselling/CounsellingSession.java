package com.synlabs.intscale.entity.counselling;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.entity.masterdata.MasterDataDocument;
import com.synlabs.intscale.entity.test.SubTag;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.enums.CounsellingSessionStatus;
import com.synlabs.intscale.enums.CounsellingSessionType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class CounsellingSession extends BaseEntity
{

  @ManyToOne
  private User counsellor;

  @ManyToOne
  private User user;

  private String                   name;
  private Long                     sequenceNumber;
  private Date scheduledOn;
  private String[] leads;

  @Enumerated(EnumType.STRING)
  private CounsellingSessionType counsellingSessionType;

  @Enumerated(EnumType.STRING)
  private CounsellingSessionStatus counsellingSessionStatus;

  @ManyToMany
  private List<SubTag> subTags =new ArrayList<>();

  @OneToMany(mappedBy = "counsellingSession")
  private List<CounsellingSessionNotes> counsellingSessionNotes = new ArrayList<>();

  public User getCounsellor()
  {
    return counsellor;
  }

  public void setCounsellor(User counsellor)
  {
    this.counsellor = counsellor;
  }

  @Override
  public User getUser()
  {
    return user;
  }

  public void setUser(User user)
  {
    this.user = user;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public Long getSequenceNumber()
  {
    return sequenceNumber;
  }

  public void setSequenceNumber(Long sequenceNumber)
  {
    this.sequenceNumber = sequenceNumber;
  }

  public Date getScheduledOn()
  {
    return scheduledOn;
  }

  public void setScheduledOn(Date scheduledOn)
  {
    this.scheduledOn = scheduledOn;
  }

  public String[] getLeads()
  {
    return leads;
  }

  public void setLeads(String[] leads)
  {
    this.leads = leads;
  }

  public CounsellingSessionType getCounsellingSessionType()
  {
    return counsellingSessionType;
  }

  public void setCounsellingSessionType(CounsellingSessionType counsellingSessionType)
  {
    this.counsellingSessionType = counsellingSessionType;
  }

  public CounsellingSessionStatus getCounsellingSessionStatus()
  {
    return counsellingSessionStatus;
  }

  public void setCounsellingSessionStatus(CounsellingSessionStatus counsellingSessionStatus)
  {
    this.counsellingSessionStatus = counsellingSessionStatus;
  }

  public List<SubTag> getSubTags()
  {
    return subTags;
  }

  public void setSubTags(List<SubTag> subTags)
  {
    this.subTags = subTags;
  }

  public List<CounsellingSessionNotes> getCounsellingSessionNotes()
  {
    return counsellingSessionNotes;
  }

  public void setCounsellingSessionNotes(List<CounsellingSessionNotes> counsellingSessionNotes)
  {
    this.counsellingSessionNotes = counsellingSessionNotes;
  }
}
