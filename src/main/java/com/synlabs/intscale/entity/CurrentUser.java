package com.synlabs.intscale.entity;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;

public class CurrentUser extends User
{

  private com.synlabs.intscale.entity.user.User user;

  public CurrentUser(com.synlabs.intscale.entity.user.User user, String[] roles)
  {
    super(user.getUsername(), user.getPasswordHash(), AuthorityUtils.createAuthorityList(roles));
    this.user = user;
  }

  public com.synlabs.intscale.entity.user.User getUser()
  {
    return user;
  }

  public void setUser(com.synlabs.intscale.entity.user.User user)
  {
    this.user = user;
  }

}
