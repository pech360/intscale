package com.synlabs.intscale.entity;

import javax.persistence.Entity;

/**
 * Created by India on 1/16/2018.
 */
@Entity
public class Pilot extends BaseEntity{
    private String name;
    private boolean active;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
