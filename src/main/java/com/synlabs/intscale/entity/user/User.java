package com.synlabs.intscale.entity.user;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.entity.Timeline;
import com.synlabs.intscale.entity.counselling.CounsellingSession;
import com.synlabs.intscale.entity.masterdata.Sponsor;
import com.synlabs.intscale.entity.test.TestResult;
import com.synlabs.intscale.entity.threesixty.Stakeholder;
import com.synlabs.intscale.enums.StakeholderRelation;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.jdo.annotations.Unique;
import javax.persistence.*;
import java.util.*;

@Entity
public class User extends BaseEntity {
    private String username;
    private String passwordHash;
    private String email;
    private String name;
    private String gender;
    private Date dob;
    private boolean active;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role", joinColumns = {@JoinColumn(name = "USER_ID")}, inverseJoinColumns = {
            @JoinColumn(name = "ROLE_ID")})
    private List<Role> roles = new ArrayList<>();

    private String language;
    private String userType;
    private String studentType;
    private String registerType;
    private String instituteName;
    private String grade;
    private String section;
    private String schoolName;
    private String designation;
    private String degree;
    private String stream;
    private String interests;
    private String industry;
    private String orgName;
    private String dashBoardType;
    private String displayPicture;
    private String higherEducation;
    // Edit Fields
    private Boolean IsOtpVerified = false;
    @Temporal(TemporalType.TIMESTAMP)
    private Date otpCreatedDate;
    private Boolean isPaid = false;
    private String flag = "New";
    @Column(unique = true)
    private String contactNumber;
    private String registeredName;
    private Boolean ftlCount;       //[first time login count]; //change
    //private Boolean contactValidate;
    @ManyToOne
    private User counsellor;

    @OneToMany(mappedBy = "counsellor")
    private List<User> users;

    @OneToMany(mappedBy = "user")
    private List<TestResult> tests;

    @OneToMany(mappedBy = "user")
    private List<Timeline> timeline = new ArrayList<>();

    private String academicScore;
    private String supw;

    private Boolean threeSixtyEnabled;
    private Boolean enableAimReport;

    private boolean allowMainAimReport = false;
    private boolean allowMiniAimReport = false;

    @OneToMany(mappedBy = "user")
    private List<Stakeholder> stakeholders = new ArrayList<>();

    @OneToOne(mappedBy = "stakeholderUser")
    private Stakeholder stakeholder;
    private String city;

    private Date counsellorAssignedOn;

    @OneToMany(mappedBy = "user")
    private List<CounsellingSession> userCounsellingSessions = new ArrayList<>();

    @OneToMany(mappedBy = "counsellor")
    private List<CounsellingSession> counsellorCounsellingSessions = new ArrayList<>();

    @OneToOne
    private Teacher teacher;

    @OneToMany(mappedBy = "user")
    private List<AcademicPerformance> academicPerformances = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_sponsor", joinColumns = {@JoinColumn(name = "user_id")}, inverseJoinColumns = {
            @JoinColumn(name = "sponsor_id")})
    private List<Sponsor> sponsors = new ArrayList<>();

    public Boolean getThreeSixtyEnabled() {
        return threeSixtyEnabled;
    }

    public void setThreeSixtyEnabled(Boolean threeSixtyEnabled) {
        this.threeSixtyEnabled = threeSixtyEnabled;
    }

    public Stakeholder getStakeholder() {
        return stakeholder;
    }

    public void setStakeholder(Stakeholder stakeholder) {
        this.stakeholder = stakeholder;
    }

    public List<Stakeholder> getStakeholders() {
        return stakeholders;
    }

    public void setStakeholders(List<Stakeholder> stakeholders) {
        this.stakeholders = stakeholders;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getStudentType() {
        return studentType;
    }

    public void setStudentType(String studentType) {
        this.studentType = studentType;
    }

    public String getRegisterType() {
        return registerType;
    }

    public void setRegisterType(String registerType) {
        this.registerType = registerType;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public String getInterests() {
        return interests;
    }

    public void setInterests(String interests) {
        this.interests = interests;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getDashBoardType() {
        return dashBoardType;
    }

    public void setDashBoardType(String dashBoardType) {
        this.dashBoardType = dashBoardType;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getDisplayPicture() {
        return displayPicture;
    }

    public void setDisplayPicture(String displayPicture) {
        this.displayPicture = displayPicture;
    }

    public Boolean getIsOtpVerified() {
        return IsOtpVerified;
    }

    public void setIsOtpVerified(Boolean isOtpVerified) {
        IsOtpVerified = isOtpVerified;
    }

    public Date getOtpCreatedDate() {
        return otpCreatedDate;
    }

    public void setOtpCreatedDate(Date otpCreatedDate) {
        this.otpCreatedDate = otpCreatedDate;
    }

    public Boolean getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(Boolean isPaid) {
        this.isPaid = isPaid;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public List<TestResult> getTests() {
        return tests;
    }

    public void setTests(List<TestResult> tests) {
        this.tests = tests;
    }

    public List<Timeline> getTimeline() {
        return timeline;
    }

    public void setTimeline(List<Timeline> timeline) {
        this.timeline = timeline;
    }

    public String getAcademicScore() {
        return academicScore;
    }

    public void setAcademicScore(String academicScore) {
        this.academicScore = academicScore;
    }

    public String getSupw() {
        return supw;
    }

    public void setSupw(String supw) {
        this.supw = supw;
    }

    public User getCounsellor() {
        return counsellor;
    }

    public void setCounsellor(User counsellor) {
        this.counsellor = counsellor;
    }

    public Boolean getEnableAimReport() {
        return enableAimReport;
    }

    public void setEnableAimReport(Boolean enableAimReport) {
        this.enableAimReport = enableAimReport;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Date getCounsellorAssignedOn() {
        return counsellorAssignedOn;
    }

    public void setCounsellorAssignedOn(Date counsellorAssignedOn) {
        this.counsellorAssignedOn = counsellorAssignedOn;
    }

    public List<CounsellingSession> getUserCounsellingSessions() {
        return userCounsellingSessions;
    }

    public void setUserCounsellingSessions(List<CounsellingSession> userCounsellingSessions) {
        this.userCounsellingSessions = userCounsellingSessions;
    }

    public List<CounsellingSession> getCounsellorCounsellingSessions() {
        return counsellorCounsellingSessions;
    }

    public void setCounsellorCounsellingSessions(List<CounsellingSession> counsellorCounsellingSessions) {
        this.counsellorCounsellingSessions = counsellorCounsellingSessions;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public List<AcademicPerformance> getAcademicPerformances() {
        return academicPerformances;
    }

    public void setAcademicPerformances(List<AcademicPerformance> academicPerformances) {
        this.academicPerformances = academicPerformances;
    }

    public String getHigherEducation() {
        return higherEducation;
    }

    public void setHigherEducation(String higherEducation) {
        this.higherEducation = higherEducation;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getRegisteredName() {
        return registeredName;
    }

    public void setRegisteredName(String registeredName) {
        this.registeredName = registeredName;
    }

    public Boolean getFtlCount() {
        return ftlCount;
    }

    public void setFtlCount(Boolean ftlCount) {
        this.ftlCount = ftlCount;
    }

    public boolean isAllowMainAimReport() {
        return allowMainAimReport;
    }

    public void setAllowMainAimReport(boolean allowMainAimReport) {
        this.allowMainAimReport = allowMainAimReport;
    }

    public boolean isAllowMiniAimReport() {
        return allowMiniAimReport;
    }

    public void setAllowMiniAimReport(boolean allowMiniAimReport) {
        this.allowMiniAimReport = allowMiniAimReport;
    }

    //	public Boolean getContactValidate() {
//		return contactValidate;
//	}
//
//	public void setContactValidate(Boolean contactValidate) {
//		this.contactValidate = contactValidate;
//	}


    public List<Sponsor> getSponsors() {
        return sponsors;
    }

    public void setSponsors(List<Sponsor> sponsors) {
        this.sponsors = sponsors;
    }

    public Set<String> getPrivileges() {
        Set<String> usrPrivileges = new HashSet<>();
        for (Role userRole : roles) {
            for (Privilege privilege : userRole.getPrivileges()) {
                usrPrivileges.add(privilege.getName());
            }
        }
        return usrPrivileges;
    }

    public boolean hasPrivilege(String priv) {

        if (StringUtils.isEmpty(priv)) {
            return true;
        }

        for (Role userRole : roles) {
            for (Privilege privilege : userRole.getPrivileges()) {
                if (privilege.getName().equals(priv)) {
                    return true;
                }
            }
        }

        return false;
    }

    public void addRole(Role role) {
        if (roles == null) {
            roles = new ArrayList<>();
        }
        roles.add(role);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        BaseEntity other = (BaseEntity) obj;
        if (getId() == null) {
            return false;
        } else if (!getId().equals(other.getId())) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        return result;
    }

    @Transient
    public Stakeholder getStakeholder(StakeholderRelation relation) {
        if (!CollectionUtils.isEmpty(stakeholders)) {
            for (Stakeholder stk : stakeholders) {
                if (stk.getRelation() == relation) {
                    return stk;
                }
            }
        }
        return null;
    }

//  @Transient
//  public boolean hasStakeholder(StakeholderRelation relation)
//  {
//    if (!CollectionUtils.isEmpty(stakeholders))
//    {
//      for (Stakeholder stk : stakeholders)
//      {
//        if (stk.getRelation() == relation)
//        {
//          return true;
//        }
//      }
//    }
//    return false;
//  }

}