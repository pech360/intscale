package com.synlabs.intscale.entity.user;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.entity.masterdata.MasterDataDocument;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.util.Date;

/**
 * Created by India on 1/3/2018.
 */
@Entity
public class Student extends BaseEntity
{
  private String             name;
  private String             username;//By Default autoFill and username=password
  private String             email;
  private String             gender;
  private Date               dob;
  private String section;
  @ManyToOne
  private School             school;
  private String             grade;
  @OneToOne
  private User               user;
  @ManyToOne
  private User               counsellor;

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getUsername()
  {
    return username;
  }

  public void setUsername(String username)
  {
    this.username = username;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public String getGender()
  {
    return gender;
  }

  public void setGender(String gender)
  {
    this.gender = gender;
  }

  public String getSection()
  {
    return section;
  }

  public School getSchool()
  {
    return school;
  }

  public void setSchool(School school)
  {
    this.school = school;
  }

  public String getGrade()
  {
    return grade;
  }

  public void setGrade(String grade)
  {
    this.grade = grade;
  }

  public Date getDob()
  {
    return dob;
  }

  public void setDob(Date dob)
  {
    this.dob = dob;
  }

  @Override
  public User getUser()
  {
    return user;
  }

  public void setUser(User user)
  {
    this.user = user;
  }

  public User getCounsellor()
  {
    return counsellor;
  }

  public void setCounsellor(User counsellor)
  {
    this.counsellor = counsellor;
  }

  public void setSection(String section)
  {
    this.section = section;
  }
}
