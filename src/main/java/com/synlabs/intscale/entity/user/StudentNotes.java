package com.synlabs.intscale.entity.user;

import com.synlabs.intscale.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class StudentNotes extends BaseEntity {
    @ManyToOne
    private User counsellor;
    @ManyToOne
    private User student;
    @Column(length = 2000)
    private String notes;
    @Column(length = 2000)
    private String selfNotes;
    private boolean pushStatus;
    private int pushCount;
    private String sessionName;

    public User getCounsellor() {
        return counsellor;
    }

    public void setCounsellor(User counsellor) {
        this.counsellor = counsellor;
    }

    public User getStudent() {
        return student;
    }

    public void setStudent(User student) {
        this.student = student;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getSelfNotes() {
        return selfNotes;
    }

    public void setSelfNotes(String selfNotes) {
        this.selfNotes = selfNotes;
    }

    public boolean isPushStatus() {
        return pushStatus;
    }

    public void setPushStatus(boolean pushStatus) {
        this.pushStatus = pushStatus;
    }

    public int getPushCount() {
        return pushCount;
    }

    public void setPushCount(int pushCount) {
        this.pushCount = pushCount;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }
}
