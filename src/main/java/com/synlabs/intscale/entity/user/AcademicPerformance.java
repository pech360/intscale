package com.synlabs.intscale.entity.user;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.enums.AcademicPerformanceName;
import com.synlabs.intscale.enums.AcademicPerformanceType;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
public class AcademicPerformance extends BaseEntity
{
  @ManyToOne
  User user;

  @Enumerated(EnumType.STRING)
  private AcademicPerformanceType type;

  @Enumerated(EnumType.STRING)
  private AcademicPerformanceName name;

  @Column(precision = 16, scale = 5)
  private BigInteger value;

  @Override
  public User getUser()
  {
    return user;
  }

  public void setUser(User user)
  {
    this.user = user;
  }

  public AcademicPerformanceType getType()
  {
    return type;
  }

  public void setType(AcademicPerformanceType type)
  {
    this.type = type;
  }

  public AcademicPerformanceName getName()
  {
    return name;
  }

  public void setName(AcademicPerformanceName name)
  {
    this.name = name;
  }

  public BigInteger getValue()
  {
    return value;
  }

  public void setValue(BigInteger value)
  {
    this.value = value;
  }
}
