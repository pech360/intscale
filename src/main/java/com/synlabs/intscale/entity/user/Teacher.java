package com.synlabs.intscale.entity.user;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.entity.masterdata.GradeSectionSubject;
import com.synlabs.intscale.entity.masterdata.MasterDataDocument;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by India on 1/3/2018.
 */
@Entity
public class Teacher extends BaseEntity
{
  private String name;
  private String email;
  private String gender;
  @ManyToOne
  private School school;
  private String education;
  private String speciality;
  private boolean classTeacher   = false;
  private boolean subjectTeacher = false;
  private boolean principal      = false;
  private boolean hod            = false;
  @OneToOne
  private User   user;
  private String grade;
  private String section;
  private Long   subject;
  private String subjectName;
  @OneToMany(mappedBy = "teacher")
  private List<GradeSectionSubject> gradeSectionSubjects = new ArrayList<>();

  @Override
  public User getUser()
  {
    return user;
  }

  public void setUser(User user)
  {
    this.user = user;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public String getGender()
  {
    return gender;
  }

  public void setGender(String gender)
  {
    this.gender = gender;
  }

  public School getSchool()
  {
    return school;
  }

  public void setSchool(School school)
  {
    this.school = school;
  }

  public String getEducation()
  {
    return education;
  }

  public void setEducation(String education)
  {
    this.education = education;
  }

  public String getSpeciality()
  {
    return speciality;
  }

  public void setSpeciality(String speciality)
  {
    this.speciality = speciality;
  }

  public boolean isClassTeacher()
  {
    return classTeacher;
  }

  public void setClassTeacher(boolean classTeacher)
  {
    this.classTeacher = classTeacher;
  }

  public boolean isSubjectTeacher()
  {
    return subjectTeacher;
  }

  public void setSubjectTeacher(boolean subjectTeacher)
  {
    this.subjectTeacher = subjectTeacher;
  }

  public String getGrade()
  {
    return grade;
  }

  public void setGrade(String grade)
  {
    this.grade = grade;
  }

  public String getSection()
  {
    return section;
  }

  public void setSection(String section)
  {
    this.section = section;
  }

  public Long getSubject()
  {
    return subject;
  }

  public void setSubject(Long subject)
  {
    this.subject = subject;
  }

  public List<GradeSectionSubject> getGradeSectionSubjects()
  {
    return gradeSectionSubjects;
  }

  public void setGradeSectionSubjects(List<GradeSectionSubject> gradeSectionSubjects)
  {
    this.gradeSectionSubjects = gradeSectionSubjects;
  }

  public boolean isPrincipal()
  {
    return principal;
  }

  public void setPrincipal(boolean principal)
  {
    this.principal = principal;
  }

  public String getSubjectName()
  {
    return subjectName;
  }

  public void setSubjectName(String subjectName)
  {
    this.subjectName = subjectName;
  }

  public boolean isHod()
  {
    return hod;
  }

  public void setHod(boolean hod)
  {
    this.hod = hod;
  }
}
