package com.synlabs.intscale.entity.user;

import com.synlabs.intscale.entity.BaseEntity;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by vikas kumar on 29-05-2017.
 */
@Entity
public class Role extends BaseEntity
{
  private String name;

  @Column(length = 500)
  private String dashboard;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
      name = "role_privilege",
      joinColumns = { @JoinColumn(name = "ROLE_ID") },
      inverseJoinColumns = { @JoinColumn(name = "PRIV_ID") })
  private Set<Privilege> privileges;

  private int rank;

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  } 

  public void addPrivilege(Privilege privilege)
  {
    if (privileges == null)
    {
      privileges = new HashSet<>();
    }
    privileges.add(privilege);
  }

  public String getDashboard()
  {
    return dashboard;
  }

  public void setDashboard(String dashboard)
  {
    this.dashboard = dashboard;
  }

  public Set<Privilege> getPrivileges()
  {
    return privileges;
  }

  public void setPrivileges(Set<Privilege> privileges)
  {
    this.privileges = privileges;
  }

  public int getRank() {
    return rank;
  }

  public void setRank(int rank) {
    this.rank = rank;
  }
}
