package com.synlabs.intscale.entity.user;

import com.synlabs.intscale.entity.BaseEntity;
import com.synlabs.intscale.service.BaseService;

import javax.persistence.Entity;

/**
 * Created by India on 1/3/2018.
 */
@Entity
public class Session extends BaseEntity{
    private String session;
    private boolean active;

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
