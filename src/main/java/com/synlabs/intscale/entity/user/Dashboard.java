package com.synlabs.intscale.entity.user;

import com.synlabs.intscale.entity.BaseEntity;

import javax.persistence.Entity;

@Entity
public class Dashboard extends BaseEntity
{
  private String name;
  private String description;

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public Dashboard(Dashboard d)
  {
    this.name = d.name;
    this.description = d.description;
  }

  public Dashboard(){

  }
}
