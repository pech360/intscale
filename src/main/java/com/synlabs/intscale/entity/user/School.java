package com.synlabs.intscale.entity.user;

import com.synlabs.intscale.entity.BaseEntity;

import javax.persistence.Entity;

@Entity
public class School extends BaseEntity
{
  private String name;
  private String address;
  private String email;
  private String mobile;
  private String logo;
  private boolean isFieldEnable;
  private boolean itselfTenant;

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getAddress()
  {
    return address;
  }

  public void setAddress(String address)
  {
    this.address = address;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public String getMobile()
  {
    return mobile;
  }

  public void setMobile(String mobile)
  {
    this.mobile = mobile;
  }

  public String getLogo()
  {
    return logo;
  }

  public void setLogo(String logo)
  {
    this.logo = logo;
  }

  public boolean isFieldEnable() {
    return isFieldEnable;
  }

  public void setFieldEnable(boolean fieldEnable) {
    isFieldEnable = fieldEnable;
  }

  public boolean isItselfTenant()
  {
    return itselfTenant;
  }

  public void setItselfTenant(boolean itselfTenant)
  {
    this.itselfTenant = itselfTenant;
  }
}
