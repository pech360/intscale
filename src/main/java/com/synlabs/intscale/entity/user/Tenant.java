package com.synlabs.intscale.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by vikas kumar on 29-05-2017.
 */
@Entity
public class Tenant
{
  @Id
  @GeneratedValue
  private Long id;
  private String name;
  private String description;
  @Column(unique = true)
  private String subDomain;
  private String address;
  private boolean active;
  private int schoolLimit;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getSubDomain() {
    return subDomain;
  }

  public void setSubDomain(String subDomain) {
    this.subDomain = subDomain;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  public int getSchoolLimit() {
    return schoolLimit;
  }

  public void setSchoolLimit(int schoolLimit) {
    this.schoolLimit = schoolLimit;
  }
}
