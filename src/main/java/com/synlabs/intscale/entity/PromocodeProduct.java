package com.synlabs.intscale.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

import com.synlabs.intscale.util.CompositePromoCodeProductKey;
//LNT
@Entity
public class PromocodeProduct  {
	@EmbeddedId
	private CompositePromoCodeProductKey key;
	@MapsId(value = "promoCodeId")
	@ManyToOne
	private PromoCode promoCode;

	public CompositePromoCodeProductKey getKey() {
		return key;
	}
	public void setKey(CompositePromoCodeProductKey key) {
		this.key = key;
	}
	public PromoCode getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(PromoCode promoCode) {
		this.promoCode = promoCode;
	}
	public PromocodeProduct(CompositePromoCodeProductKey key, PromoCode promoCode) {
		this.key = key;
		this.promoCode = promoCode;
	}
	public PromocodeProduct() {

	}

}
