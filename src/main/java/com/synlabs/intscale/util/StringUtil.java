package com.synlabs.intscale.util;

import com.synlabs.intscale.controller.ProductController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

/**
 * Created by sushil on 12-05-2017.
 */
public class StringUtil
{

  private static Logger logger= LoggerFactory.getLogger(StringUtil.class);

  public static boolean in(String verb, String... list)
  {
    for (String match : list)
    {
      if (verb.equalsIgnoreCase(match))
      {
        return true;
      }
    }
    return false;
  }

  public static String capitalizeWord(String str)
  {
    if (StringUtils.isEmpty(str))
    {
      return null;
    }

    String first;
    str = str.toLowerCase();
    try{
      String words[] = str.split("\\s");
      StringBuilder capitalizeWord = new StringBuilder();
      for (String word : words)
      {
        if(word.length()>0){
          first = word.substring(0, 1);
          String afterfirst = word.substring(1);
          capitalizeWord.append(first.toUpperCase()).append(afterfirst).append(" ");
        }
      }
      return capitalizeWord.toString().trim();
    }catch (StringIndexOutOfBoundsException e){
      logger.error("Error in Capitalizing "+ str + " default result returned", e);
      return null;
    }
  }
}
