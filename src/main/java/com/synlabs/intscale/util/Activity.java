package com.synlabs.intscale.util;
//LNT
import java.util.HashMap;
import java.util.Map;

public enum Activity {
SignUp("Sign Up"),TookDemo("Took Demo"),PurchasedBlueprint("Purchased Blueprint") ,PurchasedExplore("Purchased Explore"), CompletedAssessment("Completed Assessment");
	
	public static Map<String,Activity> lookup = new HashMap<String,Activity>();
	private String value;
	static {
		for(Activity product : Activity.values()) {
			lookup.put(product.getValue(), product);
		}
	}
	private Activity(String value) {
		this.value= value;
	}

	public String getValue() {
		return value;
	}
}

