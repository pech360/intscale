package com.synlabs.intscale.util;

import java.io.Serializable;

import javax.persistence.Embeddable;
//LNT
@Embeddable
public class CompositePromoCodeProductKey implements Serializable{
	private Long promoCodeId;
	private ProductName productname;

	public Long getPromoCodeId() {
		return promoCodeId;
	}
	public void setPromoCodeId(Long promoCodeId) {
		this.promoCodeId = promoCodeId;
	}
	public ProductName getProductname() {
		return productname;
	}
	public void setProductname(ProductName productname) {
		this.productname = productname;
	}
	public CompositePromoCodeProductKey(Long promoCodeId, ProductName productname) {
		this.promoCodeId = promoCodeId;
		this.productname = productname;
	}
	public CompositePromoCodeProductKey() {

	}

}
