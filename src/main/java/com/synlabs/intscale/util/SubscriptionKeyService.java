package com.synlabs.intscale.util;

import com.synlabs.intscale.entity.test.Subscription;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.SubscribersRepository;
import com.synlabs.intscale.jpa.SubscriptionRepository;
import com.synlabs.intscale.service.BaseService;
import com.synlabs.intscale.view.SubscriptionRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.UUID;

/**
 * Created by itrs on 6/20/2017.
 */
@Service
public class SubscriptionKeyService extends BaseService
{
  @Autowired
  private SubscriptionRepository subscriptionRepository;

  @Autowired
  private SubscribersRepository subscribersRepository;

  public Subscription create(SubscriptionRequest subscriptionRequest)
  {

    if (StringUtils.isEmpty(subscriptionRequest.getLanguage()))
    {
      throw new ValidationException("Missing firstname");
    }
    Subscription subscription=subscriptionRequest.toEntity();
    UUID uniqueKey = UUID.randomUUID();
    String uuidKey=uniqueKey.toString();
    String key=(uuidKey.replaceAll("[^-?a-z]+", "").replaceAll("-","").substring(0,5)+Integer.valueOf(uuidKey.replaceAll("[^-?0-9]+", "").replaceAll("-","").substring(5,14)));
    StringBuilder str = new StringBuilder(key);
    int idx = str.length() - 4;
    while (idx > 0)
    {
      str.insert(idx, "-");
      idx = idx - 4;
    }
    String finalKey=str.toString();
    subscription.setTestKey(finalKey);
    subscription=subscriptionRepository.save(subscription);
    return subscription;
  }
}
