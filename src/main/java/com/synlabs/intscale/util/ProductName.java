package com.synlabs.intscale.util;

import java.util.HashMap;
import java.util.Map;
//LNT
public enum ProductName {

	BLUEPRINT("Blueprint") , EXPLORE("Explore");
	
	public static Map<String,ProductName> lookup = new HashMap<String,ProductName>();
	
	static {
		for(ProductName product : ProductName.values()) {
			lookup.put(product.getValue(), product);
		}
	}
	private String value;
	
	private ProductName(String value) {
		this.value= value;
	}

	public String getValue() {
		return value;
	}

}
