package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.Pilot;
import org.apache.commons.lang3.time.DateUtils;

import java.text.SimpleDateFormat;

/**
 * Created by India on 1/16/2018.
 */
public class PilotResponse implements Response {
    private Long id;
    private String name;
    private boolean active;
    private String status;
    private String startDate;
    private String endDate;
    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

    public PilotResponse(Pilot pilot){
        this.id=pilot.getId();
        this.name=pilot.getName();
        this.active=pilot.isActive();
        this.startDate=dateFormat.format(DateUtils.addMinutes(pilot.getCreatedDate().toDate(), 330));
        if(pilot.isActive()){
           this.endDate="Till Now";
           this.status="Active";
        }else {
           this.endDate=dateFormat.format(DateUtils.addMinutes(pilot.getLastModifiedDate().toDate(), 330));
           this.status="Deactivated";
        }
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isActive() {
        return active;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getStatus() {
        return status;
    }
}
