package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.StudentNotes;
import org.apache.commons.lang3.time.DateUtils;

import java.text.SimpleDateFormat;

public class StudentNotesResponse implements Response {
    private Long id;
    private String notes;
    private String selfNotes;
    private boolean pushStatus;
    private String date;
    private String sessionName;
    public StudentNotesResponse(StudentNotes notes){
        this.id=notes.getId();
        this.notes=notes.getNotes();
        this.selfNotes=notes.getSelfNotes();
        this.pushStatus=notes.isPushStatus();
        this.sessionName=notes.getSessionName();
        this.date=new SimpleDateFormat("MM/dd/yyyy h:mm a").format(DateUtils.addMinutes(notes.getCreatedDate().toDate(), 330));
    }

    public Long getId() {
        return id;
    }

    public String getNotes() {
        return notes;
    }

    public String getSelfNotes() {
        return selfNotes;
    }

    public boolean isPushStatus() {
        return pushStatus;
    }

    public String getDate() {
        return date;
    }

    public String getSessionName() {
        return sessionName;
    }
}
