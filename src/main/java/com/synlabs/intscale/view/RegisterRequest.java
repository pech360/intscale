package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.util.StringUtil;

public class RegisterRequest {

	private String firstname;
//  private String username;
//  private String password;
//  private String repassword;
	private String mobileNum;
	private String email;

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

//  public String getUsername()
//  {
//    return username;
//  }
//
//  public void setUsername(String username)
//  {
//    this.username = username;
//  }
//
//  public String getPassword()
//  {
//    return password;
//  }
//
//  public void setPassword(String password)
//  {
//    this.password = password;
//  }
//
//  public String getRepassword()
//  {
//    return repassword;
//  }
//
//  public void setRepassword(String repassword)
//  {
//    this.repassword = repassword;
//  }

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNum() {
		return mobileNum;
	}

	public void setMobileNum(String mobileNum) {
		this.mobileNum = mobileNum;
	}

	public User toEntity() {
		User user = new User();
		user.setName(StringUtil.capitalizeWord(this.getFirstname()));
		user.setRegisteredName(StringUtil.capitalizeWord(this.getFirstname()));
		user.setEmail(this.getEmail());
		//user.setMobileNum(this.getMobileNum());
		//user.setUsername(this.getMobileNum());
		if(user.getGrade()==null)
		user.setGrade("8"); //tests are showing on the bases of Grade.
		//user.setLanguage("English");
		//user.setUsername(this.getUsername());
		user.setActive(false);
		return user;
	}
}
