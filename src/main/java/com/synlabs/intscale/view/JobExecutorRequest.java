package com.synlabs.intscale.view;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;


@Getter
@Setter
public class JobExecutorRequest implements Request {
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date date;
}
