package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.test.QuestionTag;
import com.synlabs.intscale.util.LongObfuscator;
import com.synlabs.intscale.view.question.QuestionTagRequest;

public interface Request
{
  String dateFormat = "yyyy-MM-dd";

  default Long unmask(final Long number)
  {
    return number != null ? LongObfuscator.INSTANCE.unobfuscate(number) : null;
  }

}