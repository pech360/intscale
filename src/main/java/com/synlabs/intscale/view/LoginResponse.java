
package com.synlabs.intscale.view;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoginResponse
{

  private String token;
  //private Integer ftloginFlag;//change

  public LoginResponse(String token)
  {
    this.token = token;
  }
  //change
//  public LoginResponse(String token,Integer ftloginFlag) {
//	  this.token=token;
//	  this.ftloginFlag=ftloginFlag;
//  }

  public String getToken()
  {
    return token;
  }

//  public Integer getFtloginFlag() { //change
//	return ftloginFlag;
//  }
//
//  public void setFtloginFlag(Integer ftloginFlag) { //change
//	this.ftloginFlag = ftloginFlag;
//  }
  
  

}
