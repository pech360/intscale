package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.test.Test;
import org.apache.commons.lang3.time.DateUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vikas kumar on 19-05-2017.
 */
public class TestDescriptionResponse
{

  private Long    id;
  private String  logo;
  private String  introduction;
  private String  participation;
  private String  disclaimer;
  private String  procedure;
  private String  description;
  private String  scoreCategory;
  private String  scoreSubCategory;
  private Long version;
  private boolean latestVersion;
  private String  name;
  private boolean paid;
  private boolean published;
  private int sequence;
  private boolean testForce;
  private boolean goLive;
  private String date;
  private String scoringFormula;
  private String reportType;
  public TestDescriptionResponse(Test test)
  {
    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    if(test!=null){
    this.id = test.getId();
    this.logo = test.getLogo();
    this.introduction = test.getIntroduction();
    this.participation = test.getParticipation();
    this.disclaimer = test.getDisclaimer();
    this.procedure = test.getProcedure();
    this.description = test.getDescription();
    this.scoreCategory = test.getScoreCategory();
    this.scoreSubCategory = test.getScoreSubCategory();
    this.name = test.getName();
    this.paid = test.isPaid();
    this.version=test.getVersion();
    this.published = test.isPublished();
    this.latestVersion=test.isLatestVersion();
    this.sequence=test.getSequence();
    this.testForce=test.isTestForce();
    this.goLive=test.isGoLive();
    this.date=dateFormat.format(DateUtils.addMinutes(test.getCreatedDate().toDate(), 330));
    this.reportType=test.getReportType();
    this.scoringFormula = test.getScoringFormula();
    }
  }

  public Long getId()
  {
    return id;
  }

  public String getLogo()
  {
    return logo;
  }

  public String getIntroduction()
  {
    return introduction;
  }

  public String getParticipation()
  {
    return participation;
  }

  public String getDisclaimer()
  {
    return disclaimer;
  }

  public String getProcedure()
  {
    return procedure;
  }

  public String getDescription()
  {
    return description;
  }

  public String getScoreCategory()
  {
    return scoreCategory;
  }

  public String getScoreSubCategory()
  {
    return scoreSubCategory;
  }

  public String getName()
  {
    return name;
  }

  public boolean isPaid()
  {
    return paid;
  }

  public boolean isPublished()
  {
    return published;
  }

  public Long getVersion() {
    return version;
  }

  public boolean isLatestVersion() {
    return latestVersion;
  }

  public int getSequence() {
    return sequence;
  }

  public boolean isTestForce() {
    return testForce;
  }

  public boolean isGoLive() {
    return goLive;
  }

  public String getDate() {
    return date;
  }

  public String getReportType() {
    return reportType;
  }

  public String getScoringFormula()
  {
    return scoringFormula;
  }
}
