package com.synlabs.intscale.view.paymentGateway;

public class TResponse {

	private String code;
	private String description;
	
	public TResponse(String code, String description) {
		this.code = code;
		this.description = description;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
