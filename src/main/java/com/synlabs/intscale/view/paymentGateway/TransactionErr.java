package com.synlabs.intscale.view.paymentGateway;

public class TransactionErr {

	private TResponse error;

	public TResponse getError() {
		return error;
	}

	public void setError(TResponse error) {
		this.error = error;
	}
}
