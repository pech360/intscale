package com.synlabs.intscale.view.paymentGateway;

import java.util.Date;

import com.synlabs.intscale.entity.paymentGateway.TransactionEntry;

public class TransactionResponse {
	private String id;
	private String entity;
	private Long amount; // In Rs.
	private String currency;
	private String status;
	private String order_id;
	private String invoice_id;
	private Boolean international;
	private String method;
	private String amount_refunded;
	private String refund_status;
	private Boolean captured;
	private String description;
	private String card_id;
	private String bank;
	private String wallet;
	private String vpa;
	private String email;
	private String contactNum;
	private Notes notes; // Object of Notes
	private Long fee;
	private Long tax;
	private String error_code;
	private String error_description;
	private Long created_at;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public String getInvoice_id() {
		return invoice_id;
	}

	public void setInvoice_id(String invoice_id) {
		this.invoice_id = invoice_id;
	}

	public Boolean getInternational() {
		return international;
	}

	public void setInternational(Boolean international) {
		this.international = international;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getAmount_refunded() {
		return amount_refunded;
	}

	public void setAmount_refunded(String amount_refunded) {
		this.amount_refunded = amount_refunded;
	}

	public String getRefund_status() {
		return refund_status;
	}

	public void setRefund_status(String refund_status) {
		this.refund_status = refund_status;
	}

	public Boolean getCaptured() {
		return captured;
	}

	public void setCaptured(Boolean captured) {
		this.captured = captured;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCard_id() {
		return card_id;
	}

	public void setCard_id(String card_id) {
		this.card_id = card_id;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getWallet() {
		return wallet;
	}

	public void setWallet(String wallet) {
		this.wallet = wallet;
	}

	public String getVpa() {
		return vpa;
	}

	public void setVpa(String vpa) {
		this.vpa = vpa;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactNum() {
		return contactNum;
	}

	public void setContactNum(String contactNum) {
		this.contactNum = contactNum;
	}

	public Notes getNotes() {
		return notes;
	}

	public void setNotes(Notes notes) {
		this.notes = notes;
	}

	public Long getFee() {
		return fee;
	}

	public void setFee(Long fee) {
		this.fee = fee;
	}

	public Long getTax() {
		return tax;
	}

	public void setTax(Long tax) {
		this.tax = tax;
	}

	public String getError_code() {
		return error_code;
	}

	public void setError_code(String error_code) {
		this.error_code = error_code;
	}

	public String getError_description() {
		return error_description;
	}

	public void setError_description(String error_description) {
		this.error_description = error_description;
	}

	public Long getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Long created_at) {
		this.created_at = created_at;
	}

	public TransactionEntry toEntity() {
		TransactionEntry transactionEntry = new TransactionEntry();

		transactionEntry.setPayId(this.getId());
		transactionEntry.setEntity(this.getEntity());
		transactionEntry.setAmount(this.getAmount()/100);
		transactionEntry.setCurrency(this.getCurrency());
		transactionEntry.setStatus(this.getStatus());
		transactionEntry.setOrderId(this.getOrder_id());
		transactionEntry.setInvoiceId(this.getInvoice_id());
		transactionEntry.setInternational(this.getInternational());
		transactionEntry.setMethod(this.getMethod());
		transactionEntry.setAmountRefunded(this.getAmount_refunded());
		transactionEntry.setCaptured(this.getCaptured());
		transactionEntry.setDescription(this.getDescription());
		transactionEntry.setCardId(this.getCard_id());
		transactionEntry.setBank(this.getBank());
		transactionEntry.setWallet(this.getWallet());
		transactionEntry.setVpa(this.getVpa());
		transactionEntry.setEmail(this.getEmail());
		transactionEntry.setContactNum(this.getContactNum());
		transactionEntry.setAddress(this.getNotes().getAddress());
		transactionEntry.setUsername(this.getNotes().getUsername());
		transactionEntry.setUserId(this.getNotes().getUserId());
		transactionEntry.setProductName(this.notes.getProductName());// for PromoCode
		transactionEntry.setPromoCodeName(this.notes.getPromoCode());// for PromoCode

		transactionEntry.setFee(this.getFee());
		transactionEntry.setTax(this.getTax());
		transactionEntry.setErrorCode(this.getError_code());
		transactionEntry.setErrorDescription(this.getError_description());
		transactionEntry.setCreatedAt(new Date(this.getCreated_at() * 1000));

		return transactionEntry;
	}
}
