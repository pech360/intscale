package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.StudentNotes;

public class StudentNotesRequest implements Request {
    private Long id;
    private Long studentId;
    private String notes;
    private String selfNotes;
    private String sessionName;
    public StudentNotes toEntity(StudentNotes notes){
       notes.setNotes(this.notes);
       notes.setSelfNotes(this.selfNotes);
       notes.setSessionName(this.sessionName);
       return notes;
    }

    public Long getId() {
        return id;
    }

    public StudentNotes toEntity(){
        return toEntity(new StudentNotes());
    }

    public Long getStudentId() {
        return studentId;
    }

    public String getNotes() {
        return notes;
    }

    public String getSelfNotes() {
        return selfNotes;
    }

    public String getSessionName() {
        return sessionName;
    }
}
