package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.Role;

import java.util.HashSet;
import java.util.Set;

public class RoleRequest implements Request
{
  private Long id;
  private String name;
  private Set<PrivilegeRequest> privileges = new HashSet<>();
  private Set<String>     dashboards = new HashSet<>();

  public RoleRequest() {}

  public RoleRequest(Long id)
  {
    this.id = id;
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public Set<PrivilegeRequest> getPrivileges()
  {
    return privileges;
  }

  public void setPrivileges(Set<PrivilegeRequest> privileges)
  {
    this.privileges = privileges;
  }

  public Set<String> getDashboards()
  {
    return dashboards;
  }

  public void setDashboards(Set<String> dashboards)
  {
    this.dashboards = dashboards;
  }

  public Role toEntity(Role role)
  {
    if (role == null)
    {
      role = new Role();
    }
    role.setName(this.name);
    return role;
  }

  public Role toEntity()
  {
    return toEntity(new Role());
  }
}
