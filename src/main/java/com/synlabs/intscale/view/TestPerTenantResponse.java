package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.TestPerTenant;

public class TestPerTenantResponse implements Response {
    private Long id;
    private String testName;
    private String description;
    private boolean published;

    public TestPerTenantResponse(TestPerTenant test){
       this.id=test.getId();
       this.testName=test.getName();
       this.description=test.getDescription();
       this.published=test.isPublished();
    }

    public Long getId() {
        return id;
    }

    public String getTestName() {
        return testName;
    }

    public boolean isPublished() {
        return published;
    }

    public String getDescription() {
        return description;
    }
}
