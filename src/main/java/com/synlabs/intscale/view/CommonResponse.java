package com.synlabs.intscale.view;

import java.util.ArrayList;
import java.util.List;

public class CommonResponse<T> implements Response {

    public long totalRecords;
    public int pageSize;
    public int page;
    public List<T> records ;

    public CommonResponse(long totalRecords, int pageSize, int page,List<T> records ) {
        this.totalRecords = totalRecords;
        this.pageSize = pageSize;
        this.page = page;
        this.records = records ;
    }
    public CommonResponse(CommonResponse copy, List<T> records) {
        this.totalRecords = copy.totalRecords;
        this.pageSize = copy.pageSize;
        this.page = copy.page;
        this.records = records ;
    }

    public long getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<T> getRecords() {
        return records;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }
}

