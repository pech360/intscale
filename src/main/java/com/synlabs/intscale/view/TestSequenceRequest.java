package com.synlabs.intscale.view;

/**
 * Created by India on 12/12/2017.
 */
public class TestSequenceRequest implements Request {
    private Long id;
    private int sequence;
    private boolean testForce;
    public Long getId() {
        return id;
    }
    public int getSequence() {
        return sequence;
    }
    public boolean isTestForce() {
        return testForce;
    }
}
