package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.test.TestQuestion;
import com.synlabs.intscale.view.usertest.TestQuestionResponse;

import java.util.ArrayList;
import java.util.List;

public class TestDescriptionWithQuestionAndCategoryResponse implements Response
{
  private Long   id;
  private String name;
  private String logo;
  private String introduction;
  private List<TestQuestionResponse> questions = new ArrayList<>();

  public Long getId()
  {
    return id;
  }

  public String getName()
  {
    return name;
  }

  public String getLogo()
  {
    return logo;
  }

  public String getIntroduction()
  {
    return introduction;
  }

  public List<TestQuestionResponse> getQuestions()
  {
    return questions;
  }

  public TestDescriptionWithQuestionAndCategoryResponse()
  {
  }

  public TestDescriptionWithQuestionAndCategoryResponse(Test test)
  {
    this.id = test.getId();
    this.name = test.getName();
    this.logo = test.getLogo();
    this.introduction = test.getIntroduction();
    for (TestQuestion testQuestion : test.getQuestions())
    {
      this.questions.add(new TestQuestionResponse(testQuestion));
    }
  }
}
