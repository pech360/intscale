package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.AcademicPerformance;
import com.synlabs.intscale.enums.AcademicPerformanceName;
import com.synlabs.intscale.enums.AcademicPerformanceType;

import java.math.BigInteger;

public class AcademicPerformanceRequest implements Request
{
  private Long id;

  private AcademicPerformanceType type;

  private AcademicPerformanceName name;

  private BigInteger value;

  public AcademicPerformance toEntity(AcademicPerformance academicPerformance)
  {
    if (academicPerformance == null)
    {
      academicPerformance = new AcademicPerformance();
    }
    academicPerformance.setId(this.id);
    academicPerformance.setType(this.type);
    academicPerformance.setName(this.name);
    academicPerformance.setValue(this.value);
    return academicPerformance;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public AcademicPerformanceType getType() {
    return type;
  }

  public void setType(AcademicPerformanceType type) {
    this.type = type;
  }

  public AcademicPerformanceName getName() {
    return name;
  }

  public void setName(AcademicPerformanceName name) {
    this.name = name;
  }

  public BigInteger getValue() {
    return value;
  }

  public void setValue(BigInteger value) {
    this.value = value;
  }

  public AcademicPerformanceRequest(){

  }

  public AcademicPerformanceRequest(AcademicPerformanceType type, AcademicPerformanceName name, BigInteger value) {
    this.type = type;
    this.name = name;
    this.value = value;
  }
}
