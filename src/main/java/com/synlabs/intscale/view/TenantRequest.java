package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.Tenant;

/**
 * Created by India on 12/27/2017.
 */
public class TenantRequest implements Request {
    private Long id;
    private String name;
    private String description;
    private String domain;
    private String address;
    private int schoolLimit;
    public Tenant toEntity(Tenant tenant){
        tenant.setName(this.name);
        tenant.setDescription(this.description);
        tenant.setAddress(this.address);
        tenant.setSubDomain(this.domain);
        tenant.setSchoolLimit(this.schoolLimit);
      return tenant;
    }
    public Tenant toEntity(){return toEntity(new Tenant());}

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getDomain() {
        return domain;
    }

    public String getAddress() {
        return address;
    }

    public int getSchoolLimit() {
        return schoolLimit;
    }
}
