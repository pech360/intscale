package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.view.counselling.CounsellingSessionResponse;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class UserResponseForCounsellor implements Response {
    private Long id;
    private String name;
    private String username;
    private Long userId;
    private String gender;
    private String email;
    private String section;
    private String grade;
    private String schoolName;
    private String counsellor;
    private Date counsellorAssignedOn;
    private Date aimAssessmetsCompletedOn;
    private List<CounsellingSessionResponse> userCounsellingSessions = new ArrayList<>();

    public UserResponseForCounsellor(User st){
        this.id=st.getId();
        this.name=st.getName();
        this.username=st.getUsername();
        this.gender=st.getGender();
        this.email=st.getEmail();
        this.section=st.getSection();
        this.grade=st.getGrade();
        this.schoolName=st.getSchoolName();
        this.userId=st.getId();
        this.counsellor=st.getCounsellor()!=null?st.getCounsellor().getName():"";
        this.counsellorAssignedOn = st.getCounsellorAssignedOn();
        if(!CollectionUtils.isEmpty(st.getUserCounsellingSessions())){
            this.userCounsellingSessions.addAll(st.getUserCounsellingSessions().stream().map(CounsellingSessionResponse::new).collect(Collectors.toList()));
        }

    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public String getSection()
    {
        return section;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public String getGrade()
    {
        return grade;
    }

    public String getCounsellor() {
        return counsellor;
    }

    public Long getUserId() {
        return userId;
    }

    public Date getCounsellorAssignedOn() {
        return counsellorAssignedOn;
    }

    public Date getAimAssessmetsCompletedOn() {
        return aimAssessmetsCompletedOn;
    }

    public void setAimAssessmetsCompletedOn(Date aimAssessmetsCompletedOn) {
        this.aimAssessmetsCompletedOn = aimAssessmetsCompletedOn;
    }

    public List<CounsellingSessionResponse> getUserCounsellingSessions() {
        return userCounsellingSessions;
    }
}
