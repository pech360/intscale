package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.Student;

/**
 * Created by India on 1/3/2018.
 */
public class StudentResponse implements Response {
    private Long id;
    private String name;
    private String username;//By Default autoFill and username=password
    private Long userId;
    private String gender;
    private String email;
    private String section;
    private Long school;
    private String grade;
    private String schoolName;
    private String counsellor;
    public StudentResponse(Student st){
        this.id=st.getId();
        this.name=st.getName();
        this.username=st.getUsername();
        this.gender=st.getGender();
        this.email=st.getEmail();
        this.section=st.getSection();
        this.school=st.getSchool()!=null?st.getSchool().getId():0l;
        this.grade=st.getGrade();
        this.schoolName=st.getSchool()!=null?st.getSchool().getName():"";
        this.userId=st.getUser()!=null?st.getUser().getId():0l;
        this.counsellor=st.getCounsellor()!=null?st.getCounsellor().getUsername():"";
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public String getSection()
    {
        return section;
    }

    public Long getSchool() {
        return school;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public String getGrade()
    {
        return grade;
    }

    public String getCounsellor() {
        return counsellor;
    }

    public Long getUserId() {
        return userId;
    }
}
