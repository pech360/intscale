package com.synlabs.intscale.view;

import java.util.List;

public class SponsorUserAssignRequest implements Request {

    private Long roleId;
    private Long tenantId;
    private String schoolName;
    private String query;
    private String fromDate;
    private String toDate;
    private List<Long> sponsorIds;
    private List<Long> userIds;
    private boolean useSearchQuery = false;


    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public List<Long> getSponsorIds() {
        return sponsorIds;
    }

    public void setSponsorIds(List<Long> sponsorIds) {
        this.sponsorIds = sponsorIds;
    }

    public List<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<Long> userIds) {
        this.userIds = userIds;
    }

    public boolean isUseSearchQuery() {
        return useSearchQuery;
    }

    public void setUseSearchQuery(boolean useSearchQuery) {
        this.useSearchQuery = useSearchQuery;
    }
}
