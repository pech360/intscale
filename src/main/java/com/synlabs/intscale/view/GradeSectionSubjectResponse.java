package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.masterdata.GradeSectionSubject;
import com.synlabs.intscale.view.TestTreeResponse.TestSummaryResponse;

public class GradeSectionSubjectResponse implements Response
{
  private Long   id;
  private String grade;
  private String section;
  private Long   subject;
  private String subjectName;

  public GradeSectionSubjectResponse(GradeSectionSubject gradeSectionSubject)
  {
    this.id = gradeSectionSubject.getId();
    this.grade = gradeSectionSubject.getGrade();
    this.section = gradeSectionSubject.getSection();
    this.subject = gradeSectionSubject.getSubject();
    this.subjectName = gradeSectionSubject.getSubjectName();
  }

  public Long getId()
  {
    return id;
  }

  public String getGrade()
  {
    return grade;
  }

  public String getSection()
  {
    return section;
  }

  public Long getSubject()
  {
    return subject;
  }

  public String getSubjectName()
  {
    return subjectName;
  }

}