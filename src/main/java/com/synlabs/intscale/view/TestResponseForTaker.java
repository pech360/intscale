package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.test.TestQuestion;
import com.synlabs.intscale.view.question.QuestionResponseForTaker;

import java.util.ArrayList;
import java.util.List;

public class TestResponseForTaker implements Response {
    private Long    id;
    private Long    parentId;
    private String  name;
    private String  logo;
    private String  introduction;
    private String  participation;
    private String  disclaimer;
    private String  procedure;
    private String  description;
    private boolean random;
    private boolean autoIncrementQuestion;
    private boolean navigationAllowed;
    private boolean skipQuestions;
    private boolean disableSubmit;
    private List<QuestionResponseForTaker> questions = new ArrayList<>();
    private int     submitLimit;
    private boolean discontinueTest;
    private int     discontinueTestCriteria;
    private String  videoName;

    public TestResponseForTaker(Test test)
    {
        this.id = test.getId();
        this.introduction = test.getIntroduction();
        this.participation = test.getParticipation();
        this.disclaimer = test.getDisclaimer();
        this.procedure = test.getProcedure();
        this.random = test.isRandom();
        this.autoIncrementQuestion = test.isAutoIncrementQuestion();
        this.navigationAllowed = test.isNavigationAllowed();
        this.skipQuestions = test.isSkipQuestions();
        this.disableSubmit = test.isDisableSubmit();
        this.submitLimit = test.getSubmitLimit();
        this.discontinueTest = test.isDiscontinueTest();
        this.discontinueTestCriteria = test.getDiscontinueTestCriteria();
        this.parentId = test.getParent() != null ? test.getParent().getId() : 0L;
        this.name = test.getName();
        this.logo = test.getLogo();
        this.description = test.getDescription();
        this.videoName = test.getVideoName();
        for (TestQuestion testque : test.getQuestions())
        {
            QuestionResponseForTaker question = new QuestionResponseForTaker(testque.getQuestion());
            question.setSequence(testque.getSequence());
            questions.add(question);
        }
    }

    public Long getId() {
        return id;
    }

    public Long getParentId() {
        return parentId;
    }

    public String getName() {
        return name;
    }

    public String getLogo() {
        return logo;
    }

    public String getIntroduction() {
        return introduction;
    }

    public String getParticipation() {
        return participation;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public String getProcedure() {
        return procedure;
    }

    public String getDescription() {
        return description;
    }

    public boolean isRandom() {
        return random;
    }

    public boolean isAutoIncrementQuestion() {
        return autoIncrementQuestion;
    }

    public boolean isNavigationAllowed() {
        return navigationAllowed;
    }

    public boolean isSkipQuestions() {
        return skipQuestions;
    }

    public boolean isDisableSubmit() {
        return disableSubmit;
    }

    public List<QuestionResponseForTaker> getQuestions() {
        return questions;
    }

    public int getSubmitLimit() {
        return submitLimit;
    }

    public boolean isDiscontinueTest() {
        return discontinueTest;
    }

    public int getDiscontinueTestCriteria() {
        return discontinueTestCriteria;
    }

    public String getVideoName() {
        return videoName;
    }
}
