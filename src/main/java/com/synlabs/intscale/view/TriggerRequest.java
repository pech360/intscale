package com.synlabs.intscale.view;


import lombok.Getter;
import lombok.Setter;
import org.quartz.TriggerKey;

@Getter
@Setter
public class TriggerRequest {
    private String name;
    private String group;
    private String cronExpression;
    private Long interval;

    public TriggerKey getKey() {
        return new TriggerKey(name, group);
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public Long getInterval() {
		return interval;
	}

	public void setInterval(Long interval) {
		this.interval = interval;
	}
}
