package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.Timeline;
import org.springframework.util.StringUtils;

public class TimelineRequest implements Request
{
  private Long   id;
  private String heading;
  private String description;
  private String imageName;
  private String videoName;
  private String contentType;

  public String getHeading()
  {
    return heading;
  }

  public void setHeading(String heading)
  {
    this.heading = heading;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public String getImageName()
  {
    return imageName;
  }

  public void setImageName(String imageName)
  {
    this.imageName = imageName;
  }

  public String getContentType()
  {
    return contentType;
  }

  public void setContentType(String contentType)
  {
    this.contentType = contentType;
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getVideoName()
  {
    return videoName;
  }

  public void setVideoName(String videoName)
  {
    this.videoName = videoName;
  }

  public Timeline toEntity(Timeline timeline)
  {
    if (timeline == null)
    {
      timeline = new Timeline();
    }
    timeline.setHeading(this.heading);
    timeline.setDescription(this.description);
    timeline.setImageName(this.imageName);
    timeline.setVideoName(this.videoName);
    if (StringUtils.isEmpty(this.getContentType()) || timeline.getId() != null)
    {
      if (!StringUtils.isEmpty(timeline.getImageName()))
      {
        timeline.setContentType("image");
      }
      else
      {
        if (!timeline.getVideoName().isEmpty())
        {
          timeline.setContentType("video");
        }
        else
        {
          timeline.setContentType("thought");
        }
      }
    }
    else
    {
      timeline.setContentType(this.getContentType());
    }

    return timeline;
  }
}
