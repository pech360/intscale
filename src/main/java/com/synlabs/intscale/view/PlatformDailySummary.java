package com.synlabs.intscale.view;

import com.synlabs.intscale.util.Activity;
//LNT
public class PlatformDailySummary {
	 private String name;
	 private String userName;
	 private String email;
	 private String phoneNumber;
	 private String activity;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public PlatformDailySummary( String name, String userName, String email, String phoneNumber,
			String activity) {
		this.name = name;
		this.userName = userName;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.activity = activity;
	}
	public PlatformDailySummary( String name, String userName, String email, String phoneNumber) {
		this.name = name;
		this.userName = userName;
		this.email = email;
		this.phoneNumber = phoneNumber;	
	}
	public PlatformDailySummary() {
		
	}
	public PlatformDailySummary(Object[] columns) {
		this.name = columns[0]!=null?columns[0].toString():" ";
		this.userName = columns[1]!=null?columns[1].toString():" ";
		this.email = columns[2]!=null?columns[2].toString():"";
		this.phoneNumber= columns[3]!=null?columns[3].toString():" ";
		if(columns.length>4)
			this.activity= columns[4]!=null?columns[4].toString():" ";
	}
}
