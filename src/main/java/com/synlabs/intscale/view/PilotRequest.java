package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.Pilot;

/**
 * Created by India on 1/16/2018.
 */
public class PilotRequest implements Request {
    private Long id;
    private String name;
    private boolean active;

    public Pilot toEntity(Pilot pilot){
        pilot.setActive(this.active);
        pilot.setName(this.name);
        return pilot;
    }
    public Pilot toEntity(){
        return toEntity(new Pilot());
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isActive() {
        return active;
    }
}
