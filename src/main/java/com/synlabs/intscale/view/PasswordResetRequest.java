package com.synlabs.intscale.view;

/**
 * Created by itrs on 5/3/2017.
 */
public class PasswordResetRequest implements Request
{
  private String token;
  private String password;

  public String getToken()
  {
    return token;
  }

  public void setToken(String token)
  {
    this.token = token;
  }

  public String getPassword()
  {
    return password;
  }

  public void setPassword(String password)
  {
    this.password = password;
  }
}
