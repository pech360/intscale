package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.Role;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class RoleResponse implements Response
{

  private Long                   id;
  private String                 name;
  private Set<PrivilegeResponse> privileges;
  private Set<String> dashboards = new HashSet<>();

  public RoleResponse(Role role)
  {

    this.id = role.getId();
    this.name = role.getName();
    if (role.getPrivileges() != null)
    {
      this.privileges = role.getPrivileges().stream().map(PrivilegeResponse::new).collect(Collectors.toSet());
    }

    if(role.getDashboard()!=null){
      Collections.addAll(this.dashboards, StringUtils.isEmpty(role.getDashboard()) ? new String[0] : role.getDashboard().split(","));
    }
  }

  public Long getId()
  {
    return id;
  }

  public String getName()
  {
    return name;
  }

  public Set<PrivilegeResponse> getPrivileges()
  {
    return privileges;
  }

  public Set<String> getDashboards()
  {
    return dashboards;
  }
}
