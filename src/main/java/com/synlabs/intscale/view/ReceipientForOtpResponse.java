//For messages property in success response 
package com.synlabs.intscale.view;

public class ReceipientForOtpResponse {

	private String id;
	private Long recipient;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getRecipient() {
		return recipient;
	}
	public void setRecipient(Long recipient) {
		this.recipient = recipient;
	}
}
