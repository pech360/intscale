package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.Student;

/**
 * Created by India on 1/3/2018.
 */
public class StudentRequest implements Request
{
  private Long   id;
  private String name;
  private String username;
  private String gender;
  private String email;
  private String   section;
  private Long   school;
  private String grade;

  public Student toEntity(Student st)
  {
    if (st == null)
    {
      st = new Student();
    }
    st.setName(this.name);
    st.setGender(this.gender);
    st.setUsername(this.username);
    st.setEmail(this.email);
    return st;
  }

  public Student toEntity()
  {
    return toEntity(new Student());
  }

  public Long getId()
  {
    return id;
  }

  public String getName()
  {
    return name;
  }

  public String getUsername()
  {
    return username;
  }

  public String getGender()
  {
    return gender;
  }

  public String getEmail()
  {
    return email;
  }

  public Long getSchool()
  {
    return school;
  }

  public String getGrade()
  {
    return grade;
  }

  public String getSection()
  {
    return section;
  }
}
