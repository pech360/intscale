package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.Role;

/**
 * Created by itrs on 9/19/2017.
 */
public class RoleSummaryResponse implements Response
{
  private Long id;
  private String name;
  public RoleSummaryResponse(Role role){
    this.id=role.getId();
    this.name=role.getName();
  }

  public Long getId()
  {
    return id;
  }

  public String getName()
  {
    return name;
  }
}
