package com.synlabs.intscale.view;
//LNT
public class PromoCodeValidateRequest implements Request {
	private Long userId;
	private String promoCodeName;
	private String productName;

	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getPromoCodeName() {
		return promoCodeName;
	}
	public void setPromoCodeName(String promoCodeName) {
		this.promoCodeName = promoCodeName;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}

}
