package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.masterdata.Sponsor;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.view.counselling.CounsellingSessionResponse;
import com.synlabs.intscale.view.masterdata.SponsorResponse;
import com.synlabs.intscale.view.threesixty.StakeholderResponse;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class UserResponse extends UserSummaryResponse
{
  private String userType;
  private String email;
  private String gender;
  private String grade;
  private String higherEducation;
  private String section;
  private Date   dob;
  private String language;
  private String designation;
  private String industry;
  private String orgName;
  private String img;
  private int    testCount;
  private String interest[];
  private Set<String> roles = new HashSet<>();
  private Set<String> tests = new HashSet<>();
  private String  testStatus;
  private String  school;
  private Long    tenant;
  private String  academicScore;
  private String  supw;
  private String  stream;
  private Boolean enableAimReport;
  private List<StakeholderResponse> stakeholders = new ArrayList<>();
  private String city;
  private String nameOfTestsTaken = "";
  private Date            counsellorAssignedOn;
  private TeacherResponse teacher;
  private Boolean premium;//Edit Field //content isPaid property value;
  private Boolean ftlCount; //check first time login to show begin test model
  private String contactNumber;
  private String signupDate;

  private List<CounsellingSessionResponse> userCounsellingSessions = new ArrayList<>();

  private List<CounsellingSessionResponse> counsellorCounsellingSessions = new ArrayList<>();

  private List<AcademicPerformanceResponse> academicPerformances = new ArrayList<>();
  private List<SponsorResponse> sponsors = new ArrayList<>();

public enum ResponseType
{
  User, Summary, Stakeholder
}

  public UserResponse(User user, ResponseType type)
  {
    super(user);
    type = type == null ? ResponseType.User : type;
    switch (type)
    {
      case Stakeholder:
        if (!CollectionUtils.isEmpty(user.getStakeholders()))
        {
          stakeholders = user.getStakeholders().stream()
                             .map(u -> new StakeholderResponse(u, StakeholderResponse.ResponseType.Summary))
                             .collect(Collectors.toList());
        }
        this.tenant = user.getTenant() != null ? user.getTenant().getId() : 0L;
        break;

      case Summary:
        this.email = user.getEmail();
        this.img = user.getDisplayPicture();
        this.dob = user.getDob();
        this.gender = user.getGender();
        this.school = user.getSchoolName();
        break;

      case User:
        this.userType = user.getUserType();
        this.img = user.getDisplayPicture();
        this.email = user.getEmail();
        this.language = user.getLanguage();
        this.dob = user.getDob();
        this.gender = user.getGender();
        this.grade = user.getGrade();
        this.higherEducation = user.getHigherEducation();
        this.section = user.getSection();
        this.orgName = user.getOrgName();
        //this.testCount=user.getAssignedTestCount();
        this.designation = user.getDesignation();
        this.industry = user.getIndustry();
        this.academicScore = user.getAcademicScore();
        this.supw = user.getSupw();
        this.stream = user.getStream();
        this.tenant = user.getTenant() != null ? user.getTenant().getId() : 0L;
        this.premium=user.getIsPaid();//Edit code
        this.ftlCount=user.getFtlCount();//Edit code
        this.contactNumber=user.getContactNumber();
        if (user.getInterests() != null)
        {
          this.interest = splitInterests(user.getInterests());
        }
        this.school = user.getSchoolName();
        if (user.getRoles() != null)
        {
          user.getRoles().forEach(role -> {
            this.roles.add(role.getName());
          });
        }
        if (user.getEnableAimReport() == null)
        {
          this.enableAimReport = false;
        }
        else
        {
          this.enableAimReport = user.getEnableAimReport();
        }
        this.city = user.getCity();

        if (user.getTests() != null)
        {
          this.testCount = user.getTests().size();
          user.getTests().forEach(testResult -> {
            this.nameOfTestsTaken += testResult.getTest().getName() + ", ";
          });
        }

        if (user.getTeacher() != null)
        {
          this.teacher = new TeacherResponse(user.getTeacher());
        }

        this.academicPerformances = user.getAcademicPerformances().stream().map(AcademicPerformanceResponse :: new).collect(Collectors.toList());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        this.signupDate = dateFormat.format(DateUtils.addMinutes(user.getCreatedDate().toDate(), 330));

        this.sponsors = user.getSponsors().stream().map(SponsorResponse::new).collect(Collectors.toList());
    }
  }

  public UserResponse(User user)
  {
    this(user, ResponseType.User);
  }

  public List<StakeholderResponse> getStakeholders()
  {
    return stakeholders;
  }

  public String getEmail()
  {
    return email;
  }

  public String getGender()
  {
    return gender;
  }

  public Date getDob()
  {
    return dob;
  }

  public String getLanguage()
  {
    return language;
  }

  public String getDesignation()
  {
    return designation;
  }

  public String getIndustry()
  {
    return industry;
  }

  public String getOrgName()
  {
    return orgName;
  }

  public String getImg()
  {
    return img;
  }

  public int getTestCount()
  {
    return testCount;
  }

  public String[] getInterest()
  {
    return interest;
  }

  public String getUserType()
  {
    return userType;
  }

  public Set<String> getRoles()
  {
    return roles;
  }

  public Set<String> getTests()
  {
    return tests;
  }

  public String getTestStatus()
  {
    return testStatus;
  }

  public String getGrade()
  {
    return grade;
  }

  public String getSection()
  {
    return section;
  }

  public String getSchool()
  {
    return school;
  }

  public Long getTenant()
  {
    return tenant;
  }

  public String getAcademicScore()
  {
    return academicScore;
  }

  public String getSupw()
  {
    return supw;
  }

  private String[] splitInterests(String text)
  {
    return text.split(", ");
  }

  public String getStream()
  {
    return stream;
  }

  public Boolean getEnableAimReport()
  {
    return enableAimReport;
  }

  public void setEnableAimReport(Boolean enableAimReport)
  {
    this.enableAimReport = enableAimReport;
  }

  public String getCity()
  {
    return city;
  }

  public String getNameOfTestsTaken()
  {
    return nameOfTestsTaken;
  }

  public Date getCounsellorAssignedOn()
  {
    return counsellorAssignedOn;
  }

  public TeacherResponse getTeacher()
  {
    return teacher;
  }

  public List<CounsellingSessionResponse> getUserCounsellingSessions()
  {
    return userCounsellingSessions;
  }

  public List<CounsellingSessionResponse> getCounsellorCounsellingSessions()
  {
    return counsellorCounsellingSessions;
  }

  public List<AcademicPerformanceResponse> getAcademicPerformances()
  {
    return academicPerformances;
  }

  public String getHigherEducation() {
    return higherEducation;
  }

  public Boolean getPremium() {//Edit code
	return premium;
  }

  public Boolean getFtlCount() {
	return ftlCount;
  }

  public String getContactNumber() {
	return contactNumber;
  }

  public void setContactNumber(String contactNumber) {
	this.contactNumber = contactNumber;
  }

  public String getSignupDate() {
    return signupDate;
  }

  public List<SponsorResponse> getSponsors() {
    return sponsors;
  }

  public void setSponsors(List<SponsorResponse> sponsors) {
    this.sponsors = sponsors;
  }
}
