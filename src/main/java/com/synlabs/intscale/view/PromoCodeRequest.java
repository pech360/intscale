package com.synlabs.intscale.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.synlabs.intscale.entity.PromoCode;
//LNT
public class PromoCodeRequest implements Request {

	@NotNull
	private String name;
	@NotNull
	@Min(value = 0)
	@Max(value = 100)
	private BigDecimal discount;
	@NotNull
	private Date couponStartDate;
	@NotNull
	private Date couponEndDate;
	@NotNull
	private Integer totalCount;
	@NotNull
	private List<String> productName = new ArrayList<>();

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public Integer getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	public Date getCouponStartDate() {
		return couponStartDate;
	}
	public void setCouponStartDate(Date couponStartDate) {
		this.couponStartDate = couponStartDate;
	}
	public Date getCouponEndDate() {
		return couponEndDate;
	}
	public void setCouponEndDate(Date couponEndDate) {
		this.couponEndDate = couponEndDate;
	}
	public List<String> getProductName() {
		return productName;
	}
	public void setProductName(List<String> productName) {
		this.productName = productName;
	}
	public PromoCodeRequest() {

	}
	public PromoCodeRequest(String name, BigDecimal discount, Date couponStartDate, Date couponEndDate,
			Integer totalCount, List<String> productName) {
		this.name = name;
		this.discount = discount;
		this.couponStartDate = couponStartDate;
		this.couponEndDate = couponEndDate;
		this.totalCount = totalCount;
		this.productName = productName;
	}
	public PromoCode toEntity() {
		PromoCode promoCode = new PromoCode();
		promoCode.setName(this.name);
		promoCode.setDiscount(this.discount);
		promoCode.setIsValid(true);
		promoCode.setCouponStartDate(this.couponStartDate);
		promoCode.setCouponEndDate(this.couponEndDate);
		promoCode.setTotalCount(this.totalCount);
		promoCode.setConsumedCount(0);
		return promoCode;
	}
}
