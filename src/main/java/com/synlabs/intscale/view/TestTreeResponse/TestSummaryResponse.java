package com.synlabs.intscale.view.TestTreeResponse;

import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.view.Response;

/**
 * Created by India on 1/23/2018.
 */
public class TestSummaryResponse implements Response {
    private Long id;
    private String name;
    private String description;
    private String parent;
    private boolean leafNode;
    private boolean construct;
    private String introduction;
    private String procedure;
    private String participation;
    private String disclaimer;
    private int sequence;
    private boolean testForce;
    private Long version;
    private String grades[];
    private Long parentId;

    public TestSummaryResponse(Test test){
        this.id=test.getId();
        this.name=test.getName();
        this.description=test.getDescription();
        this.parent=test.getParent()!=null?test.getParent().getName():"";
        this.leafNode=test.isLeafNode();
        this.construct=test.isConstruct();
        this.introduction=test.getIntroduction();
        this.procedure=test.getProcedure();
        this.participation=test.getParticipation();
        this.disclaimer=test.getDisclaimer();
        this.sequence=test.getSequence();
        this.testForce=test.isTestForce();
        this.version=test.getVersion();
        this.grades=test.getGrades();
        this.parentId=test.getParent()!=null?test.getParent().getId():0l;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isLeafNode() {
        return leafNode;
    }

    public boolean isConstruct() {
        return construct;
    }

    public String getDescription() {
        return description;
    }

    public String getParent() {
        return parent;
    }

    public String getIntroduction() {
        return introduction;
    }

    public String getProcedure() {
        return procedure;
    }

    public String getParticipation() {
        return participation;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public int getSequence() {
        return sequence;
    }

    public boolean isTestForce() {
        return testForce;
    }

    public Long getVersion() {
        return version;
    }

    public String[] getGrades() {
        return grades;
    }

    public Long getParentId() {
        return parentId;
    }
}
