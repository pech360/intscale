package com.synlabs.intscale.view.TestTreeResponse;

import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.view.Response;

import java.util.List;

/**
 * Created by India on 1/22/2018.
 */
public class TestSubParent implements Response {
    private Long id;
    private String name;
    private String description;
    private String image;
    private List<TestChild> testChild;
    private boolean start;
    private boolean isLeaf;
    private boolean isConstruct;
    private String  introduction;
    private String  participation;
    private String  disclaimer;
    private String  procedure;
    private int sequence;
    private boolean testForce;
    private boolean isChecker;

    public TestSubParent getResponse(Test subTest, TestSubParent childLevel2){
        childLevel2.setId(subTest.getId());
        childLevel2.setName(subTest.getName());
        childLevel2.setDescription(subTest.getDescription());
        childLevel2.setImage(subTest.getLogo());
        childLevel2.setLeaf(subTest.isLeafNode());
        childLevel2.setConstruct(subTest.isConstruct());
        childLevel2.setIntroduction(subTest.getIntroduction());
        childLevel2.setParticipation(subTest.getParticipation());
        childLevel2.setDisclaimer(subTest.getDisclaimer());
        childLevel2.setProcedure(subTest.getProcedure());
        childLevel2.setSequence(subTest.getSequence());
        childLevel2.setTestForce(subTest.isTestForce());
        return childLevel2;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TestChild> getTestChild() {
        return testChild;
    }

    public void setTestChild(List<TestChild> testChild) {
        this.testChild = testChild;
    }

    public boolean isStart() {
        return start;
    }

    public void setStart(boolean start) {
        this.start = start;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isLeaf() {
        return isLeaf;
    }

    public void setLeaf(boolean leaf) {
        isLeaf = leaf;
    }

    public boolean isConstruct() {
        return isConstruct;
    }

    public void setConstruct(boolean construct) {
        isConstruct = construct;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getParticipation() {
        return participation;
    }

    public void setParticipation(String participation) {
        this.participation = participation;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public String getProcedure() {
        return procedure;
    }

    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public boolean isTestForce() {
        return testForce;
    }

    public void setTestForce(boolean testForce) {
        this.testForce = testForce;
    }

    public boolean isChecker() {
        return isChecker;
    }

    public void setChecker(boolean checker) {
        isChecker = checker;
    }
}
