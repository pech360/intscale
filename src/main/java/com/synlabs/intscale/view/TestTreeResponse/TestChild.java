package com.synlabs.intscale.view.TestTreeResponse;

import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.view.Response;

import java.util.List;

/**
 * Created by India on 1/22/2018.
 */
public class TestChild implements Response {
    private Long id;
    private String name;
    private String description;
    private String image;
    private List<TestLeaf> testLeafs;
    private boolean selected;
    private boolean isLeaf;
    private boolean isConstruct;
    private String  introduction;
    private String  participation;
    private String  disclaimer;
    private String  procedure;
    private int sequence;
    private boolean testForce;

    public TestChild getResponse(Test child,TestChild childLevel3){
        childLevel3.setId(child.getId());
        childLevel3.setName(child.getName());
        childLevel3.setDescription(child.getDescription());
        childLevel3.setImage(child.getLogo());
        childLevel3.setLeaf(child.isLeafNode());
        childLevel3.setConstruct(child.isConstruct());
        childLevel3.setIntroduction(child.getIntroduction());
        childLevel3.setParticipation(child.getParticipation());
        childLevel3.setDisclaimer(child.getDisclaimer());
        childLevel3.setProcedure(child.getProcedure());
        childLevel3.setSequence(child.getSequence());
        childLevel3.setTestForce(child.isTestForce());
        return childLevel3;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TestLeaf> getTestLeafs() {
        return testLeafs;
    }

    public void setTestLeafs(List<TestLeaf> testLeafs) {
        this.testLeafs = testLeafs;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isLeaf() {
        return isLeaf;
    }

    public void setLeaf(boolean leaf) {
        isLeaf = leaf;
    }

    public boolean isConstruct() {
        return isConstruct;
    }

    public void setConstruct(boolean construct) {
        isConstruct = construct;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getParticipation() {
        return participation;
    }

    public void setParticipation(String participation) {
        this.participation = participation;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public String getProcedure() {
        return procedure;
    }

    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public boolean isTestForce() {
        return testForce;
    }

    public void setTestForce(boolean testForce) {
        this.testForce = testForce;
    }
}
