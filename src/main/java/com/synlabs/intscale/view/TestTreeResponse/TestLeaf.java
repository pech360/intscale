package com.synlabs.intscale.view.TestTreeResponse;

import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.view.Response;

/**
 * Created by India on 1/22/2018.
 */
public class TestLeaf implements Response {
    private Long id;
    private String name;
    private String description;
    private String image;
    private boolean selected;
    private String  introduction;
    private String  participation;
    private String  disclaimer;
    private String  procedure;
    private int sequence;
    private boolean testForce;

    public TestLeaf getResponse(Test leafNode,TestLeaf childLevel4){
        childLevel4.setId(leafNode.getId());
        childLevel4.setName(leafNode.getName());
        childLevel4.setDescription(leafNode.getDescription());
        childLevel4.setImage(leafNode.getLogo());
        childLevel4.setIntroduction(leafNode.getIntroduction());
        childLevel4.setParticipation(leafNode.getParticipation());
        childLevel4.setDisclaimer(leafNode.getDisclaimer());
        childLevel4.setProcedure(leafNode.getProcedure());
        childLevel4.setSequence(leafNode.getSequence());
        childLevel4.setTestForce(leafNode.isTestForce());
        return childLevel4;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getParticipation() {
        return participation;
    }

    public void setParticipation(String participation) {
        this.participation = participation;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public String getProcedure() {
        return procedure;
    }

    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public boolean isTestForce() {
        return testForce;
    }

    public void setTestForce(boolean testForce) {
        this.testForce = testForce;
    }
}
