package com.synlabs.intscale.view.TestTreeResponse;

import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.view.Response;

import java.util.List;

/**
 * Created by India on 1/22/2018.
 */
public class TestParent implements Response {
    private Long id;
    private String name;
    private String description;
    private String image;
    private boolean selected;
    private boolean isLeaf;
    private boolean isConstruct;
    private String  introduction;
    private String  participation;
    private String  disclaimer;
    private String  procedure;
    private int sequence;
    private boolean testForce;
    private List<TestSubParent>  subParents;

    public TestParent getResponse(Test parentTest,TestParent childLevel1){
        childLevel1.setId(parentTest.getId());
        childLevel1.setName(parentTest.getName());
        childLevel1.setDescription(parentTest.getDescription());
        childLevel1.setImage(parentTest.getLogo());
        childLevel1.setLeaf(parentTest.isLeafNode());
        childLevel1.setConstruct(parentTest.isConstruct());
        childLevel1.setIntroduction(parentTest.getIntroduction());
        childLevel1.setParticipation(parentTest.getParticipation());
        childLevel1.setDisclaimer(parentTest.getDisclaimer());
        childLevel1.setProcedure(parentTest.getProcedure());
        childLevel1.setSequence(parentTest.getSequence());
        childLevel1.setTestForce(parentTest.isTestForce());
        return childLevel1;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TestSubParent> getSubParents() {
        return subParents;
    }

    public void setSubParents(List<TestSubParent> subParents) {
        this.subParents = subParents;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isLeaf() {
        return isLeaf;
    }

    public void setLeaf(boolean leaf) {
        isLeaf = leaf;
    }

    public boolean isConstruct() {
        return isConstruct;
    }

    public void setConstruct(boolean construct) {
        isConstruct = construct;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getParticipation() {
        return participation;
    }

    public void setParticipation(String participation) {
        this.participation = participation;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public String getProcedure() {
        return procedure;
    }

    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public boolean isTestForce() {
        return testForce;
    }

    public void setTestForce(boolean testForce) {
        this.testForce = testForce;
    }
}
