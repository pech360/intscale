package com.synlabs.intscale.view;

import java.util.List;

public class OtpFailureResponse {

	private List<WarningsForOtpFailureResponse> warnings;
	private List<ErrorsForOtpFailureResponse> errors;
	private String status;
	
	public List<WarningsForOtpFailureResponse> getWarnings() {
		return warnings;
	}
	public void setWarnings(List<WarningsForOtpFailureResponse> warnings) {
		this.warnings = warnings;
	}
	public List<ErrorsForOtpFailureResponse> getErrors() {
		return errors;
	}
	public void setErrors(List<ErrorsForOtpFailureResponse> errors) {
		this.errors = errors;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
