package com.synlabs.intscale.view;

/**
 * Created by India on 1/5/2018.
 */
public class StudentGradeRequest implements Request {
    private Long[] studentIds;
    private String grade;
    private String section;

    public Long[] getStudentIds() {
        return studentIds;
    }

    public String getGrade()
    {
        return grade;
    }

    public String getSection()
    {
        return section;
    }
}
