package com.synlabs.intscale.view;
import com.synlabs.intscale.entity.test.Test;
import org.apache.commons.lang3.time.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by India on 12/22/2017.
 */
public class TestHistory {
    private Long id;
    private String name;
    private String date;
    private String notes;
    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    public TestHistory(Test test){
        this.id=test.getId();
        this.name=test.getName();
        this.date=dateFormat.format(DateUtils.addMinutes(test.getCreatedDate().toDate(), 330));
        this.notes=test.getNotes();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public String getNotes() {
        return notes;
    }
}
