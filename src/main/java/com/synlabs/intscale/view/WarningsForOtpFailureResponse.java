package com.synlabs.intscale.view;

public class WarningsForOtpFailureResponse {

	private String message;
	private String numbers;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getNumbers() {
		return numbers;
	}
	public void setNumbers(String numbers) {
		this.numbers = numbers;
	}
}
