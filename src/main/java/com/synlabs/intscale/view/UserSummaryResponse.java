package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.User;

public class UserSummaryResponse
{
  private Long    id;
  private String  username;
  private String  name;
  private String  grade;
  private Boolean threeSixtyEnabled;
  private String tenantName;

  public UserSummaryResponse(User user)
  {
    this.id = user.getId();
    this.username = user.getUsername();
    this.name = user.getName();
    this.grade = user.getGrade();
    this.threeSixtyEnabled = user.getThreeSixtyEnabled();
    this.threeSixtyEnabled = this.threeSixtyEnabled == null ? false : this.threeSixtyEnabled;
    this.tenantName = user.getTenant().getName();
  }

  public UserSummaryResponse()
  {

  }

  public Boolean getThreeSixtyEnabled()
  {
    return threeSixtyEnabled;
  }

  public String getUsername()
  {
    return username;
  }

  public String getName()
  {
    return name;
  }

  public Long getId()
  {
    return id;
  }

  public String getGrade()
  {
    return grade;
  }

  public String getTenantName()
  {
    return tenantName;
  }
}
