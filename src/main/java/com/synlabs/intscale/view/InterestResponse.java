package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.masterdata.Interest;

public class InterestResponse implements Response
{
  private Long id;
  private String   name;
  private String parent;
  private String   image;

  public String getName()
  {
    return name;
  }

  public String getParent()
  {
    return parent;
  }

  public String getImage()
  {
    return image;
  }

  public Long getId()
  {
    return id;
  }

  public InterestResponse(Interest interest)
  {
    this.name = interest.getName();
    if(interest.getParent()!=null){
      this.parent=interest.getParent().getName();
    }
    this.image=interest.getImage();
    this.id = interest.getId();
  }
}
