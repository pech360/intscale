//For message property of success response
package com.synlabs.intscale.view;

public class MessageForOtpResponse {

	private Long num_parts;
	private String sender;
	private String content;
	
	public Long getNum_parts() {
		return num_parts;
	}
	public void setNum_parts(Long num_parts) {
		this.num_parts = num_parts;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
}
