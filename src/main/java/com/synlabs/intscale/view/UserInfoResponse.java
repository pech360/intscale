package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.User;

public class UserInfoResponse implements Response {
    private Long id;
    private String username;
    public UserInfoResponse(User user){
        this.id=user.getId();
        this.username=user.getUsername();
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }
}
