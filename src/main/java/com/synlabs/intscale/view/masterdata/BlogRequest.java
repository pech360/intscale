package com.synlabs.intscale.view.masterdata;

import com.synlabs.intscale.entity.masterdata.Blog;
import com.synlabs.intscale.view.Request;

import java.util.ArrayList;
import java.util.List;

public class BlogRequest implements Request
{
  private Long   id;
  private String name;
  private String url;
  private List<Long> interestsIds = new ArrayList<>();

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getUrl()
  {
    return url;
  }

  public void setUrl(String url)
  {
    this.url = url;
  }

  public List<Long> getInterestsIds()
  {
    return interestsIds;
  }

  public void setInterestsIds(List<Long> interestsIds)
  {
    this.interestsIds = interestsIds;
  }

  public Blog toEntity(Blog blog)
  {
    if (blog == null)
    {
      blog = new Blog();
    }
    blog.setName(this.name);
    blog.setUrl(this.url);
    return blog;
  }
}
