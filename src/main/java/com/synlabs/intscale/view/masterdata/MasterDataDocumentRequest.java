package com.synlabs.intscale.view.masterdata;

import com.synlabs.intscale.entity.masterdata.MasterDataDocument;
import com.synlabs.intscale.view.Request;

/**
 * Created by itrs on 7/25/2017.
 */
public class MasterDataDocumentRequest implements Request
{
  private Long id;
  private String type;
  private String value;

  public Long getId()
  {
    return id;
  }

  public String getType()
  {
    return type;
  }

  public String getValue()
  {
    return value;
  }

  public MasterDataDocument toEntity(MasterDataDocument masterDataDocument){
    if(masterDataDocument==null){masterDataDocument=new MasterDataDocument();}
    masterDataDocument.setType(this.type);
    masterDataDocument.setValue(this.value);
    return masterDataDocument;
  }

  public MasterDataDocument toEntity(){return toEntity(new MasterDataDocument());}
}
