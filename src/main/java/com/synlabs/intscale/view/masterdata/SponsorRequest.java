package com.synlabs.intscale.view.masterdata;

import com.synlabs.intscale.entity.masterdata.Sponsor;
import com.synlabs.intscale.view.Request;

public class SponsorRequest implements Request {
    private Long id;
    private String name;
    private String description;
    private String logo;
    private boolean active;

    public Sponsor toEntity(Sponsor sponsor) {
        if (sponsor == null) {
            sponsor = new Sponsor();
        }
        sponsor.setName(this.getName());
        sponsor.setDescription(this.getDescription());
        sponsor.setActive(this.isActive());
        return sponsor;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
