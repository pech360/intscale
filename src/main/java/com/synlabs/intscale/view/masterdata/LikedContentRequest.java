package com.synlabs.intscale.view.masterdata;

import com.synlabs.intscale.view.Request;

public class LikedContentRequest implements Request
{
  private Long userId;
  private Long blogId;
  private boolean isLiked;

  public Long getUserId()
  {
    return userId;
  }

  public void setUserId(Long userId)
  {
    this.userId = userId;
  }

  public Long getBlogId()
  {
    return blogId;
  }

  public void setBlogId(Long blogId)
  {
    this.blogId = blogId;
  }

  public boolean isLiked()
  {
    return isLiked;
  }

  public void setLiked(boolean liked)
  {
    isLiked = liked;
  }
}
