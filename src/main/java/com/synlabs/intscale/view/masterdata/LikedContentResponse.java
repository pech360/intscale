package com.synlabs.intscale.view.masterdata;

import com.synlabs.intscale.entity.masterdata.LikedContent;
import com.synlabs.intscale.view.Response;

public class LikedContentResponse implements Response
{
  private Long    userId;
  private Long    blogId;
  private boolean isLiked;
  private Long likes;

  public Long getUserId()
  {
    return userId;
  }

  public Long getBlogId()
  {
    return blogId;
  }

  public boolean isLiked()
  {
    return isLiked;
  }

  public Long getLikes()
  {
    return likes;
  }

  public void setLikes(Long likes)
  {
    this.likes = likes;
  }

  public LikedContentResponse(LikedContent content){
    this.blogId = content.getBlog().getId();
    this.isLiked = content.isLiked();
    this.userId = content.getUser().getId();
  }
}
