package com.synlabs.intscale.view.masterdata;

import com.synlabs.intscale.entity.masterdata.Blog;
import com.synlabs.intscale.entity.masterdata.Interest;
import com.synlabs.intscale.view.InterestResponse;
import com.synlabs.intscale.view.Response;

import java.util.ArrayList;
import java.util.List;

public class BlogResponse implements Response
{
  private Long    id;
  private String  name;
  private String  url;
  private Long    likes;
  private String  author;
  private String  title;
  private String  description;
  private String  logoUrl;
  private String  imageUrl;
  private String  videoUrl;
  private boolean isLiked;
  private List<InterestResponse> interests = new ArrayList<>();

  public Long getId()
  {
    return id;
  }

  public String getName()
  {
    return name;
  }

  public String getUrl()
  {
    return url;
  }

  public Long getLikes()
  {
    return likes;
  }

  public List<InterestResponse> getInterests()
  {
    return interests;
  }

  public String getAuthor()
  {
    return author;
  }

  public String getTitle()
  {
    return title;
  }

  public String getDescription()
  {
    return description;
  }

  public String getLogoUrl()
  {
    return logoUrl;
  }

  public String getImageUrl()
  {
    return imageUrl;
  }

  public String getVideoUrl()
  {
    return videoUrl;
  }

  public boolean isLiked()
  {
    return isLiked;
  }

  public void setLiked(boolean liked)
  {
    this.isLiked = liked;
  }

  public BlogResponse(Blog blog)
  {
    this.id = blog.getId();
    this.url = blog.getUrl();
    this.name = blog.getName();
    this.likes = blog.getLikes();
    this.author=blog.getAuthor();
    this.title=blog.getTitle();
    this.description=blog.getDescription();
    this.logoUrl=blog.getLogoUrl();
    this.imageUrl=blog.getImageUrl();
    this.videoUrl=blog.getVideoUrl();
    for (Interest interest : blog.getInterests())
    {
      this.interests.add(new InterestResponse(interest));
    }
  }
}
