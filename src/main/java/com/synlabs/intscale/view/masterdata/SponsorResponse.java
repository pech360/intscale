package com.synlabs.intscale.view.masterdata;

import com.synlabs.intscale.entity.masterdata.Sponsor;
import com.synlabs.intscale.view.Response;

public class SponsorResponse implements Response {
    private Long id;
    private String name;
    private String description;
    private String logo;
    private String reportCoverFrontImage;
    private String reportCoverBackImage;
    private boolean active;



    public SponsorResponse(Sponsor sponsor) {
        this.id = sponsor.getId();
        this.name = sponsor.getName();
        this.description = sponsor.getDescription();
        this.logo = sponsor.getLogo();
        this.reportCoverFrontImage = sponsor.getReportCoverFrontImage();
        this.reportCoverBackImage = sponsor.getReportCoverBackImage();
        this.active = sponsor.isActive();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getReportCoverFrontImage() {
        return reportCoverFrontImage;
    }

    public void setReportCoverFrontImage(String reportCoverFrontImage) {
        this.reportCoverFrontImage = reportCoverFrontImage;
    }

    public String getReportCoverBackImage() {
        return reportCoverBackImage;
    }

    public void setReportCoverBackImage(String reportCoverBackImage) {
        this.reportCoverBackImage = reportCoverBackImage;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
