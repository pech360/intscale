package com.synlabs.intscale.view.masterdata;

import com.synlabs.intscale.entity.masterdata.MasterDataDocument;
import com.synlabs.intscale.view.Response;

/**
 * Created by itrs on 7/25/2017.
 */
public class MasterDataDocumentResponse implements Response
{
  private Long   id;
  private String type;
  private String value;

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getType()
  {
    return type;
  }

  public void setType(String type)
  {
    this.type = type;
  }

  public String getValue()
  {
    return value;
  }

  public void setValue(String value)
  {
    this.value = value;
  }

  public MasterDataDocumentResponse(MasterDataDocument masterDataDocument)
  {
    this.id = masterDataDocument.getId();
    this.type = masterDataDocument.getType();
    this.value = masterDataDocument.getValue();
  }

  public MasterDataDocumentResponse()
  {
  }
}
