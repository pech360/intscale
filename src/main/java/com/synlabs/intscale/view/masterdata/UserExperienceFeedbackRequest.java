package com.synlabs.intscale.view.masterdata;

import com.synlabs.intscale.entity.masterdata.UserExperienceFeedback;

public class UserExperienceFeedbackRequest
{
  private String username;
  private Integer rating;
  private String experienceDescription;

  public String getUsername()
  {
    return username;
  }

  public void setUsername(String username)
  {
    this.username = username;
  }

  public Integer getRating()
  {
    return rating;
  }

  public void setRating(Integer rating)
  {
    this.rating = rating;
  }

  public String getExperienceDescription()
  {
    return experienceDescription;
  }

  public void setExperienceDescription(String experienceDescription)
  {
    this.experienceDescription = experienceDescription;
  }

  public UserExperienceFeedback toEntity(UserExperienceFeedback userExperienceFeedback)
  {
    if (userExperienceFeedback == null)
    {
      userExperienceFeedback = new UserExperienceFeedback();
    }
      userExperienceFeedback.setRating(this.rating);
      userExperienceFeedback.setExperienceDescription((this.experienceDescription));
    return userExperienceFeedback;
  }
}
