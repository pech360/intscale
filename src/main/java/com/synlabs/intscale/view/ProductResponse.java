package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.test.Product;
import com.synlabs.intscale.entity.test.Test;

import java.util.*;

/**
 * Created by India on 1/24/2018.
 */
public class ProductResponse implements Response {
    private Long id;
    private String name;
    private String description;
    private Set<String> tests = new HashSet<>();
    private List<Long> testId=new ArrayList<>();

    public ProductResponse(Product product){
        this.id=product.getId();
        this.name=product.getName();
        this.description=product.getDescription();
        if(product.getTest()!=null){
            product.getTest().forEach(test ->{
                this.tests.add(test.getName());
                this.testId.add(test.getId());
            });
        }
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Set<String> getTests() {
        return tests;
    }

    public List<Long> getTestId() {
        return testId;
    }
}
