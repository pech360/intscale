package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.test.Subscription;
import com.synlabs.intscale.entity.test.Test;

import java.util.List;

/**
 * Created by itrs on 6/21/2017.
 */
public class SubscriptionRequest implements Request
{
  private Long      id;
  private String      userType;
  private int         count;
  private int         usedkeys;
  private String      language;
  private String      studentType;
  private String      gradeType;
  private String      instituteName;
  private List<Test>  tests;
  //private List<Hobby> hobbies;

  public void setId(Long id)
  {
    this.id = id;
  }

  public void setUserType(String userType)
  {
    this.userType = userType;
  }

  public void setCount(int count)
  {
    this.count = count;
  }

  public void setUsedkeys(int usedkeys)
  {
    this.usedkeys = usedkeys;
  }

  public void setLanguage(String language)
  {
    this.language = language;
  }

  public void setStudentType(String studentType)
  {
    this.studentType = studentType;
  }

  public void setGradeType(String gradeType)
  {
    this.gradeType = gradeType;
  }

  public void setInstituteName(String instituteName)
  {
    this.instituteName = instituteName;
  }

  public void setTests(List<Test> tests)
  {
    this.tests = tests;
  }

  public Long getId()
  {
    return id;
  }

  public String getUserType()
  {
    return userType;
  }

  public int getCount()
  {
    return count;
  }

  public int getUsedkeys()
  {
    return usedkeys;
  }

  public String getLanguage()
  {
    return language;
  }

  public String getStudentType()
  {
    return studentType;
  }

  public String getGradeType()
  {
    return gradeType;
  }

  public String getInstituteName()
  {
    return instituteName;
  }

  public Subscription toEntity(Subscription subscription){
    if(subscription==null){
      subscription=new Subscription();
    }
    subscription.setUserType(this.userType);
    subscription.setKeyUsesCount(this.count);
    subscription.setGradeType(this.gradeType);
    subscription.setInstituteName(this.instituteName);
    subscription.setLanguage(this.language);
    subscription.setStudentType(this.studentType);
    subscription.setUsedKeysCount(this.usedkeys);
    subscription.setTests(this.tests);
    return subscription;
  }
  public Subscription toEntity()
  {
    return toEntity(new Subscription());
  }
}
