package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.test.Subscription;

/**
 * Created by itrs on 6/21/2017.
 */
public class SubscriptionResponse implements Response
{
  private Long   id;
  private String   userType;
  private int      count;
  private int      usedkeys;
  private String   language;
  private String   studentType;
  private String   gradeType;
  private String   instituteName;
  private String uniqueTestKey;

  public SubscriptionResponse(Subscription subscription){
    this.id=subscription.getId();
    this.userType=subscription.getUserType();
    this.count=subscription.getKeyUsesCount();
    this.usedkeys=subscription.getUsedKeysCount();
    this.language=subscription.getLanguage();
    this.studentType=subscription.getStudentType();
    this.gradeType=subscription.getGradeType();
    this.instituteName=subscription.getInstituteName();
    this.uniqueTestKey=subscription.getTestKey();
  }

  public Long getId()
  {
    return id;
  }

  public String getUserType()
  {
    return userType;
  }

  public int getCount()
  {
    return count;
  }

  public int getUsedkeys()
  {
    return usedkeys;
  }

  public String getLanguage()
  {
    return language;
  }

  public String getStudentType()
  {
    return studentType;
  }

  public String getGradeType()
  {
    return gradeType;
  }

  public String getInstituteName()
  {
    return instituteName;
  }

  public String getUniqueTestKey()
  {
    return uniqueTestKey;
  }
}
