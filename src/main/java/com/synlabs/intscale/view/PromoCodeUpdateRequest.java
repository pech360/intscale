package com.synlabs.intscale.view;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.synlabs.intscale.entity.PromoCode;
//LNT
public class PromoCodeUpdateRequest implements Request {

	private Boolean isValid;
	private BigDecimal discount;
	private Integer totalCount;
	private Date couponStartDate;
	private Date couponEndDate;
	private List<String> productName;

	public Integer getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	public Date getCouponStartDate() {
		return couponStartDate;
	}
	public void setCouponStartDate(Date couponStartDate) {
		this.couponStartDate = couponStartDate;
	}
	public Date getCouponEndDate() {
		return couponEndDate;
	}
	public void setCouponEndDate(Date couponEndDate) {
		this.couponEndDate = couponEndDate;
	}
	public Boolean getIsValid() {
		return isValid;
	}
	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public List<String> getProductName() {
		return productName;
	}
	public void setProductName(List<String> productName) {
		this.productName = productName;
	}
	public PromoCode toEntity(PromoCode promoCode) throws ParseException {
		promoCode = promoCode == null ? new PromoCode() : promoCode; 
    	promoCode.setName(promoCode.getName());
		promoCode.setDiscount(this.discount == null ? promoCode.getDiscount() : this.discount);
		promoCode.setIsValid(this.isValid == null ? promoCode.getIsValid() : this.isValid);
		promoCode.setCouponStartDate(this.couponStartDate == null
				? new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(promoCode.getCouponStartDate())
				: this.couponStartDate);
		promoCode.setCouponEndDate(this.couponEndDate == null
				? new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(promoCode.getCouponEndDate())
				: this.couponEndDate);
		promoCode.setTotalCount(this.totalCount == null ? promoCode.getTotalCount() : this.totalCount);
		promoCode.setConsumedCount(promoCode.getConsumedCount());
		return promoCode;
	}

}
