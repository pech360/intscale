package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.Timeline;
import org.apache.commons.lang3.time.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimelineResponse
{
  private Long   id;
  private String heading;
  private String description;
  private String imageName;
  private String videoName;
  private String contentType;
  private String createdDate;

  public Long getId()
  {
    return id;
  }

  public String getHeading()
  {
    return heading;
  }

  public String getDescription()
  {
    return description;
  }

  public String getImageName()
  {
    return imageName;
  }

  public String getContentType()
  {
    return contentType;
  }

  public String getCreatedDate()
  {
    return createdDate;
  }

  public String getVideoName()
  {
    return videoName;
  }

  public TimelineResponse(Timeline post)
  {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    this.id = post.getId();
    this.heading = post.getHeading();
    this.description = post.getDescription();
    this.imageName = post.getImageName();
    this.videoName = post.getVideoName();
    this.contentType = post.getContentType();
    this.createdDate = dateFormat.format(DateUtils.addMinutes(post.getCreatedDate().toDate(), 330));
  }
}
