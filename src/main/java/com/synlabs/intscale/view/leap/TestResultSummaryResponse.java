package com.synlabs.intscale.view.leap;

import com.synlabs.intscale.view.Response;

public class TestResultSummaryResponse implements Response
{
  private Long  userId;
  private String  name;
  private String  type;
  private Long  constructId;
  private Integer syllabusCompleted;
  private Float percentageScore;

  public Long getUserId()
  {
    return userId;
  }

  public void setUserId(Long userId)
  {
    this.userId = userId;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getType()
  {
    return type;
  }

  public void setType(String type)
  {
    this.type = type;
  }

  public Long getConstructId()
  {
    return constructId;
  }

  public void setConstructId(Long constructId)
  {
    this.constructId = constructId;
  }

  public Integer getSyllabusCompleted()
  {
    return syllabusCompleted;
  }

  public void setSyllabusCompleted(Integer syllabusCompleted)
  {
    this.syllabusCompleted = syllabusCompleted;
  }

  public Float getPercentageScore()
  {
    return percentageScore;
  }

  public void setPercentageScore(Float percentageScore)
  {
    this.percentageScore = percentageScore;
  }
}
