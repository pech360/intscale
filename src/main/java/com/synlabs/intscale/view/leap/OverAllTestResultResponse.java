package com.synlabs.intscale.view.leap;

import com.synlabs.intscale.view.Response;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public class OverAllTestResultResponse implements Response
{
  private String name;
  private String type;
  private Long id;
  private Double syllabusCompleted;
  private Map<String, Long> values = new HashMap<>();

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getType()
  {
    return type;
  }

  public void setType(String type)
  {
    this.type = type;
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public Double getSyllabusCompleted()
  {
    return syllabusCompleted;
  }

  public void setSyllabusCompleted(Double syllabusCompleted)
  {
    this.syllabusCompleted = syllabusCompleted;
  }

  public Map<String, Long> getValues()
  {
    return values;
  }

  public void setValues(Map<String, Long> values)
  {
    this.values = values;
  }
}
