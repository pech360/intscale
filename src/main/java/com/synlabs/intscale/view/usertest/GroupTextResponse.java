package com.synlabs.intscale.view.usertest;

import com.synlabs.intscale.entity.test.GroupText;
import com.synlabs.intscale.view.Response;

/**
 * Created by India on 10/10/2017.
 */
public class GroupTextResponse implements Response {
    private Long id;
    private String commonText;
    private String commonType;
    private String fileName;
    public GroupTextResponse(GroupText text){
        if(text!=null){
        this.id=text.getId();
        this.commonText=text.getCommonText();
        if(text.getType().equals("Image")){
            this.fileName=text.getImageName();
        }else if(text.getType().equals("Audio")){
            this.fileName=text.getAudioName();
        }else if(text.getType().equals("Video")){
            this.fileName=text.getVideoName();
        }
        this.commonType=text.getType();
        }
    }

    public Long getId() {
        return id;
    }

    public String getCommonText() {
        return commonText;
    }

    public String getCommonType() {
        return commonType;
    }

    public String getFileName() {
        return fileName;
    }
}
