package com.synlabs.intscale.view.usertest;

import com.synlabs.intscale.entity.Report.Category;
import com.synlabs.intscale.entity.test.TestQuestion;
import com.synlabs.intscale.view.report.CategoryResponse;
import com.synlabs.intscale.view.Response;
import com.synlabs.intscale.view.question.QuestionResponse;

import java.util.ArrayList;
import java.util.List;

public class TestQuestionResponse implements Response
{
  private Long             testId;
  private QuestionResponse question;
  private List<CategoryResponse> category = new ArrayList<>();

  public Long getTestId()
  {
    return testId;
  }

  public QuestionResponse getQuestion()
  {
    return question;
  }

  public List<CategoryResponse> getCategory()
  {
    return category;
  }

  public TestQuestionResponse(TestQuestion testQuestion)
  {
    this.testId = testQuestion.getTest().getId();
    this.question = new QuestionResponse(testQuestion.getQuestion(), testQuestion.getCategory(), testQuestion.getSubCategory());
    for (Category tempCategory : testQuestion.getCategory())
    {
      this.category.add(new CategoryResponse(tempCategory));
    }
    for (Category tempCategory : testQuestion.getSubCategory())
    {
      this.category.add(new CategoryResponse(tempCategory));
    }
  }
}
