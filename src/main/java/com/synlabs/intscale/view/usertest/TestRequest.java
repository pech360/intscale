package com.synlabs.intscale.view.usertest;

import com.synlabs.intscale.entity.test.Question;
import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.jpa.QuestionRepository;
import com.synlabs.intscale.view.Request;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class TestRequest implements Request
{

  private Long           id;
  private Long           parentId;
  private String         name;
  private String         logo;
  private String         introduction;
  private String         participation;
  private String         disclaimer;
  private String         procedure;
  private String         description;
  private int            ageMin;
  private int            ageMax;
  private String         scoreCategory;
  private String         scoreSubCategory;
  private String         notes;
  private String         redirectTo;
  private boolean        paid;
  private boolean        showResults;
  private boolean        random;
  private boolean        autoIncrementQuestion;
  private boolean        navigationAllowed;
  private boolean        skipQuestions;
  private boolean        published;
  private Tenant         tenant;
  private String         userType;
  private String         studentType;
  private String         gender;
  private String         grades[];
  private String         language;
  private Boolean        showTimer;
  private boolean        disableSubmit;
  private int            submitLimit;
  private boolean        discontinueTest;
  private int            discontinueTestCriteria;
  private String         videoName;
  private Long           version;
  private List<Question> questions = new ArrayList<>();
  private String         primaryReportText;
  private String         secondaryReportText;
  private String         reportType;
  private String         scoringFormula;
  //  private List<Roles>       roles;

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public Long getParentId()
  {
    return parentId;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getLogo()
  {
    return logo;
  }

  public void setLogo(String logo)
  {
    this.logo = logo;
  }

  public String getIntroduction()
  {
    return introduction;
  }

  public void setIntroduction(String introduction)
  {
    this.introduction = introduction;
  }

  public String getParticipation()
  {
    return participation;
  }

  public void setParticipation(String participation)
  {
    this.participation = participation;
  }

  public String getDisclaimer()
  {
    return disclaimer;
  }

  public void setDisclaimer(String disclaimer)
  {
    this.disclaimer = disclaimer;
  }

  public String getProcedure()
  {
    return procedure;
  }

  public void setProcedure(String procedure)
  {
    this.procedure = procedure;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public int getAgeMin()
  {
    return ageMin;
  }

  public void setAgeMin(int ageMin)
  {
    this.ageMin = ageMin;
  }

  public int getAgeMax()
  {
    return ageMax;
  }

  public void setAgeMax(int ageMax)
  {
    this.ageMax = ageMax;
  }

  public String getScoreCategory()
  {
    return scoreCategory;
  }

  public void setScoreCategory(String scoreCategory)
  {
    this.scoreCategory = scoreCategory;
  }

  public String getScoreSubCategory()
  {
    return scoreSubCategory;
  }

  public void setScoreSubCategory(String scoreSubCategory)
  {
    this.scoreSubCategory = scoreSubCategory;
  }

  public String getNotes()
  {
    return notes;
  }

  public void setNotes(String notes)
  {
    this.notes = notes;
  }

  public String getRedirectTo()
  {
    return redirectTo;
  }

  public void setRedirectTo(String redirectTo)
  {
    this.redirectTo = redirectTo;
  }

  public boolean isPaid()
  {
    return paid;
  }

  public void setPaid(boolean paid)
  {
    this.paid = paid;
  }

  public boolean isShowResults()
  {
    return showResults;
  }

  public void setShowResults(boolean showResults)
  {
    this.showResults = showResults;
  }

  public boolean isRandom()
  {
    return random;
  }

  public void setRandom(boolean random)
  {
    this.random = random;
  }

  public boolean isAutoIncrementQuestion()
  {
    return autoIncrementQuestion;
  }

  public void setAutoIncrementQuestion(boolean autoIncrementQuestion)
  {
    this.autoIncrementQuestion = autoIncrementQuestion;
  }

  public boolean isNavigationAllowed()
  {
    return navigationAllowed;
  }

  public void setNavigationAllowed(boolean navigationAllowed)
  {
    this.navigationAllowed = navigationAllowed;
  }

  public boolean isSkipQuestions()
  {
    return skipQuestions;
  }

  public void setSkipQuestions(boolean skipQuestions)
  {
    this.skipQuestions = skipQuestions;
  }

  public boolean isPublished()
  {
    return published;
  }

  public void setPublished(boolean published)
  {
    this.published = published;
  }

  public Tenant getTenant()
  {
    return tenant;
  }

  public void setTenant(Tenant tenant)
  {
    this.tenant = tenant;
  }

  public List<Question> getQuestions()
  {
    return questions;
  }

  public void setQuestions(List<Question> questions)
  {
    this.questions = questions;
  }

  public String getUserType()
  {
    return userType;
  }

  public void setUserType(String userType)
  {
    this.userType = userType;
  }

  public String getStudentType()
  {
    return studentType;
  }

  public void setStudentType(String studentType)
  {
    this.studentType = studentType;
  }

  public String getGender()
  {
    return gender;
  }

  public void setGender(String gender)
  {
    this.gender = gender;
  }

  public String[] getGrades()
  {
    return grades;
  }

  public String getLanguage()
  {
    return language;
  }

  public void setLanguage(String language)
  {
    this.language = language;
  }

  public Boolean getShowTimer()
  {
    return showTimer;
  }

  public void setShowTimer(Boolean showTimer)
  {
    this.showTimer = showTimer;
  }

  public boolean isDisableSubmit()
  {
    return disableSubmit;
  }

  public void setDisableSubmit(boolean disableSubmit)
  {
    this.disableSubmit = disableSubmit;
  }

  public int getSubmitLimit()
  {
    return submitLimit;
  }

  public void setSubmitLimit(int submitLimit)
  {
    this.submitLimit = submitLimit;
  }

  public boolean isDiscontinueTest()
  {
    return discontinueTest;
  }

  public void setDiscontinueTest(boolean discontinueTest)
  {
    this.discontinueTest = discontinueTest;
  }

  public int getDiscontinueTestCriteria()
  {
    return discontinueTestCriteria;
  }

  public void setDiscontinueTestCriteria(int discontinueTestCriteria)
  {
    this.discontinueTestCriteria = discontinueTestCriteria;
  }

  public String getVideoName()
  {
    return videoName;
  }

  public void setVideoName(String videoName)
  {
    this.videoName = videoName;
  }

  public Long getVersion()
  {
    return version;
  }

  public String getPrimaryReportText()
  {
    return primaryReportText;
  }

  public void setPrimaryReportText(String primaryReportText)
  {
    this.primaryReportText = primaryReportText;
  }

  public String getSecondaryReportText()
  {
    return secondaryReportText;
  }

  public void setSecondaryReportText(String secondaryReportText)
  {
    this.secondaryReportText = secondaryReportText;
  }

  public String getReportType()
  {
    return reportType;
  }

  public String getScoringFormula()
  {
    return scoringFormula;
  }

  public void setScoringFormula(String scoringFormula)
  {
    this.scoringFormula = scoringFormula;
  }

  public Test toEntity(Test test)
  {

    if (test == null)
    {
      test = new Test();
    }
    test.setLogo(this.logo);
    test.setName(this.name);
    test.setIntroduction(this.introduction);
    test.setParticipation(this.participation);
    test.setDisclaimer(this.disclaimer);
    test.setProcedure(this.procedure);
    test.setDescription(this.description);
    test.setAgeMin(this.ageMin);
    test.setAgeMax(this.ageMax);
    test.setPaid(this.isPaid());
    test.setRedirectTo(this.redirectTo);
    test.setScoreCategory(this.scoreCategory);
    test.setScoreSubCategory(this.scoreSubCategory);
    test.setNotes(this.notes);
    test.setPublished(this.published);
    test.setShowResults(this.isShowResults());
    test.setRandom(this.isRandom());
    test.setAutoIncrementQuestion(this.isAutoIncrementQuestion());
    test.setNavigationAllowed(this.isNavigationAllowed());
    test.setSkipQuestions(this.skipQuestions);
    test.setTenant(this.tenant);
    test.setUserType(this.userType);
    test.setStudentType(this.studentType);
    test.setGender(this.gender);
    test.setGrades(this.grades);
    test.setLanguage(this.language);
    test.setDisableSubmit(this.disableSubmit);
    test.setSubmitLimit(this.submitLimit);
    test.setDiscontinueTest(this.discontinueTest);
    test.setDiscontinueTestCriteria(this.discontinueTestCriteria);
    test.setVideoName(this.videoName);
    test.setPrimaryReportText(this.primaryReportText);
    test.setSecondaryReportText(this.secondaryReportText);
    test.setReportType(this.reportType);
    return test;
  }
}
