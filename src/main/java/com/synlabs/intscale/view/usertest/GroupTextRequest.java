package com.synlabs.intscale.view.usertest;

import com.synlabs.intscale.view.Request;

/**
 * Created by India on 10/10/2017.
 */
public class GroupTextRequest implements Request {
  private String groupName;
  private String commonText;
  private String commonType;

  public String getGroupName() {
    return groupName;
  }

  public String getCommonText() {
    return commonText;
  }

  public String getCommonType() {
    return commonType;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  public void setCommonText(String commonText) {
    this.commonText = commonText;
  }

  public void setCommonType(String commonType) {
    this.commonType = commonType;
  }
}
