package com.synlabs.intscale.view.usertest;

import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.test.TestResult;
import com.synlabs.intscale.entity.test.UserAnswer;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.view.Request;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ResultRequest implements Request
{
  private Long testId;
  private Date testLoadedAt;
  private Date testBeginAt;
  private Date testFinishAt;
  private List<UserAnswerRequest> answers = new ArrayList<>();

  public Long getTestId()
  {
    return testId;
  }

  public void setTestId(Long testId)
  {
    this.testId = testId;
  }

  public List<UserAnswerRequest> getAnswers()
  {
    return answers;
  }

  public void setAnswers(List<UserAnswerRequest> answers)
  {
    this.answers = answers;
  }

  public Date getTestLoadedAt() {
    return testLoadedAt;
  }

  public Date getTestBeginAt() {
    return testBeginAt;
  }

  public Date getTestFinishAt()
  {
    return testFinishAt;
  }

  public TestResult toEntity(TestResult testResult)
  {
    if (testResult == null)
    {
      testResult = new TestResult();
    }

    for (UserAnswerRequest userAnswerRequest : answers)
    {
      testResult.getUserAnswers().add(userAnswerRequest.toEntity(new UserAnswer()));
    }

    testResult.setTestLoadedAt(this.testLoadedAt);
    testResult.setTestBeginAt(this.testBeginAt);
    testResult.setTestFinishAt(new Date());
    return testResult;
  }
}
