package com.synlabs.intscale.view.usertest;

import com.synlabs.intscale.entity.Report.Category;
import com.synlabs.intscale.view.Request;

import java.util.List;

public class TestQuestionRequest implements Request
{
  private Long           test;
  private Long           question;
  private List<Category> category;
  private List<Category> subCategory;

  public Long getTest()
  {
    return test;
  }

  public void setTest(Long test)
  {
    this.test = test;
  }

  public Long getQuestion()
  {
    return question;
  }

  public void setQuestion(Long question)
  {
    this.question = question;
  }

  public List<Category> getCategory()
  {
    return category;
  }

  public void setCategory(List<Category> category)
  {
    this.category = category;
  }

  public List<Category> getSubCategory()
  {
    return subCategory;
  }

  public void setSubCategory(List<Category> subCategory)
  {
    this.subCategory = subCategory;
  }
}
