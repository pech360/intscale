package com.synlabs.intscale.view.usertest;

import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.test.TestQuestion;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.view.Response;
import com.synlabs.intscale.view.question.QuestionResponse;

import java.util.ArrayList;
import java.util.List;

public class TestResponse implements Response
{

  private Long    id;
  private Long    parentId;
  private String  name;
  private String  logo;
  private String  introduction;
  private String  participation;
  private String  disclaimer;
  private String  procedure;
  private String  description;
  private int     ageMin;
  private int     ageMax;
  private String  scoreCategory;
  private String  scoreSubCategory;
  private String  notes;
  private String  redirectTo;
  private boolean paid;
  private boolean showResults;
  private boolean random;
  private boolean autoIncrementQuestion;
  private boolean navigationAllowed;
  private boolean skipQuestions;
  private boolean published;
  private Tenant  tenant;
  private String  userType;
  private String  studentType;
  private String  gender;
  private String  grades[];
  private String  language;
  private boolean disableSubmit;
  private List<QuestionResponse> questions = new ArrayList<>();
  private int     submitLimit;
  private boolean discontinueTest;
  private int     discontinueTestCriteria;
  private String  videoName;
  private Long    version;
  private boolean latestVersion;
  private String  primaryReportText;
  private String  secondaryReportText;
  private String  scoringFormula;
  private String  reportType;
  private boolean threeSixtyEnabled;

  //  private List<Roles>       roles;

  public enum ResponseType
  {
    Summary, Full
  }

  public TestResponse()
  {
  }

  public TestResponse(Test test, ResponseType type)
  {
    type = type == null ? ResponseType.Summary : type;
    switch (type)
    {
      case Full:
        this.introduction = test.getIntroduction();
        this.participation = test.getParticipation();
        this.disclaimer = test.getDisclaimer();
        this.procedure = test.getProcedure();
        this.ageMax = test.getAgeMax();
        this.ageMin = test.getAgeMin();
        this.scoreCategory = test.getScoreCategory();
        this.scoreSubCategory = test.getScoreSubCategory();
        this.notes = test.getNotes();
        this.redirectTo = test.getRedirectTo();
        this.paid = test.isPaid();
        this.notes = test.getNotes();
        this.showResults = test.isShowResults();
        this.random = test.isRandom();
        this.autoIncrementQuestion = test.isAutoIncrementQuestion();
        this.navigationAllowed = test.isNavigationAllowed();
        this.skipQuestions = test.isSkipQuestions();
        this.published = test.isPublished();
        this.tenant = test.getTenant();
        this.userType = test.getUserType();
        this.studentType = test.getStudentType();
        this.gender = test.getGender();
        this.grades = test.getGrades();
        this.language = test.getLanguage();
        this.disableSubmit = test.isDisableSubmit();
        this.submitLimit = test.getSubmitLimit();
        this.discontinueTest = test.isDiscontinueTest();
        this.discontinueTestCriteria = test.getDiscontinueTestCriteria();
        this.latestVersion = test.isLatestVersion();
        this.primaryReportText = test.getPrimaryReportText();
        this.secondaryReportText = test.getSecondaryReportText();
        for (TestQuestion testque : test.getQuestions())
        {
          questions.add(new QuestionResponse(testque.getQuestion(), testque.getSequence(), testque.getCategory(), testque.getSubCategory()));
        }
      case Summary:
        this.id = test.getId();
        this.parentId = test.getParent() != null ? test.getParent().getId() : 0L;
        this.name = test.getName();
        this.logo = test.getLogo();
        this.description = test.getDescription();
        this.videoName = test.getVideoName();
        this.version = test.getVersion();
        this.threeSixtyEnabled = test.getThreeSixtyEnabled() == null ? false : test.getThreeSixtyEnabled();
        this.scoringFormula = test.getScoringFormula();
        this.reportType = test.getReportType();
        break;
    }
  }

  public TestResponse(Test test)
  {
    this(test, ResponseType.Full);
  }

  public String getScoringFormula()
  {
    return scoringFormula;
  }

  public boolean isThreeSixtyEnabled()
  {
    return threeSixtyEnabled;
  }

  public Long getId()
  {
    return id;
  }

  public Long getParentId()
  {
    return parentId;
  }

  public String getName()
  {
    return name;
  }

  public String getLogo()
  {
    return logo;
  }

  public String getIntroduction()
  {
    return introduction;
  }

  public String getParticipation()
  {
    return participation;
  }

  public String getDisclaimer()
  {
    return disclaimer;
  }

  public String getProcedure()
  {
    return procedure;
  }

  public String getDescription()
  {
    return description;
  }

  public int getAgeMin()
  {
    return ageMin;
  }

  public int getAgeMax()
  {
    return ageMax;
  }

  public String getScoreCategory()
  {
    return scoreCategory;
  }

  public String getScoreSubCategory()
  {
    return scoreSubCategory;
  }

  public String getNotes()
  {
    return notes;
  }

  public String getRedirectTo()
  {
    return redirectTo;
  }

  public boolean isPaid()
  {
    return paid;
  }

  public boolean isShowResults()
  {
    return showResults;
  }

  public boolean isRandom()
  {
    return random;
  }

  public boolean isAutoIncrementQuestion()
  {
    return autoIncrementQuestion;
  }

  public boolean isNavigationAllowed()
  {
    return navigationAllowed;
  }

  public boolean isSkipQuestions()
  {
    return skipQuestions;
  }

  public boolean isPublished()
  {
    return published;
  }

  public Tenant getTenant()
  {
    return tenant;
  }

  public String getUserType()
  {
    return userType;
  }

  public String getStudentType()
  {
    return studentType;
  }

  public String getGender()
  {
    return gender;
  }

  public String[] getGrades()
  {
    return grades;
  }

  public String getLanguage()
  {
    return language;
  }

  public List<QuestionResponse> getQuestions()
  {
    return questions;
  }

  public boolean isDisableSubmit()
  {
    return disableSubmit;
  }

  public int getSubmitLimit()
  {
    return submitLimit;
  }

  public boolean isDiscontinueTest()
  {
    return discontinueTest;
  }

  public int getDiscontinueTestCriteria()
  {
    return discontinueTestCriteria;
  }

  public String getVideoName()
  {
    return videoName;
  }

  public Long getVersion()
  {
    return version;
  }

  public boolean isLatestVersion()
  {
    return latestVersion;
  }

  public String getPrimaryReportText()
  {
    return primaryReportText;
  }

  public String getSecondaryReportText()
  {
    return secondaryReportText;
  }

  public String getReportType()
  {
    return reportType;
  }
}

