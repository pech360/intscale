package com.synlabs.intscale.view.usertest;

import com.synlabs.intscale.view.Response;

public class GradeResultResponse implements Response
{
  private String grade;
  private String section;
  private String productName;
  private Long   productId;
  private String studentName;
  private Long   userId;
  private Float  maximumMarks;
  private Float  obtainedMarks;
  private Float  percentScores;

  public String getGrade()
  {
    return grade;
  }

  public void setGrade(String grade)
  {
    this.grade = grade;
  }

  public String getSection()
  {
    return section;
  }

  public void setSection(String section)
  {
    this.section = section;
  }

  public String getProductName()
  {
    return productName;
  }

  public void setProductName(String productName)
  {
    this.productName = productName;
  }

  public String getStudentName()
  {
    return studentName;
  }

  public void setStudentName(String studentName)
  {
    this.studentName = studentName;
  }

  public Long getUserId()
  {
    return userId;
  }

  public void setUserId(Long userId)
  {
    this.userId = userId;
  }

  public Long getProductId()
  {
    return productId;
  }

  public void setProductId(Long productId)
  {
    this.productId = productId;
  }

  public Float getPercentScores()
  {
    return percentScores;
  }

  public void setPercentScores(Float percentScores)
  {
    this.percentScores = percentScores;
  }

  public Float getMaximumMarks()
  {
    return maximumMarks;
  }

  public void setMaximumMarks(Float maximumMarks)
  {
    this.maximumMarks = maximumMarks;
  }

  public Float getObtainedMarks()
  {
    return obtainedMarks;
  }

  public void setObtainedMarks(Float obtainedMarks)
  {
    this.obtainedMarks = obtainedMarks;
  }
}