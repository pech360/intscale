package com.synlabs.intscale.view.usertest;

import com.synlabs.intscale.entity.test.UserAnswer;
import com.synlabs.intscale.view.Response;

public class UserAnswerResponse implements Response {
    private Long questionId;
    private String questionDescription;
    private Long id;
    private int timeTaken;
    private String description;
    private String correctAnswerDescription;
    private int marks;
    private int maxMarks;
    private int minMarks;
    private boolean hasText;
    private boolean hasImage;
    private boolean hasAudio;
    private int optionNumber;
    private String Text;


    public UserAnswerResponse(UserAnswer userAnswer) {
        this.id = userAnswer.getId();
        this.questionId = userAnswer.getQuestion().getId();
        this.questionDescription = userAnswer.getQuestion().getDescription();
        this.hasText = userAnswer.isHasText();
        this.timeTaken = userAnswer.getTimeTaken();
        this.optionNumber = userAnswer.getOptionNumber();
        this.description = userAnswer.getDescription();
        this.marks = userAnswer.getMarks();
        this.maxMarks = userAnswer.getMaxMarks();
        this.minMarks = userAnswer.getMinMarks();
    }

    public Long getQuestionId() {
        return questionId;
    }

    public Long getId() {
        return id;
    }

    public int getTimeTaken() {
        return timeTaken;
    }

    public String getDescription() {
        return description;
    }

    public int getMarks() {
        return marks;
    }

    public boolean isHasText() {
        return hasText;
    }

    public boolean isHasImage() {
        return hasImage;
    }

    public boolean isHasAudio() {
        return hasAudio;
    }

    public int getOptionNumber() {
        return optionNumber;
    }

    public String getText() {
        return Text;
    }

    public String getQuestionDescription() {
        return questionDescription;
    }

    public int getMaxMarks() {
        return maxMarks;
    }

    public String getCorrectAnswerDescription() {
        return correctAnswerDescription;
    }

    public void setCorrectAnswerDescription(String correctAnswerDescription) {
        this.correctAnswerDescription = correctAnswerDescription;
    }

    public int getMinMarks() {
        return minMarks;
    }
}
