package com.synlabs.intscale.view.usertest;

import com.synlabs.intscale.entity.test.UserTest;
import com.synlabs.intscale.view.Response;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by India on 10/6/2017.
 */
public class UserTestResponse implements Response {
    private Long id;
    private Long userId;
    private String userName;
    private String gender;
    private boolean used;
    private Set<String> tests = new HashSet<>();
    private String testStatus;
    public UserTestResponse(UserTest userTest){
        if(userTest!=null){
        this.id=userTest.getId();
        userTest.getTests().forEach(test ->{
            this.tests.add(test.getName());
        });
        this.testStatus=userTest.getTestStatus();
        this.gender=userTest.getUserEmail()!=null?userTest.getUserEmail().getGender():"";
        this.used=userTest.isUsed();
        }
    }

    public Long getId() {
        return id;
    }

    public Set<String> getTests() {
        return tests;
    }

    public String getTestStatus() {
        return testStatus;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getGender() {
        return gender;
    }

    public boolean isUsed() {
        return used;
    }
}
