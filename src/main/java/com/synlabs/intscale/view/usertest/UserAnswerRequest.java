package com.synlabs.intscale.view.usertest;

import com.synlabs.intscale.entity.test.Answers;
import com.synlabs.intscale.entity.test.Question;
import com.synlabs.intscale.entity.test.UserAnswer;
import com.synlabs.intscale.jpa.QuestionRepository;
import com.synlabs.intscale.view.Request;
import com.synlabs.intscale.view.question.AnswerRequest;
import com.synlabs.intscale.view.question.QuestionRequest;

public class UserAnswerRequest implements Request
{
  private Long        questionId;
  private Long answerId;
  private AnswerRequest answers;
  private int           timeTaken;

  public Long getQuestion()
  {
    return questionId;
  }

  public void setQuestion(Long question)
  {
    this.questionId = question;
  }

  public Long getQuestionId()
  {
    return questionId;
  }

  public void setQuestionId(Long questionId)
  {
    this.questionId = questionId;
  }

  public AnswerRequest getAnswers()
  {
    return answers;
  }

  public void setAnswers(AnswerRequest answers)
  {
    this.answers = answers;
  }

  public int getTimeTaken()
  {
    return timeTaken;
  }

  public void setTimeTaken(int timeTaken)
  {
    this.timeTaken = timeTaken;
  }

    public Long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(Long answerId) {
        this.answerId = answerId;
    }

    public UserAnswer toEntity()
  {
    return toEntity(null);
  }

  public UserAnswer toEntity(UserAnswer userAnswer)
  {
    if (userAnswer == null)
    {
      userAnswer = new UserAnswer();
    }
    userAnswer.setAnswerId(this.answerId);
    userAnswer.setTimeTaken(this.timeTaken);
    userAnswer.setDescription(this.answers.getDescription());
    userAnswer.setMarks(this.answers.getMarks());
    userAnswer.setHasText(this.answers.isHasText());
    userAnswer.setHasAudio(this.answers.isHasAudio());
    userAnswer.setHasImage(this.answers.isHasImage());
    userAnswer.setOptionNumber(this.answers.getOptionNumber());
    userAnswer.setText(this.answers.getText());
    return userAnswer;
  }
}