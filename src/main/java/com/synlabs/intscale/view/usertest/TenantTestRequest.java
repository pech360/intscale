package com.synlabs.intscale.view.usertest;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.synlabs.intscale.view.Request;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * Created by itrs on 9/20/2017.
 */
public class TenantTestRequest implements Request
{
  private Long[] userIds;
  private Long[] testIds;
  private String email;
  private String[] testNames;
  private int numberOfAttempt;
  private String pilotName;
  public Long[] getUserIds()
  {
    return userIds;
  }

  public Long[] getTestIds()
  {
    return testIds;
  }

  public int getNumberOfAttempt()
  {
    return numberOfAttempt;
  }

  public String getEmail() {
    return email;
  }

  public String[] getTestNames() {
    return testNames;
  }

  public String getPilotName() {
    return pilotName;
  }
}
