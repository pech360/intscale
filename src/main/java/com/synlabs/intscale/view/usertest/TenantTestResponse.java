package com.synlabs.intscale.view.usertest;

import com.synlabs.intscale.entity.test.Product;
import com.synlabs.intscale.entity.test.TenantTestAllotment;
import com.synlabs.intscale.view.ProductResponse;
import com.synlabs.intscale.view.Response;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by India on 9/29/2017.
 */
public class TenantTestResponse implements Response {
    private Long id;
    private Long userId;
    private String userName;
    private String email;
    private int testCount;
    private int usedTestCount;
    private int availableTestCount;
    private Date validTillDate;
    private String pilotName;
    private Set<String> tests=new HashSet<>();
    private List<ProductResponse> products = new ArrayList<>();
    public TenantTestResponse(TenantTestAllotment testAllotment){
        if(testAllotment!=null){
        this.id=testAllotment.getId();
        this.testCount=testAllotment.getTestCount();
        this.usedTestCount=testAllotment.getUsedTestCount();
        this.availableTestCount=testAllotment.getAvailableTestCount();
        this.validTillDate=testAllotment.getValidTillDate();
        this.products.addAll(testAllotment.getProducts().stream().map(ProductResponse::new).collect(Collectors.toList()));
        testAllotment.getTests().forEach(test -> {
            this.tests.add(test.getName());
        });
    }}

    public Long getId() {
        return id;
    }

    public int getTestCount() {
        return testCount;
    }

    public int getUsedTestCount() {
        return usedTestCount;
    }

    public int getAvailableTestCount() {
        return availableTestCount;
    }

    public Set<String> getTests() {
        return tests;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public Date getValidTillDate() {
        return validTillDate;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPilotName() {
        return pilotName;
    }

    public List<ProductResponse> getProducts() {
        return products;
    }
}
