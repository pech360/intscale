package com.synlabs.intscale.view.usertest;

import com.synlabs.intscale.entity.test.TestResult;
import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.test.UserAnswer;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.view.Response;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ResultResponse implements Response
{
  private Long       id;
  private Long       userId;
  private Long       testId;
  private String     primaryReportText;
  private String     secondaryReportText;
  private String     testName;
  private Date       testTakenDate;
  private BigDecimal infrequency;
  private BigDecimal acquiescence;
  private Integer    infrequencyRawScore;
  private Integer    acquiescenceRawScore;
  private List<UserAnswerResponse> userAnswerResponses = new ArrayList<>();

  public ResultResponse(TestResult testResult)
  {
    if (testResult == null)
    {
      return;
    }
    this.id = testResult.getId();
    this.userId = testResult.getUser().getId();
    this.testId = testResult.getTest().getId();
    this.testName = testResult.getTest().getName();
    this.primaryReportText = testResult.getTest().getPrimaryReportText();
    this.secondaryReportText = testResult.getTest().getSecondaryReportText();
    this.testTakenDate = testResult.getTestFinishAt();
    List<UserAnswer> userAnswers = testResult.getUserAnswers();
    for (UserAnswer userAnswer : userAnswers)
    {
      this.userAnswerResponses.add(new UserAnswerResponse(userAnswer));
    }
  }

  public ResultResponse()
  {
  }

  public Long getId()
  {
    return id;
  }

  public Long getUserId()
  {
    return userId;
  }

  public Long getTestId()
  {
    return testId;
  }

  public String getTestName()
  {
    return testName;
  }

  public List<UserAnswerResponse> getUserAnswerResponses()
  {
    return userAnswerResponses;
  }

  public String getPrimaryReportText()
  {
    return primaryReportText;
  }

  public String getSecondaryReportText()
  {
    return secondaryReportText;
  }

  public Date getTestTakenDate()
  {
    return testTakenDate;
  }

  public BigDecimal getInfrequency()
  {
    return infrequency;
  }

  public BigDecimal getAcquiescence()
  {
    return acquiescence;
  }

  public void setInfrequency(BigDecimal infrequency)
  {
    this.infrequency = infrequency;
  }

  public void setAcquiescence(BigDecimal acquiescence)
  {
    this.acquiescence = acquiescence;
  }

  public Integer getInfrequencyRawScore()
  {
    return infrequencyRawScore;
  }

  public void setInfrequencyRawScore(Integer infrequencyRawScore)
  {
    this.infrequencyRawScore = infrequencyRawScore;
  }

  public Integer getAcquiescenceRawScore()
  {
    return acquiescenceRawScore;
  }

  public void setAcquiescenceRawScore(Integer acquiescenceRawScore)
  {
    this.acquiescenceRawScore = acquiescenceRawScore;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public void setUserId(Long userId)
  {
    this.userId = userId;
  }

  public void setTestId(Long testId)
  {
    this.testId = testId;
  }

  public void setPrimaryReportText(String primaryReportText)
  {
    this.primaryReportText = primaryReportText;
  }

  public void setSecondaryReportText(String secondaryReportText)
  {
    this.secondaryReportText = secondaryReportText;
  }

  public void setTestName(String testName)
  {
    this.testName = testName;
  }

  public void setTestTakenDate(Date testTakenDate)
  {
    this.testTakenDate = testTakenDate;
  }

  public void setUserAnswerResponses(List<UserAnswerResponse> userAnswerResponses)
  {
    this.userAnswerResponses = userAnswerResponses;
  }
}
