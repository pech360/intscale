package com.synlabs.intscale.view.usertest;

import com.synlabs.intscale.view.Request;

/**
 * Created by India on 10/6/2017.
 */
public class UserTestRequest implements Request {
    private Long[] userIds;
    private boolean reAssignTest;

    public Long[] getUserIds() {
        return userIds;
    }

    public boolean isReAssignTest() {
        return reAssignTest;
    }
}
