package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.ProductTenant;

public class TenantProductResponse implements Response {
    private Long id;
    private Long tenantId;
    private String product;
    private String tenant;
    public TenantProductResponse(ProductTenant productTenant){
        this.id=productTenant.getId();
        this.tenantId=productTenant.getDomain().getId();
        this.product=productTenant.getProduct().getName();
        this.tenant=productTenant.getDomain().getName();
    }
    public Long getId() {
        return id;
    }

    public String getProduct() {
        return product;
    }

    public String getTenant() {
        return tenant;
    }

    public Long getTenantId() {
        return tenantId;
    }
}
