package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.test.Feedback;

/**
 * Created by itrs on 9/7/2017.
 */
public class FeedbackRequest implements Request
{
  private Long id;
  private Long categoryId;
  private int marksMin;
  private int marksMax;
  private String expertAnaylsis;
  private String developementPlan;

  public Feedback toEntity(){return toEntity(new Feedback());}
  public Feedback toEntity(Feedback feedback){
    feedback.setMarksMin(this.marksMin);
    feedback.setMarksMax(this.marksMax);
    feedback.setExpertAnaylsis(this.expertAnaylsis);
    feedback.setDevelopementPlan(this.developementPlan);
    return feedback;
  }
  public int getMarksMin()
  {
    return marksMin;
  }

  public int getMarksMax()
  {
    return marksMax;
  }

  public String getExpertAnaylsis()
  {
    return expertAnaylsis;
  }

  public String getDevelopementPlan()
  {
    return developementPlan;
  }

  public Long getId()
  {
    return id;
  }

  public Long getCategoryId()
  {
    return categoryId;
  }
}
