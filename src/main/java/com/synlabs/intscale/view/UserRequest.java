package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.AcademicPerformance;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.util.StringUtil;
import com.synlabs.intscale.view.threesixty.StakeholderRequest;

import java.util.*;

public class UserRequest implements Request {
	private Long id;
	private String username;
	private String name;
	private String userType;
	private String email;
	private String gender;
	private String grade;
	private String higherEducation;
	// @JsonFormat(pattern = "YYYY-MM-dd")
	private Date dob;
	private String language;
	private String designation;
	private String industry;
	private String orgName;
	private String img;
	private String interest[];
	private boolean active;
	private String password;
	private String school;
	private Long tenant;
	private Set<String> roles = new HashSet<>();
	private String academicScore;
	private String supw;
	private Boolean threeSixtyEnabled;
	private Boolean enableAimReport;
	private String stream;
	private List<StakeholderRequest> stakeholders = new ArrayList<>();
	private String city;
	private Boolean ftlCount; // change
	private String contactNumber;// change
	private List<AcademicPerformanceRequest> academicPerformances = new ArrayList<>();

	public void setId(Long id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public void setInterest(String[] interest) {
		this.interest = interest;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public void setTenant(Long tenant) {
		this.tenant = tenant;
	}

	public void setRoles(Set<String> roles) {
		this.roles = roles;
	}

	public void setAcademicScore(String academicScore) {
		this.academicScore = academicScore;
	}

	public void setSupw(String supw) {
		this.supw = supw;
	}

	public Boolean getThreeSixtyEnabled() {
		return threeSixtyEnabled;
	}

	public void setThreeSixtyEnabled(Boolean threeSixtyEnabled) {
		this.threeSixtyEnabled = threeSixtyEnabled;
	}

	public List<StakeholderRequest> getStakeholders() {
		return stakeholders;
	}

	public void setStakeholders(List<StakeholderRequest> stakeholders) {
		this.stakeholders = stakeholders;
	}

	public void setFtlCount(Boolean ftlCount) {
		this.ftlCount = ftlCount;
	}

	public Long getId() {
		return id;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public String getUsername() {
		return username;
	}

	public String getName() {
		return name;
	}

	public String getUserType() {
		return userType;
	}

	public String getEmail() {
		return email;
	}

	public String getGender() {
		return gender;
	}

	public Date getDob() {
		return dob;
	}

	public String getLanguage() {
		return language;
	}

	public String getDesignation() {
		return designation;
	}

	public String getIndustry() {
		return industry;
	}

	public String getOrgName() {
		return orgName;
	}

	public String getImg() {
		return img;
	}

	public String[] getInterest() {
		return interest;
	}

	public boolean isActive() {
		return active;
	}

	public Set<String> getRoles() {
		return roles;
	}

	public String getPassword() {
		return password;
	}

	public String getGrade() {
		return grade;
	}

	public String getSchool() {
		return school;
	}

	public Long getTenant() {
		return tenant;
	}

	public String getAcademicScore() {
		return academicScore;
	}

	public String getSupw() {
		return supw;
	}

	public String getStream() {
		return stream;
	}

	public Boolean getEnableAimReport() {
		return enableAimReport;
	}

	public String getCity() {
		return city;
	}

	public List<AcademicPerformanceRequest> getAcademicPerformances() {
		return academicPerformances;
	}

	public String getHigherEducation() {
		return higherEducation;
	}

	public Boolean getFtlCount() { // change
		return ftlCount;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public User toEntity(User user) {
		if (user == null) {
			user = new User();
			user.setEmail(this.email);
		}
		user.setUsername(this.username);
		user.setDesignation(this.designation);
		user.setName(StringUtil.capitalizeWord(this.name));
		user.setIndustry(this.industry);
		user.setGender(this.gender);
		user.setGrade(this.grade);
		user.setUserType(this.userType);
		user.setLanguage(this.language);
		user.setDob(this.dob);
		user.setSchoolName(this.school);
		user.setActive(true);
		user.setAcademicScore(this.academicScore);
		user.setSupw(this.supw);
		if (user.getGrade() != null) {
			if (user.getGrade().equals("11") || user.getGrade().equals("12")) {
				user.setStream(this.stream);
			} else {
				user.setStream("");
			}
		}
		user.setEnableAimReport(this.enableAimReport);
		if (this.interest != null) {
			user.setInterests(String.join(", ", this.interest));
		}
		user.setCity(this.city);
		user.setHigherEducation(higherEducation);
		user.setFtlCount(this.ftlCount); // change
		user.setContactNumber(this.contactNumber);//change

		return user;
	}

}
