package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.Teacher;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by India on 1/3/2018.
 */
public class TeacherRequest implements Request
{
  private Long                             id;
  private String                           name;
  private String                           email;
  private String                           gender;
  private Long                             school;
  private String                           education;
  private String                           speciality;
  private Long                             typeId;
  private boolean                          classTeacher;
  private boolean                          subjectTeacher;
  private Long                             userId;
  private String                           section;
  private Long                             subject;
  private String                           grade;
  private boolean                          principal;
  private boolean                          hod;
  private List<GradeSectionSubjectRequest> gradeSectionSubjects = new ArrayList<>();

  public Teacher toEntity(Teacher st)
  {
    if (st == null)
    {
      st = new Teacher();
    }
    st.setName(this.name);
    st.setEmail(this.email);
    st.setGender(this.gender);
    st.setEducation(this.education);
    st.setSpeciality(this.speciality);
    st.setClassTeacher(this.classTeacher);
    st.setSubjectTeacher(this.subjectTeacher);
    st.setGrade(this.grade);
    st.setSection(this.section);
    st.setPrincipal(this.principal);
    st.setSubject(this.subject);
    st.setHod(this.hod);
    return st;
  }

  public Long getId()
  {
    return id;
  }

  public String getName()
  {
    return name;
  }

  public String getEmail()
  {
    return email;
  }

  public String getGender()
  {
    return gender;
  }

  public Long getSchool()
  {
    return school;
  }

  public String getEducation()
  {
    return education;
  }

  public String getSpeciality()
  {
    return speciality;
  }

  public Long getTypeId()
  {
    return typeId;
  }

  public boolean isClassTeacher()
  {
    return classTeacher;
  }

  public boolean isSubjectTeacher()
  {
    return subjectTeacher;
  }

  public Long getUserId()
  {
    return userId;
  }

  public String getSection()
  {
    return section;
  }

  public List<GradeSectionSubjectRequest> getGradeSectionSubjects()
  {
    return gradeSectionSubjects;
  }

  public Long getSubject()
  {
    return subject;
  }

  public String getGrade()
  {
    return grade;
  }

  public boolean isPrincipal()
  {
    return principal;
  }

  public boolean isHod()
  {
    return hod;
  }
}
