package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.Tenant;

/**
 * Created by itrs on 9/19/2017.
 */
public class TenantResponse implements Response
{
  private Long id;
  private String name;
  private String domain;
  private String description;
  private String address;
  private int schoolLimit;
  public TenantResponse(Tenant tenant){
    this.id=tenant.getId();
    this.name=tenant.getName();
    this.domain=tenant.getSubDomain();
    this.description=tenant.getDescription();
    this.address=tenant.getAddress();
    this.schoolLimit=tenant.getSchoolLimit();
  }
  public Long getId()
  {
    return id;
  }

  public String getName()
  {
    return name;
  }

  public String getDomain() {
    return domain;
  }

  public String getDescription() {
    return description;
  }

  public String getAddress() {
    return address;
  }

  public int getSchoolLimit() {
    return schoolLimit;
  }
}
