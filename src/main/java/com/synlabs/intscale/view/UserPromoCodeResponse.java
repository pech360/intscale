package com.synlabs.intscale.view;

import java.util.Date;

import com.synlabs.intscale.entity.paymentGateway.TransactionEntry;
import com.synlabs.intscale.entity.user.User;
//LNT
public class UserPromoCodeResponse implements Response {

	private String username;
	private String name;
	private String email;
	private String contactNumber;
	private String grade;
	private String productPurchased;
	private Date date;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getProductPurchased() {
		return productPurchased;
	}
	public void setProductPurchased(String productPurchased) {
		this.productPurchased = productPurchased;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public UserPromoCodeResponse() {
		
	}
	public UserPromoCodeResponse (User user) {
		user = user == null ? new User(): user;
		this.contactNumber = user.getContactNumber();
		this.email = user.getEmail();
		this.grade = user.getGrade();
		this.name = user.getName();
		this.username = user.getUsername();
		
	}
}
