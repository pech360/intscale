package com.synlabs.intscale.view;

import com.synlabs.intscale.util.LongObfuscator;

public interface Response
{
  default Long mask(final Long number)
  {
    return number != null ? LongObfuscator.INSTANCE.obfuscate(number) : null;
  }
}
