package com.synlabs.intscale.view.reportShare;

import com.synlabs.intscale.view.Request;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public class ReportShareRequest implements Request {
   public List<Long> userIds;
   public String reportType;
   public String cc;
   public String emailId;

   public List<Long> getUserIds() {
      return userIds;
   }

   public void setUserIds(List<Long> userIds) {
      this.userIds = userIds;
   }

   public String getReportType() {
      return reportType;
   }

   public void setReportType(String reportType) {
      this.reportType = reportType;
   }

   public String getCc() {
      return cc;
   }

   public void setCc(String cc) {
      this.cc = cc;
   }

   public String getEmailId() {
      return emailId;
   }

   public void setEmailId(String emailId) {
      this.emailId = emailId;
   }
}
