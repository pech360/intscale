package com.synlabs.intscale.view.report;

import com.synlabs.intscale.entity.Report.Norm;
import com.synlabs.intscale.view.Request;
import com.synlabs.intscale.view.Response;

import java.math.BigDecimal;

public class NormResponse implements Response
{
  private Long       id;
  private String     language;
  private BigDecimal juniorMeanScore;
  private BigDecimal juniorStandardDeviation;
  private BigDecimal seniorMeanScore;
  private BigDecimal seniorStandardDeviation;
  private BigDecimal secondaryMeanScore;
  private BigDecimal secondaryStandardDeviation;

  public NormResponse(Norm norm)
  {
    this.id = norm.getId();
    this.language = norm.getLanguage();
    this.juniorMeanScore = norm.getJuniorMeanScore();
    this.juniorStandardDeviation = norm.getJuniorStandardDeviation();
    this.seniorMeanScore = norm.getSeniorMeanScore();
    this.seniorStandardDeviation = norm.getSeniorStandardDeviation();
    this.secondaryMeanScore = norm.getSecondaryMeanScore();
    this.secondaryStandardDeviation = norm.getSecondaryStandardDeviation();
  }

  public NormResponse()
  {

  }

  public Long getId()
  {
    return id;
  }

  public String getLanguage()
  {
    return language;
  }

  public BigDecimal getJuniorMeanScore()
  {
    return juniorMeanScore;
  }

  public BigDecimal getJuniorStandardDeviation()
  {
    return juniorStandardDeviation;
  }

  public BigDecimal getSeniorMeanScore()
  {
    return seniorMeanScore;
  }

  public BigDecimal getSeniorStandardDeviation()
  {
    return seniorStandardDeviation;
  }

  public BigDecimal getSecondaryMeanScore()
  {
    return secondaryMeanScore;
  }

  public BigDecimal getSecondaryStandardDeviation()
  {
    return secondaryStandardDeviation;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public void setJuniorMeanScore(BigDecimal juniorMeanScore) {
    this.juniorMeanScore = juniorMeanScore;
  }

  public void setJuniorStandardDeviation(BigDecimal juniorStandardDeviation) {
    this.juniorStandardDeviation = juniorStandardDeviation;
  }

  public void setSeniorMeanScore(BigDecimal seniorMeanScore) {
    this.seniorMeanScore = seniorMeanScore;
  }

  public void setSeniorStandardDeviation(BigDecimal seniorStandardDeviation) {
    this.seniorStandardDeviation = seniorStandardDeviation;
  }

  public void setSecondaryMeanScore(BigDecimal secondaryMeanScore) {
    this.secondaryMeanScore = secondaryMeanScore;
  }

  public void setSecondaryStandardDeviation(BigDecimal secondaryStandardDeviation) {
    this.secondaryStandardDeviation = secondaryStandardDeviation;
  }
}
