package com.synlabs.intscale.view.report;

import com.synlabs.intscale.entity.Report.AimReportContent;
import com.synlabs.intscale.enums.AimReportContentType;
import com.synlabs.intscale.enums.RangeValue;
import com.synlabs.intscale.view.Request;

import java.math.BigDecimal;

public class AimReportContentRequest implements Request
{

  private AimReportContentType type;
  private String     name;
  private BigDecimal rawScore;
  private BigDecimal standardScore;
  private RangeValue scoreLabel;
  private Integer    scoreLabelRating;
  private String     reportSectionName;
  private Integer    sequence;

  public AimReportContentType getType()
  {
    return type;
  }

  public String getName()
  {
    return name;
  }

  public BigDecimal getRawScore()
  {
    return rawScore;
  }

  public BigDecimal getStandardScore()
  {
    return standardScore;
  }

  public RangeValue getScoreLabel()
  {
    return scoreLabel;
  }

  public Integer getScoreLabelRating()
  {
    return scoreLabelRating;
  }

  public String getReportSectionName()
  {
    return reportSectionName;
  }

  public Integer getSequence()
  {
    return sequence;
  }



  public AimReportContent toEntity(AimReportContent aimReportContent){
    if(aimReportContent==null){
      aimReportContent = new AimReportContent();
    }
    aimReportContent.setType(this.getType());
    aimReportContent.setName(this.getName());
    aimReportContent.setRawScore(this.getRawScore());
    aimReportContent.setStandardScore(this.getStandardScore());
    aimReportContent.setReportSectionName(this.reportSectionName);
    aimReportContent.setScoreLabel(this.getScoreLabel());
    aimReportContent.setScoreLabelRating(this.getScoreLabelRating());
    aimReportContent.setSequence(this.getSequence());
    return aimReportContent;
  }

}