package com.synlabs.intscale.view.report;

import com.synlabs.intscale.entity.Report.AimExpertItem;
import com.synlabs.intscale.view.Response;

public class AimExpertItemResponse implements Response
{
  private Long   id;
  private String category;
  private String reportSection;
  private int    goalStandard;

  public AimExpertItemResponse(AimExpertItem aimExpertItem)
  {
    this.id = aimExpertItem.getId();
    this.category = aimExpertItem.getCategory().getName();
    this.reportSection = aimExpertItem.getReportSection().getName();
    this.goalStandard = aimExpertItem.getGoalStandard();
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getCategory()
  {
    return category;
  }

  public void setCategory(String category)
  {
    this.category = category;
  }

  public String getReportSection()
  {
    return reportSection;
  }

  public void setReportSection(String reportSection)
  {
    this.reportSection = reportSection;
  }

  public int getGoalStandard()
  {
    return goalStandard;
  }

  public void setGoalStandard(int goalStandard)
  {
    this.goalStandard = goalStandard;
  }
}
