package com.synlabs.intscale.view.report;

import com.synlabs.intscale.entity.Report.Subject;
import com.synlabs.intscale.view.Request;

import java.math.BigDecimal;

public class SubjectRequest implements Request
{
  private Long id;
  private String name;
  private String career;
  private BigDecimal weight;

  public Subject toEntity(Subject subject){
    if(subject==null){
      subject = new Subject();
    }
    subject.setWeight(this.getWeight());
    return subject;
  }

  public Long getId()
  {
    return id;
  }

  public String getName()
  {
    return name;
  }

  public String getCareer()
  {
    return career;
  }

  public BigDecimal getWeight()
  {
    return weight;
  }
}

