package com.synlabs.intscale.view.report;

import com.synlabs.intscale.entity.Report.ReportSection;
import com.synlabs.intscale.util.StringUtil;
import com.synlabs.intscale.view.Request;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class ReportSectionRequest implements Request {
    private Long id;
    private String name;
    private String description;
    private BigDecimal juniorMeanScore;
    private BigDecimal juniorStandardDeviation;
    private BigDecimal seniorMeanScore;
    private BigDecimal seniorStandardDeviation;
    private BigDecimal secondaryMeanScore;
    private BigDecimal secondaryStandardDeviation;
    private Integer sequence;
    private Set<Long> idsOfTestForEvaluation = new HashSet<>();

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getJuniorMeanScore() {
        return juniorMeanScore;
    }

    public BigDecimal getJuniorStandardDeviation() {
        return juniorStandardDeviation;
    }

    public BigDecimal getSeniorMeanScore() {
        return seniorMeanScore;
    }

    public BigDecimal getSeniorStandardDeviation() {
        return seniorStandardDeviation;
    }

    public BigDecimal getSecondaryMeanScore()
    {
        return secondaryMeanScore;
    }

    public BigDecimal getSecondaryStandardDeviation()
    {
        return secondaryStandardDeviation;
    }

    public Integer getSequence()
    {
        return sequence;
    }

    public Set<Long> getIdsOfTestForEvaluation() {
        return idsOfTestForEvaluation;
    }

    public ReportSection toEntity(ReportSection reportSection) {
        if (reportSection == null) {
            reportSection = new ReportSection();
        }
        int counter = 1;
        StringBuilder ids = new StringBuilder();
        reportSection.setName(this.name);
        reportSection.setDescription(this.description);
        reportSection.setJuniorMeanScore(this.juniorMeanScore);
        reportSection.setJuniorStandardDeviation(this.juniorStandardDeviation);
        reportSection.setSeniorMeanScore(this.seniorMeanScore);
        reportSection.setSeniorStandardDeviation(this.seniorStandardDeviation);
        reportSection.setSecondaryMeanScore(this.secondaryMeanScore);
        reportSection.setSecondaryStandardDeviation(this.secondaryStandardDeviation);
        reportSection.setSequence(this.sequence);

        if (this.getIdsOfTestForEvaluation().size() != 0) {

            for (Long id : this.getIdsOfTestForEvaluation()) {
                ids.append(String.valueOf(id));
                if (counter != this.getIdsOfTestForEvaluation().size()) {
                    ids.append(",");
                    counter++;
                }
            }
            reportSection.setIdsOfTestForEvaluation(ids.toString());
        }

        return reportSection;
    }
}
