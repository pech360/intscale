package com.synlabs.intscale.view.report;

import com.synlabs.intscale.entity.Report.Report;
import com.synlabs.intscale.view.Request;

import java.util.HashSet;
import java.util.Set;

public class ReportRequest implements Request {
    private Long id;
    private String name;
    private String description;
    private Set<Long> idsOfReportSection = new HashSet<>();

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Set<Long> getIdsOfReportSection() {
        return idsOfReportSection;
    }

    public Report toEntity(Report report) {
        if (report == null) {
            report = new Report();
        }

        report.setName(this.name);
        report.setDescription(this.description);
        return report;
    }
}
