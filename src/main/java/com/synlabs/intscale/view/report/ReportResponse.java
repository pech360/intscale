package com.synlabs.intscale.view.report;

import com.synlabs.intscale.entity.Report.Report;
import com.synlabs.intscale.view.Response;
import com.synlabs.intscale.view.TestDescriptionResponse;

import java.util.ArrayList;
import java.util.List;

public class ReportResponse implements Response {
    private Long id;
    private String name;
    private String description;
    private List<ReportSectionResponse> reportSections = new ArrayList<>();


    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<ReportSectionResponse> getReportSections() {
        return reportSections;
    }

    public ReportResponse(Report report){
        this.id = report.getId();
        this.name = report.getName();
        this.description = report.getDescription();
        report.getReportSections().forEach(reportSection -> {
            this.reportSections.add(new ReportSectionResponse(reportSection));
        });
    }
}
