package com.synlabs.intscale.view.report;

import com.synlabs.intscale.entity.Report.ScoreRange;
import com.synlabs.intscale.enums.RangeValue;
import com.synlabs.intscale.view.Request;

import java.math.BigDecimal;

public class ScoreRangeRequest implements Request {
    private BigDecimal startFrom;
    private BigDecimal endAt;
    private String rangeOf;
    private String text;
    private String innateTalent;
    private String strength;
    private String weakness;
    private String learningStyle;
    private String learningStyleInference;
    private String learningStyleRecommendation;
    private String developmentPlanForStrength;
    private String developmentPlanForWeakness;

    public BigDecimal getStartFrom() {
        return startFrom;
    }

    public BigDecimal getEndAt() {
        return endAt;
    }

    public String getRangeOf() {
        return rangeOf;
    }

    public String getText() {
        return text;
    }

    public String getInnateTalent() {
        return innateTalent;
    }

    public String getStrength() {
        return strength;
    }

    public String getWeakness() {
        return weakness;
    }

    public String getLearningStyle() {
        return learningStyle;
    }

    public String getLearningStyleInference()
    {
        return learningStyleInference;
    }

    public String getLearningStyleRecommendation()
    {
        return learningStyleRecommendation;
    }

    public String getDevelopmentPlanForStrength()
    {
        return developmentPlanForStrength;
    }

    public String getDevelopmentPlanForWeakness()
    {
        return developmentPlanForWeakness;
    }

    public ScoreRange toEntity(ScoreRange scoreRange) {
        scoreRange.setStartFrom(this.startFrom);
        scoreRange.setEndAt(this.endAt);
        scoreRange.setRangeOf(RangeValue.valueOf(this.rangeOf));
        scoreRange.setText(this.text);
        scoreRange.setInnateTalent(this.innateTalent);
        scoreRange.setStrength(this.strength);
        scoreRange.setWeakness(this.weakness);
        scoreRange.setLearningStyle(this.learningStyle);
        scoreRange.setLearningStyleInference(this.learningStyleInference);
        scoreRange.setLearningStyleRecommendation(this.learningStyleRecommendation);
        scoreRange.setDevelopmentPlanForStrength(this.developmentPlanForStrength);
        scoreRange.setDevelopmentPlanForWeakness(this.developmentPlanForWeakness);
        return scoreRange;
    }
}
