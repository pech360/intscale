package com.synlabs.intscale.view.report;

import com.synlabs.intscale.entity.Report.Subject;
import com.synlabs.intscale.view.Response;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class SubjectResponse implements Response
{
  private Long id;
  private String  name;
  private String                      career;
  private BigDecimal                  weight;
  private String                      type;
  private List<AimExpertItemResponse> aimExpertItems = new ArrayList<>();


  public SubjectResponse(Subject subject)
  {
    this.id = subject.getId();
    this.name = subject.getAimExpertInventory().getName();
    this.type = subject.getAimExpertInventory().getType();
    this.weight = subject.getWeight();
    subject.getAimExpertInventory().getAimExpertItems().forEach(aimExpertItem -> {
      this.aimExpertItems.add(new AimExpertItemResponse(aimExpertItem));
    });
  }

  public Long getId()
  {
    return id;
  }

  public String getName()
  {
    return name;
  }

  public String getCareer()
  {
    return career;
  }

  public BigDecimal getWeight()
  {
    return weight;
  }

  public String getType()
  {
    return type;
  }

  public List<AimExpertItemResponse> getAimExpertItems()
  {
    return aimExpertItems;
  }
}

