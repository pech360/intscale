package com.synlabs.intscale.view.report;

import com.synlabs.intscale.entity.Report.AimExpertInventory;
import com.synlabs.intscale.entity.Report.Subject;
import com.synlabs.intscale.view.Response;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AimExpertInventoryResponse implements Response
{
  private Long                        id;
  private String                      name;
  private String                      displayName;
  private String                      description;
  private String                      type;
  private String                      summary;
  private String                      tasks;
  private String                      scope;
  private String                      topRecruiters;
  private String                      education;
  private String                      topCollegesOfIndia;
  private String                      topCollegesOfAbroad;
  private String                      imageName;
  private String                      streams;
  private List<AimExpertItemResponse> aimExpertItems = new ArrayList<>();
  private List<SubjectResponse>       subjects       = new ArrayList<>();
  private List<SubjectResponse>        hobbies        = new ArrayList<>();
  private String                      leisures[];

  public AimExpertInventoryResponse(AimExpertInventory aimExpertInventory)
  {
    this.id = aimExpertInventory.getId();
    this.name = aimExpertInventory.getName();
    this.displayName = aimExpertInventory.getDisplayName();
    this.description = aimExpertInventory.getDescription();
    this.type = aimExpertInventory.getType();
    this.summary = aimExpertInventory.getSummary();
    this.tasks = aimExpertInventory.getTasks();
    this.scope = aimExpertInventory.getScope();
    this.topRecruiters = aimExpertInventory.getTopRecruiters();
    this.education = aimExpertInventory.getEducation();
    this.topCollegesOfIndia = aimExpertInventory.getTopCollegesOfIndia();
    this.topCollegesOfAbroad = aimExpertInventory.getTopCollegesOfAbroad();
    this.imageName = aimExpertInventory.getImageName();
    this.streams = aimExpertInventory.getStreams();
    aimExpertInventory.getAimExpertItems().forEach(aimExpertItem -> {
      this.aimExpertItems.add(new AimExpertItemResponse(aimExpertItem));
    });
    aimExpertInventory.getSubjects().forEach(subject -> {
      this.subjects.add(new SubjectResponse(subject));
    });
    aimExpertInventory.getHobbies().forEach(hobby -> {
      this.hobbies.add(new SubjectResponse(hobby));
    });
    this.leisures = aimExpertInventory.getLeisures();
  }

  public Long getId()
  {
    return id;
  }

  public String getName()
  {
    return name;
  }

  public String getDisplayName()
  {
    return displayName;
  }

  public String getDescription()
  {
    return description;
  }

  public String getType()
  {
    return type;
  }

  public String getSummary()
  {
    return summary;
  }

  public String getTasks()
  {
    return tasks;
  }

  public String getScope()
  {
    return scope;
  }

  public String getTopRecruiters()
  {
    return topRecruiters;
  }

  public String getEducation()
  {
    return education;
  }

  public String getTopCollegesOfIndia()
  {
    return topCollegesOfIndia;
  }

  public String getTopCollegesOfAbroad()
  {
    return topCollegesOfAbroad;
  }

  public String getImageName()
  {
    return imageName;
  }

  public String getStreams()
  {
    return streams;
  }

  public List<AimExpertItemResponse> getAimExpertItems()
  {
    return aimExpertItems;
  }

  public List<SubjectResponse> getSubjects()
  {
    return subjects;
  }

  public List<SubjectResponse> getHobbies()
  {
    return hobbies;
  }

  public String[] getLeisures()
  {
    return leisures;
  }
}
