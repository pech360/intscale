package com.synlabs.intscale.view.report;

import com.synlabs.intscale.view.Request;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserReportStatusRequest implements Request
{
  private Long                         userId;
  private Set<AimReportContentRequest> aimReportContentRequests = new HashSet<>();

  public Long getUserId()
  {
    return userId;
  }

  public Set<AimReportContentRequest> getAimReportContentRequests()
  {
    return aimReportContentRequests;
  }
}
