
package com.synlabs.intscale.view.report;

import com.synlabs.intscale.entity.Report.AimExpertInventory;
import com.synlabs.intscale.view.Request;

import java.util.ArrayList;
import java.util.List;

public class AimExpertInventoryRequest implements Request
{
  private Long                       id;
  private String                     name;
  private String                     displayName;
  private String                     description;
  private String                     type;
  private String                     summary;
  private String                     tasks;
  private String                     scope;
  private String                     topRecruiters;
  private String                     education;
  private String                     topCollegesOfIndia;
  private String                     topCollegesOfAbroad;
  private List<SubjectRequest>       subjects       = new ArrayList<>();
  private List<SubjectRequest>       hobbies       = new ArrayList<>();
  private List<AimExpertItemRequest> aimExpertItems = new ArrayList<>();
  private String[]                   leisures       = new String[3];

  public AimExpertInventory toEntity(AimExpertInventory aimExpertInventory)
  {
    if (aimExpertInventory == null)
    {
      aimExpertInventory = new AimExpertInventory();
    }
    aimExpertInventory.setId(this.id);
    aimExpertInventory.setName(this.name);
    aimExpertInventory.setDisplayName(this.displayName);
    aimExpertInventory.setDescription(this.description);
    aimExpertInventory.setType(this.type);
    aimExpertInventory.setSummary(this.summary);
    aimExpertInventory.setTasks(this.tasks);
    aimExpertInventory.setScope(this.scope);
    aimExpertInventory.setTopRecruiters(this.topRecruiters);
    aimExpertInventory.setEducation(this.education);
    aimExpertInventory.setTopCollegesOfIndia(this.topCollegesOfIndia);
    aimExpertInventory.setTopCollegesOfAbroad(this.topCollegesOfAbroad);
    aimExpertInventory.setLeisures(this.leisures);
    return aimExpertInventory;
  }

  public Long getId()
  {
    return id;
  }

  public String getName()
  {
    return name;
  }

  public String getDisplayName()
  {
    return displayName;
  }

  public String getDescription()
  {
    return description;
  }

  public String getType()
  {
    return type;
  }

  public String getSummary()
  {
    return summary;
  }

  public String getTasks()
  {
    return tasks;
  }

  public String getScope()
  {
    return scope;
  }

  public String getTopRecruiters()
  {
    return topRecruiters;
  }

  public String getEducation()
  {
    return education;
  }

  public String getTopCollegesOfIndia()
  {
    return topCollegesOfIndia;
  }

  public String getTopCollegesOfAbroad()
  {
    return topCollegesOfAbroad;
  }

  public List<SubjectRequest> getSubjects()
  {
    return subjects;
  }

  public List<SubjectRequest> getHobbies()
  {
    return hobbies;
  }

  public List<AimExpertItemRequest> getAimExpertItems()
  {
    return aimExpertItems;
  }

  public String[] getLeisures()
  {
    return leisures;
  }

  public void setLeisures(String[] leisures)
  {
    this.leisures = leisures;
  }

}
