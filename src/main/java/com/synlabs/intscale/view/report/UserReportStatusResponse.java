package com.synlabs.intscale.view.report;

import com.synlabs.intscale.entity.Report.Report;
import com.synlabs.intscale.entity.Report.UserReportStatus;
import com.synlabs.intscale.view.UserResponse;
import com.synlabs.intscale.view.threesixty.report.UserReport;

public class UserReportStatusResponse
{
  private Long id;
  private String username;
  private String email;
  private String name;
  private Long userId;
  private String school;
  private Long reportId;
  private String reportName;
  private String status;
  private String filename;

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getUsername()
  {
    return username;
  }

  public void setUsername(String username)
  {
    this.username = username;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public Long getUserId()
  {
    return userId;
  }

  public void setUserId(Long userId)
  {
    this.userId = userId;
  }

  public String getSchool()
  {
    return school;
  }

  public void setSchool(String school)
  {
    this.school = school;
  }

  public Long getReportId()
  {
    return reportId;
  }

  public void setReportId(Long reportId)
  {
    this.reportId = reportId;
  }

  public String getReportName()
  {
    return reportName;
  }

  public void setReportName(String reportName)
  {
    this.reportName = reportName;
  }

  public String getStatus()
  {
    return status;
  }

  public void setStatus(String status)
  {
    this.status = status;
  }

  public String getFilename()
  {
    return filename;
  }

  public void setFilename(String filename)
  {
    this.filename = filename;
  }

  public UserReportStatusResponse(UserReportStatus userReportStatus){
    this.id = userReportStatus.getId();
    this.username = userReportStatus.getUser().getUsername();
    this.email = userReportStatus.getUser().getEmail();
    this.name = userReportStatus.getUser().getName();
    this.userId = userReportStatus.getUser().getId();
    this.school = userReportStatus.getUser().getSchoolName();
    this.reportId = userReportStatus.getReportId();
    this.status =  userReportStatus.getStatus();
    this.filename =  userReportStatus.getFilename();
  }
}
