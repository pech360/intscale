package com.synlabs.intscale.view.report;

import com.synlabs.intscale.entity.Report.AimExpertItem;
import com.synlabs.intscale.view.Request;

public class AimExpertItemRequest implements Request
{
  private Long   id;
  private String category;
  private String reportSection;
  private int    goalStandard;

  public AimExpertItem toEntity(AimExpertItem aimExpertItem){
    if(aimExpertItem==null){
      aimExpertItem = new AimExpertItem();
    }

    aimExpertItem.setId(this.getId());
    aimExpertItem.setGoalStandard(this.goalStandard);
    return aimExpertItem;
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getCategory()
  {
    return category;
  }

  public void setCategory(String category)
  {
    this.category = category;
  }

  public String getReportSection()
  {
    return reportSection;
  }

  public void setReportSection(String reportSection)
  {
    this.reportSection = reportSection;
  }

  public int getGoalStandard()
  {
    return goalStandard;
  }

  public void setGoalStandard(int goalStandard)
  {
    this.goalStandard = goalStandard;
  }
}
