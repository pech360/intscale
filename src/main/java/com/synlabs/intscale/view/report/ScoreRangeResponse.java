package com.synlabs.intscale.view.report;

import com.synlabs.intscale.entity.Report.ScoreRange;
import com.synlabs.intscale.view.Response;

import java.math.BigDecimal;

public class ScoreRangeResponse implements Response {

    private BigDecimal startFrom;
    private BigDecimal endAt;
    private String rangeOf;
    private String text;
    private String innateTalent;
    private String strength;
    private String weakness;
    private String learningStyle;
    private String learningStyleInference;
    private String learningStyleRecommendation;
    private String developmentPlanForStrength;
    private String developmentPlanForWeakness;

    public BigDecimal getStartFrom() {
        return startFrom;
    }

    public BigDecimal getEndAt() {
        return endAt;
    }

    public String getRangeOf() {
        return rangeOf;
    }

    public String getText() {
        return text;
    }

    public String getInnateTalent() {
        return innateTalent;
    }

    public String getStrength() {
        return strength;
    }

    public String getWeakness() {
        return weakness;
    }

    public String getLearningStyle() {
        return learningStyle;
    }

    public String getLearningStyleInference()
    {
        return learningStyleInference;
    }

    public String getLearningStyleRecommendation()
    {
        return learningStyleRecommendation;
    }

    public String getDevelopmentPlanForStrength()
    {
        return developmentPlanForStrength;
    }

    public String getDevelopmentPlanForWeakness()
    {
        return developmentPlanForWeakness;
    }

    public ScoreRangeResponse(ScoreRange scoreRange) {
        this.startFrom = scoreRange.getStartFrom();
        this.endAt = scoreRange.getEndAt();
        this.rangeOf = scoreRange.getRangeOf().toString();
        this.text = scoreRange.getText();
        this.innateTalent = scoreRange.getInnateTalent();
        this.strength = scoreRange.getStrength();
        this.weakness = scoreRange.getWeakness();
        this.learningStyle = scoreRange.getLearningStyle();
        this.learningStyleInference = scoreRange.getLearningStyleInference();
        this.learningStyleRecommendation = scoreRange.getLearningStyleRecommendation();
        this.developmentPlanForStrength = scoreRange.getDevelopmentPlanForStrength();
        this.developmentPlanForWeakness = scoreRange.getDevelopmentPlanForWeakness();
    }
}
