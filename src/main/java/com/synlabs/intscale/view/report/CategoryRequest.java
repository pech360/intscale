package com.synlabs.intscale.view.report;

import com.synlabs.intscale.entity.Report.Category;
import com.synlabs.intscale.view.Request;
import com.synlabs.intscale.view.report.ScoreRangeRequest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by itrs on 7/25/2017.
 */
public class CategoryRequest implements Request {
    private Long id;
    private String name;
    private String displayName;
    private String description;
    private String color;
    private Category parent;
    private String categoryName;
    private Long parentId;
    private String highText;
    private String lowText;
    private BigDecimal juniorMeanScore;
    private BigDecimal juniorStandardDeviation;
    private BigDecimal seniorMeanScore;
    private BigDecimal seniorStandardDeviation;
    private Integer sequence;
    private String shortHighText;
    private String shortLowText;
    private String type;
    private BigDecimal secondaryMeanScore;
    private BigDecimal secondaryStandardDeviation;
    private List<ScoreRangeRequest> scoreRanges = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getColor() {
        return color;
    }

    public Category getParent() {
        return parent;
    }

    public Long getParentId() {
        return parentId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getHighText() {
        return highText;
    }

    public String getLowText() {
        return lowText;
    }

    public BigDecimal getJuniorMeanScore() {
        return juniorMeanScore;
    }

    public BigDecimal getJuniorStandardDeviation() {
        return juniorStandardDeviation;
    }

    public List<ScoreRangeRequest> getScoreRanges() {
        return scoreRanges;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Integer getSequence() {
        return sequence;
    }

    public String getShortHighText() {
        return shortHighText;
    }

    public String getShortLowText() {
        return shortLowText;
    }

    public BigDecimal getSeniorMeanScore() {
        return seniorMeanScore;
    }

    public BigDecimal getSeniorStandardDeviation() {
        return seniorStandardDeviation;
    }

    public BigDecimal getSecondaryMeanScore()
    {
        return secondaryMeanScore;
    }

    public BigDecimal getSecondaryStandardDeviation()
    {
        return secondaryStandardDeviation;
    }

    public String getType() {
        return type;
    }

    public Category toEntity(Category category) {
        if (category == null) {
            category = new Category();
        }
        category.setName(this.name);
        category.setDescription(this.description);
        category.setColor(this.color);
        category.setLowText(this.lowText);
        category.setHighText(this.highText);
        category.setJuniorMeanScore(this.juniorMeanScore);
        category.setJuniorStandardDeviation(this.juniorStandardDeviation);
        category.setSeniorMeanScore(this.seniorMeanScore);
        category.setSeniorStandardDeviation(this.seniorStandardDeviation);
        category.setSecondaryMeanScore(this.secondaryMeanScore);
        category.setSecondaryStandardDeviation(this.secondaryStandardDeviation);
        category.setDisplayName(this.displayName);
        category.setSequence(this.sequence);
        category.setShortHighText(this.shortHighText);
        category.setShortLowText(this.shortLowText);
        category.setType(this.type);
        return category;
    }

    public Category toEntity() {
        return toEntity(new Category());
    }
}




