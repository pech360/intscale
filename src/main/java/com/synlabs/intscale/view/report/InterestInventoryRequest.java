package com.synlabs.intscale.view.report;

import com.synlabs.intscale.entity.Report.InterestInventory;

public class InterestInventoryRequest {

    private Long id;
    private String interestCombination;
    private Integer region;
    private String description;
    private String shortDescription;
    private String learningStyle;
    private String learningStyleInference;
    private String learningStyleRecommendation;

    public String getInterestCombination() {
        return interestCombination;
    }

    public Integer getRegion() {
        return region;
    }

    public Long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public String getLearningStyle() {
        return learningStyle;
    }

    public String getLearningStyleInference()
    {
        return learningStyleInference;
    }

    public String getLearningStyleRecommendation()
    {
        return learningStyleRecommendation;
    }

    public InterestInventory toEntity(InterestInventory interestInventory){
        if(interestInventory==null){
            interestInventory = new InterestInventory();
        }
        interestInventory.setInterestCombination(this.interestCombination);
        interestInventory.setRegion(this.region);
        interestInventory.setDescription(this.description);
        interestInventory.setShortDescription(this.shortDescription);
        interestInventory.setLearningStyle(this.learningStyle);
        interestInventory.setLearningStyleInference(this.learningStyleInference);
        interestInventory.setLearningStyleRecommendation(this.learningStyleRecommendation);
        return interestInventory;
    }
}
