package com.synlabs.intscale.view.report;

import com.synlabs.intscale.entity.Report.Category;
import com.synlabs.intscale.view.FeedbackResponse;
import com.synlabs.intscale.view.Response;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by itrs on 7/25/2017.
 */
public class CategoryResponse implements Response {
    private Long   id;
    private String name;
    private String displayName;
    private String description;
    private String color;
    private String categoryName;
    private Long   parentId;
    private String highText;
    private String lowText;
    private BigDecimal juniorMeanScore;
    private BigDecimal juniorStandardDeviation;
    private BigDecimal seniorMeanScore;
    private BigDecimal seniorStandardDeviation;
    private BigDecimal tScore;
    private BigDecimal rawScore;
    private BigDecimal percentile;
    private Integer sequence;
    private String shortHighText;
    private String shortLowText;
    private Integer testScore = 0;
    private String type;
    private BigDecimal dislikeAndStronglyDislikeFrequency;
    private BigDecimal likeAndStronglyLikeFrequency;
    private BigDecimal secondaryMeanScore;
    private BigDecimal secondaryStandardDeviation;

    private List<BigDecimal> rawScores = new ArrayList<>();
    private List<FeedbackResponse> feedbacks = new ArrayList<>();
    private List<ScoreRangeResponse> scoreRanges = new ArrayList<>();
    private List<ScoreRangeResponse> juniorScoreRanges = new ArrayList<>();
    private List<ScoreRangeResponse> seniorScoreRanges = new ArrayList<>();
    private List<CategoryResponse> subCategories = new ArrayList<>();
    private List<NormResponse>       norms         = new ArrayList<>();

    public CategoryResponse(Category category) {
        this.name = category.getName();
        this.description = category.getDescription();
        this.id = category.getId();
        this.color = category.getColor();
        this.parentId = category.getParent() == null ? 0L : category.getParent().getId();
        this.categoryName = category.getParent() == null ? " " : category.getParent().getName();
        category.getFeedbacks().forEach(f -> {
            this.feedbacks.add(new FeedbackResponse(f));
        });
        category.getJuniorScoreRanges().forEach(scoreRange -> {
            this.juniorScoreRanges.add(new ScoreRangeResponse(scoreRange));
        });
        category.getSeniorScoreRanges().forEach(scoreRange -> {
            this.seniorScoreRanges.add(new ScoreRangeResponse(scoreRange));
        });
        this.lowText = category.getLowText();
        this.highText = category.getHighText();
        this.juniorMeanScore = category.getJuniorMeanScore();
        this.juniorStandardDeviation = category.getJuniorStandardDeviation();
        this.seniorMeanScore = category.getSeniorMeanScore();
        this.seniorStandardDeviation = category.getSeniorStandardDeviation();
        this.secondaryMeanScore = category.getSecondaryMeanScore();
        this.secondaryStandardDeviation = category.getSecondaryStandardDeviation();
        this.displayName = category.getDisplayName();
        this.sequence = category.getSequence();
        this.shortHighText = category.getShortHighText();
        this.shortLowText = category.getShortLowText();
        this.type = category.getType();
        this.norms = category.getNorms().stream().map(NormResponse::new).collect(Collectors.toList());
    }

    public CategoryResponse(CategoryResponse categoryResponse) {
        this.id=categoryResponse.getId();
        this.name=categoryResponse.getName();
        this.displayName=categoryResponse.getDisplayName();
        this.categoryName=categoryResponse.getCategoryName();
        this.parentId=categoryResponse.getParentId();
        this.tScore=categoryResponse.gettScore();
        this.rawScore=categoryResponse.getRawScore();
        this.percentile=categoryResponse.getPercentile();
        this.dislikeAndStronglyDislikeFrequency=categoryResponse.getDislikeAndStronglyDislikeFrequency();
        this.likeAndStronglyLikeFrequency=categoryResponse.getLikeAndStronglyLikeFrequency();
    }


    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Long getParentId() {
        return parentId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getColor() {
        return color;
    }

    public List<FeedbackResponse> getFeedbacks() {
        return feedbacks;
    }

    public String getHighText() {
        return highText;
    }

    public String getLowText() {
        return lowText;
    }

    public List<ScoreRangeResponse> getScoreRanges() {
        return scoreRanges;
    }

    public BigDecimal getJuniorMeanScore() {
        return juniorMeanScore;
    }

    public BigDecimal getJuniorStandardDeviation() {
        return juniorStandardDeviation;
    }

    public BigDecimal gettScore() {
        return tScore;
    }

    public void settScore(BigDecimal tScore) {
        this.tScore = tScore;
    }

    public BigDecimal getRawScore() {
        return rawScore;
    }

    public BigDecimal getPercentile() {
        return percentile;
    }

    public void setRawScore(BigDecimal rawScore) {
        this.rawScore = rawScore;
    }

    public void setPercentile(BigDecimal percentile) {
        this.percentile = percentile;
    }

    public List<CategoryResponse> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<CategoryResponse> subCategories) {
        this.subCategories = subCategories;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Integer getSequence() {
        return sequence;
    }

    public String getShortHighText() {
        return shortHighText;
    }

    public String getShortLowText() {
        return shortLowText;
    }

    public Integer getTestScore() {
        return testScore;
    }

    public void setTestScore(Integer testScore) {
        this.testScore = testScore;
    }

    public String getType() {
        return type;
    }

    public List<BigDecimal> getRawScores() {
        return rawScores;
    }

    public BigDecimal getSeniorMeanScore() {
        return seniorMeanScore;
    }

    public BigDecimal getSeniorStandardDeviation() {
        return seniorStandardDeviation;
    }

    public List<ScoreRangeResponse> getJuniorScoreRanges() {
        return juniorScoreRanges;
    }

    public List<ScoreRangeResponse> getSeniorScoreRanges() {
        return seniorScoreRanges;
    }

    public BigDecimal getSecondaryMeanScore()
    {
        return secondaryMeanScore;
    }

    public BigDecimal getSecondaryStandardDeviation()
    {
        return secondaryStandardDeviation;
    }

    public void setScoreRanges(List<ScoreRangeResponse> scoreRanges) {
        this.scoreRanges = scoreRanges;
    }

    public BigDecimal getDislikeAndStronglyDislikeFrequency() {
        return dislikeAndStronglyDislikeFrequency;
    }

    public void setDislikeAndStronglyDislikeFrequency(BigDecimal dislikeAndStronglyDislikeFrequency) {
        this.dislikeAndStronglyDislikeFrequency = dislikeAndStronglyDislikeFrequency;
    }

    public BigDecimal getLikeAndStronglyLikeFrequency() {
        return likeAndStronglyLikeFrequency;
    }

    public void setLikeAndStronglyLikeFrequency(BigDecimal likeAndStronglyLikeFrequency) {
        this.likeAndStronglyLikeFrequency = likeAndStronglyLikeFrequency;
    }

    public void setNorms(List<NormResponse> norms) {
        this.norms = norms;
    }

    public List<NormResponse> getNorms()
    {
        return norms;
    }
}
