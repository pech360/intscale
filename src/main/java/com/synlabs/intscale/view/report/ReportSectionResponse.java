package com.synlabs.intscale.view.report;

import com.synlabs.intscale.entity.Report.ReportSection;
import com.synlabs.intscale.view.Response;
import com.synlabs.intscale.view.TestDescriptionResponse;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ReportSectionResponse implements Response
{

    private Long id;
    private String name;
    private String description;
    private BigDecimal score;
    private BigDecimal juniorMeanScore;
    private BigDecimal juniorStandardDeviation;
    private BigDecimal seniorMeanScore;
    private BigDecimal seniorStandardDeviation;
    private BigDecimal secondaryMeanScore;
    private BigDecimal secondaryStandardDeviation;
    private Integer sequence;
    private InterestInventoryResponse interestInventoryResponse;
    private List<ScoreRangeResponse> scoreRanges = new ArrayList<>();
    private List<ScoreRangeResponse> juniorScoreRanges = new ArrayList<>();
    private List<ScoreRangeResponse> seniorScoreRanges = new ArrayList<>();
    private List<CategoryResponse> categories = new ArrayList<>();
    private List<TestDescriptionResponse> selectedTests = new ArrayList<>();
    private List<CategoryResponse> top3CategoriesForInterest = new ArrayList<>();
    private List<NormResponse>            norms                     = new ArrayList<>();

  public String getName()
  {
    return name;
  }

  public String getDescription()
  {
    return description;
  }

  public List<ScoreRangeResponse> getScoreRanges()
  {
    return scoreRanges;
  }

  public void setScoreRanges(List<ScoreRangeResponse> scoreRanges)
  {
    this.scoreRanges = scoreRanges;
  }

  public Long getId()
  {
    return id;
  }

  public List<CategoryResponse> getCategories()
  {
    return categories;
  }

  public void setCategories(List<CategoryResponse> categories)
  {
    this.categories = categories;
  }

  public BigDecimal getScore()
  {
    return score;
  }

  public void setScore(BigDecimal score)
  {
    this.score = score;
  }

  public List<TestDescriptionResponse> getSelectedTests()
  {
    return selectedTests;
  }

  public InterestInventoryResponse getInterestInventoryResponse()
  {
    return interestInventoryResponse;
  }

  public void setInterestInventoryResponse(InterestInventoryResponse interestInventoryResponse)
  {
    this.interestInventoryResponse = interestInventoryResponse;
  }

  public List<CategoryResponse> getTop3CategoriesForInterest()
  {
    return top3CategoriesForInterest;
  }

  public void setTop3CategoriesForInterest(List<CategoryResponse> top3CategoriesForInterest)
  {
    this.top3CategoriesForInterest = top3CategoriesForInterest;
  }

  public BigDecimal getJuniorMeanScore()
  {
    return juniorMeanScore;
  }

  public BigDecimal getJuniorStandardDeviation()
  {
    return juniorStandardDeviation;
  }

  public BigDecimal getSeniorMeanScore()
  {
    return seniorMeanScore;
  }

  public BigDecimal getSeniorStandardDeviation()
  {
    return seniorStandardDeviation;
  }

    public BigDecimal getSecondaryMeanScore()
    {
        return secondaryMeanScore;
    }

    public BigDecimal getSecondaryStandardDeviation()
    {
        return secondaryStandardDeviation;
    }

    public Integer getSequence()
    {
        return sequence;
    }

  public List<ScoreRangeResponse> getJuniorScoreRanges()
  {
    return juniorScoreRanges;
  }

  public List<ScoreRangeResponse> getSeniorScoreRanges()
  {
    return seniorScoreRanges;
  }

  public List<NormResponse> getNorms()
  {
    return norms;
  }

  public ReportSectionResponse(ReportSection reportSection)
  {
    this.id = reportSection.getId();
    this.name = reportSection.getName();
    this.description = reportSection.getDescription();
    this.juniorMeanScore = reportSection.getJuniorMeanScore();
    this.juniorStandardDeviation = reportSection.getJuniorStandardDeviation();
    this.seniorMeanScore = reportSection.getSeniorMeanScore();
    this.seniorStandardDeviation = reportSection.getSeniorStandardDeviation();
    this.secondaryMeanScore = reportSection.getSecondaryMeanScore();
    this.secondaryStandardDeviation = reportSection.getSecondaryStandardDeviation();
    this.sequence = reportSection.getSequence();
    if (reportSection.getScoreRanges() != null)
    {
      reportSection.getScoreRanges().forEach(scoreRange -> {
        this.scoreRanges.add(new ScoreRangeResponse(scoreRange));
      });
    }

    reportSection.getJuniorScoreRanges().forEach(juniorScoreRange -> {
      this.juniorScoreRanges.add(new ScoreRangeResponse(juniorScoreRange));
    });
    reportSection.getSeniorScoreRanges().forEach(seniorScoreRange -> {
      this.seniorScoreRanges.add(new ScoreRangeResponse(seniorScoreRange));
    });

    this.norms = reportSection.getNorms().stream().map(NormResponse::new).collect(Collectors.toList());
  }

  public ReportSectionResponse()
  {

  }
}
