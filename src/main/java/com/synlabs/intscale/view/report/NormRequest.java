package com.synlabs.intscale.view.report;

import com.synlabs.intscale.entity.Report.Norm;
import com.synlabs.intscale.view.Request;

import java.math.BigDecimal;

public class NormRequest implements Request
{
  private Long id;
  private String     language;
  private BigDecimal juniorMeanScore;
  private BigDecimal juniorStandardDeviation;
  private BigDecimal seniorMeanScore;
  private BigDecimal seniorStandardDeviation;
  private BigDecimal secondaryMeanScore;
  private BigDecimal secondaryStandardDeviation;


  public Norm toEntity(Norm norm){
    if(norm ==null){
      norm = new Norm();
    }
    norm.setLanguage(this.language);
    norm.setJuniorMeanScore(this.juniorMeanScore);
    norm.setJuniorStandardDeviation(this.juniorStandardDeviation);
    norm.setSeniorMeanScore(this.seniorMeanScore);
    norm.setSecondaryStandardDeviation(this.secondaryStandardDeviation);
    norm.setSecondaryMeanScore(this.secondaryMeanScore);
    norm.setSecondaryStandardDeviation(this.secondaryStandardDeviation);
    return norm;
  }

  public Long getId()
  {
    return id;
  }

  public String getLanguage()
  {
    return language;
  }

  public BigDecimal getJuniorMeanScore()
  {
    return juniorMeanScore;
  }

  public BigDecimal getJuniorStandardDeviation()
  {
    return juniorStandardDeviation;
  }

  public BigDecimal getSeniorMeanScore()
  {
    return seniorMeanScore;
  }

  public BigDecimal getSeniorStandardDeviation()
  {
    return seniorStandardDeviation;
  }

  public BigDecimal getSecondaryMeanScore()
  {
    return secondaryMeanScore;
  }

  public BigDecimal getSecondaryStandardDeviation()
  {
    return secondaryStandardDeviation;
  }
}
