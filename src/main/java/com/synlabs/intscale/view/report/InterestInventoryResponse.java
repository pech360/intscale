package com.synlabs.intscale.view.report;

import com.synlabs.intscale.entity.Report.InterestInventory;

public class InterestInventoryResponse
{

  private Long    id;
  private String  interestCombination;
  private Integer region;
  private String  description;
  private String  shortDescription;
  private String  learningStyle;
  private String  learningStyleInference;
  private String  learningStyleRecommendation;

  public Long getId()
  {
    return id;
  }

  public String getInterestCombination()
  {
    return interestCombination;
  }

  public Integer getRegion()
  {
    return region;
  }

  public String getDescription()
  {
    return description;
  }

  public String getShortDescription()
  {
    return shortDescription;
  }

  public String getLearningStyle()
  {
    return learningStyle;
  }

  public String getLearningStyleInference()
  {
    return learningStyleInference;
  }

  public String getLearningStyleRecommendation()
  {
    return learningStyleRecommendation;
  }

  public InterestInventoryResponse(InterestInventory interestInventory)
  {
    this.id = interestInventory.getId();
    this.interestCombination = interestInventory.getInterestCombination();
    this.region = interestInventory.getRegion();
    this.description = interestInventory.getDescription();
    this.shortDescription = interestInventory.getShortDescription();
    this.learningStyle = interestInventory.getLearningStyle();
    this.learningStyleInference  = interestInventory.getLearningStyleInference();
    this.learningStyleRecommendation = interestInventory.getLearningStyleRecommendation();
  }

  public InterestInventoryResponse()
  {

  }
}
