package com.synlabs.intscale.view.counselling;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.synlabs.intscale.entity.counselling.CounsellingSession;
import com.synlabs.intscale.enums.CounsellingSessionStatus;
import com.synlabs.intscale.enums.CounsellingSessionType;
import com.synlabs.intscale.view.Request;
import com.synlabs.intscale.view.question.SubTagRequest;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTimeComparator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CounsellingSessionRequest implements Request
{
  private Long                     id;
  private Long                     userId;
  private Long                     counsellorId;
  private String                   name;
  private String[]                 leads;
  @JsonFormat(pattern = "dd/MM/yyyy")
  private Date                     scheduledOn;
  private CounsellingSessionType   counsellingSessionType;
  private CounsellingSessionStatus counsellingSessionStatus;
  private List<SubTagRequest>                 tags                    = new ArrayList<>();
  private List<CounsellingSessionNoteRequest> counsellingSessionNotes = new ArrayList<>();

  public CounsellingSession toEntity(CounsellingSession counsellingSession)
  {
    if (counsellingSession == null)
    {
      counsellingSession = new CounsellingSession();
    }

    DateTimeComparator dateTimeComparator = DateTimeComparator.getDateOnlyInstance();
    Date today = DateUtils.addMinutes(new Date(), 330);

    counsellingSession.setName(this.getName());
    counsellingSession.setLeads(this.getLeads());

    counsellingSession.setScheduledOn(this.scheduledOn);
    counsellingSession.setCounsellingSessionType(this.counsellingSessionType);
    counsellingSession.setCounsellingSessionStatus(this.counsellingSessionStatus);
    if (counsellingSession.getCounsellingSessionStatus().equals(CounsellingSessionStatus.COMPLETED))
    {
      if (dateTimeComparator.compare(counsellingSession.getScheduledOn(), today) < 0)
      {
        counsellingSession.setCounsellingSessionStatus(CounsellingSessionStatus.DELAYED);
      }
    }
    return counsellingSession;
  }

  public Long getId()
  {
    return id;
  }

  public Long getUserId()
  {
    return userId;
  }

  public Long getCounsellorId()
  {
    return counsellorId;
  }

  public String getName()
  {
    return name;
  }

  public String[] getLeads()
  {
    return leads;
  }

  public Date getScheduledOn()
  {
    return scheduledOn;
  }

  public CounsellingSessionType getCounsellingSessionType()
  {
    return counsellingSessionType;
  }

  public CounsellingSessionStatus getCounsellingSessionStatus()
  {
    return counsellingSessionStatus;
  }

  public List<SubTagRequest> getTags()
  {
    return tags;
  }

  public List<CounsellingSessionNoteRequest> getCounsellingSessionNotes()
  {
    return counsellingSessionNotes;
  }
}
