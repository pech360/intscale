package com.synlabs.intscale.view.counselling;

import com.synlabs.intscale.entity.counselling.CounsellingSession;
import com.synlabs.intscale.enums.CounsellingSessionStatus;
import com.synlabs.intscale.enums.CounsellingSessionType;
import com.synlabs.intscale.view.Response;
import com.synlabs.intscale.view.question.SubTagRequest;
import com.synlabs.intscale.view.question.SubTagResponse;
import org.apache.commons.lang3.time.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class CounsellingSessionResponse implements Response
{

  private Long                     id;
  private String                   user_name;
  private String                   username;
  private Long                     userId;
  private String                   counsellorName;
  private String                   counsellorUsername;
  private Long                     counsellorId;
  private String                   name;
  private Long                     sequenceNumber;
  private Date                     scheduledOn;
  private String[]                 leads;
  private CounsellingSessionType   counsellingSessionType;
  private CounsellingSessionStatus counsellingSessionStatus;
  private List<CounsellingSessionNoteResponse> counsellingSessionNotes = new ArrayList<>();
  private List<SubTagResponse>                 subTags                    = new ArrayList<>();

  public CounsellingSessionResponse(CounsellingSession counsellingSession)
  {
    this.id = counsellingSession.getId();
    this.user_name = counsellingSession.getUser().getName();
    this.username = counsellingSession.getUser().getUsername();
    this.userId = counsellingSession.getUser().getId();
    this.counsellorName = counsellingSession.getCounsellor().getName();
    this.counsellorUsername = counsellingSession.getCounsellor().getUsername();
    this.counsellorId = counsellingSession.getCounsellor().getId();
    this.name = counsellingSession.getName();
    this.sequenceNumber = counsellingSession.getSequenceNumber();
    this.scheduledOn = DateUtils.addMinutes(counsellingSession.getScheduledOn(), 330);
    this.leads = counsellingSession.getLeads();
    this.counsellingSessionType = counsellingSession.getCounsellingSessionType();
    this.counsellingSessionStatus = counsellingSession.getCounsellingSessionStatus();
    this.counsellingSessionNotes = counsellingSession.getCounsellingSessionNotes().stream().map(CounsellingSessionNoteResponse::new).collect(Collectors.toList());
    this.subTags = counsellingSession.getSubTags().stream().map(SubTagResponse ::new).collect(Collectors.toList());
  }

  public Long getId()
  {
    return id;
  }

  public String getUser_name()
  {
    return user_name;
  }

  public void setUser_name(String user_name)
  {
    this.user_name = user_name;
  }

  public String getUsername()
  {
    return username;
  }

  public void setUsername(String username)
  {
    this.username = username;
  }

  public Long getUserId()
  {
    return userId;
  }

  public void setUserId(Long userId)
  {
    this.userId = userId;
  }

  public String getCounsellorName()
  {
    return counsellorName;
  }

  public String getCounsellorUsername()
  {
    return counsellorUsername;
  }

  public void setCounsellorUsername(String counsellorUsername)
  {
    this.counsellorUsername = counsellorUsername;
  }

  public void setCounsellorName(String counsellorName)
  {
    this.counsellorName = counsellorName;
  }

  public Long getCounsellorId()
  {
    return counsellorId;
  }

  public void setCounsellorId(Long counsellorId)
  {
    this.counsellorId = counsellorId;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public Long getSequenceNumber()
  {
    return sequenceNumber;
  }

  public void setSequenceNumber(Long sequenceNumber)
  {
    this.sequenceNumber = sequenceNumber;
  }

  public Date getScheduledOn()
  {
    return scheduledOn;
  }

  public void setScheduledOn(Date scheduledOn)
  {
    this.scheduledOn = scheduledOn;
  }

  public String[] getLeads()
  {
    return leads;
  }

  public void setLeads(String[] leads)
  {
    this.leads = leads;
  }

  public CounsellingSessionType getCounsellingSessionType()
  {
    return counsellingSessionType;
  }

  public void setCounsellingSessionType(CounsellingSessionType counsellingSessionType)
  {
    this.counsellingSessionType = counsellingSessionType;
  }

  public CounsellingSessionStatus getCounsellingSessionStatus()
  {
    return counsellingSessionStatus;
  }

  public void setCounsellingSessionStatus(CounsellingSessionStatus counsellingSessionStatus)
  {
    this.counsellingSessionStatus = counsellingSessionStatus;
  }

  public List<CounsellingSessionNoteResponse> getCounsellingSessionNotes()
  {
    return counsellingSessionNotes;
  }

  public List<SubTagResponse> getSubTags()
  {
    return subTags;
  }
}
