package com.synlabs.intscale.view.counselling;

import com.synlabs.intscale.entity.counselling.CounsellingSessionNotes;
import com.synlabs.intscale.enums.CounsellingSessionType;
import com.synlabs.intscale.view.Response;

public class CounsellingSessionNoteResponse implements Response
{
  private Long                   id;
  private CounsellingSessionType counsellingSessionType;
  private String                 statement;
  private String                 response;
  private boolean                active;
  private boolean                sample;
  private boolean timelinePost;


  public CounsellingSessionNoteResponse(CounsellingSessionNotes counsellingSessionNotes)
  {
    this.id=counsellingSessionNotes.getId();
    this.counsellingSessionType=counsellingSessionNotes.getCounsellingSessionType();
    this.statement=counsellingSessionNotes.getStatement();
    this.response=counsellingSessionNotes.getResponse();
    this.active=counsellingSessionNotes.isActive();
    this.sample=counsellingSessionNotes.isSample();
    this.timelinePost=counsellingSessionNotes.isTimelinePost();
  }

  public Long getId()
  {
    return id;
  }

  public CounsellingSessionType getCounsellingSessionType()
  {
    return counsellingSessionType;
  }

  public String getStatement()
  {
    return statement;
  }

  public String getResponse()
  {
    return response;
  }

  public boolean isActive()
  {
    return active;
  }

  public boolean isSample()
  {
    return sample;
  }

  public boolean isTimelinePost()
  {
    return timelinePost;
  }
}
