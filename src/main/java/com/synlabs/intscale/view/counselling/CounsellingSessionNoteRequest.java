package com.synlabs.intscale.view.counselling;

import com.synlabs.intscale.entity.counselling.CounsellingSessionNotes;
import com.synlabs.intscale.enums.CounsellingSessionType;
import com.synlabs.intscale.view.Request;

public class CounsellingSessionNoteRequest implements Request{
    private Long id;
    private CounsellingSessionType counsellingSessionType;
    private String statement;
    private String response;
    private boolean active;
    private boolean sample;
    private boolean timelinePost;

    public CounsellingSessionNotes toEntity(CounsellingSessionNotes counsellingSessionNotes){
        if(counsellingSessionNotes==null){
            counsellingSessionNotes = new CounsellingSessionNotes();
        }

        counsellingSessionNotes.setActive(this.active);
        counsellingSessionNotes.setStatement(this.statement);
        counsellingSessionNotes.setCounsellingSessionType(this.counsellingSessionType);
        counsellingSessionNotes.setSample(this.sample);
        counsellingSessionNotes.setResponse(this.response);
        counsellingSessionNotes.setTimelinePost(this.timelinePost);
        return counsellingSessionNotes;
    }

    public Long getId()
    {
        return id;
    }

    public CounsellingSessionType getCounsellingSessionType() {
        return counsellingSessionType;
    }

    public String getStatement() {
        return statement;
    }

    public String getResponse() {
        return response;
    }

    public boolean isActive() {
        return active;
    }

    public boolean isSample() {
        return sample;
    }

    public boolean isTimelinePost()
    {
        return timelinePost;
    }
}
