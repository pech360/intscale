package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.Teacher;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by India on 1/3/2018.
 */
public class TeacherResponse implements Response
{
  private Long    id;
  private String  name;
  private String  email;
  private String  gender;
  private Long    school;
  private String  schoolName;
  private String  education;
  private String  speciality;
  private String  grade;
  private String  gradeName;
  private boolean classTeacher;
  private boolean subjectTeacher;
  private Long    userId;
  private String  userName;
  private String  section;
  private boolean principal;
  private boolean hod;
  private Long    subject;
  private String  subjectName;
  private List<GradeSectionSubjectResponse> gradeSectionSubjects = new ArrayList<>();

  public TeacherResponse(Teacher st)
  {
    this.id = st.getId();
    this.name = st.getName();
    this.email = st.getEmail();
    this.gender = st.getGender();
    this.school = st.getSchool() != null ? st.getSchool().getId() : 0l;
    this.schoolName = st.getSchool() != null ? st.getSchool().getName() : "";
    this.education = st.getEducation();
    this.speciality = st.getSpeciality();
    this.grade = st.getGrade() != null ? st.getGrade() : "";
    this.gradeName = st.getGrade() != null ? st.getGrade() : "";
    this.classTeacher = st.isClassTeacher();
    this.subjectTeacher = st.isSubjectTeacher();
    this.userId = st.getUser() != null ? st.getUser().getId() : 0l;
    this.userName = st.getUser() != null ? st.getUser().getUsername() : "";
    this.principal = st.isPrincipal();
    this.section = st.getSection();
    this.subject = st.getSubject();
    this.subjectName = st.getSubjectName();
    this.hod = st.isHod();
    if (st.getGradeSectionSubjects() != null)
    {
      st.getGradeSectionSubjects().forEach(gradeSectionSubject -> {
        this.getGradeSectionSubjects().add(new GradeSectionSubjectResponse(gradeSectionSubject));
      });
    }

  }

  public Long getId()
  {
    return id;
  }

  public String getName()
  {
    return name;
  }

  public String getEmail()
  {
    return email;
  }

  public String getGender()
  {
    return gender;
  }

  public Long getSchool()
  {
    return school;
  }

  public String getSchoolName()
  {
    return schoolName;
  }

  public String getEducation()
  {
    return education;
  }

  public String getSpeciality()
  {
    return speciality;
  }

  public String getGrade()
  {
    return grade;
  }

  public String getGradeName()
  {
    return gradeName;
  }

  public boolean isClassTeacher()
  {
    return classTeacher;
  }

  public boolean isSubjectTeacher()
  {
    return subjectTeacher;
  }

  public Long getUserId()
  {
    return userId;
  }

  public String getUserName()
  {
    return userName;
  }

  public String getSection()
  {
    return section;
  }

  public boolean isPrincipal()
  {
    return principal;
  }

  public List<GradeSectionSubjectResponse> getGradeSectionSubjects()
  {
    return gradeSectionSubjects;
  }

  public Long getSubject()
  {
    return subject;
  }

  public String getSubjectName()
  {
    return subjectName;
  }

  public boolean isHod()
  {
    return hod;
  }
}
