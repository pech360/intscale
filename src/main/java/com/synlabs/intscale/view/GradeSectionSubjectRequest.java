package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.masterdata.GradeSectionSubject;
import com.synlabs.intscale.ex.NotFoundException;
import com.synlabs.intscale.view.TestTreeResponse.TestSummaryResponse;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;

public class GradeSectionSubjectRequest implements Request
{
  private Long   id;
  private String grade;
  private String section;
  private Long   subject;
  private String subjectName;

  public Long getId()
  {
    return id;
  }

  public String getGrade()
  {
    return grade;
  }

  public String getSection()
  {
    return section;
  }

  public Long getSubject()
  {
    return subject;
  }

  public String getSubjectName()
  {
    return subjectName;
  }

  public GradeSectionSubject toEntity(GradeSectionSubject gradeSectionSubject)
  {
    if (gradeSectionSubject == null)
    {
      gradeSectionSubject = new GradeSectionSubject();
    }

    if (StringUtils.isEmpty(this.grade))
    {
      throw new NotFoundException("Grade not found in course");
    }
    if (StringUtils.isEmpty(this.section))
    {
      throw new NotFoundException("section not found in course");
    }
    if (StringUtils.isEmpty(this.subject))
    {
      throw new NotFoundException("subject id not found in course");
    }
    if (StringUtils.isEmpty(this.subjectName))
    {
      throw new NotFoundException("subject name not found in course");
    }
    gradeSectionSubject.setGrade(this.grade);
    gradeSectionSubject.setSection(this.section);
    gradeSectionSubject.setSubject(this.subject);
    gradeSectionSubject.setSubjectName(this.subjectName);
    return gradeSectionSubject;
  }
}
