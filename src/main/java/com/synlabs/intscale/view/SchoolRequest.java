package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.School;
import com.synlabs.intscale.view.Request;

public class SchoolRequest implements Request
{
  private Long id;
  private String name;
  private String address;
  private String email;
  private String mobile;
  private String logo;
  private boolean fieldEnable;
  private boolean itselfTenant;

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public boolean isFieldEnable() {
    return fieldEnable;
  }

  public void setFieldEnable(boolean fieldEnable) {
    this.fieldEnable = fieldEnable;
  }

  public String getAddress()
  {
    return address;
  }

  public void setAddress(String address)
  {
    this.address = address;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public String getMobile()
  {
    return mobile;
  }

  public void setMobile(String mobile)
  {
    this.mobile = mobile;
  }

  public String getLogo()
  {
    return logo;
  }

  public void setLogo(String logo)
  {
    this.logo = logo;
  }

  public boolean isItselfTenant()
  {
    return itselfTenant;
  }

  public void setItselfTenant(boolean itselfTenant)
  {
    this.itselfTenant = itselfTenant;
  }

  public School toEntity(School school){
    if(school==null){
      school = new School();
    }
    school.setName(this.name);
    school.setAddress(this.address);
    school.setEmail(this.email);
    school.setLogo(this.logo);
    school.setMobile(this.mobile);
    school.setFieldEnable(this.fieldEnable);
    school.setItselfTenant(this.itselfTenant);
    return school;
  }
}
