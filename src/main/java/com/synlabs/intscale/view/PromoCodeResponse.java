package com.synlabs.intscale.view;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.synlabs.intscale.entity.PromoCode;
import com.synlabs.intscale.entity.PromocodeProduct;
import com.synlabs.intscale.util.ProductName;
//LNT
public class PromoCodeResponse implements Response {

	private Long Id;
	private String name;
	private BigDecimal discount;
	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss",timezone="IST")
	private Date couponStartDate;
	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss",timezone="IST")
	private Date couponEndDate;
	private Boolean isValid;
	private Integer totalCount;
	private Integer consumedCount;
	private List<String> productName;

	public String getName() {
		return name;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public Date getCouponStartDate() {
		return couponStartDate;
	}
	public Date getCouponEndDate() {
		return couponEndDate;
	}
	public Boolean getIsValid() {
		return isValid;
	}
	public Long getId() {
		return Id;
	}
	public Integer getTotalCount() {
		return totalCount;
	}
	public Integer getConsumedCount() {
		return consumedCount;
	}
	public List<String> getProductName() {
		return productName;
	}
	public Boolean getValid() {
		return isValid;
	}
	public void setValid(Boolean valid) {
		isValid = valid;
	}
	public void setId(Long id) {
		Id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public void setCouponStartDate(Date couponStartDate) {
		this.couponStartDate = couponStartDate;
	}
	public void setCouponEndDate(Date couponEndDate) {
		this.couponEndDate = couponEndDate;
	}
	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	public void setConsumedCount(Integer consumedCount) {
		this.consumedCount = consumedCount;
	}
	public void setProductName(List<String> productName) {
		this.productName = productName;
	}
	public PromoCodeResponse(){

	}
	public PromoCodeResponse(PromoCode promoCode, List<PromocodeProduct> promocodeProductList) {
		productName = new ArrayList<>();
		this.Id = promoCode.getId();
		this.name = promoCode.getName();
		this.discount = promoCode.getDiscount();
		try {
			this.couponStartDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(promoCode.getCouponStartDate());
			this.couponEndDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(promoCode.getCouponEndDate());

		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.isValid = promoCode.getIsValid();
		this.totalCount = promoCode.getTotalCount();
		this.consumedCount = promoCode.getConsumedCount();
		for (PromocodeProduct promocodeProduct : promocodeProductList) {
			productName.add(promocodeProduct.getKey().getProductname().getValue());
		}
	}
	
	public PromoCodeResponse(PromoCode promoCode) {
		productName = new ArrayList<>();
		this.Id = promoCode.getId();
		this.name = promoCode.getName();
		this.discount = promoCode.getDiscount();
		try {
			this.couponStartDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(promoCode.getCouponStartDate());
			this.couponEndDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(promoCode.getCouponEndDate());

		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.isValid = promoCode.getIsValid();
		this.totalCount = promoCode.getTotalCount();
		this.consumedCount = promoCode.getConsumedCount();
	}

}
