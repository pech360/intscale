package com.synlabs.intscale.view;

/**
 * Created by India on 4/5/2018.
 */
public class StudentCounsellorRequest implements Request {
    private Long[] studentIds;
    private Long counsellor;

    public Long[] getStudentIds() {
        return studentIds;
    }

    public Long getCounsellor() {
        return counsellor;
    }
}
