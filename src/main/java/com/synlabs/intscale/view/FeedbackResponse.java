package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.test.Feedback;

/**
 * Created by itrs on 9/7/2017.
 */
public class FeedbackResponse implements Response
{
  private Long id;
  private int marksMin;
  private int marksMax;
  private String expertAnaylsis;
  private String developementPlan;
  private Long categoryId;

  public FeedbackResponse(Feedback feedback){
    this.id=feedback.getId();
    this.marksMin=feedback.getMarksMin();
    this.marksMax=feedback.getMarksMax();
    this.expertAnaylsis=feedback.getExpertAnaylsis();
    this.developementPlan=feedback.getDevelopementPlan();
    this.categoryId=feedback.getCategory().getId();
  }
  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public int getMarksMin()
  {
    return marksMin;
  }

  public void setMarksMin(int marksMin)
  {
    this.marksMin = marksMin;
  }

  public int getMarksMax()
  {
    return marksMax;
  }

  public void setMarksMax(int marksMax)
  {
    this.marksMax = marksMax;
  }

  public String getExpertAnaylsis()
  {
    return expertAnaylsis;
  }

  public void setExpertAnaylsis(String expertAnaylsis)
  {
    this.expertAnaylsis = expertAnaylsis;
  }

  public String getDevelopementPlan()
  {
    return developementPlan;
  }

  public void setDevelopementPlan(String developementPlan)
  {
    this.developementPlan = developementPlan;
  }

  public Long getCategoryId()
  {
    return categoryId;
  }

  public void setCategoryId(Long categoryId)
  {
    this.categoryId = categoryId;
  }
}
