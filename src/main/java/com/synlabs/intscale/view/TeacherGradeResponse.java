package com.synlabs.intscale.view;

import java.util.List;
import java.util.Set;

/**
 * Created by India on 3/15/2018.
 */
public class TeacherGradeResponse implements Response {
    private String name;
    private String grade;
    private boolean classTeacher;
    private boolean subjectTeacher;
    private boolean principal;
    private Set<String> subjectGrades;
    private Set<String> subjectSections;
    private String section;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public boolean isClassTeacher() {
        return classTeacher;
    }

    public void setClassTeacher(boolean classTeacher) {
        this.classTeacher = classTeacher;
    }

    public boolean isSubjectTeacher() {
        return subjectTeacher;
    }

    public void setSubjectTeacher(boolean subjectTeacher) {
        this.subjectTeacher = subjectTeacher;
    }

    public Set<String> getSubjectGrades() {
        return subjectGrades;
    }

    public void setSubjectGrades(Set<String> subjectGrades) {
        this.subjectGrades = subjectGrades;
    }

    public String getSection()
    {
        return section;
    }

    public void setSection(String section)
    {
        this.section = section;
    }

    public Set<String> getSubjectSections()
    {
        return subjectSections;
    }

    public void setSubjectSections(Set<String> subjectSections)
    {
        this.subjectSections = subjectSections;
    }

    public boolean isPrincipal()
    {
        return principal;
    }

    public void setPrincipal(boolean principal)
    {
        this.principal = principal;
    }
}
