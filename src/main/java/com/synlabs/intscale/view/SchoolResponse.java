package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.School;

public class SchoolResponse implements Response
{
  private Long  id;
  private String  name;
  private String  address;
  private String  email;
  private String  mobile;
  private String  logo;
  private boolean fieldEnable;
  private boolean itselfTenant;

  public SchoolResponse(School school)
  {
    if (school == null)
    {
      school=new School();
    }
    this.id = school.getId();
    this.name = school.getName();
    this.address = school.getAddress();
    this.email = school.getEmail();
    this.mobile = school.getMobile();
    this.logo = school.getLogo();
    this.fieldEnable=school.isFieldEnable();
    this.itselfTenant = school.isItselfTenant();
  }

  public SchoolResponse()
  {
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getAddress()
  {
    return address;
  }

  public void setAddress(String address)
  {
    this.address = address;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public boolean isFieldEnable() {
    return fieldEnable;
  }

  public void setFieldEnable(boolean fieldEnable) {
    this.fieldEnable = fieldEnable;
  }

  public String getMobile()
  {
    return mobile;
  }

  public void setMobile(String mobile)
  {
    this.mobile = mobile;
  }

  public String getLogo()
  {
    return logo;
  }

  public void setLogo(String logo)
  {
    this.logo = logo;
  }

  public boolean isItselfTenant()
  {
    return itselfTenant;
  }

  public void setItselfTenant(boolean itselfTenant)
  {
    this.itselfTenant = itselfTenant;
  }
}
