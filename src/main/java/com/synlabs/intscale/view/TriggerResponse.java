package com.synlabs.intscale.view;



import org.quartz.Trigger;


public class TriggerResponse {
    private String name;
    private String group;
    private String cronExpression;
    private Long interval;
    private String type;
    private String jobName;
    private Trigger.TriggerState state;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getCronExpression() {
		return cronExpression;
	}
	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}
	public Long getInterval() {
		return interval;
	}
	public void setInterval(Long interval) {
		this.interval = interval;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public Trigger.TriggerState getState() {
		return state;
	}
	public void setState(Trigger.TriggerState state) {
		this.state = state;
	}
}
