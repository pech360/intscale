package com.synlabs.intscale.view.dto;

/**
 * Created by itrs on 6/5/2017.
 */
public class TestDTO
{
  private Long  id;
  private String  tenant_id;
  private String  name;
  private String  logo;
  private String  introduction;
  private String  participation;
  private String  disclaimer;
  private String  procedure;
  private String  description;
  private int     ageMin;
  private int     ageMax;
  private String  scoreCategory;
  private String  scoreSubCategory;
  private String  notes;
  private String  redirectTo;
  private boolean paid;

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getTenant_id()
  {
    return tenant_id;
  }

  public void setTenant_id(String tenant_id)
  {
    this.tenant_id = tenant_id;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getLogo()
  {
    return logo;
  }

  public void setLogo(String logo)
  {
    this.logo = logo;
  }

  public String getIntroduction()
  {
    return introduction;
  }

  public void setIntroduction(String introduction)
  {
    this.introduction = introduction;
  }

  public String getParticipation()
  {
    return participation;
  }

  public void setParticipation(String participation)
  {
    this.participation = participation;
  }

  public String getDisclaimer()
  {
    return disclaimer;
  }

  public void setDisclaimer(String disclaimer)
  {
    this.disclaimer = disclaimer;
  }

  public String getProcedure()
  {
    return procedure;
  }

  public void setProcedure(String procedure)
  {
    this.procedure = procedure;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public int getAgeMin()
  {
    return ageMin;
  }

  public void setAgeMin(int ageMin)
  {
    this.ageMin = ageMin;
  }

  public int getAgeMax()
  {
    return ageMax;
  }

  public void setAgeMax(int ageMax)
  {
    this.ageMax = ageMax;
  }

  public String getScoreCategory()
  {
    return scoreCategory;
  }

  public void setScoreCategory(String scoreCategory)
  {
    this.scoreCategory = scoreCategory;
  }

  public String getScoreSubCategory()
  {
    return scoreSubCategory;
  }

  public void setScoreSubCategory(String scoreSubCategory)
  {
    this.scoreSubCategory = scoreSubCategory;
  }

  public String getNotes()
  {
    return notes;
  }

  public void setNotes(String notes)
  {
    this.notes = notes;
  }

  public String getRedirectTo()
  {
    return redirectTo;
  }

  public void setRedirectTo(String redirectTo)
  {
    this.redirectTo = redirectTo;
  }

  public boolean isPaid()
  {
    return paid;
  }

  public void setPaid(boolean paid)
  {
    this.paid = paid;
  }
}
