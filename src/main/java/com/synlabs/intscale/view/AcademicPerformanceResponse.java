package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.AcademicPerformance;
import com.synlabs.intscale.enums.AcademicPerformanceName;
import com.synlabs.intscale.enums.AcademicPerformanceType;

import java.math.BigInteger;

public class AcademicPerformanceResponse implements Response
{
  private Long id;

  private AcademicPerformanceType type;

  private AcademicPerformanceName name;

  private BigInteger value;

  public AcademicPerformanceResponse(AcademicPerformance academicPerformance)
  {
    this.id = academicPerformance.getId();
    this.type = academicPerformance.getType();
    this.name = academicPerformance.getName();
    this.value = academicPerformance.getValue();
  }

  public AcademicPerformanceResponse(){

  }

  public Long getId()
  {
    return id;
  }

  public AcademicPerformanceType getType()
  {
    return type;
  }

  public AcademicPerformanceName getName()
  {
    return name;
  }

  public BigInteger getValue()
  {
    return value;
  }
}
