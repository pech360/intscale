package com.synlabs.intscale.view;

import java.util.List;

public class OtpSuccessResponse {

	private Long balance;
	private Long batch_id;
	private Long cost;
	private Long num_messages;
	private MessageForOtpResponse message;
	private String receipt_url;
	private String custom;
	private List<ReceipientForOtpResponse> messages;
	private String status;
	
	public Long getBalance() {
		return balance;
	}
	public void setBalance(Long balance) {
		this.balance = balance;
	}
	public Long getBatch_id() {
		return batch_id;
	}
	public void setBatch_id(Long batch_id) {
		this.batch_id = batch_id;
	}
	public Long getCost() {
		return cost;
	}
	public void setCost(Long cost) {
		this.cost = cost;
	}
	public Long getNum_messages() {
		return num_messages;
	}
	public void setNum_messages(Long num_messages) {
		this.num_messages = num_messages;
	}
	public MessageForOtpResponse getMessage() {
		return message;
	}
	public void setMessage(MessageForOtpResponse message) {
		this.message = message;
	}
	public List<ReceipientForOtpResponse> getMessages() {
		return messages;
	}
	public void setMessages(List<ReceipientForOtpResponse> messages) {
		this.messages = messages;
	}
	public String getReceipt_url() {
		return receipt_url;
	}
	public void setReceipt_url(String receipt_url) {
		this.receipt_url = receipt_url;
	}
	public String getCustom() {
		return custom;
	}
	public void setCustom(String custom) {
		this.custom = custom;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
