package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.test.Test;

/**
 * Created by India on 1/25/2018.
 */
public class TestConstructRequest implements Request {
    private Long id;
    private String name;
    private Long parentId;
    private String description;
    private boolean leafNode;
    private String introduction;
    private String procedure;
    private String participation;
    private String disclaimer;
    private String grades[];

    public Test toEntity(Test test){
        test.setName(this.name);
        test.setDescription(this.description);
        test.setLeafNode(this.leafNode);
        test.setConstruct(true);
        test.setIntroduction(this.introduction);
        test.setProcedure(this.procedure);
        test.setParticipation(this.participation);
        test.setDisclaimer(this.disclaimer);
        test.setGrades(this.grades);
        return test;
    }

    public Test toEntity(){
        return toEntity(new Test());
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getParentId() {
        return parentId;
    }

    public String getDescription() {
        return description;
    }

    public boolean isLeafNode() {
        return leafNode;
    }

    public String getIntroduction() {
        return introduction;
    }

    public String getProcedure() {
        return procedure;
    }

    public String getParticipation() {
        return participation;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public String[] getGrades() {
        return grades;
    }
}
