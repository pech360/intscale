package com.synlabs.intscale.view.threesixty;

import com.synlabs.intscale.entity.threesixty.StakeholderQuestionOption;
import com.synlabs.intscale.view.Response;

public class StakeholderQuestionOptionRequest implements Response
{
  private Long    id;
  private String  description;
  private Integer marks;

  public StakeholderQuestionOptionRequest(){}

  public StakeholderQuestionOptionRequest(StakeholderQuestionOption o)
  {
    this.description = o.getDescription();
    this.marks = o.getMarks();
  }

  public StakeholderQuestionOption toEntity(StakeholderQuestionOption option)
  {
    option = option == null ? new StakeholderQuestionOption() : option;
    if (option.isNew() && id != null)
    {
      option.setId(id);
    }
    option.setDescription(description);
    option.setMarks(marks);
    return option;
  }

  public boolean isNew()
  {
    return id == null;
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public Integer getMarks()
  {
    return marks;
  }

  public void setMarks(Integer marks)
  {
    this.marks = marks;
  }

}
