package com.synlabs.intscale.view.threesixty;

import com.synlabs.intscale.entity.threesixty.Stakeholder;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.enums.StakeholderRelation;
import com.synlabs.intscale.enums.StakeholderTestStatus;
import com.synlabs.intscale.service.BaseService;
import com.synlabs.intscale.view.Response;
import com.synlabs.intscale.view.usertest.TestResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StakeholderResponse implements Response
{
  private Long    id;
  private String  name;
  private String  mobile;
  private String  email;
  private String  studentName;
  private Boolean verified;

  private StakeholderRelation relation;

  private StakeholderTestStatus status;

  private List<TestResponse> tests = new ArrayList<>();

  public enum ResponseType
  {
    Full, Summary
  }

  public StakeholderResponse(Stakeholder stakeholder)
  {
    this(stakeholder, ResponseType.Summary);
  }

  public StakeholderResponse(Stakeholder stakeholder, ResponseType type)
  {
    User student = stakeholder.getUser();
    switch (type)
    {
      case Full:
        tests = stakeholder.getTests().stream()
                           .map(t -> new TestResponse(t, TestResponse.ResponseType.Summary))
                           .collect(Collectors.toList());
      case Summary:
        id = stakeholder.getId();
        studentName = student == null ? "" : student.getName();
        name = stakeholder.getName();
        mobile = stakeholder.getMobile();
        email = stakeholder.getEmail();
        relation = stakeholder.getRelation();
        status = stakeholder.getStatus();
        verified = BaseService.isVerified(stakeholder);
    }
  }

  public Boolean getVerified()
  {
    return verified;
  }

  public String getName()
  {
    return name;
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getMobile()
  {
    return mobile;
  }

  public void setMobile(String mobile)
  {
    this.mobile = mobile;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public String getStudentName()
  {
    return studentName;
  }

  public void setStudentName(String studentName)
  {
    this.studentName = studentName;
  }

  public StakeholderRelation getRelation()
  {
    return relation;
  }

  public void setRelation(StakeholderRelation relation)
  {
    this.relation = relation;
  }

  public StakeholderTestStatus getStatus()
  {
    return status;
  }

  public void setStatus(StakeholderTestStatus status)
  {
    this.status = status;
  }

  public List<TestResponse> getTests()
  {
    return tests;
  }

  public void setTests(List<TestResponse> tests)
  {
    this.tests = tests;
  }
}
