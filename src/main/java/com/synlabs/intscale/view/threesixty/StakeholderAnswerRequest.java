package com.synlabs.intscale.view.threesixty;

import com.synlabs.intscale.view.Request;

public class StakeholderAnswerRequest implements Request
{
  private Long questionId;
  private Long optionId;

  public Long getQuestionId()
  {
    return questionId;
  }

  public void setQuestionId(Long questionId)
  {
    this.questionId = questionId;
  }

  public Long getOptionId()
  {
    return optionId;
  }

  public void setOptionId(Long optionId)
  {
    this.optionId = optionId;
  }
}
