package com.synlabs.intscale.view.threesixty;

import com.synlabs.intscale.entity.threesixty.StakeholderQuestionOption;
import com.synlabs.intscale.view.Response;

public class StakeholderQuestionOptionResponse implements Response
{
  private Long id;

  private String description;

  private Integer marks;

  public StakeholderQuestionOptionResponse(StakeholderQuestionOption option)
  {
    id = option.getId();
    description = option.getDescription();
    marks = option.getMarks();
  }

  public Long getId()
  {
    return id;
  }

  public String getDescription()
  {
    return description;
  }

  public Integer getMarks()
  {
    return marks;
  }
}
