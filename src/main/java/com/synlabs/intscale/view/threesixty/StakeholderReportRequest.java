package com.synlabs.intscale.view.threesixty;

import com.synlabs.intscale.view.Request;

import java.util.Date;

public class StakeholderReportRequest implements Request
{
  private Long schoolId;
  private Long gradeId;

  private Date from;
  private Date to;

  public Long getGradeId()
  {
    return gradeId;
  }

  public void setGradeId(Long gradeId)
  {
    this.gradeId = gradeId;
  }

  public Long getSchoolId()
  {
    return schoolId;
  }

  public void setSchoolId(Long schoolId)
  {
    this.schoolId = schoolId;
  }

  public Date getFrom()
  {
    return from;
  }

  public void setFrom(Date from)
  {
    this.from = from;
  }

  public Date getTo()
  {
    return to;
  }

  public void setTo(Date to)
  {
    this.to = to;
  }

}