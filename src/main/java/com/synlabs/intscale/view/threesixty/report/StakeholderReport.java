package com.synlabs.intscale.view.threesixty.report;

import com.synlabs.intscale.enums.StakeholderRelation;

public class StakeholderReport
{
  private String email;
  private String name;
  private StakeholderRelation relation;
  private Integer marks;
  private String answer;

}
