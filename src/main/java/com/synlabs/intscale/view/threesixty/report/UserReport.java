package com.synlabs.intscale.view.threesixty.report;

import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.test.TestResult;
import com.synlabs.intscale.entity.threesixty.StakeholderQuestion;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.service.BaseService;
import com.synlabs.intscale.service.threesixty.StakeholderTestService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class UserReport
{
  private List<StakeholderQuestionReport> questions = new ArrayList<>();
  private String username;
  private String schoolName;
  private String grade;

  public UserReport(User user){
    username = user.getUsername();
    schoolName = user.getSchoolName();
    grade = user.getGrade();
  }

  public List<StakeholderQuestionReport> getQuestions()
  {
    return questions;
  }

  public String getUsername()
  {
    return username;
  }

  public String getSchoolName()
  {
    return schoolName;
  }

  public String getGrade()
  {
    return grade;
  }
}
