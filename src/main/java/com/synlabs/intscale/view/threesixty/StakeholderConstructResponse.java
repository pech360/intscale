package com.synlabs.intscale.view.threesixty;

import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.view.Response;

public class StakeholderConstructResponse implements Response {
    private Long id;
    private String description;
    private String name;
    private String image;
    private String video;
    public StakeholderConstructResponse(Test test){
        this.id=test.getId();
        this.description=test.getDescription();
        this.name=test.getName();
        this.image=test.getStakeholderImage();
        this.video=test.getStakeholderVideo();
    }

    public Long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getVideo() {
        return video;
    }
}
