package com.synlabs.intscale.view.threesixty.email;

import com.synlabs.intscale.entity.threesixty.Stakeholder;
import org.joda.time.Hours;
import org.joda.time.LocalDate;

import java.util.Date;

public class TestLinkResponse
{
  private String loginKey;
  private String baseUrl;
  private Date   validTill;

  public TestLinkResponse(Stakeholder stakeholder, String baseUrl)
  {
    this.validTill = stakeholder.getLoginValid();
    this.loginKey = stakeholder.getLoginKey();
    this.baseUrl = baseUrl;
  }

  public String getLoginKey()
  {
    return loginKey;
  }

  public String getBaseUrl()
  {
    return baseUrl;
  }

  public Date getValidTill()
  {
    return validTill;
  }

  public Integer hoursRemaining()
  {
    return Hours.hoursBetween(LocalDate.now(), LocalDate.fromDateFields(validTill))
                .getHours();
  }

  public void setLoginKey(String loginKey)
  {
    this.loginKey = loginKey;
  }

  public void setBaseUrl(String baseUrl)
  {
    this.baseUrl = baseUrl;
  }

  public void setValidTill(Date validTill)
  {
    this.validTill = validTill;
  }
}
