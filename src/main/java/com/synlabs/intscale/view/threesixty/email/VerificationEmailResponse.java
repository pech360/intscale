package com.synlabs.intscale.view.threesixty.email;

import com.synlabs.intscale.entity.threesixty.Stakeholder;
import com.synlabs.intscale.entity.user.User;

public class VerificationEmailResponse
{
  private String studentName;
  private String studentUsername;
  private String loginKey;
  private String baseUrl;

  public VerificationEmailResponse(Stakeholder stakeholder, String baseUrl)
  {
    User studentUser = stakeholder.getUser();
    if (studentUser != null)
    {
      this.studentName = studentUser.getName();
      this.studentUsername = studentUser.getUsername();
    }
    this.loginKey = stakeholder.getLoginKey();
    this.baseUrl = baseUrl;
  }

  public String getStudentName()
  {
    return studentName;
  }

  public String getStudentUsername()
  {
    return studentUsername;
  }

  public String getLoginKey()
  {
    return loginKey;
  }

  public String getBaseUrl()
  {
    return baseUrl;
  }

  public void setStudentName(String studentName)
  {
    this.studentName = studentName;
  }

  public void setStudentUsername(String studentUsername)
  {
    this.studentUsername = studentUsername;
  }

  public void setLoginKey(String loginKey)
  {
    this.loginKey = loginKey;
  }

  public void setBaseUrl(String baseUrl)
  {
    this.baseUrl = baseUrl;
  }
}
