package com.synlabs.intscale.view.threesixty;

import com.synlabs.intscale.entity.test.SubTag;
import com.synlabs.intscale.entity.threesixty.StakeholderQuestion;
import com.synlabs.intscale.entity.threesixty.StakeholderQuestionOption;
import com.synlabs.intscale.view.CategoryResponse;
import com.synlabs.intscale.view.Response;
import com.synlabs.intscale.view.question.SubTagResponse;
import com.synlabs.intscale.view.usertest.TestResponse;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

public class StakeholderQuestionResponse implements Response
{
  private Long   id;
  private String description;

  private TestResponse test;

  private Integer optionCount;

  private List<SubTagResponse> subTags = new ArrayList<>();

  private List<StakeholderQuestionOptionResponse> options = new ArrayList<>();

  private CategoryResponse subCategory;

  private CategoryResponse category;

  public enum ResponseType
  {
    Full, Options, Summary
  }

  public StakeholderQuestionResponse(StakeholderQuestion question)
  {
    this(question, ResponseType.Full);
  }

  public StakeholderQuestionResponse(StakeholderQuestion question, ResponseType type)
  {
    type = type == null ? ResponseType.Full : type;
    switch (type)
    {
      case Full:
        if (question.getTest() != null)
        {
          test = new TestResponse(question.getTest(), TestResponse.ResponseType.Summary);
        }

        List<SubTag> subTags = question.getSubTags();
        if (!CollectionUtils.isEmpty(subTags))
        {
          subTags.forEach(s -> {
            this.subTags.add(new SubTagResponse(s));
          });
        }

        if (question.getSubCategory() != null)
        {
          this.subCategory = new CategoryResponse(question.getSubCategory(), CategoryResponse.ResponseType.Summary);
        }
        if (question.getCategory() != null)
        {
          this.category = new CategoryResponse(question.getCategory(), CategoryResponse.ResponseType.Summary);
        }
      case Options:
        List<StakeholderQuestionOption> options = question.getOptions();
        if (!CollectionUtils.isEmpty(options))
        {
          this.optionCount = options.size();
          options.forEach(o -> {
            this.options.add(new StakeholderQuestionOptionResponse(o));
          });
        }
      case Summary:
        id = question.getId();
        description = question.getDescription();
    }
  }

  public CategoryResponse getSubCategory()
  {
    return subCategory;
  }

  public CategoryResponse getCategory()
  {
    return category;
  }

  public Integer getOptionCount()
  {
    return optionCount;
  }

  public TestResponse getTest()
  {
    return test;
  }

  public Long getId()
  {
    return id;
  }

  public String getDescription()
  {
    return description;
  }

  public List<StakeholderQuestionOptionResponse> getOptions()
  {
    return options;
  }

  public List<SubTagResponse> getSubTags()
  {
    return subTags;
  }
}
