package com.synlabs.intscale.view.threesixty.report;

import java.util.ArrayList;
import java.util.List;

public class StakeholderQuestionReport
{
  private String question;
  private Long id;
  private String category;
  private String subCategory;
  private String subTag;
  List<StakeholderReport> stakeholders = new ArrayList<>();
}
