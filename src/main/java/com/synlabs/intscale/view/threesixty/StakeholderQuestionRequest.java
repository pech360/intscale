package com.synlabs.intscale.view.threesixty;

import com.synlabs.intscale.entity.threesixty.StakeholderQuestion;
import com.synlabs.intscale.entity.threesixty.StakeholderQuestionOption;
import com.synlabs.intscale.view.Response;
import org.springframework.data.jpa.domain.AbstractPersistable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StakeholderQuestionRequest implements Response
{
  private Long   testId;
  private Long   id;
  private String description;
  private Long   subCategoryId;

  private List<Long> subTagIds = new ArrayList<>();

  private List<StakeholderQuestionOptionRequest> options = new ArrayList<>();

  public StakeholderQuestionRequest()
  {
  }

  public StakeholderQuestionRequest(StakeholderQuestion question)
  {
    this.description = question.getDescription();
    this.subCategoryId = question.getSubCategory() == null ? null : question.getSubCategory().getId();
    this.subTagIds = question.getSubTags().stream()
                             .map(AbstractPersistable::getId)
                             .collect(Collectors.toList());
    this.testId = question.getTest().getId();

    this.options = question.getOptions().stream()
                           .map(StakeholderQuestionOptionRequest::new)
                           .collect(Collectors.toList());
  }

  public StakeholderQuestion toEntity(StakeholderQuestion question)
  {
    question = question == null ? new StakeholderQuestion() : question;
    question.setDescription(description);

    List<StakeholderQuestionOption> newOption = new ArrayList<>();
    options.forEach(o -> {
      newOption.add(o.toEntity(null));
    });

//    question.getOptions().forEach(o->{
//      if(!o.isNew()){
//        options.forEach(ro->{
//          if(o.getId().equals(ro.getId())){
//            ro.toEntity(o);
//          }
//        });
//      }
//    });

    question.getOptions().addAll(newOption);

    return question;
  }

  public Long getSubCategoryId()
  {
    return subCategoryId;
  }

  public void setSubCategoryId(Long subCategoryId)
  {
    this.subCategoryId = subCategoryId;
  }

  public List<Long> getSubTagIds()
  {
    return subTagIds;
  }

  public void setSubTagIds(List<Long> subTagIds)
  {
    this.subTagIds = subTagIds;
  }

  public Long getTestId()
  {
    return testId;
  }

  public void setTestId(Long testId)
  {
    this.testId = testId;
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public List<StakeholderQuestionOptionRequest> getOptions()
  {
    return options;
  }

  public void setOptions(List<StakeholderQuestionOptionRequest> options)
  {
    this.options = options;
  }
}
