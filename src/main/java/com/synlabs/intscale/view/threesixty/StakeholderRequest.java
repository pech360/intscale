package com.synlabs.intscale.view.threesixty;

import com.synlabs.intscale.entity.threesixty.Stakeholder;
import com.synlabs.intscale.enums.StakeholderRelation;
import com.synlabs.intscale.enums.StakeholderTestStatus;
import com.synlabs.intscale.view.Request;

public class StakeholderRequest implements Request
{
  private Long   id;
  private String name;
  private String mobile;
  private String email;

  private StakeholderRelation   relation;

  public Stakeholder toEntity(Stakeholder stakeholder)
  {
    stakeholder = stakeholder == null ? new Stakeholder() : stakeholder;
    stakeholder.setId(id);
    stakeholder.setName(name);
    stakeholder.setMobile(mobile);
    stakeholder.setEmail(email);
    stakeholder.setRelation(relation);
    return stakeholder;
  }

  public boolean isNew()
  {
    return id == null;
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getMobile()
  {
    return mobile;
  }

  public void setMobile(String mobile)
  {
    this.mobile = mobile;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public StakeholderRelation getRelation()
  {
    return relation;
  }

  public void setRelation(StakeholderRelation relation)
  {
    this.relation = relation;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }
}
