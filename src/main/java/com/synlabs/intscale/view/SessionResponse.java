package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.Session;

/**
 * Created by India on 1/3/2018.
 */
public class SessionResponse implements Response{
    private Long id;
    private String session;
    private boolean active;
    public SessionResponse(Session session){
        this.id=session.getId();
        this.session=session.getSession();
        this.active=session.isActive();
    }

    public Long getId() {
        return id;
    }

    public String getSession() {
        return session;
    }

    public boolean isActive() {
        return active;
    }
}
