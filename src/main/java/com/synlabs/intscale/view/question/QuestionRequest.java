package com.synlabs.intscale.view.question;

import com.synlabs.intscale.entity.test.Question;
import com.synlabs.intscale.view.Request;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by itrs on 7/31/2017.
 */
public class QuestionRequest implements Request
{
  private Long id;
  private boolean hasAnimatedGif;
  private String videoName;
  private boolean hasDummy;
  private String description;
  private boolean hasAudio;
  private String audioName;
  private boolean hasImage;
  private String imageName;
  private boolean hasTimer;
  private int timeInSeconds;
  private boolean hasParagraph;
  private String paraWord;
  private String questionType;
  private int numberOfAnswers;
  private String answerAlignment;
  private String groupName;
  private List<AnswerRequest> answerRequests;
  private String hint;
  private boolean hasValidation;
  private int minLength;
  private int maxLength;
  private boolean specialCharAllowed;
  private boolean alphabetAllowed;
  private boolean numericAllowed;
  private boolean ignoreQuestion;
  private boolean hideOptionsUntilVideoEnded;

  private List<Long> subTagIds=new ArrayList<>();

  public Question toEntity(Question question){
    if(question==null){question=new Question();}
    question.setDescription(this.description);
    question.setHasAudio(this.hasAudio);
    if(this.hasAudio){
      question.setAudioName(this.audioName);
    }
    question.setHasImage(this.hasImage);
    if(this.hasImage){
      question.setImageName(this.imageName);
    }
    question.setHasTimer(this.hasTimer);
    if(this.hasTimer){
      question.setTimeInSeconds(this.timeInSeconds);
    }
    question.setHasParagraph(this.hasParagraph);
    if(this.hasParagraph){
      question.setParaWord(this.paraWord);
    }
    question.setHasAnimatedGif(this.hasAnimatedGif);
    if(this.hasAnimatedGif){
      question.setVideoName(this.videoName);
    }
    question.setHasDummy(this.hasDummy);
    question.setQuestionType(this.questionType);
    question.setNumberOfAnswers(this.numberOfAnswers);
    question.setGroupName(this.groupName);
    question.setAnswerAlignment(this.answerAlignment);
    question.setHint(this.hint);
    question.setAlphabetAllowed(this.alphabetAllowed);
    question.setNumericAllowed(this.numericAllowed);
    question.setHasValidation(this.hasValidation);
    question.setSpecialCharAllowed(this.specialCharAllowed);
    question.setMaxLength(this.maxLength);
    question.setMinLength(this.minLength);
    question.setIgnoreQuestion(this.ignoreQuestion);
    question.setHideOptionsUntilVideoEnded(this.hideOptionsUntilVideoEnded);
    return question;
  }
  public Question toEntity(){return toEntity(new Question());}

  public Long getId()
  {
    return id;
  }

  public boolean isHasAnimatedGif()
  {
    return hasAnimatedGif;
  }

  public boolean isHasDummy()
  {
    return hasDummy;
  }

  public String getDescription()
  {
    return description;
  }

  public boolean isHasAudio()
  {
    return hasAudio;
  }

  public String getAudioName()
  {
    return audioName;
  }

  public boolean isHasImage()
  {
    return hasImage;
  }

  public String getImageName()
  {
    return imageName;
  }

  public boolean isHasTimer()
  {
    return hasTimer;
  }

  public int getTimeInSeconds()
  {
    return timeInSeconds;
  }

  public boolean isHasParagraph()
  {
    return hasParagraph;
  }

  public String getParaWord()
  {
    return paraWord;
  }

  public String getQuestionType()
  {
    return questionType;
  }

  public int getNumberOfAnswers()
  {
    return numberOfAnswers;
  }

  public String getAnswerAlignment()
  {
    return answerAlignment;
  }

  public String getVideoName() {
    return videoName;
  }

  public String getGroupName()
  {
    return groupName;
  }

  public List<AnswerRequest> getAnswerRequests()
  {
    return answerRequests;
  }

  public List<Long> getSubTagIds()
  {
    return subTagIds;
  }

  public boolean isHasValidation() {
    return hasValidation;
  }

  public int getMinLength() {
    return minLength;
  }

  public int getMaxLength() {
    return maxLength;
  }

  public boolean isSpecialCharAllowed() {
    return specialCharAllowed;
  }

  public boolean isAlphabetAllowed() {
    return alphabetAllowed;
  }

  public boolean isNumericAllowed() {
    return numericAllowed;
  }

  public String getHint()
  {
    return hint;
  }

  public boolean isIgnoreQuestion()
  {
    return ignoreQuestion;
  }

  public boolean isHideOptionsUntilVideoEnded()
  {
    return hideOptionsUntilVideoEnded;
  }
}
