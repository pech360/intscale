package com.synlabs.intscale.view.question;

import com.synlabs.intscale.entity.test.SubTag;
import com.synlabs.intscale.enums.TagType;
import com.synlabs.intscale.view.Response;

public class SubTagResponse implements Response
{
  private String name;
  private Long id;
  private Long tagId;
  private TagType type;

  public SubTagResponse(SubTag subTag){
    name=subTag.getName();
    id=subTag.getId();
    tagId= subTag.getTag()==null?null:subTag.getTag().getId();
    this.type = subTag.getType();
  }

  public Long getTagId()
  {
    return tagId;
  }

  public String getName()
  {
    return name;
  }

  public Long getId()
  {
    return id;
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o)
    {
      return true;
    }
    if (o == null || getClass() != o.getClass())
    {
      return false;
    }

    SubTagResponse that = (SubTagResponse) o;

    return id.equals(that.id);
  }

  @Override
  public int hashCode()
  {
    return id.hashCode();
  }
}
