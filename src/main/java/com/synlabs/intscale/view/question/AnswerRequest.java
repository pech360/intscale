package com.synlabs.intscale.view.question;

import com.synlabs.intscale.entity.test.Answers;
import com.synlabs.intscale.view.Request;

/**
 * Created by itrs on 7/31/2017.
 */
public class AnswerRequest implements Request
{
  private Long id;
  private String description;
  private int marks;
  private boolean hasText;
  private boolean hasImage;
  private String imageName;
  private boolean hasAudio;
  private String audioName;
  private int optionNumber;
  private String text;
  private int startPoint;
  private int endPoint;
  private String sliderType;
  public Answers toEntity(){return toEntity(new Answers());}

  public Answers toEntity(Answers answers){
    if(answers==null){answers=new Answers();}
    answers.setDescription(this.description);
    answers.setHasText(this.hasText);
    answers.setMarks(this.marks);
    answers.setHasImage(this.hasImage);
    answers.setImageName(this.imageName);
    answers.setHasAudio(this.hasAudio);
    answers.setAudioName(this.audioName);
    answers.setStartPoint(this.startPoint);
    answers.setEndPoint(this.endPoint);
    answers.setSliderType(this.sliderType);
    return answers;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public int getMarks()
  {
    return marks;
  }

  public void setMarks(int marks)
  {
    this.marks = marks;
  }

  public boolean isHasText()
  {
    return hasText;
  }

  public void setHasText(boolean hasText)
  {
    this.hasText = hasText;
  }

  public boolean isHasImage()
  {
    return hasImage;
  }

  public void setHasImage(boolean hasImage)
  {
    this.hasImage = hasImage;
  }

  public String getImageName()
  {
    return imageName;
  }

  public void setImageName(String imageName)
  {
    this.imageName = imageName;
  }

  public boolean isHasAudio()
  {
    return hasAudio;
  }

  public void setHasAudio(boolean hasAudio)
  {
    this.hasAudio = hasAudio;
  }

  public String getAudioName()
  {
    return audioName;
  }

  public void setAudioName(String audioName)
  {
    this.audioName = audioName;
  }

  public int getOptionNumber()
  {
    return optionNumber;
  }

  public void setOptionNumber(int optionNumber)
  {
    this.optionNumber = optionNumber;
  }

  public String getText()
  {
    return text;
  }

  public void setText(String text)
  {
    this.text = text;
  }

  public int getStartPoint()
  {
    return startPoint;
  }

  public void setStartPoint(int startPoint)
  {
    this.startPoint = startPoint;
  }

  public int getEndPoint()
  {
    return endPoint;
  }

  public void setEndPoint(int endPoint)
  {
    this.endPoint = endPoint;
  }

  public String getSliderType()
  {
    return sliderType;
  }

  public void setSliderType(String sliderType)
  {
    this.sliderType = sliderType;
  }
}
