package com.synlabs.intscale.view.question;

import com.synlabs.intscale.entity.test.Question;
import com.synlabs.intscale.view.Response;

public class QuestionSummaryResponse implements Response
{
  private Long id;
  private String description;
  private String questionType;
  private String groupName;
  private int numberOfAnswers;

  public QuestionSummaryResponse(Question question)
  {
    this.id = question.getId();
    this.description = question.getDescription();
    this.questionType = question.getQuestionType();
    this.groupName = question.getGroupName();
    this.numberOfAnswers = question.getNumberOfAnswers();
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public int getNumberOfAnswers()
  {
    return numberOfAnswers;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public String getQuestionType()
  {
    return questionType;
  }

  public void setQuestionType(String questionType)
  {
    this.questionType = questionType;
  }

  public String getGroupName()
  {
    return groupName;
  }

  public void setGroupName(String groupName)
  {
    this.groupName = groupName;
  }

}
