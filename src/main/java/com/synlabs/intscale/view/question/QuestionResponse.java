package com.synlabs.intscale.view.question;

import com.synlabs.intscale.entity.Report.Category;
import com.synlabs.intscale.entity.test.*;
import com.synlabs.intscale.view.report.CategoryResponse;
import com.synlabs.intscale.view.Response;
import com.synlabs.intscale.view.usertest.GroupTextResponse;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by itrs on 7/31/2017.
 */
public class QuestionResponse implements Response
{
  private Long    id;
  private boolean hasAnimatedGif;
  private String  videoName;
  private boolean hasDummy;
  private String  description;
  private boolean hasAudio;
  private String  audioName;
  private boolean hasImage;
  private String  imageName;
  private boolean hasTimer;
  private int     timeInSeconds;
  private boolean hasParagraph;
  private String  paraWord;
  private String  questionType;
  private int     numberOfAnswers;
  private String  answerAlignment;
  private String  groupName;
  private List<AnswerRequest> answerRequests = new ArrayList<>();

  private Set<QuestionTagResponse> tags    = new HashSet<>();
  private List<SubTagResponse>     subTags = new ArrayList<>();

  private Integer sequence;

  private GroupTextResponse group;
  private String            hint;

  private boolean hasValidation;
  private int     minLength;
  private int     maxLength;
  private boolean specialCharAllowed;
  private boolean alphabetAllowed;
  private boolean numericAllowed;
  private List<CategoryResponse> category    = new ArrayList<>();
  private List<CategoryResponse> subcategory = new ArrayList<>();
  private boolean ignoreQuestion;
  private boolean hideOptionsUntilVideoEnded;

  public QuestionResponse(Question question, Integer sequence, List<Category> categoryList, List<Category> subCategoryList)
  {
    this(question);
    this.sequence = sequence;
    categoryList.forEach(c -> {
      this.category.add(new CategoryResponse(c));
    });
    subCategoryList.forEach(c -> {
      this.subcategory.add(new CategoryResponse(c));
    });
  }

  public QuestionResponse(Question question)
  {
    if (question != null)
    {
      this.id = question.getId();
      this.hasDummy = question.isHasDummy();
      this.hasAnimatedGif = question.isHasAnimatedGif();
      this.videoName = question.getVideoName();
      this.description = question.getDescription();
      this.hasAudio = question.isHasAudio();
      this.audioName = question.getAudioName();
      this.hasImage = question.isHasImage();
      this.imageName = question.getImageName();
      this.hasTimer = question.isHasTimer();
      this.timeInSeconds = question.getTimeInSeconds();
      this.hasParagraph = question.isHasParagraph();
      this.paraWord = question.getParaWord();
      this.questionType = question.getQuestionType();
      this.numberOfAnswers = question.getNumberOfAnswers();
      this.answerAlignment = question.getAnswerAlignment();
      this.groupName = question.getGroupName();
      this.hint = question.getHint();
      this.alphabetAllowed = question.isAlphabetAllowed();
      this.numericAllowed = question.isNumericAllowed();
      this.hasValidation = question.isHasValidation();
      this.specialCharAllowed = question.isSpecialCharAllowed();
      this.minLength = question.getMinLength();
      this.maxLength = question.getMaxLength();
      this.ignoreQuestion = question.isIgnoreQuestion();
      this.hideOptionsUntilVideoEnded = question.isHideOptionsUntilVideoEnded();
      if (!StringUtils.isEmpty(this.groupName))
      {
        if (question.getGroupText() != null)
        {
          this.group = new GroupTextResponse(question.getGroupText());
        }
      }

      List<SubTag> subTags = question.getSubTags();
      for (SubTag subTag : subTags)
      {
        this.subTags.add(new SubTagResponse(subTag));
        if (subTag.getTag() != null)
        {
          QuestionTagResponse questionTagResponse = new QuestionTagResponse(subTag.getTag());
          questionTagResponse.setSubTags(new ArrayList<>());
          this.tags.add(questionTagResponse);
        }
      }
      question.getAnswers().forEach(ans -> {
        AnswerRequest arq = new AnswerRequest();
        arq.setMarks(ans.getMarks());
        arq.setDescription(ans.getDescription());
        arq.setHasAudio(ans.isHasAudio());
        arq.setAudioName(ans.getAudioName());
        arq.setHasImage(ans.isHasImage());
        arq.setImageName(ans.getImageName());
        arq.setHasText(ans.isHasText());
        arq.setOptionNumber(ans.getOptionNumber());
        arq.setStartPoint(ans.getStartPoint());
        arq.setEndPoint(ans.getEndPoint());
        arq.setSliderType(ans.getSliderType());
        this.answerRequests.add(arq);
      });
    }
  }

  public QuestionResponse(Question question, List<Category> categoryList, List<Category> subCategoryList)
  {
    this(question);
    categoryList.forEach(c -> {
      this.category.add(new CategoryResponse(c));
    });
    subCategoryList.forEach(c -> {
      this.subcategory.add(new CategoryResponse(c));
    });
  }

  public Integer getSequence()
  {
    return sequence;
  }

  public Set<QuestionTagResponse> getTags()
  {
    return tags;
  }

  public List<SubTagResponse> getSubTags()
  {
    return subTags;
  }

  public Long getId()
  {
    return id;
  }

  public boolean isHasAnimatedGif()
  {
    return hasAnimatedGif;
  }

  public String getVideoName()
  {
    return videoName;
  }

  public boolean isHasDummy()
  {
    return hasDummy;
  }

  public String getDescription()
  {
    return description;
  }

  public boolean isHasAudio()
  {
    return hasAudio;
  }

  public String getAudioName()
  {
    return audioName;
  }

  public boolean isHasImage()
  {
    return hasImage;
  }

  public String getImageName()
  {
    return imageName;
  }

  public boolean isHasTimer()
  {
    return hasTimer;
  }

  public int getTimeInSeconds()
  {
    return timeInSeconds;
  }

  public boolean isHasParagraph()
  {
    return hasParagraph;
  }

  public String getParaWord()
  {
    return paraWord;
  }

  public String getQuestionType()
  {
    return questionType;
  }

  public int getNumberOfAnswers()
  {
    return numberOfAnswers;
  }

  public String getAnswerAlignment()
  {
    return answerAlignment;
  }

  public String getGroupName()
  {
    return groupName;
  }

  public List<AnswerRequest> getAnswerRequests()
  {
    return answerRequests;
  }

  public GroupTextResponse getGroup()
  {
    return group;
  }

  public boolean isHasValidation()
  {
    return hasValidation;
  }

  public int getMinLength()
  {
    return minLength;
  }

  public int getMaxLength()
  {
    return maxLength;
  }

  public boolean isSpecialCharAllowed()
  {
    return specialCharAllowed;
  }

  public boolean isAlphabetAllowed()
  {
    return alphabetAllowed;
  }

  public boolean isNumericAllowed()
  {
    return numericAllowed;
  }

  public String getHint()
  {
    return hint;
  }

  public List<CategoryResponse> getCategory()
  {
    return category;
  }

  public List<CategoryResponse> getSubcategory()
  {
    return subcategory;
  }

  public boolean isIgnoreQuestion()
  {
    return ignoreQuestion;
  }

  public boolean isHideOptionsUntilVideoEnded()
  {
    return hideOptionsUntilVideoEnded;
  }
}
