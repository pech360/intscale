package com.synlabs.intscale.view.question;

import com.synlabs.intscale.entity.test.Question;
import com.synlabs.intscale.view.Response;
import com.synlabs.intscale.view.usertest.GroupTextResponse;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class QuestionResponseForTaker implements Response {
    private Long    id;
    private boolean hasAnimatedGif;
    private String  videoName;
    private String  description;
    private boolean hasAudio;
    private String  audioName;
    private boolean hasImage;
    private String  imageName;
    private String  questionType;
    private List<AnswerResponseForTaker> answerRequests = new ArrayList<>();
    private GroupTextResponse group;
    private boolean hasValidation;
    private int     minLength;
    private int     maxLength;
    private boolean specialCharAllowed;
    private boolean alphabetAllowed;
    private boolean numericAllowed;
    private boolean hideOptionsUntilVideoEnded;
    private String  groupName;
    private Integer sequence;


    public QuestionResponseForTaker(Question question)
    {
        if (question != null)
        {
            this.id = question.getId();
            this.hasAnimatedGif = question.isHasAnimatedGif();
            this.videoName = question.getVideoName();
            this.description = question.getDescription();
            this.hasAudio = question.isHasAudio();
            this.audioName = question.getAudioName();
            this.hasImage = question.isHasImage();
            this.imageName = question.getImageName();
            this.questionType = question.getQuestionType();
            this.alphabetAllowed = question.isAlphabetAllowed();
            this.numericAllowed = question.isNumericAllowed();
            this.hasValidation = question.isHasValidation();
            this.specialCharAllowed = question.isSpecialCharAllowed();
            this.minLength = question.getMinLength();
            this.maxLength = question.getMaxLength();
            this.groupName = question.getGroupName();
            this.hideOptionsUntilVideoEnded = question.isHideOptionsUntilVideoEnded();
            if (!StringUtils.isEmpty(question.getGroupName()))
            {
                if (question.getGroupText() != null)
                {
                    this.group = new GroupTextResponse(question.getGroupText());
                }
            }
            question.getAnswers().forEach(ans -> {
                AnswerResponseForTaker arq = new AnswerResponseForTaker();
                arq.setId(ans.getId());
                arq.setDescription(ans.getDescription());
                arq.setHasAudio(ans.isHasAudio());
                arq.setAudioName(ans.getAudioName());
                arq.setHasImage(ans.isHasImage());
                arq.setImageName(ans.getImageName());
                arq.setHasText(ans.isHasText());
                arq.setOptionNumber(ans.getOptionNumber());
                this.answerRequests.add(arq);
            });
        }
    }

    public Long getId() {
        return id;
    }

    public boolean isHasAnimatedGif() {
        return hasAnimatedGif;
    }

    public String getVideoName() {
        return videoName;
    }

    public String getDescription() {
        return description;
    }

    public boolean isHasAudio() {
        return hasAudio;
    }

    public String getAudioName() {
        return audioName;
    }

    public boolean isHasImage() {
        return hasImage;
    }

    public String getImageName() {
        return imageName;
    }

    public String getQuestionType() {
        return questionType;
    }

    public List<AnswerResponseForTaker> getAnswerRequests() {
        return answerRequests;
    }

    public GroupTextResponse getGroup() {
        return group;
    }

    public boolean isHasValidation() {
        return hasValidation;
    }

    public int getMinLength() {
        return minLength;
    }

    public int getMaxLength() {
        return maxLength;
    }

    public boolean isSpecialCharAllowed() {
        return specialCharAllowed;
    }

    public boolean isAlphabetAllowed() {
        return alphabetAllowed;
    }

    public boolean isNumericAllowed() {
        return numericAllowed;
    }

    public boolean isHideOptionsUntilVideoEnded() {
        return hideOptionsUntilVideoEnded;
    }

    public String getGroupName()
    {
        return groupName;
    }

    public Integer getSequence()
    {
        return sequence;
    }

    public void setSequence(Integer sequence)
    {
        this.sequence = sequence;
    }
}
