package com.synlabs.intscale.view.question;

import com.synlabs.intscale.entity.test.SubTag;
import com.synlabs.intscale.enums.TagType;
import com.synlabs.intscale.view.Request;

import java.util.List;

public class SubTagRequest implements Request
{
  private String name;
  private Long id;
  private Long tagId;
  private TagType type;

  public String getName()
  {
    return name;
  }

  public Long getId()
  {
    return id;
  }

  public Long getTagId()
  {
    return tagId;
  }

  public TagType getType()
  {
    return type;
  }

  public SubTag toEntity(SubTag subTag)
  {
    if(subTag==null) subTag=new SubTag();
    subTag.setName(this.name);
    subTag.setType(this.type);
    return subTag;
  }
}
