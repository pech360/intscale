package com.synlabs.intscale.view.question;

import com.synlabs.intscale.entity.test.QuestionTag;
import com.synlabs.intscale.entity.test.SubTag;
import com.synlabs.intscale.enums.TagType;
import com.synlabs.intscale.view.Response;

import java.util.ArrayList;
import java.util.List;

public class QuestionTagResponse implements Response
{
  private String name;
  private Long id;
  private List<SubTagResponse> subTags=new ArrayList<>();
  private TagType type;


  public QuestionTagResponse(QuestionTag questionTag){
    this.id= questionTag.getId();
    this.name=questionTag.getName();
    for (SubTag subTag:questionTag.getSubTags())
    {
      this.subTags.add(new SubTagResponse(subTag));
    }
    this.type = questionTag.getType();
  }

  public void setSubTags(List<SubTagResponse> subTags)
  {
    this.subTags = subTags;
  }

  public String getName()
  {
    return name;
  }

  public Long getId()
  {
    return id;
  }

  public List<SubTagResponse> getSubTags()
  {
    return subTags;
  }

  public TagType getType()
  {
    return type;
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o)
    {
      return true;
    }
    if (o == null || getClass() != o.getClass())
    {
      return false;
    }

    QuestionTagResponse that = (QuestionTagResponse) o;

    return id.equals(that.id);
  }

  @Override
  public int hashCode()
  {
    return id.hashCode();
  }
}
