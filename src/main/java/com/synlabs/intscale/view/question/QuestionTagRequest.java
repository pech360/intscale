package com.synlabs.intscale.view.question;

import com.synlabs.intscale.entity.test.QuestionTag;
import com.synlabs.intscale.enums.TagType;
import com.synlabs.intscale.view.Request;

public class QuestionTagRequest implements Request
{
  private Long id;
  private String name;
  private TagType type;

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public TagType getType()
  {
    return type;
  }

  public QuestionTag toEntity(QuestionTag questionTag)
  {
    if(questionTag==null){
      questionTag=new QuestionTag();
    }
    questionTag.setName(name);
    questionTag.setType(this.type);
    return questionTag;
  }
}
