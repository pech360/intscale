package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.Dashboard;

public class DashboardResponse implements Response
{
  private Long id;
  private String name;
  private String description;
  private int assignedMaleCount;
  private int assignedFeMaleCount;
  private int remainingMaleCount;
  private int remainingFeMaleCount;
  private boolean isSuperAdminDashboard;
  private boolean isOperationHeadDashboard;
  private boolean isOperationManagerDashboard;
  private boolean isSchoolHeadDashboard;
  private boolean isFieldManagerDashboard;

  public Long getId()
  {
    return id;
  }

  public String getName()
  {
    return name;
  }

  public String getDescription()
  {
    return description;
  }

  public DashboardResponse(){}

  public DashboardResponse(Dashboard dashboard)
  {
    this.id = dashboard.getId();
    this.name = dashboard.getName();
    this.description = dashboard.getDescription();
  }

  public int getAssignedMaleCount() {
    return assignedMaleCount;
  }

  public void setAssignedMaleCount(int assignedMaleCount) {
    this.assignedMaleCount = assignedMaleCount;
  }

  public int getAssignedFeMaleCount() {
    return assignedFeMaleCount;
  }

  public void setAssignedFeMaleCount(int assignedFeMaleCount) {
    this.assignedFeMaleCount = assignedFeMaleCount;
  }

  public int getRemainingMaleCount() {
    return remainingMaleCount;
  }

  public void setRemainingMaleCount(int remainingMaleCount) {
    this.remainingMaleCount = remainingMaleCount;
  }

  public int getRemainingFeMaleCount() {
    return remainingFeMaleCount;
  }

  public void setRemainingFeMaleCount(int remainingFeMaleCount) {
    this.remainingFeMaleCount = remainingFeMaleCount;
  }

  public boolean isSuperAdminDashboard() {
    return isSuperAdminDashboard;
  }

  public void setSuperAdminDashboard(boolean superAdminDashboard) {
    isSuperAdminDashboard = superAdminDashboard;
  }

  public boolean isOperationHeadDashboard() {
    return isOperationHeadDashboard;
  }

  public void setOperationHeadDashboard(boolean operationHeadDashboard) {
    isOperationHeadDashboard = operationHeadDashboard;
  }

  public boolean isFieldManagerDashboard() {
    return isFieldManagerDashboard;
  }

  public void setFieldManagerDashboard(boolean fieldManagerDashboard) {
    isFieldManagerDashboard = fieldManagerDashboard;
  }

  public boolean isOperationManagerDashboard() {
    return isOperationManagerDashboard;
  }

  public void setOperationManagerDashboard(boolean operationManagerDashboard) {
    isOperationManagerDashboard = operationManagerDashboard;
  }

  public boolean isSchoolHeadDashboard() {
    return isSchoolHeadDashboard;
  }

  public void setSchoolHeadDashboard(boolean schoolHeadDashboard) {
    isSchoolHeadDashboard = schoolHeadDashboard;
  }
}
