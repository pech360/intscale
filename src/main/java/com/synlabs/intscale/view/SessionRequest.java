package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.user.Session;

/**
 * Created by India on 1/3/2018.
 */
public class SessionRequest implements Request {
    private Long id;
    private String session;
    private boolean active;
    public Session toEntity(Session session){
        if(session==null){session=new Session();}
        session.setSession(this.session);
        session.setActive(this.active);
        return session;
    }
    public Session toEntity(){
        return toEntity(new Session());
    }

    public Long getId() {
        return id;
    }

    public String getSession() {
        return session;
    }

    public boolean isActive() {
        return active;
    }
}
