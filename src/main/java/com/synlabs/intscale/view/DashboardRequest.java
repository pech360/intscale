package com.synlabs.intscale.view;

import java.util.HashSet;
import java.util.Set;

public class DashboardRequest implements Request
{
  private Set<Long> products   = new HashSet<>();
  private Set<Long> tests   = new HashSet<>();
  private Set<Long> tenants = new HashSet<>();
  private Set<String> grades  = new HashSet<>();
  private Set<String> sections  = new HashSet<>();
  private Set<String> genders  = new HashSet<>();
  private Set<String> schools = new HashSet<>();
  private Set<String> cities = new HashSet<>();
  private Set<Long> subTags = new HashSet<>();
  private Set<Long> userIds = new HashSet<>();
  private String fromDate;
  private String toDate;
  private String resultType;

  public Set<Long> getProducts()
  {
    return products;
  }

  public Set<Long> getTests()
  {
    return tests;
  }

  public Set<Long> getTenants()
  {
    return tenants;
  }

  public Set<String> getGrades()
  {
    return grades;
  }

  public Set<String> getGenders()
  {
    return genders;
  }

  public Set<String> getSchools()
  {
    return schools;
  }

  public Set<String> getCities()
  {
    return cities;
  }

  public Set<Long> getSubTags()
  {
    return subTags;
  }

  public String getFromDate()
  {
    return fromDate;
  }

  public String getToDate()
  {
    return toDate;
  }

  public Set<Long> getUserIds()
  {
    return userIds;
  }

  public String getResultType()
  {
    return resultType;
  }

  public Set<String> getSections()
  {
    return sections;
  }
}
