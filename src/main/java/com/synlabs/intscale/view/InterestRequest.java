package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.masterdata.Interest;

import java.util.List;

public class InterestRequest implements Request
{
  private Long   id;
  private String name;
  private String parent;
  private String image;

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getParent()
  {
    return parent;
  }

  public void setParent(String parent)
  {
    this.parent = parent;
  }

  public String getImage()
  {
    return image;
  }

  public void setImage(String image)
  {
    this.image = image;
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public Interest toEntity(Interest interest)
  {
    if (interest == null)
    {
      interest = new Interest();
    }
      interest.setName(this.name);
      interest.setImage(this.image);
    return interest;
  }

  public Interest toEntity()
  {
    return toEntity(new Interest());
  }
}
