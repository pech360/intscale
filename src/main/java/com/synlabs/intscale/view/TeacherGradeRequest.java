package com.synlabs.intscale.view;

/**
 * Created by India on 1/9/2018.
 */
public class TeacherGradeRequest implements Request {
    private Long[] teacherIds;
    private Long grade;
    private String section;

    public Long[] getTeacherIds() {
        return teacherIds;
    }

    public Long getGrade() {
        return grade;
    }

    public String getSection()
    {
        return section;
    }
}
