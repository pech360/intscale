package com.synlabs.intscale.view;

public class TenantProductRequest implements Request {
    private Long tenantIds[];

    public Long[] getTenantIds() {
        return tenantIds;
    }

    public void setTenantIds(Long[] tenantIds) {
        this.tenantIds = tenantIds;
    }

    public Long[] getProductIds() {
        return productIds;
    }

    public void setProductIds(Long[] productIds) {
        this.productIds = productIds;
    }

    private Long productIds[];

}
