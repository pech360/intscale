package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.Report.Category;
import com.synlabs.intscale.view.report.NormResponse;
import com.synlabs.intscale.view.report.ScoreRangeResponse;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by itrs on 7/25/2017.
 */
public class CategoryResponse implements Response
{
  private Long       id;
  private String     name;
  private String     displayName;
  private String     description;
  private String     color;
  private String     categoryName;
  private Long       parentId;
  private String     highText;
  private String     lowText;
  private BigDecimal meanScore;
  private BigDecimal standardDeviation;
  private BigDecimal tScore;
  private BigDecimal rawScore;
  private BigDecimal percentile;
  private Integer    sequence;
  private String     shortHighText;
  private String     shortLowText;
  private Integer                  testScore     = 0;
  private List<FeedbackResponse>   feedbacks     = new ArrayList<>();
  private List<ScoreRangeResponse> scoreRanges   = new ArrayList<>();
  private List<CategoryResponse>   subCategories = new ArrayList<>();
  private List<NormResponse>       norms         = new ArrayList<>();

  public enum ResponseType
  {
    Full, Summary, Default
  }

  public CategoryResponse(Category category, ResponseType type)
  {
    type = type == null ? ResponseType.Default : type;
    switch (type)
    {
      case Full:
        List<Category> subCategories = category.getSubCategories();
        if (!CollectionUtils.isEmpty(subCategories))
        {
          subCategories.forEach(c -> {
            this.subCategories.add(new CategoryResponse(c, ResponseType.Summary));
          });
        }
      case Default:
        this.color = category.getColor();
        this.parentId = category.getParent() == null ? 0L : category.getParent().getId();
        this.categoryName = category.getParent() == null ? " " : category.getParent().getName();
        category.getFeedbacks().forEach(f -> {
          this.feedbacks.add(new FeedbackResponse(f));
        });
//        category.getScoreRanges().forEach(scoreRange -> {
//          this.scoreRanges.add(new ScoreRangeResponse(scoreRange));
//        });
        this.lowText = category.getLowText();
        this.highText = category.getHighText();
//        this.meanScore = category.getMeanScore();
//        this.standardDeviation = category.getStandardDeviation();
        this.displayName = category.getDisplayName();
        this.sequence = category.getSequence();
        this.shortHighText = category.getShortHighText();
        this.shortLowText = category.getShortLowText();
        this.norms = category.getNorms().stream().map(NormResponse::new).collect(Collectors.toList());
      case Summary:
        this.name = category.getName();
        this.description = category.getDescription();
        this.id = category.getId();
    }
  }

  public CategoryResponse(Category category)
  {
    this(category, ResponseType.Default);
  }

  public Long getId()
  {
    return id;
  }

  public String getName()
  {
    return name;
  }

  public String getDescription()
  {
    return description;
  }

  public Long getParentId()
  {
    return parentId;
  }

  public String getCategoryName()
  {
    return categoryName;
  }

  public String getColor()
  {
    return color;
  }

  public List<FeedbackResponse> getFeedbacks()
  {
    return feedbacks;
  }

  public String getHighText()
  {
    return highText;
  }

  public String getLowText()
  {
    return lowText;
  }

  public List<ScoreRangeResponse> getScoreRanges()
  {
    return scoreRanges;
  }

  public BigDecimal getMeanScore()
  {
    return meanScore;
  }

  public BigDecimal getStandardDeviation()
  {
    return standardDeviation;
  }

  public BigDecimal gettScore()
  {
    return tScore;
  }

  public void settScore(BigDecimal tScore)
  {
    this.tScore = tScore;
  }

  public BigDecimal getRawScore()
  {
    return rawScore;
  }

  public BigDecimal getPercentile()
  {
    return percentile;
  }

  public void setRawScore(BigDecimal rawScore)
  {
    this.rawScore = rawScore;
  }

  public void setPercentile(BigDecimal percentile)
  {
    this.percentile = percentile;
  }

  public List<CategoryResponse> getSubCategories()
  {
    return subCategories;
  }

  public void setSubCategories(List<CategoryResponse> subCategories)
  {
    this.subCategories = subCategories;
  }

  public String getDisplayName()
  {
    return displayName;
  }

  public Integer getSequence()
  {
    return sequence;
  }

  public String getShortHighText()
  {
    return shortHighText;
  }

  public String getShortLowText()
  {
    return shortLowText;
  }

  public Integer getTestScore()
  {
    return testScore;
  }

  public void setTestScore(Integer testScore)
  {
    this.testScore = testScore;
  }

  public List<NormResponse> getNorms()
  {
    return norms;
  }
}
