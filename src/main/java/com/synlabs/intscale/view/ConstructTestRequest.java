package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.view.TestTreeResponse.TestSummaryResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by India on 1/29/2018.
 */
public class ConstructTestRequest implements Request {
    private Long id;
    private List<Long> tests=new ArrayList<>();

    public Long getId() {
        return id;
    }

    public List<Long> getTests() {
        return tests;
    }
}
