package com.synlabs.intscale.view;

import com.synlabs.intscale.entity.test.Product;
import com.synlabs.intscale.entity.test.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by India on 1/24/2018.
 */
public class ProductRequest implements Request {
    private Long id;
    private String name;
    private String description;
    private List<Long> test=new ArrayList<>();
    public Product toEntity(Product product){
        product.setName(this.name);
        product.setDescription(this.description);
        return product;
    }
    public Product toEntity(){
        return toEntity(new Product());
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<Long> getTest() {
        return test;
    }
}
