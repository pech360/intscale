package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.user.Role;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.view.PlatformDailySummary;

import org.joda.time.LocalDateTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.StreamSupport;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	@Query(value = "select * from ebdb.user where username=?1 and tenant_id=?2", nativeQuery = true)
	User getByUsernameAndTenant(String username, Tenant tenant);

	User findByUsernameAndTenant(String username, Tenant tenant);

	User findByContactNumberAndTenant(String contactNum, Tenant tenant);

	User findByEmailAndTenant(String email, Tenant tenant);

	User findByName(String name);

	int countByEmailAndTenant(String email, Tenant tenant);

	int countByUsernameAndTenant(String username, Tenant tenant);

	int countByContactNumberAndTenant(String contactNum, Tenant tenant);

	User findByUsernameOrEmailAndTenant(String username, String email, Tenant tenant);

	User findByUsernameOrEmail(String username, String email);

	// List<User> findAllByRolesIdAndTenantId(Long id, Long tenantid);

	@Query(value = "select * from ebdb.user where register_type='Google' or register_type='Facebook'", nativeQuery = true)
	List<User> findAllRecords();

	List<User> findAllByContactNumberAndTenant(String contactNumber, Tenant tenant);

	List<User> findAllByRolesIdAndTenant(Long id, Tenant tenant);

	List<User> findAllByRolesIdAndSchoolNameAndGradeAndTenant(Long roleId, String schoolName, String grade,
			Tenant tenant);

	List<User> findAllBySchoolNameAndTenant(String schoolName, Tenant tenant);

	List<User> findAllByRolesIdAndSchoolNameAndTenant(Long roleId, String schoolName, Tenant tenant);

	List<User> findAllByRolesIdAndGradeAndTenant(Long roleId, String grade, Tenant tenant);

	List<User> findAllByGradeAndTenant(String grade, Tenant tenant);

	List<User> findAllBySchoolNameAndGradeAndTenant(String schoolName, String grade, Tenant tenant);

	List<User> findAllByTenant(Tenant tenant);

	List<User> findAllByRolesIdAndTenantId(Long roleId, Long tenantId);

	List<User> findAllByTenantId(Long tenantId);

	User findOneByUsernameAndTenant(String username, Tenant tenant);

	User findOneByEmailAndTenant(String email, Tenant tenant);

	List<User> findAllByRolesAndTenant(Role testTaker, Tenant tenant);

	User findOneByIdAndTenant(Long userId, Tenant tenant);

	List<User> findAllBySchoolNameAndGradeAndGenderAndTenant(String schoolName, String grade, String gender,
			Tenant tenant);

	List<User> findAllBySchoolNameAndGenderAndTenant(String schoolName, String gender, Tenant tenant);

	List<User> findAllByGradeAndGenderAndTenant(String grade, String gender, Tenant tenant);

	List<User> findAllByGenderAndTenant(String gender, Tenant tenant);

	List<User> findAllByRolesIdAndGradeAndSectionAndTenant(Long id, String grade, String section, Tenant tenant);

	List<User> findAllByGradeAndSectionAndTenant(String grade, String section, Tenant tenant);

	User findByUsernameAndTenantOrEmailAndTenant(String username, Tenant tenant, String username1, Tenant tenant1);

	// List<User> findAllByTenantId(Long tenantid);
	List<User> findAllByIdInAndTenant(List<Long> longs, Tenant tenant);

	List<User> findAllByTenantAndSchoolNameAndGradeAndCounsellorId(Tenant tenant, String schoolId, String grade,
			Long counsellorId);

	List<User> findAllByTenantAndSchoolNameAndCounsellorId(Tenant tenant, String schoolId, Long counsellorId);

	List<User> findAllByTenantAndGradeAndCounsellorId(Tenant tenant, String grade, Long counsellorId);

	List<User> findAllByTenantAndSchoolNameAndGrade(Tenant tenant, String schoolId, String grade);

	List<User> findAllByTenantAndSchoolName(Tenant tenant, String schoolId);

	List<User> findAllByTenantAndGrade(Tenant tenant, String grade);

	List<User> findAllByTenantAndCounsellorId(Tenant tenant, Long counsellorId);

	List<User> findAllBySchoolName(String schoolName);

	List<User> findAllByCity(String city);

	// List<User> findAllByFlagAndIsPaidAndTenant(String flag,Boolean isPaid,Tenant
	// tentant);
	List<User> findAllByFlagAndIsPaid(String flag, Boolean isPaid);

	List<User> findAllByUsernameInAndTenant(List<String> usernames, Tenant tenant);

//	@Query(value="select u.name as name,u.username as userName,u.email as email ,u.contact_number as phoneNumber from ebdb.user u inner join ebdb.user_test ut on ut.user_email_id = u.id inner join ebdb.user_test_tests utt on utt.user_test_id = ut.id inner join ebdb.test t on utt.tests_id = t.id where ut.last_modified_date>=?1 and ut.last_modified_date<=?2 and t.paid= false" ,nativeQuery = true)
//	List<Object[]> findAllSignedUpAndGivenDemoTest(Date from, Date to);

	//LNT
	@Query(value="select u.name,u.username as userName,u.email,u.contact_number as phoneNumber from ebdb.user u left join ebdb.test_result tr on tr.user_id = u.id where tr.id is null and u.created_date>=?1 and u.created_date<=?2" , nativeQuery = true)
	List<Object[]> findAllSignedUpAndNotGivenDemoTest(Date from, Date to);
	@Query(value="select u.name,u.username,u.email,u.contact_number,te.product_name as activity from ebdb.user u inner join ebdb.transaction_entry te on te.user_id = u.id where te.created_at >= ?1 and te.created_at <=?2", nativeQuery = true)
	List<Object[]> findAllPurchasedProductToday(Date from, Date to );

}