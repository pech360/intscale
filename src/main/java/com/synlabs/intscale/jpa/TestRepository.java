package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.test.TestResult;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@Repository
public interface TestRepository extends JpaRepository<Test, Long>
{

  Test findByQuestionsIdAndTenant(Long id, Tenant tenant);

  List<Test> findAllByPublishedIsTrueAndTenant(Tenant tenant);

  List<Test> findAllByPublishedIsTrueAndIdInAndTenant(List<Long> testIds, Tenant tenant);

  List<Test> findAllByNameInAndTenant(List<String> testNames, Tenant tenant);

  List<Test> findAllByPublishedIsTrueAndPaidIsFalseAndTenant(Tenant tenant);

  List<Test> findAllByVersionIdAndTenant(Long versionId, Tenant tenant);

  List<Test> findAllByLatestVersionIsTrueAndTenant(Tenant tenant);

  List<Test> findAllByLatestVersionIsTrueAndParentIdIsNullAndTenant(Tenant tenant);

  List<Test> findAllByLatestVersionIsTrueAndParentIdAndTenant(Long id, Tenant tenant);

  List<Test> findAllByIdInAndTenant(List<Long> testId, Tenant tenant);

  List<Test> findAllByParentIdIsNullAndConstructIsTrueAndTenant(Tenant tenant);

  List<Test> findAllByConstructIsTrueAndParentIdAndTenant(Long id, Tenant tenant);

  List<Test> findAllByConstructIsTrueAndTenant(Tenant tenant);

  List<Test> findAllByConstructIsTrueAndLeafNodeIsTrueAndTenant(Tenant tenant);

  List<Test> findAllByConstructIsTrueAndParentIdIsNullAndTenant(Tenant tenant);

  List<Test> findAllByParentIdAndTenant(Long id, Tenant tenant);

  List<Test> findAllByLatestVersionIsTrueAndParentIdAndTenantOrConstructIsTrueAndParentIdAndTenant(Long id, Tenant tenant, Long id1, Tenant tenant1);

  List<Test> findAllByLatestVersionIsTrueAndParentIdAndIdNotInAndTenant(Long id, List<Long> tests, Tenant tenant);

  int countByLatestVersionIsTrueAndParentIdAndTenant(Long id, Tenant tenant);

  List<Test> findAllByPublishedIsTrueAndPaidIsFalseAndGoLiveIsTrueAndLatestVersionIsTrueAndTenant(Tenant tenant);
  List<Test> findAllByPublishedIsTrueAndPaidIsFalseAndGoLiveIsTrueAndLatestVersionIsTrueAndTenantAndLanguage(Tenant tenant, String language);
  //Add by Ajay
  List<Test> findAllByPublishedIsTrueAndPaidIsTrueAndGoLiveIsTrueAndLatestVersionIsTrueAndTenantAndLanguage(Tenant tenant, String language);

  List<Test> findAllByLatestVersionIsTrueAndPublishedIsTrueAndNameInAndAndTenant(List<String> testNames, Tenant tenant);

  Test findByIdAndTenant(Long testId, Tenant tenant);

  List<Test> findAllByLatestVersionIsTrueAndParentIdIsNullAndPublishedIsTrueAndTenant(Tenant tenant);

  List<Test> findAllByLatestVersionIsTrueAndParentIdAndPaidIsFalseAndPublishedIsTrueAndTenant(Long id, Tenant tenant);

  List<Test> findAllByLatestVersionIsTrueAndPaidIsTrueAndGoLiveIsTrueAndParentIdAndTenantOrConstructIsTrueAndParentIdAndTenant(Long id, Tenant tenant, Long id1, Tenant tenant1);

  List<Test> findAllByLatestVersionIsTrueAndParentIdAndGoLiveIsFalseAndPublishedIsTrueAndTenant(Long id,Tenant tenant);
  List<Test> findAllByLatestVersionIsTrueAndParentIdAndGoLiveIsFalseAndPublishedIsTrueAndTenantAndLanguage(Long id,Tenant tenant, String language);

  List<Test> findAllByLatestVersionIsTrueAndParentIdIsNotNullAndGoLiveIsFalseAndPublishedIsTrueAndTenant(Tenant tenant);
  List<Test> findAllByLatestVersionIsTrueAndParentIdIsNotNullAndGoLiveIsFalseAndPublishedIsTrueAndTenantAndLanguage(Tenant tenant, String language);

  Test findAllByLatestVersionIsTrueAndVersionId(Long parentTestId);

  List<Test> findAllByNameAndTenant(String name, Tenant tenant);

  List<Test> findAllByLeafNodeIsFalseAndConstructIsFalseAndParentIsNotNullAndTenant(Tenant tenant);

  Test findByNameAndVersionAndTenant(String testName, Long testVersion, Tenant tenant);

  List<Test> findAllByPublishedIsTrueAndIdIn(List<Long> testIds);

  List<Test> findAllByParentId(Long id);
  List<Test> findAllByParentIdAndLanguage(Long id, String language);

  int countByLatestVersionIsTrueAndParentId(Long id);
  int countByLatestVersionIsTrueAndParentIdAndLanguage(Long id, String language);

  List<Test> findAllByVersionId(Long id);

  List<Test> findAllByPublishedIsTrueAndPaidIsFalseAndGoLiveIsTrueAndLatestVersionIsTrue();

  List<Test> findAllByConstructIsTrueAndParentId(Long productId);

  List<Test> findAllByLatestVersionIsTrueAndParentId(Long constructId);

  @Query(value = "SELECT NAME FROM test WHERE id = ?1;", nativeQuery = true)
  String getTestName(Long id);

}