package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.masterdata.Blog;
import com.synlabs.intscale.entity.masterdata.Interest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BlogRepository extends JpaRepository<Blog, Long>
{
  List<Blog> findAllByInterestsIn(List<Interest> interests);
}
