package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.test.Product;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by India on 1/24/2018.
 */
@Repository
public interface ProductRepository extends JpaRepository<Product,Long> {
    List<Product> findAllByTenant(Tenant tenant);

    List<Product> findAllByTenantAndIdIn(Tenant tenant, List<Long> productIds);

    Product findOneByNameAndTenant(String name, Tenant tenant);

    List<Product> findAllByIdIn(List<Long> productIds);

    List<Product> findAllByTestId(Long testId);
}
