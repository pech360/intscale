package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.test.GroupText;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by India on 10/9/2017.
 */
public interface GroupTextRepository extends JpaRepository<GroupText,Long> {
    public GroupText findByGroupNameAndTenant(String groupName,Tenant tenant);
}
