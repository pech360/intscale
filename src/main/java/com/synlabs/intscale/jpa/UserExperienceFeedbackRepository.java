package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.masterdata.UserExperienceFeedback;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserExperienceFeedbackRepository extends JpaRepository<UserExperienceFeedback, Long>
{

}
