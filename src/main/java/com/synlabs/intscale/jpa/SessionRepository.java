package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.user.Session;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by India on 1/5/2018.
 */
@Repository
public interface SessionRepository extends JpaRepository<Session,Long> {
    List<Session> findAllByTenant(Tenant tenant);
}
