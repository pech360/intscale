package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.test.QuestionTag;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.enums.TagType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionTagRepository extends JpaRepository<QuestionTag , Long>
{

  List<QuestionTag> findAllByTenant(Tenant tenant);

  QuestionTag findByNameAndTenant(String name, Tenant tenant);

  List<QuestionTag> findAllByTypeAndTenant(TagType type, Tenant tenant);
}
