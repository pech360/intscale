package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.Report.InterestInventory;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InterestInventoryRepository extends JpaRepository<InterestInventory, Long> {

    InterestInventory findByIdAndTenant(Long id, Tenant tenant);
    List<InterestInventory> findAllByTenant(Tenant tenant);
}
