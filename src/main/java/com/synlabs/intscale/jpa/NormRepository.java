package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.Report.Norm;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NormRepository extends JpaRepository<Norm, Long>
{
  Norm findByCategoryIdAndLanguageAndTenant(Long categoryId, String language, Tenant tenant);

  Norm findByReportSectionIdAndLanguageAndTenant(Long reportSectionId, String value, Tenant tenant);
}
