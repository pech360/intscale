package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.test.QuestionTag;
import com.synlabs.intscale.entity.test.SubTag;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.enums.TagType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface SubTagRepository extends JpaRepository<SubTag,Long>
{
 SubTag findByNameAndTenant(String name, Tenant tenant);

 List<SubTag> findAllByTenant(Tenant tenant);

 SubTag findByNameAndTagAndTenant(String name, QuestionTag tag, Tenant tenant);

  List<SubTag> findAllByTypeAndTenant(TagType question, Tenant tenant);

  List<SubTag> findAllByIdInAndTenant(Set<Long> ids, Tenant tenant);
}
