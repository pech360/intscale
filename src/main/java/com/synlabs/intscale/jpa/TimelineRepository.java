package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.Timeline;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TimelineRepository extends JpaRepository<Timeline, Long> {
    List<Timeline> findAllByUser(User user);

    List<Timeline> findAllByUserIdAndContentType(Long id, String contentType);

    List<Timeline> findAllByContentTypeAndUserAndTenant(String type, User user, Tenant tenant);

    int countByUserAndContentType(User user, String contentType);

}
