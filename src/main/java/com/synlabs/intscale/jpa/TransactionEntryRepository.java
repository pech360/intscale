package com.synlabs.intscale.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.synlabs.intscale.entity.paymentGateway.TransactionEntry;

@Repository
public interface TransactionEntryRepository extends JpaRepository<TransactionEntry, Long> {
	
	public TransactionEntry findByPayId(String payId);

	public Boolean existsByUserIdAndProductNameAndPromoCodeName(Long userId, String productName, String promoCodeName);
	//LNT
	public List<TransactionEntry> findAllByPromoCodeName(String promoCodeName);
	@Query(value="select product_name from ebdb.transaction_entry where promo_code_name = ?1", nativeQuery = true)
	public List<String> findDistinctProductNameByPromoCodeName(String promoCodeName);
	
}
