package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.test.TextquestionAnswers;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by India on 5/9/2018.
 */
public interface TextquestionAnswersRepository extends JpaRepository<TextquestionAnswers,Long> {
    List<TextquestionAnswers> findAllByTestIdAndTenant(Long testId, Tenant tenant);

    List<TextquestionAnswers> findAllByTenant(Tenant tenant);
}
