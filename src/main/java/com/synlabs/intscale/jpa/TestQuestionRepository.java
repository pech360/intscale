package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.Report.Category;
import com.synlabs.intscale.entity.test.TestQuestion;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TestQuestionRepository extends JpaRepository<TestQuestion, Long>
{
  TestQuestion findByTestIdAndQuestionIdAndTenant(long testId, long questionId, Tenant tenant);

  List<TestQuestion> findByTestIdAndTenantOrderBySequenceAsc(Long id, Tenant tenant);

  List<TestQuestion> findAllByCategoryAndTestIdAndTenant(Category category, Long id, Tenant tenant);

  List<TestQuestion> findAllBySubCategoryAndTestIdAndTenant(Category category, Long id, Tenant tenant);

}
