package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.Report.Category;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long>
{

  List<Category> findAllByParentIdAndTenant(Long id, Tenant tenant);

  List<Category> findAllByActiveAndTenant(boolean active, Tenant tenant);

  //@Query(value = "select not in (?3)",nativeQuery = true)
  @Query(value = "select * from category c left join test_question tq on tq.test_id=?1 left join test_question_category tqc on tqc.test_question_id=tq.id where c.id=tqc.category_id and c.tenant_id=?2 group by c.name", nativeQuery = true)
  List<Category> findAllByTestIdAndTenant(Long id, Long tenantId);

  @Query(value = "select * from category c left join test_question tq on tq.test_id=?1 left join test_question_subcategory tqc on tqc.test_question_id=tq.id where c.id=tqc.sub_category_id and c.tenant_id=?2 group by c.name", nativeQuery = true)
  List<Category> findAllSubCategoryByTestIdAndTenant(Long id, Long tenantId);

  Category findByNameAndTenant(String name, Tenant tenant);

  List<Category> findAllByParentIdAndActiveAndTenant(Long id, boolean active, Tenant tenant);

  List<Category> findAllByParentNameAndActiveAndTenant(String name, boolean active, Tenant tenant);

  Category findByNameAndTenantAndParent(String categoryName, Tenant tenant, Category category);

  List<Category> findAllByTenant(Tenant tenant);
}
