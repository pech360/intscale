package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.user.Role;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by itrs on 5/31/2017.
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long>
{
    Role getOneByName(String name);
}
