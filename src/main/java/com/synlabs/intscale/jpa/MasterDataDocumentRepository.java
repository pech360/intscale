package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.masterdata.MasterDataDocument;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by itrs on 7/25/2017.
 */
@Repository
public interface MasterDataDocumentRepository extends JpaRepository<MasterDataDocument, Long>
{
  List<MasterDataDocument> findAllByType(String type);

  MasterDataDocument findOneByValue(String group);

  MasterDataDocument findOneByTypeAndValue(String type, String value);

  MasterDataDocument findByIdAndType(Long gradeId,String type);

  List<MasterDataDocument> findAllByTypeAndTenant(String type, Tenant tenant);

  MasterDataDocument findOneByTypeAndValueAndTenant(String type, String value, Tenant tenant);
}
