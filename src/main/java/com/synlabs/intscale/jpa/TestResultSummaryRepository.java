package com.synlabs.intscale.jpa;

import com.querydsl.core.Tuple;
import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.test.TestResultSummary;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.enums.RecordType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

public interface TestResultSummaryRepository extends JpaRepository<TestResultSummary, Long>
{

  TestResultSummary findOneByUserIdAndConstructIdAndRecordTypeAndTenant(Long userId, Long constructId, RecordType type, Tenant tenant);


  @Query(value = "SELECT AVG(percentage_score) FROM test_result_summary WHERE user_id = ?1 AND construct_id = ?2 AND record_type ='Test' AND tenant_id =?3", nativeQuery = true)
  double getAveragePercentageScore(long userid,long construct, Long tenant);

  @Query(value = "SELECT DISTINCT construct_id FROM test_result_summary WHERE grade = ?1 AND tenant_id = ?2", nativeQuery = true)
  List<Long> getDistinctconstructs(String grade);

  @Query(value = "SELECT * FROM test_result_summary INNER JOIN (SELECT construct_id, MAX(syllabus_completed) AS Maxsyllabus_completed FROM test_result_summary GROUP BY construct_id) topsyllabus_completed ON test_result_summary.construct_id = topsyllabus_completed.construct_id AND test_result_summary.syllabus_completed = topsyllabus_completed.maxsyllabus_completed WHERE tenant_id = ?1 ;", nativeQuery = true)
  List<TestResultSummary> getDistinctTopBySyllabusCompleted(long tenant);

  @Query(value = "SELECT * FROM test_result_summary WHERE record_type='Test' AND tenant_id = ?1 GROUP BY test_id;", nativeQuery = true)
  List<TestResultSummary> getDistinctByTestId(long tenant);

  @Query(value = "SELECT * FROM test_result_summary WHERE record_type = 'construct' AND grade = ?1 AND section = ?2 AND tenant_id = ?3 GROUP BY user_id ORDER BY construct_name;", nativeQuery = true)
  List<TestResultSummary> getUserRecordsByConstructType(String grade, String section, Long tenant);



  TestResultSummary findTestResultSummaryByTestResultId(Long testResultId);
}
