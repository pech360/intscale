package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.masterdata.MasterDataDocument;
import com.synlabs.intscale.entity.user.Teacher;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by India on 1/5/2018.
 */
@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Long>
{
  List<Teacher> findAllByTenant(Tenant tenant);

  List<Teacher> findAllByTenantAndSchoolIdAndGradeAndClassTeacherIsTrue(Tenant tenant, Long schoolId, String grade);

  List<Teacher> findAllByTenantAndSchoolIdAndClassTeacherIsTrue(Tenant tenant, Long schoolId);

  List<Teacher> findAllByTenantAndGradeAndClassTeacherIsTrue(Tenant tenant, String grade);

  List<Teacher> findAllByTenantAndClassTeacherIsTrue(Tenant tenant);

  List<Teacher> findAllByTenantAndSubjectTeacherIsTrue(Tenant tenant);

  List<Teacher> findAllByTenantAndUserIdIsNotNull(Tenant tenant);

  Teacher findOneByUserIdAndTenant(Long id, Tenant tenant);
}
