package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.test.Question;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by vikas kumar on 15-06-2017.
 */
public interface QuestionRepository extends JpaRepository<Question, Long>
{

  Question findFirstByGroupNameAndActiveAndTenant(String groupName,boolean active,Tenant tenant);
  List<Question> findAllByActiveAndTenant(boolean active,Tenant tenant);
  List<Question> findAllByQuestionTypeAndActiveAndTenant(String questionType,boolean active,Tenant tenant);
  List<Question> findAllByQuestionTypeAndSubTagsIdAndActiveAndTenant(String questionType,Long id,boolean active,Tenant tenant);
  List<Question> findAllBySubTagsIdAndActiveAndTenant(Long id,boolean active,Tenant tenant);
  List<Question> findAllByGroupNameAndTenant(String groupName, Tenant tenant);

  Question findByIdAndTenant(Long questionId, Tenant tenant);

}
