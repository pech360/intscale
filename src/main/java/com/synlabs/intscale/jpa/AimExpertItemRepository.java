package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.Report.AimExpertInventory;
import com.synlabs.intscale.entity.Report.AimExpertItem;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AimExpertItemRepository extends JpaRepository<AimExpertItem, Long>
{
  AimExpertItem findByIdAndTenant(Long id, Tenant tenant);
}
