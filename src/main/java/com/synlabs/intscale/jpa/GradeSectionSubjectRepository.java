package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.masterdata.GradeSectionSubject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GradeSectionSubjectRepository extends JpaRepository<GradeSectionSubject, Long>
{

}
