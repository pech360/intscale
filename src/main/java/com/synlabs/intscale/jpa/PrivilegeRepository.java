package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.user.Privilege;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by itrs on 6/13/2017.
 */
public interface PrivilegeRepository extends JpaRepository<Privilege, Long>
{
    Privilege findByName(String name);
}
