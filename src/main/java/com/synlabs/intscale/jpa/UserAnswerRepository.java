package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.test.Question;
import com.synlabs.intscale.entity.test.TestResult;
import com.synlabs.intscale.entity.test.UserAnswer;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.view.usertest.TestResponse;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

public interface UserAnswerRepository extends JpaRepository<UserAnswer, Long>
{
  List<UserAnswer> findAllByTestResultAndTenant(TestResult tr, Tenant tenant);

  UserAnswer findByTestResultAndQuestionAndTenant(TestResult testResult, Question question, Tenant tenant);

  Integer countByTestResultAndQuestionAndTenant(TestResult testResult, Question question, Tenant tenant);

  UserAnswer findByTestResultIdAndQuestionIdAndTenant(Long testResult, Long question, Tenant tenant);

  Integer countUserAnswersByTestResultAndTenantAndOptionNumber(TestResult testResult, Tenant tenant, int optionNumber);

  Integer countUserAnswersByTestResultAndTenantAndOptionNumberIsGreaterThan(TestResult testResult, Tenant tenant, int optionNumber);
}
