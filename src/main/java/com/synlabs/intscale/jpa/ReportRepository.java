package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.Report.Report;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long>
{
  List<Report> findAllByTenant(Tenant tenant);

  Report findOneByIdAndTenant(Long id, Tenant tenant);

  Report findByName(String reportName);

  Report findByNameAndTenant(String reportName, Tenant tenant);
}
