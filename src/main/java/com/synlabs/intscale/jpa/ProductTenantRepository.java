package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.ProductTenant;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.view.TenantProductResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductTenantRepository extends JpaRepository<ProductTenant,Long> {

    List<ProductTenant> findAllByDomain(Tenant tenant);

    ProductTenant findOneByDomainIdAndProductId(Long tenantId, Long productId);

    List<ProductTenant> findAllByTenant(Tenant tenant);

    List<ProductTenant> findAllByProductIdIn(List<Long> ids);

    List<ProductTenant> findAllByDomainAndProductIdIn(Tenant tenant,List<Long> productIds);
}
