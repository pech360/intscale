package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by itrs on 6/2/2017.
 */
@Repository
public interface TenantRepository extends JpaRepository<Tenant, Long>
{
    List<Tenant> findAllByActive(boolean active);

    Tenant findByActiveIsTrueAndSubDomain(String domain);

    Tenant findByActiveIsTrueAndName(String name);

    List<Tenant> findAllByActiveIsTrueAndIdNotIn(Long id);
    
    
}
