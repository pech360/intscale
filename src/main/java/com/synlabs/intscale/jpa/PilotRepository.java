package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.Pilot;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by India on 1/16/2018.
 */
@Repository
public interface PilotRepository extends JpaRepository<Pilot,Long> {
    List<Pilot> findAllByTenant(Tenant tenant);

    int countByNameAndTenant(String name, Tenant tenant);

    Pilot findTopByTenant(Tenant tenant);

    Pilot findOneByActiveAndTenant(boolean status, Tenant tenant);
}
