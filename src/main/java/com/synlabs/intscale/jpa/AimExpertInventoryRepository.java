package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.Report.AimExpertInventory;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AimExpertInventoryRepository extends JpaRepository<AimExpertInventory, Long>
{
  AimExpertInventory findByIdAndTenant(Long id, Tenant tenant);

  List<AimExpertInventory> findAllByTenant(Tenant tenant);

  List<AimExpertInventory> findAllByTypeAndTenant(String type, Tenant tenant);

  AimExpertInventory findByNameAndTenant(String aimExpertInventory, Tenant tenant);
}
