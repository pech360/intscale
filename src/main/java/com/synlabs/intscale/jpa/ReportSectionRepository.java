package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.Report.ReportSection;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReportSectionRepository extends JpaRepository<ReportSection, Long>
{

  List<ReportSection> findAllByTenant(Tenant tenant);

  ReportSection findByIdAndTenantId(Long id, Long tenant_id);

  ReportSection findByNameAndTenant(String name, Tenant tenant);
}
