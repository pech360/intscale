package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.masterdata.LikedContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LikedContentRepository extends JpaRepository<LikedContent, Long>
{
  LikedContent findByUserIdAndBlogId(Long userId, Long blogId);
}
