package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.test.TenantTestAllotment;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by itrs on 9/25/2017.
 */
public interface TenantTestAllotmentRepository extends JpaRepository<TenantTestAllotment,Long>
{
  TenantTestAllotment findByUserEmailAndTenant(String email,Tenant tenant);
}
