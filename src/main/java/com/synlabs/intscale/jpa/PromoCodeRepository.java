package com.synlabs.intscale.jpa;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.synlabs.intscale.entity.PromoCode;
//LNT
public interface PromoCodeRepository extends JpaRepository<PromoCode, Long> {

	PromoCode findByName(String name);

//	@Transactional
//	@Modifying
//	@Query(value = "update ebdb.promo_code p set p.is_valid = false where consumed_count >= total_count or CURRENT_TIMESTAMP() >= coupon_end_date or CURRENT_TIMESTAMP()< coupon_start_date or coupon_start_date>=coupon_end_date", nativeQuery = true)
//	int updatePromoCodeRefreshIsValidStatus();

	@Query(value = "select * from ebdb.promo_code where name =?1 and is_valid = true and consumed_count < total_count  and CURRENT_TIMESTAMP() > coupon_start_date and CURRENT_TIMESTAMP() < coupon_end_date", nativeQuery = true)
	PromoCode findIfIsValid(String name);

	@Query(value = "select * from ebdb.promo_code where is_valid = true and consumed_count < total_count and  CURRENT_TIMESTAMP() > coupon_start_date and CURRENT_TIMESTAMP() < coupon_end_date ", nativeQuery = true)
	List<PromoCode> findAllIfIsValid();

	@Transactional
	@Modifying
	@Query(value = "update ebdb.promo_code p set p.is_valid = ?1 where p.id=?2", nativeQuery = true)
	int updatePromoCodeSetIsValid(Boolean is_valid, Long id);

	@Transactional
	@Modifying
	@Query(value = "update ebdb.promo_code p set p.consumed_count=?1 where p.name=?2", nativeQuery = true)
	int updateConsumedCount(Integer consumed_count, String name);

	@Transactional
	@Modifying
	@Query(value = "update ebdb.promo_code p set  p.total_count =?1 , p.coupon_start_date=?2, p.coupon_end_date =?3 where p.name=?4", nativeQuery = true)
	int updatePromoCodeSetTotalCountStartDateEndDateAndEndDate(Integer total_count, Date coupon_start_date,
			Date coupon_end_date, String name);
	
     @Query(value="select * from  ebdb.promo_code  where not coupon_end_date < ?1 and not coupon_start_date > ?2 ",nativeQuery = true)
	List<PromoCode> getAllPromoCodeFromDateToDate(Date startDate , Date endDate);
}
