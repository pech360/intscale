package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.user.StudentNotes;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentNotesRepository extends JpaRepository<StudentNotes,Long>
{

    List<StudentNotes> findAllByStudentIdAndTenant(Long id, Tenant tenant);
}
