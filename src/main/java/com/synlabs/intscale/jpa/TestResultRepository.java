package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.test.TestResult;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface TestResultRepository extends JpaRepository<TestResult, Long>
{
  TestResult findByUserAndTestId(User user, Long testId);

  @Query(value = "select count(*) from test_result tr left join user u on u.id=tr.user_id where u.gender='male' and u.grade in(?1) and u.school_name in(?2) and tr.test_id in(?3) and tr.tenant_id=?4 and tr.test_finish_at between ?5 and ?6 group by tr.user_id",nativeQuery = true)
  List<Integer> countRemainingMaleCountBySchoolGradeAndDate(List<String> grades,List<String> schools,List<Long> testId,Long id,String fromDate, String toDate);

  @Query(value = "select count(*) from test_result tr left join user u on u.id=tr.user_id where u.gender='female' and u.grade in(?1) and u.school_name in(?2) and tr.test_id in(?3) and tr.tenant_id=?4 and tr.test_finish_at between ?5 and ?6 group by tr.user_id",nativeQuery = true)
  List<Integer> countRemainingFeMaleCountBySchoolGradeAndDate(List<String> grades,List<String> schools,List<Long> testId,Long id,String fromDate, String toDate);

  List<TestResult> findAllByUserGradeInAndUserGenderAndUserSchoolNameInAndTestIdInAndTenantAndCreatedDateBetween(List<String> grades, String gender, List<String> schools, List<Long> testId, Tenant tenant, Date date1, Date date2);

  List<TestResult> findAllByUserGradeInAndUserGenderAndUserSchoolNameInAndTestIdInAndTenantAndTestFinishAtBetween(List<String> grades, String gender, List<String> schools, List<Long> testId, Tenant tenant, Date date1, Date date2);

  List<TestResult> findAllByUserIdAndTenant(Long id, Tenant tenant);

  List<TestResult> findAllByUserAndTestId(User user, Long testId);

  int countAllByUser(User user);
}
