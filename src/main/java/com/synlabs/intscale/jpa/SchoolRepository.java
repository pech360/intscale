package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.user.School;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Repository
public interface SchoolRepository extends JpaRepository<School, Long>
{

    List<School> findByNameAndTenant(String schoolName,Tenant tenant);

    List<School> findAllByTenant(Tenant tenant);

    School findOneByNameAndTenant(String school, Tenant tenant);

    int countByTenant(Tenant tenant);

    List<School> findAllByIsFieldEnableIsTrueAndTenant(Tenant tenant);

    int countSchoolsByName(String schoolName);

}
