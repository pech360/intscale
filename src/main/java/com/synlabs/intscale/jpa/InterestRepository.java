package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.masterdata.Interest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface InterestRepository extends JpaRepository<Interest, Long>
{
  Interest findOneByName(String name);

  List<Interest> findAllByParentId(Long id);

  List<Interest> findAllByParentName(String name);

  List<Interest> findAllByNameIn(Set<String> names);
}
