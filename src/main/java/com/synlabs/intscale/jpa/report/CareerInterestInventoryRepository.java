package com.synlabs.intscale.jpa.report;

import com.synlabs.intscale.entity.Report.CareerInterestInventory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CareerInterestInventoryRepository extends JpaRepository<CareerInterestInventory, Long> {

    CareerInterestInventory findByCareerName(String careerName);

}
