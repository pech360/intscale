package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.test.UserTest;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

/**
 * Created by India on 10/5/2017.
 */
public interface UserTestRepository extends JpaRepository<UserTest,Long> {
    //UserTest findByTenantAndUserEmail(Tenant tenant,User user);
    List<UserTest> findAllByTestsIdInAndTenant(List<Long> ids,Tenant tenant);
    UserTest findByUserEmailAndTenant(User user,Tenant tenant);

    List<UserTest> findAllByUsedIsTrueAndTestAssignerAndTenant(User user, Tenant tenant);

    List<UserTest> findAllByIdInAndTenant(List<Long> ids, Tenant tenant);

    @Query(value = "select count(*) from user_test ut left join user_test_tests utt on ut.id=utt.user_test_id left join user u on u.id=ut.user_email_id where utt.tests_id in(?1) and u.gender='male' and u.grade in(?2) and u.school_name in(?3) and ut.tenant_id=?4 and (ut.created_date between ?5 and ?6 or ut.last_modified_date between ?5 and ?6) group by ut.user_email_id",nativeQuery = true)
    List<Integer> countAssignedMaleCountBySchoolGradeAndDate(List<Long> testId,List<String> grades,List<String> schools,Long id,String fromDate, String toDate);

    @Query(value = "select count(*) from user_test ut left join user_test_tests utt on ut.id=utt.user_test_id left join user u on u.id=ut.user_email_id where utt.tests_id in(?1) and u.gender='female' and u.grade in(?2) and u.school_name in(?3) and ut.tenant_id=?4 and (ut.created_date between ?5 and ?6 or ut.last_modified_date between ?5 and ?6) group by ut.user_email_id",nativeQuery = true)
    List<Integer> countAssignedFeMaleCountBySchoolGradeAndDate(List<Long> testId,List<String> grades,List<String> schools,Long id,String fromDate, String toDate);

    List<UserTest> findAllByUserEmailGradeInAndUserEmailGenderAndUserEmailSchoolNameInAndTenantAndCreatedDateBetweenOrUserEmailGradeInAndUserEmailGenderAndUserEmailSchoolNameInAndTenantAndLastModifiedDateBetween(List<String> grades, String feMale, List<String> schools, Tenant tenant, Date date1, Date date2, List<String> grades1, String feMale1, List<String> schools1, Tenant tenant1, Date date11, Date date21);

    List<UserTest> findAllByUserEmailGradeInAndUserEmailGenderAndUserEmailSchoolNameInAndTenantAndTestsIdInAndCreatedDateBetweenOrUserEmailGradeInAndUserEmailGenderAndUserEmailSchoolNameInAndTenantAndTestsIdInAndLastModifiedDateBetween(List<String> grades, String feMale, List<String> schools, Tenant tenant, List<Long> testId, Date date1, Date date2, List<String> grades1, String feMale1, List<String> schools1, Tenant tenant1, List<Long> testId1, Date date11, Date date21);

    int countAllByUserEmailGradeInAndUserEmailGenderAndUserEmailSchoolNameInAndTenantAndCreatedDateBetweenOrUserEmailGradeInAndUserEmailGenderAndUserEmailSchoolNameInAndTenantAndLastModifiedDateBetween(List<String> grades, String gender, List<String> schools, Tenant tenant, Date date1, Date date2, List<String> grades1, String feMale1, List<String> schools1, Tenant tenant1, Date date11, Date date21);

    int countAllByUserEmailGradeInAndUserEmailGenderAndUserEmailSchoolNameInAndTestStatusAndTenantAndCreatedDateBetweenOrUserEmailGradeInAndUserEmailGenderAndUserEmailSchoolNameInAndTestStatusAndTenantAndLastModifiedDateBetween(List<String> grades, String male, List<String> schools, String s, Tenant tenant, Date date1, Date date2, List<String> grades1, String male1, List<String> schools1, String s1, Tenant tenant1, Date date11, Date date21);
}
