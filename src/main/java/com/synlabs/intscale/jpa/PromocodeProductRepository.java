package com.synlabs.intscale.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.synlabs.intscale.entity.PromocodeProduct;
import com.synlabs.intscale.util.CompositePromoCodeProductKey;
//LNT
public interface PromocodeProductRepository extends JpaRepository<PromocodeProduct, CompositePromoCodeProductKey> {

	List<PromocodeProduct> findAllProductNameByPromoCodeId(Long promoCodeId);
	
	@Transactional
	@Modifying
	@Query(value="delete from promocode_product where promo_code_id = ?1", nativeQuery = true)
	int deleteByKeyPromoCodeId(Long promoCodeId);
	
}
