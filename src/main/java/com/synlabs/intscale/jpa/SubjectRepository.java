package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.Report.Subject;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SubjectRepository extends JpaRepository<Subject, Long>
{

  Subject findByIdAndTenant(Long id, Tenant tenant);

  List<Subject> findAllByTenant(Tenant tenant);

}
