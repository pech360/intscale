package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.Report.Report;
import com.synlabs.intscale.entity.Report.UserReportStatus;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface UserReportStatusRepository extends JpaRepository<UserReportStatus, Long>
{
  List<UserReportStatus> findAllByTenant(Tenant tenant);

  UserReportStatus findByUserAndReportIdAndTenant(User user, Long reportId, Tenant tenant);

  UserReportStatus findByUserAndReportNameAndTenant(User user, String reportName, Tenant tenant);

  UserReportStatus findByUserAndTenant(User user, Tenant tenant);

  List<UserReportStatus> findAllByStatusAndTenant(String status, Tenant tenant);

  List<UserReportStatus> findAllByTenantAndStatus(Tenant tenant, String generated);

  UserReportStatus findByUserIdAndReportNameAndTenant(Long id, String aim_composite_report, Tenant tenant);

  List<UserReportStatus> findAllByStatus(String status);
  List<UserReportStatus> findAllByStatusAndUserId(String status,Long userId);

  UserReportStatus findByUserAndReportId(User user, Long reportId);

  UserReportStatus findFirstByStatus(String status);

  UserReportStatus findFirstByStatusAndLastModifiedDateBefore(String status, Date beforeDate);

  @Query(value="select u.name as name,u.username as userName,u.email as email ,u.contact_number as phoneNumber from ebdb.user_report_status urs inner join ebdb.user u on urs.user_id = u.id where urs.created_date>=?1 and urs.created_date<=?2 and urs.status =?3",nativeQuery = true)
  List<Object[]> findAllCompletedAssessment(Date from, Date to ,String status);
}

