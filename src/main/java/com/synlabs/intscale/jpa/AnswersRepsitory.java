package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.test.Answers;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnswersRepsitory extends JpaRepository<Answers, Long>
{

}
