package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.counselling.CounsellingSessionNotes;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.enums.CounsellingSessionType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CounsellingSessionNotesRepository extends JpaRepository<CounsellingSessionNotes, Long>
{

  CounsellingSessionNotes findByIdAndTenant(Long id, Tenant tenant);

  List<CounsellingSessionNotes> findAllByCounsellingSessionTypeAndTenant(CounsellingSessionType type, Tenant tenant);

  List<CounsellingSessionNotes> findAllBySampleAndCounsellingSessionTypeAndTenant(boolean b, CounsellingSessionType counsellingSessionType, Tenant tenant);

  CounsellingSessionNotes findByIdAndSampleAndTenant(Long id, boolean b, Tenant tenant);

  List<CounsellingSessionNotes> findAllByCounsellingSessionTypeAndSampleAndTenant(CounsellingSessionType type, boolean b, Tenant tenant);
}
