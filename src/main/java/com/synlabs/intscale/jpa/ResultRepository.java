package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.test.TestResult;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

@Repository
public interface ResultRepository extends JpaRepository<TestResult, Long>
{

  TestResult findByUserIdAndTestIdAndTenant(Long userId, Long testId, Tenant tenant);

  List<TestResult> findAllByUserIdAndTenant(Long id, Tenant tenant);

  @Query(value = "SELECT DISTINCT test_id FROM test_result where tenant_id=?1", nativeQuery = true)
  List<BigInteger> findAllTestByTenantId(Long tenantId);

  List<TestResult> findAllByUserIdAndPilotNameAndTenant(Long id, String pilotName, Tenant tenant);

  List<TestResult> findAllByTestAndTenant(Test test, Tenant tenant);

  List<TestResult> findAllByUserIdAndPilotIdAndTenant(Long id, Long id1, Tenant tenant);

  List<TestResult> findAllByTestIdAndUserIdAndTenant(Long testId, Long UserId, Tenant tenant);

  List<TestResult> findAllByUserIdAndSubConstructNameAndTenant(Long id, String name, Tenant tenant);

  List<TestResult> findAllByUserIdAndTenantAndSubConstructNameIsNotNull(Long id, Tenant tenant);

  Long countDistinctByUserIdAndTestIn(Long userId, List<Test> tests);

  @Query(value = "SELECT id FROM test_result WHERE tenant_id != 1;", nativeQuery = true)
  List<BigInteger> findAllTestResultIdByLeapTenants();

  Long countByUserAndTestAndTenant(User user, Test test, Tenant tenant);
  
  @Query(value="select u.name as name,u.username as userName,u.email as email ,u.contact_number as phoneNumber from ebdb.user u inner join ebdb.test_result tr on tr.user_id = u.id inner join ebdb.test t on tr.test_id = t.id where tr.created_date>=?1 and tr.created_date<=?2 and t.paid= false" ,nativeQuery = true)
	List<Object[]> findAllSignedUpAndGivenDemoTest(Date from, Date to);

}