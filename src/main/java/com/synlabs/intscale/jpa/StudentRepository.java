package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.user.Student;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by India on 1/4/2018.
 */
@Repository
public interface StudentRepository extends JpaRepository<Student, Long>
{
  List<Student> findAllByTenant(Tenant tenant);

  List<Student> findAllByTenantAndSchoolIdAndGrade(Tenant tenant, Long schoolId,String grade);

  List<Student> findAllByTenantAndSchoolId(Tenant tenant, Long schoolId);

  List<Student> findAllByTenantAndGrade(Tenant tenant, String grade);

  Student findOneByUsernameAndTenant(String username, Tenant tenant);

  int countByUsernameAndTenant(String username, Tenant tenant);

  Student findOneByUserAndTenant(User user, Tenant tenant);

  List<Student> findAllByTenantAndSchoolIdAndGradeAndCounsellorId(Tenant tenant, Long schoolId, String grade, Long counsellorId);

  List<Student> findAllByTenantAndSchoolIdAndCounsellorId(Tenant tenant, Long schoolId, Long counsellorId);

  List<Student> findAllByTenantAndGradeAndCounsellorId(Tenant tenant, String grade, Long counsellorId);

  List<Student> findAllByTenantAndCounsellorId(Tenant tenant, Long counsellorId);

  List<Student> findAllByGradeAndSectionAndTenant(String grade, String section, Tenant tenant);

  Student findByIdAndTenant(Long studentId, Tenant tenant);
}
