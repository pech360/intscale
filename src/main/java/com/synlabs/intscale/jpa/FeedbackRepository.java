package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.test.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by itrs on 9/7/2017.
 */
@Repository
public interface FeedbackRepository extends JpaRepository<Feedback,Long>
{
}
