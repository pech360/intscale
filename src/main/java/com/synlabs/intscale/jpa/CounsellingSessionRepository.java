package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.counselling.CounsellingSession;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.enums.CounsellingSessionType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CounsellingSessionRepository extends JpaRepository<CounsellingSession, Long>
{

  CounsellingSession findByIdAndTenant(Long id, Tenant tenant);

  List<CounsellingSession> findByUserAndCounsellorAndTenant(User user, User counsellor, Tenant tenant);

  Long countCounsellingSessionsByUserAndCounsellorAndTenant(User user, User counsellor, Tenant tenant);

  List<CounsellingSession> findByUserAndCounsellorAndCounsellingSessionTypeAndTenant(User user, User counsellor, CounsellingSessionType type, Tenant tenant);

  List<CounsellingSession> findByUserAndCounsellingSessionTypeAndTenant(User user, CounsellingSessionType type, Tenant tenant);
}
