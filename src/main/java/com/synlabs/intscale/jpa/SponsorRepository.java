package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.masterdata.Sponsor;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SponsorRepository extends JpaRepository<Sponsor, Long> {
    List<Sponsor> findAllByTenant(Tenant tenant);

    List<Sponsor> findAllByIdInAndTenant(List<Long> sponsorIds, Tenant tenant);

    Sponsor findOneByIdAndTenant(Long id, Tenant tenant);
}
