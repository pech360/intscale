package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.Report.ScoreRange;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScoreRangeRepository extends JpaRepository<ScoreRange, Long>
{
}
