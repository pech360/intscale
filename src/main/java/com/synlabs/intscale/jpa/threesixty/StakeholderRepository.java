package com.synlabs.intscale.jpa.threesixty;

import com.synlabs.intscale.entity.threesixty.Stakeholder;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.enums.StakeholderRelation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StakeholderRepository extends JpaRepository<Stakeholder, Long>
{
  Stakeholder findByTenantAndLoginKey(Tenant tenant, String loginKey);

  Stakeholder findByTenantAndUserAndRelation(Tenant tenant, User user, StakeholderRelation relation);

  Stakeholder findByLoginKey(String loginKey);
}
