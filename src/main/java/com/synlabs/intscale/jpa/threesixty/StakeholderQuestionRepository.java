package com.synlabs.intscale.jpa.threesixty;

import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.threesixty.StakeholderQuestion;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;

public interface StakeholderQuestionRepository extends JpaRepository<StakeholderQuestion, Long>
{
  List<StakeholderQuestion> findByTenantAndTestIn(Tenant tenant, Collection<Test> tests);
}
