package com.synlabs.intscale.jpa.threesixty;

import com.synlabs.intscale.entity.threesixty.NumberSequence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NumberSequenceRepository extends JpaRepository<NumberSequence, Long>
{
  NumberSequence findByType(String type);
}
