package com.synlabs.intscale.jpa.threesixty;

import com.synlabs.intscale.entity.threesixty.NotificationEmail;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.enums.NotificationType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NotificationEmailRepository extends JpaRepository<NotificationEmail, Long>
{
  List<NotificationEmail> findByTypeAndTenant(NotificationType type, Tenant tenant);

  List<NotificationEmail> findByTypeAndTenantIsNull(NotificationType type);
}
