package com.synlabs.intscale.jpa.threesixty;

import com.synlabs.intscale.entity.threesixty.StakeholderQuestionOption;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StakeholderQuestionOptionRepository extends JpaRepository<StakeholderQuestionOption, Long>
{
}
