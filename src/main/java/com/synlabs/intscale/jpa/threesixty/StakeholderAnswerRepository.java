package com.synlabs.intscale.jpa.threesixty;

import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.threesixty.Stakeholder;
import com.synlabs.intscale.entity.threesixty.StakeholderAnswer;
import com.synlabs.intscale.entity.threesixty.StakeholderQuestion;
import com.synlabs.intscale.entity.user.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StakeholderAnswerRepository extends JpaRepository<StakeholderAnswer, Long>
{
  StakeholderAnswer findByTenantAndStakeholderAndQuestion(Tenant tenant, Stakeholder stakeholder, StakeholderQuestion question);

    List<StakeholderAnswer> findAllByStakeholderAndTenant(Stakeholder stakeholder, Tenant tenant);
}
