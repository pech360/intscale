package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.TestPerTenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TestPerTenantRepository extends JpaRepository<TestPerTenant,Long>{

    List<TestPerTenant> findAllByDomainId(Long id);

    TestPerTenant findOneByTestIdAndDomainId(Long id, Long tenantId);

    List<TestPerTenant> findAllByTestId(Long id);
}
