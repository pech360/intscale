package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.user.AcademicPerformance;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AcademicPerformanceRepository extends JpaRepository<AcademicPerformance, Long>
{

  AcademicPerformance findByIdAndTenant(Long id, Tenant tenant);

  List<AcademicPerformance> findAllByUserAndTenant(User user, Tenant tenant);
}
