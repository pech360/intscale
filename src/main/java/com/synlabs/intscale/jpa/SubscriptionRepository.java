package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.test.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by itrs on 6/21/2017.
 */
@Repository
public interface SubscriptionRepository extends JpaRepository<Subscription, Long>
{
}
