package com.synlabs.intscale.jpa;

import com.synlabs.intscale.entity.Report.AimReportContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AimReportContentRepository extends JpaRepository<AimReportContent, Long>
{

}
