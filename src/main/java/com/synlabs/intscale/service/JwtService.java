package com.synlabs.intscale.service;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.synlabs.intscale.common.AuthenticationWrapper;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.enums.UserType;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.util.DateUtil;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Service
public class JwtService extends BaseService
{
  private Cache<String, AuthenticationWrapper> authCache;

  @Value("${intscale.auth.secretkey}")
  private String secretkey;

  @PostConstruct
  public void init()
  {
    authCache = CacheBuilder
        .newBuilder()
        .build();
  }

  public Authentication getAuthentication(String token)
  {
    JwtParser jwtParser = Jwts.parser()
                              .setSigningKey(secretkey);
    String username = jwtParser.parseClaimsJws(token).getBody().getSubject();
    AuthenticationWrapper authenticationWrapper = authCache.getIfPresent(username);
//    if (authenticationWrapper != null && !authenticationWrapper.getToken().equals(token))
//    {
//      throw new ValidationException("You are logged in another session, so close this session");
//    }
    return authenticationWrapper == null ? null : authenticationWrapper.getAuthentication();
  }

  public void putAuthentication(String token, Authentication authentication)
  {
    JwtParser jwtParser = Jwts.parser()
                              .setSigningKey(secretkey);
    String username = jwtParser.parseClaimsJws(token).getBody().getSubject();
    authCache.put(username, new AuthenticationWrapper(username, authentication, token));
  }

  public boolean isAllowedToLogin(User user)
  {
    String username = user.getUsername();

    AuthenticationWrapper authenticationWrapper = authCache.getIfPresent(username);
    if (authenticationWrapper == null)
    {
      return false;
    }
    String token = authenticationWrapper.getToken();
    try
    {
    	Date expiration = Jwts.parser()
                .setSigningKey(secretkey).parseClaimsJws(token)
                .getBody().getExpiration();
    	System.out.println(DateUtil.formatDate(expiration));
      return expiration.after(new Date());
    }
    catch (ExpiredJwtException ex)
    {
      authCache.invalidate(username);
      return false;
    }
  }

  public Date getExpirationTimeIfLogin(User user)
  {
    String username = user.getUsername();

    AuthenticationWrapper authenticationWrapper = authCache.getIfPresent(username);
    if (authenticationWrapper == null)
    {
      return null;
    }
    String token = authenticationWrapper.getToken();
    try
    {
    	Date expiration = Jwts.parser()
                .setSigningKey(secretkey).parseClaimsJws(token)
                .getBody().getExpiration();
    	System.out.println(DateUtil.formatDate(expiration));
      return expiration;
    }
    catch (ExpiredJwtException ex)
    {
      authCache.invalidate(username);
      return null;
    }
  }
  
  public void removeToken(User user)
  {
    authCache.invalidate(user.getUsername());
  }

  public String getTokenFromAuthCache(User user)
  {

    String username = user.getUsername();
    AuthenticationWrapper authenticationWrapper = authCache.getIfPresent(username);

    return authenticationWrapper.getToken();

  }

  public boolean isPresentInCache(String username)
  {
    return authCache.getIfPresent(username) != null;
  }

  public Set<String> getLoggedInUsers(){
    return authCache.asMap().keySet();
  }
}
