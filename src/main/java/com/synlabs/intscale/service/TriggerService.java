package com.synlabs.intscale.service;

import com.synlabs.intscale.config.BaseConfig;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.view.TriggerRequest;
import com.synlabs.intscale.view.TriggerResponse;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@Service
public class TriggerService extends BaseConfig {
    @Autowired
    @Qualifier("schedulerFactoryBean")
    private Scheduler scheduler;

    public List<TriggerResponse> list() throws SchedulerException {
        List<TriggerResponse> list = new ArrayList<>();
        Set<TriggerKey> keys = scheduler.getTriggerKeys(null);
        for (TriggerKey key : keys) {
            Trigger trigger = scheduler.getTrigger(key);
            TriggerResponse response = new TriggerResponse();
            response.setName(key.getName());
            response.setGroup(key.getGroup());
            response.setJobName(trigger.getJobKey().getName());
            response.setState(scheduler.getTriggerState(key));

            if (trigger instanceof SimpleTrigger) {
                response.setType("simple");
                SimpleTrigger simpleTrigger = (SimpleTrigger) trigger;
                response.setInterval(simpleTrigger.getRepeatInterval());
            } else if (trigger instanceof CronTrigger) {
                response.setType("cron");
                CronTrigger cronTrigger = (CronTrigger) trigger;
                response.setCronExpression(cronTrigger.getCronExpression());
            }
            list.add(response);
        }
        return list;
    }

    public boolean updateAndReschedule(TriggerRequest request) throws SchedulerException, ParseException {
        TriggerKey key = new TriggerKey(request.getName(), request.getGroup());
        try {
            Trigger dbTrigger = scheduler.getTrigger(key);
            scheduler.pauseTrigger(key);
            JobDetail jobDetail = scheduler.getJobDetail(dbTrigger.getJobKey());
            if (dbTrigger instanceof SimpleTrigger) {
                if (request.getInterval() != null) {
                    if (request.getInterval() == ((SimpleTrigger) dbTrigger).getRepeatInterval()) {
                        return false;
                    }
                    SimpleTriggerFactoryBean trigger = createTrigger(jobDetail, request.getInterval());
                    trigger.setName(key.getName());
                    trigger.setGroup(key.getGroup());
                    trigger.afterPropertiesSet();
                    scheduler.rescheduleJob(key, trigger.getObject());
                }
            } else if (dbTrigger instanceof CronTrigger) {
                CronTrigger dbCronTrigger = (CronTrigger) dbTrigger;

                if (StringUtils.isEmpty(request.getCronExpression()) ||
                        request.getCronExpression().equals(dbCronTrigger.getCronExpression())) {
                    return false;
                }
                CronTriggerFactoryBean trigger = createCronTrigger(jobDetail, request.getCronExpression());
                trigger.setName(key.getName());
                trigger.setGroup(key.getGroup());
                trigger.afterPropertiesSet();
                scheduler.rescheduleJob(key, trigger.getObject());
            }
        } finally {
            scheduler.resumeTrigger(key);
        }
        return true;
    }

    public boolean pauseAllTriggers() throws SchedulerException {
        scheduler.pauseAll();
        return true;
    }

    public boolean resumeAllTriggers() throws SchedulerException {
        scheduler.resumeAll();
        return true;
    }

    public boolean pauseTrigger(TriggerRequest request) throws SchedulerException {
        Trigger trigger = getTrigger(request);
        scheduler.pauseTrigger(trigger.getKey());
        return true;
    }

    public boolean resumeTrigger(TriggerRequest request) throws SchedulerException {
        Trigger trigger = getTrigger(request);
        scheduler.resumeTrigger(trigger.getKey());
        return true;
    }

    private Trigger getTrigger(TriggerRequest request) throws SchedulerException {
        if (StringUtils.isEmpty(request.getName())) {
            throw new ValidationException("Trigger name is required.");
        }
        if (StringUtils.isEmpty(request.getGroup())) {
            throw new ValidationException("Group name is required.");
        }
        TriggerKey key = request.getKey();
        Trigger trigger = scheduler.getTrigger(key);
        if (trigger == null) {
            throw new ValidationException(String.format("Not trigger found[name=%s,group=%s]", request.getName(), request.getGroup()));
        }
        return trigger;
    }

}
