package com.synlabs.intscale.service;

import com.querydsl.jpa.impl.JPAQuery;
import com.synlabs.intscale.ImportModel.ImportStudentModel;
import com.synlabs.intscale.entity.Report.UserReportStatus;
import com.synlabs.intscale.entity.Timeline;
import com.synlabs.intscale.entity.counselling.QCounsellingSession;
import com.synlabs.intscale.entity.user.*;
import com.synlabs.intscale.enums.CounsellingSessionStatus;
import com.synlabs.intscale.enums.RegisterType;
import com.synlabs.intscale.enums.UserType;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.*;
import com.synlabs.intscale.util.StringUtil;
import com.synlabs.intscale.view.*;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by India on 1/4/2018.
 */
@Service
public class StudentService extends BaseService {
    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private MasterDataDocumentRepository documentRepository;

    @Autowired
    private SchoolRepository schoolRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private ExcelImportService excelImportService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StudentNotesRepository notesRepository;

    @Autowired
    private TimelineRepository timelineRepository;

    @Autowired
    private UserReportStatusRepository userReportStatusRepository;

    @PersistenceContext
    private EntityManager entityManager;

    private PasswordEncoder encoder = new BCryptPasswordEncoder();

    public List<Student> findAll() {
        return studentRepository.findAllByTenant(getTenant());
    }

    public List<UserResponseForCounsellor> findAllByCounsellor() {
        List<User> users = userRepository.findAllByTenantAndCounsellorId(getTenant(), getUser().getId());
        List<UserResponseForCounsellor> userResponses = users.stream().map(UserResponseForCounsellor::new).collect(Collectors.toList());
        userResponses.forEach(user -> {
            UserReportStatus userReportStatus = userReportStatusRepository.findByUserIdAndReportNameAndTenant(user.getId(), "Aim Composite Report", getTenant());
            user.setAimAssessmetsCompletedOn(DateUtils.addMinutes(userReportStatus.getCreatedDate().toDate(), 330));
        });

        return userResponses;
    }

    public Student save(StudentRequest request) {
        Student student = request.toEntity();
        student.setSection(request.getSection());
        student.setSchool(schoolRepository.findOne(request.getSchool()));
        student.setGrade(request.getGrade());
        User user = userService.createStudentUser(request);
        if (StringUtils.isEmpty(user))
            throw new ValidationException("User not created successfully");
        student.setUser(user);
        return studentRepository.save(student);
    }

    public Student update(StudentRequest request) {
        Student student = request.toEntity(studentRepository.findOne(request.getId()));
        student.setSection(request.getSection());
        student.setGrade(request.getGrade());
        student.setSchool(schoolRepository.findOne(request.getSchool()));
        if (StringUtils.isEmpty(student.getUser()))
            throw new ValidationException("User not found by student");
        User user = userService.updateStudentUser(request, student.getUser());
        if (StringUtils.isEmpty(user))
            throw new ValidationException("User not updated successfully");
        return studentRepository.save(student);
    }

    public void delete(Long id) {
        Student student = studentRepository.findOne(id);
        User user = student.getUser();
        user.setActive(false);
        user.setTenant(null);
        user.setRoles(null);
        userService.save(user);
        studentRepository.delete(student);
    }

    public List<Student> findAllByFilter(Long schoolId, String grade, Long counsellorId) {
        if (schoolId != null && grade != null && counsellorId != null)
            return studentRepository.findAllByTenantAndSchoolIdAndGradeAndCounsellorId(getTenant(), schoolId, grade, counsellorId);
        if (schoolId != null && counsellorId != null)
            return studentRepository.findAllByTenantAndSchoolIdAndCounsellorId(getTenant(), schoolId, counsellorId);
        if (grade != null && counsellorId != null)
            return studentRepository.findAllByTenantAndGradeAndCounsellorId(getTenant(), grade, counsellorId);
        if (schoolId != null && grade != null)
            return studentRepository.findAllByTenantAndSchoolIdAndGrade(getTenant(), schoolId, grade);
        if (schoolId != null)
            return studentRepository.findAllByTenantAndSchoolId(getTenant(), schoolId);
        if (grade != null)
            return studentRepository.findAllByTenantAndGrade(getTenant(), grade);
        if (counsellorId != null)
            return studentRepository.findAllByTenantAndCounsellorId(getTenant(), counsellorId);
        return studentRepository.findAllByTenant(getTenant());
    }

    public HashSet<User> findAllFilteredDataByCounsellor(String schoolId, String gradeId, String city) {


        JPAQuery<User> query = new JPAQuery<>(entityManager);
        QUser user = QUser.user;
        query.select(user)
                .distinct()
                .from(user)
                .where(user.counsellor.eq(getUser()));

        if (!StringUtils.isEmpty(gradeId)) {
            query.where(user.grade.eq(gradeId));
        }

        if (!StringUtils.isEmpty(schoolId)) {
            query.where(user.schoolName.eq(schoolId));
        }

        if (!StringUtils.isEmpty(city)) {
            query.where(user.city.eq(city));
        }

        return new HashSet<>(query.fetch());


    }

    public boolean assignGradeToStudents(StudentGradeRequest request) {
        if (request.getStudentIds() != null && request.getGrade() != null && request.getSection() != null) {
            List<Long> ids = Arrays.asList(request.getStudentIds());
            Student student = null;
            for (Long id : ids) {
                student = studentRepository.findOne(id);
                student.setGrade(request.getGrade());
                student.setSection(request.getSection());
                User user = student.getUser();
                user.setGrade(request.getGrade());
                user.setSection(request.getSection());
                userService.save(user);
                studentRepository.save(student);
            }
            return true;
        }
        return false;
    }

    public boolean assignCounsellorToStudents(StudentCounsellorRequest request) {
        if (request.getStudentIds() != null && request.getCounsellor() != null) {
            List<Long> ids = Arrays.asList(request.getStudentIds());
            User user = null;
            User counsellor = userRepository.findOne(request.getCounsellor());
            for (Long id : ids) {
                user = userRepository.findOne(id);
                user.setCounsellor(counsellor);
                user.setCounsellorAssignedOn(new Date());
                user = userRepository.saveAndFlush(user);
                counsellor.getUsers().add(user);
            }
            userRepository.saveAndFlush(counsellor);
            return true;
        }
        return false;
    }

    public boolean removeGradeToStudents(StudentGradeRequest request) {
        if (request.getStudentIds() != null) {
            List<Long> ids = Arrays.asList(request.getStudentIds());
            Student student = null;
            for (Long id : ids) {
                student = studentRepository.findOne(id);
                student.setGrade(null);
                studentRepository.save(student);
            }
            return true;
        }
        return false;
    }

    public boolean removeCounsellorsToStudents(StudentCounsellorRequest request) {
        if (request.getStudentIds() != null) {
            List<Long> ids = Arrays.asList(request.getStudentIds());
            User student = null;
            for (Long id : ids) {
                student = userRepository.findOne(id);
                student.setCounsellor(null);
                userRepository.save(student);
            }
            return true;
        }
        return false;
    }

    public boolean updateGradeToStudents(Long id, String grade, String section) {
        if (id != null && grade != null && section != null) {
            Student student = studentRepository.findOne(id);
            student.setGrade(grade);
            student.setSection(section);
            User user = student.getUser();
            user.setGrade(grade);
            user.setSection(section);
            userService.save(user);
            studentRepository.save(student);
            return true;
        }
        return false;
    }

    public Map<String, String> importStudentRecords(MultipartFile file, String dryRun) throws Exception {
        boolean errorInFile = false;
        String validationMessage;
        Map<String, String> statusMap = new HashMap<>();
        statusMap.put("error", "false");
        Map<String, ArrayList> studentRecords = excelImportService.importStudentRecords(file);
        ArrayList<ImportStudentModel> importStudentList = studentRecords.get("students");
        List<Student> studentList = new ArrayList<>(importStudentList.size());
        Role defaultRole = roleRepository.getOneByName("TestTaker");
        List<Role> defaultRoleList = new ArrayList<>();
        if (defaultRole != null) {
            defaultRoleList.add(defaultRole);
        }
        for (ImportStudentModel importStudent : importStudentList) {
            validationMessage = importStudent.validate();
            if (userRepository.countByUsernameAndTenant(importStudent.getUsername(), getTenant()) > 0) {
                statusMap.put(importStudent.getName(), "username is already registered");
                errorInFile = true;
            } else {
                if (!StringUtils.isEmpty(importStudent.getEmail())) {
                    if (userRepository.countByEmailAndTenant(importStudent.getEmail(), getTenant()) > 0) {
                        statusMap.put(importStudent.getName(), "email is already registered");
                        errorInFile = true;
                    } else {
                        statusMap.put(importStudent.getName(), validationMessage);
                    }
                }
            }
            if (!validationMessage.equalsIgnoreCase("ok")) {
                errorInFile = true;
            }
        }
        if (!errorInFile && dryRun.equalsIgnoreCase("false")) {
            for (ImportStudentModel importStudent : importStudentList) {
                User user = userRepository.save(convertImportStudentModelToUser(importStudent, defaultRoleList));
                if (user != null) {
                    Student student = convertImportStudentModelToStudent(importStudent, user);
                    studentList.add(student);
                }
            }
            studentRepository.save(studentList);
        }
        if (errorInFile) {
            statusMap.put("error", "true");
        }
        return statusMap;
    }

    private Student convertImportStudentModelToStudent(ImportStudentModel importStudent, User user) {
        Student student = new Student();
        student.setName(importStudent.getName());
        student.setGender(importStudent.getGender());
        student.setUsername(importStudent.getUsername());

        if (!StringUtils.isEmpty(importStudent.getEmail())) {
            student.setEmail(importStudent.getEmail());
        }

        student.setSchool(schoolRepository.findOneByNameAndTenant(importStudent.getSchool(), getTenant()));
        student.setSection(importStudent.getSection());
        student.setGrade(importStudent.getGrade());
        student.setDob(importStudent.getDob());
        student.setUser(user);

        return student;
    }

    private User convertImportStudentModelToUser(ImportStudentModel importUser, List<Role> defaultRoleList) {
        User user = new User();
        user.setName(StringUtil.capitalizeWord(importUser.getName()));
        user.setPasswordHash(encoder.encode(importUser.getUsername()));
        user.setActive(true);
        user.setThreeSixtyEnabled(true);
        user.setUserType(UserType.Student.toString());
        user.setGender(importUser.getGender());
        user.setUsername(importUser.getUsername());
        user.setSection(importUser.getSection());
        user.setSchoolName(importUser.getSchool());
        user.setGrade(importUser.getGrade());
        user.setDob(importUser.getDob());
        user.setRegisterType(RegisterType.MANUALLY.getType());

        if (!StringUtils.isEmpty(importUser.getEmail())) {
            user.setEmail(importUser.getEmail());
        }

        if (!defaultRoleList.isEmpty()) {
            user.setRoles(defaultRoleList);
        }

        return user;
    }

    public List<StudentNotes> findAllByStudentId(Long id) {
        return notesRepository.findAllByStudentIdAndTenant(id, getTenant());
    }

    public StudentNotes saveNotes(StudentNotesRequest request) {
        StudentNotes notes = request.toEntity();
        notes.setStudent(userRepository.findOne(request.getStudentId()));
        notes.setCounsellor(getUser());
        return notesRepository.save(notes);
    }

    public StudentNotes pushNotes(StudentNotesRequest request) {
        Timeline timeline = new Timeline();
        timeline.setHeading("Notes By Your Counsellor");
        timeline.setDescription(request.getNotes());
        timeline.setContentType("counsellornotes");
        timeline.setUser(userRepository.findOne(request.getStudentId()));
        timelineRepository.save(timeline);
        StudentNotes notes = notesRepository.findOne(request.getId());
        notes.setPushStatus(true);
        notes.setPushCount(notes.getPushCount() + 1);
        return notesRepository.save(notes);
    }

    public StudentNotes saveAndPushNotes(StudentNotesRequest request) {
        Timeline timeline = new Timeline();
        timeline.setHeading("Notes By Your Counsellor");
        timeline.setDescription(request.getNotes());
        timeline.setContentType("counsellornotes");
        timeline.setUser(userRepository.findOne(request.getStudentId()));
        timelineRepository.save(timeline);
        StudentNotes notes = request.toEntity();
        notes.setStudent(userRepository.findOne(request.getStudentId()));
        notes.setCounsellor(getUser());
        notes.setPushStatus(true);
        notes.setPushCount(notes.getPushCount() + 1);
        return notesRepository.save(notes);
    }

    public StudentNotes updateNotes(StudentNotesRequest request) {
        StudentNotes notes = request.toEntity(notesRepository.findOne(request.getId()));
        return notesRepository.save(notes);
    }

    public StudentNotes pushUpdateNotes(StudentNotesRequest request) {
        Timeline timeline = new Timeline();
        timeline.setHeading("Notes By Your Counsellor");
        timeline.setDescription(request.getNotes());
        timeline.setContentType("counsellornotes");
        timeline.setUser(userRepository.findOne(request.getStudentId()));
        timelineRepository.save(timeline);
        StudentNotes notes = request.toEntity(notesRepository.findOne(request.getId()));
        notes.setPushStatus(true);
        notes.setPushCount(notes.getPushCount() + 1);
        return notesRepository.save(notes);
    }
}
