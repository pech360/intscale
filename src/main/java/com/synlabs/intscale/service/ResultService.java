package com.synlabs.intscale.service;

import com.querydsl.jpa.impl.JPAQuery;
import com.synlabs.intscale.ImportModel.ImportStudentModel;
import com.synlabs.intscale.ImportModel.ImportTestResponseModel;
import com.synlabs.intscale.entity.Pilot;
import com.synlabs.intscale.entity.Report.Report;
import com.synlabs.intscale.entity.Report.UserReportStatus;
import com.synlabs.intscale.entity.test.*;
import com.synlabs.intscale.entity.user.Role;
import com.synlabs.intscale.entity.user.Student;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.enums.*;
import com.synlabs.intscale.ex.NotFoundException;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.*;
import com.synlabs.intscale.service.communication.CommunicationService;
import com.synlabs.intscale.util.DateUtil;
import com.synlabs.intscale.view.DashboardRequest;
import com.synlabs.intscale.view.leap.TestResultSummaryResponse;
import com.synlabs.intscale.view.usertest.GradeResultResponse;
import com.synlabs.intscale.view.usertest.ResultRequest;
import com.synlabs.intscale.view.usertest.ResultResponse;
import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

import static com.synlabs.intscale.util.DateUtil.DayTime.END_OF_DAY;
import static com.synlabs.intscale.util.DateUtil.DayTime.START_OF_DAY;
import static java.util.stream.Collectors.groupingBy;

@Service
public class ResultService extends BaseService {
    @Autowired
    private ResultRepository resultRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TestRepository testRepository;
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private UserAnswerRepository userAnswerRepository;
    @Autowired
    private UserTestRepository userTestRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PilotService pilotService;
    @Autowired
    private ValidityIndicesCalculator validityIndicesCalculator;
    @Autowired
    private TextquestionAnswersRepository textquestionAnswersRepository;
    @Autowired
    private QuestionService questionService;
    @Autowired
    private TestService testService;
    @Autowired
    private UserService userService;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private AnswersRepsitory answersRepository;
    @Autowired
    private TimelineService timelineService;
    @Autowired
    private ReportService reportService;
    @Autowired
    private CommunicationService communicationService;
    @Autowired
    private TestResultSummaryRepository testResultSummaryRepository;
    @Autowired
    private ExcelImportService excelImportService;
    @Autowired
    private AsyncReportService asyncReportService;
    @PersistenceContext
    private EntityManager entityManager;

    @Value("${scale.image.upload.location}")
    protected String filedir;

    private static Logger logger = LoggerFactory.getLogger(ReportService.class);

    public TestResult get(Long id) {
        return resultRepository.findOne(id);
    }

    public ResultResponse getByCurrentUserAndTestId(Long testId, Long userId) {
        User user = null;
        if (StringUtils.isEmpty(userId) || userId == 0) {
            user = getFreshUser();
        } else {
            user = userService.getOneById(userId);
        }

        if (StringUtils.isEmpty(testId)) {
            throw new ValidationException(String.format("TestId not found"));
        }

        Test test = testRepository.findOne(testId);
        if (test == null) {
            throw new ValidationException(String.format("Test[%s] not found", testId));
        }

        TestResult testResult = resultRepository.findByUserIdAndTestIdAndTenant(user.getId(), test.getId(), getTenant());
        if (testResult == null) {
            throw new ValidationException(String.format("Result not found"));
        }

        ResultResponse resultResponse = new ResultResponse(testResult);

        resultResponse.getUserAnswerResponses().forEach(userAnswerResponse -> {
            userAnswerResponse.setCorrectAnswerDescription(questionService.getCorrectAnswer(userAnswerResponse.getQuestionId()).getDescription());
        });

        Integer infrequencyCount = 0;
        Integer acquiescenceCount = 0;
        infrequencyCount = userAnswerRepository.countUserAnswersByTestResultAndTenantAndOptionNumber(testResult, getTenant(), 3);
        acquiescenceCount = userAnswerRepository.countUserAnswersByTestResultAndTenantAndOptionNumberIsGreaterThan(testResult, getTenant(), 3);
        Map<String, BigDecimal> validityIndices = validityIndicesCalculator.getValidityIndicesPercentile(test.getName(), infrequencyCount, acquiescenceCount);

        if (validityIndices != null) {
            resultResponse.setInfrequency(validityIndices.get("infrequency"));
            resultResponse.setAcquiescence(validityIndices.get("acquiescence"));
            resultResponse.setInfrequencyRawScore(infrequencyCount);
            resultResponse.setAcquiescenceRawScore(acquiescenceCount);
        }

        return resultResponse;
    }

    public TestResult get(String username, Long testId) {
        if (StringUtils.isEmpty(username)) {
            throw new ValidationException(String.format("Username not found"));
        }
        if (StringUtils.isEmpty(testId)) {
            throw new ValidationException(String.format("TestId not found"));
        }

        User user = userRepository.findByUsernameAndTenant(username, getTenant());
        if (user == null) {
            throw new ValidationException(String.format("User[%s] not found", username));
        }
        Test test = testRepository.findOne(testId);
        if (test == null) {
            throw new ValidationException(String.format("Test[%s] not found", testId));
        }
        TestResult testResult = resultRepository.findByUserIdAndTestIdAndTenant(user.getId(), test.getId(), getTenant());
        if (testResult == null) {
            throw new ValidationException(String.format("Result not found"));
        }

        return testResult;
    }

    public TestResult saveResult(ResultRequest request) {
        Pilot pilot = pilotService.findOneByActive(true);
        if (StringUtils.isEmpty(pilot)) {
            throw new ValidationException("Current Pilot Exception");
        }
        User user = getFreshUser();
        Test test = testRepository.findOne(request.getTestId());

        Long countOfSameTestTakenByUser = resultRepository.countByUserAndTestAndTenant(user, test, getTenant());
        if (countOfSameTestTakenByUser > 0) {
            throw new ValidationException("You have already taken this test, no need to save it again.");
        }

        TestResult testResult = request.toEntity(new TestResult());

        if (!test.isGoLive()) {
            //test.setPublished(false);
            //testRepository.save(test);
            testResult.setSubConstructName(test.getParent() != null ? test.getParent().getName() : "");
        }
        testResult.setTest(test);

        UserTest userTest = userTestRepository.findByUserEmailAndTenant(user, getTenant());
        if (userTest != null) {
            List<Test> assignedTest = userTest.getTests();
            List<Test> remTest = new ArrayList<>();
            for (Test t : assignedTest) {
                if (!t.getName().equals(test.getName())) {
                    remTest.add(t);
                }
            }
            userTest.setTests(remTest);
            userTest.setTestStatus(TestStatus.TIA.getStatus());
            if (remTest.isEmpty()) {
                userTest.setTestStatus(TestStatus.TCS.getStatus());
            }
            userTestRepository.save(userTest);
        }

        testResult.setPilot(pilot);
        testResult.setUser(user);
        testResult = resultRepository.saveAndFlush(testResult);

        for (int i = 0; i < testResult.getUserAnswers().size(); i++) {
            Answers answers = answersRepository.findOne(testResult.getUserAnswers().get(i).getAnswerId());
            testResult.getUserAnswers().get(i).setQuestion(questionRepository.findOne(request.getAnswers().get(i).getQuestion()));
            testResult.getUserAnswers().get(i).setTestResult(testResult);
            testResult.getUserAnswers().get(i).setMarks(answers.getMarks());
            testResult.getUserAnswers().get(i).setOptionNumber(answers.getOptionNumber());
        }

        List<UserAnswer> userAnswers = new ArrayList<>();
        List<Integer> markList = new ArrayList<>();
        Long sumOfRawScore = 0L;
        Long sumOfMaxScore = 0L;
        double percentageScore = 0.0;

        for (UserAnswer un : testResult.getUserAnswers()) {
            un.getQuestion().getAnswers().forEach(an -> {
                markList.add(an.getMarks());
            });
            un.setMaxMarks(Collections.max(markList));
            if (markList.size() < 2) {
                un.setMinMarks(0);
            } else {
                un.setMinMarks(Collections.min(markList));
            }
            markList.clear();
            userAnswers.add(un);
            sumOfRawScore = sumOfRawScore + un.getMarks();
            sumOfMaxScore = sumOfMaxScore + un.getMaxMarks();
        }

        // before saving, remove the answer whose question has ignore flag true
        userAnswers.removeIf(userAnswer -> userAnswer.getQuestion().isIgnoreQuestion());

        userAnswerRepository.save(userAnswers);
        userAnswerRepository.flush();

//    timelineService.savePostFromSavedTest(test);

        if (!getTenant().getSubDomain().equalsIgnoreCase("assess")) {
            saveTestResultSummary(sumOfRawScore, sumOfMaxScore, testResult);
        } else {
            Report report = reportService.getReport("Aim Composite Report");
            boolean isEligible = reportService.allowReportToUser(user, report, getTenant());
            if (isEligible) {
                congratsUserForAllowedReport(user, report);
            } else {
                reportService.setUserReportStatus(user, report, ReportStatus.InProgress, null, null);
            }
        }
        if (!user.getFlag().equals("Bulk")) {
            if (!user.getIsPaid()) {
                asyncReportService.sendDemoTestCompleteEmail(user.getId());
            }
        }
//    else if (user.getLanguage()==null)
//      communicationService.demoCompleteMail(user);

        return testResult;
    }

    private TestResultSummary toConstructResultSummary(TestResultSummary testResultSummary, TestResultSummary constructResultSummary) {

        if (constructResultSummary == null) {
            constructResultSummary = new TestResultSummary();

            constructResultSummary.setRecordType(RecordType.Construct);
            constructResultSummary.setUserId(testResultSummary.getUserId());
            constructResultSummary.setStudentName(testResultSummary.getStudentName());
            constructResultSummary.setGrade(testResultSummary.getGrade());
            constructResultSummary.setSection(testResultSummary.getSection());
            constructResultSummary.setConstructId(testResultSummary.getConstructId());
            constructResultSummary.setConstructName(testResultSummary.getConstructName());
        }

        double syllabusCompleted = howMuchTestAreTakenOfAConstruct(testResultSummary.getUserId(), testResultSummary.getConstructId());
        constructResultSummary.setSyllabusCompleted(syllabusCompleted);

        double percentageScore = testResultSummaryRepository.getAveragePercentageScore(testResultSummary.getUserId(), testResultSummary.getConstructId(), getTenant().getId());

        constructResultSummary.setPercentageScore(percentageScore);

        if (!StringUtils.isEmpty(percentageScore)) {
            if (percentageScore < 41) {
                constructResultSummary.setScoreRange(RangeValue.C);
            }
            if (percentageScore > 40 && percentageScore < 71) {
                constructResultSummary.setScoreRange(RangeValue.B);
            }
            if (percentageScore > 70) {
                constructResultSummary.setScoreRange(RangeValue.A);
            }
        }

        return constructResultSummary;
    }

    private double howMuchTestAreTakenOfAConstruct(Long userId, Long constructId) {
        List<Test> allTestByConstruct = testService.getAllTestByProduct(constructId);
        Long numberOfTakenTest = resultRepository.countDistinctByUserIdAndTestIn(userId, allTestByConstruct);

        return (double) ((numberOfTakenTest * 100) / allTestByConstruct.size());
    }

    public void getResultByGradeAndTest(String grade, Long testId) {
        Role role = roleRepository.getOneByName("TestTaker");
        List<User> users = userRepository.findAllByRolesIdAndGradeAndTenant(role.getId(), "7", getTenant());
        for (User user : users) {
            List<TestResult> testResults = resultRepository.findAllByTestIdAndUserIdAndTenant(testId, user.getId(), getTenant());
        }
    }

    public List<ResultResponse> getTestsTakenByUser(Long userId) {
        List<TestResult> testResultList = resultRepository.findAllByUserIdAndTenant(userId, getTenant());
        List<ResultResponse> resultResponseList = new ArrayList<>(testResultList.size());
        ResultResponse resultResponse = new ResultResponse();
        for (TestResult testResult : testResultList) {
            resultResponse.setTestId(testResult.getTest().getId());
            resultResponse.setTestName(testResult.getTest().getName());
            resultResponse.setTestTakenDate(testResult.getTestFinishAt());
            resultResponseList.add(resultResponse);
        }

        return resultResponseList;
    }

    public ResultResponse getResultByUserIdAndTestId(Long userId, Long testId) {
        Test test = testRepository.getOne(testId);

        if (!test.getReportType().equalsIgnoreCase("StudentReport")) {
            throw new ValidationException("report for the selected assessment can't be shown to you.");
        }

        TestResult testResult = resultRepository.findByUserIdAndTestIdAndTenant(userId, testId, getTenant());
        if (testResult == null) {
            throw new ValidationException("Result not found");
        }

        ResultResponse resultResponse = new ResultResponse(testResult);

        resultResponse.getUserAnswerResponses().forEach(userAnswerResponse -> {
            userAnswerResponse.setCorrectAnswerDescription(questionService.getCorrectAnswer(userAnswerResponse.getQuestionId()).getDescription());
        });

        return resultResponse;
    }

    public HashMap<Long, List> getAverageMarksOfProductByGrade(Long productId, Long constructId, String grade, Long studentId, String section) {
        Integer sumOfObtainedMarks = 0;
        Integer sumOfMaximumMarks = 0;
        TestResult testResult;
        List<Test> tests;
        List<Student> users = new ArrayList<>();
        List<Test> products = new ArrayList<>();
        List<Integer> testsPercentScores = new ArrayList<>();
        List<Integer> testsObtainedMarks = new ArrayList<>();
        List<Integer> testsMaximumMarks = new ArrayList<>();
        HashMap<Long, List> productsResult = new HashMap<>();
        HashMap<Long, String> questionIdAndTextAnswers = new HashMap<>();

        List<TextquestionAnswers> textquestionAnswers = textquestionAnswersRepository.findAllByTenant(getTenant());
        textquestionAnswers.forEach(textQue -> {
            questionIdAndTextAnswers.put(textQue.getQuestionId(), textQue.getTextAnswer());
        });

        if (StringUtils.isEmpty(grade)) {
            throw new ValidationException("Please choose grade");
        }
        if (StringUtils.isEmpty(section)) {
            throw new ValidationException("Please choose grade");
        }
        if (studentId == 0) {
//      Role role = roleRepository.getOneByName("TestTaker"); //getting specific role from db for getting users of that role
            users = studentRepository.findAllByGradeAndSectionAndTenant(grade, section, getTenant());//getting  all the users who taken these tests
//      List<Student> students = studentRepository
//          .findAllByTenantAndGradeId(getTenant(), masterDataDocumentRepository.findOneByTypeAndValue("Grade", grade).getId()); //getting  all the users who taken these tests
        } else {
            users.add(studentRepository.findByIdAndTenant(studentId, getTenant()));
        }

        if (productId != 0 && constructId == 0) // for getting data of all constructs of a product
        {  //added single construct
            products.add(testRepository.getOne(productId));
            //products.addAll(testService.getAllConstructByProductId(productId));
        } else {
            if (constructId != 0) // for getting data of single construct
            {//added single subConstruct
                products.add(testRepository.getOne(constructId));
            } else // for getting data of all products
            { //added all construct
                products.addAll(testService.findAllSubjectByGradeAndTeacher(grade)); //pass grade as arguement
            }
        }

        for (Test product : products) {
            List<GradeResultResponse> results = new ArrayList<>();
            if (constructId != 0) {
                tests = testService.getAllVersionTestByConstructId(product.getId()); //pass grade as arguement
            } else {
                tests = testService.getAllTestByProductId(product.getId()); //getting all the test of the product
            }

            for (Student student : users) {
                for (Test test : tests) {
                    testResult = resultRepository.findByUserIdAndTestIdAndTenant(student.getUser().getId(), test.getId(), getTenant());
                    if (testResult != null) {
                        for (UserAnswer userAnswer : testResult.getUserAnswers()) {
                            String questionType = userAnswer.getQuestion().getQuestionType() != null ? userAnswer.getQuestion().getQuestionType() : "";
                            if (questionType.equals("Text")) {
                                if (userAnswer.getText().equals(questionIdAndTextAnswers.get(userAnswer.getQuestion().getId()))) {
                                    userAnswer.setMarks(userAnswer.getMaxMarks());
                                }
                            }

                            sumOfObtainedMarks = userAnswer.getMarks() + sumOfObtainedMarks;// calculating sum of obtained marks of a single test
                            sumOfMaximumMarks = userAnswer.getMaxMarks() + sumOfMaximumMarks;// calculating sum of max marks of a single test
                        }

                        testsMaximumMarks.add(sumOfMaximumMarks);
                        testsObtainedMarks.add(sumOfObtainedMarks);
                        testsPercentScores.add(calculatePercentage(sumOfObtainedMarks, sumOfMaximumMarks)); // adding sum of marks into the list of test marks
                        sumOfObtainedMarks = 0;
                        sumOfMaximumMarks = 0;
                    }
                }

                if (testsPercentScores.size() > 0) {
                    GradeResultResponse gradeResultResponse = new GradeResultResponse();
                    gradeResultResponse.setPercentScores(calculateAverage(testsPercentScores));
                    gradeResultResponse.setObtainedMarks(calculateAverage(testsObtainedMarks));
                    gradeResultResponse.setMaximumMarks(calculateAverage(testsMaximumMarks));
//          gradeResultResponse.setGrade(student.getGrade().getValue());
                    gradeResultResponse.setGrade(student.getGrade());
                    gradeResultResponse.setUserId(student.getId());
                    gradeResultResponse.setStudentName(student.getName());
                    gradeResultResponse.setProductName(product.getName());
                    gradeResultResponse.setProductId(product.getId());
                    results.add(gradeResultResponse);
                    testsPercentScores.clear(); // empty the variable for further use
                    testsMaximumMarks.clear();
                    testsObtainedMarks.clear();
                }
            }
            if (results.size() > 0) {
                productsResult.put(product.getId(), results);
            }
        }
        return productsResult;
    }

    private Float calculateAverage(List<Integer> numberList) {
        Integer sumOfNumbers = 0;
        for (Integer num : numberList) {
            sumOfNumbers += num;
        }
        return (float) (sumOfNumbers / numberList.size());
    }

    private Integer calculatePercentage(Integer num1, Integer num2) {
        return ((num1 * 100) / num2);
    }

    public Map<Long, TestResultSummaryResponse> getTestIdWithNames() {
        TestResultSummaryResponse testResultSummaryResponse;
        List<TestResultSummary> testResultSummaries = testResultSummaryRepository.getDistinctByTestId(getTenant().getId());

        Map<Long, TestResultSummaryResponse> tests = new HashMap<>(testResultSummaries.size());

        for (TestResultSummary testResultSummary : testResultSummaries) {
            testResultSummaryResponse = new TestResultSummaryResponse();
            testResultSummaryResponse.setName(testResultSummary.getTestName());
            testResultSummaryResponse.setType("test");

            tests.put(testResultSummary.getTestId(), testResultSummaryResponse);
        }

        testResultSummaries.clear();
        testResultSummaries = testResultSummaryRepository.getDistinctTopBySyllabusCompleted(getTenant().getId());

        for (TestResultSummary testResultSummary : testResultSummaries) {
            testResultSummaryResponse = new TestResultSummaryResponse();
            testResultSummaryResponse.setName(testResultSummary.getConstructName());
            testResultSummaryResponse.setSyllabusCompleted(testResultSummary.getSyllabusCompleted().intValue());
            testResultSummaryResponse.setType("construct");

            tests.put(testResultSummary.getConstructId(), testResultSummaryResponse);
        }

        return tests;
    }

    public void syncTestResult() {
        List<BigInteger> testResultIds = resultRepository.findAllTestResultIdByLeapTenants();
        List<Integer> markList = new ArrayList<>();
        Long sumOfRawScore = 0L;
        Long sumOfMaxScore = 0L;
        double percentageScore = 0.0;

        for (BigInteger testResultId : testResultIds) {
            sumOfRawScore = 0L;
            sumOfMaxScore = 0L;
            TestResult testResult = resultRepository.getOne(testResultId.longValue());

            for (UserAnswer un : testResult.getUserAnswers()) {
                un.getQuestion().getAnswers().forEach(an -> {
                    markList.add(an.getMarks());
                });
                un.setMaxMarks(Collections.max(markList));

                markList.clear();
                sumOfRawScore = sumOfRawScore + un.getMarks();
                sumOfMaxScore = sumOfMaxScore + un.getMaxMarks();
            }

            try {
                percentageScore = (double) ((sumOfRawScore * 100) / sumOfMaxScore);

                TestResultSummary testResultSummary = new TestResultSummary();
                testResultSummary.setRecordType(RecordType.Test);
                testResultSummary.setUserId(testResult.getUser().getId());
                testResultSummary.setStudentName(testResult.getUser().getName());
                testResultSummary.setGrade(testResult.getUser().getGrade());
                testResultSummary.setSection(testResult.getUser().getSection());
                testResultSummary.setRawScore(sumOfRawScore);
                testResultSummary.setMaxMarks(sumOfMaxScore);
                testResultSummary.setPercentageScore(percentageScore);

                testResultSummary.setTestId(testResult.getTest().getId());
                testResultSummary.setTestName(testResult.getTest().getName());
                if (testResult.getTest().getParent() != null) {
                    testResultSummary.setSubConstructName(testResult.getTest().getParent().getName());
                    testResultSummary.setSubConstructId(testResult.getTest().getParent().getId());

                    if (testResult.getTest().getParent().getParent() != null) {
                        testResultSummary.setConstructName(testResult.getTest().getParent().getParent().getName());
                        testResultSummary.setConstructId(testResult.getTest().getParent().getParent().getId());
                    }
                }

                testResultSummary.setTestResultId(testResult.getId());
                if (testResult.getTest().getTeacher() != null) {
                    testResultSummary.setSubjectTeacherId(testResult.getTest().getTeacher().getId());
                }

                if (percentageScore < 41) {
                    testResultSummary.setScoreRange(RangeValue.C);
                }
                if (percentageScore > 40 && percentageScore < 71) {
                    testResultSummary.setScoreRange(RangeValue.B);
                }
                if (percentageScore > 70) {
                    testResultSummary.setScoreRange(RangeValue.A);
                }

                testResultSummary = testResultSummaryRepository.saveAndFlush(testResultSummary);

                if (testResultSummary.getConstructId() != null) {
                    TestResultSummary constructResultSummary = testResultSummaryRepository
                            .findOneByUserIdAndConstructIdAndRecordTypeAndTenant(testResultSummary.getUserId(), testResultSummary.getConstructId(), RecordType.Construct, getTenant());

                    constructResultSummary = toConstructResultSummary(testResultSummary, constructResultSummary);

                    testResultSummaryRepository.saveAndFlush(constructResultSummary);

                }
            } catch (Exception e) {
                logger.error("something happened wrong in saving TEST_RESULT_SUMMARY for user:- " + testResult.getUser().getUsername() + "testResult:- " + testResult.getId(), e);
            }
        }
    }

    private boolean isErrorInTestResponseSheet(Map<String, String> statusMap, ArrayList<ImportTestResponseModel> responses, Map<String, User> users, Map<Long, Test> tests,
                                               Map<Long, Question> questions) {
        boolean errorInFile = false;
        String validationMessage;
        User user;
        Test test;
        Question question;
        boolean optionExists;

        for (ImportTestResponseModel response : responses) {
            validationMessage = response.validate();

            if (validationMessage.equalsIgnoreCase("ok")) {
                if (!users.containsKey(response.getUsername())) {
                    user = userRepository.findByUsernameAndTenant(response.getUsername(), getTenant());

                    if (user == null) {
                        statusMap.put(response.getUsername(), "User not found in db");
                        errorInFile = true;
                    } else {
                        users.put(user.getUsername(), user);
                    }
                }
                if (!tests.containsKey(response.getTestId())) {
                    test = testRepository.findByIdAndTenant(response.getTestId(), getTenant());

                    if (test == null) {
                        statusMap.put(response.getTestId().toString() + "Test", "not found in db");
                        errorInFile = true;
                    } else {
                        tests.put(test.getId(), test);
                    }
                }

                question = questions.get(response.getQuestionId());
                if (question == null) {
                    question = questionRepository.findByIdAndTenant(response.getQuestionId(), getTenant());

                    if (question == null) {
                        statusMap.put(response.getQuestionId().toString() + "Question", "not found in db");
                        errorInFile = true;
                    } else {
                        questions.put(question.getId(), question);
                    }
                }
                if (question != null) {
                    optionExists = question.getAnswers().stream()
                            .anyMatch(option -> option.getOptionNumber() == response.getOption());
                    if (!optionExists) {
                        statusMap.put("QuestionId :- " + response.getQuestionId().toString(), response.getUsername() + " | option:- " + response.getOption() + " doesn't exist");
                        errorInFile = true;
                    }
                }
            } else {
                statusMap.put(response.getUsername(), validationMessage);
                statusMap.put("error", "true");
            }
        }
        return errorInFile;
    }

    @Transactional
    public Map<String, String> importTestResponsesFromSheet(MultipartFile sheet, String dryRun) {
        Map<String, String> statusMap = new HashMap<>();
        statusMap.put("error", "false");
        Map<String, ArrayList> testResponseSheet = excelImportService.importTestResponses(sheet);
        ArrayList<ImportTestResponseModel> responses = testResponseSheet.get("responses");

        Map<String, User> users = new HashMap<>();
        Map<Long, Test> tests = new HashMap<>();
        Map<Long, Question> questions = new HashMap<>();

        boolean errorInFile = isErrorInTestResponseSheet(statusMap, responses, users, tests, questions);
        if (errorInFile) {
            return statusMap;
        } else {
            if (dryRun.equalsIgnoreCase("false")) {
                Pilot pilot = pilotService.findOneByActive(true);
                Test test;
                User user;
                String username;
                TestResult testResult;
                boolean alreadyExist = false;
                List<ImportTestResponseModel> userTestResponse;
                Map<Long, List<ImportTestResponseModel>> userResponseByTest;
                Answers chosenAnswer = null;
                List<UserAnswer> userAnswers = new ArrayList<>();
                List<Integer> markList = new ArrayList<>();
                Long sumOfRawScore = 0L;
                Long sumOfMaxScore = 0L;
                double percentageScore = 0.0;
                int count = 0;
                StringBuilder msg;
                boolean isEligible = false;
                Report report = reportService.getReport("Aim Composite Report");

                if (StringUtils.isEmpty(pilot)) {
                    throw new ValidationException("Current Pilot Exception");
                }

                Map<String, Map<Long, List<ImportTestResponseModel>>> userResponsesByTest = new HashMap<>();

                Map<String, List<ImportTestResponseModel>> allResponsesByUser = responses.parallelStream()
                        .collect(groupingBy(ImportTestResponseModel::getUsername));
                allResponsesByUser.forEach((user_name, userResponses) -> {
                    Map<Long, List<ImportTestResponseModel>> responsesByTest = userResponses.parallelStream()
                            .collect(groupingBy(ImportTestResponseModel::getTestId));
                    userResponsesByTest.put(user_name, responsesByTest);
                });

                //          Iterating Map By User
                for (Map.Entry<String, Map<Long, List<ImportTestResponseModel>>> entryByUser : userResponsesByTest.entrySet()) {
                    username = entryByUser.getKey();
                    userResponseByTest = entryByUser.getValue();
                    user = users.get(username);

//          Iterating Map By Test for same User
                    for (Map.Entry<Long, List<ImportTestResponseModel>> userEntryByTest : userResponseByTest.entrySet()) {
                        test = tests.get(userEntryByTest.getKey());
                        userTestResponse = userEntryByTest.getValue();

                        testResult = new TestResult();
                        testResult.setPilot(pilot);
                        testResult.setUser(user);
                        testResult.setTest(test);

                        testResult.setTestBeginAt(null);
                        testResult.setTestFinishAt(null);
//            testResult.setTestBeginAt(DateUtil.convertStringToDate(userTestResponse.get(0).getTestStartTimeStamp(),-330));
//            testResult.setTestFinishAt(DateUtil.convertStringToDate(userTestResponse.get(0).getTestFinishTimeStamp(),-330));
//

                        testResult.setSubConstructName(test.getParent() != null ? test.getParent().getName() : "");

                        alreadyExist = resultRepository.countByUserAndTestAndTenant(testResult.getUser(), testResult.getTest(), getTenant()) > 0;

                        if (!alreadyExist) {
                            testResult = resultRepository.saveAndFlush(testResult);
                            userAnswers.clear();
                            markList.clear();
                            sumOfRawScore = 0L;
                            sumOfMaxScore = 0L;

//            Iterating List of user responses for a test for a user
                            for (ImportTestResponseModel response : userTestResponse) {
                                Question question = questionRepository.findByIdAndTenant(response.getQuestionId(), getTenant());
                                for (Answers ans : question.getAnswers()) {
                                    if (ans.getOptionNumber() == response.getOption()) {
                                        chosenAnswer = ans;
                                    }
                                }
//                chosenAnswer = answersRepository.findOne(response.getOption());
                                if (chosenAnswer == null) {
                                    throw new NotFoundException("chosen option not found");
                                }
                                UserAnswer userAnswer = new UserAnswer();
                                userAnswer.setAnswerId(chosenAnswer.getId());
                                userAnswer.setQuestion(chosenAnswer.getQuestion());
                                userAnswer.setTestResult(testResult);
                                userAnswer.setTimeTaken(0);
                                userAnswer.setDescription(chosenAnswer.getDescription());
                                userAnswer.setMarks(chosenAnswer.getMarks());
                                userAnswer.setHasText(chosenAnswer.isHasText());
                                userAnswer.setHasAudio(chosenAnswer.isHasAudio());
                                userAnswer.setHasImage(chosenAnswer.isHasImage());
                                userAnswer.setOptionNumber(chosenAnswer.getOptionNumber());
                                if (userAnswer.isHasText()) {
                                    userAnswer.setText(response.getText());
                                }

                                userAnswer.getQuestion().getAnswers().forEach(an -> {
                                    markList.add(an.getMarks());
                                });
                                userAnswer.setMaxMarks(Collections.max(markList));
                                if (markList.size() < 2) {
                                    userAnswer.setMinMarks(0);
                                } else {
                                    userAnswer.setMinMarks(Collections.min(markList));
                                }

                                if (!userAnswer.getQuestion().isIgnoreQuestion()) {
                                    sumOfRawScore = sumOfRawScore + userAnswer.getMarks();
                                    sumOfMaxScore = sumOfMaxScore + userAnswer.getMaxMarks();

                                    count = userAnswerRepository.countByTestResultAndQuestionAndTenant(userAnswer.getTestResult(), userAnswer.getQuestion(), getTenant());
                                    if (count == 0) {
                                        userAnswers.add(userAnswer);
                                    }
                                }
                            }

                            userAnswerRepository.save(userAnswers);
                            userAnswerRepository.flush();
                            if (!user.getTenant().getName().equalsIgnoreCase("aim")) {
                                saveTestResultSummary(sumOfRawScore, sumOfMaxScore, testResult);
                            } else {
                                isEligible = reportService.allowReportToUser(user, report, user.getTenant());
                                if (isEligible) {
                                    congratsUserForAllowedReport(user, report);
                                } else {
                                    reportService.setUserReportStatus(user, report, ReportStatus.InProgress, null, null);
                                }
                            }
                            msg = new StringBuilder(testResult.getUser().getUsername() + "'s :-" + testResult.getTest().getName() + "'s result saved");
                            logger.info(msg.toString());
                        }
                    }
                }
//        report will be made by scheduler
//        asyncReportService.processMultipleReports(users.values(), getTenant());

//        send email
                String[] emails = {"vedantb@intscale.com", "mayankb@intscale.com"};
                communicationService.testUploaded(emails, users.keySet().size());
            }
        }
        return statusMap;
    }

    private void saveTestResultSummary(Long sumOfRawScore, Long sumOfMaxScore, TestResult testResult) {
        double percentageScore;
        try {
            try {
                percentageScore = (double) ((sumOfRawScore * 100) / sumOfMaxScore);
            } catch (ArithmeticException ae) {
                percentageScore = 0.0;
            }

            TestResultSummary testResultSummary = new TestResultSummary();
            testResultSummary.setRecordType(RecordType.Test);
            testResultSummary.setUserId(testResult.getUser().getId());
            testResultSummary.setStudentName(testResult.getUser().getName());
            testResultSummary.setGrade(testResult.getUser().getGrade());
            testResultSummary.setSection(testResult.getUser().getSection());
            testResultSummary.setRawScore(sumOfRawScore);
            testResultSummary.setMaxMarks(sumOfMaxScore);
            testResultSummary.setPercentageScore(percentageScore);

            testResultSummary.setTestId(testResult.getTest().getId());
            testResultSummary.setTestName(testResult.getTest().getName());
            if (testResult.getTest().getParent() != null) {
                testResultSummary.setSubConstructName(testResult.getTest().getParent().getName());
                testResultSummary.setSubConstructId(testResult.getTest().getParent().getId());

                if (testResult.getTest().getParent().getParent() != null) {
                    testResultSummary.setConstructName(testResult.getTest().getParent().getParent().getName());
                    testResultSummary.setConstructId(testResult.getTest().getParent().getParent().getId());
                }
            }

            testResultSummary.setTestResultId(testResult.getId());
            if (testResult.getTest().getTeacher() != null) {
                testResultSummary.setSubjectTeacherId(testResult.getTest().getTeacher().getId());
            }

            if (percentageScore < 41) {
                testResultSummary.setScoreRange(RangeValue.C);
            }
            if (percentageScore > 40 && percentageScore < 71) {
                testResultSummary.setScoreRange(RangeValue.B);
            }
            if (percentageScore > 70) {
                testResultSummary.setScoreRange(RangeValue.A);
            }

            testResultSummary = testResultSummaryRepository.saveAndFlush(testResultSummary);

            if (testResultSummary.getConstructId() != null) {
                TestResultSummary constructResultSummary = testResultSummaryRepository
                        .findOneByUserIdAndConstructIdAndRecordTypeAndTenant(testResultSummary.getUserId(), testResultSummary.getConstructId(), RecordType.Construct, getTenant());

                constructResultSummary = toConstructResultSummary(testResultSummary, constructResultSummary);

                testResultSummaryRepository.saveAndFlush(constructResultSummary);

            }
        } catch (Exception e) {
            logger.error("something happened wrong in saving TEST_RESULT_SUMMARY", e);
        }
    }

    public String downloadTestResponsesInSheet(DashboardRequest request) throws IOException {
        String username;
        String name;
        Long testId;

        Path path = Paths.get(filedir);
        String filename = path.resolve(UUID.randomUUID().toString() + ".csv").toString();
        FileWriter fw = new FileWriter(filename);
        fw.append("USERNAME,NAME,TEST-ID,QUESTION-ID,OPTION,OPTION-TEXT,TEST BEGIN AT,TEST FINISHED AT\n");

        Date fromDate = DateUtil.getFormattedDate(request.getFromDate(), START_OF_DAY);
        Date toDate = DateUtil.getFormattedDate(request.getToDate(), END_OF_DAY);
        if (fromDate == null) {
            fromDate = new Date();
        }
        if (toDate == null) {
            toDate = new Date();
        }

        List<TestResult> testResults = getTestResults(request, fromDate, toDate);

        for (TestResult testResult : testResults) {
            username = testResult.getUser().getUsername();
            name = testResult.getUser().getName();
            testId = testResult.getTest().getId();
            for (UserAnswer userAnswer : testResult.getUserAnswers()) {
                fw.append(String.format("%s,%s,%d,%d,", username, name, testId, userAnswer.getQuestion().getId()));
                try {
                    fw.append(String.format("%d,", userAnswer.getOptionNumber()));
//          fw.append(String.format("%d,", userAnswer.getQuestion().getAnswers().get(userAnswer.getOptionNumber() - 1).getId()));
                } catch (ArrayIndexOutOfBoundsException e) {
                    logger.error(username + "'s chosen option number:- " + userAnswer.getOptionNumber() + "  NOT FOUND IN    ||    Q.Id:- " + userAnswer.getQuestion().getId());
                    throw new ValidationException(username + "'s chosen option number:- " + userAnswer.getOptionNumber() + "  NOT FOUND IN Q.Id:- " + userAnswer.getQuestion().getId());
                }

                fw.append(userAnswer.isHasText() ? userAnswer.getText() + "," : "-,");
                fw.append(DateUtil.formatDate(testResult.getTestBeginAt())).append(",");
                fw.append(DateUtil.formatDate(testResult.getTestFinishAt())).append("\n");
            }
        }
        fw.flush();
        fw.close();

        return filename;
    }

    private List<TestResult> getTestResults(DashboardRequest request, Date fromDate, Date toDate) {
        JPAQuery<TestResult> query = new JPAQuery<>(entityManager);
        QTestResult testResult = QTestResult.testResult;
        query.from(testResult)
                .distinct()
                .select(testResult)
                .where(testResult.testFinishAt.between(fromDate, toDate));

        if (!CollectionUtils.isEmpty(request.getSchools())) {
            query.where(testResult.user.schoolName.in(request.getSchools()));
        }

        return query.fetch();
    }

    public void congratsUserForAllowedReport(User user, Report report) {
        reportService.setUserReportStatus(user, report, ReportStatus.Wait, null, null);

//    logger.info("Sending a notification email on completion of all assessments to " + user.getName());
        communicationService.sendNotificationUponCompletionOfTests(user);

        timelineService.savePostForAimReport(user);
    }
}