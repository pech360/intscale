package com.synlabs.intscale.service;

import com.synlabs.intscale.ImportModel.*;
import com.synlabs.intscale.entity.ProductTenant;
import com.synlabs.intscale.entity.Report.AimExpertInventory;
import com.synlabs.intscale.entity.Report.Category;
import com.synlabs.intscale.entity.Pilot;
import com.synlabs.intscale.entity.Report.Category;
import com.synlabs.intscale.entity.TestPerTenant;
import com.synlabs.intscale.entity.masterdata.MasterDataDocument;
import com.synlabs.intscale.entity.test.*;
import com.synlabs.intscale.entity.threesixty.StakeholderQuestion;
import com.synlabs.intscale.entity.user.Role;
import com.synlabs.intscale.entity.user.Teacher;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.enums.TestStatus;
import com.synlabs.intscale.ex.NotFoundException;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.*;
import com.synlabs.intscale.service.communication.CommunicationService;
import com.synlabs.intscale.service.threesixty.StakeholderQuestionService;
import com.synlabs.intscale.view.*;
import com.synlabs.intscale.view.TestTreeResponse.TestChild;
import com.synlabs.intscale.view.TestTreeResponse.TestLeaf;
import com.synlabs.intscale.view.TestTreeResponse.TestParent;
import com.synlabs.intscale.view.TestTreeResponse.TestSubParent;
import com.synlabs.intscale.view.usertest.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class TestService extends BaseService {

    @Autowired
    private TestRepository testRepository;
    @Autowired
    private QuestionRepository questionsRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ExcelImportService excelImportService;
    @Autowired
    private MasterDataDocumentService masterDataDocumentService;
    @Autowired
    private AnswersRepsitory answersRepsitory;
    @Autowired
    private ResultRepository resultRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TestQuestionRepository testQuestionRepository;
    @Autowired
    private TenantTestAllotmentRepository testAllotmentRepository;
    @Autowired
    private UserTestRepository userTestRepository;
    @Autowired
    private ReportService reportService;
    @Autowired
    private UserService userService;
    @Autowired
    private PilotService pilotService;
    @Autowired
    private TeacherRepository teacherRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private UserAnswerRepository userAnswerRepository;
    @Autowired
    private QuestionTagRepository questionTagRepository;
    @Autowired
    private SubTagRepository subTagRepository;
    @Autowired
    private StakeholderQuestionService stakeholderQuestionService;
    @Autowired
    private ProductTenantRepository productTenantRepository;
    @Autowired
    private TestPerTenantRepository testPerTenantRepository;
    @Autowired
    private TenantRepository tenantRepository;
    @Autowired
    private CommunicationService communicationService;



    //This should return user tests that have been taken !!
    public List<TestDescriptionResponse> getUserTests() {
        List<TestResult> testResultList = resultRepository.findAllByUserIdAndTenant(getUser().getId(), getTenant());

        List<TestDescriptionResponse> testDescriptionResponseList = new ArrayList<>(testResultList.size());
        for (TestResult testResult : testResultList) {
            testDescriptionResponseList.add(new TestDescriptionResponse(testResult.getTest()));
        }
        return testDescriptionResponseList;
    }

    public List<TestDescriptionResponse> getTakenTestsByUserId(Long userId) {
        List<TestResult> testResultList = resultRepository.findAllByUserIdAndTenant(userId, getTenant());

        List<TestDescriptionResponse> testDescriptionResponseList = new ArrayList<>(testResultList.size());
        for (TestResult testResult : testResultList) {
            testDescriptionResponseList.add(new TestDescriptionResponse(testResult.getTest()));
        }
        return testDescriptionResponseList;
    }

    public Test saveTest(TestRequest request) {
        Test test = request.toEntity(new Test());
        test = testRepository.saveAndFlush(test);
        test.setVersion(1l);
        test.setLatestVersion(true);
        test.setVersionId(null);
        Question question;
        for (int i = 0; i < request.getQuestions().size(); i++) {
            question = request.getQuestions().get(i);
            TestQuestion testQuestion = new TestQuestion(test, question, i);
            testQuestion = testQuestionRepository.saveAndFlush(testQuestion);
            test.addQuestions(testQuestion);
        }
        test.setParent(testRepository.findOne(request.getParentId()));
        return testRepository.saveAndFlush(test);
    }

    //This should return all tests available in system for user to take test, not just the user tests that already have been taken!!
    public Set<TestDescriptionResponse> availableTests() {
        //todo add free test list by default
        UserTest userTest = userTestRepository.findByUserEmailAndTenant(getUser(), getTenant());
        return new HashSet<TestDescriptionResponse>(userTest.getTests().stream().map(TestDescriptionResponse::new).collect(Collectors.toList()));

    }

    public List<TestDescriptionResponse> listAllTests() {
        List<Test> allTests = new ArrayList<>(testRepository.findAllByLatestVersionIsTrueAndTenant(getTenant()));
        return new ArrayList<>(allTests.stream().map(TestDescriptionResponse::new).collect(Collectors.toList()));
    }

    public List<TestDescriptionResponse> listAllTestWithAllVersions() {
        List<Test> allTests = new ArrayList<>(testRepository.findAllByLeafNodeIsFalseAndConstructIsFalseAndParentIsNotNullAndTenant(getTenant()));
        return new ArrayList<>(allTests.stream().map(TestDescriptionResponse::new).collect(Collectors.toList()));
    }

    public Test getOne(Long id) {
        Test test = testRepository.findOne(id);
        return test;
    }

    public Test getTestWithTestQuestionsWithDerivedCategories(Long id) {
        Test test = testRepository.findOne(id);
        return test;
    }

    public TestDescriptionResponse findOne(Long id) {
        return new TestDescriptionResponse(testRepository.findOne(id));
    }

    public void attachTestImage(String filename, Long testId, boolean isStakeholderImage) {
        Test test = testRepository.findOne(testId);
        if (isStakeholderImage) {
            test.setStakeholderImage(filename);
        } else {
            test.setLogo(filename);
        }
        testRepository.save(test);
    }

    public void attachTestVideo(String filename, Long testId, boolean stakeholderVideo) {
        Test test = testRepository.findOne(testId);
        if (stakeholderVideo) {
            test.setStakeholderVideo(filename);
        } else {
            test.setVideoName(filename);
        }
        testRepository.save(test);
    }

    public void deleteTest(Long id) {
        testRepository.delete(id);
    }

  /*public Test updateTest(TestRequest request, boolean version)
  {
    Test test = null;
    Long versionCount = 0l;
    Long versionId = null;
    if (version)
    {
      test = testRepository.findOne(request.getId());
      test.setLatestVersion(false);
      test.setPublished(false);
      versionCount = test.getVersion() + 1;
      versionId = test.getVersionId();
      testRepository.save(test);
      test = new Test();
    }
    else
    {
      test = testRepository.findOne(request.getId());
    }
    test = request.toEntity(test);
    if (version)
    {
      test.setVersion(versionCount);
      if (versionId == null)
      {
        test.setVersionId(request.getId());
      }
      else
      {
        test.setVersionId(versionId);
      }
      test.setLatestVersion(true);
      test.setGoLive(false);
      test.setPublished(false);
    }
    List<TestQuestion> questions = new ArrayList<>();
    List<TestQuestion> existQuestions = test.getQuestions();
    if (request.getQuestions() != null)
    {
      test = testRepository.saveAndFlush(test);
      Question question;
      Set<Long> existIds = new HashSet<>();
      Set<Long> currentIds = new HashSet<>();
      existQuestions.forEach(tq -> {
        existIds.add(tq.getQuestion().getId());
      });
      request.getQuestions().forEach(cq -> {
        currentIds.add(cq.getId());
      });
      //add question
      for (int i = 0; i < request.getQuestions().size(); i++)
      {
        question = request.getQuestions().get(i);
        if (!existIds.contains(question.getId()))
        {
          TestQuestion testQuestion = new TestQuestion(test, question, i);
          testQuestion = testQuestionRepository.saveAndFlush(testQuestion);
          test.addQuestions(testQuestion);
        }
      }
      //Remove question
      for (int i = 0; i < test.getQuestions().size(); i++)
      {
        TestQuestion testQuestion = test.getQuestions().get(i);
        if (!currentIds.contains(testQuestion.getQuestion().getId()))
        {
          test.getQuestions().remove(i);
        }
      }
      existIds.clear();
      currentIds.clear();
    }
    test.setParent(testRepository.findOne(request.getParentId()));
    return testRepository.saveAndFlush(test);
  }*/

    @Transactional
    public Test updateTest(TestRequest request, boolean version) {
        Test test = null;
        Test oldTest = null;
        Long versionCount = 0l;
        Long versionId = null;
        test = testRepository.findOne(request.getId());
        List<TestQuestion> existQuestions = test.getQuestions();
        if (version) {
            test.setLatestVersion(false);
            test.setPublished(false);
            versionCount = test.getVersion() + 1;
            versionId = test.getVersionId();
            oldTest = testRepository.save(test);
            test = new Test();
        }
        test = request.toEntity(test);
        if (version) {
            test.setVersion(versionCount);
            if (versionId == null) {
                test.setVersionId(request.getId());
            } else {
                test.setVersionId(versionId);
            }
            test.setLatestVersion(true);
            test.setGoLive(false);
            test.setPublished(false);
        }
        if (!CollectionUtils.isEmpty(request.getQuestions())) {
            test = testRepository.saveAndFlush(test);
            Question question;
            Set<Long> existIds = new HashSet<>();
            Set<Long> currentIds = new HashSet<>();
            existQuestions.forEach(tq -> {
                existIds.add(tq.getQuestion().getId());
            });
            request.getQuestions().forEach(cq -> {
                currentIds.add(cq.getId());
            });
            //add question
            for (int i = 0; i < request.getQuestions().size(); i++) {
                question = request.getQuestions().get(i);
                if (!existIds.contains(question.getId())) {
                    TestQuestion testQuestion = new TestQuestion(test, question, i);
                    testQuestion = testQuestionRepository.saveAndFlush(testQuestion);
                    test.addQuestions(testQuestion);
                }
            }
            if (version) {
//        for(TestQuestion tq:existQuestions){
//          tq.setTest(test);
//          test.addQuestions(tq);
                for (int i = 0; i < request.getQuestions().size(); i++) {
                    question = request.getQuestions().get(i);
                    if (existIds.contains(question.getId())) {

                        for (TestQuestion eQ : existQuestions) {

                            if (eQ.getQuestion().getId().equals(question.getId())) {

                                TestQuestion testQuestion = new TestQuestion(test, eQ.getQuestion(), eQ.getCategory(), eQ.getSubCategory(), i);
                                testQuestion = testQuestionRepository.saveAndFlush(testQuestion);
                                test.addQuestions(testQuestion);
                            }
                        }

                    }
                }
//        }
            }
            //Remove question
//      for(Long id: existIds){
//        if(!currentIds.contains(id)){
//          for(int i = 0; i <test.getQuestions().size() ; i++){
//            TestQuestion testQuestion=test.getQuestions().get(i);
//            if(testQuestion.getQuestion().getId().equals(id)){
//              test.getQuestions().remove(i);
//            }
//          }
//        }
//      }
            existIds.clear();
            currentIds.clear();
        }
        if (oldTest != null) {
            test.setParent(oldTest.getParent());
        }

        test = testRepository.saveAndFlush(test);

        if (version && oldTest != null) {
            updateTestPerTenantWhenVersionGetChanged(oldTest, test);
        }
        if (!test.getId().equals(request.getId())) {
            Test fromTest = testRepository.findOne(request.getId());
            List<StakeholderQuestion> questions = fromTest.getStakeholderQuestions();

            for (StakeholderQuestion question : questions) {
                stakeholderQuestionService.createCopy(question, test);
            }
        }
        return test;
    }

    private void updateTestPerTenantWhenVersionGetChanged(Test oldVersionTest, Test newVersionTest) {
        List<Product> products = productRepository.findAllByTestId(oldVersionTest.getId());
        products.forEach(product -> {
            List<Test> testList = product.getTest();
            testList.remove(oldVersionTest);
            testList.add(newVersionTest);
            product.setTest(testList);
            productRepository.save(product);
        });
        List<TestPerTenant> testPerTenants = testPerTenantRepository.findAllByTestId(oldVersionTest.getId());
        testPerTenants.forEach(testPerTenant -> {
            testPerTenant.setDescription(testPerTenant.getDescription() + " (Version Updated By Admin)");
            testPerTenant.setPublished(false);
            testPerTenant.setTestId(newVersionTest.getId());
            testPerTenantRepository.save(testPerTenant);
        });
    }

    private Question convertImportQuestionModeltoQuestion(ImportQuestionModel importQuestion) {
        Question question = new Question();
        SubTag subTag;
        List<SubTag> subTagsList = new ArrayList<>();
        Set<String> subTagsRequestSet = new HashSet<>(); // for seprating comma separated subtags into Hashset
        question.setDescription(importQuestion.getDescription());
        question.setQuestionType(importQuestion.getQuestionType());
        question.setGroupName(importQuestion.getGroupName());
        question.setNumberOfAnswers(importQuestion.getNumberOfAnswers());
        question.setHint(importQuestion.getHint());
        question.setIgnoreQuestion(Boolean.valueOf(importQuestion.getIgnoreQuestion()));
        QuestionTag questionTag = questionTagRepository.findByNameAndTenant(importQuestion.getQuestionTag(), getTenant());
        Collections.addAll(subTagsRequestSet, StringUtils.isEmpty(importQuestion.getSubTag()) ? new String[0] : importQuestion.getSubTag().split(","));
        for (String subTagsRequest : subTagsRequestSet) {
            if (questionTag != null) {
                subTag = subTagRepository.findByNameAndTagAndTenant(subTagsRequest, questionTag, getTenant());
                if (subTag != null) {
                    subTagsList.add(subTag);
                }
            }
        }
        question.setSubTags(subTagsList);
        if (Boolean.valueOf(importQuestion.getHasDummy())) {
            question.setHasDummy(true);
        }

        if (!StringUtils.isEmpty(importQuestion.getGroupName()) && masterDataDocumentService.findOneByValue(question.getGroupName()) == null) {
            MasterDataDocument md = new MasterDataDocument();
            md.setType("Group");
            md.setValue(importQuestion.getGroupName());
            masterDataDocumentService.save(md);
        }

        return question;
    }

    private Answers convertImportAnswerModeltoAnswer(ImportAnswerModel importAnswer, int optionNumberCounter, Question tempQuestion) {
        Answers answers = new Answers();
        answers.setDescription(importAnswer.getDescription());
        answers.setMarks(importAnswer.getMarks());
        answers.setOptionNumber(optionNumberCounter);
        answers.setQuestion(tempQuestion);
        return answers;
    }

    private Category convertImportCategoryModeltoCategory(ImportCategoryModel importCategory) {
        Category category = new Category();
        category.setName(importCategory.getCategoryName());
        category.setDescription(importCategory.getCategoryDescription());
        category.setActive(true);
        if (importCategory.getParent() != null) {
            if (importCategory.getCategoryName().equals(importCategory.getParent())) {
                category.setParent(null);
            } else {
                category.setParent(categoryRepository.findByNameAndTenant(importCategory.getParent(), getTenant()));
            }
        }
        if (StringUtils.isEmpty(importCategory.getColor())) {
            category.setColor(null);
        } else {
            category.setColor(importCategory.getColor());
        }
        return category;
    }

    private QuestionTag convertQuestionTagModelToQuestionTag(ImportQuestionTagModel importQuestionTagModel) {
        QuestionTag questionTag = new QuestionTag();
        questionTag.setName(importQuestionTagModel.getTagName());
        return questionTag;
    }

    private SubTag convertQuestionTagModelToSubTag(ImportQuestionTagModel importQuestionTagModel) {
        SubTag subTag = new SubTag();
        subTag.setName(importQuestionTagModel.getTagName());
        QuestionTag questionTag = questionTagRepository.findByNameAndTenant(importQuestionTagModel.getParentTagName(), getTenant());
        if (questionTag != null) {
            subTag.setTag(questionTag);
        }
        return subTag;
    }

    private Test convertImportTestModeltoCategory(ImportTestModel importTest) {
        Test test = new Test();
        Set<String> gradeSet = new HashSet<>();
        test.setName(importTest.getName());
        test.setIntroduction(importTest.getIntroduction());
        test.setDescription(importTest.getDescription());
        test.setProcedure(importTest.getProcedure());
        test.setParticipation(importTest.getParticipation());
        test.setDisclaimer(importTest.getDisclaimer());
        test.setNotes(importTest.getNotes());
        test.setDisableSubmit(true);
        Collections.addAll(gradeSet, StringUtils.isEmpty(importTest.getGrades()) ? new String[0] : importTest.getGrades().split(","));
        test.setGrades(gradeSet.toArray(new String[gradeSet.size()]));
        return test;
    }

    private Test assignTestQuestionsToTest(Test test, List<Question> savedQuestionList, List<Category> categoryList, List<Category> subCategoryList) {
//    List<Category> categories = new ArrayList<>();
        for (int counter = 0; counter < savedQuestionList.size(); counter++) {
//      categories.add(categoryList.get(counter));
//      categories.add(subCategoryList.get(counter));

            TestQuestion testQuestion = new TestQuestion(test, savedQuestionList.get(counter), categoryList.get(counter), subCategoryList.get(counter), counter);
            testQuestion = testQuestionRepository.save(testQuestion);
            test.addQuestions(testQuestion);
//      categories.clear();
        }
        return test;
    }

    public Test publishTest(Long id, Boolean publish) {
        Test test = testRepository.findOne(id);
        test.setPublished(publish);
        testRepository.save(test);
        return test;
    }

    public List<Test> getAllTakenTestByTenant() {
        List<Test> tests = new ArrayList<>();
        resultRepository.findAllTestByTenantId(getTenant().getId()).forEach(id -> {
            tests.add(testRepository.findOne(id.longValue()));
        });
        return tests;
    }

    @Transactional
    public Map<String, String> bulkTestUpload(MultipartFile file, String dryrun, String testType) throws Exception {
        if (testType.isEmpty()) {
            throw new NotFoundException("test type not found");
        }
        boolean errorInFile = false;
        String validationMessage;
        Map<String, String> statusMap = new HashMap<>();
        statusMap.put("error", "false");

        Map<String, ArrayList> testRecords = excelImportService.importTestRecords(file);
        ArrayList<ImportQuestionModel> importQuestionModels = testRecords.get("questions");// contains all questions
        List<Question> questionList = new ArrayList<>(importQuestionModels.size());
        ArrayList<ImportAnswerModel> importAnswerModel = testRecords.get("answers");// contains all answers
        List<Answers> answersList = new ArrayList<>();
        Map<Integer, List<ImportAnswerModel>> importAnswerModelMap = new HashMap<>(importAnswerModel.size()); //for mapping of answers to questions
        ArrayList<ImportCategoryModel> importCategoryModels = testRecords.get("categories");// contains all categories
        ArrayList<ImportTestModel> importTestModel = testRecords.get("tests");// contains test
        ArrayList<ImportQuestionTagModel> importQuestionTagModels = testRecords.get("questionTags");// contains test

//    List<Category> categoryList = new ArrayList<>(importCategoryModels.size());
//    List<Test> testList = new ArrayList<>(importTestModel.size());
        int optionNumberCounter = 1;

        errorInFile = vaidateCategory(errorInFile, statusMap, importCategoryModels);

        errorInFile = vaidateQuestionTags(errorInFile, statusMap, importQuestionTagModels);

        errorInFile = validateQuestions(errorInFile, statusMap, importQuestionModels);

        errorInFile = validateAnswers(testType, errorInFile, statusMap, importAnswerModel);

        errorInFile = validateTest(errorInFile, statusMap, importTestModel);

        if (testType.equalsIgnoreCase("Mcq Test")) {
            //filter answers w.r.t. questions
            for (ImportAnswerModel importAnswer : importAnswerModel) {
                List<ImportAnswerModel> list = importAnswerModelMap.get(importAnswer.getQuestionNumber());
                if (list == null) {
                    list = new ArrayList<ImportAnswerModel>();
                    importAnswerModelMap.put(importAnswer.getQuestionNumber(), list);
                }
                list.add(importAnswer);
            }
        }

        if (!errorInFile && dryrun.equalsIgnoreCase("false")) {
            saveCategories(importCategoryModels);
            saveQuestionTags(importQuestionTagModels);
            if (testType.equalsIgnoreCase("Mcq Test")) {
                if (!importQuestionModels.get(0).getQuestionType().equalsIgnoreCase("Mcq")) {
                    throw new ValidationException("Please Choose Mcq Test Or Upload Mcq questions");
                }
            }

            if (testType.equalsIgnoreCase("Group Question Test")) {
                if (!importQuestionModels.get(0).getQuestionType().equalsIgnoreCase("Group")) {
                    throw new ValidationException("Please Choose 'Group Question Test' Or Upload Group questions");
                }
            }

            Question savedQuestion;
            List<ImportAnswerModel> importAnswerModelList = new ArrayList<>();
            List<Question> savedQuestionList = new ArrayList<>(questionList.size());
            List<Category> categoryListToAttachWithTestQuestion = new ArrayList<>(questionList.size());
            List<Category> subCategoryListToAttachWithTest = new ArrayList<>(questionList.size());
            for (ImportQuestionModel importQuestion : importQuestionModels)// saving questions in db
            {
                savedQuestion = questionsRepository.save(convertImportQuestionModeltoQuestion(importQuestion));
                categoryListToAttachWithTestQuestion.add(categoryRepository.findByNameAndTenant(importQuestion.getCategory(), getTenant()));
                subCategoryListToAttachWithTest.add(categoryRepository.findByNameAndTenant(importQuestion.getSubCategory(), getTenant()));
                savedQuestionList.add(savedQuestion);
                if (testType.equalsIgnoreCase("Mcq Test"))// saving filtered answers with corresponding questions
                {

                    importAnswerModelList = importAnswerModelMap.get(importQuestion.getQuestionNumber());
                    for (ImportAnswerModel importAnswer : importAnswerModelList) {
                        answersList.add(convertImportAnswerModeltoAnswer(importAnswer, optionNumberCounter++, savedQuestion));
                    }
                    optionNumberCounter = 1;
                }
                if (testType.equalsIgnoreCase("Group Question Test")) {
                    if (Boolean.valueOf(importQuestion.getIsReverse())) {
                        for (ImportAnswerModel importAnswer : reverse(importAnswerModel)) {
                            answersList.add(convertImportAnswerModeltoAnswer(importAnswer, optionNumberCounter++, savedQuestion));
                        }
                        optionNumberCounter = 1;
                    } else {
                        for (ImportAnswerModel importAnswer : importAnswerModel) {
                            answersList.add(convertImportAnswerModeltoAnswer(importAnswer, optionNumberCounter++, savedQuestion));
                        }
                        optionNumberCounter = 1;
                    }
                }
                answersRepsitory.save(answersList);
                answersList.clear();
                if (testType.equalsIgnoreCase("Mcq Test")) {
                    importAnswerModelList.clear();
                }
            }

            saveTests(importTestModel, savedQuestionList, categoryListToAttachWithTestQuestion, subCategoryListToAttachWithTest);
        }
        if (errorInFile) {
            statusMap.put("error", "true");
        }
        return statusMap;
    }

    private void saveTests(ArrayList<ImportTestModel> importTestModel, List<Question> savedQuestionList, List<Category> categoryList, List<Category> subCategoryList) {
        for (ImportTestModel importTest : importTestModel)// saving test
        {
            Test test = testRepository.save(convertImportTestModeltoCategory(importTest));
            test = assignTestQuestionsToTest(test, savedQuestionList, categoryList, subCategoryList);
            test.setVersion(1l);
            test.setLatestVersion(true);
            test.setVersionId(null);
            testRepository.save(test);
        }
    }

    private void saveCategories(ArrayList<ImportCategoryModel> importCategoryModels) {
        Category category;
        for (ImportCategoryModel importCategory : importCategoryModels) //saving categories in db
        {
            category = convertImportCategoryModeltoCategory(importCategory);

            if (categoryRepository.findByNameAndTenant(category.getName(), getTenant()) == null) {
                categoryRepository.save(category);
            }
        }
    }

    private void saveQuestionTags(ArrayList<ImportQuestionTagModel> importQuestionTagModels) {
        Category category;
        QuestionTag questionTag;
        SubTag subTag;
        for (ImportQuestionTagModel importQuestionTagModel : importQuestionTagModels) //saving QuestionTag in db
        {

            if (importQuestionTagModel.getTagName().equalsIgnoreCase(importQuestionTagModel.getParentTagName())) {
                questionTag = convertQuestionTagModelToQuestionTag(importQuestionTagModel);
                if (questionTagRepository.findByNameAndTenant(questionTag.getName(), getTenant()) == null) {
                    questionTagRepository.saveAndFlush(questionTag);
                }
            } else {
                subTag = convertQuestionTagModelToSubTag(importQuestionTagModel);
                if (subTagRepository.findByNameAndTagAndTenant(subTag.getName(), subTag.getTag(), getTenant()) == null) {
                    subTagRepository.saveAndFlush(subTag);
                }
            }
        }
    }

    private boolean validateTest(boolean errorInFile, Map<String, String> statusMap, ArrayList<ImportTestModel> importTestModel) {
        String validationMessage;
        for (ImportTestModel importTest : importTestModel) //validating Test
        {
            validationMessage = importTest.validate();
            statusMap.put(importTest.getName(), validationMessage);
            if (!validationMessage.equalsIgnoreCase("ok")) {
                errorInFile = true;
            }
        }
        return errorInFile;
    }

    private boolean validateAnswers(String testType, boolean errorInFile, Map<String, String> statusMap, ArrayList<ImportAnswerModel> importAnswerModel) {
        String validationMessage;
        for (ImportAnswerModel importAnswer : importAnswerModel) //validating Answers
        {
            switch (testType) {
                case "Mcq Test":
                    validationMessage = importAnswer.validateForMCQAnswers();
                    break;
                case "Group Question Test":
                    validationMessage = importAnswer.validateForGroupQuestionAnswers();
                    break;
                default:
                    throw new NotFoundException("testType not found");
            }
            statusMap.put(importAnswer.getDescription(), validationMessage);
            if (!validationMessage.equalsIgnoreCase("ok")) {
                errorInFile = true;
            }
        }
        return errorInFile;
    }

    private boolean validateQuestions(boolean errorInFile, Map<String, String> statusMap, ArrayList<ImportQuestionModel> importQuestionModel) {
        String validationMessage;
        for (ImportQuestionModel importQuestion : importQuestionModel)//validating Questions
        {

            validationMessage = importQuestion.validate();
            statusMap.put(importQuestion.getDescription(), validationMessage);
            if (!validationMessage.equalsIgnoreCase("ok")) {
                errorInFile = true;
            }
        }
        return errorInFile;
    }

    private boolean vaidateCategory(boolean errorInFile, Map<String, String> statusMap, ArrayList<ImportCategoryModel> importCategoryModel) {
        String validationMessage;
        for (ImportCategoryModel importCategory : importCategoryModel)//validating Categories
        {
            validationMessage = importCategory.validate();
            statusMap.put(importCategory.getCategoryName(), validationMessage);
            if (!validationMessage.equalsIgnoreCase("ok")) {
                errorInFile = true;
            }
        }
        return errorInFile;
    }

    private boolean vaidateQuestionTags(boolean errorInFile, Map<String, String> statusMap, ArrayList<ImportQuestionTagModel> importQuestionTagModelList) {
        String validationMessage;
        for (ImportQuestionTagModel importQuestionTagModel : importQuestionTagModelList)//validating Categories
        {
            validationMessage = importQuestionTagModel.validate();
            statusMap.put(importQuestionTagModel.getTagName(), validationMessage);
            if (!validationMessage.equalsIgnoreCase("ok")) {
                errorInFile = true;
            }
        }
        return errorInFile;
    }

    public boolean isChecker() {
        boolean isChecker = false;
        for (Role role : getUser().getRoles()) {
            if ("checker".equals(role.getName().toLowerCase())) {
                isChecker = true;
                break;
            }
        }
        return isChecker;
    }

    public List<TestParent> availablePublishTests() {
        List<Test> remainTest = getUserAvailableTestEitherFreeAndPaid(null);
        Set<Test> constructList = new LinkedHashSet<>();
        for (Test t : remainTest) {
            Test test = t;
            while (test.getParent() != null) {
                test = test.getParent();
            }
            if (test.isLeafNode() || test.isConstruct()) {
                constructList.add(test);
            }
        }
        Set<Test> remainTestAfterGradeFilter = new HashSet<>(remainTest.size());
        remainTest.forEach(avlTest -> {
            TestPerTenant testPerTenant = testPerTenantRepository.findOneByTestIdAndDomainId(avlTest.getId(), getTenant().getId());
            if (StringUtils.isEmpty(testPerTenant)) {
                if (avlTest.getGrades() != null && Arrays.asList(avlTest.getGrades()).contains(userRepository.findOne(getUser().getId()).getGrade())) {
                    remainTestAfterGradeFilter.add(avlTest);
                }
            } else if (testPerTenant.isPublished()) {
                if (avlTest.getGrades() != null && Arrays.asList(avlTest.getGrades()).contains(userRepository.findOne(getUser().getId()).getGrade())) {
                    remainTestAfterGradeFilter.add(avlTest);
                }
            }

        });
        //todo need to add interceptor for all test to control published and unpublished to any specific tenant
        List<TestParent> finalList = new ArrayList<>();
        List<TestParent> testParents = getTestHierarchyForUserView(new ArrayList<>(constructList), new ArrayList<>(remainTestAfterGradeFilter));
        testParents.forEach(testParent -> {
            boolean flag = false;
            for (TestSubParent subParent : testParent.getSubParents()) {
                if (!subParent.getTestChild().isEmpty()) {
                    flag = true;
                    break;
                }
            }
            if (flag) {
                finalList.add(testParent);
            }
        });
        return finalList;
    }

    public List<Test> getUserAvailableTestEitherFreeAndPaid(Long parentId) {
        User user = userRepository.findByUsernameAndTenant(getUser().getUsername(), getTenant());
        Set<Test> userAvailableTest = new HashSet<>();
        boolean isChecker = isChecker();
        if (isChecker && parentId == null) {
            return testRepository.findAllByLatestVersionIsTrueAndParentIdIsNotNullAndGoLiveIsFalseAndPublishedIsTrueAndTenantAndLanguage(getTenant(), getFreshUser().getLanguage());
        } else if (isChecker) {
            List<Test> testList = testRepository.findAllByLatestVersionIsTrueAndParentIdAndGoLiveIsFalseAndPublishedIsTrueAndTenantAndLanguage(parentId, getTenant(), getFreshUser().getLanguage());
            List<TestResult> testResults = resultRepository.findAllByUserIdAndSubConstructNameAndTenant(getUser().getId(), testRepository.findOne(parentId).getName(), getTenant());
            if (testResults.isEmpty()) {
                testResults = resultRepository.findAllByUserIdAndTenantAndSubConstructNameIsNotNull(getUser().getId(), getTenant());
                testResults.forEach(testResult -> {
                    testResult.setSubConstructName(null);
                    resultRepository.save(testResult);
                });
            } else if (testResults.size() >= testList.size()) {
                testResults = resultRepository.findAllByUserIdAndTenantAndSubConstructNameIsNotNull(getUser().getId(), getTenant());
                testResults.forEach(testResult -> {
                    testResult.setSubConstructName(null);
                    resultRepository.save(testResult);
                });
                testList.clear();
            } else {
                testResults.forEach(testResult -> {
                    testList.remove(testResult.getTest());
                });
            }
            return testList;
        }
        Pilot pilot = pilotService.findOneByActive(true);
        if (StringUtils.isEmpty(pilot)) {
            throw new ValidationException("Current Pilot Exception");
        }
        UserTest userTest = userTestRepository.findByUserEmailAndTenant(getUser(), getTenant());
        if (userTest != null) {
            userTest.getTests().forEach(uts -> {
                TestPerTenant testPerTenant = testPerTenantRepository.findOneByTestIdAndDomainId(uts.getId(), getTenant().getId());
                if (!user.getFlag().equals("Bulk")) {
                    if (!StringUtils.isEmpty(testPerTenant)) {
                        userAvailableTest.add(uts);
                    } else if (uts.isPublished()) {
                        userAvailableTest.add(uts);
                    }
                } else {

                    user.setLanguage(userTest.getLanguage());
                    // user.setLanguage("English");
                    userRepository.save(user);
                    //   userAvailableTest.addAll(userTest.getTests());
                    userAvailableTest.addAll(testRepository.findAllByPublishedIsTrueAndPaidIsTrueAndGoLiveIsTrueAndLatestVersionIsTrueAndTenantAndLanguage(getTenant(), getFreshUser().getLanguage()));
                }


            });
        } else {
            if (user.getFlag() == null) {
                //This is starting code
                //When, product will assign to user then only assign test will available to user
                userAvailableTest.addAll(testRepository.findAllByPublishedIsTrueAndPaidIsTrueAndGoLiveIsTrueAndLatestVersionIsTrueAndTenantAndLanguage(getTenant(), getFreshUser().getLanguage()));
            } else if (user.getFlag().equals("Bulk")) {

                userAvailableTest.addAll(testRepository.findAllByPublishedIsTrueAndPaidIsFalseAndGoLiveIsTrueAndLatestVersionIsTrueAndTenantAndLanguage(getTenant(), getFreshUser().getLanguage()));
                //user.setLanguage("English");
                //	userRepository.save(user);

            } else if (user.getFlag().equals("New")) {
                Boolean b = user.getIsPaid();
                System.out.println(b);
                if (b) { //For paid user
                    userAvailableTest.addAll(testRepository.findAllByPublishedIsTrueAndPaidIsTrueAndGoLiveIsTrueAndLatestVersionIsTrueAndTenantAndLanguage(getTenant(), getFreshUser().getLanguage()));
                } else { //for unpaid user
                    userAvailableTest.addAll(testRepository.findAllByPublishedIsTrueAndPaidIsFalseAndGoLiveIsTrueAndLatestVersionIsTrueAndTenantAndLanguage(getTenant(), getFreshUser().getLanguage()));
                }
            }
        }

        List<TestResult> testResult = resultRepository.findAllByUserIdAndPilotIdAndTenant(getUser().getId(), pilot.getId(), getTenant());
        testResult.forEach(tr -> {
//            if (tr.getTest().getVersionId() == null) {
//                userAvailableTest.removeAll(testRepository.findAllByVersionId(tr.getTest().getId()));
            userAvailableTest.removeAll(testRepository.findAllByNameAndTenant(tr.getTest().getName(), getTenant()));

//            }
//            userAvailableTest.remove(tr.getTest());
        });
        if (parentId != null) {
            TreeSet<Test> testSet = new TreeSet<>(new Comparator() {
                @Override
                public int compare(Object o1, Object o2) {
                    Test t1 = (Test) o1;
                    Test t2 = (Test) o2;
                    Integer v1 = t1.getSequence(), v2 = t2.getSequence();
                    return v1.compareTo(v2);
                }
            });
            List<Test> testByParent = new ArrayList<>();
            userAvailableTest.forEach(remTest -> {
                if (parentId.equals(remTest.getParent() != null ? remTest.getParent().getId() : null)) {
                    testSet.add(remTest);
                }
            });
            if (!testSet.isEmpty()) {
                testByParent.add(testSet.first());
            }
            return testByParent;
        }
        return new ArrayList<>(userAvailableTest);
    }

    public List<TestParent> getTestHierarchyForUserView(List<Test> construct, List<Test> test) {
        boolean isChecker = isChecker();
        List<Test> parent = construct;
        List<TestParent> level1 = new ArrayList<>();
        List<TestSubParent> level2 = new ArrayList<>();
        List<TestChild> level3 = new ArrayList<>();
        List<TestLeaf> level4 = new ArrayList<>();
        User user = getFreshUser();

        for (Test parentTest : parent) {
            TestParent childLevel1 = new TestParent();
            level2 = new ArrayList<>();
            if (parentTest.isLatestVersion() && test.contains(parentTest)) {
                childLevel1 = childLevel1.getResponse(parentTest, childLevel1);
            } else if (parentTest.isConstruct() || parentTest.isLeafNode()) {
                childLevel1 = childLevel1.getResponse(parentTest, childLevel1);
                List<Test> subParent = testRepository.findAllByParentIdAndLanguage(parentTest.getId(), user.getLanguage());
                for (Test subTest : subParent) {
                    TestSubParent childLevel2 = new TestSubParent();
                    level3 = new ArrayList<>();
                    if (subTest.isLatestVersion() && test.contains(subTest)) {
                        childLevel2 = childLevel2.getResponse(subTest, childLevel2);
                    } else if (subTest.isConstruct() || subTest.isLeafNode()) {
                        childLevel2 = childLevel2.getResponse(subTest, childLevel2);
                        List<Test> subParentChild = testRepository.findAllByParentIdAndLanguage(subTest.getId(), user.getLanguage());
                        for (Test child : subParentChild) {
                            TestChild childLevel3 = new TestChild();
                            level4 = new ArrayList<>();
                            if (child.isLatestVersion() && test.contains(child)) {
                                childLevel3 = childLevel3.getResponse(child, childLevel3);
                            } else if (child.isConstruct() || child.isLeafNode()) {
                                childLevel3 = childLevel3.getResponse(child, childLevel3);
                                List<Test> childTest = testRepository.findAllByParentIdAndLanguage(child.getId(), user.getLanguage());
                                for (Test leafNode : childTest) {
                                    TestLeaf childLevel4 = new TestLeaf();
                                    if (leafNode.isLatestVersion() && test.contains(leafNode)) {
                                        childLevel4 = childLevel4.getResponse(leafNode, childLevel4);
                                        level4.add(childLevel4);
                                    }
                                }
                            }
                            childLevel3.setTestLeafs(level4);
                            level3.add(childLevel3);
                        }
                    }
                    List<TestChild> afterRemoveBlank = new ArrayList<>();
                    level3.forEach(lev -> {
                        if (lev.getId() != null) {
                            afterRemoveBlank.add(lev);
                        }
                    });
                    childLevel2.setTestChild(afterRemoveBlank);
                    childLevel2.setStart(true);
                    childLevel2.setChecker(isChecker);
                    if (childLevel2.getTestChild().size() < testRepository.countByLatestVersionIsTrueAndParentIdAndLanguage(childLevel2.getId(), user.getLanguage())) {
                        childLevel2.setStart(false);
                    }
                    level2.add(childLevel2);
                }
            }
            childLevel1.setSubParents(level2);
            level1.add(childLevel1);
        }
        return level1;
    }

    public List<Test> availablePublishTestsForAdmin() {
        return testRepository.findAllByPublishedIsTrueAndTenant(getTenant());
    }

    public boolean userTestAssignment(UserTestRequest request) {
        Pilot pilot = pilotService.findOneByActive(true);
        if (StringUtils.isEmpty(pilot)) {
            throw new ValidationException("Current Pilot Exception");
        }
        List<Long> userIds = Arrays.asList(request.getUserIds());
        int userCount = userIds.size();
        TenantTestAllotment testAllotment = testAllotmentRepository.findByUserEmailAndTenant(getUser().getEmail(), getTenant());
        if (testAllotment == null)
            return false;
        if (userCount > testAllotment.getAvailableTestCount()) {
            int extraCount = userCount - testAllotment.getAvailableTestCount();
            throw new ValidationException("Exceed Limit by Allowed test count " + extraCount);
        }
        List<Test> test = new ArrayList<>();
        testAllotment.getTests().forEach(ts -> {
            test.add(ts);
        });
        List<UserTest> assignedUserTestsList = new ArrayList<>();
        List<UserTest> unAssignedUserTestsList = new ArrayList<>();
        List<UserTest> allUserTestList = new ArrayList<>();
        for (Long id : userIds) {
            UserTest userTest = userTestRepository.findByUserEmailAndTenant(userRepository.findOne(id), getTenant());
            if (userTest != null && userTest.getTestAssigner() != null && userTest.getTestAssigner().equals(getUser()) && userTest.isUsed()) {
                assignedUserTestsList.add(userTest);
            } else if (userTest != null) {
                unAssignedUserTestsList.add(userTest);
            } else {
                userTest = new UserTest();
                userTest.setUserEmail(userRepository.findOne(id));
                userTest.setTestStatus(TestStatus.TNA.getStatus());
                userTest = userTestRepository.save(userTest);
                unAssignedUserTestsList.add(userTest);
            }
        }
        if (unAssignedUserTestsList.size() > testAllotment.getAvailableTestCount()) {
            int extraCount = unAssignedUserTestsList.size() - testAllotment.getAvailableTestCount();
            throw new ValidationException("Exceed Limit by Allowed test count " + extraCount);
        }
        allUserTestList.addAll(assignedUserTestsList);
        allUserTestList.addAll(unAssignedUserTestsList);
        for (UserTest userTest : allUserTestList) {
            List<Test> adminTest = new ArrayList<>();
            adminTest.addAll(test);
            List<Test> finalTest = new ArrayList<>();
            List<TestResult> testResult = resultRepository.findAllByUserIdAndPilotIdAndTenant(userTest.getUserEmail().getId(), pilot.getId(), getTenant());
            adminTest.removeAll(userTest.getTests());
            adminTest.addAll(userTest.getTests());//remove duplicate
            userTest.setTestStatus(TestStatus.NTA.getStatus());
            testResult.forEach(tr -> {
                if (tr.getTest().getVersionId() == null) {
                    adminTest.removeAll(testRepository.findAllByVersionIdAndTenant(tr.getTest().getId(), getTenant()));
                }
                adminTest.remove(tr.getTest());
            });
            adminTest.forEach(admTest -> {
                if (admTest.getGrades() != null && Arrays.asList(admTest.getGrades()).contains(userTest.getUserEmail().getGrade())) {
                    finalTest.add(admTest);
                }
            });
            userTest.setTests(finalTest);
            if (finalTest.isEmpty()) {
                userTest.setTestStatus(TestStatus.ATT.getStatus());
            }
            userTest.setUsed(true);
            userTest.setTestAssigner(getUser());
            userTestRepository.save(userTest);
        }
        int realCount = unAssignedUserTestsList.size();
        int remCount = (testAllotment.getAvailableTestCount() - realCount) < 0 ? 0 : (testAllotment.getAvailableTestCount() - realCount);
        int usedCount = (testAllotment.getUsedTestCount() + realCount);
        testAllotment.setAvailableTestCount(remCount);
        testAllotment.setUsedTestCount(usedCount);
        testAllotmentRepository.save(testAllotment);
        return true;
    }

    public void removeAllPreviousUserTestAssignmentLink() {
        userTestRepository.findAllByUsedIsTrueAndTestAssignerAndTenant(getUser(), getTenant()).forEach(userTest -> {
            userTest.setTestAssigner(null);
            userTest.setUsed(false);
            userTestRepository.save(userTest);
        });
    }

    @Transactional
    public void userTestAssignmentByExcelSheet(String username, String product, String language,  User currentUser) {
        Pilot pilot = pilotService.findOneByActive(true, currentUser.getTenant());
        if (StringUtils.isEmpty(pilot)) {
            throw new ValidationException("Current Pilot Exception");
        }
        List<Test> finalTest = new ArrayList<>();
        User user = userRepository.findOneByUsernameAndTenant(username, currentUser.getTenant());
        UserTest userTest = userTestRepository.findByUserEmailAndTenant(user, currentUser.getTenant());
        if (userTest == null) {
            userTest = new UserTest();
        }
        Product completeProduct = productRepository.findOneByNameAndTenant(product, currentUser.getTenant());
        List<Test> completeProductTests =   completeProduct.getTest();
        List<Test> remainingTest = new ArrayList<>(completeProductTests);
        List<TestResult> testResult = resultRepository.findAllByUserIdAndPilotIdAndTenant(user.getId(), pilot.getId(), currentUser.getTenant());
        for (TestResult tr : testResult) {
            if (tr.getTest().getVersionId() == null) {
                List<Test> allTestVersionsOfSameTest = testRepository.findAllByVersionIdAndTenant(tr.getTest().getId(), currentUser.getTenant());
                remainingTest.removeAll(allTestVersionsOfSameTest);
            }
            remainingTest.remove(tr.getTest());
        }
        String gradeFilter = StringUtils.isEmpty(user.getGrade()) ? "8" : user.getGrade();
        //filter testList by matching user grade and test grade
        remainingTest.forEach(admTest -> {
            if (admTest.getGrades() != null && Arrays.asList(admTest.getGrades()).contains(gradeFilter)) {
                finalTest.add(admTest);
            }
        });
        userTest.setTests(finalTest);
        userTest.setUserEmail(user);
        userTest.setTestStatus(TestStatus.NTA.getStatus());
        userTest.setUsed(true);
        if(user.getLanguage()==null) {
            user.setLanguage(language);

        }
        if(language==null)
        {
            userTest.setLanguage(user.getLanguage());
        }
        else{
            userTest.setLanguage(language);
            user.setLanguage(language);
        }

        userTest.setTestAssigner(getUser());
        if (finalTest.isEmpty()) {
            userTest.setTestStatus(TestStatus.ATT.getStatus());
        }
        userTest.setUsed(true);
        userTest.setTestAssigner(currentUser);
        userTest.setCreatedBy(currentUser);
        userTest.setTenant(currentUser.getTenant());

        userTestRepository.save(userTest);
        userRepository.save(user);
    }

    public boolean userTestDLinkAll(UserTestRequest request) {
        List<Long> userIds = Arrays.asList(request.getUserIds());
        for (Long id : userIds) {
            userTestAssignmentUpdate(id);
        }
        return true;
    }

    public boolean tenantTestAllotment(TenantTestRequest request) {
        List<Long> userIds = Arrays.asList(request.getUserIds());
        List<Long> productIds = new ArrayList<>(Arrays.asList(request.getTestIds()));
        List<Long> preIds = new ArrayList<>();
        List<Test> offerTest = new ArrayList<>();
        List<ProductTenant> productTenants = productTenantRepository.findAllByDomainAndProductIdIn(getTenant(), productIds);
        productTenants.forEach(productTenant -> {
            preIds.add(productTenant.getProduct().getId());
            offerTest.addAll(productTenant.getProduct().getTest());
        });
        productIds.removeAll(preIds);
        Set<Test> tests = new LinkedHashSet<>();
        List<Long> testIds = new LinkedList<>();
        List<Product> products = productRepository.findAllByIdIn(productIds);
        Pilot pilot = pilotService.findOneByActive(true);
        if (StringUtils.isEmpty(pilot)) {
            throw new ValidationException("Current pilot is not found");
        }

        products.forEach(product -> {
            tests.addAll(product.getTest());
        });
        tests.forEach(test -> {
            testIds.add(test.getId());
        });
        List<Test> testList = testRepository.findAllByPublishedIsTrueAndIdIn(testIds);
        if (!CollectionUtils.isEmpty(offerTest)) {
            testList.addAll(offerTest);
        }
        for (Long id : userIds) {
            User user = userRepository.findOne(id);
            /*userTestRepository.findAllByUsedIsTrueAndTestAssignerAndTenant(user,getTenant()).forEach(userTest -> {
                userTest.setTestAssigner(null);
                userTest.setUsed(false);
                userTestRepository.save(userTest);
            });*/
            TenantTestAllotment tenantTestAllotment = testAllotmentRepository.findByUserEmailAndTenant(user.getEmail(), getTenant());
            if (tenantTestAllotment == null) {
                tenantTestAllotment = new TenantTestAllotment();
            }
            tenantTestAllotment.setAvailableTestCount(request.getNumberOfAttempt());
            tenantTestAllotment.setUserEmail(userRepository.getOne(id).getEmail());
            tenantTestAllotment.setTests(testList);
            tenantTestAllotment.setProducts(products);
            tenantTestAllotment.setUsedTestCount(0);
            tenantTestAllotment.setTestCount(request.getNumberOfAttempt());
            testAllotmentRepository.save(tenantTestAllotment);
        }
        return true;
    }

    private List<ImportAnswerModel> reverse(List<ImportAnswerModel> list) {
        List<ImportAnswerModel> reverseAnswers = new ArrayList<>();
        List<Integer> marks = new ArrayList<>();
        for (ImportAnswerModel importAnswerModel : list) {
            reverseAnswers.add(new ImportAnswerModel(importAnswerModel));
            marks.add(importAnswerModel.getMarks());
        }
        int i = 0;
        int j = list.size() - 1;

        for (; j >= 0; j--) {
            reverseAnswers.get(i).setMarks(marks.get(j));
            i++;
        }
        return reverseAnswers;
    }

    public boolean userTestAssignmentUpdate(Long id, String[] userTestArray) {
        Pilot pilot = pilotService.findOneByActive(true);
        if (StringUtils.isEmpty(pilot))
            throw new ValidationException("Current Pilot Exception");
        //List<Test> testList = new ArrayList<>();
        List<Test> finalTest = new ArrayList<>();
        if (userTestArray == null || userTestArray.equals("undefined"))
            return false;
        User user = userRepository.findOne(id);
        UserTest userTest = userTestRepository.findByUserEmailAndTenant(user, getTenant());
        if (userTest == null)
            return false;
        if (!(userTest.isUsed() && userTest.getTestAssigner().equals(getUser())))
            throw new ValidationException("can't update User out of scope");
        List<String> testNames = Arrays.asList(userTestArray);
        List<Test> remainingTest = testRepository.findAllByLatestVersionIsTrueAndPublishedIsTrueAndNameInAndAndTenant(testNames, getTenant());
        List<TestResult> testResult = resultRepository.findAllByUserIdAndPilotIdAndTenant(id, pilot.getId(), getTenant());
        testResult.forEach(tr -> {
            if (tr.getTest().getVersionId() == null) {
                remainingTest.removeAll(testRepository.findAllByVersionIdAndTenant(tr.getTest().getId(), getTenant()));
            }
            remainingTest.remove(tr.getTest());
        });
        //filter testList by matching user grade and test grade
        remainingTest.forEach(admTest -> {
            if (admTest.getGrades() != null && Arrays.asList(admTest.getGrades()).contains(user.getGrade())) {
                finalTest.add(admTest);
            }
        });
        userTest.setTests(finalTest);
        if (finalTest.size() == 0) {
            userTest.setTestStatus(TestStatus.TNA.getStatus());
        }
        userTestRepository.save(userTest);
        return true;
    }

    public boolean userTestAssignmentUpdate(Long id) {
        List<Test> testList = new ArrayList<>();
        User user = userRepository.findOne(id);
        UserTest userTest = userTestRepository.findByUserEmailAndTenant(user, getTenant());
        if (userTest == null) {
            return false;
        }
        if (!(userTest.isUsed() && userTest.getTestAssigner().equals(getUser())))
            throw new ValidationException("can't update User out of scope");
        userTest.setTests(testList);
        userTest.setTestStatus(TestStatus.TNA.getStatus());
        userTestRepository.save(userTest);
        return true;
    }

    public List<TestQuestion> updateTestQuestionsWithCategory(List<TestQuestionRequest> request) {
        TestQuestion testQuestion;
        List<TestQuestion> testQuestions = new ArrayList<>(request.size());
        for (TestQuestionRequest testQuestionRequest : request) {
            testQuestion = testQuestionRepository.findByTestIdAndQuestionIdAndTenant(testQuestionRequest.getTest(), testQuestionRequest.getQuestion(), getTenant());
            if (testQuestion == null) {
                throw new NotFoundException("Question not found");
            }
            testQuestion.getCategory().clear();
            testQuestion.setCategory(testQuestionRequest.getCategory());
            testQuestion.setSubCategory(testQuestionRequest.getSubCategory());
            testQuestions.add(testQuestionRepository.save(testQuestion));
        }
        return testQuestions;
    }

    public List<TenantTestResponse> getListAdminTest(String roleName) {
        List<User> users = new ArrayList<>();
        List<TenantTestResponse> tenantTestResponses = new ArrayList<>();
        if (!StringUtils.isEmpty(roleName)) {
            users = userRepository.findAllByRolesAndTenant(userService.getOneRoleByName(roleName), getTenant());
        } else {
            users = userRepository.findAll();
        }
        for (User user : users) {
            TenantTestResponse tenantTestResponse = new TenantTestResponse(testAllotmentRepository.findByUserEmailAndTenant(user.getEmail(), getTenant()));
            tenantTestResponse.setUserId(user.getId());
            tenantTestResponse.setUserName(user.getUsername());
            tenantTestResponse.setEmail(user.getEmail());
            tenantTestResponses.add(tenantTestResponse);
        }
        return tenantTestResponses;
    }

    public List<UserTestResponse> getListUserTest(String schoolName, String grade, String gender, String status, String type) {
        List<User> users = new ArrayList<>();
        List<UserTestResponse> testResponses = new ArrayList<>();
        if (!StringUtils.isEmpty(schoolName) && !schoolName.equals("undefined") && !StringUtils.isEmpty(grade) && !grade
                .equals("undefined") && !StringUtils.isEmpty(gender) && !gender.equals("undefined")) {
            users = userRepository.findAllBySchoolNameAndGradeAndGenderAndTenant(schoolName, grade, gender, getTenant());
        } else if (!StringUtils.isEmpty(schoolName) && !schoolName.equals("undefined") && !StringUtils.isEmpty(grade) && !grade
                .equals("undefined")) {
            users = userRepository.findAllBySchoolNameAndGradeAndTenant(schoolName, grade, getTenant());
        } else if (!StringUtils.isEmpty(schoolName) && !schoolName.equals("undefined") && !StringUtils.isEmpty(gender) && !gender.equals("undefined")) {
            users = userRepository.findAllBySchoolNameAndGenderAndTenant(schoolName, gender, getTenant());
        } else if (!StringUtils.isEmpty(grade) && !grade
                .equals("undefined") && !StringUtils.isEmpty(gender) && !gender.equals("undefined")) {
            users = userRepository.findAllByGradeAndGenderAndTenant(grade, gender, getTenant());
        } else if (!StringUtils.isEmpty(schoolName) && !schoolName.equals("undefined")) {
            users = userRepository.findAllBySchoolNameAndTenant(schoolName, getTenant());
        } else if (!StringUtils.isEmpty(grade) && !grade.equals("undefined")) {
            users = userRepository.findAllByGradeAndTenant(grade, getTenant());
        } else if (!StringUtils.isEmpty(gender) && !gender.equals("undefined")) {
            users = userRepository.findAllByGenderAndTenant(gender, getTenant());
        } else {
            users = userRepository.findAllByTenant(getTenant());
        }
        for (User user : users) {
            UserTestResponse userTestResponse = new UserTestResponse(userTestRepository.findByUserEmailAndTenant(user, getTenant()));
            userTestResponse.setUserId(user.getId());
            userTestResponse.setUserName(user.getUsername());
            if (StringUtils.isEmpty(status) && StringUtils.isEmpty(type)) {
                testResponses.add(userTestResponse);
            } else if (StringUtils.isEmpty(status) && "assignedUser".equals(type) && userTestResponse.isUsed()) {
                testResponses.add(userTestResponse);
            } else if (StringUtils.isEmpty(status) && "unAssignedUser".equals(type) && !userTestResponse.isUsed()) {
                testResponses.add(userTestResponse);
            } else if (StringUtils.isEmpty(type) && userTestResponse.getTestStatus().equals(status)) {
                testResponses.add(userTestResponse);
            } else if (userTestResponse.getTestStatus().equals(status)) {
                testResponses.add(userTestResponse);
            } else if ("assignedUser".equals(type) && userTestResponse.isUsed()) {
                testResponses.add(userTestResponse);
            } else if ("unAssignedUser".equals(type) && !userTestResponse.isUsed()) {
                testResponses.add(userTestResponse);
            }
        }
        return testResponses;
    }

    public List<UserTestResponse> getListUserAllotedTest(Long roleId, String schoolName, String grade) {
        List<User> users = new ArrayList<>();
        List<UserTestResponse> testResponses = new ArrayList<>();
        if (!StringUtils.isEmpty(roleId) && roleId != 0 && !StringUtils.isEmpty(schoolName) && !schoolName.equals("undefined") && !StringUtils.isEmpty(grade) && !grade
                .equals("undefined")) {
            users = userRepository.findAllByRolesIdAndSchoolNameAndGradeAndTenant(roleId, schoolName, grade, getTenant());
        } else if (!StringUtils.isEmpty(roleId) && roleId != 0 && !StringUtils.isEmpty(schoolName) && !schoolName.equals("undefined")) {
            users = userRepository.findAllByRolesIdAndSchoolNameAndTenant(roleId, schoolName, getTenant());
        } else if (!StringUtils.isEmpty(roleId) && roleId != 0 && !StringUtils.isEmpty(grade) && !grade.equals("undefined")) {
            users = userRepository.findAllByRolesIdAndGradeAndTenant(roleId, grade, getTenant());
        } else if (!StringUtils.isEmpty(schoolName) && !schoolName.equals("undefined") && !StringUtils.isEmpty(grade) && !grade.equals("undefined")) {
            users = userRepository.findAllBySchoolNameAndGradeAndTenant(schoolName, grade, getTenant());
        } else if (!StringUtils.isEmpty(schoolName) && !schoolName.equals("undefined")) {
            users = userRepository.findAllBySchoolNameAndTenant(schoolName, getTenant());
        } else if (!StringUtils.isEmpty(grade) && !grade.equals("undefined")) {
            users = userRepository.findAllByGradeAndTenant(grade, getTenant());
        } else {
            users = userRepository.findAll();
        }
        for (User user : users) {
            UserTestResponse userTestResponse = new UserTestResponse(userTestRepository.findByUserEmailAndTenant(user, getTenant()));
            userTestResponse.setUserId(user.getId());
            userTestResponse.setUserName(user.getUsername());
            testResponses.add(userTestResponse);
        }
        return testResponses;
    }

    public boolean tenantTestUpdate(TenantTestRequest request) {
        List<Long> proIds = Arrays.asList(request.getTestIds());
        if (proIds.isEmpty()) {
            return false;
        }
        TenantTestAllotment allotment = testAllotmentRepository.findByUserEmailAndTenant(request.getEmail(), getTenant());
        if (allotment == null) {
            return false;
        }
        Set<Test> testList = new LinkedHashSet<>();
        List<Product> products = productRepository.findAllByTenantAndIdIn(getTenant(), proIds);
        products.forEach(product -> {
            testList.addAll(product.getTest());
        });
        allotment.setTests(new ArrayList<>(testList));
        allotment.setTestCount(request.getNumberOfAttempt());
        allotment.setAvailableTestCount(request.getNumberOfAttempt() - allotment.getUsedTestCount());
        allotment.setProducts(products);
        testAllotmentRepository.save(allotment);
        return true;
    }

    public List<TestDescriptionResponse> testSequencing(List<TestSequenceRequest> request) {
        HashSet<Integer> set = new HashSet<>();
        request.forEach(r -> {
            set.add(r.getSequence());
        });
        if (set.size() < request.size()) {
            throw new ValidationException("Duplicate sequence number not allowed.");
        }
        List<TestDescriptionResponse> responses = new ArrayList<>();
        for (TestSequenceRequest tsr : request) {
            Test test = testRepository.findOne(tsr.getId());
            test.setSequence(tsr.getSequence());
            test.setTestForce(tsr.isTestForce());
            responses.add(new TestDescriptionResponse(testRepository.save(test)));
        }
        return responses;
    }

    public List<ScoringResponse> listForScoring(Long categoryId, Long subCategoryId, Long testId) {
        return reportService.listForScoring(categoryId, subCategoryId, testId);
    }

    public List<Test> testHistory(Long id) {
        List<Test> testList = new LinkedList<>();
        Long versionId = testRepository.findOne(id).getVersionId();
        if (versionId == null) {
            testList.add(testRepository.findOne(id));
            return testList;
        }
        testList.add(testRepository.findOne(versionId));
        testList.addAll(testRepository.findAllByVersionIdAndTenant(versionId, getTenant()));
        return testList;
    }

    public boolean goLiveTest(Long id) {
        Test test = testRepository.findOne(id);
        if (test == null) {
            return false;
        }
        List<TestResult> testResults = resultRepository.findAllByTestAndTenant(test, getTenant());
        testResults.forEach(tr -> {
            List<UserAnswer> userAnswers = userAnswerRepository.findAllByTestResultAndTenant(tr, getTenant());
            userAnswerRepository.delete(userAnswers);
        });
        resultRepository.delete(testResults);
        test.setGoLive(true);
        testRepository.save(test);
        return true;
    }

    //Four level test tree
    public List<TestParent> getTreeTest(List<Test> tests) {
        List<Test> parent = null;
        if (tests != null) {
            parent = tests;
        } else {
            parent = testRepository.findAllByConstructIsTrueAndParentIdIsNullAndTenant(getTenant());
        }
        List<TestParent> level1 = new ArrayList<>();
        List<TestSubParent> level2 = new ArrayList<>();
        List<TestChild> level3 = new ArrayList<>();
        List<TestLeaf> level4 = new ArrayList<>();

        for (Test parentTest : parent) {
            TestParent childLevel1 = new TestParent();
            level2 = new ArrayList<>();
            childLevel1.setId(parentTest.getId());
            childLevel1.setName(parentTest.getName());
            List<Test> subParent = testRepository.findAllByParentIdAndTenant(parentTest.getId(), getTenant());
            for (Test subTest : subParent) {
                TestSubParent childLevel2 = new TestSubParent();
                level3 = new ArrayList<>();
                childLevel2.setId(subTest.getId());
                childLevel2.setName(subTest.getName());
                List<Test> subParentChild = testRepository.findAllByParentIdAndTenant(subTest.getId(), getTenant());
                for (Test child : subParentChild) {
                    TestChild childLevel3 = new TestChild();
                    level4 = new ArrayList<>();
                    childLevel3.setId(child.getId());
                    childLevel3.setName(child.getName());
                    List<Test> childTest = testRepository.findAllByParentIdAndTenant(child.getId(), getTenant());
                    for (Test leafNode : childTest) {
                        TestLeaf childLevel4 = new TestLeaf();
                        childLevel4.setId(leafNode.getId());
                        childLevel4.setName(leafNode.getName());
                        level4.add(childLevel4);
                    }
                    childLevel3.setTestLeafs(level4);
                    level3.add(childLevel3);
                }
                childLevel2.setTestChild(level3);
                level2.add(childLevel2);
            }
            childLevel1.setSubParents(level2);
            level1.add(childLevel1);
        }
        return level1;
    }

    public List<Test> findAllParentTestByTenant() {
        return testRepository.findAllByLatestVersionIsTrueAndParentIdIsNullAndTenant(getTenant());
    }

    public List<Test> findAllchildTestByParent(Long id) {
        return testRepository.findAllByLatestVersionIsTrueAndPaidIsTrueAndGoLiveIsTrueAndParentIdAndTenantOrConstructIsTrueAndParentIdAndTenant(id, getTenant(), id, getTenant());
    }

    public List<Test> findAllByIds(Long[] ids) {
        List<Long> testId = Arrays.asList(ids);
        return testRepository.findAllByIdInAndTenant(testId, getTenant());
    }

    public List<Test> findAllParentTestByTenantAndConstruct() {
        return testRepository.findAllByParentIdIsNullAndConstructIsTrueAndTenant(getTenant());
    }

    public List<Test> findAllchildTestByParentAndConstruct(Long id) {
        return testRepository.findAllByConstructIsTrueAndParentIdAndTenant(id, getTenant());
    }

    public List<Test> findAllConstructByTenant() {
        return testRepository.findAllByConstructIsTrueAndTenant(getTenant());
    }

    public Test save(TestConstructRequest request) {
        Test test = null;
        if (StringUtils.isEmpty(request.getId())) {
            test = request.toEntity();
        } else {
            test = request.toEntity(testRepository.findOne(request.getId()));
        }
        if (StringUtils.isEmpty(request.getParentId()) || request.getParentId() == 0) {
            test.setParent(null);
            testRepository.findAllByConstructIsTrueAndParentIdAndTenant(request.getId(), getTenant()).forEach(subConstruct -> {
                subConstruct.setGrades(request.getGrades());
                testRepository.save(subConstruct);
                testRepository.findAllByLatestVersionIsTrueAndParentIdAndTenant(subConstruct.getId(), getTenant()).forEach(itemTest -> {
                    itemTest.setGrades(request.getGrades());
                    testRepository.save(itemTest);
                });
            });
        } else {
            Test parent = testRepository.findOne(request.getParentId());
            test.setParent(parent);
            test.setGrades(parent.getGrades());
        }
        return testRepository.save(test);
    }

    public List<Test> findAllConstructByTenantAndLeafISTrue() {
        return testRepository.findAllByConstructIsTrueAndLeafNodeIsTrueAndTenant(getTenant());
    }

    public List<Test> findAllTestByConstructId(Long id) {
        return testRepository.findAllByLatestVersionIsTrueAndParentIdAndTenant(id, getTenant());
    }

    public List<Test> findAllTestForConstruct() {
        return testRepository.findAllByLatestVersionIsTrueAndParentIdIsNullAndPublishedIsTrueAndTenant(getTenant());
    }

    public Test updateConstructTest(ConstructTestRequest request) {
        Test test = testRepository.findOne(request.getId());
        if (!StringUtils.isEmpty(test)) {
            List<Test> tests = testRepository.findAllByLatestVersionIsTrueAndParentIdAndIdNotInAndTenant(request.getId(), request.getTests(), getTenant());
            request.getTests().forEach(conTest -> {
                Test test1 = testRepository.findOne(conTest);
                test1.setParent(test);
                test1.setGrades(test.getGrades());
                testRepository.save(test1);
            });
            tests.forEach(childTest -> {
                childTest.setParent(null);
                childTest.setGrades(null);
                testRepository.save(childTest);
            });
            if (request.getTests().isEmpty()) {
                testRepository.findAllByLatestVersionIsTrueAndParentIdAndTenant(request.getId(), getTenant()).forEach(testChild -> {
                    testChild.setGrades(null);
                    testChild.setParent(null);
                    testRepository.save(testChild);
                });
            }
        }
        return test;
    }

    public void deleteConstruct(Long id) {
        testRepository.delete(testRepository.findOne(id));
    }

    public List<Test> getAllTestByProductId(Long productId) {
        List<Test> testList = new ArrayList<>();
        List<Test> subject = testRepository.findAllByConstructIsTrueAndParentId(productId);
        subject.forEach(testSubject -> {
            testList.addAll(testRepository.findAllByParentId(testSubject.getId()));
        });
        return testList;
    }

    public List<Test> getAllTestByCostructId(Long constructId) {
        List<Test> testList = new ArrayList<>();
        testList.addAll(testRepository.findAllByLatestVersionIsTrueAndParentId(constructId));
        return testList;
    }

    public List<Test> getAllConstructByProductId(Long productId) {
        return testRepository.findAllByConstructIsTrueAndParentId(productId);
    }

    public List<Test> getAllVersionTestByConstructId(Long constructId) {
        List<Test> testList = new ArrayList<>();
        testList.addAll(testRepository.findAllByParentId(constructId));
        return testList;
    }

    public List<Test> getAllProductsOfGrade() { // argument named with grade would be added here


     /*
                code for filtering all the products by grade would be written here
      */
        return testRepository.findAllByParentIdIsNullAndConstructIsTrueAndTenant(getTenant());
    }

    public Set<Test> findAllSubjectByGradeAndTenant(String grade) {
        Set<Test> testList = new HashSet<>();
        testRepository.findAllByConstructIsTrueAndParentIdIsNullAndTenant(getTenant()).forEach(test -> {
            if (test.getGrades() != null && Arrays.asList(test.getGrades()).contains(grade)) testList.add(test);
        });

        List<TestPerTenant> allByDomainId = testPerTenantRepository.findAllByDomainId(getTenant().getId());
        allByDomainId.forEach(testPerTenant -> {
            Test test = testRepository.findOne(testPerTenant.getTestId());
            if (test.getGrades() != null && Arrays.asList(test.getGrades()).contains(grade)) {
                if (test.getParent() != null) {
                    if (test.getParent() != null) {
                        testList.add(test.getParent().getParent());
                    }
                }
            }
            ;

        });
        return testList;
    }

    public List<Test> getAllTestByProduct(Long productId) {
        List<Test> testList = new ArrayList<>();
        List<Test> subject = testRepository.findAllByConstructIsTrueAndParentId(productId);
        subject.forEach(testSubject -> {
            testList.addAll(testRepository.findAllByLatestVersionIsTrueAndParentId(testSubject.getId()));
        });
        return testList;
    }

    public List<Test> findAllSubjectByGradeAndTeacher(String grade) {
        Set<Test> testSet = new HashSet<>();
        Teacher t = teacherRepository.findOneByUserIdAndTenant(getUser().getId(), getTenant());
//        if (t.isClassTeacher() && t.getGrade() != null && t.getGrade().equals(grade)) {
        if (!StringUtils.isEmpty(grade)) {
            List<Test> testList = testRepository.findAllByConstructIsTrueAndParentIdIsNullAndTenant(getTenant());
            //Add all offer test
            testList.addAll(getAllOfferTestParents());
            if (!testList.isEmpty()) {
                testList.forEach(test -> {
                    if (test.getGrades() != null && Arrays.asList(test.getGrades()).contains(grade)) {
                        testSet.add(test);
                    }
                });
            }
            return new ArrayList<>(testSet);
        } else {
            throw new ValidationException("Please choose grade");
        }
//        subjectTeacherRepository.findAllByTeacherIdAndTenant(t.getId(), getTenant())
//                .forEach(teacher -> {
//                    testSet.add(teacher.getSubject());
//                });
//        return new ArrayList<>(testSet);
    }

    private List<Test> getAllOfferTestParents() {
        List<Test> tests = new ArrayList<>();
        testPerTenantRepository.findAllByDomainId(getTenant().getId()).forEach(test -> {
            Test ptest = testRepository.getOne(test.getTestId()).getParent() != null ? testRepository.getOne(test.getTestId()).getParent().getParent() : null;
            if (ptest != null) {
                tests.add(ptest);
            }
        });
        return tests;
    }

    public boolean hasOnlyOneTest(Long constructId) {
        Pilot pilot = pilotService.findOneByActive(true);
        List<Test> userTestItem = new ArrayList<>();
        if (StringUtils.isEmpty(pilot)) {
            throw new ValidationException("Current Pilot Exception");
        }
        Set<Test> allTest = new HashSet<>();
        UserTest userTest = userTestRepository.findByUserEmailAndTenant(getUser(), getTenant());
        if (userTest != null) {
            userTestItem.addAll(userTest.getTests());
        }
        Test construct = testRepository.findOne(constructId);
        if (construct.getParent() == null && !construct.isLeafNode()) {
            for (Test product : testRepository.findAllByConstructIsTrueAndParentIdAndTenant(constructId, getTenant())) {
                userTestItem.forEach(test -> {
                    if (test.getParent().getId() == product.getId()) {
                        allTest.add(test);
                    }
                });
                allTest.addAll(testRepository.findAllByLatestVersionIsTrueAndParentIdAndPaidIsFalseAndPublishedIsTrueAndTenant(product.getId(), getTenant()));
            }
        } else if (construct.getParent() == null && construct.isLeafNode()) {
            allTest.addAll(testRepository.findAllByLatestVersionIsTrueAndParentIdAndPaidIsFalseAndPublishedIsTrueAndTenant(constructId, getTenant()));
            userTestItem.forEach(test -> {
                if (test.getParent().getId() == constructId) {
                    allTest.add(test);
                }
            });
        }
        List<TestResult> testResult = resultRepository.findAllByUserIdAndPilotIdAndTenant(getUser().getId(), pilot.getId(), getTenant());
        testResult.forEach(tr -> {
            if (tr.getTest().getVersionId() == null) {
                allTest.removeAll(testRepository.findAllByVersionIdAndTenant(tr.getTest().getId(), getTenant()));
            }
            allTest.remove(tr.getTest());
        });
        if (allTest.isEmpty())
            return true;
        return false;
    }

    public boolean hasOnlyOneTestOnUser() {
        List<Test> remainTest = getUserAvailableTestEitherFreeAndPaid(null);
        Set<Test> remainTestAfterGradeFilter = new HashSet<>(remainTest.size());
        remainTest.forEach(avlTest -> {
            if (avlTest.getGrades() != null && Arrays.asList(avlTest.getGrades()).contains(userRepository.findOne(getUser().getId()).getGrade())) {
                remainTestAfterGradeFilter.add(avlTest);
            }
        });
        if (remainTestAfterGradeFilter.isEmpty()) {
            //    communicationService.demoCompleteMail(userRepository.findByName(userRepository.findByEmailAndTenant(getUser().getEmail(),getTenant()).getName()));
            return true;
        }
        return false;
    }

    private boolean balancedParenthensies(String s) {
        Stack<Character> stack = new Stack<Character>();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '[' || c == '(' || c == '{') {
                stack.push(c);
            } else if (c == ']') {
                if (stack.isEmpty() || stack.pop() != '[') {
                    return false;
                }
            } else if (c == ')') {
                if (stack.isEmpty() || stack.pop() != '(') {
                    return false;
                }
            } else if (c == '}') {
                if (stack.isEmpty() || stack.pop() != '{') {
                    return false;
                }
            }

        }
        return stack.isEmpty();
    }

    public void saveTestScoringFormula(Long testId, String formula) {
        Test test = testRepository.findOne(testId);
        if (balancedParenthensies(formula)) {
            test.setScoringFormula(formula);
            testRepository.saveAndFlush(test);
        } else {
            throw new ValidationException("enter correct formula and check parenthesis");
        }
    }

    public boolean isUserThreeSixtyEligible(User user) {
        user = getFreshUser(user.getId());

        //1. check user finished all tests or not
        UserTest userTest = userTestRepository.findByUserEmailAndTenant(user, getTenant());
        if (userTest == null) {
            return false;
        }
        String status = userTest.getTestStatus();
        if (!(status.equals(TestStatus.ATT.getStatus())
                || status.equals(TestStatus.TCS.getStatus()))) {
            return false;
        }

        //2. atleast one threesixty enabled test in all taken tests
        for (TestResult testResult : user.getTests()) {
            if (isThreeSixty(testResult.getTest())) {
                return true;
            }
        }
        return false;
    }

    public boolean isUserThreeSixtyEligible() {
        return isUserThreeSixtyEligible(getUser());
    }

    public Map<String, String> importTestQuestions(MultipartFile file, String dryRun) {

        boolean errorInFile = false;
        String validationMessage;
        Map<String, String> statusMap = new HashMap<>();
        statusMap.put("error", "false");
        Map<String, ArrayList> testQuestionRecords = excelImportService.importTestQuestionsRecords(file);
        ArrayList<ImportTestQuestionModel> importTestQuestionModels = testQuestionRecords.get("testQuestions");
        Test test = null;
        Question question = null;
        Category category = null;
        Category subCategory = null;
        List<String> categoryNames = new ArrayList<>();
        List<String> subCategoryNames = new ArrayList<>();


        for (ImportTestQuestionModel importTestQuestionModel : importTestQuestionModels) {
            validationMessage = importTestQuestionModel.validate();
            if (!validationMessage.equalsIgnoreCase("ok")) {
                statusMap.put(importTestQuestionModel.getTestName().trim(), validationMessage);
                errorInFile = true;
            }

//      finding test in db if it exist or not
            test = testRepository.findByNameAndVersionAndTenant(importTestQuestionModel.getTestName(), importTestQuestionModel.getTestVersion(), getTenant());


            if (test == null) {
                statusMap.put(importTestQuestionModel.getTestName(), "Test not found in database, create first.");
                errorInFile = true;
            }


//      finding question in db if it exist or not
            question = questionsRepository.findByIdAndTenant(importTestQuestionModel.getQuestionId(), getTenant());
            if (question == null) {
                statusMap.put(importTestQuestionModel.getQuestionId().toString(), "Question not found in database, create first.");
                errorInFile = true;
            }


            if (importTestQuestionModel.getCategory() != null) {
                categoryNames.clear();
                Collections.addAll(categoryNames, StringUtils.isEmpty(importTestQuestionModel.getCategory()) ? new String[0] : importTestQuestionModel.getCategory().split(","));

                for (String categoryName : categoryNames) {
                    try {
                        category = categoryRepository.findByNameAndTenant(categoryName, getTenant());
                    } catch (Exception e) {
                        System.out.println("something happend wrong with " + categoryName);
                        statusMap.put(categoryName, "something happend wrong with " + categoryName);
                    }
                    if (category == null) {
                        statusMap.put(categoryName, "Category not found in database, create first.");
                        errorInFile = true;
                    }
                }

            }


            if (importTestQuestionModel.getSubCategory() != null) {
                subCategoryNames.clear();
                Collections.addAll(subCategoryNames, StringUtils.isEmpty(importTestQuestionModel.getSubCategory()) ? new String[0] : importTestQuestionModel.getSubCategory().split(","));

                for (String subCategoryName : subCategoryNames) {
                    try {
                        category = categoryRepository.findByNameAndTenant(subCategoryName, getTenant());
                    } catch (Exception e) {
                        System.out.println("something happend wrong with " + subCategoryName);
                        statusMap.put(subCategoryName, "something happend wrong with " + subCategoryName);
                    }
                    if (category == null) {
                        statusMap.put(subCategoryName, "SubCategory not found in database, create first.");
                        errorInFile = true;
                    }
                }
            }
        }

        if (!errorInFile && dryRun.equalsIgnoreCase("false")) {

            for (ImportTestQuestionModel importTestQuestionModel : importTestQuestionModels) {
                test = testRepository.findByNameAndVersionAndTenant(importTestQuestionModel.getTestName(), importTestQuestionModel.getTestVersion(), getTenant());
                test.getQuestions().clear();
                testRepository.saveAndFlush(test);
            }

            for (ImportTestQuestionModel importTestQuestionModel : importTestQuestionModels) {

                test = testRepository.findByNameAndVersionAndTenant(importTestQuestionModel.getTestName(), importTestQuestionModel.getTestVersion(), getTenant());

                TestQuestion testQuestion = convertImportTestQuestionModeltoTestQuestion(importTestQuestionModel, test);
                testQuestion = testQuestionRepository.saveAndFlush(testQuestion);
                test.addQuestions(testQuestion);
                testRepository.saveAndFlush(test);
            }
        }

        if (errorInFile) {
            statusMap.put("error", "true");
        }
        return statusMap;
    }

    private TestQuestion convertImportTestQuestionModeltoTestQuestion(ImportTestQuestionModel importTestQuestionModel, Test test) {
        List<String> categoryNames = new ArrayList<>();
        List<String> subCategoryNames = new ArrayList<>();
        List<Category> categories = new ArrayList<>();
        List<Category> subCategories = new ArrayList<>();


        Question question = questionsRepository.findByIdAndTenant(importTestQuestionModel.getQuestionId(), getTenant());

        if (importTestQuestionModel.getCategory() != null) {
            categoryNames.clear();
            categories.clear();
            Collections.addAll(categoryNames, StringUtils.isEmpty(importTestQuestionModel.getCategory()) ? new String[0] : importTestQuestionModel.getCategory().split(","));

            for (String categoryName : categoryNames) {
                categories.add(categoryRepository.findByNameAndTenant(categoryName, getTenant()));
            }
        }

        if (importTestQuestionModel.getSubCategory() != null) {
            subCategoryNames.clear();
            subCategories.clear();
            Collections.addAll(subCategoryNames, StringUtils.isEmpty(importTestQuestionModel.getSubCategory()) ? new String[0] : importTestQuestionModel.getSubCategory().split(","));

            for (String subCategoryName : subCategoryNames) {
                subCategories.add(categoryRepository.findByNameAndTenant(subCategoryName, getTenant()));
            }
        }
        return new TestQuestion(test, question, categories, subCategories);

    }

    @Transactional
    public void tenantProductAllotment(TenantProductRequest request) {
        List<Long> tenantIds = Arrays.asList(request.getTenantIds());
        List<Long> productIds = Arrays.asList(request.getProductIds());
        for (Long tenantId : tenantIds) {
            for (Long productId : productIds) {
                ProductTenant productTenant = productTenantRepository.findOneByDomainIdAndProductId(tenantId, productId);
                if (StringUtils.isEmpty(productTenant)) {
                    productTenant = new ProductTenant();
                    Product product = productRepository.findOne(productId);
                    product.getTest().forEach(test -> {
                        TestPerTenant testPerTenant = new TestPerTenant();
                        testPerTenant.setTestId(test.getId());
                        testPerTenant.setDomainId(tenantId);
                        testPerTenant.setDescription(test.getDescription());
                        testPerTenant.setPublished(false);
                        testPerTenant.setName(test.getName());
                        testPerTenant.setType("Offered By " + getTenant().getName());
                        testPerTenantRepository.save(testPerTenant);
                    });
                    productTenant.setDomain(tenantRepository.findOne(tenantId));
                    productTenant.setProduct(product);
                    productTenantRepository.save(productTenant);
                }
            }
        }
    }

    @Transactional
    public void removeTenantProductAllotment(Long id, Long tenantId) {
        ProductTenant productTenant = productTenantRepository.findOne(id);
        productTenant.getProduct().getTest().forEach(test -> {
            testPerTenantRepository.delete(testPerTenantRepository.findOneByTestIdAndDomainId(test.getId(), tenantId));
        });
        productTenantRepository.delete(id);
    }

    public List<TestPerTenantResponse> getAllOfferTest() {
        return testPerTenantRepository.findAllByDomainId(getTenant().getId()).stream().map(TestPerTenantResponse::new).collect(Collectors.toList());
    }

    public void updateOfferTest(Long id, boolean published) {
        TestPerTenant testPerTenant = testPerTenantRepository.findOne(id);
        if (testPerTenant.isPublished()) {
            testPerTenant.setPublished(false);
        } else {
            if (testRepository.getOne(testPerTenant.getTestId()).isGoLive()) {
                testPerTenant.setPublished(true);
            } else {
                throw new ValidationException("You can't publish this test,because it's have Beta version so pls contact to your Offer Test referral");
            }
        }
        testPerTenantRepository.save(testPerTenant);
    }

    public List<TenantProductResponse> getProductAllotment() {
        return productTenantRepository.findAllByTenant(getTenant()).stream().map(TenantProductResponse::new).collect(Collectors.toList());
    }
}
