package com.synlabs.intscale.service;

import com.querydsl.jpa.impl.JPAQuery;
import com.synlabs.intscale.entity.Report.QUserReportStatus;
import com.synlabs.intscale.entity.Report.UserReportStatus;
import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.test.TestResult;
import com.synlabs.intscale.entity.test.UserTest;
import com.synlabs.intscale.entity.user.Dashboard;
import com.synlabs.intscale.entity.user.QUser;
import com.synlabs.intscale.entity.user.Role;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.enums.DashboardType;
import com.synlabs.intscale.enums.ReportStatus;
import com.synlabs.intscale.jpa.*;
import com.synlabs.intscale.util.DateUtil;
import com.synlabs.intscale.view.DashboardRequest;
import com.synlabs.intscale.view.DashboardResponse;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DurationFieldType;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.synlabs.intscale.util.DateUtil.DayTime.END_OF_DAY;
import static com.synlabs.intscale.util.DateUtil.DayTime.START_OF_DAY;

@Service
public class DashboardService extends BaseService
{
  @PersistenceContext
  private EntityManager entityManager;

  @Autowired
  private TestResultRepository testResultRepository;

  @Autowired
  private DashboardRepository dashboardRepository;

  @Autowired
  private UserTestRepository userTestRepository;

  @Autowired
  private ProductRepository productRepository;

  @Autowired
  private UserReportStatusRepository userReportStatusRepository;

  private static Logger logger = LoggerFactory.getLogger(DashboardService.class);

  /*public List<DashboardResponse> getTestTakers()
  {
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
    List<TestResult> testResultList = testResultRepository.findAll();
    List<DashboardResponse> responses = new ArrayList<>(testResultList.size());
    testResultList.forEach(testResult -> {
      DashboardResponse response = new DashboardResponse();

    });
    return responses;
  }*/

  public List<Dashboard> getAllDashboards()
  {
    return dashboardRepository.findAll();
  }

  public DashboardResponse getDashboards(DashboardRequest request)
  {
    Set<String> dashboards = new HashSet<>();
    DashboardResponse response = new DashboardResponse();
    for (Role role : getUser().getRoles())
    {
      Collections.addAll(dashboards, StringUtils.isEmpty(role.getDashboard()) ? new String[0] : role.getDashboard().split(","));
    }
    for (String dashboard : dashboards)
    {
      if (DashboardType.SUPER_ADMIN.getType().equals(dashboard))
      {
        response.setSuperAdminDashboard(true);
      }
      else if (DashboardType.OPERATION_HEAD.getType().equals(dashboard))
      {
        response.setOperationHeadDashboard(true);
      }
      else if (DashboardType.OPERATION_MANAGER.getType().equals(dashboard))
      {
        response.setOperationManagerDashboard(true);
      }
      else if (DashboardType.SCHOOL_HEAD.getType().equals(dashboard))
      {
        response.setSchoolHeadDashboard(true);
      }
      else if (DashboardType.FIELD_MANAGER.getType().equals(dashboard))
      {
        response.setFieldManagerDashboard(true);
      }
    }
    List<String> schools = new ArrayList<>(request.getSchools());
    List<String> grades = new ArrayList<>(request.getGrades());
    List<Long> tests = new ArrayList<>(request.getTests());
    List<Test> testList = new ArrayList<>();
    if (tests.isEmpty())
    {
      productRepository.findAllByTenantAndIdIn(getTenant(), new ArrayList<>(request.getProducts()))
                       .forEach(product -> {
                         testList.addAll(product.getTest());
                       });
      testList.forEach(test -> {
        tests.add(test.getId());
      });
    }
    String formattedFromDate = "";
    String formattedToDate = "";
    int maleAssignedCount = 0;
    int feMaleAssignedCount = 0;
    int maleAttemptCount = 0;
    int feMaleAttemptCount = 0;
    DateFormat writeFormat = new SimpleDateFormat("yyyy-MM-dd");
    Date date1 = null;
    Date date2 = null;
    try
    {
      formattedFromDate = writeFormat.format(writeFormat.parse(request.getFromDate()));
      formattedToDate = writeFormat.format(DateUtils.addDays(DateUtils.addMinutes(writeFormat.parse(request.getToDate()), -330), 2));
      date1 = writeFormat.parse(formattedFromDate);
      date2 = writeFormat.parse(formattedToDate);
    }
    catch (ParseException e)
    {
      e.printStackTrace();
    }
    if (!schools.isEmpty() && !grades.isEmpty() && !tests.isEmpty())
    {
      maleAssignedCount = getMaleAssignedCount(schools, grades, tests, date1, date2);
      feMaleAssignedCount = getFeMaleAssignedCount(schools, grades, tests, date1, date2);
      maleAttemptCount = getMaleAttemptCount(schools, grades, tests, date1, date2);
      feMaleAttemptCount = getFeMaleAttemptCount(schools, grades, tests, date1, date2);
    }
    else if (!schools.isEmpty() && !grades.isEmpty())
    {
      maleAssignedCount = getMaleAssignedCountWhenTestIdIsNull(schools, grades, date1, date2);
      feMaleAssignedCount = getFeMaleAssignedCountWhenTestIdIsNull(schools, grades, date1, date2);
      maleAttemptCount = getMaleAttemptCountWhenTestIdIsNull(schools, grades, date1, date2);
      feMaleAttemptCount = getFeMaleAttemptCountWhenTestIdIsNull(schools, grades, date1, date2);
    }
    response.setAssignedMaleCount(maleAssignedCount);
    response.setAssignedFeMaleCount(feMaleAssignedCount);
    response.setRemainingMaleCount(maleAttemptCount);
    response.setRemainingFeMaleCount(feMaleAttemptCount);
    return response;
  }

  public int getMaleAssignedCountWhenTestIdIsNull(List<String> schools, List<String> grades, Date date1, Date date2)
  {
    return userTestRepository
        .countAllByUserEmailGradeInAndUserEmailGenderAndUserEmailSchoolNameInAndTenantAndCreatedDateBetweenOrUserEmailGradeInAndUserEmailGenderAndUserEmailSchoolNameInAndTenantAndLastModifiedDateBetween(
            grades, "Male", schools, getTenant(), date1, date2, grades, "Male", schools, getTenant(), date1, date2);
  }

  public int getMaleAssignedCount(List<String> schools, List<String> grades, List<Long> testId, Date date1, Date date2)
  {
    Set<UserTest> groupedUser = new HashSet<>();
    Set<Long> resultUser = new HashSet<>();
    List<UserTest> userTestsListByTestId = userTestRepository
        .findAllByUserEmailGradeInAndUserEmailGenderAndUserEmailSchoolNameInAndTenantAndTestsIdInAndCreatedDateBetweenOrUserEmailGradeInAndUserEmailGenderAndUserEmailSchoolNameInAndTenantAndTestsIdInAndLastModifiedDateBetween(
            grades, "Male", schools, getTenant(), testId, date1, date2, grades, "Male", schools, getTenant(), testId, date1, date2);
    userTestsListByTestId.forEach(userTest -> {
      groupedUser.add(userTest);
    });
    List<TestResult> testResults = testResultRepository
        .findAllByUserGradeInAndUserGenderAndUserSchoolNameInAndTestIdInAndTenantAndTestFinishAtBetween(grades, "Male", schools, testId, getTenant(), date1, date2);
    testResults.forEach(testResult -> {
      resultUser.add(testResult.getUser().getId());
    });
    return groupedUser.size() + resultUser.size();
  }

  public int getFeMaleAssignedCountWhenTestIdIsNull(List<String> schools, List<String> grades, Date date1, Date date2)
  {
    return userTestRepository
        .countAllByUserEmailGradeInAndUserEmailGenderAndUserEmailSchoolNameInAndTenantAndCreatedDateBetweenOrUserEmailGradeInAndUserEmailGenderAndUserEmailSchoolNameInAndTenantAndLastModifiedDateBetween(
            grades, "FeMale", schools, getTenant(), date1, date2, grades, "FeMale", schools, getTenant(), date1, date2);
  }

  public int getFeMaleAssignedCount(List<String> schools, List<String> grades, List<Long> testId, Date date1, Date date2)
  {
    Set<UserTest> groupedUser = new HashSet<>();
    Set<Long> resultUser = new HashSet<>();
    List<UserTest> userTestsListByTestId = userTestRepository
        .findAllByUserEmailGradeInAndUserEmailGenderAndUserEmailSchoolNameInAndTenantAndTestsIdInAndCreatedDateBetweenOrUserEmailGradeInAndUserEmailGenderAndUserEmailSchoolNameInAndTenantAndTestsIdInAndLastModifiedDateBetween(
            grades, "FeMale", schools, getTenant(), testId, date1, date2, grades, "FeMale", schools, getTenant(), testId, date1, date2);
    userTestsListByTestId.forEach(userTest -> {
      groupedUser.add(userTest);
    });
    List<TestResult> testResults = testResultRepository
        .findAllByUserGradeInAndUserGenderAndUserSchoolNameInAndTestIdInAndTenantAndTestFinishAtBetween(grades, "FeMale", schools, testId, getTenant(), date1, date2);
    testResults.forEach(testResult -> {
      resultUser.add(testResult.getUser().getId());
    });
    return groupedUser.size() + resultUser.size();
  }

  public int getMaleAttemptCount(List<String> schools, List<String> grades, List<Long> testId, Date date1, Date date2)
  {
    Set<Long> resultUser = new HashSet<>();
    List<TestResult> testResults = testResultRepository
        .findAllByUserGradeInAndUserGenderAndUserSchoolNameInAndTestIdInAndTenantAndTestFinishAtBetween(grades, "Male", schools, testId, getTenant(), date1, date2);
    testResults.forEach(testResult -> {
      resultUser.add(testResult.getUser().getId());
    });
    return resultUser.size();
  }

  public int getFeMaleAttemptCount(List<String> schools, List<String> grades, List<Long> testId, Date date1, Date date2)
  {
    Set<Long> resultUser = new HashSet<>();
    List<TestResult> testResults = testResultRepository
        .findAllByUserGradeInAndUserGenderAndUserSchoolNameInAndTestIdInAndTenantAndTestFinishAtBetween(grades, "FeMale", schools, testId, getTenant(), date1, date2);
    testResults.forEach(testResult -> {
      resultUser.add(testResult.getUser().getId());
    });
    return resultUser.size();
  }

  public int getMaleAttemptCountWhenTestIdIsNull(List<String> schools, List<String> grades, Date date1, Date date2)
  {
    return userTestRepository
        .countAllByUserEmailGradeInAndUserEmailGenderAndUserEmailSchoolNameInAndTestStatusAndTenantAndCreatedDateBetweenOrUserEmailGradeInAndUserEmailGenderAndUserEmailSchoolNameInAndTestStatusAndTenantAndLastModifiedDateBetween(
            grades, "Male", schools, "test completed successfully", getTenant(), date1, date2, grades, "Male", schools, "test completed successfully", getTenant(), date1, date2);
  }

  public int getFeMaleAttemptCountWhenTestIdIsNull(List<String> schools, List<String> grades, Date date1, Date date2)
  {
    return userTestRepository
        .countAllByUserEmailGradeInAndUserEmailGenderAndUserEmailSchoolNameInAndTestStatusAndTenantAndCreatedDateBetweenOrUserEmailGradeInAndUserEmailGenderAndUserEmailSchoolNameInAndTestStatusAndTenantAndLastModifiedDateBetween(
            grades, "FeMale", schools, "test completed successfully", getTenant(), date1, date2, grades, "FeMale", schools, "test completed successfully", getTenant(), date1,
            date2);
  }

  public HashSet<User> getNewUsers(DashboardRequest request)
  {
    Date fromDate = DateUtil.getFormattedDate(request.getFromDate(),START_OF_DAY);
    Date toDate = DateUtil.getFormattedDate(request.getToDate(),END_OF_DAY);

    if (fromDate == null)
    {
      fromDate = new LocalDate().minusDays(1).toDate();
    }
    if (toDate == null)
    {
      toDate = new LocalDate().toDate();
    }

    JPAQuery<User> query = new JPAQuery<>(entityManager);
    QUser user = QUser.user;
    query.select(user)
         .distinct()
         .from(user)
         .where(user.createdDate.between(fromDate,toDate));

    if (!CollectionUtils.isEmpty(request.getGrades()))
    {
      query.where(user.grade.in(request.getGrades()));
    }

    if (!CollectionUtils.isEmpty(request.getSchools()))
    {
      query.where(user.schoolName.in(request.getSchools()));
    }

    if (!CollectionUtils.isEmpty(request.getCities()))
    {
      query.where(user.city.in(request.getCities()));
    }

    if (!CollectionUtils.isEmpty(request.getTenants()) && getTenant().getName().equalsIgnoreCase("aim"))
    {
      query.where(user.tenant.id.in(request.getTenants()));
    }
    else
    {
      query.where(user.tenant.id.eq(getTenant().getId()));
    }

    return new HashSet<>(query.fetch());
  }

  public HashSet<User> getUsersForAimTestsByStatus(DashboardRequest request, ReportStatus status)
  {
    Date fromDate = DateUtil.getFormattedDate(request.getFromDate(),START_OF_DAY);
    Date toDate = DateUtil.getFormattedDate(request.getToDate(),END_OF_DAY);
    if (fromDate == null)
    {
      fromDate = new LocalDate().minusDays(1).toDate();
    }
    if (toDate == null)
    {
      toDate = new LocalDate().toDate();
    }

    JPAQuery<User> query = new JPAQuery<>(entityManager);
    QUserReportStatus userReportStatus = QUserReportStatus.userReportStatus;
    query.select(userReportStatus.user)
         .distinct()
         .from(userReportStatus)
         .where(userReportStatus.createdDate.between(fromDate,toDate))
         .where(userReportStatus.status.eq(status.toString()));

    if (!CollectionUtils.isEmpty(request.getGrades()))
    {
      query.where(userReportStatus.user.grade.in(request.getGrades()));
    }

    if (!CollectionUtils.isEmpty(request.getSchools()))
    {
      query.where(userReportStatus.user.schoolName.in(request.getSchools()));
    }

    if (!CollectionUtils.isEmpty(request.getCities()))
    {
      query.where(userReportStatus.user.city.in(request.getCities()));
    }

    if (!CollectionUtils.isEmpty(request.getTenants()) && getTenant().getName().equalsIgnoreCase("aim"))
    {
      query.where(userReportStatus.user.tenant.id.in(request.getTenants()));
    }
    else
    {
      query.where(userReportStatus.user.tenant.id.eq(getTenant().getId()));
    }


    return new HashSet<>(query.fetch());
  }

  public Long getUsersForAimTestsByStatusCount(DashboardRequest request, ReportStatus status)
  {
    Date fromDate = DateUtil.getFormattedDate(request.getFromDate(),START_OF_DAY);
    Date toDate = DateUtil.getFormattedDate(request.getToDate(),END_OF_DAY);
    if (fromDate == null)
    {
      fromDate = new LocalDate().minusDays(1).toDate();
    }
    if (toDate == null)
    {
      toDate = new LocalDate().toDate();
    }

    JPAQuery<User> query = new JPAQuery<>(entityManager);
    QUserReportStatus userReportStatus = QUserReportStatus.userReportStatus;
    query.distinct()
         .from(userReportStatus)
         .where(userReportStatus.createdDate.between(fromDate,toDate))
         .where(userReportStatus.status.eq(status.toString()));

    if (!CollectionUtils.isEmpty(request.getGrades()))
    {
      query.where(userReportStatus.user.grade.in(request.getGrades()));
    }

    if (!CollectionUtils.isEmpty(request.getSchools()))
    {
      query.where(userReportStatus.user.schoolName.in(request.getSchools()));
    }

    if (!CollectionUtils.isEmpty(request.getCities()))
    {
      query.where(userReportStatus.user.city.in(request.getCities()));
    }

    if (!CollectionUtils.isEmpty(request.getTenants()) && getTenant().getName().equalsIgnoreCase("aim"))
    {
      query.where(userReportStatus.user.tenant.id.in(request.getTenants()));
    }
    else
    {
      query.where(userReportStatus.user.tenant.id.eq(getTenant().getId()));
    }
    return query.fetchCount();
  }

  public JPAQuery<UserReportStatus> getUserReportStatus(DashboardRequest request, ReportStatus status)
  {
    Date fromDate = DateUtil.getFormattedDate(request.getFromDate(),START_OF_DAY);
    Date toDate = DateUtil.getFormattedDate(request.getToDate(),END_OF_DAY);
    if (fromDate == null)
    {
      fromDate = new LocalDate().minusDays(1).toDate();
    }
    if (toDate == null)
    {
      toDate = new LocalDate().toDate();
    }

    JPAQuery<UserReportStatus> query = new JPAQuery<>(entityManager);
    QUserReportStatus userReportStatus = QUserReportStatus.userReportStatus;
    query.select(userReportStatus)
         .distinct()
         .from(userReportStatus)
         .where(userReportStatus.createdDate.between(fromDate,toDate))
         .where(userReportStatus.status.eq(status.toString()));

    if (!CollectionUtils.isEmpty(request.getGrades()))
    {
      query.where(userReportStatus.user.grade.in(request.getGrades()));
    }

    if (!CollectionUtils.isEmpty(request.getSchools()))
    {
      query.where(userReportStatus.user.schoolName.in(request.getSchools()));
    }

    if (!CollectionUtils.isEmpty(request.getCities()))
    {
      query.where(userReportStatus.user.city.in(request.getCities()));
    }

    if (!CollectionUtils.isEmpty(request.getTenants()) && getTenant().getName().equalsIgnoreCase("aim"))
    {
      query.where(userReportStatus.user.tenant.id.in(request.getTenants()));
    }
    else
    {
      query.where(userReportStatus.user.tenant.id.eq(getTenant().getId()));
    }
    return query;
  }

  public Long getNewUsersCount(DashboardRequest request)
  {
    Date fromDate = DateUtil.getFormattedDate(request.getFromDate(),START_OF_DAY);
    Date toDate = DateUtil.getFormattedDate(request.getToDate(),END_OF_DAY);

    if (fromDate == null)
    {
      fromDate = new LocalDate().minusDays(1).toDate();
    }
    if (toDate == null)
    {
      toDate = new LocalDate().toDate();
    }

    JPAQuery<User> query = new JPAQuery<>(entityManager);
    QUser user = QUser.user;
    query.select(user)
         .distinct()
         .from(user)
         .where(user.createdDate.between(fromDate,toDate));

    if (!CollectionUtils.isEmpty(request.getGrades()))
    {
      query.where(user.grade.in(request.getGrades()));
    }

    if (!CollectionUtils.isEmpty(request.getSchools()))
    {
      query.where(user.schoolName.in(request.getSchools()));
    }

    if (!CollectionUtils.isEmpty(request.getCities()))
    {
      query.where(user.city.in(request.getCities()));
    }

    if (!CollectionUtils.isEmpty(request.getTenants()) && getTenant().getName().equalsIgnoreCase("aim"))
    {
      query.where(user.tenant.id.in(request.getTenants()));
    }
    else
    {
      query.where(user.tenant.id.eq(getTenant().getId()));
    }

    return query.fetchCount();
  }
}
