package com.synlabs.intscale.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.synlabs.intscale.common.CategoryMatrix;
import com.synlabs.intscale.common.CategoryMatrixItem;
import com.synlabs.intscale.entity.Report.Category;
import com.synlabs.intscale.ex.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
public class CategoryMatrixCalculator
{
  @Autowired
  private ResourceLoader resourceLoader;

  private static final Logger logger = LoggerFactory.getLogger(CategoryMatrixCalculator.class);

  private List<CategoryMatrix> categoryMatrixFiles = new ArrayList<>();

  private ObjectMapper jsonMapper = new ObjectMapper();

  @PostConstruct
  public void init()
  {
    try
    {
      Resource[] resources

          = ResourcePatternUtils.getResourcePatternResolver(resourceLoader)
                                .getResources("classpath*:/reportCalculation/*.json");

      for (Resource resource : resources)
      {
        logger.info("Loaded Category Norms Matrix from {}", resource.getFilename());
        CategoryMatrix file = jsonMapper.readValue(resource.getInputStream(), CategoryMatrix.class);
        categoryMatrixFiles.add(file);
      }
    }
    catch (IOException e)
    {
      logger.error("Cannot load Category Norms Matrix from disk!", e);
    }
  }

  public BigDecimal getPercentileByCategory(Category category, Integer rawScore, String grade)
  {
    if (StringUtils.isEmpty(grade))
    {
      return new BigDecimal(0);
    }

    CategoryMatrix file;

    switch (grade)
    {
      case "6":
      case "7":
      case "8":
        file = categoryMatrixFiles.get(0);
        for (CategoryMatrixItem row : file.getCategoryMatrix())
        {
          if (row.getCategory().equalsIgnoreCase(category.getName()))
          {
            BigDecimal normArray[] = row.getNormScore();
            return normArray[rawScore];
          }
        }
        break;
      case "9":
      case "10":
      case "11":
      case "12":
        file = categoryMatrixFiles.get(1);
        for (CategoryMatrixItem row : file.getCategoryMatrix())
        {
          if (row.getCategory().equalsIgnoreCase(category.getName()))
          {
            BigDecimal normArray[] = row.getNormScore();
            return normArray[rawScore];
          }
        }
        break;
      default:
        return new BigDecimal(0);
    }

    return new BigDecimal(0);

//    if (grade.equals("6") || grade.equals("7") || grade.equals("8"))
//    {
//      file = categoryMatrixFiles.get(0);
//    }
//    else
//    {
//      file = categoryMatrixFiles.get(1);
//    }
//    for (CategoryMatrixItem row : file.getCategoryMatrix())
//    {
//      if (row.getCategory().equalsIgnoreCase(category.getName()))
//      {
//        BigDecimal normArray[] = row.getNormScore();
//        return normArray[rawScore];

    /******************* code for finding the closest norm from the available norms starts here********************
     Integer index = 0;
     BigDecimal distance = new BigDecimal(0);
     while (index < normArray.length)
     {
     BigDecimal temp = normArray[index].subtract(rawScore);
     if (temp.signum() != -1)
     {
     distance = temp; //getting first positive distance
     break;
     }
     index++;
     }

     for (Integer c = 0; c < normArray.length; c++)
     {
     BigDecimal cdistance = normArray[c].subtract(rawScore);
     if (cdistance.signum() == 1 && cdistance.compareTo(distance) <= 0)
     {
     index = c;
     distance = cdistance; // try to getting minimum distance
     }
     }
     BigDecimal score = normArray[index]; // found score in norms have the minimum distance and closest to raw score
     return index;

     ********************** code for finding the closest norm from the available norms ends here*********************/

//      }
//    }
//    throw new NotFoundException(category.getName() + "'s norm not found for calculating score");

  }

}
