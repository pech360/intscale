package com.synlabs.intscale.service;

import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.ex.NotFoundException;
import com.synlabs.intscale.jpa.TenantRepository;
import com.synlabs.intscale.view.TenantRequest;
import com.synlabs.intscale.view.TenantResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by India on 12/27/2017.
 */
@Service
public class TenantService extends BaseService {
    @Autowired
    private TenantRepository tenantRepository;

    public List<Tenant> findAll() {
        return tenantRepository.findAllByActive(true);
    }

    public List<Tenant> findAllExceptCurrent() {
        return tenantRepository.findAllByActiveIsTrueAndIdNotIn(getTenant().getId());
    }

    public Tenant save(TenantRequest request) {
        Tenant tenant = request.toEntity();
        tenant.setActive(true);
        return tenantRepository.save(tenant);
    }

    public Tenant update(TenantRequest request) {
        return tenantRepository.save(request.toEntity(tenantRepository.findOne(request.getId())));
    }

    public void delete(Long id) {
        Tenant tenant = tenantRepository.findOne(id);
        if (tenant != null)
            tenant.setActive(false);
        tenantRepository.save(tenant);
    }
    
   public Set<String> getDomains(){
	   Set<String> domains = new HashSet<String>();
	   if(tenantRepository .findAll().isEmpty() || tenantRepository .findAll() == null)
		   throw new NotFoundException("No Domains found.");
	   for( Tenant tenant : tenantRepository .findAll())
		   domains.add(tenant.getSubDomain());
    	return domains;
    }

}
