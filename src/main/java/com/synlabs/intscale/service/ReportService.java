package com.synlabs.intscale.service;

import com.google.common.util.concurrent.RateLimiter;
import com.querydsl.core.group.GroupBy;
import com.querydsl.jpa.impl.JPAQuery;
import com.synlabs.intscale.common.EmailMessage;
import com.synlabs.intscale.entity.Report.*;
import com.synlabs.intscale.entity.Timeline;
import com.synlabs.intscale.entity.masterdata.MasterDataDocument;
import com.synlabs.intscale.entity.test.*;
import com.synlabs.intscale.entity.user.AcademicPerformance;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.enums.*;
import com.synlabs.intscale.ex.NotFoundException;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.*;
import com.synlabs.intscale.service.communication.CommunicationService;
import com.synlabs.intscale.store.FileStore;
import com.synlabs.intscale.util.DateUtil;
import com.synlabs.intscale.view.*;
import com.synlabs.intscale.view.report.*;
import com.synlabs.intscale.view.ScoringResponse;
import com.synlabs.intscale.view.UserDetails;
import com.synlabs.intscale.view.report.CategoryResponse;
import com.synlabs.intscale.view.reportShare.ReportShareRequest;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import okhttp3.*;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.mariuszgromada.math.mxparser.Expression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.YamlMapFactoryBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.ConnectException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.synlabs.intscale.enums.AcademicPerformanceName.MATHEMATICS;
import static com.synlabs.intscale.enums.AcademicPerformanceName.SCIENCE;
import static com.synlabs.intscale.util.DateUtil.DayTime.END_OF_DAY;
import static com.synlabs.intscale.util.DateUtil.DayTime.START_OF_DAY;

/**
 * Created by itrs on 9/14/2017.
 */
@Service
public class ReportService extends BaseService {
	private static Logger logger = LoggerFactory.getLogger(ReportService.class);

	@Value("${spring.datasource.sqlfile}")
	protected String sqlfile;

	protected Map<String, Object> queries;

	@Value("${scale.image.upload.location}")
	protected String filedir;

	@Value("${intscale.pdfserver.baseUrl}")
	private String pdfServerUrl;

	@Value("${intscale.auth.secretkey}")
	private String secretkey;

	@Value("${scale.image.upload.location:}")
	private String tempDirPath;

	@Value("${intscale.baseUrl}")
	private String intscaleBaseUrl;

	@Value("${intscale.s3.assets-url}")
	private String intscale_assets;

	@Value("${intscale.report.permitsPerSecond}")
	private Double permitsPerSecond;

	@Autowired
	@Qualifier("dataSource")
	protected DataSource dataSource;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private CategoryService categoryService;
	@Autowired
	private UserService userService;
	@Autowired
	private TestRepository testRepository;
	@Autowired
	private UserTestRepository userTestRepository;
	@Autowired
	private UserAnswerRepository userAnswerRepository;
	@Autowired
	private TestResultRepository testResultRepository;
	@Autowired
	private TestQuestionRepository testQuestionRepository;
	@Autowired
	private TextquestionAnswersRepository textquestionAnswersRepository;
	@Autowired
	private ReportRepository reportRepository;
	@Autowired
	private ScoreRangeRepository scoreRangeRepository;
	@Autowired
	private ReportSectionRepository reportSectionRepository;
	@Autowired
	private InterestInventoryRepository interestInventoryRepository;
	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private CategoryMatrixCalculator categoryMatrixCalculator;
	@Autowired
	private OkHttpClient okHttpClient;
	@Autowired
	private UserRepository userRepository;

	protected NamedParameterJdbcTemplate jdbcTemplate;

	@Autowired
	private UserReportStatusRepository userReportStatusRepository;

	@Autowired
	private FileStore store;

	@Autowired
	private CommunicationService communicationService;

	@Autowired
	private TimelineService timelineService;

	@Autowired
	private AimReportContentRepository aimReportContentRepository;

	@Autowired
	private DashboardService dashboardService;

	@Autowired
	private NormRepository normRepository;

	@Autowired
	private MasterDataDocumentService masterDataDocumentService;

	@Autowired
	private AsyncReportService asyncReportService;

	private RateLimiter rateLimiter;

	@PostConstruct
	public void loadConfiguration() {
		YamlMapFactoryBean factory = new YamlMapFactoryBean();
		factory.setResources(new ClassPathResource[] { new ClassPathResource(sqlfile) });
		this.queries = factory.getObject();
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	@PostConstruct
	public void initRateLimiter() {
		this.rateLimiter = RateLimiter.create(permitsPerSecond);
	}

	private static final String COMMA_DELIMITER = ",";
	private static final String NEW_LINE_SEPARATOR = "\n";
	private static final String DEFAULT_REPORT_FILE_HEADER = "TestDate,TestStartTime,TestBeginTime,TestEndTime,TestName,SubConstructName,ConstructName,Grade,UserName,Gender,SchoolName,Category,SubCategory,GroupName,TimeTaken,Question,QuestionId,SubTag,Marks,OptionNumber,Answer,AnswerText";
	private static final String TEST_COUNT_FILE_HEADER = "SerialNumber,School,Grade,UserName,Gender,TestCount,TestNames";
	private static final String USERTEST_ASSIGNMENT_FILE_HEADER = "UserName,School,Grade,Gender,TestNames,TestStatus";
	private static final String USER_INTEREST_FILE_HEADER = "UserName,FullName,School,Grade,Gender,Interests,AcademicScore,FeedbackRating";

	public String generateTestReport(XSSFWorkbook workbook, Long testId, Date fromdate, Date todate, String school,
			String grade, Long category, Long subCategory) throws IOException {
		try {

			String defaultReportV2 = generateDefaultReport(testId, fromdate, todate, school, grade, category,
					subCategory);

			if (!StringUtils.isEmpty(defaultReportV2)) {
				return defaultReportV2;
			}

			/* ################################################## */
			Path path = Paths.get(filedir);
			logger.info(filedir);
			String filename = path.resolve(UUID.randomUUID().toString() + ".csv").toString();
			logger.info("in generateTestReport 70 fileName " + filename);
			StringBuilder testReport = new StringBuilder((String) queries.get("TestSummaryReport"));
			Map<String, Object> paramMap = new HashMap<>();
			String formattedFromDate = "";
			String formattedToDate = "";

			if ((!StringUtils.isEmpty(fromdate)) && (!StringUtils.isEmpty(todate))) {
				DateFormat writeFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date fdate = DateUtils.addMinutes(fromdate, 0);
				Date tdate = DateUtils.addDays(DateUtils.addMinutes(todate, -330), 2);
				if (fdate != null && tdate != null) {
					formattedFromDate = writeFormat.format(fdate);
					formattedToDate = writeFormat.format(tdate);
				}
				paramMap.put("fromDate", formattedFromDate);
				paramMap.put("toDate", formattedToDate);
				testReport.append(" and tr.test_finish_at between :fromDate  and  :toDate ");
			}
			if (!StringUtils.isEmpty(school)) {
				paramMap.put("school", school);
				testReport.append(" and u.school_name=:school ");
			}
			if (!StringUtils.isEmpty(grade)) {
				paramMap.put("grad", grade);
				testReport.append(" and u.grade=:grad");
			}
			paramMap.put("testId", testId);
			if (category != null) {
				paramMap.put("category", category);
				testReport.append(" and pc.id=:category ");
			}
			if (subCategory != null) {
				paramMap.put("subCategory", subCategory);
				testReport.append(" and sc.id=:subCategory ");
			}
			if (category == null && subCategory == null) {
				testReport.append(" and pc.parent_id is null group by un.id order by u.username limit 150000;");
			} else {
				testReport.append(" group by un.id order by u.username limit 150000;");
			}
			List<Map<String, Object>> testMapList = jdbcTemplate.queryForList(testReport.toString(), paramMap);
			StringBuilder parentName = new StringBuilder();
			StringBuilder childName = new StringBuilder();
			Set<String> parentSet = new HashSet<>();
			Set<String> childSet = new HashSet<>();
			FileWriter fileWriter = new FileWriter(filename);
			fileWriter.append(DEFAULT_REPORT_FILE_HEADER.toString());
			fileWriter.append(NEW_LINE_SEPARATOR);
			HashMap<Long, String> questionIdAndTextAnswers = new HashMap<>();
			List<TextquestionAnswers> textquestionAnswers = textquestionAnswersRepository.findAllByTenant(getTenant());
			textquestionAnswers.forEach(textQue -> {
				questionIdAndTextAnswers.put(textQue.getQuestionId(), textQue.getTextAnswer());
			});
			String obtainMarks = "0";
			for (Map<String, Object> report : testMapList) {
				String questionType = report.get("questionType") != null ? report.get("questionType").toString() : "";
				if (questionType.equals("Text")) {
					if (report.get("answerText").equals(questionIdAndTextAnswers.get(report.get("id")))) {
						obtainMarks = "1";
					}
				} else {
					obtainMarks = report.get("marks") != null ? String.valueOf(report.get("marks")) : "";
				}
				String parentCat = report.get("parentCategoryName") != null
						? report.get("parentCategoryName").toString()
						: "";
				String childCat = report.get("subcategoryName") != null ? report.get("subcategoryName").toString() : "";
				if (parentCat != "") {
					for (String s : parentCat.split(",")) {
						parentSet.add(s.replaceAll("[ ]", ""));
					}
				}
				if (childCat != "") {
					for (String s : childCat.split(",")) {
						childSet.add(s.replaceAll("[ ]", ""));
					}
				}
				parentSet.toString();
				for (String s : parentSet) {
					parentName.append(s + " ");
				}
				for (String s : childSet) {
					childName.append(s + " ");
				}
				// Row row = sheet.createRow(rownum++);
				// cellnum = 0;
				String dateStr = report.get("resultDateTime") != null ? report.get("resultDateTime").toString() : "";
				String loadedDateStr = report.get("loadedTime") != null ? report.get("loadedTime").toString() : "";
				String beginDateStr = report.get("beginTime") != null ? report.get("beginTime").toString() : "";
				Date date = null, loadedDateTime = null, beginDateTime = null;
				String dtformat = "yyyy-MM-dd HH:mm:ss";
				String dformat = "MM/dd/yyyy";
				String tformat = "HH:mm:ss";
				SimpleDateFormat formatter = new SimpleDateFormat(dtformat);
				SimpleDateFormat dateFormat = new SimpleDateFormat(dformat);
				SimpleDateFormat timeFormat = new SimpleDateFormat(tformat);
				try {
					date = formatter.parse(dateStr);
					if (loadedDateStr != "") {
						loadedDateTime = formatter.parse(loadedDateStr);
						loadedDateTime = DateUtils.addMinutes(loadedDateTime, 330);
					}
					if (beginDateStr != "") {
						beginDateTime = formatter.parse(beginDateStr);
						beginDateTime = DateUtils.addMinutes(beginDateTime, 330);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				date = DateUtils.addMinutes(date, 330);

				fileWriter.append(date != null ? dateFormat.format(date) : "Not Found");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(loadedDateTime != null ? timeFormat.format(loadedDateTime) : "Not Found");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(beginDateTime != null ? timeFormat.format(beginDateTime) : "Not Found");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(date != null ? timeFormat.format(date) : "Not Found");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf('"'))
						.append(report.get("testName") != null ? String.valueOf(report.get("testName")) : "")
						.append(String.valueOf('"'));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf('"')).append(
						report.get("subConstructName") != null ? String.valueOf(report.get("subConstructName")) : "")
						.append(String.valueOf('"'));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf('"'))
						.append(report.get("constructName") != null ? String.valueOf(report.get("constructName")) : "")
						.append(String.valueOf('"'));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("grade") != null ? String.valueOf(report.get("grade")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("userName") != null ? String.valueOf(report.get("userName")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("gender") != null ? String.valueOf(report.get("gender")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf('"'))
						.append(report.get("schoolName") != null ? String.valueOf(report.get("schoolName")) : "")
						.append(String.valueOf('"'));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(parentName.toString());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(childName.toString());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("group_name") != null ? String.valueOf(report.get("group_name")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("answerTime") != null ? String.valueOf(report.get("answerTime")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf('"'))
						.append(report.get("questionDescription") != null
								? String.valueOf(report.get("questionDescription"))
								: "")
						.append(String.valueOf('"'));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("id") != null ? String.valueOf(report.get("id")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf('"'))
						.append(report.get("subTag") != null ? String.valueOf(report.get("subTag")) : "")
						.append(String.valueOf('"'));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(obtainMarks);
				fileWriter.append(COMMA_DELIMITER);
				fileWriter
						.append(report.get("option_number") != null ? String.valueOf(report.get("option_number")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf('"')).append(
						report.get("ansDescription") != null ? String.valueOf(report.get("ansDescription")) : "")
						.append(String.valueOf('"'));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf('"'))
						.append(report.get("answerText") != null ? String.valueOf(report.get("answerText")) : "")
						.append(String.valueOf('"'));
				fileWriter.append(NEW_LINE_SEPARATOR);
				obtainMarks = "0";
				parentSet.clear();
				childSet.clear();
				parentName = new StringBuilder();
				childName = new StringBuilder();
			}
			logger.info(filename);
			fileWriter.flush();
			fileWriter.close();
			return filename;
		} catch (IOException e) {
			logger.info("Error in generating Test Summary Report", e);
		}
		return null;
	}

	public String generateCustomizeTestReport(XSSFWorkbook workbook, Long testId, Date fromdate, Date todate,
			String school, String grade) throws IOException {
		try {
			XSSFSheet sheet = workbook.createSheet("Test Summary Report");
			CellStyle style = workbook.createCellStyle();
			style.setBorderBottom(CellStyle.BORDER_THIN);
			style.setBorderLeft(CellStyle.BORDER_THIN);
			style.setBorderRight(CellStyle.BORDER_THIN);
			style.setBorderTop(CellStyle.BORDER_THIN);
			CellStyle style1 = headerCellStyle(workbook);
			Path path = Paths.get(filedir);
			logger.info(filedir);
			String filename = path.resolve(UUID.randomUUID().toString() + ".xlxs").toString();
			logger.info("in generateTestReport 70 fileName " + filename);
			int rownum = 0;
			int cellnum = 0;
			Row header = sheet.createRow(rownum++);
			header.setHeight((short) 500);
			StringBuilder remainUser = new StringBuilder((String) queries.get("RemainingUser"));
			StringBuilder maxCountQuery = new StringBuilder((String) queries.get("MaxCountQuery"));
			Map<String, Object> param = new HashMap<>();
			param.put("testId", testId);
			int maxCount = 0;
			boolean headerFlag = true;
			List<Map<String, Object>> countList = jdbcTemplate.queryForList(maxCountQuery.toString(), param);
			if (!countList.isEmpty()) {
				maxCount = countList.get(0).get("maxCount") != null
						? Integer.parseInt(countList.get(0).get("maxCount").toString())
						: 0;
			}
			for (int k = 1; k <= maxCount; k++) {
				if (headerFlag) {
					createCell(header, "SerialNumber", cellnum++, style1);
					createCell(header, "School", cellnum++, style1);
					createCell(header, "Grade", cellnum++, style1);
					createCell(header, "UserName", cellnum++, style1);
					createCell(header, "Gender", cellnum++, style1);
					createCell(header, "Marks" + k, cellnum++, style1);
					headerFlag = false;
				} else {
					createCell(header, "Marks" + k, cellnum++, style1);
				}
			}
			for (int k = 1; k <= maxCount; k++) {
				if (headerFlag) {
					createCell(header, "TimeTaken" + k, cellnum++, style1);
					headerFlag = false;
				} else {
					createCell(header, "TimeTaken" + k, cellnum++, style1);
				}
			}
			for (int k = 1; k <= maxCount; k++) {
				if (headerFlag) {
					createCell(header, "Sequence" + k, cellnum++, style1);
					headerFlag = false;
				} else {
					createCell(header, "Sequence" + k, cellnum++, style1);
				}
			}
			StringBuilder testReport = new StringBuilder((String) queries.get("CustomizeReport"));
			Map<String, Object> paramMap = new HashMap<>();
			String formattedFromDate = "";
			String formattedToDate = "";
			if ((!StringUtils.isEmpty(fromdate)) && (!StringUtils.isEmpty(todate))) {
				DateFormat writeFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date fdate = DateUtils.addMinutes(fromdate, 0);
				Date tdate = DateUtils.addDays(DateUtils.addMinutes(todate, -330), 2);
				if (fdate != null && tdate != null) {
					formattedFromDate = writeFormat.format(fdate);
					formattedToDate = writeFormat.format(tdate);
				}
				paramMap.put("fromDate", formattedFromDate);
				paramMap.put("toDate", formattedToDate);
				testReport.append(" and tr.test_finish_at between :fromDate  and  :toDate ");
			}
			if (!StringUtils.isEmpty(school)) {
				paramMap.put("school", school);
				testReport.append(" and u.school_name=:school ");
			}
			if (!StringUtils.isEmpty(grade)) {
				paramMap.put("grad", grade);
				testReport.append(" and u.grade=:grad");
			}
			paramMap.put("testId", testId);
			testReport.append(" order by q.id;");
			Row row = null;
			int sequenceCount = 1;
			boolean flag = true;
			Set<String> userIds = new HashSet<>();
			List<Map<String, Object>> testMapList = jdbcTemplate.queryForList(testReport.toString(), paramMap);
			testMapList.forEach(tm -> {
				userIds.add(tm.get("userId").toString());
			});
			Map<String, Object> paramForUser = new HashMap<>();
			paramForUser.put("userIds", userIds);
			List<Map<String, Object>> remainingUser = jdbcTemplate.queryForList(remainUser.toString(), paramForUser);
			testMapList.addAll(remainingUser);
			Collections.sort(testMapList, new Comparator<Map>() {
				@Override
				public int compare(Map o1, Map o2) {
					return (o1.get("username").toString().toUpperCase())
							.compareTo(o2.get("username").toString().toUpperCase());
				}
			});
			Set<String> ids = new HashSet<>();
			int size = 0;
			int preSize = 0;
			for (Map<String, Object> report : testMapList) {
				ids.add(report.get("userId").toString());
				size = ids.size();
				if (flag) {
					row = sheet.createRow(rownum++);
					cellnum = 0;
					createCellForInt(row, sequenceCount++, cellnum++, style);
					createCell(row, report.get("schoolName") != null ? String.valueOf(report.get("schoolName")) : "",
							cellnum++, style);
					createCellForInt(row,
							report.get("grade") != null ? Integer.parseInt(report.get("grade").toString()) : 0,
							cellnum++, style);
					createCell(row, report.get("username") != null ? String.valueOf(report.get("username")) : "",
							cellnum++, style);
					createCell(row, report.get("gender") != null ? String.valueOf(report.get("gender")) : "", cellnum++,
							style);
					createCellForInt(row,
							report.get("marks") != null ? Integer.parseInt(report.get("marks").toString()) : 0,
							cellnum++, style);
					flag = false;
				} else if (size > preSize) {
					row = sheet.createRow(rownum++);
					cellnum = 0;
					createCellForInt(row, sequenceCount++, cellnum++, style);
					createCell(row, report.get("schoolName") != null ? String.valueOf(report.get("schoolName")) : "",
							cellnum++, style);
					createCell(row, report.get("grade") != null ? String.valueOf(report.get("grade")) : "", cellnum++,
							style);
					createCell(row, report.get("username") != null ? String.valueOf(report.get("username")) : "",
							cellnum++, style);
					createCell(row, report.get("gender") != null ? String.valueOf(report.get("gender")) : "", cellnum++,
							style);
					createCellForInt(row,
							report.get("marks") != null ? Integer.parseInt(report.get("marks").toString()) : 0,
							cellnum++, style);
				} else {
					createCellForInt(row,
							report.get("marks") != null ? Integer.parseInt(report.get("marks").toString()) : 0,
							cellnum++, style);
				}
				preSize = size;
			}
			int serialNumber = sequenceCount;
			row = null;
			flag = true;
			rownum = 1;
			size = 0;
			preSize = 0;
			ids.clear();
			for (Map<String, Object> report : testMapList) {
				ids.add(report.get("userId").toString());
				size = ids.size();
				if (flag) {
					row = sheet.getRow(rownum++);
					if (row != null) {
						cellnum = maxCount + 5;
						createCellForInt(row,
								report.get("time_taken") != null ? Integer.parseInt(report.get("time_taken").toString())
										: 0,
								cellnum++, style);
						flag = false;
					}
				} else if (size > preSize) {
					row = sheet.getRow(rownum++);
					if (row != null) {
						cellnum = maxCount + 5;
						createCellForInt(row,
								report.get("time_taken") != null ? Integer.parseInt(report.get("time_taken").toString())
										: 0,
								cellnum++, style);
					}
				} else {
					createCellForInt(row,
							report.get("time_taken") != null ? Integer.parseInt(report.get("time_taken").toString())
									: 0,
							cellnum++, style);
				}
				preSize = size;
			}
			row = null;
			flag = true;
			rownum = 1;
			size = 0;
			preSize = 0;
			ids.clear();
			for (Map<String, Object> report : testMapList) {
				ids.add(report.get("userId").toString());
				size = ids.size();
				if (flag) {
					sequenceCount = 1;
					row = sheet.getRow(rownum++);
					if (row != null) {
						cellnum = maxCount + maxCount + 5;
						createCellForInt(row, sequenceCount++, cellnum++, style);
						flag = false;
					}
				} else if (size > preSize) {
					sequenceCount = 1;
					row = sheet.getRow(rownum++);
					if (row != null) {
						cellnum = maxCount + maxCount + 5;
						createCellForInt(row, sequenceCount++, cellnum++, style);
					}
				} else {
					createCellForInt(row, sequenceCount++, cellnum++, style);
				}
				preSize = size;
			}
			for (int i = 0; i < cellnum; i++) {
				sheet.autoSizeColumn(i);
			}
			logger.info(filename);
			FileOutputStream out = new FileOutputStream(filename);
			workbook.write(out);
			out.close();
			out.flush();
			return filename;
		} catch (IOException e) {
			logger.info("Error in generating Test Summary Report", e);
		}
		return null;
	}

	public String generateTestCountReportPerUser(XSSFWorkbook workbook, Long testId, Date fromdate, Date todate,
			String school, String grade) throws IOException {
		try {
			/*
			 * XSSFSheet sheet = workbook.createSheet("Test Summary Report"); CellStyle
			 * style = workbook.createCellStyle();
			 * style.setBorderBottom(CellStyle.BORDER_THIN);
			 * style.setBorderLeft(CellStyle.BORDER_THIN);
			 * style.setBorderRight(CellStyle.BORDER_THIN);
			 * style.setBorderTop(CellStyle.BORDER_THIN); CellStyle style1 =
			 * headerCellStyle(workbook);
			 */
			Path path = Paths.get(filedir);
			logger.info(filedir);
			String filename = path.resolve(UUID.randomUUID().toString() + ".csv").toString();
			logger.info("in generateTestReport 70 fileName " + filename);
			/*
			 * int rownum = 0; int cellnum = 0; Row header = sheet.createRow(rownum++);
			 * header.setHeight((short) 500); createCell(header, "SerialNumber", cellnum++,
			 * style1); createCell(header, "School", cellnum++, style1); createCell(header,
			 * "Grade", cellnum++, style1); createCell(header, "UserName", cellnum++,
			 * style1); createCell(header, "Gender", cellnum++, style1); createCell(header,
			 * "TestCount", cellnum++, style1); createCell(header, "TestNames", cellnum++,
			 * style1);
			 */
			StringBuilder testReport = new StringBuilder((String) queries.get("TestPerUser"));
			Map<String, Object> paramMap = new HashMap<>();
			if (!StringUtils.isEmpty(testId) && testId != 0) {
				paramMap.put("testId", testId);
				testReport.append(" where tr.id In(select id from test_result where test_id=:testId) ");

				if (!getTenant().getName().equalsIgnoreCase("aim")) {
					paramMap.put("tenantId", getTenant().getId());
					testReport.append(" and tr.tenant_id=:tenantId ");
				}
			} else {
				if (!getTenant().getName().equalsIgnoreCase("aim")) {
					paramMap.put("tenantId", getTenant().getId());
					testReport.append(" where tr.tenant_id=:tenantId ");
				}
			}

			String formattedFromDate = "";
			String formattedToDate = "";
			if ((!StringUtils.isEmpty(fromdate)) && (!StringUtils.isEmpty(todate))) {
				DateFormat writeFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date fdate = DateUtils.addMinutes(fromdate, 0);
				Date tdate = DateUtils.addDays(DateUtils.addMinutes(todate, -330), 2);
				if (fdate != null && tdate != null) {
					formattedFromDate = writeFormat.format(fdate);
					formattedToDate = writeFormat.format(tdate);
				}
				paramMap.put("fromDate", formattedFromDate);
				paramMap.put("toDate", formattedToDate);
				testReport.append(" and tr.test_finish_at between :fromDate  and  :toDate ");
			}

			if (!StringUtils.isEmpty(school)) {
				paramMap.put("school", school);
				testReport.append(" and u.school_name=:school ");
			}
			if (!StringUtils.isEmpty(grade)) {
				paramMap.put("grad", grade);
				testReport.append(" and u.grade=:grad");
			}
			testReport.append(" group by u.username order by u.username;");
			// Row row = null;
			int sequenceCount = 1;
			List<Map<String, Object>> testMapList = jdbcTemplate.queryForList(testReport.toString(), paramMap);
			FileWriter fileWriter = new FileWriter(filename);
			fileWriter.append(TEST_COUNT_FILE_HEADER.toString());
			fileWriter.append(NEW_LINE_SEPARATOR);
			for (Map<String, Object> report : testMapList) {
				fileWriter.append(String.valueOf(sequenceCount++));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf('"'))
						.append(report.get("schoolName") != null ? String.valueOf(report.get("schoolName")) : "")
						.append(String.valueOf('"'));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("grade") != null ? String.valueOf(report.get("grade")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("username") != null ? String.valueOf(report.get("username")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("gender") != null ? String.valueOf(report.get("gender")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("testCount") != null ? String.valueOf(report.get("testCount")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf('"'))
						.append(report.get("testNames") != null ? String.valueOf(report.get("testNames")) : "")
						.append(String.valueOf('"'));
				// fileWriter.append(report.get("testNames") != null ?
				// String.valueOf(report.get("testNames")).replaceAll(",","") : "");
				fileWriter.append(NEW_LINE_SEPARATOR);
				/*
				 * row = sheet.createRow(rownum++); cellnum = 0; createCellForInt(row,
				 * sequenceCount++, cellnum++, style); createCell(row, report.get("schoolName")
				 * != null ? String.valueOf(report.get("schoolName")) : "", cellnum++, style);
				 * createCellForInt(row, report.get("grade") != null ?
				 * Integer.parseInt(report.get("grade").toString()) : 0, cellnum++, style);
				 * createCell(row, report.get("username") != null ?
				 * String.valueOf(report.get("username")) : "", cellnum++, style);
				 * createCell(row, report.get("gender") != null ?
				 * String.valueOf(report.get("gender")) : "", cellnum++, style);
				 * createCellForInt(row, report.get("testCount") != null ?
				 * Integer.parseInt(report.get("testCount").toString()) : 0, cellnum++, style);
				 * createCell(row, report.get("testNames") != null ?
				 * String.valueOf(report.get("testNames")) : "", cellnum++, style);
				 */
			}
			/*
			 * for (int i = 0; i < cellnum; i++) { sheet.autoSizeColumn(i); }
			 */
			logger.info(filename);
			/*
			 * FileOutputStream out = new FileOutputStream(filename); workbook.write(out);
			 * out.close(); out.flush();
			 */
			fileWriter.flush();
			fileWriter.close();
			return filename;
		} catch (IOException e) {
			logger.info("Error in generating Test Summary Report", e);
		}
		return null;
	}

	public List<ScoringResponse> listForScoring(Long categoryId, Long subCategoryId, Long testId) {
		List<Map<String, Object>> scoreList = getData(categoryId, subCategoryId, testId);
		List<ScoringResponse> responses = new ArrayList<>();
		for (Map<String, Object> report : scoreList) {
			ScoringResponse response = new ScoringResponse();
			response.setUsername(report.get("username") != null ? report.get("username").toString() : "");
			response.setTestname(report.get("testName") != null ? report.get("testName").toString() : "");
			response.setCategory(report.get("pcName") != null ? report.get("pcName").toString() : "");
			response.setSubcategory(report.get("scName") != null ? report.get("scName").toString() : "");
			response.setGender(report.get("gender") != null ? report.get("gender").toString() : "");
			response.setGrade(report.get("grade") != null ? report.get("grade").toString() : "");
			response.setItems(report.get("items") != null ? report.get("items").toString() : "");
			response.setMarks(report.get("marks") != null ? report.get("marks").toString() : "");
			response.setMean(report.get("mean") != null ? report.get("mean").toString() : "");
			response.setTime(report.get("answerTime") != null ? report.get("answerTime").toString() : "");
			responses.add(response);
		}
		return responses;
	}

	public List<Map<String, Object>> getData(Long categoryId, Long subCategoryId, Long testId) {
		StringBuilder scoreEngine = new StringBuilder((String) queries.get("ScoreEngine"));
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("categoryId", categoryId);
		paramMap.put("subCategoryId", subCategoryId);
		paramMap.put("testId", testId);
		return jdbcTemplate.queryForList(scoreEngine.toString(), paramMap);
	}

	public String generateReportForScoringEngine(XSSFWorkbook workbook, Long id, Date fromdate, Date todate,
			String school, String grade, Long category, Long subCategory) {
		try {
			List<Category> allCategories = new ArrayList<>();
			List<Category> allSubCategories = new ArrayList<>();
			if (category == null) {
				allCategories = categoryService.getCategoryByTest(id);
			} else {
				allCategories.add(categoryService.findOneById(category));
			}
			if (subCategory == null) {
				allSubCategories = categoryService.getSubCategoryByTest(id);
			} else {
				allSubCategories.add(categoryService.findOneById(subCategory));
			}
			List<TestQuestion> testQuestions = testRepository.getOne(id) != null
					? testRepository.getOne(id).getQuestions()
					: new ArrayList<>();
			Set<Long> questionId = new HashSet<>(testQuestions.size());
			testQuestions.forEach(tq -> {
				questionId.add(tq.getQuestion().getId());
			});
			Path path = Paths.get(filedir);
			logger.info(filedir);
			String filename = path.resolve(UUID.randomUUID().toString() + ".csv").toString();
			logger.info("in generateScoringReport 70 fileName " + filename);

			StringBuilder reportHeader = new StringBuilder();
			reportHeader.append(
					"TimeStamp(yyyy-MM-dd HH:mm:ss),TestName,UserName,Name,DOB(yyyy-mm-dd),Gender,Language,UserType,Email,StudentType,Grade,"
							+ "Degree,Stream,InstituteName,Designation,Industry,Organization,Max Score,Total Score,Avg Score,Total Time");
			for (Category c : allCategories) {
				reportHeader.append(",Category-" + c.getName() + "(Total)");
				reportHeader.append(",Category-" + c.getName() + "(Scored)");
				reportHeader.append(",Category-" + c.getName() + "(Mean)");
			}
			for (Category sc : allSubCategories) {
				reportHeader.append(",SubCategory-" + sc.getName() + "(Total)");
				reportHeader.append(",SubCategory-" + sc.getName() + "(Scored)");
				reportHeader.append(",SubCategory-" + sc.getName() + "(Mean)");
			}
			int ic = 0;
			for (Long qid : questionId) {
				reportHeader.append(",Q-" + (ic + 1) + " (id-" + qid + ")");
				ic++;
			}
			StringBuilder scoreEngineCategoryWise = new StringBuilder((String) queries.get("ScoreEngineCategoryWise"));
			StringBuilder scoreEngineUserWise = new StringBuilder((String) queries.get("ScoreEngineUserWise"));
			StringBuilder scoreEngineMarks = new StringBuilder((String) queries.get("ScoreEngineMarks"));
			Map<String, Object> paramMap = new HashMap<>();
			String formattedFromDate = "";
			String formattedToDate = "";
			if ((!StringUtils.isEmpty(fromdate)) && (!StringUtils.isEmpty(todate))) {
				DateFormat writeFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date fdate = DateUtils.addMinutes(fromdate, 0);
				Date tdate = DateUtils.addDays(DateUtils.addMinutes(todate, -330), 2);
				if (fdate != null && tdate != null) {
					formattedFromDate = writeFormat.format(fdate);
					formattedToDate = writeFormat.format(tdate);
				}
				paramMap.put("fromDate", formattedFromDate);
				paramMap.put("toDate", formattedToDate);
				scoreEngineCategoryWise.append(" and tr.test_finish_at between :fromDate  and  :toDate ");
				scoreEngineUserWise.append(" and tr.test_finish_at between :fromDate  and  :toDate ");
				scoreEngineMarks.append(" and tr.test_finish_at between :fromDate  and  :toDate ");
			}
			if (!StringUtils.isEmpty(school)) {
				paramMap.put("school", school);
				scoreEngineCategoryWise.append(" and u.school_name=:school ");
				scoreEngineUserWise.append(" and u.school_name=:school ");
				scoreEngineMarks.append(" and u.school_name=:school ");
			}
			if (!StringUtils.isEmpty(grade)) {
				paramMap.put("grad", grade);
				scoreEngineCategoryWise.append(" and u.grade=:grad");
				scoreEngineUserWise.append(" and u.grade=:grad");
				scoreEngineMarks.append(" and u.grade=:grad");
			}
			paramMap.put("testId", id);
			scoreEngineCategoryWise.append(" order by u.username;");
			scoreEngineUserWise.append(" group by u.username;");
			scoreEngineMarks.append(" group by un.id order by un.question_id;");
			List<Map<String, Object>> testMapList = jdbcTemplate.queryForList(scoreEngineCategoryWise.toString(),
					paramMap);
			List<Map<String, Object>> userList = jdbcTemplate.queryForList(scoreEngineUserWise.toString(), paramMap);
			List<Map<String, Object>> markList = jdbcTemplate.queryForList(scoreEngineMarks.toString(), paramMap);
			int maxMarks = 0;
			int obtainMarks = 0;
			int count = 0;
			Set<Long> set = new HashSet<>();
			FileWriter fileWriter = new FileWriter(filename);
			fileWriter.append(reportHeader.toString());
			fileWriter.append(NEW_LINE_SEPARATOR);
			for (Map<String, Object> report : userList) {
				String upUserName = report.get("username") != null ? String.valueOf(report.get("username")) : "";
				String dateStr = report.get("resultDateTime") != null ? String.valueOf(report.get("resultDateTime"))
						: "";
				String dobStr = report.get("dob") != null ? String.valueOf(report.get("dob")) : "";
				Date date = null;
				String dtformat = "yyyy-MM-dd HH:mm:ss";
				SimpleDateFormat formatter = new SimpleDateFormat(dtformat);
				try {
					date = formatter.parse(dateStr);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				date = DateUtils.addMinutes(date, 330);
				fileWriter.append(formatter.format(date));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf('"'))
						.append(report.get("testName") != null ? String.valueOf(report.get("testName")) : "")
						.append(String.valueOf('"'));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("username") != null ? String.valueOf(report.get("username")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf('"'))
						.append(report.get("name") != null ? String.valueOf(report.get("name")) : "")
						.append(String.valueOf('"'));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(dobStr.split(" ")[0]);
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("gender") != null ? String.valueOf(report.get("gender")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("language") != null ? String.valueOf(report.get("language")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("user_type") != null ? String.valueOf(report.get("user_type")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("email") != null ? String.valueOf(report.get("email")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("student_type") != null ? String.valueOf(report.get("student_type")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("grade") != null ? String.valueOf(report.get("grade")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("degree") != null ? String.valueOf(report.get("degree")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("stream") != null ? String.valueOf(report.get("stream")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf('"')).append(
						report.get("institute_name") != null ? String.valueOf(report.get("institute_name")) : "")
						.append(String.valueOf('"'));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("designation") != null ? String.valueOf(report.get("designation")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("industry") != null ? String.valueOf(report.get("industry")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("org_name") != null ? String.valueOf(report.get("org_name")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("maxMarks") != null ? String.valueOf(report.get("maxMarks")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("marks") != null ? String.valueOf(report.get("marks")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("mean") != null ? String.valueOf(report.get("mean")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("answerTime") != null ? String.valueOf(report.get("answerTime")) : "");
				for (Category c : allCategories) {
					for (Map<String, Object> subReport : testMapList) {
						String catName = subReport.get("parentCategoryName") != null
								? subReport.get("parentCategoryName").toString()
								: "";
						String downUserName = subReport.get("username") != null
								? String.valueOf(subReport.get("username"))
								: "";
						Long qid = subReport.get("id") != null ? Long.valueOf(subReport.get("id").toString()) : 0l;
						if (catName.equals(c.getName()) && upUserName.equals(downUserName)) {
							if (set.add(qid)) {
								int m = subReport.get("max_marks") != null
										? Integer.parseInt(subReport.get("max_marks").toString())
										: 0;
								int om = subReport.get("marks") != null
										? Integer.parseInt(subReport.get("marks").toString())
										: 0;
								maxMarks = maxMarks + m;
								obtainMarks = obtainMarks + om;
								count++;
							}
						}
					}
					fileWriter.append(COMMA_DELIMITER);
					fileWriter.append(String.valueOf(maxMarks));
					fileWriter.append(COMMA_DELIMITER);
					fileWriter.append(String.valueOf(obtainMarks));
					// createCellForInt(row, maxMarks, cellnum++, style);
					// createCellForInt(row, obtainMarks, cellnum++, style);
					Double meanValue = Double.valueOf(obtainMarks) / Double.valueOf(count);
					fileWriter.append(COMMA_DELIMITER);
					fileWriter.append(String.valueOf(meanValue));
					// createCell(row, meanValue.toString(), cellnum++, style);
					set.clear();
					maxMarks = 0;
					obtainMarks = 0;
					count = 0;
				}
				for (Category c : allSubCategories) {
					for (Map<String, Object> subReport : testMapList) {
						String catName = subReport.get("subcategoryName") != null
								? subReport.get("subcategoryName").toString()
								: "";
						String downUserName = subReport.get("username") != null
								? String.valueOf(subReport.get("username"))
								: "";
						Long qid = subReport.get("id") != null ? Long.valueOf(subReport.get("id").toString()) : 0l;
						if (catName.equals(c.getName()) && upUserName.equals(downUserName)) {
							if (set.add(qid)) {
								int m = subReport.get("max_marks") != null
										? Integer.parseInt(subReport.get("max_marks").toString())
										: 0;
								int om = subReport.get("marks") != null
										? Integer.parseInt(subReport.get("marks").toString())
										: 0;
								maxMarks = maxMarks + m;
								obtainMarks = obtainMarks + om;
								count++;
							}
						}
					}
					fileWriter.append(COMMA_DELIMITER);
					fileWriter.append(String.valueOf(maxMarks));
					fileWriter.append(COMMA_DELIMITER);
					fileWriter.append(String.valueOf(obtainMarks));
					// createCellForInt(row, maxMarks, cellnum++, style);
					// createCellForInt(row, obtainMarks, cellnum++, style);
					Double meanValue = Double.valueOf(obtainMarks) / Double.valueOf(count);
					fileWriter.append(COMMA_DELIMITER);
					fileWriter.append(String.valueOf(meanValue));
					// createCell(row, meanValue.toString(), cellnum++, style);
					set.clear();
					maxMarks = 0;
					obtainMarks = 0;
					count = 0;
				}
				for (Map<String, Object> markReport : markList) {
					String downUserName = markReport.get("username") != null
							? String.valueOf(markReport.get("username"))
							: "";
					if (upUserName.equals(downUserName)) {
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(
								markReport.get("marks") != null ? String.valueOf(markReport.get("marks").toString())
										: "");
						// createCellForInt(row, markReport.get("marks") != null ?
						// Integer.parseInt(markReport.get("marks").toString()) : 0, cellnum++, style);
					}
				}
				fileWriter.append(NEW_LINE_SEPARATOR);
			}
			logger.info(filename);
			fileWriter.flush();
			fileWriter.close();
			return filename;
		} catch (IOException e) {
			logger.info("Error in generating Scoring Summary Report", e);
		}
		return null;
	}

	public String generateReportForCategoryWiseScore(XSSFWorkbook workbook, Long id, Date fromdate, Date todate,
			String school, String grade, Long category, Long subCategory) {
		try {
			List<Category> allCategories = new ArrayList<>();
			List<Category> allSubCategories = new ArrayList<>();
			if (category == null) {
				allCategories = categoryService.getCategoryByTest(id);
			} else {
				allCategories.add(categoryService.findOneById(category));
			}
			if (subCategory == null) {
				allSubCategories = categoryService.getSubCategoryByTest(id);
			} else {
				allSubCategories.add(categoryService.findOneById(subCategory));
			}
			Path path = Paths.get(filedir);
			logger.info(filedir);
			String filename = path.resolve(UUID.randomUUID().toString() + ".csv").toString();
			logger.info("in generateScoringReport 70 fileName " + filename);

			StringBuilder reportHeader = new StringBuilder();
			reportHeader.append("TestName,UserName,NumberOfItems,Total Score,Avg Score,TotalTime");
			for (Category c : allCategories) {
				// reportHeader.append(",Category-" + c.getName() + "(Total)");
				reportHeader.append(",Category-" + c.getName() + "(Scored)");
				reportHeader.append(",Category-" + c.getName() + "(Mean)");
			}
			for (Category sc : allSubCategories) {
				// reportHeader.append(",SubCategory-" + sc.getName() + "(Total)");
				reportHeader.append(",SubCategory-" + sc.getName() + "(Scored)");
				reportHeader.append(",SubCategory-" + sc.getName() + "(Mean)");
			}
			StringBuilder scoreEngineCategoryWise = new StringBuilder((String) queries.get("ScoreEngineCategoryWise"));
			StringBuilder scoreEngineUserWise = new StringBuilder((String) queries.get("ScoreEngineUserWise"));
			Map<String, Object> paramMap = new HashMap<>();
			String formattedFromDate = "";
			String formattedToDate = "";
			if ((!StringUtils.isEmpty(fromdate)) && (!StringUtils.isEmpty(todate))) {
				DateFormat writeFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date fdate = DateUtils.addMinutes(fromdate, 0);
				Date tdate = DateUtils.addDays(DateUtils.addMinutes(todate, -330), 2);
				if (fdate != null && tdate != null) {
					formattedFromDate = writeFormat.format(fdate);
					formattedToDate = writeFormat.format(tdate);
				}
				paramMap.put("fromDate", formattedFromDate);
				paramMap.put("toDate", formattedToDate);
				scoreEngineCategoryWise.append(" and tr.test_finish_at between :fromDate  and  :toDate ");
				scoreEngineUserWise.append(" and tr.test_finish_at between :fromDate  and  :toDate ");
			}
			if (!StringUtils.isEmpty(school)) {
				paramMap.put("school", school);
				scoreEngineCategoryWise.append(" and u.school_name=:school ");
				scoreEngineUserWise.append(" and u.school_name=:school ");
			}
			if (!StringUtils.isEmpty(grade)) {
				paramMap.put("grad", grade);
				scoreEngineCategoryWise.append(" and u.grade=:grad");
				scoreEngineUserWise.append(" and u.grade=:grad");
			}
			paramMap.put("testId", id);
			scoreEngineCategoryWise.append(" order by u.username;");
			scoreEngineUserWise.append(" group by u.username;");
			List<Map<String, Object>> testMapList = jdbcTemplate.queryForList(scoreEngineCategoryWise.toString(),
					paramMap);
			List<Map<String, Object>> userList = jdbcTemplate.queryForList(scoreEngineUserWise.toString(), paramMap);
			int maxMarks = 0;
			int obtainMarks = 0;
			int count = 0;
			Set<Long> set = new HashSet<>();
			FileWriter fileWriter = new FileWriter(filename);
			fileWriter.append(reportHeader.toString());
			fileWriter.append(NEW_LINE_SEPARATOR);
			for (Map<String, Object> report : userList) {
				String upUserName = report.get("username") != null ? String.valueOf(report.get("username")) : "";
				fileWriter.append(String.valueOf('"'))
						.append(report.get("testName") != null ? String.valueOf(report.get("testName")) : "")
						.append(String.valueOf('"'));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("username") != null ? String.valueOf(report.get("username")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("rows") != null ? String.valueOf(report.get("rows")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("marks") != null ? String.valueOf(report.get("marks")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("mean") != null ? String.valueOf(report.get("mean")) : "");
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(report.get("answerTime") != null ? String.valueOf(report.get("answerTime")) : "");
				for (Category c : allCategories) {
					for (Map<String, Object> subReport : testMapList) {
						String catName = subReport.get("parentCategoryName") != null
								? subReport.get("parentCategoryName").toString()
								: "";
						String downUserName = subReport.get("username") != null
								? String.valueOf(subReport.get("username"))
								: "";
						Long qid = subReport.get("id") != null ? Long.valueOf(subReport.get("id").toString()) : 0l;
						if (catName.equals(c.getName()) && upUserName.equals(downUserName)) {
							if (set.add(qid)) {
								int om = subReport.get("marks") != null
										? Integer.parseInt(subReport.get("marks").toString())
										: 0;
								obtainMarks = obtainMarks + om;
								count++;
							}
						}
					}
					fileWriter.append(COMMA_DELIMITER);
					fileWriter.append(String.valueOf(obtainMarks));
					Double meanValue = Double.valueOf(obtainMarks) / Double.valueOf(count);
					fileWriter.append(COMMA_DELIMITER);
					fileWriter.append(String.valueOf(meanValue));
					set.clear();
					// maxMarks = 0;
					obtainMarks = 0;
					count = 0;
				}
				for (Category c : allSubCategories) {
					for (Map<String, Object> subReport : testMapList) {
						String catName = subReport.get("subcategoryName") != null
								? subReport.get("subcategoryName").toString()
								: "";
						String downUserName = subReport.get("username") != null
								? String.valueOf(subReport.get("username"))
								: "";
						Long qid = subReport.get("id") != null ? Long.valueOf(subReport.get("id").toString()) : 0l;
						if (catName.equals(c.getName()) && upUserName.equals(downUserName)) {
							if (set.add(qid)) {
								int om = subReport.get("marks") != null
										? Integer.parseInt(subReport.get("marks").toString())
										: 0;
								obtainMarks = obtainMarks + om;
								count++;
							}
						}
					}
					fileWriter.append(COMMA_DELIMITER);
					fileWriter.append(String.valueOf(obtainMarks));
					Double meanValue = Double.valueOf(obtainMarks) / Double.valueOf(count);
					fileWriter.append(COMMA_DELIMITER);
					fileWriter.append(String.valueOf(meanValue));
					set.clear();
					// maxMarks = 0;
					obtainMarks = 0;
					count = 0;
				}
				fileWriter.append(NEW_LINE_SEPARATOR);
			}
			logger.info(filename);
			fileWriter.flush();
			fileWriter.close();
			return filename;
		} catch (IOException e) {
			logger.info("Error in generating Scoring Summary Report", e);
		}
		return null;
	}

	protected CellStyle headerCellStyle(Workbook workbook) {
		XSSFFont font = (XSSFFont) workbook.createFont();
		font.setBold(true);
		CellStyle style = workbook.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setBorderBottom(CellStyle.BORDER_THIN);
		style.setBorderLeft(CellStyle.BORDER_THIN);
		style.setBorderRight(CellStyle.BORDER_THIN);
		style.setBorderTop(CellStyle.BORDER_THIN);
		style.setFont(font);
		style.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		return style;
	}

	public void createCell(Row row, String value, int cellnumber, CellStyle cellStyle) {
		Cell cell = row.createCell(cellnumber);
		cell.setCellValue(value);
		cell.setCellStyle(cellStyle);
	}

	public void createCellForInt(Row row, int value, int cellnumber, CellStyle cellStyle) {
		Cell cell = row.createCell(cellnumber);
		cell.setCellValue(value);
		cell.setCellStyle(cellStyle);
	}

	private TestResult getTestResult(User user, Long testId) {
		List<TestResult> testResults = testResultRepository.findAllByUserAndTestId(user, testId);
		if (testResults.size() > 1) {
			testResults.sort((a, b) -> (b.getId().intValue() - a.getId().intValue()));
			return testResults.get(0);

		}
		if (testResults.size() == 1) {
			return testResults.get(0);
		}
		return null;
	}

	private List<Question> getQuestionsByCategoryAndTest(Category category, Long testId) {
		List<TestQuestion> testQuestionList = testQuestionRepository.findAllByCategoryAndTestIdAndTenant(category,
				testId, getTenant());
		List<Question> questions = new ArrayList<>();
		testQuestionList.forEach(testQuestion -> questions.add(testQuestion.getQuestion()));
		return questions;
	}

	private List<Question> getQuestionsBySubCategoryAndTest(Category category, Long testId) {
		List<TestQuestion> testQuestionList = testQuestionRepository.findAllBySubCategoryAndTestIdAndTenant(category,
				testId, getTenant());
		List<Question> questions = new ArrayList<>();
		testQuestionList.forEach(testQuestion -> questions.add(testQuestion.getQuestion()));
		return questions;
	}

	private Float getUserAnswersOfQuestions(TestResult testResult, List<Question> questionList) {
		List<UserAnswer> userAnswerList = getUserAnswersOfQuestionsV1(testResult, questionList);

		return getAverageScoreFromUserAnswers(userAnswerList);
	}

	private Float getAverageScoreFromUserAnswers(List<UserAnswer> userAnswerList) {

		Float sumOfObtainedMarks = 0.0f;
		Float sumOfMaximumMarks = 0.0f;
		for (UserAnswer userAnswer : userAnswerList) {
			sumOfObtainedMarks = userAnswer.getMarks() + sumOfObtainedMarks;
			sumOfMaximumMarks = userAnswer.getMaxMarks() + sumOfMaximumMarks;
		}
		return sumOfObtainedMarks;
	}

	private List<BigDecimal> getCategoryScore(TestResult testResult, Category category, List<Question> questionList,
			Map<Long, CategoryResponse> mapOfCategoryIdAndMergedCategoriesAndSubCategories, String reportType,
			HashMap<Long, String> questionIdAndTextAnswers) {
		BigDecimal rawScore = new BigDecimal(0);
		BigDecimal percentile;
		List<BigDecimal> scores = new ArrayList<>(2);
		HashMap<String, BigDecimal> obtainedScores = new HashMap<>();
		if (category.getParent() == null)// for category
		{
//      logger.info("STATRING Calculation of catgory " + category.getName() + "by its subcategories");
			obtainedScores = calculateRawScoreForCategory(category, mapOfCategoryIdAndMergedCategoriesAndSubCategories,
					reportType, obtainedScores);
//      logger.info("SCORE CALCULATED:" + category.getName() + rawScore.toPlainString());
//            percentile = categoryMatrixCalculator.getPercentileByCategory(category, rawScore, getUser().getGrade());
		} else // for subCategory
		{
			List<UserAnswer> userAnswerList = getUserAnswersOfQuestionsV1(testResult, questionList);
			obtainedScores = calculateRawScoreForSubCategory(userAnswerList, category, reportType,
					questionIdAndTextAnswers, obtainedScores);
		}

		scores.add(obtainedScores.get("percentageRawScores"));
		scores.add(obtainedScores.get("dislikeAndStronglyDislikeFrequency"));
		scores.add(obtainedScores.get("likeAndStronglyLikeFrequency"));

		return scores;
	}

	private List<UserAnswer> getUserAnswersOfQuestionsV1(TestResult testResult, List<Question> questionList) {
		List<UserAnswer> userAnswerList = new ArrayList<>(questionList.size());
		questionList.forEach(question -> {
			UserAnswer userAnswer = userAnswerRepository.findByTestResultAndQuestionAndTenant(testResult, question,
					getTenant());
			if (userAnswer != null) {
				userAnswerList.add(userAnswer);
			}
		});
		return userAnswerList;
	}

	private HashMap<String, BigDecimal> calculateRawScoreForSubCategory(List<UserAnswer> userAnswerList,
			Category category, String reportType, HashMap<Long, String> questionIdAndTextAnswers,
			HashMap<String, BigDecimal> obtainedScores) {

		Integer sumOfObtainedMarks = 0;
		Integer sumOfMaximumMarks = 0;
		Integer sumOfMinimumMarks = 0;
		Integer dislikeAndStronglyDislikeFrequency = 0;
		Integer likeAndStronglyLikeFrequency = 0;
		BigDecimal percentageRawScores = new BigDecimal(0);

		for (UserAnswer userAnswer : userAnswerList) {
			if (userAnswer.getQuestion().getQuestionType().equalsIgnoreCase("Text")) {
				if (questionIdAndTextAnswers.get(userAnswer.getQuestion().getId())
						.equalsIgnoreCase(userAnswer.getText())) {
					userAnswer.setMarks(userAnswer.getMaxMarks());
				} else {
					userAnswer.setMarks(userAnswer.getMinMarks());
				}
			}
			sumOfObtainedMarks = userAnswer.getMarks() + sumOfObtainedMarks;
			sumOfMaximumMarks = userAnswer.getMaxMarks() + sumOfMaximumMarks;
			sumOfMinimumMarks = userAnswer.getMinMarks() + sumOfMinimumMarks;

			if (userAnswer.getOptionNumber() == 1 || userAnswer.getOptionNumber() == 2) {
				dislikeAndStronglyDislikeFrequency++;
			}
			if (userAnswer.getOptionNumber() == 4 || userAnswer.getOptionNumber() == 5) {
				likeAndStronglyLikeFrequency++;
			}
		}

		switch (reportType) {
		case "TALENT":
			percentageRawScores = BigDecimal.valueOf(sumOfObtainedMarks * 5)
					.divide(BigDecimal.valueOf(sumOfMaximumMarks), 10, RoundingMode.HALF_EVEN);
			break;
		case "INTSCALE":
		default:
//        logger.info(category.getName() + "'s sumOfObtainedMarks: " + sumOfObtainedMarks + " dividedBy sumOfMaximumMarks: " + sumOfMaximumMarks);
			percentageRawScores = BigDecimal.valueOf((sumOfObtainedMarks - sumOfMinimumMarks) * 100)
					.divide(BigDecimal.valueOf(sumOfMaximumMarks - sumOfMinimumMarks), 2, RoundingMode.HALF_EVEN);
//        logger.info(" = percentage-rawScores: " + percentageRawScores.toPlainString());
			break;
		}

		obtainedScores.put("percentageRawScores", percentageRawScores);
		obtainedScores.put("dislikeAndStronglyDislikeFrequency",
				BigDecimal.valueOf(dislikeAndStronglyDislikeFrequency));
		obtainedScores.put("likeAndStronglyLikeFrequency", BigDecimal.valueOf(likeAndStronglyLikeFrequency));

		return obtainedScores;

	}

	private HashMap<String, BigDecimal> calculateRawScoreForCategory(Category category,
			Map<Long, CategoryResponse> mapOfCategoryIdAndMergedCategoriesAndSubCategories, String reportType,
			HashMap<String, BigDecimal> obtainedScores) {
		BigDecimal sumOfRawScores = new BigDecimal(0);
		BigDecimal percentageRawScores = new BigDecimal(0);
		BigDecimal dislikeAndStronglyDislikeFrequency = new BigDecimal(0);
		BigDecimal likeAndStronglyLikeFrequency = new BigDecimal(0);

		for (CategoryResponse subCategory : mapOfCategoryIdAndMergedCategoriesAndSubCategories.get(category.getId())
				.getSubCategories()) {
			sumOfRawScores = subCategory.getRawScore().add(sumOfRawScores);
			dislikeAndStronglyDislikeFrequency = subCategory.getDislikeAndStronglyDislikeFrequency()
					.add(dislikeAndStronglyDislikeFrequency);
			likeAndStronglyLikeFrequency = subCategory.getLikeAndStronglyLikeFrequency()
					.add(likeAndStronglyLikeFrequency);

//      logger.info("subcate-------- " + subCategory.getName() + ":" + subCategory.getRawScore());
		}

		switch (reportType) {
		case "TALENT":
		case "INTSCALE":
		default:
			percentageRawScores = sumOfRawScores.divide(BigDecimal.valueOf(
					mapOfCategoryIdAndMergedCategoriesAndSubCategories.get(category.getId()).getSubCategories().size()),
					2, RoundingMode.HALF_EVEN);
		}

		obtainedScores.put("percentageRawScores", percentageRawScores);
		obtainedScores.put("dislikeAndStronglyDislikeFrequency", dislikeAndStronglyDislikeFrequency);
		obtainedScores.put("likeAndStronglyLikeFrequency", likeAndStronglyLikeFrequency);
		return obtainedScores;

	}

	public Map<String, Float> getIndividualReport(Long testId) {
		Map<String, Float> finalMapOfCategoryAndItsObtainedMarksAverage = new HashMap<>();
		Map<Category, List<Question>> mapOfCategoryAndItsQuestions = new HashMap<>();
		List<Long> userAnsweredQuestionIds = new ArrayList<>();
		List<Long> questionIdsInTest = new ArrayList<>();
		Test test = testRepository.findOne(testId);
		TestResult testResult = getTestResult(getUser(), testId);
		List<UserAnswer> userAnswers = userAnswerRepository.findAllByTestResultAndTenant(testResult, getTenant());// getting
																													// all
																													// questions
																													// responded
																													// by
																													// user
		userAnswers.forEach(userAnswer -> userAnsweredQuestionIds.add(userAnswer.getQuestion().getId())); // getting
																											// only the
																											// ids
		test.getQuestions().forEach(tQ -> questionIdsInTest.add(tQ.getQuestion().getId())); // getting question ids of
																							// questions present in test
		try {
			questionIdsInTest.retainAll(userAnsweredQuestionIds);
		} catch (NullPointerException e) {
			logger.error("Empty question list");
		}

		List<TestQuestion> finalTestQuestions = new ArrayList<>();

		questionIdsInTest.forEach(queId -> finalTestQuestions
				.add(testQuestionRepository.findByTestIdAndQuestionIdAndTenant(testId, queId, getTenant())));

		List<Category> categories = categoryService.getCategoryByTest(testId);

		categories.forEach(category -> mapOfCategoryAndItsQuestions.put(category,
				getQuestionsByCategoryAndTest(category, testId)));

		mapOfCategoryAndItsQuestions.forEach((key, value) -> finalMapOfCategoryAndItsObtainedMarksAverage
				.put(key.getName(), getUserAnswersOfQuestions(testResult, value)));

		return finalMapOfCategoryAndItsObtainedMarksAverage;
	}

	public List<CategoryResponse> getIndividualReportV1(Long testId, Long userId) {
//****************    Initialization  starts  ************//
		User user = null;
		if (StringUtils.isEmpty(userId) || userId == 0) {
			user = getUser();
		} else {
			user = userService.getOneById(userId);
		}
		Map<Category, List<Question>> mapOfCategoryAndItsQuestions = new HashMap<>();
		Map<Category, List<Question>> mapOfSubCategoryAndItsQuestions = new HashMap<>();
		List<Long> userAnsweredQuestionIds = new ArrayList<>();
		List<Long> questionIdsInTest = new ArrayList<>();
		List<TestQuestion> finalTestQuestions = new ArrayList<>();

		/* start -***********Getting the answer key for test-based questions ********/

		HashMap<Long, String> questionIdAndTextAnswers = new HashMap<>();

		List<TextquestionAnswers> textquestionAnswers = textquestionAnswersRepository.findAllByTenant(getTenant());
		textquestionAnswers.forEach(textQue -> {
			questionIdAndTextAnswers.put(textQue.getQuestionId(), textQue.getTextAnswer());
		});

		/* ending - *************/

//****************    Initialization ends   ***************//

		Test test = testRepository.findOne(testId); // getting taken test
		List<Category> categories = categoryService.getCategoryByTest(testId); // get categories
		List<Category> subCategories = categoryService.getSubCategoryByTest(testId); // get subCategories
		List<CategoryResponse> mergedCategoriesAndSubCategoriesList = categoryService
				.mergeCategoriesandSubCategories(categories, subCategories); // merging categories and subCategories
		TestResult testResult = getTestResult(user, testId); // getting user testTaken details
		List<UserAnswer> userAnswers = userAnswerRepository.findAllByTestResultAndTenant(testResult, getTenant());// getting
																													// all
																													// Questions
																													// responded
																													// by
																													// User
		userAnswers.forEach(userAnswer -> userAnsweredQuestionIds.add(userAnswer.getQuestion().getId())); // getting the
																											// QuestionIds
																											// of
																											// Questions
																											// responded
																											// by User
		test.getQuestions().forEach(tQ -> questionIdsInTest.add(tQ.getQuestion().getId())); // getting question ids of
																							// questions present in test

		try // filtering the Questions and selecting only User responded Questions
		{
			questionIdsInTest.retainAll(userAnsweredQuestionIds);
		} catch (NullPointerException e) {
			logger.error("Empty question list");
		}

		questionIdsInTest.forEach(queId -> finalTestQuestions
				.add(testQuestionRepository.findByTestIdAndQuestionIdAndTenant(testId, queId, getTenant()))); // getting
																												// the
																												// derived
																												// testQuestions

		Map<Long, CategoryResponse> mapOfCategoryIdAndMergedCategoriesAndSubCategories = mergedCategoriesAndSubCategoriesList
				.stream().collect(Collectors.toMap(CategoryResponse::getId, categoryResponse -> categoryResponse));

		categories.forEach(category -> mapOfCategoryAndItsQuestions.put(category,
				getQuestionsByCategoryAndTest(category, testId))); // making a map of Category and its Questions

		subCategories.forEach(subCategory -> mapOfSubCategoryAndItsQuestions.put(subCategory,
				getQuestionsBySubCategoryAndTest(subCategory, testId))); // making a map of subCategory and its
																			// Questions

		mapOfSubCategoryAndItsQuestions.forEach((Category key, List<Question> value) -> {
			setCategoryScoreInDataMap(testResult, mapOfCategoryIdAndMergedCategoriesAndSubCategories, key, value,
					"INTSCALE", questionIdAndTextAnswers);
		});
		mapOfCategoryAndItsQuestions.forEach((Category key, List<Question> value) -> {
			setCategoryScoreInDataMap(testResult, mapOfCategoryIdAndMergedCategoriesAndSubCategories, key, value,
					"INTSCALE", questionIdAndTextAnswers);
		});

//      finalMapOfSubCategoryAndItsScore
//          .put(key.getId(),
//               getCategoryScore(testResult, key, value, finalMapOfSubCategoryAndItsScore, mergedCategoriesAndSubCategoriesList));
//    mapOfCategoryAndItsQuestions.forEach((key, value) -> finalMapOfCategoryAndItsScore
//        .put(key.getId(), getCategoryScore(testResult, key, value, finalMapOfSubCategoryAndItsScore, mergedCategoriesAndSubCategoriesList)));

		mergedCategoriesAndSubCategoriesList = new ArrayList<>(
				mapOfCategoryIdAndMergedCategoriesAndSubCategories.values());

		return mergedCategoriesAndSubCategoriesList;
	}

	private void setCategoryScoreInDataMap(TestResult testResult,
			Map<Long, CategoryResponse> mapOfCategoryIdAndMergedCategoriesAndSubCategories, Category category,
			List<Question> questions, String reportType, HashMap<Long, String> questionIdAndTextAnswers) {
		List<BigDecimal> scores = getCategoryScore(testResult, category, questions,
				mapOfCategoryIdAndMergedCategoriesAndSubCategories, reportType, questionIdAndTextAnswers);

		if (category.getParent() == null) {
			/* code for category */
			mapOfCategoryIdAndMergedCategoriesAndSubCategories.get(category.getId()).getRawScores()
					.add((scores.get(0)));
			mapOfCategoryIdAndMergedCategoriesAndSubCategories.get(category.getId())
					.setDislikeAndStronglyDislikeFrequency(scores.get(1));
			mapOfCategoryIdAndMergedCategoriesAndSubCategories.get(category.getId())
					.setLikeAndStronglyLikeFrequency(scores.get(2));
//      logger.info("Adding score to Category rawScoresList " + category.getName() + ":" + scores.get(0));
//            mapOfCategoryIdAndMergedCategoriesAndSubCategories.get(category.getId()).setPercentile(scores.get(1));
		} else {
			/* code for subcategory */
			mapOfCategoryIdAndMergedCategoriesAndSubCategories.get(category.getParent().getId()).getSubCategories()
					.forEach((CategoryResponse subcategory) -> {
						if (subcategory.getId().equals(category.getId())) {
							subcategory.setRawScore(scores.get(0));
							subcategory.setDislikeAndStronglyDislikeFrequency(scores.get(1));
							subcategory.setLikeAndStronglyLikeFrequency(scores.get(2));
//                    subcategory.setPercentile(scores.get(1));
						}
					});
		}
	}

	public File downloadAimReport(Long id, Tenant tenant, AimReportType reportType)
			throws FileNotFoundException, IOException {
		if (tenant == null) {
			tenant = getTenant();
		}

		User user = userService.getOneById(id);

		boolean isAllowed = false;
		if (reportType.equals(AimReportType.DEMO)) {
			isAllowed = allowReportToUser(user, "Aim Report Demo", tenant);
		} else {
			isAllowed = allowReportToUser(user, "Aim Composite Report", tenant);
		}

		if (!isAllowed) {
			return null;
		}

		String url = intscaleBaseUrl + "report/index.html/#!/pdfreport/aimReport";
		String fileName = user.getUsername() + "_" + user.getName() + ".pdf";
		if (!StringUtils.isEmpty(reportType)) {
			if (reportType.equals(AimReportType.MINI)) {
				url = intscaleBaseUrl + "report/index.html/#!/pdfreport/aimReportMini";
				fileName = user.getUsername() + "_" + user.getName() + "MINI.pdf";
			}
			if (reportType.equals(AimReportType.DEMO)) {
				url = intscaleBaseUrl + "report/index.html/#!/pdfreport/aimReportDemo";
				fileName = user.getUsername() + "_" + user.getName() + "DEMO.pdf";
			}

		}

		File outputPdfFile = new File(tempDirPath + "/" + fileName);
		FileOutputStream out = new FileOutputStream(outputPdfFile);

		String concat = url.concat("/" + id).concat("/" + getToken(tenant));

		String body = String.format("{\"url\":\"%s\"}", concat);

		Request request = new Request.Builder().url(pdfServerUrl)
				.post(RequestBody.create(MediaType.parse("application/json"), body)).build();

		Response response = null;
		try {
			try {
				logger.info(String.format("URL: %s, Body: %s", pdfServerUrl, body));
				response = okHttpClient.newCall(request).execute();
				if (!response.isSuccessful()) {
					logger.error("IntscalePdfServer request failed");
					return null;
				}
				out.write(response.body().bytes());
			} catch (Exception ex) {
				logger.error(ex.getMessage());
				return null;
			} finally {
				if (out != null) {
					out.flush();
					out.close();
					if (response != null) {
						if (response.body() != null)
							response.body().close();
					}
				}
			}
		} catch (ConnectException e) {
			logger.error("coudn't connect with " + pdfServerUrl + " to generate pdf.", e);
			return null;
		} catch (IOException e) {
			logger.error("fail to generate pdf.", e);
			return null;
		}
		return outputPdfFile;
	}

	private String getToken(Tenant tenant) {
		User user = userRepository.findByUsernameAndTenant("vikumarsing", tenant);

		String roleName = "NonAdmin";
		// String roleName = "TestTaker";
		if (user.getRoles().contains(userService.getOneRoleByName("Admin"))) {
			roleName = "Admin";
		}
		return Jwts.builder().setSubject(user.getEmail()).claim("assets_url", intscale_assets).setIssuedAt(new Date())
				.setExpiration(new DateTime().plusHours(1).toDate()).claim("dashboardType", user.getDashBoardType())
				.claim("userType", roleName).claim("hostName", tenant.getSubDomain())
				.signWith(SignatureAlgorithm.HS512, secretkey).compact();

	}

	public String getTestNameByTestId(Long id) {
		return testRepository.findOne(id).getName();
	}

	public String getUserNameByUserId(Long id) {
		return userRepository.findOne(id).getUsername();
	}

	public User getUser(Long id) {
		return userRepository.findOne(id);
	}

	public String generatedUserTestAssignmentSheet(List<Long> ids) throws IOException {
		List<UserTest> userTests = userTestRepository.findAllByIdInAndTenant(ids, getTenant());
		Path path = Paths.get(filedir);
		logger.info(filedir);
		String filename = path.resolve(UUID.randomUUID().toString() + ".csv").toString();
		logger.info("in generateTestReport 70 fileName " + filename);
		FileWriter fileWriter = new FileWriter(filename);
		fileWriter.append(USERTEST_ASSIGNMENT_FILE_HEADER.toString());
		fileWriter.append(NEW_LINE_SEPARATOR);
		for (UserTest userTest : userTests) {
			fileWriter.append(userTest.getUserEmail() != null ? userTest.getUserEmail().getUsername() : "");
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(String.valueOf('"'))
					.append(userTest.getUserEmail() != null ? userTest.getUserEmail().getSchoolName() : "")
					.append(String.valueOf('"'));
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(userTest.getUserEmail() != null ? userTest.getUserEmail().getGrade() : "");
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(userTest.getUserEmail() != null ? userTest.getUserEmail().getGender() : "");
			fileWriter.append(COMMA_DELIMITER);
			StringBuilder testNames = new StringBuilder();
			userTest.getTests().forEach(test -> {
				testNames.append(test.getName() + ",");
			});
			fileWriter.append(String.valueOf('"')).append(testNames).append(String.valueOf('"'));
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(userTest.getTestStatus() != null ? userTest.getTestStatus() : "");
			fileWriter.append(NEW_LINE_SEPARATOR);
		}
		fileWriter.flush();
		fileWriter.close();
		return filename;
	}

	public List<CategoryResponse> getTalentReport(Long testId, Long userId) {
//****************    Initialization  starts  ************//
		User user = null;
		if (StringUtils.isEmpty(userId) || userId == 0) {
			user = getUser();
		} else {
			user = userService.getOneById(userId);
		}
		Map<Category, List<Question>> mapOfCategoryAndItsQuestions = new HashMap<>();
		Map<Category, List<Question>> mapOfSubCategoryAndItsQuestions = new HashMap<>();
		List<Long> userAnsweredQuestionIds = new ArrayList<>();
		List<Long> questionIdsInTest = new ArrayList<>();
		List<TestQuestion> finalTestQuestions = new ArrayList<>();

		/* start -***********Getting the answer key for test-based questions ********/

		HashMap<Long, String> questionIdAndTextAnswers = new HashMap<>();

		List<TextquestionAnswers> textquestionAnswers = textquestionAnswersRepository.findAllByTenant(getTenant());
		textquestionAnswers.forEach(textQue -> {
			questionIdAndTextAnswers.put(textQue.getQuestionId(), textQue.getTextAnswer());
		});

		/* ending - *************/

//****************    Initialization ends   ***************//

		Test test = testRepository.findOne(testId); // getting taken test
		List<Category> categories = categoryService.getCategoryByTest(testId); // get categories
		List<Category> subCategories = categoryService.getSubCategoryByTest(testId); // get subCategories
		List<CategoryResponse> mergedCategoriesAndSubCategoriesList = categoryService
				.mergeCategoriesandSubCategories(categories, subCategories); // merging categories and subCategories
		TestResult testResult = getTestResult(user, testId); // getting user testTaken details
		List<UserAnswer> userAnswers = userAnswerRepository.findAllByTestResultAndTenant(testResult, getTenant());// getting
																													// all
																													// Questions
																													// responded
																													// by
																													// User
		userAnswers.forEach(userAnswer -> userAnsweredQuestionIds.add(userAnswer.getQuestion().getId())); // getting the
																											// QuestionIds
																											// of
																											// Questions
																											// responded
																											// by User
		test.getQuestions().forEach(tQ -> questionIdsInTest.add(tQ.getQuestion().getId())); // getting question ids of
																							// questions present in test

		try // filtering the Questions and selecting only User responded Questions
		{
			questionIdsInTest.retainAll(userAnsweredQuestionIds);
		} catch (NullPointerException e) {
			logger.error("Empty question list");
		}

		questionIdsInTest.forEach(queId -> finalTestQuestions
				.add(testQuestionRepository.findByTestIdAndQuestionIdAndTenant(testId, queId, getTenant()))); // getting
																												// the
																												// derived
																												// testQuestions

		Map<Long, CategoryResponse> mapOfCategoryIdAndMergedCategoriesAndSubCategories = mergedCategoriesAndSubCategoriesList
				.stream().collect(Collectors.toMap(CategoryResponse::getId, categoryResponse -> categoryResponse));

		categories.forEach(category -> mapOfCategoryAndItsQuestions.put(category,
				getQuestionsByCategoryAndTest(category, testId))); // making a map of Category and its Questions

		subCategories.forEach(subCategory -> mapOfSubCategoryAndItsQuestions.put(subCategory,
				getQuestionsBySubCategoryAndTest(subCategory, testId))); // making a map of subCategory and its
																			// Questions

		mapOfSubCategoryAndItsQuestions.forEach((Category key, List<Question> value) -> {
			setCategoryScoreInDataMap(testResult, mapOfCategoryIdAndMergedCategoriesAndSubCategories, key, value,
					"TALENT", questionIdAndTextAnswers);
		});

		mapOfCategoryAndItsQuestions.forEach((Category key, List<Question> value) -> {
			setCategoryScoreInDataMap(testResult, mapOfCategoryIdAndMergedCategoriesAndSubCategories, key, value,
					"TALENT", questionIdAndTextAnswers);
		});

		mergedCategoriesAndSubCategoriesList = new ArrayList<>(
				mapOfCategoryIdAndMergedCategoriesAndSubCategories.values());

		if (StringUtils.isEmpty(test.getScoringFormula())) {
			return mergedCategoriesAndSubCategoriesList;
		} else {
			String expression = test.getScoringFormula();
			BigDecimal sumOfRawScores;

			for (CategoryResponse category : mergedCategoriesAndSubCategoriesList) {
				sumOfRawScores = BigDecimal.valueOf(0);
				for (BigDecimal rawScore : category.getRawScores()) {
					sumOfRawScores = sumOfRawScores.add(rawScore);
				}
				category.setRawScore(sumOfRawScores.divide(BigDecimal.valueOf(category.getRawScores().size()), 10,
						RoundingMode.HALF_EVEN));
				expression = expression.replaceAll(category.getName().trim(), category.getRawScore().toString());
			}

//      Double testScore = infix(expression);

			Expression e = new Expression(expression);
			Double testScore = e.calculate();

			for (CategoryResponse category : mergedCategoriesAndSubCategoriesList) {
				category.setTestScore(testScore.intValue());
			}
			return mergedCategoriesAndSubCategoriesList;
		}

	}

	public String generatedUserInterestSheet(List<Long> userIds) throws IOException {
		// List<User> users = userService.findAllByIdInAndTenant(longs);
		Path path = Paths.get(filedir);
		logger.info(filedir);
		String filename = path.resolve(UUID.randomUUID().toString() + ".csv").toString();
		logger.info("in generateTestReport 70 fileName " + filename);
		FileWriter fileWriter = new FileWriter(filename);
		fileWriter.append(USER_INTEREST_FILE_HEADER.toString());
		fileWriter.append(NEW_LINE_SEPARATOR);
		String userInterestReport = new String((String) queries.get("UserInterstReport"));
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("userIds", userIds);
		paramMap.put("username", "");
		paramMap.put("tenantId", getTenant().getId());
		List<Map<String, Object>> testMapList = jdbcTemplate.queryForList(userInterestReport, paramMap);
		for (Map<String, Object> report : testMapList) {
			fileWriter.append(report.get("username") != null ? report.get("username").toString() : "");
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(String.valueOf('"'))
					.append(report.get("fullname") != null ? report.get("fullname").toString() : "")
					.append(String.valueOf('"'));
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(String.valueOf('"'))
					.append(report.get("school_name") != null ? report.get("school_name").toString() : "")
					.append(String.valueOf('"'));
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(report.get("grade") != null ? report.get("grade").toString() : "");
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(report.get("gender") != null ? report.get("gender").toString() : "");
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(String.valueOf('"'))
					.append(report.get("interests") != null ? report.get("interests").toString() : "")
					.append(String.valueOf('"'));
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(String.valueOf('"'))
					.append(report.get("academic_score") != null ? report.get("academic_score").toString() : "")
					.append(String.valueOf('"'));
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(report.get("rating") != null ? report.get("rating").toString() : "");
			fileWriter.append(NEW_LINE_SEPARATOR);
		}
		fileWriter.flush();
		fileWriter.close();
		return filename;
	}

	public List<UserDetails> getuserDetails(String username) {
		List<UserDetails> userDetails = new ArrayList<>();
		String userInterestReport = new String((String) queries.get("UserInterstReport"));
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("userIds", 0l);
		paramMap.put("username", username);
		paramMap.put("tenantId", getTenant().getId());
		List<Map<String, Object>> testMapList = jdbcTemplate.queryForList(userInterestReport, paramMap);
		for (Map<String, Object> report : testMapList) {
			UserDetails details = new UserDetails();
			details.setUsername(report.get("username") != null ? report.get("username").toString() : "");
			details.setName(report.get("fullname") != null ? report.get("fullname").toString() : "");
			details.setSchool(report.get("school_name") != null ? report.get("school_name").toString() : "");
			details.setGrade(report.get("grade") != null ? report.get("grade").toString() : "");
			details.setGender(report.get("gender") != null ? report.get("gender").toString() : "");
			details.setInterest(report.get("interests") != null ? report.get("interests").toString() : "");
			details.setAcademic(report.get("academic_score") != null ? report.get("academic_score").toString() : "");
			details.setRating(report.get("rating") != null ? report.get("rating").toString() : "");
			userDetails.add(details);
		}
		return userDetails;
	}

	public List<ReportResponse> saveReport(ReportRequest request) {
		Report report;
		ReportSection reportSection;
		if (request.getId() == null) {
			report = request.toEntity(new Report());
		} else {
			report = reportRepository.findOneByIdAndTenant(request.getId(), getTenant());
		}

		if (request.getName().isEmpty()) {
			throw new ValidationException("report name can not be empty");
		}

		if (request.getIdsOfReportSection().isEmpty()) {
			throw new ValidationException("please choose report section");
		}

		report = request.toEntity(report);
		report.getReportSections().clear();

		for (Long id : request.getIdsOfReportSection()) {
			reportSection = reportSectionRepository.findByIdAndTenantId(id, getTenant().getId());
			if (reportSection != null) {
				report.getReportSections().add(reportSection);
			}
		}
		report.setTenant(getTenant());
		reportRepository.saveAndFlush(report);

		return getReports();
	}

	public List<ReportResponse> getReports() {
		List<Report> reports = reportRepository.findAllByTenant(getTenant());
		return reports.stream().map(ReportResponse::new).collect(Collectors.toList());
	}

	public ReportResponse getReport(Long reportId) {

		Report report = reportRepository.findOneByIdAndTenant(reportId, getTenant());
		return new ReportResponse(report);
	}

	public Report getReport(String reportName) {
		return reportRepository.findByName(reportName);
	}

	/**************************************
	 * AIM REPORT STARTS
	 **************************************/

	public List<ReportSectionResponse> getAimCompositeReport(Long userId) {
//****************    Initialization  starts  ************//
		User user;

		if (StringUtils.isEmpty(userId) || userId == 0) {
			user = getUser();
		} else {
			user = userService.getOneById(userId);
		}

		List<Test> tests = new ArrayList<>();
		List<String> testIds = new ArrayList<>();
		Map<Category, List<Question>> mapOfCategoryAndItsQuestions = new HashMap<>();
		Map<Category, List<Question>> mapOfSubCategoryAndItsQuestions = new HashMap<>();
		List<Long> userAnsweredQuestionIds = new ArrayList<>();
		List<Long> questionIdsInTest = new ArrayList<>();
		List<TestQuestion> finalTestQuestions = new ArrayList<>();

		List<Category> categories = new ArrayList<>();
		List<Category> subCategories = new ArrayList<>();
		Map<Long, CategoryResponse> mapOfCategoryIdAndMergedCategoriesAndSubCategories = new HashMap<>();
		ArrayList<CategoryResponse> mergedCategoriesAndSubCategoriesListHavingScores;
		List<ReportSectionResponse> reportSectionResponses = new ArrayList<>();
		TestResult testResult;
		ReportSectionResponse reportSectionResponse;

		/* start -***********Getting the answer key for test-based questions ********/

		HashMap<Long, String> questionIdAndTextAnswers = new HashMap<>();

		List<TextquestionAnswers> textquestionAnswers = textquestionAnswersRepository.findAllByTenant(getTenant());
		textquestionAnswers.forEach(textQue -> {
			questionIdAndTextAnswers.put(textQue.getQuestionId(), textQue.getTextAnswer());
		});

		/* ending - *************/

//****************    Initialization ends   ***************//

		Report report = getReport("Aim Composite Report");

		if (report == null) {
			throw new NotFoundException("Aim Composite Report Not Found");
		}

		if (report.getReportSections().size() == 0) {
			throw new NotFoundException("Report Sections Not Found");
		}
		for (ReportSection reportSection : report.getReportSections()) {
			reportSectionResponse = new ReportSectionResponse(reportSection);

			tests.clear();
			testIds.clear();
			Collections.addAll(testIds, StringUtils.isEmpty(reportSection.getIdsOfTestForEvaluation()) ? new String[0]
					: reportSection.getIdsOfTestForEvaluation().split(","));
			tests = getTestFromLatestTestResultOfUser(user, testIds, getTenant());

			if (tests == null || tests.size() == 0) {
				throw new NotFoundException("Report Section- " + reportSection.getName() + "'s Tests Not Found");
			}

			Boolean testsAreTaken = checkIfAllTestsAreTaken(user, tests, getTenant());

			List<CategoryResponse> mergedCategoriesAndSubCategoriesList = new ArrayList<>();

			if (testsAreTaken) {
				for (Test test : tests) {
					categories.clear();
					categories.addAll(categoryService.getCategoryByTest(test.getId()));// get categories

					subCategories.clear();
					subCategories.addAll(categoryService.getSubCategoryByTest(test.getId())); // get subCategories}

					mergedCategoriesAndSubCategoriesList.clear();
					mergedCategoriesAndSubCategoriesList = categoryService.mergeCategoriesandSubCategories(categories,
							subCategories); // merging categories and subCategories

					testResult = getTestResult(user, test.getId()); // getting user testTaken details

					List<UserAnswer> userAnswers = userAnswerRepository.findAllByTestResultAndTenant(testResult,
							getTenant());// getting all Questions responded by User

					userAnsweredQuestionIds.clear();
					userAnswers.forEach(userAnswer -> userAnsweredQuestionIds.add(userAnswer.getQuestion().getId())); // getting
																														// the
																														// QuestionIds
																														// of
																														// Questions
																														// responded
																														// by
																														// User

					questionIdsInTest.clear();
					test.getQuestions().forEach(tQ -> questionIdsInTest.add(tQ.getQuestion().getId())); // getting
																										// question ids
																										// of questions
																										// present in
																										// test

					try // filtering the Questions and selecting only User responded Questions
					{
						questionIdsInTest.retainAll(userAnsweredQuestionIds);
					} catch (NullPointerException e) {
						logger.error("Empty question list");
					}

					finalTestQuestions.clear();
					questionIdsInTest.forEach(queId -> finalTestQuestions.add(testQuestionRepository
							.findByTestIdAndQuestionIdAndTenant(test.getId(), queId, getTenant()))); // getting the
																										// derived
																										// testQuestions

//                    mapOfCategoryIdAndMergedCategoriesAndSubCategories.putAll(mergedCategoriesAndSubCategoriesList.stream().collect(
//                            Collectors.toMap(CategoryResponse::getId, categoryResponse -> categoryResponse)));

					mergedCategoriesAndSubCategoriesList.forEach(cat -> {
						CategoryResponse c = mapOfCategoryIdAndMergedCategoriesAndSubCategories.get(cat.getId());
						if (c == null) {
							mapOfCategoryIdAndMergedCategoriesAndSubCategories.put(cat.getId(), cat);
						} else {
							c.getSubCategories().clear();
							c.setSubCategories(cat.getSubCategories());

						}

					});

					mapOfCategoryAndItsQuestions.clear();
					// making a map of Category and its Questions
					for (Category category1 : categories) {
						mapOfCategoryAndItsQuestions.put(category1,
								getQuestionsByCategoryAndTest(category1, test.getId()));
					}

					mapOfSubCategoryAndItsQuestions.clear();
					// making a map of subCategory and its Questions
					for (Category subCategory1 : subCategories) {
						mapOfSubCategoryAndItsQuestions.put(subCategory1,
								getQuestionsBySubCategoryAndTest(subCategory1, test.getId()));
					}

					// TestResult tr = testResult;
					TestResult tr = getTestResult(user, test.getId());

					// for cat
					mapOfSubCategoryAndItsQuestions.forEach((Category subCategory, List<Question> questions) -> {
						setCategoryScoreInDataMap(tr, mapOfCategoryIdAndMergedCategoriesAndSubCategories, subCategory,
								questions, "INTSCALE", questionIdAndTextAnswers);
					});

					// for subcat
					mapOfCategoryAndItsQuestions.forEach((Category category, List<Question> questions) -> {
						setCategoryScoreInDataMap(tr, mapOfCategoryIdAndMergedCategoriesAndSubCategories, category,
								questions, "INTSCALE", questionIdAndTextAnswers);
					});

				}

			} else {
				throw new ValidationException("User has not taken all the necessary assessments");
			}

			mergedCategoriesAndSubCategoriesListHavingScores = new ArrayList<>(
					mapOfCategoryIdAndMergedCategoriesAndSubCategories.values());
			mapOfCategoryIdAndMergedCategoriesAndSubCategories.clear();

//            getting the mean of raw scores and and saving as one raw score for each category
			getMeanOfCategoryRawScoresAndSetZScore(mergedCategoriesAndSubCategoriesListHavingScores, user);

			reportSectionResponse.setCategories(mergedCategoriesAndSubCategoriesListHavingScores);

//           getting the overall score for reportSection
			BigDecimal s = getMeanOfCategoriesPercentileScore(reportSectionResponse, user);
			reportSectionResponse.setScore(s);

			if (reportSectionResponse.getName().equalsIgnoreCase("Interest")) {
				StringBuffer str = new StringBuffer();

				List<CategoryResponse> catgoriesForInterest = reportSectionResponse.getCategories().stream()
						.map(CategoryResponse::new).collect(Collectors.toList());

				List<String> top3CatgoriesForInterest = gettingTop3CatgoriesForInterest(catgoriesForInterest);
				if (top3CatgoriesForInterest.isEmpty() || top3CatgoriesForInterest.size() != 3) {
					throw new NotFoundException("Top 3 categories for Interest not found");
				}

				for (String name : top3CatgoriesForInterest) {
					str = str.append(name.charAt(0));

					for (CategoryResponse cat : reportSectionResponse.getCategories()) {
						if (name.equals(cat.getName())) {
							reportSectionResponse.getTop3CategoriesForInterest().add(cat);
						}
					}
				}

				List<InterestInventory> interestInventories = interestInventoryRepository.findAllByTenant(getTenant());

				String obtainedInterestCombination = str.toString();
				for (InterestInventory interestInventory : interestInventories) {
					if (interestInventory.getInterestCombination().equalsIgnoreCase(obtainedInterestCombination)) {
						reportSectionResponse
								.setInterestInventoryResponse(new InterestInventoryResponse(interestInventory));
					}
				}
			}

			if (reportSectionResponse.getName().equalsIgnoreCase("Social Intelligence")) {

				Category categoryForReplacement = categoryRepository.findByNameAndTenant("Social Perception",
						getTenant());
				if (categoryForReplacement == null) {
					throw new NotFoundException("Social Perception Category not found in db");
				}
				CategoryResponse categoryResponseForReplacement = new CategoryResponse(categoryForReplacement);

				reportSectionResponse.getCategories().forEach(categoryResponse -> {
					if (categoryResponse.getName().equalsIgnoreCase("Perceiving Emotion")) {
						categoryResponse.setDisplayName(categoryResponseForReplacement.getDisplayName());
						categoryResponse.setDescription(categoryForReplacement.getDescription());

						switch (user.getGrade()) {
						case "6":
						case "7":
						case "8":
							categoryResponse.setScoreRanges(categoryResponseForReplacement.getJuniorScoreRanges());
							break;
						case "9":
						case "10":
						case "11":
						case "12":
							categoryResponse.setScoreRanges(categoryResponseForReplacement.getSeniorScoreRanges());
							break;
						default:
							throw new ValidationException("User's grade is missing");
						}
					}

				});
			}
			reportSectionResponses.add(reportSectionResponse);
		}

		return reportSectionResponses;
	}

	private List<Test> getTestFromLatestTestResultOfUser(User user, List<String> testIds, Tenant tenant) {
		List<Test> tests = new ArrayList<>();
		List<TestResult> testResultsOfTestsHavingSameName = new ArrayList<>();
		List<Test> testsHavingSameName = new ArrayList<>();
		for (String testId : testIds) {
			testsHavingSameName.clear();
			testResultsOfTestsHavingSameName.clear();

			Test test = testRepository.findByIdAndTenant(Long.valueOf(testId), tenant);

			if (test != null) {
//                logger.info(" Language {} for Test {} : {}", test.getLanguage(), test.getId(), test.getDescription());
				if ((StringUtils.isEmpty(test.getLanguage()) && StringUtils.isEmpty(user.getLanguage()))
						|| (test.getLanguage() != null && test.getLanguage().equalsIgnoreCase(user.getLanguage()))) {
					testsHavingSameName = testRepository.findAllByNameAndTenant(test.getName(), tenant);

					testsHavingSameName.forEach(testHavingSameName -> {
						TestResult testResult = getTestResult(user, testHavingSameName.getId());
						if (testResult != null) {
							testResultsOfTestsHavingSameName.add(testResult);
						}
					});

					if (testResultsOfTestsHavingSameName.size() == 0) {
						return null;

					}
					if (testResultsOfTestsHavingSameName.size() > 1) {
						testResultsOfTestsHavingSameName.sort((a, b) -> (b.getId().intValue() - a.getId().intValue()));
						tests.add(testResultsOfTestsHavingSameName.get(0).getTest());

					}
					if (testResultsOfTestsHavingSameName.size() == 1) {
						tests.add(testResultsOfTestsHavingSameName.get(0).getTest());
					}
				}

			}
		}
//        if (testResultsOfTestsHavingSameName.size() == 0)
//        {
//          return null;
//        }
//        if (testResultsOfTestsHavingSameName.size() > 1)
//        {
//          testResultsOfTestsHavingSameName.sort((a, b) -> (b.getId().intValue() - a.getId().intValue()));
//          tests.add(testResultsOfTestsHavingSameName.get(0).getTest());
//
//        }
//        if (testResultsOfTestsHavingSameName.size() == 1)
//        {
//          tests.add(testResultsOfTestsHavingSameName.get(0).getTest());
//        }
		// }
//      }

		return tests;
	}

	private List<String> gettingTop3CatgoriesForInterest(List<CategoryResponse> catgoriesForInterest) {
		List<String> categoriesName = new ArrayList<>();
		List<CategoryResponse> equalItemsFromList;

		while (categoriesName.size() < 3) {

//             trying to get item who is unique and largest rawScore
			catgoriesForInterest.sort((a, b) -> (b.getRawScore().subtract(a.getRawScore())).intValue());
			equalItemsFromList = getEqualItemsFromList(catgoriesForInterest, catgoriesForInterest.get(0), "rawScore");

//            checking for tie
			if (equalItemsFromList.size() == 1) {

//                tie not happened, simply get the item
				if (!ifAlreadyExist(catgoriesForInterest.get(0).getName(), categoriesName)) {
					categoriesName.add(catgoriesForInterest.get(0).getName());
				}
			} else {

//                it's a tie, break it by largest dislike count
				catgoriesForInterest.sort((a, b) -> (b.getDislikeAndStronglyDislikeFrequency()
						.subtract(a.getDislikeAndStronglyDislikeFrequency())).intValue());
				equalItemsFromList = getEqualItemsFromList(equalItemsFromList, equalItemsFromList.get(0),
						"dislikeAndStronglyDislikeFrequency");

//               again check for tie
				if (equalItemsFromList.size() == 1) {

//                    tie not happened, simply get the item
					if (!ifAlreadyExist(equalItemsFromList.get(0).getName(), categoriesName)) {
						categoriesName.add(equalItemsFromList.get(0).getName());
					}
				} else {

//                    it's a tie, break it by specific priority order list
					categoriesName = pickItemsBySpecificOrder(categoriesName, equalItemsFromList);
				}
			}

//            updating the main list
			for (int i = 0; i < catgoriesForInterest.size(); i++) {
				for (String item : categoriesName) {
					if (catgoriesForInterest.get(i).getName().equals(item)) {
						catgoriesForInterest.remove(i);
					}
				}
			}
		}

		return categoriesName;

	}

	private List<String> pickItemsBySpecificOrder(List<String> categoryNames,
			List<CategoryResponse> equalItemsFromList) {
		String[] priorityList = new String[] { "Investigative", "Social", "Enterprising", "Conventional", "Realistic",
				"Artistic" };
		int[] priorityIndexes = new int[equalItemsFromList.size()];
		CategoryResponse item;
		for (int i = 0; i < equalItemsFromList.size(); i++) {
			item = equalItemsFromList.get(i);
			for (int j = 0; j < priorityList.length; j++) {
				if (item.getName().equals(priorityList[j])) {
					priorityIndexes[i] = j;
				}
			}
		}

		Arrays.sort(priorityIndexes);

		Boolean result = ifAlreadyExist(priorityList[priorityIndexes[0]], categoryNames);
		if (!result) {
			categoryNames.add(priorityList[priorityIndexes[0]]);
		}

		return categoryNames;
	}

	private Boolean ifAlreadyExist(String element, List<String> list) {
		boolean flag = false;
		for (String item : list) {
			if (item.equals(element)) {
				flag = true;
			}
		}
		return flag;
	}

	private int countUniqueValues(List<CategoryResponse> list8) {
		if (list8.size() == 0) {
			return 0;
		}
		int count = 0;
		for (int i = 0; i < list8.size() - 1; i++) {
			if (list8.get(i).getRawScore().equals(list8.get(i + 1).getRawScore())) {
				break;
			} else {
				count++;
			}
		}
		return count;
	}

	private List<CategoryResponse> getEqualItemsFromList(List<CategoryResponse> list1, CategoryResponse cat,
			String param) {
		List<CategoryResponse> resultList = new ArrayList<>();
		if (param.equals("rawScore")) {
			list1.forEach(item -> {
				if (cat.getRawScore().equals(item.getRawScore())) {
					resultList.add(item);
				}
			});
		}
		if (param.equals("dislikeAndStronglyDislikeFrequency")) {
			list1.forEach(item -> {
				if (cat.getDislikeAndStronglyDislikeFrequency().equals(item.getDislikeAndStronglyDislikeFrequency())) {
					resultList.add(item);
				}
			});
		}
		return resultList;
	}

	private boolean checkIfAllTestsAreTaken(User user, List<Test> tests, Tenant tenant) {
		TestResult testResult;
		for (Test test : tests) {
			testResult = getTestResult(user, test.getId());
			if (testResult == null) {
				return false;
			}
		}
		return true;
	}

	private BigDecimal getMeanOfCategoriesPercentileScore(ReportSectionResponse reportSectionResponse, User user) {
		String grade = user.getGrade();
		String language = user.getLanguage();
		BigDecimal sumOfCategoryRawScores = new BigDecimal(0);
		BigDecimal reportSectionRawScore = new BigDecimal(0);
		BigDecimal z = new BigDecimal(0);

		for (CategoryResponse categoryResponse : reportSectionResponse.getCategories()) {
			sumOfCategoryRawScores = sumOfCategoryRawScores.add(categoryResponse.getRawScore());
		}

		try {
			reportSectionRawScore = sumOfCategoryRawScores
					.divide(new BigDecimal(reportSectionResponse.getCategories().size()), 2, RoundingMode.HALF_EVEN);
		} catch (ArithmeticException ae) {
			String reason = reportSectionResponse.getName() + "/categories="
					+ reportSectionResponse.getCategories().size() + "/" + ExceptionUtils.getStackTrace(ae);
			setUserReportStatus(user, "Aim Composite Report", ReportStatus.Failed, null, reason);
			throw new ArithmeticException(reason);
		}

		NormResponse norm = getNormFilterByLanguage(reportSectionResponse.getNorms(), language);

		if (norm.getJuniorMeanScore() == null || norm.getSeniorMeanScore() == null
				|| norm.getSecondaryMeanScore() == null) {
			throw new NotFoundException(reportSectionResponse.getName() + " -mean score not found.");
		}
		if (norm.getJuniorStandardDeviation() == null || norm.getSeniorStandardDeviation() == null
				|| norm.getSecondaryStandardDeviation() == null) {
			throw new NotFoundException(reportSectionResponse.getName() + " -standard deviation score not found.");
		}

		switch (grade) {
		case "6":
		case "7":
		case "8":
			reportSectionResponse.setScoreRanges(reportSectionResponse.getJuniorScoreRanges());

			z = ((reportSectionRawScore.subtract(norm.getJuniorMeanScore())).divide(norm.getJuniorStandardDeviation(),
					10, RoundingMode.HALF_EVEN));
			break;
		case "9":
		case "10":
			reportSectionResponse.setScoreRanges(reportSectionResponse.getJuniorScoreRanges());

			z = ((reportSectionRawScore.subtract(norm.getSecondaryMeanScore()))
					.divide(norm.getSeniorStandardDeviation(), 10, RoundingMode.HALF_EVEN));
			break;
		case "11":
		case "12":
			reportSectionResponse.setScoreRanges(reportSectionResponse.getSeniorScoreRanges());

			z = ((reportSectionRawScore.subtract(norm.getSeniorMeanScore())).divide(norm.getSeniorStandardDeviation(),
					10, RoundingMode.HALF_EVEN));
			break;
		default:
			throw new ValidationException("User's grade is missing");
		}
		z = (z.multiply(BigDecimal.valueOf(10)).add(BigDecimal.valueOf(50)));
		z = z.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		return z;
	}

	public NormResponse getNormFilterByLanguage(List<NormResponse> norms, String language) {
		if (StringUtils.isEmpty(language)) {
			language = "English";
		}

		for (NormResponse norm : norms) {
			if (norm.getLanguage().equalsIgnoreCase(language)) {
				return norm;
			}
		}

		return null;
	}

	private void getMeanOfCategoryRawScoresAndSetZScore(
			ArrayList<CategoryResponse> mergedCategoriesAndSubCategoriesList, User user) {
		String grade = user.getGrade();
		String language = user.getLanguage();
		BigDecimal sumOfRawScores = new BigDecimal(0);
		BigDecimal z;
		for (CategoryResponse category : mergedCategoriesAndSubCategoriesList) {
			sumOfRawScores = BigDecimal.valueOf(0);
			for (BigDecimal rawScore : category.getRawScores()) {
				sumOfRawScores = sumOfRawScores.add(rawScore);
			}

			category.setRawScore(sumOfRawScores.divide(BigDecimal.valueOf(category.getRawScores().size()), 2,
					RoundingMode.HALF_EVEN));

			NormResponse norm = getNormFilterByLanguage(category.getNorms(), language);

			if (norm.getJuniorMeanScore() == null || norm.getSeniorMeanScore() == null
					|| norm.getSecondaryMeanScore() == null) {
				throw new NotFoundException(category.getName() + " -mean score not found.");
			}
			if (norm.getJuniorStandardDeviation() == null || norm.getSeniorStandardDeviation() == null
					|| norm.getSecondaryStandardDeviation() == null) {
				throw new NotFoundException(category.getName() + " -standard deviation score not found.");
			}

			switch (grade) {
			case "6":
			case "7":
			case "8":
				category.setScoreRanges(category.getJuniorScoreRanges());
				z = ((category.getRawScore().subtract(norm.getJuniorMeanScore()))
						.divide(norm.getJuniorStandardDeviation(), 10, RoundingMode.HALF_EVEN));
				break;
			case "9":
			case "10":
				category.setScoreRanges(category.getJuniorScoreRanges());
				z = ((category.getRawScore().subtract(norm.getSecondaryMeanScore()))
						.divide(norm.getSecondaryStandardDeviation(), 10, RoundingMode.HALF_EVEN));
				break;
			case "11":
			case "12":
				category.setScoreRanges(category.getSeniorScoreRanges());
				z = ((category.getRawScore().subtract(norm.getSeniorMeanScore()))
						.divide(norm.getSeniorStandardDeviation(), 10, RoundingMode.HALF_EVEN));
				break;
			default:
				throw new ValidationException("User's grade is missing");
			}

			z = z.multiply(BigDecimal.valueOf(10)).add(BigDecimal.valueOf(50));
			z = z.abs();
			z = z.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			category.setPercentile(z);
		}
	}

	public void saveReportSectionScoreRanges(List<ScoreRangeRequest> request, Long reportSectionId,
			String applicableFor) {
		if (reportSectionId == null) {
			throw new ValidationException("please choose ReportSection first");
		}

		ReportSection reportSection = reportSectionRepository.findByIdAndTenantId(reportSectionId, getTenant().getId());
		if (reportSection == null) {
			throw new NotFoundException("Report Section not found");
		}

		if (StringUtils.isEmpty(applicableFor)) {
			throw new ValidationException("for whom this is applicable, Junior or Senior ?");
		}

		List<ScoreRange> scoreRanges = new ArrayList<>(request.size());
		for (ScoreRangeRequest r : request) {
			scoreRanges.add(r.toEntity(new ScoreRange()));
		}

		scoreRanges = scoreRangeRepository.save(scoreRanges);
		reportSection.getScoreRanges().clear();

		if (applicableFor.equalsIgnoreCase("senior")) {
			reportSection.getSeniorScoreRanges().clear();
			reportSection.setSeniorScoreRanges(scoreRanges);
		} else {
			reportSection.getJuniorScoreRanges().clear();
			reportSection.setJuniorScoreRanges(scoreRanges);
		}
		reportSection.setScoreRanges(scoreRanges);
		reportSectionRepository.save(reportSection);
	}

	public List<ReportSectionResponse> getReportSections() {
		List<String> testIds = new ArrayList<>();
		List<ReportSection> reportSections = reportSectionRepository.findAllByTenant(getTenant());
		ReportSectionResponse reportSectionResponse;
		ReportSection reportSection;
		List<ReportSectionResponse> reportSectionResponses = reportSections.stream().map(ReportSectionResponse::new)
				.collect(Collectors.toList());
		for (int i = 0; i < reportSectionResponses.size(); i++) {
			reportSectionResponse = reportSectionResponses.get(i);
			reportSection = reportSections.get(i);
			testIds.clear();
			Collections.addAll(testIds, StringUtils.isEmpty(reportSection.getIdsOfTestForEvaluation()) ? new String[0]
					: reportSection.getIdsOfTestForEvaluation().split(","));
			for (String testId : testIds) {
				Test test = testRepository.findByIdAndTenant(Long.valueOf(testId), getTenant());
				reportSectionResponse.getSelectedTests().add(new TestDescriptionResponse(test));
			}
		}
		return reportSectionResponses;
	}

	public ReportSectionResponse getReportSection(Long id) {
		ReportSection reportSection = reportSectionRepository.findByIdAndTenantId(id, getTenant().getId());
		List<String> testIds = new ArrayList<>();
		ReportSectionResponse reportSectionResponse = new ReportSectionResponse(reportSection);
		Collections.addAll(testIds, StringUtils.isEmpty(reportSection.getIdsOfTestForEvaluation()) ? new String[0]
				: reportSection.getIdsOfTestForEvaluation().split(","));
		testIds.forEach(testId -> {
			Test test = testRepository.findByIdAndTenant(Long.valueOf(testId), getTenant());
			reportSectionResponse.getSelectedTests().add(new TestDescriptionResponse(test));
		});
		return reportSectionResponse;
	}

	public ReportSectionResponse saveReportSection(ReportSectionRequest request) {
		ReportSection reportSection;
		if (request.getId() == null) {
			reportSection = new ReportSection();
		} else {
			reportSection = reportSectionRepository.findByIdAndTenantId(request.getId(), getTenant().getId());
		}

		if (request.getName().isEmpty()) {
			throw new ValidationException("Report name can not be empty");
		}

		reportSection = request.toEntity(reportSection);

		reportSection.setTenant(getTenant());

		return new ReportSectionResponse(reportSectionRepository.saveAndFlush(reportSection));

	}

	public List<InterestInventoryResponse> saveInterestInventory(InterestInventoryRequest request) {
		Boolean errorFlag = false;
		if (request.getInterestCombination() == null) {
			errorFlag = true;
		}
		if (request.getRegion() == null) {
			errorFlag = true;
		}
		if (request.getDescription() == null) {
			errorFlag = true;
		}
		if (errorFlag) {
			throw new ValidationException("please check input");
		} else {
			InterestInventory interestInventory;
			if (request.getId() == null) {
				interestInventory = new InterestInventory();
			} else {
				interestInventory = interestInventoryRepository.findByIdAndTenant(request.getId(), getTenant());
			}
			interestInventory = request.toEntity(interestInventory);
			interestInventoryRepository.saveAndFlush(interestInventory);
			return getInterestInventory();
		}

	}

	public List<InterestInventoryResponse> getInterestInventory() {
		List<InterestInventory> interestInventories = interestInventoryRepository.findAllByTenant(getTenant());
		return interestInventories.stream().map(InterestInventoryResponse::new).collect(Collectors.toList());

	}

	public List<ReportSectionResponse> getReportSectionWithCategory() {
		List<ReportSection> reportSectionList = reportSectionRepository.findAllByTenant(getTenant());
		List<String> testIds = new ArrayList<>();
		ReportSectionResponse reportSectionResponse;
		List<ReportSectionResponse> reportSections = new ArrayList<>();
		Map<Long, CategoryResponse> categories = new HashMap<>();
		Test test;

		for (ReportSection reportSection : reportSectionList) {
			reportSectionResponse = new ReportSectionResponse(reportSection);
			Collections.addAll(testIds, StringUtils.isEmpty(reportSection.getIdsOfTestForEvaluation()) ? new String[0]
					: reportSection.getIdsOfTestForEvaluation().split(","));
			for (String testId : testIds) {
				test = testRepository.findByIdAndTenant(Long.valueOf(testId), getTenant());
				if (test != null) {
					for (TestQuestion testQuestion : test.getQuestions()) {
						for (Category category : testQuestion.getCategory()) {
							categories.putIfAbsent(category.getId(), new CategoryResponse(category));
						}
					}
				}
			}

			reportSectionResponse.getCategories().addAll(categories.values());
			categories.clear();
			testIds.clear();

			reportSections.add(reportSectionResponse);
		}
		return reportSections;
	}

	public void copyJuniorNormsToSenior(Long reportId) {
		List<Test> tests = new ArrayList<>();
		List<String> testIds = new ArrayList<>();
		List<Category> categories = new ArrayList<>();

		List<MasterDataDocument> languages = masterDataDocumentService.getAllLanguages();

		Report report = reportRepository.findOneByIdAndTenant(reportId, getTenant());
		if (report == null) {
			throw new NotFoundException("Report Not Found");
		}

		if (report.getReportSections().size() == 0) {
			throw new NotFoundException("Report Sections Not Found");
		}

		for (ReportSection reportSection : report.getReportSections()) {
			tests.clear();
			testIds.clear();
			Collections.addAll(testIds, StringUtils.isEmpty(reportSection.getIdsOfTestForEvaluation()) ? new String[0]
					: reportSection.getIdsOfTestForEvaluation().split(","));
			testIds.forEach(testId -> {
				Test test = testRepository.findByIdAndTenant(Long.valueOf(testId), getTenant());
				if (test != null) {
					tests.add(test);
				}
			});
			if (tests.size() == 0) {
				throw new NotFoundException("Report Section- " + reportSection.getName() + "'s Tests Not Found");
			}

			for (Test test : tests) {
				logger.info(test.getName());
				categories.clear();
				categories.addAll(categoryService.getCategoryByTest(test.getId()));// get categories

				categories.forEach(category -> {
					category.getSeniorScoreRanges().clear();
					category.getJuniorScoreRanges().forEach(sr -> {
						ScoreRange scoreRange = new ScoreRange();
						scoreRange.setStartFrom(sr.getStartFrom());
						scoreRange.setEndAt(sr.getEndAt());
						scoreRange.setRangeOf(sr.getRangeOf());
						scoreRange.setText(sr.getText());
						scoreRange.setInnateTalent(sr.getInnateTalent());
						scoreRange.setStrength(sr.getStrength());
						scoreRange.setWeakness(sr.getWeakness());
						scoreRange.setLearningStyle(sr.getLearningStyle());
						scoreRange.setLearningStyleInference(sr.getLearningStyleInference());
						scoreRange.setLearningStyleRecommendation(sr.getLearningStyleRecommendation());
						scoreRange.setDevelopmentPlanForStrength(sr.getDevelopmentPlanForStrength());
						scoreRange.setDevelopmentPlanForWeakness(sr.getDevelopmentPlanForWeakness());
						ScoreRange scoreRange1 = scoreRangeRepository.saveAndFlush(scoreRange);
						category.getSeniorScoreRanges().add(scoreRange1);
					});

					languages.forEach(language -> {
						Norm norm = normRepository.findByCategoryIdAndLanguageAndTenant(category.getId(),
								language.getValue(), getTenant());
						if (norm == null) {
							norm = new Norm();
						}
						norm.setLanguage(language.getValue());
						norm.setJuniorMeanScore(category.getJuniorMeanScore());
						norm.setJuniorStandardDeviation(category.getJuniorStandardDeviation());
						norm.setSeniorMeanScore(category.getSeniorMeanScore());
						norm.setSeniorStandardDeviation(category.getSeniorStandardDeviation());
						norm.setSecondaryMeanScore(category.getSecondaryMeanScore());
						norm.setSecondaryStandardDeviation(category.getSecondaryStandardDeviation());
						norm.setCategory(category);
						normRepository.saveAndFlush(norm);
					});

					categoryRepository.saveAndFlush(category);
				});
			}
			reportSection.getSeniorScoreRanges().clear();
			reportSection.getJuniorScoreRanges().forEach(sr -> {
				ScoreRange scoreRange = new ScoreRange();
				scoreRange.setStartFrom(sr.getStartFrom());
				scoreRange.setEndAt(sr.getEndAt());
				scoreRange.setRangeOf(sr.getRangeOf());
				scoreRange.setText(sr.getText());
				scoreRange.setInnateTalent(sr.getInnateTalent());
				scoreRange.setStrength(sr.getStrength());
				scoreRange.setWeakness(sr.getWeakness());
				scoreRange.setLearningStyle(sr.getLearningStyle());
				scoreRange.setLearningStyleInference(sr.getLearningStyleInference());
				scoreRange.setLearningStyleRecommendation(sr.getLearningStyleRecommendation());
				scoreRange.setDevelopmentPlanForStrength(sr.getDevelopmentPlanForStrength());
				scoreRange.setDevelopmentPlanForWeakness(sr.getDevelopmentPlanForWeakness());
				ScoreRange scoreRange1 = scoreRangeRepository.saveAndFlush(scoreRange);
				reportSection.getSeniorScoreRanges().add(scoreRange1);

			});
			languages.forEach(language -> {
				Norm norm = normRepository.findByReportSectionIdAndLanguageAndTenant(reportSection.getId(),
						language.getValue(), getTenant());
				if (norm == null) {
					norm = new Norm();
				}
				norm.setLanguage(language.getValue());
				norm.setJuniorMeanScore(reportSection.getJuniorMeanScore());
				norm.setJuniorStandardDeviation(reportSection.getJuniorStandardDeviation());
				norm.setSeniorMeanScore(reportSection.getSeniorMeanScore());
				norm.setSeniorStandardDeviation(reportSection.getSeniorStandardDeviation());
				norm.setSecondaryMeanScore(reportSection.getSecondaryMeanScore());
				norm.setSecondaryStandardDeviation(reportSection.getSecondaryStandardDeviation());
				norm.setReportSection(reportSection);
				normRepository.saveAndFlush(norm);
			});
			reportSectionRepository.saveAndFlush(reportSection);
		}
	}

	public boolean allowReportToUser(User user, String reportName, Tenant tenant) {
		Report report = reportRepository.findByNameAndTenant(reportName, tenant);
		return allowReportToUser(user, report, tenant);
	}

	public boolean allowReportToUser(User user, Report report, Tenant tenant) {
		List<Test> tests = new ArrayList<>();
		List<String> testIds;
		// String testid;
		int totalTestCount = 0;
		if (user == null) {
			user = getUser();
		}
		if (report == null) {
			logger.error("report not found while checking eligibility for it");
			return false;
		}
		HashMap<String, List<String>> testIdsByReportSection = new HashMap<>(report.getReportSections().size());

		for (ReportSection section : report.getReportSections()) {
			testIdsByReportSection.put(section.getName(), new ArrayList<>());
			testIds = testIdsByReportSection.get(section.getName());

			Collections.addAll(testIds, StringUtils.isEmpty(section.getIdsOfTestForEvaluation()) ? new String[0]
					: section.getIdsOfTestForEvaluation().split(","));
			totalTestCount += testIds.size();

		}

		// testIdsByReportSection.put("Directing Emotions", new ArrayList<>());
		// testid = "613";

		// Collections.addAll(testIds, StringUtils.isEmpty(613) ? new String[0] :
		// section.getIdsOfTestForEvaluation().split(","));
		// totalTestCount += testIds.size();

//    int testTakenCount = testResultRepository.countAllByUser(user);
//    if(testTakenCount<totalTestCount){
//        return false;
//    }

		for (List<String> testIds1 : testIdsByReportSection.values()) {
			tests = getTestFromLatestTestResultOfUser(user, testIds1, tenant);
			if (tests == null || tests.size() == 0) {
				return false;

			}
			boolean testsAreTaken = checkIfAllTestsAreTaken(user, tests, tenant);
			if (!testsAreTaken) {
				return false;
			}
		}

//          List<String> testIds1=new ArrayList<>();
//          testIds1.add("613");
//          tests = getTestFromLatestTestResultOfUser(user, testIds1, tenant);
//          if (tests == null || tests.size() == 0) {
//              return false;
//
//          }
//          boolean testsAreTaken = checkIfAllTestsAreTaken(user, tests, tenant);
//          if (!testsAreTaken) {
//              return false;
//          }

//    if (tests == null || tests.size() == 0) {
//        return false;
// }
//    boolean testsAreTaken = checkIfAllTestsAreTaken(user, tests, tenant);
//    if (!testsAreTaken) {
//      return false;
//    }
		user.setEnableAimReport(true);
		userRepository.saveAndFlush(user);
		return true;
	}

	public void checkAndAllowAllUserForReport(Long reportId) {
		File file = null;
		Report report = reportRepository.findOneByIdAndTenant(reportId, getTenant());
		List<User> users = userRepository.findAllByTenant(getTenant());

		for (User user : users) {
			try {
				boolean isAllowed = allowReportToUser(user, report, getTenant());

				user.setEnableAimReport(isAllowed);
				userRepository.saveAndFlush(user);
				if (isAllowed) {
					UserReportStatus userReportStatus = setUserReportStatus(user, report, ReportStatus.Wait, null,
							null);
					if (userReportStatus != null) {
						file = createReport(userReportStatus, getTenant(), AimReportType.MAIN);
						if (file == null) {
							logger.error("Error! in making " + userReportStatus.getUser().getUsername() + "'s report");
						} else {
							logger.info(
									userReportStatus.getUser().getUsername() + "'s report made and saved successfully");
							if (!file.delete()) {
								logger.error("Could not delete temporary file after processing: " + file);
							}
						}
					}
					userRepository.saveAndFlush(user);
				}
			} catch (Exception e) {
				logger.error("Something happened wrong while processing report", e);
				setUserReportStatus(user, report, ReportStatus.Failed, null, e.getMessage());
			}
		}
	}

	public File createReport(UserReportStatus userReportStatus, Tenant tenant, AimReportType reportType)
			throws IOException {
		this.rateLimiter.acquire(1);
		setUserReportStatus(userReportStatus.getUser(), userReportStatus.getReportName(), ReportStatus.Generating, null,
				null);

//      (NOTIFICATION)=> send a post on timline
		List<Timeline> posts = timelineService.getPostsByTypeAndUser("aimReport", userReportStatus.getUser());
		if (posts.size() == 0) {
			timelineService.savePostForAimReport(userReportStatus, ReportStatus.Generating.toString());
		} else {
			// timelineService.savePostForAimReport(userReportStatus,
			// ReportStatus.Generating.toString());

		}
		File file = downloadAimReport(userReportStatus.getUser().getId(), tenant, reportType);
		if (file == null || file.length() == 0) {
			setUserReportStatus(userReportStatus.getUser(), userReportStatus.getReportName(), ReportStatus.Failed, null,
					"empty file from report server");
			return null;
		}
		store.store("profileimage", file);
		setUserReportStatus(userReportStatus.getUser(), userReportStatus.getReportName(), ReportStatus.Generated,
				file.getName(), null);
		return file;
	}

	public UserReportStatus setUserReportStatus(User user, String reportName, ReportStatus status, String fileName,
			String reason) {
		Report report = getReport(reportName);
		return setUserReportStatus(user, report, status, fileName, reason);
	}

	public UserReportStatus setUserReportStatus(User user, Report report, ReportStatus status, String fileName,
			String reason) {
		UserReportStatus userReportStatus = userReportStatusRepository.findByUserAndReportId(user, report.getId());
		if (userReportStatus == null) {
			userReportStatus = new UserReportStatus();
		}

		userReportStatus.setUser(user);
		userReportStatus.setReportId(report.getId());
		userReportStatus.setReportName(report.getName());
		userReportStatus.setStatus(status.toString());
		userReportStatus.setFilename(fileName);
		if (userReportStatus.getStatus().equalsIgnoreCase("Failed")) {
			if (!StringUtils.isEmpty(reason)) {
				userReportStatus.setReason(reason);
			}
		} else {
			userReportStatus.setReason(null);
		}

		userReportStatus = userReportStatusRepository.saveAndFlush(userReportStatus);
		if (userReportStatus.getStatus() != null) {
			if (!userReportStatus.getStatus().equals("InProgress")) {
				logger.info(userReportStatus.getUser().getUsername() + " report status updated to - "
						+ userReportStatus.getStatus());
			}
		}
		return userReportStatus;
	}

	public String tryToGetReportFromS3Bucket(User user, AimReportType reportType) {
		UserReportStatus userReportStatus = userReportStatusRepository.findByUserAndTenant(user, getTenant());
		if (userReportStatus == null) {
			logger.info(user.getUsername() + "'s report have not made yet");
			return null;
		}
		logger.info("finding report in S3 Bucket");
		String fileName = null;
		if (userReportStatus.getStatus().equals(ReportStatus.Generated.name())) {
			fileName = user.getUsername() + "_" + user.getName() + ".pdf";
			if (!StringUtils.isEmpty(reportType)) {
				if (reportType.equals(AimReportType.MINI)) {
					fileName = user.getUsername() + "_" + user.getName() + "MINI.pdf";
				}
			}
		}
		return fileName;
	}

	public List<UserReportStatusResponse> getUserReportStatus(String status, DashboardRequest request) {

		Date fromDate = com.synlabs.intscale.util.DateUtil.getFormattedDate(request.getFromDate(), START_OF_DAY);
		Date toDate = DateUtil.getFormattedDate(request.getToDate(), END_OF_DAY);
		if (fromDate == null) {
			fromDate = new LocalDate().minusDays(1).toDate();
		}
		if (toDate == null) {
			toDate = new LocalDate().toDate();
		}

		JPAQuery<UserReportStatus> query = new JPAQuery<>(entityManager);
		QUserReportStatus userReportStatus = QUserReportStatus.userReportStatus;
		query.select(userReportStatus).from(userReportStatus).where(userReportStatus.createdDate
				.between(fromDate, toDate).or(userReportStatus.lastModifiedDate.between(fromDate, toDate)));

		if (!CollectionUtils.isEmpty(request.getGrades())) {
			query.where(userReportStatus.user.grade.in(request.getGrades()));
		}

		if (!CollectionUtils.isEmpty(request.getSchools())) {
			query.where(userReportStatus.user.schoolName.in(request.getSchools()));
		}

		if (!CollectionUtils.isEmpty(request.getCities())) {
			query.where(userReportStatus.user.city.in(request.getCities()));
		}

		if (!StringUtils.isEmpty(status) && !status.equalsIgnoreCase("all")) {
			query.where(userReportStatus.status.eq(status));
		}

		List<UserReportStatus> userReportStatusList = query.fetch();

		return userReportStatusList.stream().map(UserReportStatusResponse::new).collect(Collectors.toList());
	}

	public List<UserReportStatusResponse> getUserReportStatusByFullName(String fullName) {

		JPAQuery<UserReportStatus> query = new JPAQuery<>(entityManager);
		QUserReportStatus userReportStatus = QUserReportStatus.userReportStatus;
		query.select(userReportStatus).from(userReportStatus);

		if (!StringUtils.isEmpty(fullName)) {
			query.where(userReportStatus.user.name.like("%" + fullName + "%")
					.or(userReportStatus.user.username.like("%" + fullName + "%")));
		} else {
			throw new ValidationException("Please enter fullName for which you want see the report");
		}

		List<UserReportStatus> userReportStatusList = query.fetch();

		return userReportStatusList.stream().map(UserReportStatusResponse::new).collect(Collectors.toList());
	}

	public void prepareShareReport(String email, String cc, Long userId, String reportTypeString,
			boolean isSendToUser) {
		User user = userRepository.findOneByIdAndTenant(userId, getTenant());
		if (user == null) {
			throw new NotFoundException("user not found");
		}
		if (isSendToUser) {
			email = user.getEmail();
		}

		AimReportType reportType = null;
		if (!StringUtils.isEmpty(reportTypeString)) {
			try {
				reportType = AimReportType.valueOf(reportTypeString);
			} catch (IllegalArgumentException e) {
				logger.error("requested reportType not found", e);
			}
		}

		String fileName = tryToGetReportFromS3Bucket(user, reportType);
		if (StringUtils.isEmpty(fileName)) {
			throw new NotFoundException(user.getUsername() + "'s report not found");
		}
		File file = getReportFileByName(fileName);
		shareReport(email, cc, user, file);
	}

	public void shareReport(String email, String cc, User user, File file) {
		communicationService.shareReport(user, email, cc, file);
		if (file != null) {
			file.deleteOnExit();
		}
	}

	public File getReportFileByName(String fileName) {
		InputStream is = null;
		File file = new File(tempDirPath + "/" + fileName);
		try {
			is = store.getStream("profileimage", fileName);
			OutputStream out = new FileOutputStream(file);
			IOUtils.copy(is, out);
			out.close();
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		return file;
	}

	public UserReportStatus getUserReportStatusByUser(Long userId, Tenant tenant) throws IOException {
		if (tenant == null) {
			tenant = getTenant();
		}
		User user = userRepository.findOneByIdAndTenant(userId, tenant);
		UserReportStatus userReportStatus = userReportStatusRepository.findByUserAndTenant(user, tenant);
		if (userReportStatus == null) {
			logger.info(user.getUsername() + "'s userReportStatus not found in db");
			return null;
		} else {

//            createReport(userReportStatus, tenant);
			return userReportStatus;
		}
	}

	public void saveAimReportExcelInDb(UserReportStatusRequest request) throws IOException {
		UserReportStatus userReportStatus = getUserReportStatusByUser(request.getUserId(), getTenant());
		if (userReportStatus == null) {
			logger.error("userReportStatus not found for userId - " + request.getUserId().toString());
		} else {
			AimReportContent aimReportContent = null;
			aimReportContentRepository.delete(userReportStatus.getAimReportContents());
			userReportStatus.getAimReportContents().clear();
			for (AimReportContentRequest aimReportContentRequest : request.getAimReportContentRequests()) {
				aimReportContent = aimReportContentRequest.toEntity(new AimReportContent());
				aimReportContent.setUserReportStatus(userReportStatus);
				aimReportContent.setTenant(getTenant());
				aimReportContentRepository.saveAndFlush(aimReportContent);
				userReportStatus.getAimReportContents().add(aimReportContent);
			}

			userReportStatusRepository.saveAndFlush(userReportStatus);

			logger.info(userReportStatus.getUser().getUsername() + "'s Aim Profile successfully saved in db");
		}
	}

	public String downloadAimReportInExcel(DashboardRequest request) {

		User currentUser = userRepository.findOne(getUser().getId());
		Path path = Paths.get(filedir);
		String filename = path.resolve(UUID.randomUUID().toString() + ".csv").toString();
		FileWriter fileWriter = null;

		List<AimReportContent> careers = new ArrayList<>();
		List<AimReportContent> subjects = new ArrayList<>();
		List<AimReportContent> leisures = new ArrayList<>();
		List<AimReportContent> categories = new ArrayList<>();
		List<AimReportContent> reportSections = new ArrayList<>();
		List<String> interestCategories = new ArrayList<>();
		HashMap<String, List<AimReportContent>> categoriesByReportSection = new HashMap<>();

		try {
			boolean writeForHeaderFlag = true;
			fileWriter = new FileWriter(filename);
			fileWriter.append(
					"USERNAME, NAME, EMAIL, GENDER, GRADE, STREAM, SCHOOL, CITY, ACADEMIC SCORE, SCIENCE, MATHEMATICS, SUPW, INTEREST, LEARNING MODE, THINKING STYLE, LEARNING ENVIRONMENT,");
			JPAQuery<UserReportStatus> query = dashboardService.getUserReportStatus(request, ReportStatus.Generated);

			int page = 1;
			int offset = 0;
			int limit = 50;

			long totalRecordsCount = query.fetchCount();
			List<UserReportStatus> generatedReports = null;

			while (totalRecordsCount > offset) {
				offset = (page - 1) * limit;
				if (offset > 0) {
					query.offset(offset);
				}
				query.limit(limit);
				generatedReports = query.fetch();

				for (UserReportStatus report : generatedReports) {
					if (report.getAimReportContents() != null) {
						if (report.getAimReportContents().size() > 0) {
							categories.clear();
							reportSections.clear();
							careers.clear();
							subjects.clear();
							leisures.clear();

							categories = report.getAimReportContents().stream()
									.filter(content -> content.getType().equals(AimReportContentType.Category))
									.collect(Collectors.toList());

							reportSections = report.getAimReportContents().stream()
									.filter(content -> content.getType().equals(AimReportContentType.ReportSection))
									.collect(Collectors.toList());

							careers = report.getAimReportContents().stream()
									.filter(content -> content.getType().equals(AimReportContentType.Career))
									.collect(Collectors.toList());

							subjects = report.getAimReportContents().stream()
									.filter(content -> content.getType().equals(AimReportContentType.Subject))
									.collect(Collectors.toList());

							leisures = report.getAimReportContents().stream()
									.filter(content -> content.getType().equals(AimReportContentType.Leisure))
									.collect(Collectors.toList());

							categoriesByReportSection.clear();

							categories.forEach(category -> {
								if (categoriesByReportSection.containsKey(category.getReportSectionName())) {
									categoriesByReportSection.get(category.getReportSectionName()).add(category);
								} else {
									List<AimReportContent> groupedCategories = new ArrayList<>();
									groupedCategories.add(category);
									categoriesByReportSection.put(category.getReportSectionName(), groupedCategories);
								}
							});

//            for header code starts----------------------------------------------------
							if (writeForHeaderFlag) {
								for (List<AimReportContent> cats : categoriesByReportSection.values()) {
									cats.sort((a, b) -> (a.getName().compareToIgnoreCase(b.getName())));

									for (AimReportContent cat : cats) {
										fileWriter.append(cat.getName()).append("'s Raw Score,");

									}
									fileWriter
											.append(String.valueOf(reportSections.stream()
													.filter(reportSection -> reportSection.getName()
															.equals(cats.get(0).getReportSectionName()))
													.collect(Collectors.toList()).get(0).getName()))
											.append("'s Raw Score,");

									for (AimReportContent cat : cats) {
										fileWriter.append(cat.getName()).append("'s Standard Score,");

									}
									fileWriter
											.append(String.valueOf(reportSections.stream()
													.filter(reportSection -> reportSection.getName()
															.equals(cats.get(0).getReportSectionName()))
													.collect(Collectors.toList()).get(0).getName()))
											.append("'s Standard Score,");

									for (AimReportContent cat : cats) {

										fileWriter.append(cat.getName()).append("'s Score Label,");
									}
									fileWriter
											.append(String.valueOf(reportSections.stream()
													.filter(reportSection -> reportSection.getName()
															.equals(cats.get(0).getReportSectionName()))
													.collect(Collectors.toList()).get(0).getName()))
											.append("'s Score Label,");

									for (AimReportContent cat : cats) {

										fileWriter.append(cat.getName()).append("'s Score Label Rating,");
									}
									fileWriter
											.append(String.valueOf(reportSections.stream()
													.filter(reportSection -> reportSection.getName()
															.equals(cats.get(0).getReportSectionName()))
													.collect(Collectors.toList()).get(0).getName()))
											.append("'s Score Label Rating,");

									if (cats.get(0).getReportSectionName().equalsIgnoreCase("Interest")) {
										for (AimReportContent cat : cats) {
											interestCategories.add(cat.getName());
										}
									}

									if (cats.get(0).getReportSectionName()
											.equalsIgnoreCase("Top3CategoriesOfInterest")) {
										for (String ic : interestCategories) {
											fileWriter.append("Final").append(ic).append(",");
										}

									}
								}
								fileWriter.append("\n");
								writeForHeaderFlag = false;

							}
//            for header code ends----------------------------------------------------
                         //LNT
							String learningMode = report.getAimReportContents().stream()
									.filter(r -> r.getType().equals(AimReportContentType.LearningMode))
									.collect(Collectors.toList()).isEmpty() ? "  " : report.getAimReportContents().stream()
													.filter(r -> r.getType().equals(AimReportContentType.LearningMode))
													.collect(Collectors.toList()).get(0).getName();
							String thinkingStyle = report.getAimReportContents().stream()
									.filter(r -> r.getType().equals(AimReportContentType.ThinkingStyle))
									.collect(Collectors.toList()).isEmpty() ? "  "	: report.getAimReportContents().stream()
													.filter(r -> r.getType().equals(AimReportContentType.ThinkingStyle))
													.collect(Collectors.toList()).get(0).getName();
							String learningEnvironment = report.getAimReportContents().stream()
									.filter(r -> r.getType().equals(AimReportContentType.LearningEnvironment))
									.collect(Collectors.toList()).isEmpty() ? "  "	: report.getAimReportContents().stream()
													.filter(r -> r.getType()
															.equals(AimReportContentType.LearningEnvironment))
													.collect(Collectors.toList()).get(0).getName();

							fileWriter.append(String.format("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,",
									'"' + report.getUser().getUsername() + '"',
									'"' + report.getUser().getName() + '"',
									'"' + report.getUser().getEmail() + '"',
									'"' + report.getUser().getGender() + '"',
									'"' + report.getUser().getGrade() + '"',
									'"' + report.getUser().getStream() + '"',
									'"' + report.getUser().getSchoolName() + '"',
									'"' + report.getUser().getCity() + '"',
									'"' + report.getUser().getAcademicScore() + '"',
									'"' + String.valueOf(getAcademicPerformanceValue(report.getUser(),
											AcademicPerformanceType.SUBJECT, SCIENCE)) + '"',
									'"' + String.valueOf(getAcademicPerformanceValue(report.getUser(),
											AcademicPerformanceType.SUBJECT, MATHEMATICS)) + '"',
									'"' + report.getUser().getSupw() + '"',
									'"' + report.getUser().getInterests() + '"',
									'"' + learningMode  + '"',
									'"' + thinkingStyle + '"',
									'"' + learningEnvironment + '"'));

//            ------------------------------------------------------------------------------------

							for (List<AimReportContent> cats : categoriesByReportSection.values())// for rawScores
							{
								cats.sort((a, b) -> (a.getName().compareToIgnoreCase(b.getName())));

								for (AimReportContent cat : cats) {
									fileWriter.append(String.format("%s,", cat.getRawScore()));
								}
								fileWriter.append(String.valueOf(reportSections.stream()
										.filter(reportSection -> reportSection.getName()
												.equals(cats.get(0).getReportSectionName()))
										.collect(Collectors.toList()).get(0).getRawScore()));
								fileWriter.append(",");

//            }
////            ------------------------------------------------------------------------------------
//            for (List<AimReportContent> cats : categoriesByReportSection.values())// for standard scores
//            {
//              cats.sort((a, b) -> (a.getName().compareToIgnoreCase(b.getName())));

								for (AimReportContent cat : cats) {
									fileWriter.append(String.format("%s,", cat.getStandardScore()));
								}
								fileWriter.append(String.valueOf(reportSections.stream()
										.filter(reportSection -> reportSection.getName()
												.equals(cats.get(0).getReportSectionName()))
										.collect(Collectors.toList()).get(0).getStandardScore()));
								fileWriter.append(",");

//            }
//
////            ------------------------------------------------------------------------------------
//            for (List<AimReportContent> cats : categoriesByReportSection.values())// for score label
//            {
//              cats.sort((a, b) -> (a.getName().compareToIgnoreCase(b.getName())));

								for (AimReportContent cat : cats) {
									fileWriter.append(String.format("%s,", cat.getScoreLabel()));
								}
								fileWriter.append(String.valueOf(reportSections.stream()
										.filter(reportSection -> reportSection.getName()
												.equals(cats.get(0).getReportSectionName()))
										.collect(Collectors.toList()).get(0).getScoreLabel()));
								fileWriter.append(",");

//            }
//
//            //            ------------------------------------------------------------------------------------
//            for (List<AimReportContent> cats : categoriesByReportSection.values())// for score label rating
//            {
//              cats.sort((a, b) -> (a.getName().compareToIgnoreCase(b.getName())));

								for (AimReportContent cat : cats) {
									fileWriter.append(String.format("%s,", cat.getScoreLabelRating().toString()));
								}
								fileWriter.append(String.valueOf(reportSections.stream()
										.filter(reportSection -> reportSection.getName()
												.equals(cats.get(0).getReportSectionName()))
										.collect(Collectors.toList()).get(0).getScoreLabelRating()));
								fileWriter.append(",");

								if (cats.get(0).getReportSectionName().equalsIgnoreCase("Top3CategoriesOfInterest")) {

									for (String interestCat : interestCategories) {
										List<AimReportContent> matchedCats = cats.stream()
												.filter(cat -> cat.getName().equals(interestCat))
												.collect(Collectors.toList());
										if (matchedCats.size() < 1) {
											fileWriter.append("0,");
										} else {
											fileWriter.append(String.valueOf(matchedCats.get(0).getScoreLabelRating()))
													.append(",");
										}
									}
								}

							}

//            ------------------------------------------------------------------------------------  careers
							for (AimReportContent career : careers) {
								fileWriter.append('"').append(career.getName()).append('"');
								fileWriter.append(",");
							}
//            ------------------------------------------------------------------------------------  subjects
							for (AimReportContent subject : subjects) {
								fileWriter.append('"').append(subject.getName()).append('"');
								fileWriter.append(",");
							}
//           --------------------------------------------------------------------------------------  leisures
							for (AimReportContent leisure : leisures) {
								fileWriter.append('"').append(leisure.getName()).append('"');
								fileWriter.append(",");
							}
							fileWriter.append("\n");
						}
					}
				}
				page++;
			}

			fileWriter.flush();
			fileWriter.close();
			File file = new File(filename);
			if (!StringUtils.isEmpty(currentUser.getEmail())) {
				String subject = "Aim2excel:Aim Data Report";
				String message = "Please find Aim Data Report in attachment";
				EmailMessage emailMessage = new EmailMessage(new String[] { currentUser.getEmail() }, subject, message,
						file);
				communicationService.sendGeneralEmail(emailMessage);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return filename;
	}

	public String downloadAimReportScoreCardInExcel(Long userId) {

		Path path = Paths.get(filedir);
		String filename = path.resolve(UUID.randomUUID().toString() + ".csv").toString();
		FileWriter fileWriter = null;

		List<AimReportContent> careers = new ArrayList<>();
		List<AimReportContent> subjects = new ArrayList<>();
		List<AimReportContent> leisures = new ArrayList<>();
		List<AimReportContent> categories = new ArrayList<>();
		List<AimReportContent> reportSections = new ArrayList<>();
		HashMap<String, List<AimReportContent>> categoriesByReportSection = new HashMap<>();

		try {
			boolean writeForHeaderFlag = true;
			fileWriter = new FileWriter(filename);

			Set<UserReportStatus> generatedReports = new HashSet<>();

			User user = userRepository.findOneByIdAndTenant(userId, getTenant());

			UserReportStatus report = userReportStatusRepository.findByUserAndTenant(user, getTenant());

			if (report == null) {
				throw new NotFoundException("user report not generated");
			}

			if (report.getAimReportContents() != null) {
				if (report.getAimReportContents().size() > 0) {

					categories.clear();
					reportSections.clear();
					careers.clear();
					subjects.clear();
					leisures.clear();

					categories = report.getAimReportContents().stream()
							.filter(content -> content.getType().equals(AimReportContentType.Category))
							.collect(Collectors.toList());

					reportSections = report.getAimReportContents().stream()
							.filter(content -> content.getType().equals(AimReportContentType.ReportSection))
							.collect(Collectors.toList());

					careers = report.getAimReportContents().stream()
							.filter(content -> content.getType().equals(AimReportContentType.Career))
							.collect(Collectors.toList());

					subjects = report.getAimReportContents().stream()
							.filter(content -> content.getType().equals(AimReportContentType.Subject))
							.collect(Collectors.toList());

					leisures = report.getAimReportContents().stream()
							.filter(content -> content.getType().equals(AimReportContentType.Leisure))
							.collect(Collectors.toList());

					categoriesByReportSection.clear();

					categories.forEach(category -> {
						if (categoriesByReportSection.containsKey(category.getReportSectionName())) {
							categoriesByReportSection.get(category.getReportSectionName()).add(category);
						} else {
							List<AimReportContent> groupedCategories = new ArrayList<>();
							groupedCategories.add(category);
							categoriesByReportSection.put(category.getReportSectionName(), groupedCategories);
						}
					});

					if (writeForHeaderFlag) {
						for (List<AimReportContent> cats : categoriesByReportSection.values()) {
							cats.sort((a, b) -> (a.getName().compareToIgnoreCase(b.getName())));

							for (AimReportContent cat : cats) {
								fileWriter.append(cat.getName()).append(",");
								fileWriter.append(String.format("%s", cat.getStandardScore()));
								fileWriter.append("\n");

							}
							fileWriter.append(String.valueOf(reportSections.stream()
									.filter(reportSection -> reportSection.getName()
											.equals(cats.get(0).getReportSectionName()))
									.collect(Collectors.toList()).get(0).getName()).toUpperCase());
							fileWriter.append(",");
							fileWriter.append(String.valueOf(reportSections.stream()
									.filter(reportSection -> reportSection.getName()
											.equals(cats.get(0).getReportSectionName()))
									.collect(Collectors.toList()).get(0).getStandardScore()));

							fileWriter.append("\n");
						}

						writeForHeaderFlag = false;

					}

//                                ------------------------------------------------------------------------------------
					for (AimReportContent career : careers) {
						fileWriter.append('"').append(career.getName()).append('"');
						fileWriter.append("\n");
					}

//            ------------------------------------------------------------------------------------
					for (AimReportContent subject : subjects) {
						fileWriter.append('"').append(subject.getName()).append('"');
						fileWriter.append("\n");
					}

					// --------------------------------------------------------------------------------------
					for (AimReportContent leisure : leisures) {
						fileWriter.append('"').append(leisure.getName()).append('"');
						fileWriter.append("\n");
					}

				}
			}

			fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return filename;
	}

	// Avneet
	public String downloadAimReportScoreCardInExcel2(Long userId) {

		Path path = Paths.get(filedir);
		String filename = path.resolve(UUID.randomUUID().toString() + ".csv").toString();
		FileWriter fileWriter = null;

		List<AimReportContent> careers = new ArrayList<>();
		List<AimReportContent> subjects = new ArrayList<>();
		List<AimReportContent> leisures = new ArrayList<>();
		List<AimReportContent> categories = new ArrayList<>();
		List<AimReportContent> reportSections = new ArrayList<>();
		HashMap<String, List<AimReportContent>> categoriesByReportSection = new HashMap<>();
		Map<String, String> userDetails = new LinkedHashMap<>();

		try {
			boolean writeForHeaderFlag = true;
			fileWriter = new FileWriter(filename);
			fileWriter.append(
					"S.NO,USERNAME, PROFILE DETAILS , RAW SCORE, STANDARD SCORE, SCORE RANGE LABEL, SCORE RANGE , COMMENTS\n");

			Set<UserReportStatus> generatedReports = new HashSet<>();

			User user = userRepository.findOneByIdAndTenant(userId, getTenant());
			UserReportStatus report = userReportStatusRepository.findByUserAndTenant(user, getTenant());

			long careerListSize = report.getAimReportContents().stream()
					.filter(r -> r.getType().equals(AimReportContentType.Career)).count();
			long leisureListSize = report.getAimReportContents().stream()
					.filter(r -> r.getType().equals(AimReportContentType.Leisure)).count();
			long subjectsListSize = report.getAimReportContents().stream()
					.filter(r -> r.getType().equals(AimReportContentType.Subject)).count();

			userDetails.put("Name", user.getName());
			userDetails.put("Email", user.getEmail());
			userDetails.put("Gender", user.getGender());
			userDetails.put("Grade", user.getGrade());
			userDetails.put("School", user.getSchoolName());
			userDetails.put("City", user.getCity());
			userDetails.put("Score", user.getAcademicScore());
			userDetails.put("Supw", user.getSupw() == null ? "" : user.getSupw());

			if (user.getAcademicPerformances().size() != 0) {
				userDetails.put("Marks in science",
						user.getAcademicPerformances().stream()
								.filter(p -> p.getName().equals(AcademicPerformanceName.SCIENCE))
								.collect(Collectors.toList()).get(0).getValue().toString());

				userDetails.put("Marks in maths",
						user.getAcademicPerformances().stream()
								.filter(p -> p.getName().equals(AcademicPerformanceName.MATHEMATICS))
								.collect(Collectors.toList()).get(0).getValue().toString());
			} else {
				userDetails.put("Marks in science", " ");
				userDetails.put("Marks in maths", " ");
			}
			userDetails.put("Stream Captured", user.getStream() == null ? "" : user.getStream());
			userDetails.put("Stream Recommended", " ");

			for (int i = 0; i < careerListSize; i++)
				userDetails.put(String.format("Career Recommendations", i),
						report.getAimReportContents().stream()
								.filter(r -> r.getType().equals(AimReportContentType.Career))
								.collect(Collectors.toList()).get(i).getName());

			for (int i = 0; i < subjectsListSize; i++)
				userDetails.put(String.format("Subjects for Careers %s", i),
						report.getAimReportContents().stream()
								.filter(r -> r.getType().equals(AimReportContentType.Subject))
								.collect(Collectors.toList()).get(i).getName());

			for (int i = 0; i < leisureListSize; i++)
				userDetails.put(String.format("Leisure For Career %s", i),
						report.getAimReportContents().stream()
								.filter(r -> r.getType().equals(AimReportContentType.Leisure))
								.collect(Collectors.toList()).get(i).getName());

			// New Keys. No data for these values yet.
            //LNT
			String learningMode = report.getAimReportContents().stream()
					.filter(r -> r.getType().equals(AimReportContentType.LearningMode)).collect(Collectors.toList())
					.isEmpty() ?  "  "  :  report.getAimReportContents().stream()
									.filter(r -> r.getType().equals(AimReportContentType.LearningMode))
									.collect(Collectors.toList()).get(0).getName();
			String thinkingStyle = report.getAimReportContents().stream()
					.filter(r -> r.getType().equals(AimReportContentType.ThinkingStyle)).collect(Collectors.toList())
					.isEmpty() ?  "  "  :  report.getAimReportContents().stream()
									.filter(r -> r.getType().equals(AimReportContentType.ThinkingStyle))
									.collect(Collectors.toList()).get(0).getName();
			String learningEnvironment = report.getAimReportContents().stream()
					.filter(r -> r.getType().equals(AimReportContentType.LearningEnvironment))
					.collect(Collectors.toList()).isEmpty() ? "  " : report.getAimReportContents().stream()
									.filter(r -> r.getType().equals(AimReportContentType.LearningEnvironment))
									.collect(Collectors.toList()).get(0).getName();

			userDetails.put("Learning Mode", learningMode );
			userDetails.put("Thinking Style", thinkingStyle );
			userDetails.put("Learning Environment", learningEnvironment );

			if (report == null) {
				throw new NotFoundException("user report not generated");
			}

			if (report.getAimReportContents() != null) {
				if (report.getAimReportContents().size() > 0) {

					categories.clear();
					reportSections.clear();
					careers.clear();
					subjects.clear();
					leisures.clear();

					categories = report.getAimReportContents().stream()
							.filter(content -> content.getType().equals(AimReportContentType.Category))
							.collect(Collectors.toList());

					reportSections = report.getAimReportContents().stream()
							.filter(content -> content.getType().equals(AimReportContentType.ReportSection))
							.collect(Collectors.toList());

//                    careers = report.getAimReportContents().stream()
//                            .filter(content -> content.getType().equals(AimReportContentType.Career))
//                            .collect(Collectors.toList());
//
//                    subjects = report.getAimReportContents().stream()
//                            .filter(content -> content.getType().equals(AimReportContentType.Subject))
//                            .collect(Collectors.toList());
//
//                    leisures = report.getAimReportContents().stream()
//                            .filter(content -> content.getType().equals(AimReportContentType.Leisure))
//                            .collect(Collectors.toList());

					categoriesByReportSection.clear();

					categories.forEach(category -> {
						if (categoriesByReportSection.containsKey(category.getReportSectionName())) {
							categoriesByReportSection.get(category.getReportSectionName()).add(category);
						} else {
							List<AimReportContent> groupedCategories = new ArrayList<>();
							groupedCategories.add(category);
							categoriesByReportSection.put(category.getReportSectionName(), groupedCategories);
						}
					});

					if (writeForHeaderFlag) {

						List<String> column1 = new ArrayList<>();
						column1.addAll(userDetails.keySet());
						int index = 0;
						for (Map.Entry<String, String> entry : userDetails.entrySet()) {
							fileWriter.append(String.format("%s,%s,", ++index, entry.getKey()));
							fileWriter.append('"').append(entry.getValue() == null ? "" : entry.getValue()).append('"');
							fileWriter.append("\n");
						}

						for (List<AimReportContent> cats : categoriesByReportSection.values()) {
							cats.sort((a, b) -> (a.getName().compareToIgnoreCase(b.getName())));

							AimReportContent reportContent = reportSections.stream()
									.filter(r -> r.getName().equals(cats.get(0).getReportSectionName()))
									.collect(Collectors.toList()).get(0);

							fileWriter.append(String.format("%s,%s,%s,%s,%s,%s,%s\n", ++index,
									reportContent.getName().toUpperCase(), " ",
									reportContent.getRawScore() == null ? " " : reportContent.getRawScore(),
									reportContent.getStandardScore(), reportContent.getScoreLabel(),
									reportContent.getScoreLabelRating()));

							for (AimReportContent cat : cats) {
								fileWriter.append(String.format("%s,%s,%s", ++index, cat.getName(), " ")).append(",");
								fileWriter.append(String.format("%s,%s,%s,%s", cat.getRawScore(),
										cat.getStandardScore(), cat.getScoreLabel(), cat.getScoreLabelRating()));
								fileWriter.append("\n");

							}
						}

						writeForHeaderFlag = false;

					}

				}
			}

			fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return filename;
	}

	public ReportSectionResponse saveReportStandardScore(ReportSectionRequest request, String language) {
		if (StringUtils.isEmpty(language)) {
			throw new ValidationException("please choose a language");
		}
		ReportSection reportSection = reportSectionRepository.findByIdAndTenantId(request.getId(), getTenant().getId());
		Norm norm = normRepository.findByReportSectionIdAndLanguageAndTenant(reportSection.getId(), language,
				getTenant());
		if (norm == null) {
			norm = new Norm();
		}
		norm.setLanguage(language);
		norm.setJuniorMeanScore(request.getJuniorMeanScore());
		norm.setJuniorStandardDeviation(request.getJuniorStandardDeviation());
		norm.setSecondaryMeanScore(request.getSecondaryMeanScore());
		norm.setSecondaryStandardDeviation(request.getSecondaryStandardDeviation());
		norm.setSeniorMeanScore(request.getSeniorMeanScore());
		norm.setSeniorStandardDeviation(request.getSeniorStandardDeviation());
		norm.setReportSection(reportSection);
		normRepository.saveAndFlush(norm);

		reportSection.setSequence(request.getSequence());
		reportSection = reportSectionRepository.save(reportSection);
		return new ReportSectionResponse(reportSection);
	}

	public void testingScheduler() {
		logger.info("i am scheduler and i am being invoked ");
	}

	public UserReportStatus getOneUserReportStatus(ReportStatus reportStatus) {
		return userReportStatusRepository.findFirstByStatus(reportStatus.name());
	}

    public UserReportStatus getOneUserReportStatusByDate(ReportStatus reportStatus, Date date) {
        return userReportStatusRepository.findFirstByStatusAndLastModifiedDateBefore(reportStatus.name(), date);
    }

	public Map<String, Long> getUserReportStatusCount(DashboardRequest request) {
		Date fromDate = com.synlabs.intscale.util.DateUtil.getFormattedDate(request.getFromDate(), START_OF_DAY);
		Date toDate = DateUtil.getFormattedDate(request.getToDate(), END_OF_DAY);
		if (fromDate == null) {
			fromDate = new LocalDate().minusDays(1).toDate();
		}
		if (toDate == null) {
			toDate = new LocalDate().toDate();
		}

		JPAQuery<User> query = new JPAQuery<>(entityManager);
		QUserReportStatus userReportStatus = QUserReportStatus.userReportStatus;
		query.select(userReportStatus).from(userReportStatus)
				.where(userReportStatus.createdDate.between(fromDate, toDate)
						.or(userReportStatus.lastModifiedDate.between(fromDate, toDate)))
				.groupBy(userReportStatus.status);

		if (!CollectionUtils.isEmpty(request.getGrades())) {
			query.where(userReportStatus.user.grade.in(request.getGrades()));
		}

		if (!CollectionUtils.isEmpty(request.getSchools())) {
			query.where(userReportStatus.user.schoolName.in(request.getSchools()));
		}

		if (!CollectionUtils.isEmpty(request.getCities())) {
			query.where(userReportStatus.user.city.in(request.getCities()));
		}

		Map<String, Long> result = query
				.transform(GroupBy.groupBy(userReportStatus.status).as(userReportStatus.count()));

		return result;

	}

	public void prepareForProcessMultipleReports(List<Long> userIds, String reportTypeString) {

		AimReportType reportType = null;
		if (!StringUtils.isEmpty(reportTypeString)) {
			try {
				reportType = AimReportType.valueOf(reportTypeString);
			} catch (IllegalArgumentException e) {
				logger.error("requested reportType not found", e);
			}
		}
		if (CollectionUtils.isEmpty(userIds)) {
			throw new ValidationException("You have not selected any user");
		}
		List<User> users = userRepository.findAllByIdInAndTenant(userIds, getTenant());
		asyncReportService.processMultipleReports(users, getTenant(), reportType);
	}

	public void shareMultipleReportsByEmail(ReportShareRequest request, boolean isSendToUser) {
		if (CollectionUtils.isEmpty(request.getUserIds())) {
			throw new ValidationException("You have not selected any user");
		}
		request.getUserIds().forEach(userId -> {
			prepareShareReport(request.getEmailId(), request.getCc(), userId, request.getReportType(), isSendToUser);
		});

	}

	public void processMultipleReportsWithUsernames(String usernames) {

		List<String> usernameList = Arrays.asList(usernames.split("\\s*,\\s*"));

        if (CollectionUtils.isEmpty(usernameList)) {
            throw new ValidationException("You have not selected any user");
        }
        List<User> users = userRepository.findAllByUsernameInAndTenant(usernameList, getTenant());
        asyncReportService.processMultipleReports(users, getTenant(), AimReportType.MAIN);
    }

	public BigInteger getAcademicPerformanceValue(User user, AcademicPerformanceType type,
			AcademicPerformanceName name) {
		BigInteger result = BigInteger.ZERO;
		if (user.getAcademicPerformances() != null) {
			Optional<AcademicPerformance> firstMatch = user.getAcademicPerformances().stream()
					.filter(academicPerformance -> academicPerformance.getType().equals(type)
							&& academicPerformance.getName().equals(name))
					.findFirst();
			return firstMatch.isPresent() ? firstMatch.get().getValue() : result;
		}
		return result;
	}

	public List<ReportSectionResponse> getAimReportDemoScores(Long userId) {
		// **************** AIM DEMO REPORT ************//
		User user;

		if (StringUtils.isEmpty(userId) || userId == 0) {
			user = getUser();
		} else {
			user = userService.getOneById(userId);
		}

		List<Test> tests = new ArrayList<>();
		List<String> testIds = new ArrayList<>();
		Map<Category, List<Question>> mapOfCategoryAndItsQuestions = new HashMap<>();
		Map<Category, List<Question>> mapOfSubCategoryAndItsQuestions = new HashMap<>();
		List<Long> userAnsweredQuestionIds = new ArrayList<>();
		List<Long> questionIdsInTest = new ArrayList<>();
		List<TestQuestion> finalTestQuestions = new ArrayList<>();

		List<Category> categories = new ArrayList<>();
		List<Category> subCategories = new ArrayList<>();
		Map<Long, CategoryResponse> mapOfCategoryIdAndMergedCategoriesAndSubCategories = new HashMap<>();
		ArrayList<CategoryResponse> mergedCategoriesAndSubCategoriesListHavingScores;
		List<ReportSectionResponse> reportSectionResponses = new ArrayList<>();
		TestResult testResult;
		ReportSectionResponse reportSectionResponse;

		/* start -***********Getting the answer key for test-based questions ********/

		HashMap<Long, String> questionIdAndTextAnswers = new HashMap<>();

		List<TextquestionAnswers> textquestionAnswers = textquestionAnswersRepository.findAllByTenant(getTenant());
		textquestionAnswers.forEach(textQue -> {
			questionIdAndTextAnswers.put(textQue.getQuestionId(), textQue.getTextAnswer());
		});

		/* ending - *************/

//****************    Initialization ends   ***************//

		Report report = getReport("Aim Report Demo");

		if (report == null) {
			throw new NotFoundException("Aim Report Demo Not Found");
		}

		if (report.getReportSections().size() == 0) {
			throw new NotFoundException("Report Sections Not Found");
		}
		for (ReportSection reportSection : report.getReportSections()) {
			reportSectionResponse = new ReportSectionResponse(reportSection);

			tests.clear();
			testIds.clear();
			Collections.addAll(testIds, StringUtils.isEmpty(reportSection.getIdsOfTestForEvaluation()) ? new String[0]
					: reportSection.getIdsOfTestForEvaluation().split(","));
			tests = getTestFromLatestTestResultOfUser(user, testIds, getTenant());

			if (tests == null || tests.size() == 0) {
				throw new NotFoundException("Report Section- " + reportSection.getName() + "'s Tests Not Found");
			}

			Boolean testsAreTaken = checkIfAllTestsAreTaken(user, tests, getTenant());

			List<CategoryResponse> mergedCategoriesAndSubCategoriesList = new ArrayList<>();

			if (testsAreTaken) {
				for (Test test : tests) {
					categories.clear();
					categories.addAll(categoryService.getCategoryByTest(test.getId()));// get categories

					subCategories.clear();
					subCategories.addAll(categoryService.getSubCategoryByTest(test.getId())); // get subCategories}

					mergedCategoriesAndSubCategoriesList.clear();
					mergedCategoriesAndSubCategoriesList = categoryService.mergeCategoriesandSubCategories(categories,
							subCategories); // merging categories and subCategories

					testResult = getTestResult(user, test.getId()); // getting user testTaken details

					List<UserAnswer> userAnswers = userAnswerRepository.findAllByTestResultAndTenant(testResult,
							getTenant());// getting all Questions responded by User

					userAnsweredQuestionIds.clear();
					userAnswers.forEach(userAnswer -> userAnsweredQuestionIds.add(userAnswer.getQuestion().getId())); // getting
																														// the
																														// QuestionIds
																														// of
																														// Questions
																														// responded
																														// by
																														// User

					questionIdsInTest.clear();
					test.getQuestions().forEach(tQ -> questionIdsInTest.add(tQ.getQuestion().getId())); // getting
																										// question ids
																										// of questions
																										// present in
																										// test

					try // filtering the Questions and selecting only User responded Questions
					{
						questionIdsInTest.retainAll(userAnsweredQuestionIds);
					} catch (NullPointerException e) {
						logger.error("Empty question list");
					}

					finalTestQuestions.clear();
					questionIdsInTest.forEach(queId -> finalTestQuestions.add(testQuestionRepository
							.findByTestIdAndQuestionIdAndTenant(test.getId(), queId, getTenant()))); // getting the
																										// derived
																										// testQuestions

//                    mapOfCategoryIdAndMergedCategoriesAndSubCategories.putAll(mergedCategoriesAndSubCategoriesList.stream().collect(
//                            Collectors.toMap(CategoryResponse::getId, categoryResponse -> categoryResponse)));

					mergedCategoriesAndSubCategoriesList.forEach(cat -> {
						CategoryResponse c = mapOfCategoryIdAndMergedCategoriesAndSubCategories.get(cat.getId());
						if (c == null) {
							mapOfCategoryIdAndMergedCategoriesAndSubCategories.put(cat.getId(), cat);
						} else {
							c.getSubCategories().clear();
							c.setSubCategories(cat.getSubCategories());

						}

					});

					mapOfCategoryAndItsQuestions.clear();
					// making a map of Category and its Questions
					for (Category category1 : categories) {
						mapOfCategoryAndItsQuestions.put(category1,
								getQuestionsByCategoryAndTest(category1, test.getId()));
					}

					mapOfSubCategoryAndItsQuestions.clear();
					// making a map of subCategory and its Questions
					for (Category subCategory1 : subCategories) {
						mapOfSubCategoryAndItsQuestions.put(subCategory1,
								getQuestionsBySubCategoryAndTest(subCategory1, test.getId()));
					}

					// TestResult tr = testResult;
					TestResult tr = getTestResult(user, test.getId());

					// for cat
					mapOfSubCategoryAndItsQuestions.forEach((Category subCategory, List<Question> questions) -> {
						setCategoryScoreInDataMap(tr, mapOfCategoryIdAndMergedCategoriesAndSubCategories, subCategory,
								questions, "INTSCALE", questionIdAndTextAnswers);
					});

					// for subcat
					mapOfCategoryAndItsQuestions.forEach((Category category, List<Question> questions) -> {
						setCategoryScoreInDataMap(tr, mapOfCategoryIdAndMergedCategoriesAndSubCategories, category,
								questions, "INTSCALE", questionIdAndTextAnswers);
					});

				}

			} else {
				throw new ValidationException("User has not taken all the necessary assessments");
			}

			mergedCategoriesAndSubCategoriesListHavingScores = new ArrayList<>(
					mapOfCategoryIdAndMergedCategoriesAndSubCategories.values());
			mapOfCategoryIdAndMergedCategoriesAndSubCategories.clear();

//            getting the mean of raw scores and and saving as one raw score for each category
			getMeanOfCategoryRawScoresAndSetZScore(mergedCategoriesAndSubCategoriesListHavingScores, user);

			reportSectionResponse.setCategories(mergedCategoriesAndSubCategoriesListHavingScores);

//           getting the overall score for reportSection
			BigDecimal s = getMeanOfCategoriesPercentileScore(reportSectionResponse, user);
			reportSectionResponse.setScore(s);

			if (reportSectionResponse.getName().equalsIgnoreCase("Careers from Interest")) {
				StringBuffer str = new StringBuffer();

				List<CategoryResponse> catgoriesForInterest = reportSectionResponse.getCategories().stream()
						.map(CategoryResponse::new).collect(Collectors.toList());

				List<String> top3CatgoriesForInterest = gettingTop3CatgoriesForInterest(catgoriesForInterest);
				if (top3CatgoriesForInterest.isEmpty() || top3CatgoriesForInterest.size() != 3) {
					throw new NotFoundException("Top 3 categories for Interest not found");
				}

				for (String name : top3CatgoriesForInterest) {
					str = str.append(name.charAt(0));

					for (CategoryResponse cat : reportSectionResponse.getCategories()) {
						if (name.equals(cat.getName())) {
							reportSectionResponse.getTop3CategoriesForInterest().add(cat);
						}
					}
				}

				List<InterestInventory> interestInventories = interestInventoryRepository.findAllByTenant(getTenant());

				String obtainedInterestCombination = str.toString();
				for (InterestInventory interestInventory : interestInventories) {
					if (interestInventory.getInterestCombination().equalsIgnoreCase(obtainedInterestCombination)) {
						reportSectionResponse
								.setInterestInventoryResponse(new InterestInventoryResponse(interestInventory));
					}
				}
			}

			reportSectionResponses.add(reportSectionResponse);
		}

		return reportSectionResponses;
	}

	public String generateDefaultReport(Long testId, Date fromdate, Date todate, String school, String grade,
			Long category, Long subCategory) throws IOException {

		Path path = Paths.get(filedir);
		logger.info(filedir);
		String filename = path.resolve(UUID.randomUUID().toString() + ".csv").toString();
		FileWriter fileWriter = new FileWriter(filename);
		fileWriter.append(DEFAULT_REPORT_FILE_HEADER);
		fileWriter.append(NEW_LINE_SEPARATOR);

		HashMap<Long, String> textQuestionsAnswersByQuestionId = getTextQuestionsAnswersByQuestionId(testId);
		HashMap<Long, TestQuestion> testQuestionsByQuestionId = getTestQuestionsByQuestionId(testId);

		int page = 1;
		int offset = 0;
		int limit = 100;

		JPAQuery<TestResult> query = getTestResultQuery(testId, fromdate, todate, school, grade);
		long totalTestResultsCount = query.fetchCount();

		while (totalTestResultsCount > offset) {
			offset = (page - 1) * limit;
			if (offset > 0) {
				query.offset(offset);
			}
			query.limit(limit);
			List<TestResult> testResults = query.fetch();
			for (int i = 0; i < testResults.size(); i++) {
				TestResult tr = testResults.get(i);

				for (int j = 0; j < tr.getUserAnswers().size(); j++) {

					UserAnswer ua = tr.getUserAnswers().get(j);
					TestQuestion testQuestion = testQuestionsByQuestionId.get(ua.getQuestion().getId());
					boolean isValid = true;

					if (category != null) {
						isValid = testQuestion.getCategory().stream().anyMatch(c -> c.getId().equals(category));
					}
					if (subCategory != null) {
						isValid = testQuestion.getSubCategory().stream()
								.anyMatch(subC -> subC.getId().equals(subCategory));
					}
					if (!isValid) {
						continue;
					}

					TestResultReportResponse response = fillTestResultReportResponse(textQuestionsAnswersByQuestionId,
							tr, ua, testQuestion);
					writeTestResultReportResponse(fileWriter, response);

				}
			}
			page++;
		}

		logger.info(filename);
		fileWriter.flush();
		fileWriter.close();

		return filename;
	}

	public void writeTestResultReportResponse(FileWriter fileWriter, TestResultReportResponse response)
			throws IOException {
		fileWriter.append(response.getTestTakenDate());
		fileWriter.append(COMMA_DELIMITER);
		fileWriter.append(response.getTestStartTime());
		fileWriter.append(COMMA_DELIMITER);
		fileWriter.append(response.getTestBeginTime());
		fileWriter.append(COMMA_DELIMITER);
		fileWriter.append(response.getTestEndTime());
		fileWriter.append(COMMA_DELIMITER);
		fileWriter.append(response.getTestName());
		fileWriter.append(COMMA_DELIMITER);
		fileWriter.append(response.getSubConstructName());
		fileWriter.append(COMMA_DELIMITER);
		fileWriter.append(response.getConstructName());
		fileWriter.append(COMMA_DELIMITER);
		fileWriter.append(response.getGrade());
		fileWriter.append(COMMA_DELIMITER);
		fileWriter.append(response.getUsername());
		fileWriter.append(COMMA_DELIMITER);
		fileWriter.append(response.getGender());
		fileWriter.append(COMMA_DELIMITER);
		fileWriter.append(response.getSchool());
		fileWriter.append(COMMA_DELIMITER);
		fileWriter.append(response.getCategory());
		fileWriter.append(COMMA_DELIMITER);
		fileWriter.append(response.getSubCategory());
		fileWriter.append(COMMA_DELIMITER);
		fileWriter.append(response.getGroupName());
		fileWriter.append(COMMA_DELIMITER);
		fileWriter.append(String.valueOf(response.getTimeTaken()));
		fileWriter.append(COMMA_DELIMITER);
		fileWriter.append(String.valueOf('"')).append(response.getQuestion()).append(String.valueOf('"'));
		fileWriter.append(COMMA_DELIMITER);
		fileWriter.append(String.valueOf(response.getQuestionId()));
		fileWriter.append(COMMA_DELIMITER);
		fileWriter.append(String.valueOf('"')).append(response.getSubTag()).append(String.valueOf('"'));
		fileWriter.append(COMMA_DELIMITER);
		fileWriter.append(String.valueOf(response.getMarks()));
		fileWriter.append(COMMA_DELIMITER);
		fileWriter.append(String.valueOf(response.getOptionNumber()));
		fileWriter.append(COMMA_DELIMITER);
		fileWriter.append(String.valueOf('"')).append(response.getAnswer()).append(String.valueOf('"'));
		fileWriter.append(COMMA_DELIMITER);
		fileWriter.append(String.valueOf('"')).append(response.getAnswerText()).append(String.valueOf('"'));
		fileWriter.append(NEW_LINE_SEPARATOR);
	}

	public TestResultReportResponse fillTestResultReportResponse(HashMap<Long, String> textQuestionsAnswersByQuestionId,
			TestResult tr, UserAnswer ua, TestQuestion testQuestion) {
		TestResultReportResponse response = new TestResultReportResponse();

		String dformat = "MM/dd/yyyy";
		String tformat = "HH:mm:ss";
		SimpleDateFormat dateFormat = new SimpleDateFormat(dformat);
		SimpleDateFormat timeFormat = new SimpleDateFormat(tformat);

		Date testTakenDate = DateUtils.addMinutes(tr.getTestFinishAt(), 330);
		Date testStartTime = DateUtils.addMinutes(tr.getTestLoadedAt(), 330);
		Date testBeginTime = DateUtils.addMinutes(tr.getTestBeginAt(), 330);
		Date testEndTime = DateUtils.addMinutes(tr.getTestFinishAt(), 330);

		response.setTestTakenDate(dateFormat.format(testTakenDate));
		response.setTestStartTime(timeFormat.format(testStartTime));
		response.setTestBeginTime(timeFormat.format(testBeginTime));
		response.setTestEndTime(timeFormat.format(testEndTime));
		response.setTestName(tr.getTest().getName());
		response.setSubConstructName(tr.getTest().getParent().getName());
		response.setConstructName(tr.getTest().getParent().getParent().getName());
		response.setGrade(tr.getUser().getGrade());
		response.setUsername(tr.getUser().getUsername());
		response.setGender(tr.getUser().getGender());
		response.setSchool(tr.getUser().getSchoolName());
		response.setGroupName(ua.getQuestion().getGroupName());
		response.setTimeTaken(ua.getTimeTaken());
		response.setQuestion(ua.getQuestion().getDescription());
		response.setQuestionId(ua.getQuestion().getId());

		response.setMarks(0);// default
		if (ua.getQuestion().getQuestionType().equals("Text")) {
			if (ua.getText().equals(textQuestionsAnswersByQuestionId.get(ua.getQuestion().getId()))) {
				response.setMarks(1);
			}
		} else {
			response.setMarks(ua.getMarks());
		}
		response.setOptionNumber(ua.getOptionNumber());
		response.setAnswer(ua.getDescription());
		response.setAnswerText(ua.getText());
		response.setCategory(getCategoryNamesInTestQuestion(testQuestion, "category"));
		response.setSubCategory(getCategoryNamesInTestQuestion(testQuestion, "subcategory"));
		response.setSubTag(getSubTagNamesInQuestion(ua.getQuestion()));

		return response;
	}

	public String getSubTagNamesInQuestion(Question question) {
		StringBuilder subTagNames = new StringBuilder();
		if (question != null) {
			if (question.getSubTags() != null) {
				question.getSubTags().forEach(subTag -> {
					subTagNames.append(subTag.getName()).append(" ");
				});
			}
		}

		return subTagNames.toString();
	}

	public String getCategoryNamesInTestQuestion(TestQuestion testQuestion, String contentType) {

		StringBuilder names = new StringBuilder();

		if (testQuestion != null) {
			switch (contentType) {
			case "category":
				testQuestion.getCategory().forEach(cat -> {
					names.append(cat.getName()).append(" ");
				});
				break;
			case "subcategory":
				testQuestion.getSubCategory().forEach(subCat -> {
					names.append(subCat.getName()).append(" ");
				});
				break;
			}
		}
		return names.toString();
	}

	public HashMap<Long, TestQuestion> getTestQuestionsByQuestionId(Long testId) {
		HashMap<Long, TestQuestion> testQuestionsByQuestionId = new HashMap<>();
		List<TestQuestion> testQuestions = testQuestionRepository.findByTestIdAndTenantOrderBySequenceAsc(testId,
				getTenant());
		testQuestions.forEach(testQuestion -> {
			testQuestionsByQuestionId.put(testQuestion.getQuestion().getId(), testQuestion);
		});
		return testQuestionsByQuestionId;
	}

	public HashMap<Long, String> getTextQuestionsAnswersByQuestionId(Long testId) {
		HashMap<Long, String> textQuestionAnswersByQuestionId = new HashMap<>();

		List<TextquestionAnswers> textquestionAnswers = textquestionAnswersRepository.findAllByTestIdAndTenant(testId,
				getTenant());
		textquestionAnswers.forEach(textQue -> {
			textQuestionAnswersByQuestionId.put(textQue.getQuestionId(), textQue.getTextAnswer());
		});

		return textQuestionAnswersByQuestionId;
	}

	public JPAQuery<TestResult> getTestResultQuery(Long testId, Date fromdate, Date todate, String school,
			String grade) {

		QTestResult testResult = QTestResult.testResult;
		JPAQuery<TestResult> query = new JPAQuery<>(entityManager);
		query.from(testResult).select(testResult);

		if (!StringUtils.isEmpty(testId) && testId > 0) {
			query.where(testResult.test.id.eq(testId));
		}
		if ((!StringUtils.isEmpty(fromdate)) && (!StringUtils.isEmpty(todate))) {
			Date fdate = DateUtils.addMinutes(fromdate, 0);
			Date tdate = DateUtils.addDays(DateUtils.addMinutes(todate, -330), 2);

			query.where(testResult.testFinishAt.between(fdate, tdate));
		}
		if (!StringUtils.isEmpty(school)) {
			query.where(testResult.user.schoolName.eq(school));
		}
		if (!StringUtils.isEmpty(grade)) {
			query.where(testResult.user.grade.eq(grade));
		}
		query.where(testResult.tenant.eq(getTenant()));

		return query;
	}
}