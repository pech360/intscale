package com.synlabs.intscale.service;

import com.synlabs.intscale.entity.test.QuestionTag;
import com.synlabs.intscale.entity.test.SubTag;
import com.synlabs.intscale.enums.TagType;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.QuestionTagRepository;
import com.synlabs.intscale.jpa.SubTagRepository;
import com.synlabs.intscale.view.question.QuestionTagRequest;
import com.synlabs.intscale.view.question.SubTagRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class QuestionTagService extends BaseService {
    @Autowired
    private QuestionTagRepository questionTagRepository;
    @Autowired
    private SubTagRepository subTagRepository;

    public List<QuestionTag> all() {
        return questionTagRepository.findAllByTenant(getTenant());
    }

    public QuestionTag saveTag(QuestionTagRequest request) {
        if (StringUtils.isEmpty(request.getName())) {
            throw new ValidationException("Tag has empty name.");
        }
        QuestionTag questionTag = request.toEntity(new QuestionTag());
        return questionTagRepository.saveAndFlush(questionTag);
    }

    public List<SubTag> allSubTag() {
        return subTagRepository.findAllByTenant(getTenant());
    }

    public QuestionTag updateTag(QuestionTagRequest request) {
        if (StringUtils.isEmpty(request.getName())) {
            throw new ValidationException(" Tag name is empty");
        }
        QuestionTag questionTag = questionTagRepository.findOne(request.getId());
        questionTag = request.toEntity(questionTag);
        return questionTagRepository.saveAndFlush(questionTag);
    }

    public SubTag saveSubTag(SubTagRequest request) {
        if (StringUtils.isEmpty(request.getName())) {
            throw new ValidationException("Sub tag name is empty");
        }
        if (request.getTagId() == null) {
            throw new ValidationException("Question Tag id is empty");
        }
        QuestionTag questionTag = questionTagRepository.findOne(request.getTagId());

        if (questionTag == null) {
            throw new ValidationException(String.format("Question[id=%s] tag not found", request.getTagId()));
        }

        SubTag subTag = request.toEntity(null);
        subTag.setTag(questionTag);
        subTagRepository.saveAndFlush(subTag);
        return subTag;
    }

    public SubTag updateSubTag(SubTagRequest request) {
        if (StringUtils.isEmpty(request.getName())) {
            throw new ValidationException("Sub tag name is empty");
        }
        if (request.getTagId() == null) {
            throw new ValidationException("Question Tag id is empty");
        }
        QuestionTag questionTag = questionTagRepository.findOne(request.getTagId());

        if (questionTag == null) {
            throw new ValidationException(String.format("Question[id=%s] tag not found", request.getTagId()));
        }

        SubTag subTag = subTagRepository.findOne(request.getId());
        subTag = request.toEntity(subTag);
        subTag.setTag(questionTag);
        subTagRepository.saveAndFlush(subTag);
        return subTag;
    }

    public List<QuestionTag> listTagsByType(TagType type) {
        List<QuestionTag> tags = new ArrayList<>();

        switch (type) {
            case COUNSELLING_SESSION:
                tags = questionTagRepository.findAllByTypeAndTenant(type, getTenant());
                break;

            case QUESTION:
            default:
                List<QuestionTag> temp = questionTagRepository.findAllByTypeAndTenant(null, getTenant());
                if (!CollectionUtils.isEmpty(temp)) {
                    tags.addAll(temp);
                }

                temp = questionTagRepository.findAllByTypeAndTenant(TagType.QUESTION, getTenant());
                if (!CollectionUtils.isEmpty(temp)) {
                    tags.addAll(temp);
                }
                break;

        }

        return tags;
    }

    public List<SubTag> listSubTagsByType(TagType type) {
        List<SubTag> subTags = new ArrayList<>();

        switch (type) {
            case COUNSELLING_SESSION:
                subTags = subTagRepository.findAllByTypeAndTenant(type, getTenant());
                break;

            case QUESTION:
            default:
                List<SubTag> temp = subTagRepository.findAllByTypeAndTenant(null, getTenant());
                if (!CollectionUtils.isEmpty(temp)) {
                    subTags.addAll(temp);
                }

                temp = subTagRepository.findAllByTypeAndTenant(TagType.QUESTION, getTenant());
                if (!CollectionUtils.isEmpty(temp)) {
                    subTags.addAll(temp);
                }
                break;
        }

        return subTags;
    }

}