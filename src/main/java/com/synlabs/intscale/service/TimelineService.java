package com.synlabs.intscale.service;

import com.synlabs.intscale.entity.Report.UserReportStatus;
import com.synlabs.intscale.entity.Timeline;
import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.enums.ReportStatus;
import com.synlabs.intscale.jpa.TimelineRepository;
import com.synlabs.intscale.jpa.UserRepository;
import com.synlabs.intscale.service.communication.CommunicationService;
import com.synlabs.intscale.view.TimelineRequest;
import com.synlabs.intscale.view.TimelineResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TimelineService extends BaseService {
    @Autowired
    private TimelineRepository timelineRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CommunicationService communicationService;

    public List<TimelineResponse> savePost(TimelineRequest request) {
        Timeline post;

        if (request.getId() == null) {
            post = request.toEntity(new Timeline());
            post.setUser(getUser());
        } else {
            post = timelineRepository.findOne(request.getId());
            post = request.toEntity(post);
        }
        timelineRepository.saveAndFlush(post);

        List<Timeline> timeline = userRepository.getOne(getUser().getId()).getTimeline();
        return timeline.stream().map(TimelineResponse::new).collect(Collectors.toList());
    }

    public Timeline savePost(TimelineRequest request, User user) {
        Timeline post;
        post = request.toEntity(new Timeline());
        post.setUser(user);
        return timelineRepository.saveAndFlush(post);

    }

    public List<TimelineResponse> getTimeLine() {
        List<Timeline> timeline = timelineRepository.findAllByUser(getUser());
        return timeline.stream().map(TimelineResponse::new).collect(Collectors.toList());
    }

    public void savePostFromSavedTest(Test test) {
        Timeline post = new Timeline();
        post.setHeading(test.getName());
        post.setDescription(test.getDescription());
        post.setContentType("test");
        post.setImageName(test.getLogo());
        post.setUser(getUser());
        timelineRepository.saveAndFlush(post);
    }


    public void savePostForAimReport(User user) {
        String contentType = "aimReport";
        int count = timelineRepository.countByUserAndContentType(user, contentType);
        if (count == 0) {
            Timeline post = new Timeline();
            post.setHeading("Bravo, you have completed all the assessments.");
            post.setDescription("");
            post.setContentType(contentType);
            post.setImageName("aimReport.png");
            post.setUser(user);

            timelineRepository.saveAndFlush(post);
        }
    }

    public void savePostForAimReport(UserReportStatus userReportStatus, String status) {
        String contentType = "aimReport";
        int count = timelineRepository.countByUserAndContentType(userReportStatus.getUser(), contentType);
        if (count == 0) {
            Timeline post = new Timeline();
            post.setHeading("Bravo, you have completed all the assessments.");
            post.setDescription("");
            post.setContentType(contentType);
            post.setImageName("aimReport.png");
            post.setUser(userReportStatus.getUser());
            userReportStatus.setStatus(status);
            timelineRepository.saveAndFlush(post);
        }
//else {
//            Timeline post = new Timeline();
//            post.setHeading("Bravo, you have completed all the assessments.");
//            post.setDescription("");
//            post.setContentType(contentType);
//            post.setImageName("aimReport.png");
//            post.setUser(userReportStatus.getUser());
//            userReportStatus.setStatus(status);
//            timelineRepository.saveAndFlush(post);
//        }
    }

    public TimelineResponse getPost(Long id) {
        return new TimelineResponse(timelineRepository.findOne(id));
    }

    public List<TimelineResponse> getImagePosts() {
        List<Timeline> timeline = timelineRepository.findAllByUserIdAndContentType(getUser().getId(), "image");
        return timeline.stream().map(TimelineResponse::new).collect(Collectors.toList());
    }

    public List<TimelineResponse> getThoughtPosts() {
        List<Timeline> timeline = timelineRepository.findAllByUserIdAndContentType(getUser().getId(), "thought");
        return timeline.stream().map(TimelineResponse::new).collect(Collectors.toList());
    }

    public void deletePost(Long id) {
        timelineRepository.delete(id);
    }

    public void sharePostOnEmail(Long id, String email, String body) {
        Timeline timeline = timelineRepository.findOne(id);
        communicationService.sharePostOnEmail(email, timeline, body);
    }

    public List<Timeline> getPostsByTypeAndUser(String reportStatus, User user) {
        return timelineRepository.findAllByUserIdAndContentType(user.getId(), reportStatus);
    }
}
