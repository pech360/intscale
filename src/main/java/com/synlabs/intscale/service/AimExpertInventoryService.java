package com.synlabs.intscale.service;

import com.synlabs.intscale.ImportModel.ImportAimExpertInventoryModel;
import com.synlabs.intscale.ImportModel.ImportCareerInterestInventoryModel;
import com.synlabs.intscale.entity.Report.*;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.ex.NotFoundException;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.*;
import com.synlabs.intscale.jpa.report.CareerInterestInventoryRepository;
import com.synlabs.intscale.view.report.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.jdo.annotations.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AimExpertInventoryService extends BaseService {
    @Autowired
    private AimExpertInventoryRepository aimExpertInventoryRepository;
    @Autowired
    private AimExpertItemRepository aimExpertItemRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ReportSectionRepository reportSectionRepository;
    @Autowired
    private SubjectRepository subjectRepository;
    @Autowired
    private ExcelImportService excelImportService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CareerInterestInventoryRepository careerInterestInventoryRepository;

    @Transactional
    public List<AimExpertInventoryResponse> saveAimExpertInventory(AimExpertInventoryRequest aimExpertInventoryRequest) {
        AimExpertInventory aimExpertInventory;
        AimExpertItem aimExpertItem;
        Category category;
        ReportSection reportSection;
        List<AimExpertItem> aimExpertItemList = new ArrayList<>();

        if (aimExpertInventoryRequest.getId() == null) {
            aimExpertInventory = new AimExpertInventory();
        } else {
            aimExpertInventory = aimExpertInventoryRepository.findByIdAndTenant(aimExpertInventoryRequest.getId(), getTenant());
        }

        aimExpertInventory = aimExpertInventoryRequest.toEntity(aimExpertInventory);
        aimExpertInventory.setTenant(getTenant());
        aimExpertInventory.getAimExpertItems().clear();
        aimExpertInventory = aimExpertInventoryRepository.save(aimExpertInventory);

        for (AimExpertItemRequest aimExpertItemRequest : aimExpertInventoryRequest.getAimExpertItems()) {
            if (aimExpertItemRequest.getId() == null) {
                aimExpertItem = new AimExpertItem();
            } else {
                aimExpertItem = aimExpertItemRepository.findByIdAndTenant(aimExpertItemRequest.getId(), getTenant());
            }
            aimExpertItem = aimExpertItemRequest.toEntity(aimExpertItem);

            try {

                category = categoryRepository.findByNameAndTenant(aimExpertItemRequest.getCategory(), getTenant());
            } catch (Exception e) {
                throw new NotFoundException(aimExpertItemRequest.getCategory() + "returns more than one");
            }
            if (category == null) {
                throw new NotFoundException(aimExpertItemRequest.getCategory() + "not found");
            }
            aimExpertItem.setCategory(category);

            reportSection = reportSectionRepository.findByNameAndTenant(aimExpertItemRequest.getReportSection(), getTenant());
            if (reportSection == null) {
                throw new NotFoundException(aimExpertItemRequest.getReportSection() + "not found");
            }
            aimExpertItem.setReportSection(reportSection);

            aimExpertItem.setTenant(getTenant());
            aimExpertItem.setAimExpertInventory(aimExpertInventory);
            aimExpertItemList.add(aimExpertItem);

        }

        aimExpertItemRepository.save(aimExpertItemList);

//    return getAimExpertInventoryList(aimExpertInventory.getType());
        return null;
    }

    public List<AimExpertInventoryResponse> getAimExpertInventoryList(String type) {
        List<AimExpertInventory> aimExpertInventoryList = aimExpertInventoryRepository.findAllByTypeAndTenant(type, getTenant());
        return aimExpertInventoryList.stream().map(AimExpertInventoryResponse::new).collect(Collectors.toList());
    }

    public List<AimExpertInventoryResponse> getaimExpertInventoryListForReport(String type, Long userId) {
        User user = userRepository.findOneByIdAndTenant(userId, getTenant());
        if (user == null) {
            throw new NotFoundException("user not found for getting career library");
        }

        if (!StringUtils.isEmpty(user.getGrade())) {
            switch (user.getGrade()) {
                case "6":
                case "7":
                case "8":
                case "9":
                case "10":
                    return getAimExpertInventoryList(type);
                case "11":
                case "12":
                    if (StringUtils.isEmpty(user.getStream()) || (!type.equals("career"))) {
                        return getAimExpertInventoryList(type);
                    } else {
                        return getAimExpertInventoryFilteredByStream(user.getStream(), type);
                    }
                default:
                    throw new ValidationException("User's grade is missing");
            }
        }
        return getAimExpertInventoryList(type);
    }

    public List<AimExpertInventoryResponse> getAimExpertInventoryFilteredByStream(String stream, String type) {
        if (StringUtils.isEmpty(stream)) {
            throw new NotFoundException("user's stream not found");
        }

        List<AimExpertInventory> filteredList = new ArrayList<>();

        List<AimExpertInventory> aimExpertInventories = aimExpertInventoryRepository.findAllByTypeAndTenant(type, getTenant());

        aimExpertInventories.forEach(aimExpertInventory -> {
            if (aimExpertInventory.getStreams().contains(stream)) {
                filteredList.add(aimExpertInventory);
            }
        });

        return filteredList.stream().map(AimExpertInventoryResponse::new).collect(Collectors.toList());
    }

    public void saveCareerMatrix(AimExpertInventoryRequest m, String type) {
    /* this function can be used for creating matrix with-
                                                          =>career-subject
                                                                 or
                                                          =>career-hobby
       depending on type
                                                          */

        AimExpertInventory career = aimExpertInventoryRepository.findByIdAndTenant(m.getId(), getTenant());

        List<SubjectRequest> itemRequests = m.getSubjects();

        itemRequests.forEach(itemRequest -> {
            Subject subject;
            if (itemRequest.getId() == null) {
                subject = new Subject();
            } else {
                subject = subjectRepository.findByIdAndTenant(itemRequest.getId(), getTenant());
            }

            AimExpertInventory subjectAimExpertInventory = aimExpertInventoryRepository.findByNameAndTenant(itemRequest.getName(), getTenant());
            subject = itemRequest.toEntity(subject);
            subjectAimExpertInventory.setHobbies(null);
            subjectAimExpertInventory.setSubjects(null);
            subject.setAimExpertInventory(subjectAimExpertInventory);
            subject.setTenant(getTenant());

//      subject.setAimExpertInventory(career);

            subject = subjectRepository.saveAndFlush(subject);

            List<Subject> items = new ArrayList<>();

            if (type.equalsIgnoreCase("subject")) {
                items = career.getSubjects();
            }
            if (type.equalsIgnoreCase("leisure")) {
                items = career.getHobbies();
            }
            boolean isAlreadyExist = false;
            int index = 0;
            for (int i = 0; i < items.size(); i++) {
                if (items.get(i).getAimExpertInventory().getName().equalsIgnoreCase(subject.getAimExpertInventory().getName())) {
                    isAlreadyExist = true;
                    index = i;
                }

            }
            if (isAlreadyExist) {
                items.remove(index);
                items.add(index, subject);
            } else {
                items.add(subject);
            }

            if (type.equalsIgnoreCase("subject")) {
                career.setSubjects(items);
            }
            if (type.equalsIgnoreCase("leisure")) {
                career.setHobbies(items);
            }
        });

        aimExpertInventoryRepository.saveAndFlush(career);

    }

    public List<SubjectResponse> getSubjects() {
        List<Subject> subjects = subjectRepository.findAllByTenant(getTenant());
        return subjects.stream().map(SubjectResponse::new).collect(Collectors.toList());
    }

    public List<String> getAimExpertInventoryNameList(String type) {
        List<String> aimExpertInventoryNames = new ArrayList<>();
        List<AimExpertInventory> aimExpertInventoryList = aimExpertInventoryRepository.findAllByTypeAndTenant(type, getTenant());
        aimExpertInventoryList.forEach(aimExpertInventory -> {
            aimExpertInventoryNames.add(aimExpertInventory.getName());
        });

        return aimExpertInventoryNames;
    }

    public AimExpertInventoryResponse getAimExpertInventoryItem(String name) {
        AimExpertInventory aimExpertInventory = aimExpertInventoryRepository.findByNameAndTenant(name, getTenant());
        return new AimExpertInventoryResponse(aimExpertInventory);
    }

    public void attachAimExpertInventoryImage(String filename, Long id) {
        AimExpertInventory aimExpertInventory = aimExpertInventoryRepository.findByIdAndTenant(id, getTenant());
        aimExpertInventory.setImageName(filename);
        aimExpertInventoryRepository.saveAndFlush(aimExpertInventory);
    }

    public AimExpertInventoryResponse getAimExpertInventoryItem(Long id) {
        AimExpertInventory aimExpertInventory = aimExpertInventoryRepository.findByIdAndTenant(id, getTenant());
        return new AimExpertInventoryResponse(aimExpertInventory);
    }

    public Map<String, String> importAimExpertInventory(MultipartFile file, String dryRun) {

        boolean errorInFile = false;
        String validationMessage;
        Map<String, String> statusMap = new HashMap<>();
        statusMap.put("error", "false");
        Map<String, ArrayList> aimExpertInventoryRecords = excelImportService.importAimExpertInventoryRecords(file);
        ArrayList<ImportAimExpertInventoryModel> importAimExpertInventoryModels = aimExpertInventoryRecords.get("aimExpertInventories");


        for (ImportAimExpertInventoryModel importAimExpertInventoryModel : importAimExpertInventoryModels) {
            validationMessage = importAimExpertInventoryModel.validate();
            if (!validationMessage.equalsIgnoreCase("ok")) {
                statusMap.put(importAimExpertInventoryModel.getName().trim(), validationMessage);
                errorInFile = true;
            }

            AimExpertInventory aimExpertInventory = aimExpertInventoryRepository.findByNameAndTenant(importAimExpertInventoryModel.getName().trim(), getTenant());
            if (aimExpertInventory == null) {
                statusMap.put(importAimExpertInventoryModel.getName().trim(), "not found in database, create first.");
                errorInFile = true;
            }
        }

        if (!errorInFile && dryRun.equalsIgnoreCase("false")) {
            for (ImportAimExpertInventoryModel importAimExpertInventoryModel : importAimExpertInventoryModels) {
                AimExpertInventory aimExpertInventory = convertImportAimExpertModelToAimExpertInventory(importAimExpertInventoryModel);
                aimExpertInventoryRepository.saveAndFlush(aimExpertInventory);
            }
        }

        if (errorInFile) {
            statusMap.put("error", "true");
        }
        return statusMap;
    }

    private AimExpertInventory convertImportAimExpertModelToAimExpertInventory(ImportAimExpertInventoryModel importAimExpertInventoryModel) {
        AimExpertInventory aimExpertInventory = aimExpertInventoryRepository.findByNameAndTenant(importAimExpertInventoryModel.getName().trim(), getTenant());
        if (aimExpertInventory == null) {
            throw new NotFoundException(importAimExpertInventoryModel.getName() + " not found");
        }
        if (!StringUtils.isEmpty(importAimExpertInventoryModel.getDisplayName())) {
            aimExpertInventory.setDisplayName(importAimExpertInventoryModel.getDisplayName().trim());
        }
        if (!StringUtils.isEmpty(importAimExpertInventoryModel.getDescription())) {
            aimExpertInventory.setDescription(importAimExpertInventoryModel.getDescription().trim());
        }
        if (!StringUtils.isEmpty(importAimExpertInventoryModel.getSummary())) {
            aimExpertInventory.setSummary(importAimExpertInventoryModel.getSummary().trim());
        }
        if (!StringUtils.isEmpty(importAimExpertInventoryModel.getTasks())) {
            aimExpertInventory.setTasks(importAimExpertInventoryModel.getTasks().trim());
        }
        if (!StringUtils.isEmpty(importAimExpertInventoryModel.getScope())) {
            aimExpertInventory.setScope(importAimExpertInventoryModel.getScope().trim());
        }
        if (!StringUtils.isEmpty(importAimExpertInventoryModel.getEducation())) {
            aimExpertInventory.setEducation(importAimExpertInventoryModel.getEducation().trim());
        }
        if (!StringUtils.isEmpty(importAimExpertInventoryModel.getTopRecruiters())) {
            aimExpertInventory.setTopRecruiters(importAimExpertInventoryModel.getTopRecruiters());
        }
        if (!StringUtils.isEmpty(importAimExpertInventoryModel.getTopCollegesOfIndia())) {
            aimExpertInventory.setTopCollegesOfIndia(importAimExpertInventoryModel.getTopCollegesOfIndia().trim());
        }
        if (!StringUtils.isEmpty(importAimExpertInventoryModel.getTopCollegesOfAbroad())) {
            aimExpertInventory.setTopCollegesOfAbroad(importAimExpertInventoryModel.getTopCollegesOfAbroad().trim());
        }
        if (!StringUtils.isEmpty(importAimExpertInventoryModel.getStreams())) {
            aimExpertInventory.setStreams(importAimExpertInventoryModel.getStreams());
        }

        return aimExpertInventory;
    }

    public Map<String, String> importCareerInterestInventory(MultipartFile file, String dryRun) {

        boolean errorInFile = false;
        String validationMessage;
        Map<String, String> statusMap = new HashMap<>();
        statusMap.put("error", "false");
        Map<String, ArrayList> importCareerInterestInventory = excelImportService.importCareerInterestInventory(file);
        ArrayList<ImportCareerInterestInventoryModel> importCareerInterestInventoryModels = importCareerInterestInventory.get("careerInterestInventory");

        for (int i = 0; i < importCareerInterestInventoryModels.size(); i++) {
            ImportCareerInterestInventoryModel importCareerInterestInventoryModel = importCareerInterestInventoryModels.get(i);
            validationMessage = importCareerInterestInventoryModel.validate();
            statusMap.put(i + 1 + "", validationMessage);
            if (!validationMessage.equalsIgnoreCase("OK")) {
                errorInFile = true;
            }
        }

        if (errorInFile) {
            statusMap.put("error", "true");
        }

        if (!errorInFile && dryRun.equalsIgnoreCase("false")) {
            for (int i = 0; i < importCareerInterestInventoryModels.size(); i++) {
                ImportCareerInterestInventoryModel model = importCareerInterestInventoryModels.get(i);

                CareerInterestInventory careerInterest = careerInterestInventoryRepository.findByCareerName(model.getCareerName());
                careerInterest = model.toEntity(careerInterest);
                careerInterestInventoryRepository.saveAndFlush(careerInterest);
            }
        }
        return statusMap;
    }

    public List<ImportCareerInterestInventoryModel> getCareerInterestInventory() {
        List<CareerInterestInventory> careerInterestInventory = careerInterestInventoryRepository.findAll();
        return careerInterestInventory.stream().map(ImportCareerInterestInventoryModel::new).collect(Collectors.toList());
    }
}
