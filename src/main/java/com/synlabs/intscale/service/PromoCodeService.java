package com.synlabs.intscale.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.synlabs.intscale.entity.PromoCode;
import com.synlabs.intscale.entity.PromocodeProduct;
import com.synlabs.intscale.entity.paymentGateway.TransactionEntry;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.ex.NotFoundException;
import com.synlabs.intscale.ex.PromocodeSuccess;
import com.synlabs.intscale.ex.UploadException;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.PromoCodeRepository;
import com.synlabs.intscale.jpa.PromocodeProductRepository;
import com.synlabs.intscale.jpa.TransactionEntryRepository;
import com.synlabs.intscale.jpa.UserRepository;
import com.synlabs.intscale.util.CompositePromoCodeProductKey;
import com.synlabs.intscale.util.ProductName;
import com.synlabs.intscale.view.PromoCodeRequest;
import com.synlabs.intscale.view.PromoCodeResponse;
import com.synlabs.intscale.view.PromoCodeUpdateRequest;
import com.synlabs.intscale.view.UserPromoCodeResponse;
//LNT
@Service
public class PromoCodeService extends BaseService {
	private static Logger logger = LoggerFactory.getLogger(PromoCodeService.class);
	@Autowired
	private PromoCodeRepository promoCodeRepository;
	@Autowired
	private PromocodeProductRepository promocodeProductRepository;
	@Autowired
	private TransactionEntryRepository transactionEntryRepository;
	@Autowired
	private UserRepository userRepository;

	public List<PromoCodeResponse> getAllAvailableCodes() {
		List<PromoCode> promoCodeList = promoCodeRepository.findAll();
		if (promoCodeList == null)
			throw new NotFoundException("No Promo Codes Available");
		List<PromoCodeResponse> promoCodeResponseViewList = new ArrayList<>();
		for (PromoCode promoCode : promoCodeList) {
			promoCodeResponseViewList.add(new PromoCodeResponse(promoCode,
					promocodeProductRepository.findAllProductNameByPromoCodeId(promoCode.getId())));
		}
		return promoCodeResponseViewList;
	}

	public List<PromoCodeResponse> getAllValidAvailableCodes() {
		List<PromoCode> promoCodeList = promoCodeRepository.findAllIfIsValid();
		if (promoCodeList == null)
			throw new NotFoundException("No Promo Codes Live..");
		List<PromoCodeResponse> promoCodeResponseViewList = new ArrayList<>();
		for (PromoCode promoCode : promoCodeList) {
			promoCodeResponseViewList.add(new PromoCodeResponse(promoCode,
					promocodeProductRepository.findAllProductNameByPromoCodeId(promoCode.getId())));
		}
		return promoCodeResponseViewList;
	}

	public PromoCodeResponse findPromoCode(String name) {
		PromoCode promoCode = promoCodeRepository.findByName(name.toUpperCase());
		if (promoCode == null)
			throw new NotFoundException("Promo Code does not exist.");
		return new PromoCodeResponse(promoCode,
				promocodeProductRepository.findAllProductNameByPromoCodeId(promoCode.getId()));
	}

	public PromoCodeResponse checkIfValid(Long id) {
		PromoCode promoCode = promoCodeRepository.findOne(id);
		if (promoCode == null) {
			throw new NotFoundException("Promo Code  does not exists");
		}
		promoCode = promoCodeRepository.findIfIsValid(promoCode.getName());
		if (promoCode == null) {
			throw new ValidationException("Promo Code Invalid");
		}
		return new PromoCodeResponse(promoCode,
				promocodeProductRepository.findAllProductNameByPromoCodeId(promoCode.getId()));
	}

	public PromoCodeResponse createPromoCode(PromoCodeRequest promoCodeRequest) {
		if (promoCodeRequest.getCouponStartDate().after(promoCodeRequest.getCouponEndDate()))
			throw new ValidationException("PromoCode Start date should be smaller than PromoCode End date");
		promoCodeRequest.setName(promoCodeRequest.getName().toUpperCase());
		PromoCode promoCode = promoCodeRepository.findByName(promoCodeRequest.getName());
		if (promoCode != null) {
			logger.warn("Promo Code already exists {} ", promoCode.getName());
			throw new ValidationException("Promo Code already exists");
		}
		promoCode = promoCodeRepository.save(promoCodeRequest.toEntity());
		if (promoCode == null) {
			throw new UploadException("Promo Code Not Created..");
		}
		List<String> productNamesList = promoCodeRequest.getProductName();
		for (String product : productNamesList) {
			promocodeProductRepository.save(new PromocodeProduct(
					new CompositePromoCodeProductKey(promoCode.getId(), ProductName.valueOf(product.toUpperCase())),
					promoCode));
		}
		return new PromoCodeResponse(promoCode,
				promocodeProductRepository.findAllProductNameByPromoCodeId(promoCode.getId()));
	}

	public PromoCodeResponse updateIsValid(Long id, Boolean isValid) {
		PromoCode promoCode = promoCodeRepository.findOne(id);
		if (promoCode == null)
			throw new NotFoundException("Promo Code Not Found ");
		if ( promoCodeRepository.updatePromoCodeSetIsValid(isValid, id) == 0)
			throw new UploadException("Is Valid already " + isValid);
		return new PromoCodeResponse(promoCode,
				promocodeProductRepository.findAllProductNameByPromoCodeId(promoCode.getId()));
	}

	public PromoCodeResponse update(PromoCodeUpdateRequest promoCodeUpdateRequest, Long id) throws ParseException {
		if (promoCodeUpdateRequest.getCouponStartDate() != null && promoCodeUpdateRequest.getCouponEndDate() != null
				&& promoCodeUpdateRequest.getCouponStartDate().after(promoCodeUpdateRequest.getCouponEndDate())) {
			throw new ValidationException("PromoCode Start date should be smaller than PromoCode End date");
		}

		PromoCode promoCode = promoCodeRepository.findOne(id);
		if (promoCode == null)
			throw new NotFoundException("Promo Code Does Not Exist");
		promoCode = promoCodeRepository.save(promoCodeUpdateRequest.toEntity(promoCode));
		if (promoCodeUpdateRequest.getProductName() == null || promoCodeUpdateRequest.getProductName().isEmpty()) {
			throw new ValidationException("Product name can't be empty");
		}

		List<String> te = transactionEntryRepository
				.findDistinctProductNameByPromoCodeName(promoCode.getName().toUpperCase());
		for (String product : promoCodeUpdateRequest.getProductName()) {
			if (ProductName.lookup.get(product) == null) {
				throw new ValidationException("Product not found");
			}
			te.remove(product);
		}
		if (!te.isEmpty()) {
			throw new ValidationException(
					"This Promocode has been used already on this Product . So you can not delete this product.");
		}

		List<String> products = promoCodeUpdateRequest.getProductName();

		promocodeProductRepository.deleteByKeyPromoCodeId(promoCode.getId());

		for (String product : products) {
			promocodeProductRepository.save(new PromocodeProduct(
					new CompositePromoCodeProductKey(promoCode.getId(), ProductName.lookup.get(product)), promoCode));
		}
		PromoCodeResponse response = new PromoCodeResponse(promoCode);
		response.setProductName(products);
		return response;
	}

	public PromoCodeResponse applyPromoCode(String name, String productName) {
		List<PromocodeProduct> promocodeProductList = new ArrayList<>();
		PromoCode promoCode = promoCodeRepository.findByName(name.toUpperCase());
		if (promoCode == null)
			throw new NotFoundException("Promo Code does not exist ");
		promoCode = promoCodeRepository.findIfIsValid(promoCode.getName().toUpperCase());
		if (promoCode == null)
			throw new ValidationException("Promo Code Expired ");
		promocodeProductList = promocodeProductRepository.findAllProductNameByPromoCodeId(promoCode.getId());
		Boolean flag = false;
		for (PromocodeProduct promocodeProduct : promocodeProductList) {
			if (productName.equalsIgnoreCase(promocodeProduct.getKey().getProductname().toString()) == true) {
				flag = true;
				break;
			}
		}
		if (flag == false)
			throw new ValidationException("Promo Code not applicable on this product ");
		if (transactionEntryRepository.existsByUserIdAndProductNameAndPromoCodeName(this.getUser().getId(), productName,
				name))
			throw new ValidationException("Promo Code already used for this product ");
		PromoCodeResponse response = new PromoCodeResponse();
		response.setDiscount(promoCode.getDiscount());
		return response;
	}

	public List<PromoCodeResponse> getAllPromoCodeFromDateToDate(String sDate, String eDate) {
		Date startDate = null, endDate = null;
		try {
			startDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(sDate);
			endDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(eDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (startDate.after(endDate))
			throw new ValidationException("Start Date can not be greater than end date .");
		List<PromoCode> promoCodeList = new ArrayList<>();
		List<PromoCodeResponse> promoCodeResponseViewList = new ArrayList<>();
		promoCodeList = promoCodeRepository.getAllPromoCodeFromDateToDate(startDate, endDate);
		if (promoCodeList == null || promoCodeList.isEmpty())
			throw new NotFoundException("No Promo Codes found between specified dates .");
		for (PromoCode promo : promoCodeList) {
			promoCodeResponseViewList.add(new PromoCodeResponse(promo,
					promocodeProductRepository.findAllProductNameByPromoCodeId(promo.getId())));
		}
		return promoCodeResponseViewList;
	}

	public List<UserPromoCodeResponse> getAllUsersOfPromoCode(Long promoCodeId) {
		List<TransactionEntry> transactionEntryList = transactionEntryRepository
				.findAllByPromoCodeName(promoCodeRepository.findOne(promoCodeId).getName().toUpperCase());
		if (transactionEntryList == null || transactionEntryList.isEmpty())
			throw new NotFoundException("No User used this PromoCode yet.");
		List<UserPromoCodeResponse> userPromoCodeResponseList = new ArrayList<>();
		for (TransactionEntry transactionEntry : transactionEntryList) {
			UserPromoCodeResponse userPromoCodeResponse = new UserPromoCodeResponse(userRepository.findOne(transactionEntry.getUserId()));
			userPromoCodeResponse.setDate(transactionEntry.getCreatedAt());
			userPromoCodeResponse.setProductPurchased(transactionEntry.getProductName());
			userPromoCodeResponseList.add(userPromoCodeResponse);
		}
		return userPromoCodeResponseList;
	}

}
