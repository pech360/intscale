package com.synlabs.intscale.service;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.plus.Plus;
import com.google.api.services.plus.model.Person;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.mysema.commons.lang.URLEncoder;
import com.querydsl.jpa.impl.JPAQuery;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.Version;
import com.synlabs.intscale.ImportModel.ImportUserModel;
import com.synlabs.intscale.ImportModel.ImportUserTestAssignModel;
import com.synlabs.intscale.auth.IntScaleAuth;
import com.synlabs.intscale.common.Menu;
import com.synlabs.intscale.entity.CurrentUser;
import com.synlabs.intscale.entity.PromoCode;
import com.synlabs.intscale.entity.masterdata.MasterDataDocument;
import com.synlabs.intscale.entity.paymentGateway.TransactionEntry;
import com.synlabs.intscale.entity.test.TenantTestAllotment;
import com.synlabs.intscale.entity.token.PasswordResetToken;
import com.synlabs.intscale.entity.user.*;
import com.synlabs.intscale.enums.*;
import com.synlabs.intscale.ex.AuthException;
import com.synlabs.intscale.ex.NotFoundException;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.*;
import com.synlabs.intscale.service.communication.CommunicationService;
import com.synlabs.intscale.store.FileStore;
import com.synlabs.intscale.util.DateUtil;
import com.synlabs.intscale.util.StringUtil;
import com.synlabs.intscale.view.*;
import com.synlabs.intscale.view.paymentGateway.TResponse;
import com.synlabs.intscale.view.paymentGateway.TransactionResponse;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.PROMO_CODE;
import static com.synlabs.intscale.util.DateUtil.DayTime.END_OF_DAY;
import static com.synlabs.intscale.util.DateUtil.DayTime.START_OF_DAY;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class UserService extends BaseService {

private static Logger logger = LoggerFactory.getLogger(UserService.class);

@Autowired
private TransactionEntryRepository transactionEntryRepository;

@Autowired
private RestTemplate restTemplate;

@Autowired
private UserRepository userRepository;

@Autowired
private TestRepository testRepository;

@Autowired
private TeacherRepository teacherRepository;

@Autowired
private ExcelImportService excelImportService;

@Autowired
private RoleRepository roleRepository;

@Autowired
private PrivilegeRepository privilegeRepository;

@Autowired
private TenantRepository tenantRepository;

@Autowired
private TenantTestAllotmentRepository testAllotmentRepository;

@Autowired
private UserMenuBuilder menuBuilder;

@Autowired
private CommunicationService communicationService;

@Autowired
private PasswordResetTokenRepository passwordResetTokenRepository;

@Autowired
private StudentRepository studentRepository;

@Autowired
private SchoolRepository schoolRepository;

@Autowired
DashboardRepository dashboardRepository;

@Autowired
private TestService testService;

@Autowired
private FileStore store;

@Autowired
private ProductService productService;

@Autowired
private MasterDataDocumentRepository masterDataDocumentRepository;

@Autowired
private DashboardService dashboardService;

@Autowired
private AcademicPerformanceRepository academicPerformanceRepository;

@Autowired
private AsyncUserService asyncUserService;

@Autowired
private PromoCodeRepository promoCodeRepository;

@Value("${intscale.auth.ttl_hours}")
private int validTill;

@Value("${scale.image.upload.location}")
protected String filedir;

@Value("${intscale.payment.key_id}")
public String keyId;

@Value("${intscale.payment.key_secret}")
public String keySecret;

@Value("${intscale.payment.basicToken}")
public String basicToken;

private final SecureRandom sr = new SecureRandom();

private PasswordEncoder encoder = new BCryptPasswordEncoder();
private GoogleIdTokenVerifier verifier;

private Cache<String, Menu> menuCache = CacheBuilder.newBuilder().build();
private com.restfb.types.User fbProfile;

@PersistenceContext
private EntityManager entityManager;
String passCode;

@PostConstruct
public void init() {

fbProfile = new com.restfb.types.User();

NetHttpTransport transport = new NetHttpTransport();
JacksonFactory factory = new JacksonFactory();
verifier = new GoogleIdTokenVerifier.Builder(transport, factory).build();

// Iterate through the privilege list and add it to the database!

Field[] fields = IntScaleAuth.Privileges.class.getDeclaredFields();
for (Field f : fields) {
if (Modifier.isStatic(f.getModifiers()) && Modifier.isFinal(f.getModifiers())) {
logger.info("Found privilege {} ", f.getName());
Privilege p = privilegeRepository.findByName(f.getName());

if (p == null) {
p = new Privilege();
p.setName(f.getName());
p.setDescription(f.getName());
privilegeRepository.save(p);

// attach it to tech admin role:)
Role role = roleRepository.getOneByName("Admin");
if (role == null) {
role = new Role();
role.setName("Admin");
}
role.addPrivilege(p);
roleRepository.save(role);
logger.info("attached to  admin, saved {}", f.getName());
}
}
}

Role role = roleRepository.getOneByName("Stakeholder");
if (role == null) {
role = new Role();
role.setName("Stakeholder");
roleRepository.saveAndFlush(role);
}

fields = IntScaleAuth.Dashboard.class.getDeclaredFields();
for (Field f : fields) {
if (Modifier.isStatic(f.getModifiers()) && Modifier.isFinal(f.getModifiers())) {
logger.info("Found Dashboard Card {} ", f.getName());
Dashboard d = dashboardRepository.findByName(f.getName());
if (d == null) {
d = new Dashboard();
d.setName(f.getName());
d.setDescription(f.getName());
dashboardRepository.save(d);
}
}
}
}

public CurrentUser loadUserByUsername(String username, String host) {
Tenant tenant = tenantRepository.findByActiveIsTrueAndSubDomain(host);
if (tenant == null) {
return null;
}
User user = userRepository.findByUsernameAndTenantOrEmailAndTenant(username, tenant, username, tenant);
if (user == null) {
logger.warn("user not found {}", username);
throw new UsernameNotFoundException(String.format("User %s was not found", username));
}
return new CurrentUser(user, new String[] { "zz" });
}

private Person userDetailsFromGoogle(String token) {
try {
GoogleCredential credential = new GoogleCredential().setAccessToken(token);
Plus plus = new Plus.Builder(new NetHttpTransport(), new JacksonFactory(), credential)
.setApplicationName("assess.aim2excel.in").build();
return plus.people().get("me").execute();
} catch (IOException e) {
logger.error("Error in getting user details from google", e);
return null;
}
}

public User register(SocialLoginRequest login, String host) {
Tenant tenant = tenantRepository.findByActiveIsTrueAndSubDomain(host);

if (StringUtils.isEmpty(login.getEmail())) {
return null;
}
if (StringUtils.isEmpty(login.getToken())) {
return null;
}
if (StringUtils.isEmpty(tenant)) {
return null;
}
boolean result = false;
Person googleProfile = new Person();

switch (login.getProvider()) {
case "google":
result = validateGoogle(login);
if (result) {
googleProfile = userDetailsFromGoogle(login.getToken());
}
break;
case "facebook":
fbProfile = validateFacebook(login);
result = fbProfile != null;
break;
}

if (result) {
if (userRepository.countByEmailAndTenant(login.getEmail(), tenant) > 0) {
throw new ValidationException("User is already registered with this email!");
}
Pair<String, String> name = splitName(login.getName());
User user = new User();
user.setEmail(login.getEmail());
user.setTenant(tenant);
user.setGrade("8");
String password = RandomStringUtils.random(8, true, true); // assign random password
user.setPasswordHash(encoder.encode(password));
user.setName(StringUtil.capitalizeWord(login.getName()));
// user.setContactValidate(false); //change

DateFormat formatter = new SimpleDateFormat("yyyy-MM-DD");

if (login.getProvider().equals("google")) {
user.setRegisterType(RegisterType.GOOGLE.getType());
if (googleProfile != null) {
if (googleProfile.getGender() != null) {
user.setGender(googleProfile.getGender());
}
try {
if (googleProfile.getBirthday() != null) {
Date dob = formatter.parse(googleProfile.getBirthday());
user.setDob(dob);
}
} catch (ParseException e) {
logger.error("Error in converting Google Dob in Intscale date format", e);
}
}
} else if (login.getProvider().equals("facebook")) {
user.setRegisterType(RegisterType.FACEBOOK.getType());
if (fbProfile != null) {
if (fbProfile.getGender() != null) {
user.setGender(fbProfile.getGender());
}
try {
if (fbProfile.getBirthday() != null) {
Date dob = formatter.parse(fbProfile.getBirthday());
user.setDob(dob);
}
} catch (ParseException e) {
logger.error("Error in converting Facebook Dob in Intscale date format", e);
}
}
}

Path path = Paths.get(filedir);
String filename = UUID.randomUUID().toString() + ".jpg";
File fullfilename = new File(path.resolve(filename).toString());
String imageUrl = login.getImageUrl();

try {
saveImage(imageUrl, fullfilename);
user.setDisplayPicture(filename);
} catch (Exception exp) {
logger.error("Error in saving social profile image", exp);
}

int rnd = sr.nextInt(10000);
rnd = rnd < 0 ? -rnd : rnd;
String username = String.format("%s%04d", name.getFirst(), (rnd));
if (userRepository.countByUsernameAndTenant(username, tenant) > 0) {
throw new ValidationException("This username is already registered");
}
user.setUsername(username);
Role defaultRole = roleRepository.getOneByName("TestTaker");
List<Role> defaultRoleList = new ArrayList<>();
if (defaultRole != null) {
defaultRoleList.add(defaultRole);
}
if (!defaultRoleList.isEmpty()) {
user.setRoles(defaultRoleList);
}
user = userRepository.save(user);
Student student = saveStudent(user);
if (student != null) {
studentRepository.save(student);
}
communicationService.registerWelcomeEmail(user, password, tenant.getSubDomain());
return user;
}
return null;
}

private Pair<String, String> splitName(String name) {
String[] splitted = name.split(" ");
return Pair.of(splitted[0], splitted.length > 1 ? splitted[1] : null);
}

public User validate(SocialLoginRequest login, String host) {
if (StringUtils.isEmpty(login.getEmail())) {
return null;
}
if (StringUtils.isEmpty(login.getToken())) {
return null;
}
if (StringUtils.isEmpty(host)) {
return null;
}
logger.info("Token is {}", login.getToken());
logger.info("Id Token is {}", login.getIdToken());

boolean result = false;
switch (login.getProvider()) {
case "google":
result = validateGoogle(login);
break;
case "facebook":
fbProfile = validateFacebook(login);
result = fbProfile != null;
break;
}

if (result) {
Tenant tenant = tenantRepository.findByActiveIsTrueAndSubDomain(host);
if (tenant == null) {
throw new AuthException("Invalid host");
}
if (!host.equalsIgnoreCase(tenant.getSubDomain())) {
logger.error("Invalid host !" + login.getEmail(), " tried to login at Host " + host);
throw new AuthException("Invalid host");
}
User user = userRepository.findByEmailAndTenant(login.getEmail(), tenant);
if (user == null) {
return register(login, host); // user not found in db, doing register and continue to login
} else {
return user; // user found, continue to login
}
}
return null;
}

public User validate(String username, String password, String host) {
Tenant tenant = tenantRepository.findByActiveIsTrueAndSubDomain(host);
if (tenant == null) {
return null;
}
User user = userRepository.findByUsernameAndTenant(username, tenant);
if (user == null) {
user = userRepository.findByEmailAndTenant(username, tenant);
}
if (user != null && encoder.matches(password, user.getPasswordHash())) {
return user;
}
return null;
}

public User getUser(String username) {
return userRepository.findByUsernameAndTenant(username, getTenant());
}

public String generateOTP() {
String numbers = "0123456789";
int len = 6;
String otpCode = "";
Random rndm_method = new Random();
char[] otp = new char[len];
int i = 0;
while (i < len) {
otp[i] = numbers.charAt(rndm_method.nextInt(numbers.length()));
otpCode = String.valueOf(otpCode) + otp[i];
++i;
}
return otpCode;
}

public User register(RegisterRequest request, String host) {
// User userExist = null;

if (StringUtils.isEmpty(request.getFirstname())) {
throw new ValidationException("Missing firstname");
}

if (StringUtils.isEmpty(request.getEmail())) {
throw new ValidationException("Missing email");
}

//    if (StringUtils.isEmpty(request.getPassword()))
//    {
//      throw new ValidationException("Missing password");
//    }
//
//    if (StringUtils.isEmpty(request.getRepassword()))
//    {
//      throw new ValidationException("Missing re-password");
//    }
//
//    if (StringUtils.isEmpty(request.getUsername()))
//    {
//      throw new ValidationException("Missing username");
//    }
if (StringUtils.isEmpty(request.getMobileNum())) {
throw new ValidationException("Missing Mobile Number");
}
//
//    if (!request.getPassword().equals(request.getRepassword()))
//    {
//      throw new ValidationException("Password do not match");
//    }
//
//    if (request.getPassword().trim().length() < 6)
//    {
//      throw new ValidationException("Password should be min 6 charcters");
//    }

Tenant tenant = tenantRepository.findByActiveIsTrueAndSubDomain(host);
if (StringUtils.isEmpty(tenant)) {
throw new ValidationException("Org is not available by HostName");
}

// if (userRepository.countByUsernameAndTenant(request.getUsername(), tenant) >
// 0)
if (userRepository.countByContactNumberAndTenant(request.getMobileNum(), tenant) > 0) {
// throw new ValidationException("This username is already registered");
throw new ValidationException("This mobile number is already registered, Please try to login");
// To delete existing user
// userExist = userRepository.findByUsernameAndTenant(request.getMobileNum(),
// tenant);
}

if (userRepository.countByEmailAndTenant(request.getEmail(), tenant) > 0) {
throw new ValidationException("This email is already registered, Please try to login");
// userExist = userRepository.findByUsernameAndTenant(request.getMobileNum(),
// tenant);
// userExist=userRepository.findByEmailAndTenant(request.getEmail(), tenant);
}

User user = request.toEntity();
// If User is already exist
// if (userExist != null)
// user.setId(userExist.getId());
// Random Generate userName
// Pair<String, String> name = splitName(request.getFirstname());

int rnd = sr.nextInt(10000);
rnd = rnd < 0 ? -rnd : rnd;
// String username = String.format("%s%04d", name.getFirst(), (rnd));
String username = String.format("%s%04d", request.getFirstname(), (rnd));
if (userRepository.countByUsernameAndTenant(username, tenant) > 0) {
throw new ValidationException("This username is already registered");
}
user.setUsername(username);
// Finished generate userName

String otp = this.generateOTP();
// user.setPasswordHash(encoder.encode(request.getPassword()));
user.setPasswordHash(encoder.encode(otp));
user.setTenant(tenant);
user.setContactNumber(request.getMobileNum());
user.setRegisterType(RegisterType.SELF.getType());
Role defaultRole = roleRepository.getOneByName("TestTaker");
// Role defaultRole = roleRepository.getOneByName("Admin");
List<Role> defaultRoleList = new ArrayList<>();
if (defaultRole != null) {
defaultRoleList.add(defaultRole);
}
if (!defaultRoleList.isEmpty()) {
user.setRoles(defaultRoleList);
}

// Send otp to user
String smsURL = "https://api.textlocal.in/send/?apikey=xDs4SheZwZE-YtU6M2P06Q6MX4zXYESAQwaP6iFFXw&sender="
+ URLEncoder.encodeParam("AIMEXL", "UTF-8") + "&numbers="
+ URLEncoder.encodeParam(request.getMobileNum(), "UTF-8") + "&message="
+ URLEncoder.encodeParam("Hi, " + otp + " is the OTP for registering at AIM2EXCEL.", "UTF-8");

String success = restTemplate.getForEntity(smsURL, OtpSuccessResponse.class).getBody().getStatus();
if (success.equals("success")) {
user.setOtpCreatedDate(new Date());
user = userRepository.save(user);
System.out.println("Otp has Sent Successfully " + otp);
} else if (success.equals("failure") || restTemplate.getForEntity(smsURL, OtpFailureResponse.class).getBody()
.getStatus().equals("failure")) {
throw new ValidationException("Otp can not send");
} else {
throw new ValidationException("Otp can not send..");
}

// user = userRepository.save(user);
// To send email for registered user
// communicationService.registerWelcomeEmail(user,
// request.getPassword(),tenant.getSubDomain());
// communicationService.registerWelcomeEmail(user, otp, tenant.getSubDomain());
// //to send mail for user

Student student = saveStudent(user);
if (student != null) {
studentRepository.save(student);
}
return user;
}

public User resentOTP(String contactNum, String host) {
// replace userName from conhtactNum
Tenant tenant = tenantRepository.findByActiveIsTrueAndSubDomain(host); // To get Tenant
// User user = userRepository.getByUsernameAndTenant(username, tenant);
User user = userRepository.findByContactNumberAndTenant(contactNum, tenant);
if (user != null) {
String otp = this.generateOTP();
// Send otp to user
String smsURL = "https://api.textlocal.in/send/?apikey=xDs4SheZwZE-YtU6M2P06Q6MX4zXYESAQwaP6iFFXw&sender="
+ URLEncoder.encodeParam("AIMEXL", "UTF-8") + "&numbers="
+ URLEncoder.encodeParam(contactNum, "UTF-8") + "&message="
+ URLEncoder.encodeParam("Hi, " + otp + " is the OTP for registering at AIM2EXCEL.", "UTF-8");
String success = restTemplate.getForEntity(smsURL, OtpSuccessResponse.class).getBody().getStatus();
if (success.equals("success")) {
user.setOtpCreatedDate(new Date());
user.setPasswordHash(encoder.encode(otp));
user = userRepository.save(user);
System.out.println("Otp has Sent Successfully " + otp);

return user;
} else if (success.equals("failure") || restTemplate.getForEntity(smsURL, OtpFailureResponse.class)
.getBody().getStatus().equals("failure")) {
throw new ValidationException("Otp can not send");
} else {
throw new ValidationException("Otp can not send..");
}
}
return user;
}

public User otpVerify(String contactNum, String pkgAmount, String otp, String host) {
Date curTimestamp = new Date(System.currentTimeMillis());
System.out.println("curTimestamp: " + curTimestamp);
Tenant tenant = tenantRepository.findByActiveIsTrueAndSubDomain(host); // To get Tenant
User user = userRepository.findByContactNumberAndTenant(contactNum, tenant);
if (user != null) {
Date otpCreatedDate = user.getOtpCreatedDate();
System.out.println("otpCreatedDate: " + otpCreatedDate);
if ((TimeUnit.MILLISECONDS.toMinutes(curTimestamp.getTime() - otpCreatedDate.getTime())) <= 30) {
// String otpEn=user.getPasswordHash();
if (encoder.matches(otp, user.getPasswordHash())) {
user.setIsOtpVerified(true);
passCode = otp;
userRepository.save(user);
// To send email for registered user
// communicationService.registerWelcomeEmail(user, otp, tenant.getSubDomain());
if (pkgAmount == null || pkgAmount.equals("null"))
communicationService.testSignUpMail(user, otp, tenant.getSubDomain());

return user;
} else
throw new ValidationException("OTP has mismatched");
} else
throw new ValidationException("OTP has expired");
}
return user;
}

private Student saveStudent(User user) {
Student student = new Student();
student.setName(user.getName());
student.setGender(user.getGender());
student.setUsername(user.getUsername());
student.setEmail(user.getEmail());
student.setSchool(schoolRepository.findOneByNameAndTenant(user.getSchoolName(), getTenant()));
student.setSection(user.getSection());
student.setGrade(user.getGrade());
student.setDob(user.getDob());
student.setUser(user);
return student;
}

public HttpHeaders createHttpHeaders(String user, String password) {
// String notEncoded = user + ":" + password;
String encodedAuth = basicToken;// Base64.getEncoder().encodeToString(notEncoded.getBytes());
HttpHeaders headers = new HttpHeaders();
// headers.setContentType(MediaType.APPLICATION_JSON);
headers.add("Authorization", "Basic " + encodedAuth);
return headers;
}

public TResponse getTransaction(String payId, String host) {
String name;
ResponseEntity<?> responseEntity = null;
Tenant tenant = tenantRepository.findByActiveIsTrueAndSubDomain(host); // To get Tenant
try {
HttpHeaders httpHeaders = createHttpHeaders(keyId, keySecret);
HttpEntity<String> entity = new HttpEntity<String>("parameters", httpHeaders);
responseEntity = restTemplate.exchange("https://api.razorpay.com/v1/payments/" + payId, HttpMethod.GET,
entity, TransactionResponse.class);
TransactionResponse transactionResponse = (TransactionResponse) responseEntity.getBody();

TransactionEntry transactionEntry = transactionResponse.toEntity();

User user = userRepository.findByUsernameAndTenant(transactionResponse.getNotes().getUsername(), tenant);

transactionEntry.setUserId(user.getId());
//LNT
String notesPromo = transactionResponse.getNotes().getPromoCode();
if(notesPromo!=null) {
PromoCode code = promoCodeRepository.findByName(notesPromo.toUpperCase());
//if code == null means user has not applied any coupon code in Payment
if (code != null) 
    {
        transactionEntry.setDiscount(code.getDiscount());
        if (code.getConsumedCount() < code.getTotalCount())
             promoCodeRepository.updateConsumedCount(code.getConsumedCount() + 1, code.getName().toUpperCase());
        transactionEntry.setPromoCodeName(notesPromo.toUpperCase());
     }
}
// Store Data In DB
if (transactionEntryRepository.save(transactionEntry) != null) { // Update is_paid flag
user.setIsPaid(true);
user.setFtlCount(true);
user.setLanguage("English"); // change the language
name = user.getName();
user.setName(null);
user.setGrade(null);
userRepository.save(user);
communicationService.buyProduct(user, passCode, name);
}
return new TResponse("200", "Success");
} catch (Exception ex) {
ex.printStackTrace();
return new TResponse("400", "PaymentId Mismatch");
}
}

private com.restfb.types.User validateFacebook(SocialLoginRequest login) {
FacebookClient facebookClient = new DefaultFacebookClient(login.getToken(), Version.VERSION_2_9);
com.restfb.types.User user = facebookClient.fetchObject("me", com.restfb.types.User.class,
Parameter.with("fields", "id, name, age_range, birthday,gender, email"));

// only getting name and UID
return (user != null && user.getEmail().equals(login.getEmail())) ? user : null;
}

private boolean validateGoogle(SocialLoginRequest login) {
GoogleIdToken token;

// need to put this hack
// at local id token is null and token has the value
// at server id token is the correct one and token has bogus value!!
if (StringUtils.isEmpty(login.getIdToken())) {
login.setIdToken(login.getToken());
}

try {
token = verifier.verify(login.getIdToken());
} catch (GeneralSecurityException | IOException e) {
logger.info("Error in google login", e);
return false;
}

if (token != null) {
GoogleIdToken.Payload payload = token.getPayload();
String email = payload.getEmail();
String name = (String) payload.get("name");
logger.info("Allowing google login using {} for {}", email, name);
return true;

}
return false;
}

public void attachProfileImage(String filename) {
User user = getUser();
user.setDisplayPicture(filename);
userRepository.save(user);
}

public User updateUser(UserRequest request) {
User user = userRepository.findOne(request.getId());

if (StringUtils.isEmpty(request.getEmail())) {
throw new ValidationException("Please enter email!");
}
if (!request.getEmail().equalsIgnoreCase(user.getEmail())) {

if (userRepository.countByEmailAndTenant(request.getEmail(), getTenant()) > 0) {
throw new ValidationException("This email is already registered");
} else {
user.setEmail(request.getEmail());
}
}

if (!request.getUsername().equalsIgnoreCase(user.getUsername())) {
if (userRepository.countByUsernameAndTenant(request.getUsername(), getTenant()) > 0) {
throw new ValidationException("This username is already registered");
}
}

Student student = studentRepository.findOneByUserAndTenant(user, getTenant());
if (!StringUtils.isEmpty(student)) {
if (studentRepository.countByUsernameAndTenant(request.getUsername(), getTenant()) > 1) {
throw new ValidationException("Duplicate username found!");
}
student.setName(request.getName());
student.setUsername(request.getUsername());
student.setEmail(request.getEmail());
studentRepository.save(student);
user.setPasswordHash(encoder.encode(request.getUsername()));
}
List<Role> roles = new ArrayList<>();
if (user == null) {
throw new ValidationException("Cannot locate logged in user!");
}
Set<String> preDefinedDashboard = new HashSet<>();
preDefinedDashboard.add("OperationHead");
preDefinedDashboard.add("OperationManager");
preDefinedDashboard.add("SchoolHead");
preDefinedDashboard.add("FieldManager");
preDefinedDashboard.add("SchoolAdmin");
for (String roleName : request.getRoles()) {
Role role = roleRepository.getOneByName(roleName);
if (role != null) {
roles.add(role);
}
if ("Counsellor".equals(roleName)) {
user.setDashBoardType("Counsellor");
} else if (preDefinedDashboard.contains(roleName)) {
user.setDashBoardType("CommonDashboard");
}
if (DashboardType.MANAGEMENT.getType().equals(roleName)) {
user.setDashBoardType("Management");
}
if (DashboardType.TEACHER.getType().equals(roleName)) {
user.setDashBoardType("Teacher");
}
}
user.setRoles(roles);
if (request.getTenant() != null) {
user.setTenant(tenantRepository.findOne(request.getTenant()));
}
user = userRepository.save(request.toEntity(user));
invalidateMenuCache(user);
return user;
}

public List<Role> getRoles() {
List<Role> roles = roleRepository.findAll();
Role defaultRole = roleRepository.getOneByName("TestTaker");
Role admin = roleRepository.getOneByName("Admin");
Role schoolAdmin = roleRepository.getOneByName("SchoolAdministrator");
if (getUser().getRoles().contains(admin)) {
return roles;
}
if (getUser().getRoles().contains(schoolAdmin)) {
roles.clear();
roles.add(roleRepository.getOneByName("Management"));
roles.add(roleRepository.getOneByName("Teacher"));
roles.add(roleRepository.getOneByName("FieldAgent"));
roles.add(defaultRole);
roles.add(schoolAdmin);
return roles;
}
Role fieldAgent = roleRepository.getOneByName("FieldAgent");
if (!StringUtils.isEmpty(fieldAgent)) {
roles.clear();
roles.add(defaultRole);
}
roles.remove(admin);
return roles;
}

public List<Privilege> getPrivileges() {
return privilegeRepository.findAll();
}

public Role saveRole(RoleRequest request) {
int counter = 1;
StringBuilder dashboardString = new StringBuilder();
Role role = request.toEntity();
logger.info("Saving role {}", role.getName());

for (PrivilegeRequest pr : request.getPrivileges()) {
role.addPrivilege(privilegeRepository.findOne(pr.getId()));
}
for (String dashboard : request.getDashboards()) {
dashboardString.append(dashboard);
if (counter != request.getDashboards().size()) {
dashboardString.append(",");
counter++;
}

}

role.setDashboard(dashboardString.toString());
role = roleRepository.save(role);
invalidateMenuCache();
return role;
}

public Role updateRole(RoleRequest request) {
int counter = 1;
StringBuilder dashboardString = new StringBuilder();
Role role = request.toEntity(roleRepository.findOne(request.getId()));
if (role == null) {
throw new NotFoundException("Missing role");
}

role.getPrivileges().clear();

for (PrivilegeRequest pr : request.getPrivileges()) {
role.addPrivilege(privilegeRepository.findOne(pr.getId()));
}

for (String dashboard : request.getDashboards()) {
dashboardString.append(dashboard);
if (counter != request.getDashboards().size()) {
dashboardString.append(",");
counter++;
}

}

role.setDashboard(dashboardString.toString());
role = roleRepository.save(role);
invalidateMenuCache();
return role;
}

public void deleteRole(RoleRequest request) {
roleRepository.delete(request.getId());
invalidateMenuCache();
}

	public void deleteUser(Long id) {
		User user = userRepository.findOneByIdAndTenant(id, getTenant());
		user.setActive(false);
		userRepository.saveAndFlush(user);

	}

public Menu getCurrentUserMenu() {
User user = getUser();
Menu menu = menuCache.getIfPresent(user.getUsername());
if (menu == null) {
menu = menuBuilder.buildForUser(user);
menuCache.put(user.getUsername(), menu);
}

return menu;
}

private void invalidateMenuCache(User user) {
logger.info("Invalidating menu cache as something got changed somewhere!");
Menu menu = menuBuilder.buildForUser(user);
menuCache.put(user.getUsername(), menu);
}

private void invalidateMenuCache() {
logger.info("Invalidating menu cache for all as something got changed somewhere!");
menuCache.invalidateAll();
}

public List<UserResponse> userLists() {
List<User> userList = new ArrayList<>();
getUser().getRoles().forEach(role -> {
if ("schoolhead".equals(role.getName().toLowerCase())) {
userList.addAll(userRepository.findAllBySchoolNameAndTenant(getUser().getSchoolName(), getTenant()));
} else if ("schooladmin".equals(role.getName().toLowerCase())) {
userList.addAll(userRepository.findAllBySchoolNameAndTenant(getUser().getSchoolName(), getTenant()));
} else if ("fieldmanager".equals(role.getName().toLowerCase())) {
userList.addAll(userRepository.findAllBySchoolNameAndTenant(getUser().getSchoolName(), getTenant()));
} else {
userList.addAll(userRepository.findAllByTenant(getTenant()));
}
});
return userRepository.findAllByTenant(getTenant()).stream().map(UserResponse::new).collect(Collectors.toList());
//    return userRepository.findAllByTenant(getTenant()).stream().map(UserResponse::new).collect(Collectors.toList());
}

public User saveUser(UserRequest request) {
if (userRepository.countByUsernameAndTenant(request.getUsername(), getTenant()) > 0) {
throw new ValidationException("This username is already registered");
}
if (userRepository.countByEmailAndTenant(request.getEmail(), getTenant()) > 0) {
throw new ValidationException("User is already registered with this email!");
}
User user = null;
user = request.toEntity(user);
logger.info("Saving user {} with password {}", user.getEmail(), request.getPassword());
if (request.getPassword() != null) {
user.setPasswordHash(encoder.encode(request.getPassword()));
}
Set<String> preDefinedDashboard = new HashSet<>();
preDefinedDashboard.add("OperationHead");
preDefinedDashboard.add("OperationManager");
preDefinedDashboard.add("SchoolHead");
preDefinedDashboard.add("FieldManager");
preDefinedDashboard.add("SchoolAdmin");
for (String role : request.getRoles()) {
user.addRole(roleRepository.getOneByName(role));
if ("Counsellor".equals(role)) {
user.setDashBoardType("Counsellor");
} else if (preDefinedDashboard.contains(role)) {
user.setDashBoardType("CommonDashboard");
}

if (DashboardType.MANAGEMENT.getType().equals(role)) {
user.setDashBoardType("Management");
}
if (DashboardType.TEACHER.getType().equals(role)) {
user.setDashBoardType("Teacher");
}

}
user.setRoles(user.getRoles());
user.setRegisterType(RegisterType.MANUALLY.getType());
communicationService.registerWelcomeEmail(user, request.getPassword(), getTenant().getSubDomain());
return userRepository.save(user);
}

public void changePassword(UserPasswordChangeRequest request) {
User user = userRepository.findOne(request.getId());

if (user == null) {
throw new NotFoundException("Cannot locate user");
}

user.setPasswordHash(encoder.encode(request.getPassword()));
userRepository.save(user);
}

public User findOneById(Long userId) {
return userRepository.findOne(userId);
}

public Role findOneRoleById(Long userId) {
return roleRepository.findOne(userId);
}

public Map<String, String> importRecords(MultipartFile file, String dryRun, String rootAccess, String email, boolean allowMainAimReport, boolean allowMiniAimReport)
throws Exception {
boolean errorInFile = false;
String validationMessage;
Map<String, String> statusMap = new HashMap<>();
statusMap.put("error", "false");
Map<String, ArrayList> userRecords = excelImportService.importUserRecords(file);
ArrayList<ImportUserModel> importUsersList = userRecords.get("users");
List<MasterDataDocument> streams = masterDataDocumentRepository.findAllByType("Stream");

for (ImportUserModel importUser : importUsersList) {
validationMessage = importUser.validate(streams);
if (userRepository.countByUsernameAndTenant(importUser.getUsername(), getTenant()) > 0) {
statusMap.put(importUser.getUsername(), "username is already registered");
errorInFile = true;
}
if (!StringUtils.isEmpty(importUser.getEmail())) {
if (userRepository.countByEmailAndTenant(importUser.getEmail(), getTenant()) > 0) {
statusMap.put(importUser.getUsername(), "email is already registered");
errorInFile = true;
}
}
if (!validationMessage.equalsIgnoreCase("ok")) {
statusMap.put(importUser.getUsername(), validationMessage);
errorInFile = true;
}
statusMap.putIfAbsent(importUser.getUsername(), validationMessage);
}

if (!errorInFile && dryRun.equalsIgnoreCase("false")) {
asyncUserService.saveImportedUsers(rootAccess, importUsersList, getUser(), email,allowMainAimReport,allowMiniAimReport);
}
if (errorInFile) {
statusMap.put("error", "true");
}
return statusMap;
}

public Map<String, String> userTestAssignment(MultipartFile file, String dryRun, String email) throws Exception {
boolean errorInFile = false;
Map<String, String> statusMap = new HashMap<>();
// statusMap.put("error", "false");
Map<String, ArrayList> userTestRecords = excelImportService.importUserTestAssignmentRecords(file);
ArrayList<ImportUserTestAssignModel> importUserTestList = userTestRecords.get("userTestAssignments");
for (ImportUserTestAssignModel importUserTest : importUserTestList) {

User user = userRepository.findOneByUsernameAndTenant(importUserTest.getUsername(), getTenant());
if (user == null) {
statusMap.put(importUserTest.getUsername(), "This user is not exist");
errorInFile = true;
} else if (StringUtils.isEmpty(productService.findOneByName(importUserTest.getProduct()))) {
statusMap.put(importUserTest.getProduct(), "This product is not exist");
errorInFile = true;
} else {
statusMap.put(importUserTest.getUsername(), "OK");
}
}
if (errorInFile) {
// statusMap.put("error", "true");
return statusMap;
}
if (!errorInFile && dryRun.equalsIgnoreCase("false")) {
// testService.removeAllPreviousUserTestAssignmentLink();
asyncUserService.assignProductToUsers(importUserTestList, email, getUser());
}
return statusMap;
}

public User updateUserProfile(UserRequest request) {
User user = userRepository.findOne(request.getId());
// List<User>
// allUserData=userRepository.findAllByContactNumberAndTenant(request.getContactNumber(),
// getTenant());
// List<User> userConValidateLst=userRepository.findAllRecords();
if (user == null) {
throw new ValidationException("Cannot locate logged in user!");
}
//        //Change code for google/facebook Registration time
//        if(user.getRegisterType().equalsIgnoreCase("Google") || user.getRegisterType().equalsIgnoreCase("Facebook")) {
//        if(user.getContactValidate()==false) {
//        if(allUserData!=null)
//         for(User userValidate:allUserData) {
//         if(userValidate.getContactNumber()!=null && userValidate.getContactNumber().equals(request.getContactNumber())) {
//         throw new ValidationException("Contact Number is already exist");
//         }
//         }
//        user.setContactValidate(true);
//        }
//        }
// Change code finish

// Validate all type of users for contactNumber
//        if(allUserData!=null) {
//        for(User userContactValidate:allUserData) {
//        if(allUserData.size()==1) {
//        if(userContactValidate.getId()==request.getId()) {
//        break;
//        }else {
//        if(userContactValidate.getContactNumber()!=null && userContactValidate.getContactNumber().equals(request.getContactNumber())) {
//        throw new ValidationException("Contact Number is already exist");
//        }
//        }
//        }else { //for more than one user
//        if(userContactValidate.getContactNumber()!=null && userContactValidate.getContactNumber().equals(request.getContactNumber())) {
//       throw new ValidationException("Contact Number is already exist");
//       }
//        }
//        }
//        }

user = request.toEntity(user);
if (request.getEmail() != null) {
if (user.getEmail() == null) {
user.setEmail(request.getEmail());
} else {
if (!request.getEmail().equalsIgnoreCase(user.getEmail())) {
User oneByEmailAndTenant = userRepository.findOneByEmailAndTenant(request.getEmail(), getTenant());
if (oneByEmailAndTenant == null) {
user.setEmail(request.getEmail());
} else {
throw new ValidationException("Chosen email id already exists");
}
}
}
}
saveAcademicPerformances(request.getAcademicPerformances(), user);

return userRepository.saveAndFlush(user);
}

public void saveAcademicPerformances(List<AcademicPerformanceRequest> requests, User user) {
for (AcademicPerformanceRequest academicPerformanceRequest : requests) {
AcademicPerformance academicPerformance = academicPerformanceRepository
.findByIdAndTenant(academicPerformanceRequest.getId(), getTenant());
academicPerformance = academicPerformanceRequest.toEntity(academicPerformance);
academicPerformance.setUser(user);
academicPerformance.setTenant(user.getTenant());
academicPerformanceRepository.saveAndFlush(academicPerformance);

}
}

public User convertImportUserModelToUser(ImportUserModel importUser, List<Role> defaultRoleList, boolean allowMainAimReport, boolean allowMiniAimReport) {
User user = new User();
user.setName(importUser.getName());
user.setPasswordHash(encoder.encode(importUser.getPassword()));
user.setActive(true);

user.setAllowMainAimReport(allowMainAimReport);
user.setAllowMiniAimReport(allowMiniAimReport);

if (importUser.getDob() != null) {
user.setDob(importUser.getDob());
} else {
user.setDob(null);
}
user.setUsername(importUser.getUsername());
if (!StringUtils.isEmpty(importUser.getGrade())) {
user.setGrade(importUser.getGrade());
}
if (!StringUtils.isEmpty(importUser.getSection())) {
user.setSection(importUser.getSection());
}
if (!StringUtils.isEmpty(importUser.getLanguage())) {
user.setLanguage(importUser.getLanguage());
}
if (!StringUtils.isEmpty(importUser.getSchool())) {
user.setSchoolName(importUser.getSchool());
}
if (!StringUtils.isEmpty(importUser.getCity())) {
user.setCity(importUser.getCity());
}
if (!StringUtils.isEmpty(importUser.getStream())) {
user.setStream(importUser.getStream());
}

if (!StringUtils.isEmpty(importUser.getAcademicScore())) {
user.setAcademicScore(importUser.getAcademicScore());
}
if (!StringUtils.isEmpty(importUser.getInterests())) {
user.setInterests(importUser.getInterests());
}
user.setRegisterType(RegisterType.MANUALLY.getType());
// user.setContactNumber(importUser.getContactNumber());
user.setFlag("Bulk");// edit
user.setIsPaid(null);

if (!StringUtils.isEmpty(importUser.getHigherEducation())) {
user.setHigherEducation(importUser.getHigherEducation());
}
if (!StringUtils.isEmpty(importUser.getEmail())) {
user.setEmail(importUser.getEmail());
}
if (!StringUtils.isEmpty(importUser.getContactNumber())) {
user.setContactNumber(importUser.getContactNumber());
}

if (!StringUtils.isEmpty(importUser.getGender())) {
if (importUser.getGender().equalsIgnoreCase("male")) {
user.setGender("Male");
} else {
if (importUser.getGender().equalsIgnoreCase("female")) {
user.setGender("Female");
} else {
user.setGender(importUser.getGender());
}
}
}

if (!defaultRoleList.isEmpty()) {
user.setRoles(defaultRoleList);
}

return user;
}

public User getFreshUserFromDb() {
return userRepository.findOne(getUser().getId());
}

public List<Tenant> getTenantList() {
return tenantRepository.findAll();
}

public List<User> getUserListByTenant() {
return userRepository.findAllByTenant(getTenant());
}

	public CommonResponse getUserListPaginated(Long roleId, Long tenantId, String schoolName, String searchQuery,String fromDate, String toDate, int pageSize, int page) {
		JPAQuery<User> query =  createUserQuery(roleId, tenantId,  schoolName, searchQuery, fromDate, toDate);
		long totalRecords = query.fetchCount();

		int offset = (page-1) * pageSize;
		query.offset(offset);
		query.limit(pageSize);

		List<User> users =  query.fetch();

		return new CommonResponse<>(totalRecords, pageSize, page, users);
	}

    public CommonResponse getUserResponsePaginated(Long roleId, Long tenantId, String schoolName, String searchQuery,String fromDate, String toDate, int pageSize, int page) {

        CommonResponse userListPaginated = getUserListPaginated(roleId, tenantId, schoolName, searchQuery,fromDate, toDate, pageSize, page);
        List<User> users = userListPaginated.records;
        List<UserResponse> responseList = users.stream().map(UserResponse::new).collect(Collectors.toList());

        return new CommonResponse<>(userListPaginated, responseList);
    }



public TenantTestAllotment getAllowedTestDetails() {
return testAllotmentRepository.findByUserEmailAndTenant(getUser().getEmail(), getTenant());
}

public void generateToken(String username, String host) {
Tenant tenant = tenantRepository.findByActiveIsTrueAndSubDomain(host);
if (tenant == null) {
throw new ValidationException("Org is not available by HostName");
}
if (StringUtils.isEmpty(username)) {
throw new ValidationException("Please enter username or email!");
}

User user = userRepository.findByUsernameAndTenant(username, tenant);
if (user == null) {
user = userRepository.findByEmailAndTenant(username, tenant);
}
if (user == null) {
throw new ValidationException(String.format("User not found %s.", username));
}

String email;
String name;
email = user.getEmail();
name = user.getName();
// generating token string
String tokenString = RandomStringUtils.randomNumeric(6);

//    Create Valid till date
Calendar cal = Calendar.getInstance();
cal.setTime(new Date());
// cal.add(Calendar.HOUR_OF_DAY, validTill);
cal.add(Calendar.MINUTE, validTill);
Date validTill = cal.getTime();

//    saving token to database
PasswordResetToken token = new PasswordResetToken();
token.setExpired(false);
token.setToken(tokenString);
token.setValidTill(validTill);
token.setEmail(email);
passwordResetTokenRepository.save(token);

//    Sending Email
communicationService.sendResetPasswordSmsAndEmail(tokenString, email, name);
//  }
}

public void resetPassword(PasswordResetRequest request, String host) {
String token = request.getToken();
String password = request.getPassword();
Tenant tenant = tenantRepository.findByActiveIsTrueAndSubDomain(host);
if (tenant == null) {
throw new ValidationException("Org is not available by HostName");
}
if (StringUtils.isEmpty(token)) {
throw new ValidationException("Empty token");
}
if (StringUtils.isEmpty(password)) {
throw new ValidationException("Empty password");
}
if (password.trim().length() < 6) {
throw new ValidationException("Password should be min 6 characters");
}
PasswordResetToken passwordToken = passwordResetTokenRepository.findByToken(token);
if (passwordToken == null) {
throw new ValidationException("Invalid Token");
}
if (passwordToken.isExpired() || passwordToken.getValidTill().getTime() < (new Date().getTime())) {
throw new ValidationException("Token expired");
}
String email = passwordToken.getEmail();
User user = userRepository.findByUsernameAndTenant(email, tenant);
if (user == null) {
user = userRepository.findByEmailAndTenant(email, tenant);
}
if (user == null) {
throw new ValidationException(String.format("User not found %s.", email));
}
user.setPasswordHash(encoder.encode(password));
userRepository.saveAndFlush(user);
passwordResetTokenRepository.delete(passwordToken.getId());
}

private void saveImage(String imageUrl, File destinationFile) throws IOException {
URL url = new URL(imageUrl);

try (InputStream is = url.openStream(); OutputStream os = new FileOutputStream(destinationFile)) {
byte[] b = new byte[2048];
int length;

while ((length = is.read(b)) != -1) {
os.write(b, 0, length);
}
store.store("profileimage", destinationFile);
}
}

public User createStudentUser(StudentRequest request) {
if (request.getUsername().trim().length() < 6) {
throw new ValidationException("Username should be min 6 characters");
}
if (!StringUtils.isEmpty(request.getEmail())) {
if (userRepository.countByEmailAndTenant(request.getEmail(), getTenant()) > 0) {
throw new ValidationException("This email is already registered");
}
}
if (userRepository.countByUsernameAndTenant(request.getUsername(), getTenant()) > 0) {
throw new ValidationException("This username is already registered");
}
User user = new User();
user.setActive(true);
user.setUserType(UserType.Student.toString());
user.setName(StringUtil.capitalizeWord(request.getName()));
user.setUsername(request.getUsername());
user.setEmail(request.getEmail());
user.setGender(request.getGender());
user.setPasswordHash(encoder.encode(request.getUsername()));
user.setSchoolName(schoolRepository.findOne(request.getSchool()) != null
? schoolRepository.findOne(request.getSchool()).getName()
: "");
Role defaultRole = roleRepository.getOneByName("TestTaker");
List<Role> defaultRoleList = new ArrayList<>();
if (defaultRole != null) {
defaultRoleList.add(defaultRole);
}
if (!defaultRoleList.isEmpty()) {
user.setRoles(defaultRoleList);
}
userRepository.save(user);
if (request.getEmail() != null) {
communicationService.registerWelcomeEmail(user, request.getUsername(), getTenant().getSubDomain());
}
return user;
}

public User updateStudentUser(StudentRequest request, User user) {
if (request.getEmail() != null && userRepository.countByEmailAndTenant(request.getEmail(), getTenant()) > 1) {
throw new ValidationException("This email is already registered");
}
if (userRepository.countByUsernameAndTenant(request.getUsername(), getTenant()) > 1) {
throw new ValidationException("This username is already registered");
}
user.setName(StringUtil.capitalizeWord(request.getName()));
user.setEmail(request.getEmail());
user.setGender(request.getGender());
user.setUserType(UserType.Student.toString());
user.setSchoolName(schoolRepository.findOne(request.getSchool()) != null
? schoolRepository.findOne(request.getSchool()).getName()
: "");
userRepository.save(user);
return user;
}

public void save(User user) {
userRepository.save(user);
}


    public JPAQuery<User> createUserQuery(Long roleId, Long tenantId, String schoolName, String searchQuery, String fromDateString, String toDateString) {


		Date fromDate = com.synlabs.intscale.util.DateUtil.getFormattedDate(fromDateString, START_OF_DAY);
		Date toDate = DateUtil.getFormattedDate(toDateString, END_OF_DAY);


		JPAQuery<User> query = new JPAQuery<>(entityManager);
		QUser user = QUser.user;
		QRole role = QRole.role;
		query.select(user).from(user);

		if(fromDate!=null){
			query.where(user.createdDate.after(fromDate));
		}
		if(toDate!=null){
			query.where(user.createdDate.before(toDate));
		}
		if (!StringUtils.isEmpty(tenantId) && tenantId != 0) {
			query.where(user.tenant.id.eq(tenantId));
		} else {
			query.where(user.tenant.eq(getTenant()));
		}

		if (!StringUtils.isEmpty(schoolName)) {
			query.where(user.schoolName.eq(schoolName));
		}
		if (!StringUtils.isEmpty(query)) {
			searchQuery = "%" + searchQuery + "%";
			query.where(user.name.like(searchQuery)
					.or(user.username.like(searchQuery))
					.or(user.email.like(searchQuery))
					.or(user.contactNumber.like(searchQuery))
					.or(user.schoolName.like(searchQuery))
					.or(user.city.like(searchQuery))
			);
		}

		if (!StringUtils.isEmpty(roleId) && roleId != 0) {
			query.innerJoin(user.roles, role).where(role.id.eq(roleId)).fetch();
		}
        query.orderBy(user.createdDate.desc());
		return query;
    }

    public String downloadUserList(Long roleId, Long tenantId) throws IOException {
        int page = 1;
        int offset = 0;
        int limit = 1000;

        JPAQuery<User> query =  createUserQuery(roleId, tenantId,  null, null, null, null);
        long totalRecordsCount = query.fetchCount();

        Path path = Paths.get(filedir);
        String filename = path.resolve(UUID.randomUUID().toString() + ".csv").toString();
        FileWriter fileWriter = new FileWriter(filename);
        fileWriter.append("USERNAME");
        fileWriter.append(',');
        fileWriter.append("NAME");
        fileWriter.append(',');
        fileWriter.append("Email");
        fileWriter.append(',');
        fileWriter.append("GENDER");
        fileWriter.append(',');
        fileWriter.append("GRADE");
        fileWriter.append(',');
        fileWriter.append("SCHOOL");
        fileWriter.append(',');
        fileWriter.append("INTEREST");
        fileWriter.append('\n');

        while (totalRecordsCount > offset) {
            offset = (page - 1) * limit;
            if (offset > 0) {
                query.offset(offset);
            }
            query.limit(limit);
            List<User> usersList = query.fetch();
            for (User user : usersList) {
                fileWriter.append(String.valueOf('"')).append(user.getUsername()).append(String.valueOf('"'));
                fileWriter.append(',');
                fileWriter.append(String.valueOf('"')).append(user.getName()).append(String.valueOf('"'));
                fileWriter.append(',');
                fileWriter.append(String.valueOf('"')).append(user.getEmail()).append(String.valueOf('"'));
                fileWriter.append(',');
                fileWriter.append(String.valueOf('"')).append(user.getGender()).append(String.valueOf('"'));
                fileWriter.append(',');
                fileWriter.append(String.valueOf('"')).append(user.getGrade()).append(String.valueOf('"'));
                fileWriter.append(',');
                fileWriter.append(String.valueOf('"')).append(user.getSchoolName()).append(String.valueOf('"'));
                fileWriter.append(',');
                fileWriter.append(user.getInterests());
                fileWriter.append('\n');
            }
            page++;
        }

        fileWriter.flush();
        fileWriter.close();

return filename;
}

public List<User> getUserByGrade(String grade) {
Role role = roleRepository.getOneByName("TestTaker");
return userRepository.findAllByRolesIdAndGradeAndTenant(role.getId(), grade, getTenant());
}

public List<Student> getUserByGradeAndSection(String grade, String section) {
Teacher teacher = teacherRepository.findOneByUserIdAndTenant(getUser().getId(), getTenant());
if (StringUtils.isEmpty(section)) {
section = teacher.getSection();
}
if (StringUtils.isEmpty(section)) {
grade = teacher.getGrade();
}

return studentRepository.findAllByGradeAndSectionAndTenant(grade, section, getTenant());
}

public List<User> userListByTeacher() {
List<User> users = userRepository.findAllByRolesAndTenant(roleRepository.getOneByName("Teacher"), getTenant());
users.addAll(userRepository.findAllByRolesAndTenant(roleRepository.getOneByName("Management"), getTenant()));
teacherRepository.findAllByTenantAndUserIdIsNotNull(getTenant()).forEach(teacher -> {
users.remove(teacher.getUser());
});
return users;
}

public List<User> userByCounsellor() {
return userRepository.findAllByRolesAndTenant(roleRepository.getOneByName("Counsellor"), getTenant());
}

public User getOneById(Long userId) {
return userRepository.findOne(userId);
}

public void updateEmailIdOfUser(String email) {
if (userRepository.countByEmailAndTenant(email, getTenant()) > 0) {
throw new ValidationException("This email is already registered");
}

User user = userRepository.findOne(getUser().getId());
user.setEmail(email);
userRepository.saveAndFlush(user);
}

public void selfUpdatePassword(String password) {
User user = userRepository.findOne(getUser().getId());

if (user == null) {
throw new NotFoundException("Cannot locate user");
}

if (password.trim().length() < 5) {
throw new ValidationException("Password should be min 6 characters");
}

user.setPasswordHash(encoder.encode(password));
userRepository.save(user);
}

public Role getOneRoleByName(String name) {
return roleRepository.getOneByName(name);
}

public List<User> getUserBySchoolAndGrade(String school, String grade) {
if (!StringUtils.isEmpty(school) && !StringUtils.isEmpty(grade)) {
return userRepository.findAllBySchoolNameAndGradeAndTenant(school, grade, getTenant());
} else if (!StringUtils.isEmpty(school)) {
return userRepository.findAllBySchoolNameAndTenant(school, getTenant());
} else {
return new ArrayList<User>();
}
}

public List<User> findByFullName(String fullName) {
JPAQuery<User> query = new JPAQuery<>(entityManager);
QUser user = QUser.user;
query.select(user).from(user);

if (!StringUtils.isEmpty(fullName)) {
query.where(user.name.like("%" + fullName + "%").or(user.username.like("%" + fullName + "%")));
} else {
throw new ValidationException("Please enter fullName for which you want see the report");
}
return query.fetch();

}

public List<User> findAllByParams(String schoolId, String grade, Long counsellorId) {
if (!StringUtils.isEmpty(schoolId) && !StringUtils.isEmpty(grade) && !StringUtils.isEmpty(counsellorId)) {
return userRepository.findAllByTenantAndSchoolNameAndGradeAndCounsellorId(getTenant(), schoolId, grade,
counsellorId);
}
if (!StringUtils.isEmpty(schoolId) && !StringUtils.isEmpty(counsellorId)) {
return userRepository.findAllByTenantAndSchoolNameAndCounsellorId(getTenant(), schoolId, counsellorId);
}
if (!StringUtils.isEmpty(grade) && !StringUtils.isEmpty(counsellorId)) {
return userRepository.findAllByTenantAndGradeAndCounsellorId(getTenant(), grade, counsellorId);
}
if (!StringUtils.isEmpty(schoolId) && !StringUtils.isEmpty(grade)) {
return userRepository.findAllByTenantAndSchoolNameAndGrade(getTenant(), schoolId, grade);
}
if (!StringUtils.isEmpty(schoolId)) {
return userRepository.findAllByTenantAndSchoolName(getTenant(), schoolId);
}
if (!StringUtils.isEmpty(grade)) {
return userRepository.findAllByTenantAndGrade(getTenant(), grade);
}
if (!StringUtils.isEmpty(counsellorId)) {
return userRepository.findAllByTenantAndCounsellorId(getTenant(), counsellorId);
}
return userRepository.findAllByTenant(getTenant());
}

public List<User> findAll() {

return userRepository.findAllByRolesAndTenant(roleRepository.getOneByName("TestTaker"), getTenant());
}

public int updateSchoolNameOfUsers(String oldSchoolName, String newSchoolName) {
if (StringUtils.isEmpty(oldSchoolName)) {
throw new ValidationException("oldSchoolName is empty");
}
if (StringUtils.isEmpty(newSchoolName)) {
throw new ValidationException("newSchoolName is empty");
}
List<User> users = userRepository.findAllBySchoolName(oldSchoolName);

users.forEach(user -> {
user.setSchoolName(newSchoolName);
});

int count = schoolRepository.countSchoolsByName(newSchoolName);

if (count == 0) {
School school = new School();
school.setName(newSchoolName);
school.setEmail(newSchoolName + "@gmail.com");
school.setLogo("default-school.png");
school.setFieldEnable(false);
school.setItselfTenant(false);
schoolRepository.save(school);
}

userRepository.save(users);
return users.size();
}

public int updateCityOfUsers(String oldCity, String newCity) {
if (StringUtils.isEmpty(oldCity)) {
throw new ValidationException("oldCity is empty");
}
if (StringUtils.isEmpty(newCity)) {
throw new ValidationException("newCity is empty");
}

List<User> users = userRepository.findAllByCity(oldCity);

users.forEach(user -> {
user.setCity(newCity);
});
if (masterDataDocumentRepository.findOneByTypeAndValue("City", newCity) == null) {
MasterDataDocument city = new MasterDataDocument();
city.setType("City");
city.setValue(newCity);
masterDataDocumentRepository.saveAndFlush(city);
}
userRepository.save(users);

return users.size();
}

public String downloadUserProfileInSheet(DashboardRequest request) throws IOException {

Path path = Paths.get(filedir);
String filename = path.resolve(UUID.randomUUID().toString() + ".csv").toString();
FileWriter fw = new FileWriter(filename);
fw.append(
"USERNAME,NAME,PASSWORD,GENDER,DOB,GRADE,SECTION,LANGUAGE,SCHOOL,EMAIL,CITY,STREAM,ACADEMIC SCORE,INTERESTS\n");

HashSet<User> users = dashboardService.getNewUsers(request);
for (User user : users) {
fw.append(StringUtils.isEmpty(user.getUsername()) ? "," : user.getUsername() + ',');
fw.append(StringUtils.isEmpty(user.getName()) ? "," : user.getName() + ',');
fw.append(StringUtils.isEmpty(user.getPasswordHash()) ? "," : user.getPasswordHash() + ',');
fw.append(StringUtils.isEmpty(user.getGender()) ? "," : user.getGender() + ',');
fw.append(StringUtils.isEmpty(formatDate(user.getDob(), "MM-dd-yyyy")) ? ","
: formatDate(user.getDob(), "MM-dd-yyyy") + ',');
fw.append(StringUtils.isEmpty(user.getGrade()) ? "," : user.getGrade() + ',');
fw.append(StringUtils.isEmpty(user.getSection()) ? "," : user.getSection() + ',');
fw.append(StringUtils.isEmpty(user.getLanguage()) ? "," : user.getLanguage() + ',');
fw.append(StringUtils.isEmpty(user.getSchoolName()) ? ","
: String.format("%s%s%s,", String.valueOf('"'), user.getSchoolName(), String.valueOf('"')));
fw.append(StringUtils.isEmpty(user.getEmail()) ? "," : user.getEmail() + ',');
fw.append(StringUtils.isEmpty(user.getCity()) ? ","
: String.format("%s%s%s,", String.valueOf('"'), user.getCity(), String.valueOf('"')));
fw.append(StringUtils.isEmpty(user.getStream()) ? "," : user.getStream() + ',');
fw.append(StringUtils.isEmpty(user.getAcademicScore()) ? "," : user.getAcademicScore() + ',');
fw.append(StringUtils.isEmpty(user.getInterests()) ? ",\n"
: String.format("%s%s%s\n", String.valueOf('"'), user.getInterests(), String.valueOf('"')));
}
fw.flush();
fw.close();
return filename;
}

public User getUserByUsernameOrEmail(String username, String host) {
Tenant tenant = getTenant(host);
if (tenant == null) {
return null;
}
User user = userRepository.findByUsernameAndTenantOrEmailAndTenant(username, tenant, username, tenant);
if (user == null) {
logger.warn("user not found {}", username);
throw new UsernameNotFoundException(String.format("User %s was not found", username));
}
return user;
}

public File exportExampleFile(String type) {

Path path = Paths.get(filedir);
logger.info(filedir);
String filename = path.resolve(UUID.randomUUID().toString()).toString() + ".xlsx";

XSSFWorkbook workbook = new XSSFWorkbook();
XSSFSheet sheet = workbook.createSheet("Sheet1");

XSSFRow row = sheet.createRow(0);

int col = 0;

row.createCell(col++).setCellValue("USERNAME");
row.createCell(col++).setCellValue("NAME");
row.createCell(col++).setCellValue("PASSWORD");
row.createCell(col++).setCellValue("GENDER");
row.createCell(col++).setCellValue("DOB");
row.createCell(col++).setCellValue("GRADE");
row.createCell(col++).setCellValue("SECTION");
row.createCell(col++).setCellValue("LANGUAGE");
row.createCell(col++).setCellValue("SCHOOL");
row.createCell(col++).setCellValue("EMAIL");
row.createCell(col++).setCellValue("CITY");
row.createCell(col++).setCellValue("STREAM");
row.createCell(col++).setCellValue("ACADEMIC SCORE");
row.createCell(col++).setCellValue("INTERESTS");
row.createCell(col++).setCellValue("SCIENCE");
row.createCell(col++).setCellValue("MATHS");
row.createCell(col++).setCellValue("HIGHER EDUCATION");
row.createCell(col).setCellValue("CONTACT NUMBER");

File file = new File(filename);
try {
workbook.write(new FileOutputStream(file));
} catch (IOException e) {
logger.error("error in creating ", e);
}
return file;

}

}

