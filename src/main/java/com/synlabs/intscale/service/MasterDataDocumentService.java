package com.synlabs.intscale.service;

import com.querydsl.jpa.impl.JPAQuery;
import com.synlabs.intscale.entity.masterdata.*;
import com.synlabs.intscale.entity.user.QSchool;
import com.synlabs.intscale.entity.user.QUser;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.jpa.*;
import com.synlabs.intscale.view.InterestRequest;
import com.synlabs.intscale.view.masterdata.*;
import okhttp3.*;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by itrs on 7/25/2017.
 */
@Service
public class MasterDataDocumentService extends BaseService
{

  @Autowired
  private MasterDataDocumentRepository masterDataDocumentRepository;

  @Autowired
  private InterestRepository interestRepository;

  @Autowired
  private UserExperienceFeedbackRepository userExperienceFeedbackRepository;

  @Autowired
  private BlogRepository blogRepository;

  @Autowired
  private OkHttpClient okHttpClient;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private LikedContentRepository likedContentRepository;

  @PersistenceContext
  private EntityManager entityManager;

  @Value("${scale.image.upload.location}")
  protected String filedir;

  @Value("${intscale.link.preview.baseUrl}")
  private String linkPreviewBaseUrl;

  private static Logger logger = LoggerFactory.getLogger(ReportService.class);
  private ResponseBody responseBody;

  public List<MasterDataDocument> getRecords()
  {
    return masterDataDocumentRepository.findAll();
  }

  public MasterDataDocument saveRecord(MasterDataDocumentRequest request)
  {
    if (masterDataDocumentRepository.findOneByTypeAndValueAndTenant(request.getType(), request.getValue(), getTenant()) == null)
    {
      return masterDataDocumentRepository.save(request.toEntity());
    }
    return new MasterDataDocument();
  }

  public MasterDataDocument updateRecord(MasterDataDocumentRequest request)
  {
    if (masterDataDocumentRepository.findOneByTypeAndValue(request.getType(), request.getValue()) == null)
    {
      return masterDataDocumentRepository.save(request.toEntity(masterDataDocumentRepository.findOne(request.getId())));
    }
    return new MasterDataDocument();
  }

  public void deleteRecord(Long id)
  {
    masterDataDocumentRepository.delete(masterDataDocumentRepository.findOne(id));
  }

  public MasterDataDocument save(MasterDataDocument md)
  {
    return masterDataDocumentRepository.save(md);
  }

  public List<MasterDataDocument> getAllDesignations()
  {
    return masterDataDocumentRepository.findAllByType("Designation");
  }

  public List<MasterDataDocument> getAllIndustries()
  {
    return masterDataDocumentRepository.findAllByType("Industry");
  }

  public List<MasterDataDocument> getAllInterests()
  {
    return masterDataDocumentRepository.findAllByType("Interest");
  }

  public List<MasterDataDocument> getAllHobbies()
  {
    return masterDataDocumentRepository.findAllByType("Hobby");
  }

  public List<MasterDataDocument> getAllStudentTypes()
  {
    return masterDataDocumentRepository.findAllByType("StudentType");
  }

  public List<MasterDataDocument> getAllLanguages()
  {
    return masterDataDocumentRepository.findAllByType("Language");
  }

  public List<MasterDataDocument> getAllGender()
  {
    return masterDataDocumentRepository.findAllByType("Gender");
  }

  public List<MasterDataDocument> getAllGrades()
  {
    return masterDataDocumentRepository.findAllByTypeAndTenant("Grade", getTenant());
  }

  public List<MasterDataDocument> getAllSections()
  {
    return masterDataDocumentRepository.findAllByTypeAndTenant("Section", getTenant());
  }

  public List<MasterDataDocument> getAllSubjects()
  {
    return masterDataDocumentRepository.findAllByType("Subject");
  }

  public List<MasterDataDocument> getAllDegrees()
  {
    return masterDataDocumentRepository.findAllByType("Degree");
  }

  public List<MasterDataDocument> getAllUserTypes()
  {
    return masterDataDocumentRepository.findAllByType("UserType");
  }

  public List<MasterDataDocument> getAllStreams()
  {
    return masterDataDocumentRepository.findAllByType("Stream");
  }

  public List<MasterDataDocument> getAllGroups()
  {
    return masterDataDocumentRepository.findAllByType("Group");
  }

  public List<MasterDataDocument> getAllTeacherTypes()
  {
    return masterDataDocumentRepository.findAllByType("TeacherType");
  }

  public List<MasterDataDocument> getAllpilots()
  {
    return masterDataDocumentRepository.findAllByType("Pilot");
  }

  public List<MasterDataDocument> getAllCities()
  {
    return masterDataDocumentRepository.findAllByType("City");
  }

  public MasterDataDocument findOneByValue(String groupName)
  {
    return masterDataDocumentRepository.findOneByValue(groupName);
  }

  public MasterDataDocument findOneByTypeAndValue(String type, String value)
  {
    return masterDataDocumentRepository.findOneByTypeAndValue(type, value);
  }

  public Interest saveInterest(InterestRequest request)
  {
    Interest interest = request.toEntity(new Interest());
    if (!StringUtils.isEmpty(request.getParent()))
    {
      interest.setParent(interestRepository.findOneByName(request.getParent()));
    }
    return interestRepository.save(interest);
  }

  public List<Interest> getAllInterest()
  {
    return interestRepository.findAll();
  }

  public Interest findOneById(Long interestId)
  {
    return interestRepository.findOne(interestId);
  }

  public void deleteInterest(Interest interest)
  {
    interestRepository.delete(interest);
  }

  public Interest updateInterest(InterestRequest request)
  {
    Interest interest = interestRepository.findOne(request.getId());
    interest = request.toEntity(interest);
    if (!StringUtils.isEmpty(request.getParent()))
    {
      interest.setParent(interestRepository.findOneByName(request.getParent()));
    }
    return interestRepository.save(interest);
  }

  public List<Interest> getParentInterest()
  {
    List<Interest> interests = new ArrayList<>();
    for (Interest c : interestRepository.findAll())
    {
      if (StringUtils.isEmpty(c.getParent()))
      {
        interests.add(c);
      }
    }
    return interests;
  }

  public List<Interest> getChildInterest()
  {
    List<Interest> interests = new ArrayList<>();
    for (Interest c : interestRepository.findAll())
    {
      if (!StringUtils.isEmpty(c.getParent()))
      {
        interests.add(c);
      }
    }
    return interests;
  }

  public List<Interest> getChildInterestByParent(Long id)
  {
    List<Interest> interests = new ArrayList<>();
    if (id != null)
    {
      return interestRepository.findAllByParentId(id);
    }
    return interests;
  }

  public List<Interest> getChildInterestByParentName(String name)
  {
    return interestRepository.findAllByParentName(name);
  }

  public void deleteInterests(List<Long> list)
  {
    list.forEach(id -> {
      Interest interest = interestRepository.findOne(id);
      if (interest.getParent() == null)
      {
        List<Interest> interestChilds = interestRepository.findAllByParentId(id);
        interestChilds.forEach(interestChild -> {
          interestChild.setParent(null);
          list.remove(interestChild.getId());
          interestRepository.saveAndFlush(interestChild);
        });
        interestRepository.delete(interest);
      }
      else
      {
        interestRepository.delete(interest);
      }
    });

  }

  public void attachInterestImage(String filename, Long id)
  {
    Interest interest = interestRepository.findOne(id);
    interest.setImage(filename);
    interestRepository.save(interest);
  }

  public void saveUserExperienceFeedback(UserExperienceFeedbackRequest request)
  {
    {
      UserExperienceFeedback userExperienceFeedback = request.toEntity(new UserExperienceFeedback());
      userExperienceFeedback.setUsername(getUser().getUsername());
      userExperienceFeedbackRepository.save(userExperienceFeedback);
    }
  }

  public String downloadUserExperienceFeedback() throws IOException
  {
    int page = 1;
    int offset = 0;
    int limit = 1000;

    QUserExperienceFeedback uef = QUserExperienceFeedback.userExperienceFeedback;
    JPAQuery<UserExperienceFeedback> query = new JPAQuery<>(entityManager);
    query.from(uef)
            .select(uef);
    long totalRecordsCount = query.fetchCount();
    Path path = Paths.get(filedir);
    String filename = path.resolve(UUID.randomUUID().toString() + ".csv").toString();
    FileWriter fileWriter = new FileWriter(filename);
    fileWriter.append("Username");
    fileWriter.append(',');
    fileWriter.append("School");
    fileWriter.append(',');
    fileWriter.append("Rating");
    fileWriter.append(',');
    fileWriter.append("Comments");
    fileWriter.append('\n');

    while (totalRecordsCount > offset) {
      offset = (page - 1) * limit;
      if (offset > 0) {
        query.offset(offset);
      }
      query.limit(limit);
      List<UserExperienceFeedback> userExperienceFeedbackList = query.fetch();

      for (UserExperienceFeedback userExperienceFeedback : userExperienceFeedbackList)
      {
        fileWriter.append(String.valueOf('"')).append(userExperienceFeedback.getUsername()).append(String.valueOf('"'));
        fileWriter.append(',');
        fileWriter.append(String.valueOf('"')).append(userExperienceFeedback.getUser().getSchoolName()).append(String.valueOf('"'));
        fileWriter.append(',');
        fileWriter.append(String.valueOf('"')).append(userExperienceFeedback.getRating().toString()).append(String.valueOf('"'));
        fileWriter.append(',');
        fileWriter.append(String.valueOf('"')).append(userExperienceFeedback.getExperienceDescription()).append(String.valueOf('"'));
        fileWriter.append('\n');
      }
      page++;
    }

    fileWriter.flush();
    fileWriter.close();

    return filename;
  }

  public List<Blog> getAllBlogs()
  {
    return blogRepository.findAll();
  }

  public Blog findBlogById(Long blogId)
  {
    return blogRepository.findOne(blogId);
  }

  public Blog saveBlog(BlogRequest blogRequest) throws IOException, JSONException
  {
    Blog blog = blogRequest.toEntity(new Blog());

    String url = linkPreviewBaseUrl + blogRequest.getUrl();

    Request request = new Request.Builder()
        .url(url)
        .build();

    Response response = null;

    logger.info("fetching preview from url: " + url);
    response = okHttpClient.newCall(request).execute();
    if (response.isSuccessful())
    {
      if (response.body() != null)
      {
        String jsonData = response.body().string();

        JSONObject Jobject = new JSONObject(jsonData);
        JSONObject data = Jobject.getJSONObject("data");

        try
        {
          if (!data.get("logo").toString().equals("null"))
          {
            JSONObject logoData = data.getJSONObject("logo");
            blog.setLogoUrl(logoData.getString("url"));
          }
        }
        catch (JSONException e)
        {
          blog.setLogoUrl(data.getString("logo"));
        }

        try
        {
          if (!data.get("image").toString().equals("null"))
          {
            JSONObject imageData = data.getJSONObject("image");
            blog.setImageUrl(imageData.getString("url"));
          }
        }
        catch (JSONException e)
        {
          blog.setImageUrl(data.getString("image"));
        }

        if (!data.get("author").toString().equals("null"))
        {
          blog.setAuthor(data.getString("author"));
        }

        if (!data.get("title").toString().equals("null"))
        {
          blog.setTitle(data.getString("title"));
        }

        if (!data.get("description").toString().equals("null"))
        {
          blog.setDescription(data.getString("description"));
        }

        blog.setLikes(0L);
      }
    }
    else
    {
      logger.error("link preview request failed");
    }

    blog.getInterests().clear();
    blogRequest.getInterestsIds().forEach(interestId -> {
      blog.getInterests().add(interestRepository.findOne(interestId));
    });

    return blogRepository.save(blog);
  }

  public Blog updateBlog(BlogRequest request)
  {
    Blog blog = blogRepository.findOne(request.getId());
    blog = request.toEntity(blog);
    return blogRepository.save(blog);
  }

  public void deleteBlogs(List<Long> list)
  {
    list.forEach(id -> blogRepository.delete(id));
  }

  public List<BlogResponse> recommendBlogsBasedOnInterest()
  {
    User user = userRepository.findOne(getUser().getId());

    Set<String> interests = new HashSet<>();
    Collections.addAll(interests, StringUtils.isEmpty(user.getInterests()) ? new String[0] : user.getInterests().split(", "));

    List<Blog> blogs = blogRepository.findAllByInterestsIn(interestRepository.findAllByNameIn(interests));
    List<BlogResponse> blogsResponse = blogs.stream().map(BlogResponse::new).collect(Collectors.toList());
    blogsResponse.forEach(blogResponse -> {
      LikedContent content = likedContentRepository.findByUserIdAndBlogId(getUser().getId(), blogResponse.getId());
      if (content == null)
      {
        blogResponse.setLiked(false);
      }
      else
      {
        blogResponse.setLiked(content.isLiked());
      }
    });
    return blogsResponse;
  }

  public LikedContentResponse likeBlog(Long blogId)
  {
    Blog blog = blogRepository.findOne(blogId);
    LikedContent content = likedContentRepository.findByUserIdAndBlogId(getUser().getId(), blogId);
    if (content == null)
    {
      content = new LikedContent();
      content.setBlog(blogRepository.findOne(blogId));
      content.setUser(getUser());
      content.setLiked(true);
      blog.setLikes(blog.getLikes() + 1);
    }
    else
    {
      content.setLiked(!content.isLiked());
      if (content.isLiked())
      {
        blog.setLikes(blog.getLikes() + 1);
      }
      else
      {
        blog.setLikes(blog.getLikes() - 1);
      }
    }

    content = likedContentRepository.saveAndFlush(content);
    blogRepository.saveAndFlush(blog);
    LikedContentResponse likedContentResponse = new LikedContentResponse(content);
    likedContentResponse.setLikes(blog.getLikes());
    return likedContentResponse;
  }

  public List<MasterDataDocument> getAllTags()
  {
    return masterDataDocumentRepository.findAllByType("Tag");
  }

  public List<String> getAllCitiesFromUserTable()
  {

    QUser user = QUser.user;
    JPAQuery<String> query = new JPAQuery<>(entityManager);

    List<String> cityNames = query.select(user.city)
                                  .from(user)
                                  .distinct()
                                  .orderBy(user.city.asc())
                                  .fetch();

    List<String> allCities = getAllCities().stream().map(MasterDataDocument::getValue).collect(Collectors.toList());

    cityNames.removeIf(allCities::contains);

    return cityNames;
  }
}
