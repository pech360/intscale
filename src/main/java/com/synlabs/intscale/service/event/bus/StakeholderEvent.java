package com.synlabs.intscale.service.event.bus;

import com.synlabs.intscale.entity.threesixty.Stakeholder;
import com.synlabs.intscale.enums.EventType;

public class StakeholderEvent
{
  private Stakeholder stakeholder;
  private EventType eventType;

  public StakeholderEvent(Stakeholder stakeholder, EventType eventType)
  {
    this.stakeholder = stakeholder;
    this.eventType = eventType;
  }

  public Stakeholder getStakeholder()
  {
    return stakeholder;
  }

  public EventType getEventType()
  {
    return eventType;
  }
}
