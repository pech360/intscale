package com.synlabs.intscale.service.event.bus;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.synlabs.intscale.entity.threesixty.Stakeholder;
import com.synlabs.intscale.enums.EventType;
import com.synlabs.intscale.service.BaseService;
import com.synlabs.intscale.service.communication.CommunicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

import static com.synlabs.intscale.enums.EventType.StakeholderRemindEmail;
import static com.synlabs.intscale.enums.EventType.StakeholderSentLinkEmail;

@Service
public class EmailEventBus extends BaseService
{
  @Autowired
  private EventBus eventBus;

  @Autowired
  private CommunicationService communicationService;

  @PostConstruct
  public void init()
  {
    eventBus.register(this);
  }

  @Subscribe
  public void stakeholderEvent(StakeholderEvent stakeholderEvent)
  {
    try
    {
      EventType type = stakeholderEvent.getEventType();
      Stakeholder stakeholder = stakeholderEvent.getStakeholder();
      switch (type)
      {
        case StakeholderRemindEmail:
          communicationService.sendStakeholderTestRemind(stakeholder);
          break;
        case StakeholderSentLinkEmail:
          communicationService.sendStakeholderTestLink(stakeholder);
          break;
        case StakeholderVerificationEmail:
          communicationService.sendStakeholderVerification(stakeholder);
          break;
      }
    }
    catch (Exception ex)
    {
//      TODO : send notification email about this email failure
    }
  }
}
