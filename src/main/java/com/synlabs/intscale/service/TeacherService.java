package com.synlabs.intscale.service;

import com.synlabs.intscale.entity.masterdata.GradeSectionSubject;
import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.user.Teacher;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.ex.NotFoundException;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.*;
import com.synlabs.intscale.view.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * Created by India on 1/5/2018.
 */
@Service
public class TeacherService extends BaseService {
    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private SchoolRepository schoolRepository;

    @Autowired
    private TestRepository testRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MasterDataDocumentRepository documentRepository;

    @Autowired
    private GradeSectionSubjectRepository gradeSectionSubjectRepository;

    public List<Teacher> findAll() {
        return teacherRepository.findAllByTenant(getTenant());
    }

    public List<Teacher> findAllByTeacherType() {
        return teacherRepository.findAllByTenantAndClassTeacherIsTrue(getTenant());
    }

    public List<Teacher> findAllSubjectTeacher() {
        return teacherRepository.findAllByTenant(getTenant());
    }

    public Teacher save(TeacherRequest request) {
        Teacher teacher = request.toEntity(new Teacher());
        User user = userRepository.findOne(request.getUserId());
        if (user == null) {
            throw new NotFoundException("user not found to map with teacher entity");
        }
        if (!StringUtils.isEmpty(request.getUserId())) {
            teacher.setUser(user);
            teacher.setGender(user.getGender());
            teacher.setEmail(user.getEmail());
            teacher.setGrade(request.getGrade());

        }

        teacher.setSchool(schoolRepository.findOneByNameAndTenant(teacher.getUser().getSchoolName(), getTenant()));
        teacher.getGradeSectionSubjects().forEach(gradeSectionSubject -> {
            gradeSectionSubjectRepository.delete(gradeSectionSubject.getId());
        });

        teacher.setTenant(getTenant());
        teacher = teacherRepository.saveAndFlush(teacher);
        user.setTeacher(teacher);
        userRepository.saveAndFlush(user);

        if (teacher.isSubjectTeacher() || teacher.isClassTeacher() || teacher.isHod()) {
            if (request.getGradeSectionSubjects() == null) {
                throw new ValidationException("subject teacher must have GRADE, SECTION and SUBJECT");
            } else {
                List<GradeSectionSubject> gradeSectionSubjectList = new ArrayList<>();
                GradeSectionSubject gradeSectionSubject;
                for (GradeSectionSubjectRequest gradeSectionSubjectRequest : request.getGradeSectionSubjects()) {
                    if (gradeSectionSubjectRequest.getId() == null) {
                        gradeSectionSubject = gradeSectionSubjectRequest.toEntity(new GradeSectionSubject());
                        gradeSectionSubject.setTenant(getTenant());
                    } else {
                        gradeSectionSubject = gradeSectionSubjectRepository.findOne(gradeSectionSubjectRequest.getId());
                        gradeSectionSubject = gradeSectionSubjectRequest.toEntity(gradeSectionSubject);
                    }
                    gradeSectionSubject.setTeacher(teacher);
                    gradeSectionSubjectList.add(gradeSectionSubject);
                }

                gradeSectionSubjectList = gradeSectionSubjectRepository.save(gradeSectionSubjectList);
            }
        }
        return teacher;
    }

    public Teacher update(TeacherRequest request) {
        Teacher teacher = request.toEntity(teacherRepository.findOne(request.getId()));

        if (!StringUtils.isEmpty(request.getUserId())) {
            teacher.setUser(userRepository.findOne(request.getUserId()));
            teacher.setGender(teacher.getUser().getGender());
            teacher.setEmail(teacher.getUser().getEmail());
            teacher.setGrade(request.getGrade());
        }

        teacher.setSchool(schoolRepository.findOneByNameAndTenant(teacher.getUser().getSchoolName(), getTenant()));
        teacher.getGradeSectionSubjects().forEach(gradeSectionSubject -> {
            gradeSectionSubjectRepository.delete(gradeSectionSubject.getId());
        });
        teacher.getGradeSectionSubjects().clear();
        teacherRepository.save(teacher);

        if (teacher.isSubjectTeacher() || teacher.isClassTeacher() || teacher.isHod()) {
            if (request.getGradeSectionSubjects() == null) {
                throw new ValidationException("subject teacher must have GRADE, SECTION and SUBJECT");
            } else {
                List<GradeSectionSubject> gradeSectionSubjectList = new ArrayList<>();
                GradeSectionSubject gradeSectionSubject;

                for (GradeSectionSubjectRequest gradeSectionSubjectRequest : request.getGradeSectionSubjects()) {
                    if (gradeSectionSubjectRequest.getId() == null) {
                        gradeSectionSubject = gradeSectionSubjectRequest.toEntity(new GradeSectionSubject());
                    } else {
                        gradeSectionSubject = gradeSectionSubjectRepository.findOne(gradeSectionSubjectRequest.getId());
                        gradeSectionSubject = gradeSectionSubjectRequest.toEntity(gradeSectionSubject);
                    }
                    gradeSectionSubject.setTeacher(teacher);
                    gradeSectionSubjectList.add(gradeSectionSubject);
                }
                gradeSectionSubjectList = gradeSectionSubjectRepository.save(gradeSectionSubjectList);
            }
        }
        return teacher;
    }

    public List<Teacher> findAllByFilter(Long schoolId, String grade) {
        if (schoolId != null && grade != null) {
            return teacherRepository.findAllByTenantAndSchoolIdAndGradeAndClassTeacherIsTrue(getTenant(), schoolId, grade);
        }
        if (schoolId != null) {
            return teacherRepository.findAllByTenantAndSchoolIdAndClassTeacherIsTrue(getTenant(), schoolId);
        }
        if (grade != null) {
            return teacherRepository.findAllByTenantAndGradeAndClassTeacherIsTrue(getTenant(), grade);
        }
        return teacherRepository.findAllByTenantAndClassTeacherIsTrue(getTenant());
    }

    public boolean assignGradeToTeachers(TeacherGradeRequest request) {
        if (request.getTeacherIds() != null && request.getGrade() != null) {
            List<Long> ids = Arrays.asList(request.getTeacherIds());
            Teacher teacher = null;
            for (Long id : ids) {
                teacher = teacherRepository.findOne(id);
                teacher.setGrade(documentRepository.findOne(request.getGrade()).getValue());
                teacherRepository.save(teacher);
            }
            return true;
        }
        return false;
    }

    public boolean removeGradeToTeachers(TeacherGradeRequest request) {
        if (request.getTeacherIds() != null) {
            List<Long> ids = Arrays.asList(request.getTeacherIds());
            Teacher teacher = null;
            for (Long id : ids) {
                teacher = teacherRepository.findOne(id);
                teacher.setGrade(null);
                teacherRepository.save(teacher);
            }
            return true;
        }
        return false;
    }

    public boolean updateGradeToTeachers(Long id, Long gradeId) {
        if (id != null && gradeId != null) {
            Teacher teacher = teacherRepository.findOne(id);
            teacher.setGrade(documentRepository.findOne(gradeId).getValue());
            teacherRepository.save(teacher);
            return true;
        }
        return false;
    }

    public void delete(Long id) {
        teacherRepository.delete(teacherRepository.findOne(id));
    }

    public List<Teacher> findAllsubjectTeacherList() {
        return teacherRepository.findAllByTenant(getTenant());
    }

    public void deleteSubjectTeacher(Long id) {
        teacherRepository.delete(id);
    }

    public TeacherGradeResponse getAllTeacherInfo() {
        TeacherGradeResponse response = new TeacherGradeResponse();
        Set<String> grades = new HashSet<>();
        Set<String> sections = new HashSet<>();
        Teacher teacher = teacherRepository.findOneByUserIdAndTenant(getUser().getId(), getTenant());
        if (teacher.getGrade() != null) {
            grades.add(teacher.getGrade());
            response.setGrade(teacher.getGrade());
        }
        if (teacher.getSection() != null) {
            sections.add(teacher.getSection());
            response.setSection(teacher.getSection());
        }
        if (teacher.isSubjectTeacher() && teacher.getGradeSectionSubjects() != null) {
            teacher.getGradeSectionSubjects().forEach(gradeSectionSubject -> {
                grades.add(gradeSectionSubject.getGrade());
                sections.add(gradeSectionSubject.getSection());
            });

        }

        response.setName(teacher.getName());
        response.setClassTeacher(teacher.isClassTeacher());
        response.setSubjectTeacher(teacher.isSubjectTeacher());
        response.setSubjectGrades(grades);
        response.setSubjectSections(sections);
        response.setPrincipal(teacher.isPrincipal());
        return response;
    }
}
