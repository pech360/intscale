package com.synlabs.intscale.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.synlabs.intscale.common.ValidityIndicesMatrix;
import com.synlabs.intscale.common.ValidityIndicesMatrixItem;
import com.synlabs.intscale.ex.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ValidityIndicesCalculator
{
  @Autowired
  private ResourceLoader resourceLoader;

  private static final Logger logger = LoggerFactory.getLogger(CategoryMatrixCalculator.class);

  private List<ValidityIndicesMatrix> validityIndicesMatriceFiles = new ArrayList<>();

  private ObjectMapper jsonMapper = new ObjectMapper();

  @PostConstruct
  public void init()
  {
    try
    {
      Resource[] resources

          = ResourcePatternUtils.getResourcePatternResolver(resourceLoader)
                                .getResources("classpath*:/validityIndices/*.json");

      for (Resource resource : resources)
      {
        logger.info("Loaded Validity Indices Matrix from {}", resource.getFilename());
        ValidityIndicesMatrix file = jsonMapper.readValue(resource.getInputStream(), ValidityIndicesMatrix.class);
        validityIndicesMatriceFiles.add(file);
      }
    }
    catch (IOException e)
    {
      logger.error("Cannot load Validity Indices Matrix from disk!", e);
    }
  }

  public Map<String,BigDecimal> getValidityIndicesPercentile(String test, Integer infrequencyRawScore, Integer acquiescenceRawScore)
  {
    for (ValidityIndicesMatrix file : validityIndicesMatriceFiles)
    {
      for (ValidityIndicesMatrixItem row : file.getValidityIndicesMatrix())
      {
        if (row.getTest().equalsIgnoreCase(test))
        {
          Map<String,BigDecimal> validityIndices = new HashMap<>(2);
          BigDecimal infrequencyNormArray[] = row.getInfrequency();
          BigDecimal acquiescenceNormArray[] = row.getAcquiescence();
          validityIndices.put("infrequency",getPercentile(infrequencyRawScore, infrequencyNormArray));
          validityIndices.put("acquiescence",getPercentile(acquiescenceRawScore, acquiescenceNormArray));
          return validityIndices;
        }
      }
    }
    logger.error(test+" not found in validity indices matrix for calculating percentile");
    return null;
  }

  private BigDecimal getPercentile(Integer rawScore, BigDecimal normArray[])
  {
    Integer index = 0;
    BigDecimal distance = new BigDecimal(0);
    while (index < normArray.length)
    {
      BigDecimal temp = normArray[index].subtract(BigDecimal.valueOf(rawScore));
      if (temp.signum() != -1)
      {
        distance = temp; //getting first positive distance
        break;
      }
      index++;
    }

    for (Integer c = 0; c < normArray.length; c++)
    {
      BigDecimal cdistance = normArray[c].subtract(BigDecimal.valueOf(rawScore));
      if (cdistance.signum() == 1 && cdistance.compareTo(distance) <= 0)
      {
        index = c;
        distance = cdistance; // try to getting minimum distance
      }
    }
//          BigDecimal score = normArray[index];
    return BigDecimal.valueOf(index);
  }
}
