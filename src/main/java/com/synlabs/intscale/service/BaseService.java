package com.synlabs.intscale.service;

import com.synlabs.intscale.entity.CurrentUser;
import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.threesixty.NumberSequence;
import com.synlabs.intscale.entity.threesixty.Stakeholder;
import com.synlabs.intscale.entity.user.Role;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.enums.StakeholderRelation;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.RoleRepository;
import com.synlabs.intscale.jpa.TenantRepository;
import com.synlabs.intscale.jpa.TestRepository;
import com.synlabs.intscale.jpa.UserRepository;
import com.synlabs.intscale.jpa.threesixty.NumberSequenceRepository;
import com.synlabs.intscale.jpa.threesixty.StakeholderRepository;
import com.synlabs.intscale.view.report.AimExpertInventoryResponse;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

abstract public class BaseService
{
  @Autowired
  private NumberSequenceRepository numberSequenceRepository;

  @Autowired
  private RoleRepository roleRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private TestRepository testRepository;

  @Autowired
  private StakeholderRepository stakeholderRepository;

  @Autowired
  private TenantRepository tenantRepository;

  public User getUser()
  {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication == null || !authentication.isAuthenticated())
    {
      return null;
    }

    if (authentication.getPrincipal() instanceof CurrentUser)
    {
      return ((CurrentUser) authentication.getPrincipal()).getUser();
    }
    return null;
  }

  public Tenant getTenant()
  {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication == null || !authentication.isAuthenticated())
    {
      return null;
    }

    if (!(authentication.getPrincipal() instanceof String))
    {
      return ((CurrentUser) authentication.getPrincipal()).getUser().getTenant();
    }
    return null;
  }

  public Tenant getTenant(String host){
   return tenantRepository.findByActiveIsTrueAndSubDomain(host);
  }

  public Tenant getTenantByName(String name){
   return tenantRepository.findByActiveIsTrueAndName(name);
  }

  public static boolean isThreeSixty(User user)
  {
    return user.getThreeSixtyEnabled() == null ? false : user.getThreeSixtyEnabled();
  }

  public static boolean isThreeSixty(Test test)
  {
    return test.getThreeSixtyEnabled() == null ? false : test.getThreeSixtyEnabled();
  }

  public String getRandomString(int size)
  {
    return RandomStringUtils.randomAlphabetic(size);
  }

  public String getRandomString()
  {
    return getRandomString(10);
  }

  public String generateSequence(String type)
  {
    NumberSequence sequence = numberSequenceRepository.findByType(type);
    if (sequence == null)
    {
      sequence = new NumberSequence();
      sequence.setType(type);
      sequence.setCurrent(1L);
    }
    String string = sequence.getSequence();
    sequence.setCurrent(sequence.getCurrent() + 1);
    numberSequenceRepository.saveAndFlush(sequence);
    return string;
  }

  public Role getRoleByName(String name)
  {
    return roleRepository.getOneByName(name);
  }

  public User getFreshUser()
  {
    return userRepository.findOne(getUser().getId());
  }

  public User getFreshUser(Long id)
  {
    return userRepository.findOne(id);
  }

  public StakeholderRelation[] getAllStakeholderRelations()
  {
    return StakeholderRelation.values();
  }

  public static String formatDate(Date date, String format)
  {
    return date == null ? "" : new SimpleDateFormat(format).format(date);
  }

  public static String formatDate(Date date)
  {
    return date == null ? "" : new SimpleDateFormat("yyyy-MM-dd HH:mm a").format(date);
  }

  public Test getLatestVersion(Test test)
  {
    if (test.isLatestVersion())
    {
      return test;
    }

    Long parentTestId = test.getVersionId() == null ? test.getId() : test.getVersionId();
    Test latest = testRepository.findAllByLatestVersionIsTrueAndVersionId(parentTestId);
    if (latest == null)
    {
      throw new ValidationException("Cannot locate latest verion.");
    }
    return latest;
  }

  public static boolean isVerified(Stakeholder stakeholder)
  {
    return stakeholder.getVerified() == null ? false : stakeholder.getVerified();
  }

  public String generateStakeholderLoginKey()
  {
    String loginKey = getRandomString();
    Stakeholder byLoginKey = stakeholderRepository.findByTenantAndLoginKey(getTenant(), loginKey);
    while (byLoginKey != null)
    {
      loginKey = getRandomString();
      byLoginKey = stakeholderRepository.findByTenantAndLoginKey(getTenant(), loginKey);
    }
    return loginKey;
  }
}
