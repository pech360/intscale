package com.synlabs.intscale.service;

import com.synlabs.intscale.entity.user.Session;
import com.synlabs.intscale.jpa.SessionRepository;
import com.synlabs.intscale.view.SessionRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by India on 1/5/2018.
 */
@Service
public class SessionService extends BaseService {
    @Autowired
    private SessionRepository sessionRepository;

    public List<Session> findAll() {
        return sessionRepository.findAllByTenant(getTenant());
    }

    public Session save(SessionRequest request) {
        return sessionRepository.save(request.toEntity());
    }

    public Session update(SessionRequest request) {
        return sessionRepository.save(request.toEntity(sessionRepository.findOne(request.getId())));
    }

    public void delete(Long id) {
        sessionRepository.delete(sessionRepository.findOne(id));
    }
}
