package com.synlabs.intscale.service;

import com.synlabs.intscale.entity.Pilot;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.PilotRepository;
import com.synlabs.intscale.view.PilotRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by India on 1/16/2018.
 */
@Service
public class PilotService extends BaseService {

    @Autowired
    private PilotRepository pilotRepository;

    public List<Pilot> findAll() {
        return pilotRepository.findAllByTenant(getTenant());
    }

    public Pilot save(PilotRequest request) {
        if(pilotRepository.countByNameAndTenant(request.getName(),getTenant())>0){
            throw new ValidationException("Pilot name can't be Duplicate");
        }
        Pilot oldPilot=pilotRepository.findOneByActiveAndTenant(true,getTenant());
        if(oldPilot!=null){
            oldPilot.setActive(false);
            pilotRepository.save(oldPilot);
        }
        Pilot pilot=pilotRepository.save(request.toEntity());
        return pilot;
    }

    public Pilot findOneByActive(boolean status) {
        return pilotRepository.findOneByActiveAndTenant(status,getTenant());
    }

    public Pilot findOneByActive(boolean status, Tenant tenant) {
        return pilotRepository.findOneByActiveAndTenant(status,tenant);
    }
}
