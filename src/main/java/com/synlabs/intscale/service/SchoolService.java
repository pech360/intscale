package com.synlabs.intscale.service;

import com.querydsl.core.Tuple;
import com.querydsl.core.group.GroupBy;
import com.querydsl.jpa.impl.JPAQuery;
import com.synlabs.intscale.entity.masterdata.GradeSectionSubject;
import com.synlabs.intscale.entity.masterdata.MasterDataDocument;
import com.synlabs.intscale.entity.test.QTestResultSummary;
import com.synlabs.intscale.entity.test.TestResultSummary;
import com.synlabs.intscale.entity.user.*;
import com.synlabs.intscale.enums.RecordType;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.RoleRepository;
import com.synlabs.intscale.jpa.SchoolRepository;
import com.synlabs.intscale.jpa.TestResultSummaryRepository;
import com.synlabs.intscale.jpa.UserRepository;
import com.synlabs.intscale.util.DateUtil;
import com.synlabs.intscale.view.DashboardRequest;
import com.synlabs.intscale.view.SchoolRequest;
import com.synlabs.intscale.view.SchoolResponse;
import com.synlabs.intscale.view.leap.OverAllTestResultResponse;
import com.synlabs.intscale.view.leap.TestResultSummaryResponse;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static com.querydsl.core.types.Projections.list;
import static com.synlabs.intscale.util.DateUtil.DayTime.END_OF_DAY;
import static com.synlabs.intscale.util.DateUtil.DayTime.START_OF_DAY;

@Service
public class SchoolService extends BaseService {
    @Autowired
    private SchoolRepository schoolRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private TestService testService;

    @Autowired
    private DashboardService dashboardService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MasterDataDocumentService masterDataDocumentService;

    @Autowired
    private TestResultSummaryRepository testResultSummaryRepository;

    @Autowired
    private RoleRepository roleRepository;

    @PersistenceContext
    private EntityManager entityManager;

    public School saveSchool(SchoolRequest request) {
        if (getTenant().getSchoolLimit() > schoolRepository.countByTenant(getTenant())) {
            School school = request.toEntity(new School());
            return schoolRepository.save(school);
        }
        throw new ValidationException("Exceed the school limit");
    }

    public School getSchool(Long id) {
        return schoolRepository.findOne(id);
    }

    public List<School> getAllSchools() {
        List<School> schools = new ArrayList<>();
        getUser().getRoles().forEach(role -> {
            if ("schoolhead".equals(role.getName().toLowerCase())) {
                schools.add(schoolRepository.findOneByNameAndTenant(getUser().getSchoolName(), getTenant()));
            } else if ("schooladmin".equals(role.getName().toLowerCase())) {
                schools.add(schoolRepository.findOneByNameAndTenant(getUser().getSchoolName(), getTenant()));
            } else if ("fieldmanager".equals(role.getName().toLowerCase())) {
                schools.addAll(schoolRepository.findAllByIsFieldEnableIsTrueAndTenant(getTenant()));
            } else {
                schools.addAll(schoolRepository.findAllByTenant(getTenant()));
            }
        });
        return schools;
    }

    public List<SchoolResponse> getAllSchoolsByUser() {
        List<School> schools = new ArrayList<>();
        if (getUser().getRoles().contains(userService.getOneRoleByName("FieldManager"))) {
            schools.addAll(schoolRepository.findAllByIsFieldEnableIsTrueAndTenant(getTenant()));
        } else {
            schools.addAll(schoolRepository.findByNameAndTenant(getUser().getSchoolName(), getTenant()));
        }
        return new ArrayList<>(schools.stream().map(SchoolResponse::new).collect(Collectors.toList()));
    }

    public School updateSchool(SchoolRequest request) {
        School school = request.toEntity(schoolRepository.findOne(request.getId()));
        schoolRepository.save(school);
        return school;
    }

    public void deleteSchool(Long schoolId) {
        schoolRepository.delete(schoolId);
    }

    public void attachTestImage(String filename, Long schoolId) {
        School school = schoolRepository.findOne(schoolId);
        school.setLogo(filename);
        schoolRepository.save(school);
    }

    private static Map<String, Date> getStartAndEndDateOfAcademicYear(DashboardRequest request) {
        Date fromDate = DateUtil.getFormattedDate(request.getFromDate(), START_OF_DAY);
        Date toDate = DateUtil.getFormattedDate(request.getToDate(), END_OF_DAY);

        if (fromDate == null) {
            LocalDate localDate = new LocalDate();
            if (new LocalDate().getMonthOfYear() < 4) {
                localDate = localDate.minusYears(1);
            }
            localDate = localDate.withMonthOfYear(4);
            fromDate = localDate.toDate();
        }
        if (toDate == null) {
            toDate = new LocalDate().toDate();
        }

        Map<String, Date> dates = new HashMap<>(2);
        dates.put("fromDate", fromDate);
        dates.put("toDate", toDate);
        return dates;
    }

    private Map<Long, String> getDistinctConstructsByGradeAndSection(String grade, String section) {
        Map<Long, String> constructs = new HashMap<>();
        QTestResultSummary testResultSummary = QTestResultSummary.testResultSummary;
        JPAQuery<Tuple> query = new JPAQuery<>(entityManager);
        query.select(testResultSummary.constructId, testResultSummary.constructName)
                .distinct()
                .from(testResultSummary)
                .where(testResultSummary.recordType.eq(RecordType.Construct))
                .where(testResultSummary.tenant.eq(getTenant()));

        if (!StringUtils.isEmpty(grade)) {
            query.where(testResultSummary.grade.eq(grade));
        }
        if (!StringUtils.isEmpty(section)) {
            query.where(testResultSummary.section.eq(section));
        }

        List<Tuple> fetch = query.fetch();
        fetch.forEach(f -> {
            constructs.put(f.get(testResultSummary.constructId), f.get(testResultSummary.constructName));
        });

        return constructs;
    }

    public Map<String, List<OverAllTestResultResponse>> getOverAllTestResultForGrades(DashboardRequest request) {
        Map<String, List<OverAllTestResultResponse>> resultByGrade = new HashMap<>();
        List<OverAllTestResultResponse> constructResults;
        Map<String, Long> scoreRangeAndCount = new HashMap<>();

        Map<String, Date> startAndEndDateOfAcademicYear = getStartAndEndDateOfAcademicYear(request);
        Date fromDate = startAndEndDateOfAcademicYear.get("fromDate");
        Date toDate = startAndEndDateOfAcademicYear.get("toDate");

        User user = getFreshUser();

        List<String> grades = new ArrayList<>();
        String section = "";

        if (!StringUtils.isEmpty(request.getResultType())) {
//      if (request.getResultType().equals("management"))
//      {
            grades = new ArrayList<>(getGradesByTeacher());
//      }
        } else {
            grades = new ArrayList<>(request.getGrades());

            for (String s : request.getSections()) {
                section = s;
            }
        }

        for (String grade : grades) {
            Map<Long, String> constructs = getSubjectsByTeacher(grade, section);

            constructResults = new ArrayList<>();

            for (Long constructId : constructs.keySet()) {
                JPAQuery<Tuple> query = new JPAQuery<>(entityManager);
                QTestResultSummary testResultSummary = QTestResultSummary.testResultSummary;

                OverAllTestResultResponse constructResult = new OverAllTestResultResponse();
//        constructResult.setName(constructId.get(testResultSummary.constructName));
                constructResult.setId(constructId);
                constructResult.setType("Construct");
                scoreRangeAndCount.clear();

                query.from(testResultSummary)
                        .where(testResultSummary.recordType.eq(RecordType.Construct))
                        .where(testResultSummary.grade.eq(grade))
                        .where(testResultSummary.constructId.eq(constructId))
                        .where(testResultSummary.tenant.eq(getTenant()))
                        .where(testResultSummary.createdDate.between(fromDate, toDate))
                        .groupBy(testResultSummary.scoreRange);

                if (!StringUtils.isEmpty(section)) {
                    query.where(testResultSummary.section.eq(section));
                }

                scoreRangeAndCount =
                        query.transform(
                                GroupBy.groupBy(testResultSummary.scoreRange.stringValue()).as(testResultSummary.scoreRange.count()));

                if (scoreRangeAndCount.size() > 0) {
                    constructResult.getValues().putAll(scoreRangeAndCount);
                    constructResults.add(constructResult);
                }

            }
            if (constructResults.size() > 0) {
                resultByGrade.putIfAbsent(grade, constructResults);
            }

        }

        return resultByGrade;
    }

    public Map<String, List<OverAllTestResultResponse>> getOverAllTestResultForSections(DashboardRequest request) {
        Map<String, List<OverAllTestResultResponse>> resultByGrade = new HashMap<>();
        List<OverAllTestResultResponse> constructResults = new ArrayList<>();
        Map<String, Long> scoreRangeAndCount = new HashMap<>();

        Map<String, Date> startAndEndDateOfAcademicYear = getStartAndEndDateOfAcademicYear(request);
        Date fromDate = startAndEndDateOfAcademicYear.get("fromDate");
        Date toDate = startAndEndDateOfAcademicYear.get("toDate");

        User user = getFreshUser();

        Set<String> grades = new HashSet<>();
        List<String> sections = new ArrayList<>();
        Set<Long> constructs = request.getProducts();

        grades = request.getGrades();

        sections = masterDataDocumentService.getAllSections().stream().map(MasterDataDocument::getValue).collect(Collectors.toList());

        for (String grade : grades) {
            for (String section : sections) {
                constructResults = new ArrayList<>();

                for (Long constructId : constructs) {
                    JPAQuery<Tuple> query = new JPAQuery<>(entityManager);
                    QTestResultSummary testResultSummary = QTestResultSummary.testResultSummary;

                    OverAllTestResultResponse constructResult = new OverAllTestResultResponse();
                    constructResult.setId(constructId);
                    constructResult.setType("Construct");
                    scoreRangeAndCount.clear();

                    query.from(testResultSummary)
                            .where(testResultSummary.recordType.eq(RecordType.Construct))
                            .where(testResultSummary.grade.eq(grade))
                            .where(testResultSummary.section.eq(section))
                            .where(testResultSummary.constructId.eq(constructId))
                            .where(testResultSummary.tenant.eq(getTenant()))
                            .where(testResultSummary.createdDate.between(fromDate, toDate))
                            .groupBy(testResultSummary.scoreRange);

                    scoreRangeAndCount =
                            query.transform(
                                    GroupBy.groupBy(testResultSummary.scoreRange.stringValue()).as(testResultSummary.scoreRange.count()));

                    if (scoreRangeAndCount.size() > 0) {
                        constructResult.getValues().putAll(scoreRangeAndCount);
                        constructResults.add(constructResult);
                    }
                }
                if (constructResults.size() > 0) {
                    resultByGrade.putIfAbsent(section, constructResults);
                }
            }
        }

        return resultByGrade;
    }

    public Map<String, List<OverAllTestResultResponse>> getOverAllTestResultForGradeAndSectionChapterWise(DashboardRequest request) {

        Map<String, List<OverAllTestResultResponse>> resultByGrade = new HashMap<>();
        List<OverAllTestResultResponse> constructResults;
        Map<String, Double> scoreRangeAndCount = new HashMap<>();

        Map<String, Date> startAndEndDateOfAcademicYear = getStartAndEndDateOfAcademicYear(request);
        Date fromDate = startAndEndDateOfAcademicYear.get("fromDate");
        Date toDate = startAndEndDateOfAcademicYear.get("toDate");

        List<String> grades = new ArrayList<>();
        String section = "";

        grades = new ArrayList<>(request.getGrades());

        for (String s : request.getSections()) {
            section = s;
        }

        for (String grade : grades) {
            Map<Long, String> constructs = getSubjectsByTeacher(grade, section);

            constructResults = new ArrayList<>();

            for (Long constructId : constructs.keySet()) {
                JPAQuery<Tuple> query = new JPAQuery<>(entityManager);
                QTestResultSummary testResultSummary = QTestResultSummary.testResultSummary;

                OverAllTestResultResponse constructResult = new OverAllTestResultResponse();
                constructResult.setId(constructId);
                constructResult.setType("");
                scoreRangeAndCount.clear();

                query.from(testResultSummary)
                        .select(testResultSummary.testId, testResultSummary.percentageScore.avg())
                        .where(testResultSummary.recordType.eq(RecordType.Test))
                        .where(testResultSummary.grade.eq(grade))
                        .where(testResultSummary.constructId.eq(constructId))
                        .where(testResultSummary.tenant.eq(getTenant()))
                        .where(testResultSummary.createdDate.between(fromDate, toDate))
                        .groupBy(testResultSummary.testId);

                if (!StringUtils.isEmpty(section)) {
                    query.where(testResultSummary.section.eq(section));
                }

                List<Tuple> fetch = query.fetch();

                for (Tuple item : fetch) {
                    String s = item.get(testResultSummary.testId).toString();
                    Double aDouble = item.get(testResultSummary.percentageScore.avg());
                    constructResult.getValues().put(s, aDouble == null ? 0L : aDouble.longValue());
                }
                if (constructResult.getValues().size() > 0) {
                    constructResults.add(constructResult);
                }

            }
            if (constructResults.size() > 0) {
                resultByGrade.putIfAbsent(grade, constructResults);
            }

        }

        return resultByGrade;
    }

    public Map<Long, List<TestResultSummaryResponse>> getOverAllTestResultForGradeAndSectionsUserList(DashboardRequest request) {

        Map<String, Date> startAndEndDateOfAcademicYear = getStartAndEndDateOfAcademicYear(request);
        Date fromDate = startAndEndDateOfAcademicYear.get("fromDate");
        Date toDate = startAndEndDateOfAcademicYear.get("toDate");

        User user = getFreshUser();

        String grade = "";
        String section = "";

        for (String g : request.getGrades()) {
            grade = g;
        }
        for (String s : request.getSections()) {
            section = s;
        }
        Map<Long, String> subjects = getSubjectsByTeacher(grade, section);
        JPAQuery<TestResultSummary> query = new JPAQuery<>(entityManager);
        QTestResultSummary testResultSummary = QTestResultSummary.testResultSummary;

        query.from(testResultSummary)
                .distinct()
                .where(testResultSummary.recordType.eq(RecordType.Construct))
                .where(testResultSummary.grade.eq(grade))
                .where(testResultSummary.section.eq(section))
                .groupBy(testResultSummary.userId, testResultSummary.constructId)
                .where(testResultSummary.tenant.eq(getTenant()))
                .orderBy(testResultSummary.constructName.asc())
                .where(testResultSummary.createdDate.between(fromDate, toDate));
        List<TestResultSummary> testResultSummaries = query.fetch();
        Map<Long, List<TestResultSummaryResponse>> result = new HashMap<>();

        testResultSummaries.forEach(summary -> {

            if (subjects.containsKey(summary.getConstructId())) {
                TestResultSummaryResponse response = new TestResultSummaryResponse();
                List<TestResultSummaryResponse> r = result.get(summary.getUserId());
                if (r == null) {
                    response.setName(summary.getStudentName());
                    response.setUserId(summary.getUserId());
                    response.setType(summary.getConstructName());
                    response.setConstructId(summary.getConstructId());
                    response.setSyllabusCompleted(summary.getSyllabusCompleted().intValue());
                    response.setPercentageScore(summary.getPercentageScore().floatValue());

                    List<TestResultSummaryResponse> responseList = new ArrayList<>();

                    responseList.add(response);

                    result.put(summary.getUserId(), responseList);
                } else {
                    response.setName(summary.getStudentName());
                    response.setUserId(summary.getUserId());
                    response.setConstructId(summary.getConstructId());
                    response.setType(summary.getConstructName());
                    response.setSyllabusCompleted(summary.getSyllabusCompleted().intValue());
                    response.setPercentageScore(summary.getPercentageScore().floatValue());
                    r.add(response);
                }
            }

        });
        return result;
    }

    public List<String> getOverAllTestResultForGradeAndSectionConstructList(DashboardRequest request) {
        Map<String, Date> startAndEndDateOfAcademicYear = getStartAndEndDateOfAcademicYear(request);
        Date fromDate = startAndEndDateOfAcademicYear.get("fromDate");
        Date toDate = startAndEndDateOfAcademicYear.get("toDate");

        User user = getFreshUser();

        String grade = "";
        String section = "";

        for (String g : request.getGrades()) {
            grade = g;
        }
        for (String s : request.getSections()) {
            section = s;
        }

        JPAQuery<String> query = new JPAQuery<>(entityManager);
        QTestResultSummary testResultSummary = QTestResultSummary.testResultSummary;
        query.from(testResultSummary)
                .select(testResultSummary.constructName)
                .distinct()
                .where(testResultSummary.recordType.eq(RecordType.Construct))
                .where(testResultSummary.grade.eq(grade))
                .where(testResultSummary.section.eq(section))
                .where(testResultSummary.tenant.eq(getTenant()))
                .orderBy(testResultSummary.constructName.asc())

                .where(testResultSummary.createdDate.between(fromDate, toDate));
//             .where(testResultSummary.createdDate.after(fromDate))
//             .where(testResultSummary.createdDate.before(toDate))
        return query.fetch();
    }

    public List<TestResultSummaryResponse> getOverAllTestResultForGradeAndSectionAndUserAndConstruct(DashboardRequest request) {
        TestResultSummaryResponse chapterResult;
        List<TestResultSummaryResponse> results = new ArrayList<>();

        Map<String, Date> startAndEndDateOfAcademicYear = getStartAndEndDateOfAcademicYear(request);
        Date fromDate = startAndEndDateOfAcademicYear.get("fromDate");
        Date toDate = startAndEndDateOfAcademicYear.get("toDate");

        User user = getFreshUser();

        String grade = "";
        String section = "";
        Long constructId = 0L;
        Long userId = 0L;

        for (Long id : request.getUserIds()) {
            userId = id;
        }
        for (Long product : request.getProducts()) {
            constructId = product;
        }

        for (String g : request.getGrades()) {
            grade = g;
        }
        for (String s : request.getSections()) {
            section = s;
        }

        JPAQuery<TestResultSummary> query = new JPAQuery<>(entityManager);
        QTestResultSummary testResultSummary = QTestResultSummary.testResultSummary;

        query.from(testResultSummary)
                .where(testResultSummary.recordType.eq(RecordType.Test))
                .where(testResultSummary.grade.eq(grade))
                .where(testResultSummary.section.eq(section))
                .where(testResultSummary.constructId.eq(constructId))
                .where(testResultSummary.tenant.eq(getTenant()))
                .where(testResultSummary.userId.eq(userId))
                .where(testResultSummary.createdDate.between(fromDate, toDate));
        List<TestResultSummary> summaries = query.fetch();
        for (TestResultSummary summary : summaries) {
            chapterResult = new TestResultSummaryResponse();
            chapterResult.setName(summary.getTestName());
            chapterResult.setType(summary.getConstructName());
            chapterResult.setPercentageScore(summary.getPercentageScore().floatValue());
            results.add(chapterResult);
        }

        return results;
    }

    public List<Map<Long, String>> getAttendanceSheetByGradeAndSectionAndConstruct(DashboardRequest request) {
        List<Map<Long, String>> results = new ArrayList<>();
        Map<String, Date> startAndEndDateOfAcademicYear = getStartAndEndDateOfAcademicYear(request);
        Date fromDate = startAndEndDateOfAcademicYear.get("fromDate");
        Date toDate = startAndEndDateOfAcademicYear.get("toDate");

        User teacher = getFreshUser();

        String grade = "";
        String section = "";
        Long constructId = 0L;

        for (Long product : request.getProducts()) {
            constructId = product;
        }
        for (String g : request.getGrades()) {
            grade = g;
        }
        for (String s : request.getSections()) {
            section = s;
        }

        QUser user = QUser.user;
        QTestResultSummary testResultSummary = QTestResultSummary.testResultSummary;

        if (StringUtils.isEmpty(grade) || StringUtils.isEmpty(section) || StringUtils.isEmpty(constructId)) {
            throw new ValidationException("request is not complete");
        }

        /* Get the students */
        JPAQuery<Tuple> query = new JPAQuery<>(entityManager);
        List<Tuple> students = query.from(user)
                .select(user.id, user.name)
                .where(user.grade.eq(grade))
                .where(user.section.eq(section))

                .where(testResultSummary.createdDate.between(fromDate, toDate))
//         .where(testResultSummary.createdDate.before(toDate))
//         .where(testResultSummary.createdDate.after(fromDate))
                .where(user.tenant.eq(getTenant())).fetch();

        Map<Long, String> users = new HashMap<>();
        students.forEach(student -> {
            users.put(student.get(user.id), student.get(user.name));
        });

        /* Get the Tests */
        JPAQuery<Long> getChapterIdsByConstructIdQuery = new JPAQuery<>(entityManager);
        List<Tuple> tests = getChapterIdsByConstructIdQuery.from(testResultSummary)
                .select(testResultSummary.testId, testResultSummary.testName)
                .distinct()
                .where(testResultSummary.recordType.eq(RecordType.Test))
                .where(testResultSummary.constructId.eq(constructId))
                .where(testResultSummary.grade.eq(grade))
                .where(testResultSummary.section.eq(section))
                .where(testResultSummary.createdDate.between(fromDate, toDate))
//         .where(testResultSummary.createdDate.before(toDate))
//         .where(testResultSummary.createdDate.after(fromDate))
                .where(testResultSummary.tenant.eq(getTenant())).fetch();

        Map<Long, String> chapters = new TreeMap<Long, String>();
        tests.forEach(test -> {
            chapters.put(test.get(testResultSummary.testId), test.get(testResultSummary.testName));
        });

        JPAQuery<Long> attendanceQuery;
        Map<Long, String> attendanceSheet = new HashMap<>(students.size());

        for (Tuple student : students) {
            StringBuilder attendance = new StringBuilder();

            for (Map.Entry<Long, String> testPair : chapters.entrySet()) {
                attendanceQuery = new JPAQuery<>(entityManager);
                List<Long> result = attendanceQuery.from(testResultSummary)
                        .select(testResultSummary.id)
                        .where(testResultSummary.userId.eq(student.get(user.id)))
                        .where(testResultSummary.recordType.eq(RecordType.Test))
                        .where(testResultSummary.testId.eq(testPair.getKey()))
                        .where(testResultSummary.tenant.eq(getTenant()))
                        .where(testResultSummary.createdDate.between(fromDate, toDate)).fetch();

                if (result.size() > 0) {
                    if (!StringUtils.isEmpty(attendance.toString())) {
                        attendance.append(",");
                    }
                    attendance.append("Taken");
                } else {
                    if (!StringUtils.isEmpty(attendance.toString())) {
                        attendance.append(",");
                    }
                    attendance.append("Not Taken");
                }

            }
            attendanceSheet.put(student.get(user.id), attendance.toString());
        }

        results.add(users);
        results.add(chapters);
        results.add(attendanceSheet);
        return results;
    }

    public Set<String> getGradesByTeacher() {
        Set<String> grades = new HashSet<>();
        Teacher teacher = getFreshUser().getTeacher();
        if (teacher == null) {
            throw new ValidationException("Current user is not a teacher");
        }
        if (teacher.isPrincipal()) {
            grades = masterDataDocumentService.getAllGrades().stream().map(MasterDataDocument::getValue).collect(Collectors.toSet());
        }
        if (teacher.isHod() || teacher.isClassTeacher() || teacher.isSubjectTeacher()) {
            grades = teacher.getGradeSectionSubjects().stream().map(GradeSectionSubject::getGrade).collect(Collectors.toSet());
        }

        if (teacher.isClassTeacher()) {
            if (!StringUtils.isEmpty(teacher.getGrade())) {
                grades.add(teacher.getGrade());
            }
        }

        return grades;
    }

    public Set<String> getSectionsByTeacher() {

        Set<String> sections = new HashSet<>();
        Teacher teacher = getFreshUser().getTeacher();
        if (teacher == null) {
            throw new ValidationException("Current user is not a teacher");
        }
        if (teacher.isPrincipal()) {
            sections = masterDataDocumentService.getAllSections().stream().map(MasterDataDocument::getValue).collect(Collectors.toSet());
        }
        if (teacher.isHod() || teacher.isClassTeacher() || teacher.isSubjectTeacher()) {
            sections = teacher.getGradeSectionSubjects().stream().map(GradeSectionSubject::getSection).collect(Collectors.toSet());

        }
        if (teacher.isClassTeacher()) {
            if (!StringUtils.isEmpty(teacher.getSection())) {
                sections.add(teacher.getSection());
            }
        }

        return sections;
    }

    public Map<Long, String> getSubjectsByTeacher(String grade, String section) {

        Map<Long, String> subjects = new HashMap<>();
        Teacher teacher = getFreshUser().getTeacher();
        if (teacher == null) {
            throw new ValidationException("Current user is not a teacher");
        }
        if (teacher.isPrincipal()) {
            subjects = getDistinctConstructsByGradeAndSection(grade, section);
        }
        if (teacher.isHod()) {
            subjects = teacher.getGradeSectionSubjects()
                    .stream()
                    .filter(gradeSectionSubject -> StringUtils.isEmpty(grade) || gradeSectionSubject.getGrade().equals(grade))
                    .collect(Collectors.toMap(GradeSectionSubject::getSubject, GradeSectionSubject::getSubjectName,
                            (subject1, subject2) -> {
                                return subject1;
                            }));
        }
        if (teacher.isHod() || teacher.isClassTeacher() || teacher.isSubjectTeacher()) {
            subjects = teacher.getGradeSectionSubjects()
                    .stream()
                    .filter(gradeSectionSubject -> StringUtils.isEmpty(grade) || gradeSectionSubject.getGrade().equals(grade))
                    .filter(gradeSectionSubject -> StringUtils.isEmpty(section) || gradeSectionSubject.getSection().equals(section))
                    .collect(Collectors.toMap(GradeSectionSubject::getSubject, GradeSectionSubject::getSubjectName,
                            (subject1, subject2) -> {
                                return subject1;
                            }));

            if (teacher.isClassTeacher()) {
                if (!StringUtils.isEmpty(teacher.getGrade())) {
                    if (!StringUtils.isEmpty(teacher.getSection())) {
                        if (teacher.getGrade().equals(grade) && teacher.getSection().equals(section)) {
                            Map<Long, String> classTeacherSubjects = getDistinctConstructsByGradeAndSection(grade, section);

                            subjects.putAll(classTeacherSubjects);
                        }
                    }
                }

            }

        }

        return subjects;
    }

    public Map<String, Map<String, Long>> getNumberOfStudentsByGrade(DashboardRequest request) {
        List<MasterDataDocument> grades = masterDataDocumentService.getAllGrades();
        List<MasterDataDocument> sections = masterDataDocumentService.getAllSections();

        Map<String, Map<String, Long>> studentCountByGrade = new HashMap<>();

        Map<String, Date> startAndEndDateOfAcademicYear = getStartAndEndDateOfAcademicYear(request);
        Date fromDate = startAndEndDateOfAcademicYear.get("fromDate");
        Date toDate = startAndEndDateOfAcademicYear.get("toDate");

        Long count = 0L;
        QUser user = QUser.user;
        QRole role = QRole.role;
        Map<String, Long> records;
        Role testTakerRole = roleRepository.getOneByName("TestTaker");

        for (MasterDataDocument grade : grades) {
            JPAQuery<Long> query = new JPAQuery<>(entityManager);
            count = query.from(user)
                    .where(user.grade.eq(grade.getValue()))
                    .where(user.tenant.eq(getTenant()))
                    .innerJoin(user.roles, role).where(role.id.eq(testTakerRole.getId()))
                    .fetchCount();

            records = new HashMap<>();
            records.put("all", count);

            for (MasterDataDocument section : sections) {
                JPAQuery<Long> query1 = new JPAQuery<>(entityManager);

                count = query1.from(user)
                        .where(user.grade.eq(grade.getValue()))
                        .where(user.section.eq(section.getValue()))
                        .where(user.tenant.eq(getTenant()))
                        .innerJoin(user.roles, role).where(role.id.eq(testTakerRole.getId()))
                        .fetchCount();

                records.put(section.getValue(), count);
            }
            studentCountByGrade.put(grade.getValue(), records);
        }
        return studentCountByGrade;
    }

    public List<String> getSchoolNamesFromUserTable() {
        QUser user = QUser.user;
        QSchool school = QSchool.school;
        JPAQuery<String> query = new JPAQuery<>(entityManager);

        List<String> schoolNames = query.select(user.schoolName)
                .from(user)
                .distinct()
                .orderBy(user.schoolName.asc())
                .fetch();

        query = new JPAQuery<>(entityManager);

        List<String> schools = query.select(school.name)
                .from(school)
                .distinct()
                .fetch();

        schoolNames.removeIf(schools::contains);

        return schoolNames;
    }
}
