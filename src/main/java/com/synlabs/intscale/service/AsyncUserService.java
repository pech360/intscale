package com.synlabs.intscale.service;

import com.synlabs.intscale.ImportModel.ImportUserModel;
import com.synlabs.intscale.ImportModel.ImportUserTestAssignModel;
import com.synlabs.intscale.common.EmailMessage;
import com.synlabs.intscale.entity.user.Role;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.enums.AcademicPerformanceName;
import com.synlabs.intscale.enums.AcademicPerformanceType;
import com.synlabs.intscale.jpa.RoleRepository;
import com.synlabs.intscale.jpa.UserRepository;
import com.synlabs.intscale.service.communication.CommunicationService;
import com.synlabs.intscale.view.AcademicPerformanceRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Service
public class AsyncUserService {

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private TestService testService;
    @Autowired
    private CommunicationService communicationService;

    private static Logger logger = LoggerFactory.getLogger(AsyncUserService.class);


    @Async
    public void assignProductToUsers(ArrayList<ImportUserTestAssignModel> importUserTestList, String emailTo, User currentUser) {
        for (ImportUserTestAssignModel importUserTest : importUserTestList) {
            testService.userTestAssignmentByExcelSheet(importUserTest.getUsername(), importUserTest.getProduct(), importUserTest.getLanguage(), currentUser);
        }

        String subject = "Aim2excel:User Test Assignment Notification";
        String message = "This is to notify you that the user test assignment task has now been completed. Assigned users no." + importUserTestList.size();
        EmailMessage emailMessage = new EmailMessage(new String[]{emailTo}, subject, message);
        communicationService.sendGeneralEmail(emailMessage);
    }

    @Async
    public void saveImportedUsers(String rootAccess, ArrayList<ImportUserModel> importUsersList, User currentUser, String emailTo, boolean allowMainAimReport, boolean allowMiniAimReport) {
        //List<Student> studentList = null;
        logger.info("Trying to import " + importUsersList.size() + " users asynchronously");
        Role defaultRole = roleRepository.getOneByName("TestTaker");
        List<Role> defaultRoleList = new ArrayList<>();
        if (defaultRole != null) {
            defaultRoleList.add(defaultRole);
        }

        for (ImportUserModel importUser : importUsersList) {

            User user = userService.convertImportUserModelToUser(importUser, defaultRoleList,allowMainAimReport,allowMiniAimReport);
            if (user != null) {
                if (rootAccess.equalsIgnoreCase("true")) {
                    user.setPasswordHash(importUser.getPassword());
                }

                user.setCreatedBy(currentUser);
                user.setTenant(currentUser.getTenant());
                user = userRepository.save(user);
                communicationService.registerWelcomeEmail(user, importUser.getPassword(), currentUser.getTenant().getSubDomain());
                //Student student = saveStudent(user);
                // studentList.add(student);

                if (!StringUtils.isEmpty(user.getGrade())) {
                    if (user.getGrade().equals("9") || user.getGrade().equals("10")) {
                        List<AcademicPerformanceRequest> academicPerformances = new ArrayList<>(2);

                        AcademicPerformanceRequest academicPerformanceRequest = new AcademicPerformanceRequest(AcademicPerformanceType.SUBJECT, AcademicPerformanceName.SCIENCE, BigInteger.valueOf(importUser.getScience()));
                        academicPerformances.add(academicPerformanceRequest);
                        academicPerformanceRequest = new AcademicPerformanceRequest(AcademicPerformanceType.SUBJECT, AcademicPerformanceName.MATHEMATICS, BigInteger.valueOf(importUser.getMaths()));
                        academicPerformances.add(academicPerformanceRequest);
                        userService.saveAcademicPerformances(academicPerformances, user);
                    }
                }
            }

        }
        logger.info(importUsersList.size() + " users imported.");
        // studentRepository.save(studentList);

        String subject = "Aim2excel:User Import Notification";
        String message = "This is to notify you that the user import task has now been completed. Imported users no." + importUsersList.size();
        EmailMessage emailMessage = new EmailMessage(new String[]{emailTo}, subject, message);
        communicationService.sendGeneralEmail(emailMessage);
    }
}
