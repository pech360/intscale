package com.synlabs.intscale.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import com.synlabs.intscale.entity.paymentGateway.TransactionEntry;
import com.synlabs.intscale.ex.NotFoundException;
import com.synlabs.intscale.jpa.TransactionEntryRepository;
import com.synlabs.intscale.view.paymentGateway.TransactionEntryView;
import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.PROMO_CODE;

//LNT
@Service
public class TransactionEntryService {
	@Autowired
	private TransactionEntryRepository transactionEntryRepository ;
	
	public List<TransactionEntryView> getAllTransactions(){
		
		List<TransactionEntryView> transactionEntryViewList = new ArrayList<>();
		List<TransactionEntry> transactionEntryList = transactionEntryRepository.findAll();
		if(transactionEntryList.isEmpty())
			throw new NotFoundException("No Transactions to display..");
		for(TransactionEntry transactionEntry : transactionEntryList)
			transactionEntryViewList.add(transactionEntry.toTransactionEntryView());
		return transactionEntryViewList;
	}	
}

