package com.synlabs.intscale.service;

import com.synlabs.intscale.entity.masterdata.MasterDataDocument;
import com.synlabs.intscale.entity.test.*;
import com.synlabs.intscale.jpa.AnswersRepsitory;
import com.synlabs.intscale.jpa.GroupTextRepository;
import com.synlabs.intscale.jpa.QuestionRepository;
import com.synlabs.intscale.jpa.QuestionTagRepository;
import com.synlabs.intscale.jpa.SubTagRepository;
import com.synlabs.intscale.util.StringUtil;
import com.synlabs.intscale.view.question.*;
import com.synlabs.intscale.view.usertest.GroupTextRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class QuestionService extends BaseService {
    @Autowired
    private QuestionRepository questionsRepository;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private MasterDataDocumentService masterDataDocumentService;
    @Autowired
    private AnswersRepsitory answersRepsitory;
    @Autowired
    private SubTagRepository subTagRepository;
    @Autowired
    private GroupTextRepository groupTextRepository;

    public List<Question> findAll() {
        return questionsRepository.findAllByActiveAndTenant(true, getTenant());
    }

    public List<Question> findByCustomSearch(String questionType, Long subtagId) {
        if ((questionType != null && !questionType.equals("undefined") && !questionType.equals("")) && (subtagId != null && !subtagId.equals("undefined") && !subtagId.equals(""))) {
            return questionsRepository.findAllByQuestionTypeAndSubTagsIdAndActiveAndTenant(questionType, subtagId, true, getTenant());
        } else if (questionType != null && !questionType.equals("undefined") && !questionType.equals("")) {
            return questionsRepository.findAllByQuestionTypeAndActiveAndTenant(questionType, true, getTenant());
        } else if (subtagId != null && !subtagId.equals("undefined") && !subtagId.equals("")) {
            return questionsRepository.findAllBySubTagsIdAndActiveAndTenant(subtagId, true, getTenant());
        } else {
            return questionsRepository.findAllByActiveAndTenant(true, getTenant());
        }

    }

    public Question save(QuestionRequest questionRequest) {
        List<Answers> answers = new ArrayList<>();
        Question question = null;
        if (StringUtils.isEmpty(questionRequest.getId())) {
            question = questionRequest.toEntity();
        } else {
            question = questionRequest.toEntity(questionsRepository.findOne(questionRequest.getId()));
            question.getSubTags().clear();
        }

        if (!StringUtils.isEmpty(question.getGroupName()) && masterDataDocumentService.findOneByValue(question.getGroupName()) == null) {
            MasterDataDocument md = new MasterDataDocument();
            md.setType("Group");
            md.setValue(question.getGroupName());
            masterDataDocumentService.save(md);
        }

        if (questionRequest.getSubTagIds().size() > 0) {
            for (Long subtagId : questionRequest.getSubTagIds()) {
                question.addTag(subTagRepository.findOne(subtagId));
            }
        }

        question = questionsRepository.save(question);


        if (!StringUtils.isEmpty(question.getGroupName())) {


            GroupText groupText = findOneByGroupText(question.getGroupName());
            question.setGroupText(groupText);

        }

        if (StringUtils.isEmpty(questionRequest.getId())) {
            int i = 1;
            for (AnswerRequest ar : questionRequest.getAnswerRequests()) {
                Answers newAnswers = ar.toEntity();
                if ("Text".equals(questionRequest.getQuestionType())) {
                    newAnswers.setHasText(true);
                }
                newAnswers.setOptionNumber(i++);
                newAnswers.setQuestion(question);
                answers.add(newAnswers);
            }
            answersRepsitory.save(answers);
            i = 1;
        } else {
            int i = 0;
            List<Answers> preAnswers = question.getAnswers();
            for (AnswerRequest ar : questionRequest.getAnswerRequests()) {
                if (!(i >= preAnswers.size())) {
                    Answers newAnswers = preAnswers.get(i);
                    newAnswers.setDescription(ar.getDescription());
                    newAnswers.setMarks(ar.getMarks());
                    if ("Text".equals(questionRequest.getQuestionType())) {
                        newAnswers.setHasText(true);
                    }
                    answers.add(newAnswers);
                } else {
                    Answers newAnswers = ar.toEntity();
                    if ("Text".equals(questionRequest.getQuestionType())) {
                        newAnswers.setHasText(true);
                    }
                    newAnswers.setOptionNumber(i + 1);
                    newAnswers.setQuestion(question);
                    answers.add(newAnswers);
                }
                i++;
            }
            if (i > preAnswers.size()) {
                question.setNumberOfAnswers(questionRequest.getAnswerRequests().size());
                questionsRepository.save(question);
            } else if (i < preAnswers.size()) {
                preAnswers.forEach(preOneAns -> {
                    if (preOneAns.getOptionNumber() > questionRequest.getAnswerRequests().size()) {
                        preOneAns.setQuestion(null);
                        answersRepsitory.save(preOneAns);
                    }
                });
            }
            answersRepsitory.save(answers);
        }
        return question;
    }

    public void attachQuestionImage(String filename, Long id) {
        Question question = questionsRepository.findOne(id);
        question.setImageName(filename);
        questionsRepository.save(question);
    }

    public void attachQuestionVideo(String filename, Long id) {
        Question question = questionsRepository.findOne(id);
        question.setVideoName(filename);
        questionsRepository.save(question);
    }

    public void attachQuestionAudio(String filename, Long id) {
        Question question = questionsRepository.findOne(id);
        question.setAudioName(filename);
        questionsRepository.save(question);
    }

    public void attachAnswerAudio(List<String> filename, Long id) {
        Question question = questionsRepository.findOne(id);
        List<Answers> preAnswers = question.getAnswers();
        for (int i = 0; i < filename.size(); i++) {
            Answers ans = preAnswers.get(i);
            ans.setHasAudio(true);
            ans.setAudioName(filename.get(i));
            answersRepsitory.save(ans);
        }
    }

    public void attachAnswerAudio(List<String> filename, List<Integer> integerList, Long id) {
        List<Integer> realInteger = new ArrayList<>();
        integerList.forEach(i -> {
            if (i != null) {
                realInteger.add(i);
            }
        });
        Question question = questionsRepository.findOne(id);
        List<Answers> preAnswers = question.getAnswers();
        List<Answers> updatedAnswers = new ArrayList<>();
        for (int i = 0; i < filename.size(); i++) {
            Answers ans = preAnswers.get(realInteger.get(i));
            ans.setHasAudio(true);
            ans.setAudioName(filename.get(i));
            updatedAnswers.add(ans);
        }
        if (preAnswers.size() > realInteger.size()) {
            for (int i = 0; i < preAnswers.size(); i++) {
                if (!realInteger.contains(i)) {
                    Answers ans = preAnswers.get(i);
                    updatedAnswers.add(ans);
                }
            }
        }
        answersRepsitory.save(updatedAnswers);
    }

    public void attachAnswerImage(List<String> filename, Long id) {
        Question question = questionsRepository.findOne(id);
        List<Answers> preAnswers = question.getAnswers();
        for (int i = 0; i < filename.size(); i++) {
            Answers ans = preAnswers.get(i);
            ans.setHasImage(true);
            ans.setImageName(filename.get(i));
            answersRepsitory.save(ans);
        }
    }

    public void attachAnswerImage(List<String> filename, List<Integer> integerList, Long id) {
        List<Integer> realInteger = new ArrayList<>();
        integerList.forEach(i -> {
            if (i != null) {
                realInteger.add(i);
            }
        });
        Question question = questionsRepository.findOne(id);
        List<Answers> preAnswers = question.getAnswers();
        List<Answers> updatedAnswers = new ArrayList<>();
        for (int i = 0; i < filename.size(); i++) {
            Answers ans = preAnswers.get(realInteger.get(i));
            ans.setHasImage(true);
            ans.setImageName(filename.get(i));
            updatedAnswers.add(ans);
        }
        if (preAnswers.size() > realInteger.size()) {
            for (int i = 0; i < preAnswers.size(); i++) {
                if (!realInteger.contains(i)) {
                    Answers ans = preAnswers.get(i);
                    updatedAnswers.add(ans);
                }
            }
        }
        answersRepsitory.save(updatedAnswers);
    }

    public Question findOneByGroupName(String groupName) {
        return questionsRepository.findFirstByGroupNameAndActiveAndTenant(groupName, true, getTenant());
    }

    public void deleteQuestion(Long id) {
        Question question = questionsRepository.findOne(id);
        question.setActive(false);
        questionsRepository.save(question);
    }

    public void deleteMultipleQuestions(List<Long> questionIds) {
        questionIds.forEach(id -> {
            deleteQuestion(id);
        });
    }

    public Question getOneQuestion(Long questionId) {
        Question question = questionsRepository.findOne(questionId);
        return question;
    }

    public void saveGroupActivity(String fileName, GroupTextRequest request) {
        GroupText text = groupTextRepository.findByGroupNameAndTenant(request.getGroupName(), getTenant());
        if (text == null) {
            text = new GroupText();
        }
        if (request.getCommonType().equals("Image")) {
            text.setHasImage(true);
            text.setImageName(fileName);
        } else if (request.getCommonType().equals("Audio")) {
            text.setHasAudio(true);
            text.setAudioName(fileName);
        } else if (request.getCommonType().equals("Video")) {
            text.setHasVideo(true);
            text.setVideoName(fileName);
        }
        text.setCommonText(request.getCommonText());
        if (request.getCommonText().equals("null")) {
            text.setCommonText("");
        }
        text.setGroupName(request.getGroupName());
        text.setType(request.getCommonType());
        text = groupTextRepository.save(text);
        if (text != null) {
            for (Question question : questionsRepository.findAllByGroupNameAndTenant(request.getGroupName(), getTenant())) {
                question.setGroupText(text);
                questionsRepository.save(question);
            }
        }
    }

    public GroupText findOneByGroupText(String groupName) {
        return groupTextRepository.findByGroupNameAndTenant(groupName, getTenant());
    }

    public Answers getCorrectAnswer(Long id) {
        Question question = questionsRepository.findOne(id);
        Answers correctAnswer = question.getAnswers().get(0);
        for (Answers answer : question.getAnswers()) {
            if (correctAnswer.getMarks() < answer.getMarks()) {
                correctAnswer = answer;
            }
        }
        return correctAnswer;
    }

}
