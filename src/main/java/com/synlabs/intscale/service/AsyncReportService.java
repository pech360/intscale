package com.synlabs.intscale.service;

import com.synlabs.intscale.ImportModel.ImportUserTestAssignModel;
import com.synlabs.intscale.common.EmailMessage;
import com.synlabs.intscale.entity.Report.Report;
import com.synlabs.intscale.entity.Report.UserReportStatus;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.enums.AimReportType;
import com.synlabs.intscale.enums.ReportStatus;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.*;
import com.synlabs.intscale.service.communication.CommunicationService;
import com.synlabs.intscale.util.DateUtil;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class AsyncReportService extends BaseService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TimelineService timelineService;
    @Autowired
    private ReportService reportService;
    @Autowired
    private CommunicationService communicationService;

    @Value("${scale.image.upload.location}")
    protected String filedir;

    private static Logger logger = LoggerFactory.getLogger(ReportService.class);

    @Async
    public void processAimReport(User user, Tenant tenant, Report report, AimReportType reportType, boolean isAutoShare) throws IOException {
        logger.info("processing "+reportType+" report of " + user.getUsername());
        File file = null;
        UserReportStatus userReportStatus = reportService.getUserReportStatusByUser(user.getId(), tenant);
        try {
            boolean enableAimReport = reportService.allowReportToUser(user, report, tenant);
            if (enableAimReport) {
//                timelineService.savePostForAimReport(user);
                if (userReportStatus != null) {
                    file = reportService.createReport(userReportStatus, tenant, reportType);
                    if (file == null) {
                        reportError(userReportStatus);
                    } else {
                        logger.info(userReportStatus.getUser().getUsername() + "'s report made and saved successfully");
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Something went wrong while making report of: "+userReportStatus.getUser().getUsername(), e);
            reportError(userReportStatus);
        }

        try{
            if(file!=null){
                if(isAutoShare){
                    logger.info("Sending report to testTaker-"+userReportStatus.getUser().getUsername() );
                    communicationService.sendReportToTestTaker(user, file);
                }
                file.deleteOnExit();
            }

        }catch (Exception e){
            logger.error("Something went wrong with report of(while sending or deleting)"+userReportStatus.getUser().getUsername(), e);
        }

    }

    @Async
    public void processMultipleReports(Collection<User> users, Tenant tenant, AimReportType reportType) {
        Report report = reportService.getReport("Aim Composite Report");
        users.forEach(user -> {
            reportService.setUserReportStatus(user, report, ReportStatus.Queued, null, null);
        });
        users.forEach(user -> {
            try {
                processAimReport(user, tenant, report, reportType, false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Async
    public void makeScheduledReport() throws IOException {

//        logger.info("scheduler started at " + DateUtil.formatDate(new Date()));
        UserReportStatus userReportStatus = reportService.getOneUserReportStatus(ReportStatus.Wait);

        if (userReportStatus == null) {
//            try to recovering previous issues if any
            userReportStatus = reportService.getOneUserReportStatusByDate(ReportStatus.Generating, new DateTime().minusMinutes(60).toDate());
        }
        if (userReportStatus != null) {
            if (userReportStatus.getUser() != null) {

                Report report = reportService.getReport("Aim Composite Report");
                Tenant tenant = getTenantByName("aim");

                processAimReport(userReportStatus.getUser(), tenant, report, AimReportType.MAIN,userReportStatus.getUser().isAllowMainAimReport());
                processAimReport(userReportStatus.getUser(), tenant, report, AimReportType.MINI,userReportStatus.getUser().isAllowMiniAimReport());
            }
        }
    }

    public void reportError(UserReportStatus userReportStatus) {
        logger.error("Error! in making " + userReportStatus.getUser().getUsername() + "'s report");
        reportService.setUserReportStatus(userReportStatus.getUser(), userReportStatus.getReportName(), ReportStatus.Failed, null, null);
    }

    @Async
    public void sendDemoTestCompleteEmail(Long userId) {
        User user = userRepository.findOne(userId);

        try {
            File file = reportService.downloadAimReport(user.getId(), user.getTenant(), AimReportType.DEMO);
            if (file == null || file.length() == 0) {
                throw new ValidationException("Coudn't create Demo Report for " + user.getUsername());
            }
            List<File> files = new ArrayList<>();
            files.add(file);
            communicationService.demoCompleteMail(user,files);
        } catch (Exception e) {
            logger.error("error in sending email of demo report ", e);
            communicationService.demoCompleteMail(user,null);
        }
    }


}
