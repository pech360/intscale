package com.synlabs.intscale.service;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.synlabs.intscale.common.EmailMessage;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.ResultRepository;
import com.synlabs.intscale.jpa.UserReportStatusRepository;
import com.synlabs.intscale.jpa.UserRepository;
import com.synlabs.intscale.service.communication.CommunicationService;
import com.synlabs.intscale.util.Activity;
import com.synlabs.intscale.util.ITextPdfUtil;
import com.synlabs.intscale.util.PdfTableUtil;
import com.synlabs.intscale.util.ProductName;
import com.synlabs.intscale.view.PlatformDailySummary;
//LNT
@Service
public class PlatformDailySummaryService extends BaseService {
	@Autowired
	private CommunicationService communicationService;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserReportStatusRepository userReportStatusRepository;
	@Autowired
	private ResultRepository resultRepository;
	@Value("${scale.image.upload.location}")
	protected String filedir;
	private static Logger logger = LoggerFactory.getLogger(PlatformDailySummaryService.class);

	public List<PlatformDailySummary> findAllSignedUpAndGivenDemoTest(Date from, Date to) {
		if (from.after(to))
			throw new ValidationException("Start Date can not be greater than end date.");
		List<Object[]> objectArrayList = resultRepository.findAllSignedUpAndGivenDemoTest(from, to);
		List<PlatformDailySummary> platformDailySummaryList = new ArrayList<>();
		if (objectArrayList == null || objectArrayList.isEmpty())
			return platformDailySummaryList;
		for (Object[] obj : objectArrayList) {
			PlatformDailySummary temp = new PlatformDailySummary(obj);
			temp.setActivity(Activity.TookDemo.getValue());
			platformDailySummaryList.add(temp);
		}
		return platformDailySummaryList;
	}
//present in user but not in test_result.... Need to be changed
	public List<PlatformDailySummary> findAllSignedUpAndNotGivenDemoTest(Date from, Date to) {
		if (from.after(to))
			throw new ValidationException("Start Date can not be greater than end date.");
		List<Object[]> objectArrayList = userRepository.findAllSignedUpAndNotGivenDemoTest(from, to);
		List<PlatformDailySummary> platformDailySummaryList = new ArrayList<>();
		if (objectArrayList == null || objectArrayList.isEmpty())
			return platformDailySummaryList;
		for (Object[] obj : objectArrayList) {
			PlatformDailySummary temp = new PlatformDailySummary(obj);
			temp.setActivity(Activity.SignUp.getValue());
			platformDailySummaryList.add(temp);
		}
		return platformDailySummaryList;
	}

	public List<PlatformDailySummary> findAllCompletedAssessment(Date from , Date to , String status){
		if (from.after(to))
			throw new ValidationException("Start Date can not be greater than end date.");
		List<Object[]> objectArrayList = userReportStatusRepository.findAllCompletedAssessment(from, to, status);
		List<PlatformDailySummary> platformDailySummaryList = new ArrayList<>();
		if (objectArrayList == null || objectArrayList.isEmpty())
			return platformDailySummaryList;
		for (Object[] obj : objectArrayList) {
			PlatformDailySummary temp = new PlatformDailySummary(obj);
			temp.setActivity(Activity.CompletedAssessment.getValue());
			platformDailySummaryList.add(temp);
		}
		return platformDailySummaryList;
	}
	
	public List<PlatformDailySummary> findAllPurchasedProductToday(Date from, Date to) {
		if (from.after(to))
			throw new ValidationException("Start Date can not be greater than end date .");
		List<Object[]> objectArrayList = userRepository.findAllPurchasedProductToday(from, to);
		List<PlatformDailySummary> platformDailySummaryList = new ArrayList<>();
		if (objectArrayList == null || objectArrayList.isEmpty())
			return platformDailySummaryList;
		for (Object[] obj : objectArrayList) {
			PlatformDailySummary temp = new PlatformDailySummary(obj);
			if (temp.getActivity().equalsIgnoreCase(ProductName.BLUEPRINT.getValue())) {
				temp.setActivity(Activity.PurchasedBlueprint.getValue());
			}
			if (temp.getActivity().equalsIgnoreCase(ProductName.EXPLORE.getValue())) {
				temp.setActivity(Activity.PurchasedExplore.getValue());
			}
			platformDailySummaryList.add(temp);
		}
		return platformDailySummaryList;
	}

	public void createPdfOfPlatformDailySummary(List<PlatformDailySummary> platformDailySummaryList, String filename,
			String[] mailTo, List<String> columnHeaders) {
		Document document = new Document(PageSize.A4, 10f, 10f, 10f, 10f);
		try {
			Files.createDirectories(Paths.get("/tmp/intscale/pdf/"));
			FileOutputStream fout = new FileOutputStream(String.format("/tmp/intscale/pdf/%s.pdf", filename));
			PdfWriter.getInstance(document, fout);
			document.open();
			ITextPdfUtil.setDocumentProperties(document);
			document.add(ITextPdfUtil.getHeading("Platform Daily Summary", Element.ALIGN_CENTER, 16, 2f));
			PdfPTable pdfTable = new PdfPTable(columnHeaders.size());
			PdfTableUtil.addTableHeader(pdfTable, columnHeaders);
			int sNo = 1;
			if (!platformDailySummaryList.isEmpty() && platformDailySummaryList != null) {
				for (PlatformDailySummary platformDailySummary : platformDailySummaryList) {
					PdfTableUtil.addTableRows(pdfTable,
							Arrays.asList(sNo, platformDailySummary.getName(), platformDailySummary.getUserName(),
									platformDailySummary.getEmail(), platformDailySummary.getPhoneNumber(),
									platformDailySummary.getActivity()));
					sNo++;
				}
			}
			pdfTable.setWidthPercentage(95f);
			document.add(pdfTable);
			EmailMessage emailMessage = new EmailMessage(mailTo, "Platform Summary for "
					+ new DateTime(LocalDate.now().minusDays(1).toDate(), DateTimeZone.forID("Asia/Kolkata"))
							.toDate().toString().substring(0, 10),
					"Please find the attachment below.");
			logger.info("Initiating email service , sending email to " + mailTo.toString());
			File file = new File(String.format("/tmp/intscale/pdf/%s.pdf", filename));
			List<File> fileToSend = new ArrayList<>();
			fileToSend.add(file);
			emailMessage.setAttachments(fileToSend);
			logger.info("Attaching the platform daily summary file");
			communicationService.sendGeneralEmail(emailMessage);
			logger.info("Email sent to " + mailTo.toString());
			document.close();
			fout.close();

		} catch (Exception e) {
			logger.error("Error sending email ", e);
			e.printStackTrace();
		}

	}

}
