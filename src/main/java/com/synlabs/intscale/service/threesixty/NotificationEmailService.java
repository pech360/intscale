package com.synlabs.intscale.service.threesixty;

import com.synlabs.intscale.enums.NotificationType;
import com.synlabs.intscale.jpa.threesixty.NotificationEmailRepository;
import com.synlabs.intscale.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class NotificationEmailService extends BaseService
{
  @Autowired
  private NotificationEmailRepository repository;

  public String[] getNotificationEmails(NotificationType type)
  {
    //Fetching all tenant notification emails
    List<String> tenantEmails = repository.findByTypeAndTenant(type, getTenant()).stream()
                                          .filter(n -> !StringUtils.isEmpty(n.getEmail()))
                                          .map(n -> n.getEmail())
                                          .collect(Collectors.toList());

    //Fetching null tenant emails
    List<String> nullTenantEmails = repository.findByTypeAndTenantIsNull(type).stream()
                                              .filter(n -> !StringUtils.isEmpty(n.getEmail()))
                                              .map(n -> n.getEmail())
                                              .collect(Collectors.toList());
    Set<String> emails = new HashSet<>();
    emails.addAll(tenantEmails);
    emails.addAll(nullTenantEmails);
    return emails.toArray(new String[] {});
  }
}
