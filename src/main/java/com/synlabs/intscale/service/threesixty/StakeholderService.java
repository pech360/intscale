package com.synlabs.intscale.service.threesixty;

import com.google.common.eventbus.EventBus;
import com.querydsl.jpa.impl.JPAQuery;
import com.synlabs.intscale.entity.test.QUserTest;
import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.test.UserTest;
import com.synlabs.intscale.entity.threesixty.Stakeholder;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.enums.EventType;
import com.synlabs.intscale.enums.StakeholderTestStatus;
import com.synlabs.intscale.enums.TestStatus;
import com.synlabs.intscale.enums.UserType;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.UserRepository;
import com.synlabs.intscale.jpa.UserTestRepository;
import com.synlabs.intscale.jpa.threesixty.StakeholderRepository;
import com.synlabs.intscale.service.BaseService;
import com.synlabs.intscale.service.event.bus.StakeholderEvent;
import com.synlabs.intscale.view.UserRequest;
import com.synlabs.intscale.view.threesixty.StakeholderRequest;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class StakeholderService extends BaseService
{
  @Value("${intscale.threesixty.loginValid}")
  private int validHours;

  @Value("${intscale.auth.secretkey}")
  private String secretkey;

  @Value("${intscale.auth.ttl_hours}")
  private int ttlhours;

  @Value("${intscale.s3.assets-url}")
  private String intscale_assets;

  @Autowired
  private StakeholderRepository stakeholderRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private UserTestRepository userTestRepository;

  @Autowired
  private EntityManager entityManager;

  @Autowired
  private StakeholderTestService stakeholderTestService;

  @Autowired
  private EventBus eventBus;

  public List<User> getUsers()
  {
    Tenant tenant = getTenant();
    JPAQuery<UserTest> query = new JPAQuery<>(entityManager);
    QUserTest userTest = QUserTest.userTest;
    List<UserTest> fetch = query.select(userTest)
                                .from(userTest)
                                .innerJoin(userTest.userEmail).fetchJoin()
                                .where(userTest.tenant.eq(tenant))
                                .where(userTest.userEmail.active.isTrue())
                                .fetch();
    return fetch.stream().map(s -> s.getUserEmail()).collect(Collectors.toList());
  }

  public User getStakeholders(Long userId)
  {
    User user = userRepository.findOne(userId);
    if (user == null)
    {
      throw new ValidationException(String.format("Cannot locate User[id = %s]", userId));
    }
    return user;
  }

  /**
   * Saves user stakeholders coming in user request
   *
   * @param request
   * @return
   */
  @Transactional
  public User save(UserRequest request)
  {
    if (request.getId() == null)
    {
      throw new ValidationException("User id is required.");
    }

    User user = userRepository.findOne(request.getId());
    if (user == null)
    {
      throw new ValidationException(String.format("Cannot locate User[id = %s]", request.getId()));
    }

    user.setThreeSixtyEnabled(request.getThreeSixtyEnabled());
    user.setThreeSixtyEnabled(isThreeSixty(user));

    if (isThreeSixty(user))
    {
      //1. Check user have assigned test or not
      UserTest userTest = userTestRepository.findByUserEmailAndTenant(user, getTenant());
      if (userTest == null)
      {
        throw new ValidationException("User is not assigned test.");
      }

      //2. check student completed all test
      String status = userTest.getTestStatus();
      if (!(status.equals(TestStatus.ATT.getStatus())
          || status.equals(TestStatus.TCS.getStatus())))
      {
        throw new ValidationException("Student did not completed all tests.");
      }

      //3. Check user have atleast one threesixty enabled test
      Set<Test> tests = stakeholderTestService.getStakeholderTests(user);
      if (CollectionUtils.isEmpty(tests))
      {
        throw new ValidationException("Student is not assigned any Three Sixty enabled test.");
      }

      List<StakeholderRequest> stakeholdersRequest = request.getStakeholders();
      List<Stakeholder> stakeholders = new ArrayList<>();
      if (!CollectionUtils.isEmpty(stakeholdersRequest))
      {
        stakeholdersRequest.forEach(s -> {
          Stakeholder stakeholder = saveStakeholder(s, user);
          stakeholders.add(stakeholder);
        });
      }
      user.setStakeholders(stakeholders);
    }
    userRepository.saveAndFlush(user);
    return getFreshUser(user.getId());
  }

  private Stakeholder saveStakeholder(StakeholderRequest request, User user)
  {
    validateStakeholderRequest(request);
    boolean newStakeholder = request.isNew();
    Stakeholder stakeholder = null;

    if (newStakeholder)
    {
      Stakeholder stk = stakeholderRepository.findByTenantAndUserAndRelation(getTenant(), user, request.getRelation());
      if (stk != null)
      {
        throw new ValidationException(String.format("Duplicate Stakeholder [relation=%s]", request.getRelation()));
      }
      stakeholder = request.toEntity(null);
      stakeholder.setLoginKey(generateStakeholderLoginKey());
      stakeholder.setStatus(StakeholderTestStatus.Created);
      stakeholder.setUser(user);

      User stakeholderUser = new User();
      stakeholderUser.setName(stakeholder.getName());
      stakeholderUser.setUserType(UserType.Stakeholder.toString());
      stakeholderUser.setEmail(stakeholder.getEmail());
      stakeholderUser.setActive(true);
      stakeholderUser.setUsername(generateSequence("STKHLDRUSR"));
      stakeholderUser.getRoles().add(getRoleByName("Stakeholder"));
      stakeholderUser.setPasswordHash(getRandomString(20));
      userRepository.saveAndFlush(stakeholderUser);

      stakeholder.setStakeholderUser(stakeholderUser);
      stakeholderRepository.saveAndFlush(stakeholder);
      sendStakeholderVerification(stakeholder.getId());
    }
    else
    {
      stakeholder = stakeholderRepository.findOne(request.getId());
      if (stakeholder == null)
      {
        throw new ValidationException(String.format("Cannot locate Stakeholder[id=%s]", request.getId()));
      }
      stakeholder = request.toEntity(stakeholder);
      stakeholderRepository.saveAndFlush(stakeholder);
    }
    return stakeholder;
  }

  private void validateStakeholderRequest(StakeholderRequest request)
  {
    if (StringUtils.isEmpty(request.getName()))
    {
      throw new ValidationException("Name is required.");
    }
    if (StringUtils.isEmpty(request.getEmail()))
    {
      throw new ValidationException("Email is required.");
    }
    if (request.getRelation() == null)
    {
      throw new ValidationException("Relation is required.");
    }
  }

  @Transactional
  public User sendStakeholderLink(Long stakeholderId)
  {
    Stakeholder stakeholder = stakeholderRepository.findOne(stakeholderId);
    if (stakeholder == null)
    {
      throw new ValidationException(String.format("Cannot locate Stakeholder[id = %s]", stakeholderId));
    }

    User user = stakeholder.getUser();
    if (!isThreeSixty(user))
    {
      throw new ValidationException("User is not 360 enabled.");
    }

    if (!isVerified(stakeholder))
    {
      throw new ValidationException("Stakeholder is not verified.");
    }
    stakeholder.setLoginValid(DateTime.now().plusHours(validHours).toDate());
    stakeholder.setStatus(StakeholderTestStatus.Sent);
    stakeholder.setLoginKey(generateStakeholderLoginKey());
    stakeholder.setLastSend(new Date());
    stakeholderRepository.saveAndFlush(stakeholder);
    //trigger test link email
    eventBus.post(new StakeholderEvent(stakeholder, EventType.StakeholderSentLinkEmail));
    return stakeholder.getUser();
  }

  public User sendStakeholderReminder(Long stakeholderId)
  {
    Stakeholder stakeholder = stakeholderRepository.findOne(stakeholderId);
    if (stakeholder == null)
    {
      throw new ValidationException(String.format("Cannot locate Stakeholder[id = %s]", stakeholderId));
    }

    User user = stakeholder.getUser();
    if (!isThreeSixty(user))
    {
      throw new ValidationException("User is not 360 enabled.");
    }

    if (!isVerified(stakeholder))
    {
      throw new ValidationException("Stakeholder is not verified.");
    }

    if (stakeholder.getStatus() == StakeholderTestStatus.Created)
    {
      return sendStakeholderLink(stakeholderId);
    }

    stakeholder.setLastRemind(new Date());
    stakeholderRepository.saveAndFlush(stakeholder);
//  trigger test reminder email
    eventBus.post(new StakeholderEvent(stakeholder, EventType.StakeholderRemindEmail));
    return user;
  }

  public void sendStakeholderVerification(Long stakeholderId)
  {
    Stakeholder stakeholder = stakeholderRepository.findOne(stakeholderId);
    if (stakeholder == null)
    {
      throw new ValidationException(String.format("Cannot locate Stakeholder[id = ]", stakeholderId));
    }
    stakeholder.setLastVerificationSend(new Date());
    stakeholderRepository.saveAndFlush(stakeholder);
    eventBus.post(new StakeholderEvent(stakeholder, EventType.StakeholderVerificationEmail));
  }

  public String getStakeholderToken(String loginKey)
  {
    Stakeholder stakeholder = stakeholderRepository.findByLoginKey(loginKey);
    if (stakeholder == null)
    {
      throw new ValidationException("Invalid Login key.");
    }

    User user = stakeholder.getUser();
    if (user == null)
    {
      throw new ValidationException("Cannot locate stakeholder's student.");
    }

    if (!isThreeSixty(user))
    {
      throw new ValidationException("Student is not threesixty enabled.");
    }

    Date loginValid = stakeholder.getLoginValid();
    if (loginValid == null)
    {
      throw new ValidationException("Stakeholder is not sent.");
    }

    if (new Date().after(loginValid))
    {
      throw new ValidationException("Login key expired.");
    }

    User stakeholderUser = stakeholder.getStakeholderUser();
    return Jwts.builder()
               .setSubject(stakeholderUser.getUsername())
               .setExpiration(stakeholder.getLoginValid())
               .claim("assets_url", intscale_assets)
               .claim("userType", stakeholderUser.getUserType())
               .setIssuedAt(new Date())
               .signWith(SignatureAlgorithm.HS512, secretkey)
               .compact();
  }

  public List<Stakeholder> saveStudentStakeholders(List<StakeholderRequest> request)
  {
    User user = getUser();
    if (CollectionUtils.isEmpty(request))
    {
      return getFreshUser().getStakeholders();
    }
    UserRequest userRequest = new UserRequest();
    userRequest.setId(user.getId());
    userRequest.setThreeSixtyEnabled(user.getThreeSixtyEnabled());
    userRequest.setStakeholders(request);
    return save(userRequest).getStakeholders();
  }

  public void verify(String loginKey)
  {
    Stakeholder stakeholder = stakeholderRepository.findByLoginKey(loginKey);
    if (stakeholder == null)
    {
      throw new ValidationException("Invalid Url or Already Verified.");
    }
    verify(stakeholder);
  }

  public void verify(Stakeholder stakeholder)
  {
    stakeholder.setVerified(true);
    stakeholderRepository.saveAndFlush(stakeholder);
    sendStakeholderLink(stakeholder.getId());
  }
}