package com.synlabs.intscale.service.threesixty;

import com.synlabs.intscale.entity.threesixty.Stakeholder;
import com.synlabs.intscale.entity.threesixty.StakeholderAnswer;
import com.synlabs.intscale.entity.threesixty.StakeholderQuestion;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.enums.StakeholderRelation;
import com.synlabs.intscale.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.PipedWriter;
import java.util.HashSet;
import java.util.Set;

@Service
public class AsyncService extends BaseService
{
  @Autowired
  private StakeholderTestService stakeholderTestService;

  @Async
  public void writeStakeholderExcelReport(Set<User> users, PipedWriter pw) throws IOException
  {
    StakeholderRelation[] relations = getAllStakeholderRelations();

    //adding csv header
    StringBuffer sb1 = new StringBuffer();
    sb1.append("School Name,Grade,Username,Gender,QuestionId,Question,Category,Subcategory");
    for (StakeholderRelation relation : relations)
    {
      sb1.append(String.format(",%s,%s Date", relation.toString(), relation.toString()));
    }
    pw.write(sb1.toString() + "\n");

    //writing csv data
    for (User user : users)
    {
//      user = getFreshUser(user.getId());
      Set<StakeholderQuestion> stakeholderQuestions = new HashSet<>();
      stakeholderTestService.getStakeholderTests(user).forEach(t -> {
        stakeholderQuestions.addAll(t.getStakeholderQuestions());
      });
      for (StakeholderQuestion question : stakeholderQuestions)
      {
        StringBuffer sb = new StringBuffer("");
        //user details appended to csv report
        sb.append(String.format("%s,%s,%s,%s,",
                                user.getSchoolName(),
                                user.getGrade(),
                                user.getUsername(),
                                user.getGender()));

        //question details appended to csv
        sb.append(String.format("%s,%s,%s,%s",
                                question.getId(),
                                question.getDescription(),
                                question.getCategory().getName(),
                                question.getSubCategory().getName()));
//        TODO : append question tags

        for (StakeholderRelation relation : relations)
        {
          Stakeholder stakeholder = user.getStakeholder(relation);
          if (stakeholder != null)
          {
            switch (stakeholder.getStatus())
            {
              case Finished:
                StakeholderAnswer answer = stakeholder.getAnswer(question.getId());
                if (answer == null)
                {
                  sb.append(",Not answered.,Not answered.");
                }
                else
                {
                  sb.append(",")
                    .append(answer.getMarksObtained())
                    .append(",")
                    .append(formatDate(stakeholder.getFinishedDate()));
                }
                break;
              case Created:
                sb.append(",Not Sent.,Not Sent.");
                break;
              case Sent:
                sb.append(",Not started.,Not started.");
                break;
              case InProgress:
                sb.append(",Test in progress,Test in progress.");
                break;
            }
          }
          else
          {
            sb.append(",Not Created.,Not Created.");
          }
        }
        pw.write(sb.toString() + "\n");
      }
    }
    pw.close();
  }
}
