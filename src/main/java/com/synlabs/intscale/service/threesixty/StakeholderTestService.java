package com.synlabs.intscale.service.threesixty;

import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.test.TestResult;
import com.synlabs.intscale.entity.threesixty.Stakeholder;
import com.synlabs.intscale.entity.threesixty.StakeholderAnswer;
import com.synlabs.intscale.entity.threesixty.StakeholderQuestion;
import com.synlabs.intscale.entity.threesixty.StakeholderQuestionOption;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.enums.StakeholderTestStatus;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.UserTestRepository;
import com.synlabs.intscale.jpa.threesixty.StakeholderAnswerRepository;
import com.synlabs.intscale.jpa.threesixty.StakeholderQuestionOptionRepository;
import com.synlabs.intscale.jpa.threesixty.StakeholderQuestionRepository;
import com.synlabs.intscale.jpa.threesixty.StakeholderRepository;
import com.synlabs.intscale.service.BaseService;
import com.synlabs.intscale.view.threesixty.StakeholderAnswerRequest;
import com.synlabs.intscale.view.threesixty.StakeholderConstructResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class StakeholderTestService extends BaseService
{
  @Autowired
  private StakeholderQuestionRepository questionRepository;

  @Autowired
  private StakeholderQuestionOptionRepository optionRepository;

  @Autowired
  private StakeholderAnswerRepository answerRepository;

  @Autowired
  private StakeholderRepository stakeholderRepository;

  public List<StakeholderQuestion> getQuestion(Long constructId)
  {
    User user = getFreshUser();
    if (!user.getUserType().equalsIgnoreCase("Stakeholder"))
    {
      throw new ValidationException("User is stakeholder.");
    }
    Stakeholder stakeholder = user.getStakeholder();
    if (stakeholder == null)
    {
      throw new ValidationException("Cannot find stakeholder for current user.");
    }
    /*if (stakeholder.getStatus() == StakeholderTestStatus.Finished)
    {
      throw new ValidationException("Stakeholder already completed test.");
    }*/

    Boolean threeSixty = stakeholder.getUser().getThreeSixtyEnabled();
    threeSixty = threeSixty == null ? false : threeSixty;
    if (!threeSixty)
    {
      throw new ValidationException("Student is not threesixty enabled.");
    }

    Set<Test> tests = stakeholder.getTests();
    if (CollectionUtils.isEmpty(tests))
    {
      stakeholder.getTests().addAll(getStakeholderTests(stakeholder.getUser()));
      stakeholderRepository.saveAndFlush(stakeholder);
    }
    List<Test> remainingTest=new ArrayList<>();
    for(Test test:tests){
      if(test.getParent()!=null&&test.getParent().getParent()!=null&&test.getParent().getParent().getId().equals(constructId)){
        remainingTest.add(test);
      }
    }
    return questionRepository.findByTenantAndTestIn(getTenant(), remainingTest);
  }

  public Set<Test> getStakeholderTests(User user)
  {
    return user.getTests().stream()
               .map(TestResult::getTest)
               .filter(BaseService::isThreeSixty)
               .collect(Collectors.toSet());
  }

  private void saveAnswer(StakeholderAnswerRequest request, Stakeholder stakeholder)
  {
    //2. getting question and option
    StakeholderQuestionOption option = optionRepository.findOne(request.getOptionId());
    if (option == null)
    {
      throw new ValidationException(String.format("Cannot locate option[id = %s]", request.getOptionId()));
    }
    StakeholderQuestion question = questionRepository.findOne(request.getQuestionId());
    if (question == null)
    {
      throw new ValidationException(String.format("Cannot locate Question[id=%s]", request.getQuestionId()));
    }

    //3. validating that selected option belong to its question
    if (!option.getQuestion().equals(question))
    {
      throw new ValidationException("Invalid option selected.");
    }

    //4. finding old saved answer
    StakeholderAnswer answer = answerRepository.findByTenantAndStakeholderAndQuestion(getTenant(), stakeholder, question);
    if (answer == null)
    {
      answer = new StakeholderAnswer();
      answer.setStakeholder(stakeholder);
      answer.setTest(question.getTest());
    }
    answer.setQuestion(question);
    answer.setQuestionDescription(question.getDescription());
    answer.setSelectedOption(option);
    answer.setAnsDescription(option.getDescription());
    answer.setMarksObtained(option.getMarks());
    answerRepository.saveAndFlush(answer);
  }

  public void saveAnswers(List<StakeholderAnswerRequest> requests)
  {
    //1. getting stakeholder from login user
    User user = getFreshUser();
    Stakeholder stakeholder = user.getStakeholder();
    if (stakeholder == null)
    {
      throw new ValidationException(String.format("Invalid User[username=%s]", user));
    }
    for (StakeholderAnswerRequest request : requests)
    {
      saveAnswer(request, stakeholder);
    }
    stakeholder.setStatus(StakeholderTestStatus.InProgress);
    stakeholderRepository.saveAndFlush(stakeholder);
    if(!getConstructs().isEmpty()){
      throw new ValidationException("InProcess");
    }
  }

  public void finishTest()
  {
    //1. getting stakeholder from login user
    User user = getFreshUser();
    Stakeholder stakeholder = user.getStakeholder();
    if (stakeholder == null)
    {
      throw new ValidationException(String.format("Invalid User[username=%s]", user));
    }
    stakeholder.setFinishedDate(new Date());
    stakeholder.setStatus(StakeholderTestStatus.Finished);
    stakeholderRepository.saveAndFlush(stakeholder);
  }

  public List<Test> getConstructs() {
    User user = getFreshUser();
    if (!user.getUserType().equalsIgnoreCase("Stakeholder"))
    {
      throw new ValidationException("User is stakeholder.");
    }
    Stakeholder stakeholder = user.getStakeholder();
    if (stakeholder == null)
    {
      throw new ValidationException("Cannot find stakeholder for current user.");
    }
    Boolean threeSixty = stakeholder.getUser().getThreeSixtyEnabled();
    threeSixty = threeSixty == null ? false : threeSixty;
    if (!threeSixty)
    {
      throw new ValidationException("Student is not threesixty enabled.");
    }
    Set<Test> tests = stakeholder.getTests();
    if (CollectionUtils.isEmpty(tests))
    {
      stakeholder.getTests().addAll(getStakeholderTests(stakeholder.getUser()));
      stakeholder=stakeholderRepository.saveAndFlush(stakeholder);
      tests = stakeholder.getTests();
    }
    List<StakeholderAnswer> stakeholderAnswers=answerRepository.findAllByStakeholderAndTenant(stakeholder,getTenant());
    for(StakeholderAnswer answer:stakeholderAnswers){
      tests.remove(answer.getTest());
    }
    List<Test> constructs=new ArrayList<>();
    tests.forEach(test -> {
      if(test.getParent()!=null&&test.getParent().getParent()!=null){
        constructs.add(test.getParent().getParent());
      }else if(test.getParent()!=null&&test.getParent().getParent()==null){
        constructs.add(test.getParent());
      }
    });
    return constructs;
  }
}
