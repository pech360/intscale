package com.synlabs.intscale.service.threesixty;

import com.querydsl.jpa.impl.JPAQuery;
import com.synlabs.intscale.entity.masterdata.MasterDataDocument;
import com.synlabs.intscale.entity.threesixty.QStakeholder;
import com.synlabs.intscale.entity.user.School;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.enums.StakeholderTestStatus;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.MasterDataDocumentRepository;
import com.synlabs.intscale.jpa.SchoolRepository;
import com.synlabs.intscale.service.BaseService;
import com.synlabs.intscale.view.threesixty.StakeholderReportRequest;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.io.Reader;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Service
public class StakeholderReportService extends BaseService
{
  @Autowired
  private EntityManager entityManager;

  @Autowired
  private SchoolRepository schoolRepository;

  @Autowired
  private MasterDataDocumentRepository masterDataDocumentRepository;

  @Autowired
  private AsyncService asyncService;

  public Reader generateReport(StakeholderReportRequest request) throws IOException
  {
    Set<User> users = getAllStakeholderUsers(request);
    PipedReader pr = new PipedReader();
    PipedWriter pw = new PipedWriter();
    pr.connect(pw);
    asyncService.writeStakeholderExcelReport(users, pw);
    return pr;
  }

  private Set<User> getAllStakeholderUsers(StakeholderReportRequest request)
  {
    //Fetching grade and school for filtering users
    if (request.getSchoolId() == null)
    {
      throw new ValidationException("Please select school.");
    }
    School school = schoolRepository.findOne(request.getSchoolId());
    if (request.getSchoolId() != null && school == null)
    {
      throw new ValidationException(String.format("Cannot locate School[id=%s]", request.getSchoolId()));
    }

    MasterDataDocument grade = masterDataDocumentRepository.findByIdAndType(request.getGradeId(), "Grade");
    if (request.getGradeId() != null && grade == null)
    {
      throw new ValidationException(String.format("Cannot locate Grade[id=%s]", request.getGradeId()));
    }

    //create query for fetching stakeholder users filtered by fromDate, toDate, Finished, schoolName, grade
    JPAQuery<User> query = new JPAQuery<>(entityManager);
    QStakeholder stakeholder = QStakeholder.stakeholder;
    query.select(stakeholder.user)
         .distinct()
         .from(stakeholder)
         .where(stakeholder.status.eq(StakeholderTestStatus.Finished))
         .where(stakeholder.tenant.eq(getTenant()));
    if (request.getFrom() != null)
    {
      query.where(stakeholder.finishedDate.after(request.getFrom()));
    }
    if (request.getTo() != null)
    {
      Date to = LocalDate.fromDateFields(request.getTo())
                         .plusDays(1)
                         .toDate();
      query.where(stakeholder.finishedDate.before(to));
    }

    if (school != null && grade != null)
    {
      query.innerJoin(stakeholder.user)
           .on(stakeholder.user.schoolName.eq(school.getName()))
           .on(stakeholder.user.grade.eq(grade.getValue()));
    }
    else if (school != null)
    {
      query.innerJoin(stakeholder.user)
           .on(stakeholder.user.schoolName.eq(school.getName()));
    }
    else if (grade != null)
    {
      query.innerJoin(stakeholder.user)
           .on(stakeholder.user.grade.eq(grade.getValue()));
    }

    query.innerJoin(stakeholder.user.stakeholders).fetchJoin();
//    query.innerJoin(stakeholder.user.tests).fetchJoin();

    return new HashSet<>(query.fetch());
  }

}
