package com.synlabs.intscale.service.threesixty;

import com.synlabs.intscale.service.BaseService;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.MappedSuperclass;
import java.math.BigDecimal;
import java.math.BigInteger;

@MappedSuperclass
public abstract class ImportService extends BaseService
{
  protected static final Logger logger = LoggerFactory.getLogger(ImportService.class);

  String getString(XSSFRow row, int rowNumber, int cellNumber)
  {
    XSSFCell cell = row.getCell(cellNumber);
    if (cell == null)
    {
      return "";
    }
    String result = "";
    switch (cell.getCellType())
    {
      case XSSFCell.CELL_TYPE_STRING:
        result = cell.getStringCellValue();
        break;
      case XSSFCell.CELL_TYPE_NUMERIC:
        result = cell.getRawValue();
        break;
    }
    return result;
  }

  BigDecimal getBigDecimal(XSSFRow row, int rowNumber, int cellNumber)
  {
    XSSFCell cell = row.getCell(cellNumber);
    if (cell == null)
    {
      return new BigDecimal(0);
    }
    BigDecimal bigDecimal = null;
    bigDecimal = getDecimalValue(cell);
    return bigDecimal;
  }

  Integer getInt(XSSFRow row, int rowNumber, int cellNumber)
  {
    XSSFCell cell = row.getCell(cellNumber);
    if (cell == null)
    {
      return 0;
    }
    Integer integer = null;
    integer = getIntValue(cell);
    int val = integer.intValue();
    return val;
  }

  BigDecimal getDecimalValue(XSSFCell cell) throws NumberFormatException
  {
    BigDecimal decimal = null;
    switch (cell.getCellType())
    {
      case XSSFCell.CELL_TYPE_STRING:
        decimal = new BigDecimal(cell.getStringCellValue());
        break;
      case XSSFCell.CELL_TYPE_NUMERIC:
        decimal = new BigDecimal(cell.getNumericCellValue());
        break;
    }
    return decimal == null ? new BigDecimal(0) : decimal;
  }

  Integer getIntValue(XSSFCell cell) throws NumberFormatException
  {
    BigInteger integer = null;
    switch (cell.getCellType())
    {
      case XSSFCell.CELL_TYPE_STRING:
        integer = new BigInteger(cell.getStringCellValue());
        break;
      case XSSFCell.CELL_TYPE_NUMERIC:
        integer = new BigInteger(cell.getRawValue());
        break;
    }
    return integer == null ? 0 : integer.intValue();
  }

}
