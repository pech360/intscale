package com.synlabs.intscale.service.threesixty;

import com.querydsl.jpa.impl.JPAQuery;
import com.synlabs.intscale.entity.Report.Category;
import com.synlabs.intscale.entity.test.QuestionTag;
import com.synlabs.intscale.entity.test.SubTag;
import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.threesixty.QStakeholderQuestion;
import com.synlabs.intscale.entity.threesixty.StakeholderQuestion;
import com.synlabs.intscale.entity.threesixty.StakeholderQuestionOption;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.CategoryRepository;
import com.synlabs.intscale.jpa.QuestionTagRepository;
import com.synlabs.intscale.jpa.SubTagRepository;
import com.synlabs.intscale.jpa.TestRepository;
import com.synlabs.intscale.jpa.threesixty.StakeholderQuestionOptionRepository;
import com.synlabs.intscale.jpa.threesixty.StakeholderQuestionRepository;
import com.synlabs.intscale.view.threesixty.StakeholderQuestionFilterRequest;
import com.synlabs.intscale.view.threesixty.StakeholderQuestionOptionRequest;
import com.synlabs.intscale.view.threesixty.StakeholderQuestionRequest;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class StakeholderQuestionService extends ImportService
{
  @Autowired
  private EntityManager entityManager;

  @Autowired
  private TestRepository testRepository;

  @Autowired
  private StakeholderQuestionRepository questionRepository;

  @Autowired
  private StakeholderQuestionOptionRepository optionRepository;

  @Autowired
  private SubTagRepository subTagRepository;

  @Autowired
  private CategoryRepository categoryRepository;

  @Autowired
  private QuestionTagRepository tagRepository;

  public List<StakeholderQuestion> filter(StakeholderQuestionFilterRequest request)
  {
    JPAQuery<StakeholderQuestion> query = new JPAQuery<>(entityManager);
    QStakeholderQuestion question = QStakeholderQuestion.stakeholderQuestion;

    query.select(question).from(question)
         .where(question.tenant.eq(getTenant()))
         .where(question.test.latestVersion.isTrue());

    return query.fetch();
  }

  private List<SubTag> getSubTags(List<Long> ids)
  {
    List<SubTag> subTags = new ArrayList<>();
    if (!CollectionUtils.isEmpty(ids))
    {
      ids.forEach(stid -> {
        SubTag subtag = subTagRepository.findOne(stid);
        if (subtag == null)
        {
          throw new ValidationException(String.format("Cannot locate SubTag[id=%s]", stid));
        }
        subTags.add(subtag);
      });
    }
    return subTags;
  }

  public StakeholderQuestion save(StakeholderQuestionRequest request)
  {
    validateQuestion(request);

    Test test = testRepository.findOne(request.getTestId());
    if (test == null)
    {
      throw new ValidationException(String.format("Cannot locate Test[id = %s]", request.getTestId()));
    }
    if (test.getThreeSixtyEnabled() == null)
    {
      test.setThreeSixtyEnabled(true);
      testRepository.saveAndFlush(test);
    }

    Category subCategory = categoryRepository.findOne(request.getSubCategoryId());
    if (subCategory == null)
    {
      throw new ValidationException(String.format("Cannot locate Subcategory[id=%s]", request.getSubCategoryId()));
    }
    Category category = subCategory.getParent();
    if (category == null)
    {
      throw new ValidationException(String.format("Invalid Subcategory [id=%s]", request.getSubCategoryId()));
    }

    List<SubTag> subTags = getSubTags(request.getSubTagIds());

    StakeholderQuestion question = request.toEntity(null);
    question.setTest(test);
    question.setSubTags(subTags);
    List<StakeholderQuestionOption> options = question.getOptions();
    question.setOptions(null);
    question.setSubCategory(subCategory);
    question.setCategory(category);
    questionRepository.saveAndFlush(question);
    question.setOptions(options);
    saveQuestionOptions(question);
    return question;
  }

  public StakeholderQuestion createCopy(StakeholderQuestion question, Test test)
  {
    StakeholderQuestionRequest questionRequest = new StakeholderQuestionRequest(question);
    questionRequest.setTestId(test.getId());
    return save(questionRequest);
  }

  @Transactional
  public StakeholderQuestion update(StakeholderQuestionRequest request)
  {
    StakeholderQuestion question = questionRepository.findOne(request.getId());
    if (question == null)
    {
      throw new ValidationException(String.format("Cannot locate StakeholderQuestion[id=%s]", request.getId()));
    }

    validateQuestion(request);

    Test test = testRepository.findOne(request.getTestId());
    if (test == null)
    {
      throw new ValidationException(String.format("Cannot locate Test[id = %s]", request.getTestId()));
    }

    Category subCategory = categoryRepository.findOne(request.getSubCategoryId());
    if (subCategory == null)
    {
      throw new ValidationException(String.format("Cannot locate sub category[id=%s]", request.getSubCategoryId()));
    }
    Category category = subCategory.getParent();
    if (category == null)
    {
      throw new ValidationException(String.format("Invalid Subcategory [id=%s]", request.getSubCategoryId()));
    }

    List<SubTag> subTags = getSubTags(request.getSubTagIds());
    List<StakeholderQuestionOption> options = question.getOptions();
    question.setOptions(new ArrayList<>());
    optionRepository.delete(options);

    request.toEntity(question);
    question.setTest(test);
    question.setSubTags(subTags);
    options = question.getOptions();
    question.setOptions(null);
    question.setSubCategory(subCategory);
    question.setCategory(category);
    questionRepository.saveAndFlush(question);
    question.setOptions(options);
    saveQuestionOptions(question);
    return question;
  }

  private void saveQuestionOptions(StakeholderQuestion question)
  {
    List<StakeholderQuestionOption> options = question.getOptions();
    options.forEach(o -> {
      o.setQuestion(question);
    });
    optionRepository.save(options);
  }

  private void validateQuestion(StakeholderQuestionRequest request)
  {
    if (request.getTestId() == null)
    {
      throw new ValidationException("Please select test.");
    }

    if (StringUtils.isEmpty(request.getDescription()))
    {
      throw new ValidationException("Description Is Required.");
    }

    if (request.getSubCategoryId() == null)
    {
      throw new ValidationException("Sub Category id required.");
    }

    if (!CollectionUtils.isEmpty(request.getOptions()))
    {
      request.getOptions().forEach(o -> {
        if (StringUtils.isEmpty(o.getDescription()))
        {
          throw new ValidationException("Description is required.");
        }
        if (StringUtils.isEmpty(o.getMarks()))
        {
          throw new ValidationException("Marks is required.");
        }
      });
    }
  }

  public List<StakeholderQuestion> uploadQuestions(Long testId, MultipartFile file) throws IOException
  {
    if (testId == null)
    {
      throw new ValidationException("TestId is required.");
    }

    Test test = testRepository.findOne(testId);
    if (test == null)
    {
      throw new ValidationException(String.format("Cannot locate Test[id=%s]", testId));
    }
    XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
    XSSFSheet questionSheet = workbook.getSheet("questions");
    XSSFSheet optionSheet = workbook.getSheet("options");
    if (questionSheet == null)
    {
      throw new ValidationException("Cannot locate questions sheet in file.");
    }
    if (optionSheet == null)
    {
      throw new ValidationException("Cannot locate options sheet in file.");
    }
    List<StakeholderQuestionOptionRequest> optionRequests = importOptions(optionSheet);
    if (CollectionUtils.isEmpty(optionRequests))
    {
      throw new ValidationException("Options sheet is empty.");
    }

    List<StakeholderQuestionRequest> questionRequests = importQuestion(questionSheet);
    if (CollectionUtils.isEmpty(questionRequests))
    {
      throw new ValidationException("Question sheet is empty.");
    }
    questionRequests.forEach(q -> {
      q.setOptions(optionRequests);
      q.setTestId(test.getId());
    });

    List<StakeholderQuestion> questions = new ArrayList<>();
    questionRequests.forEach(q -> {
      questions.add(save(q));
    });

    return questions;
  }

  private List<StakeholderQuestionRequest> importQuestion(XSSFSheet questionSheet)
  {
    List<StakeholderQuestionRequest> requests = new ArrayList<>();
    int row = 1;
    int column = 0;
    XSSFRow xssfRow = questionSheet.getRow(row);
    while (xssfRow != null)
    {
      column = 0;
      String description = getString(xssfRow, row + 1, column).trim();
      column = 1;
      String categoryName = getString(xssfRow, row + 1, column).trim();
      column = 2;
      String subCategoryName = getString(xssfRow, row + 1, column).trim();
      column = 3;
      String tagName = getString(xssfRow, row + 1, column).trim();
      column = 4;
      String subTagName = getString(xssfRow, row + 1, column).trim();
      if (StringUtils.isEmpty(categoryName))
      {
        throw new ValidationException(String.format("category name is required in row = %s", row));
      }
      if (StringUtils.isEmpty(subCategoryName))
      {
        throw new ValidationException(String.format("sub category name is required in row = %s", row));
      }
      if (StringUtils.isEmpty(tagName))
      {
        throw new ValidationException(String.format("tag name is required in row = %s", row));
      }
      if (StringUtils.isEmpty(tagName))
      {
        throw new ValidationException(String.format("sub tag name is required in row = %s", row));
      }

      //saving subcategory
      Category category = categoryRepository.findByNameAndTenant(categoryName, getTenant());
      Category subCategory = null;
      if (category == null)
      {
        category = new Category();
        category.setName(categoryName);
        categoryRepository.saveAndFlush(category);
        subCategory = new Category();
        subCategory.setName(subCategoryName);
        subCategory.setParent(category);
        categoryRepository.saveAndFlush(subCategory);
      }
      subCategory = categoryRepository.findByNameAndTenantAndParent(subCategoryName, getTenant(), category);
      if (subCategory == null)
      {
        subCategory = new Category();
        subCategory.setName(subCategoryName);
        subCategory.setParent(category);
        categoryRepository.saveAndFlush(subCategory);
      }

      //saving tags and subtags
      QuestionTag tag = tagRepository.findByNameAndTenant(tagName, getTenant());
      SubTag subTag = null;
      if (tag == null)
      {
        tag = new QuestionTag();
        tag.setName(tagName);
        tagRepository.saveAndFlush(tag);

        subTag = new SubTag();
        subTag.setTag(tag);
        subTag.setName(subTagName);
        subTagRepository.saveAndFlush(subTag);
      }
      subTag = subTagRepository.findByNameAndTagAndTenant(subTagName, tag, getTenant());
      if (subTag == null)
      {
        subTag = new SubTag();
        subTag.setTag(tag);
        subTag.setName(subTagName);
        subTagRepository.saveAndFlush(subTag);
      }

      StakeholderQuestionRequest question = new StakeholderQuestionRequest();
      question.setDescription(description);
      question.setSubCategoryId(subCategory.getId());
      question.getSubTagIds().add(subTag.getId());
      requests.add(question);
      row++;
      xssfRow = questionSheet.getRow(row);
    }
    return requests;
  }

  private List<StakeholderQuestionOptionRequest> importOptions(XSSFSheet optionSheet)
  {
    List<StakeholderQuestionOptionRequest> requests = new ArrayList<>();
    int row = 1;
    int column = 0;
    XSSFRow xssfRow = optionSheet.getRow(row);
    while (xssfRow != null)
    {
      column = 0;
      String description = getString(xssfRow, row + 1, column);
      column = 1;
      Integer marks = getInt(xssfRow, row + 1, column);
      StakeholderQuestionOptionRequest option = new StakeholderQuestionOptionRequest();
      option.setDescription(description);
      option.setMarks(marks);
      requests.add(option);
      row++;
      xssfRow = optionSheet.getRow(row);
    }
    return requests;
  }
}
