package com.synlabs.intscale.service;

import com.synlabs.intscale.entity.Report.Category;
import com.synlabs.intscale.entity.Report.Norm;
import com.synlabs.intscale.entity.Report.ScoreRange;
import com.synlabs.intscale.entity.test.Feedback;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.CategoryRepository;
import com.synlabs.intscale.jpa.FeedbackRepository;
import com.synlabs.intscale.jpa.NormRepository;
import com.synlabs.intscale.jpa.ScoreRangeRepository;
import com.synlabs.intscale.view.report.CategoryRequest;
import com.synlabs.intscale.view.report.CategoryResponse;
import com.synlabs.intscale.view.FeedbackRequest;
import com.synlabs.intscale.view.report.ScoreRangeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by itrs on 7/25/2017.
 */
@Service
public class CategoryService extends BaseService {
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private FeedbackRepository feedbackRepository;
    @Autowired
    private ScoreRangeRepository scoreRangeRepository;
    @Autowired
    private NormRepository normRepository;

    public Category saveCategory(CategoryRequest request) {
        Category category = request.toEntity(new Category());
        if (!StringUtils.isEmpty(request.getParentId())) {
            category.setParent(categoryRepository.findOne(request.getParentId()).getName().equals(request.getName()) ? null : categoryRepository.findOne(request.getParentId()));
        }
        return categoryRepository.save(category);
    }

    public List<Category> getCategory() {
        return categoryRepository.findAllByTenant(getTenant());
    }

    public Category findOneById(Long categoryId) {
        return categoryRepository.findOne(categoryId);
    }

    public void deleteCategory(Category category) {
        categoryRepository.delete(category);
    }

    public Category updateCategory(CategoryRequest request) {

        Category category = request.toEntity(categoryRepository.findOne(request.getId()));
        if (StringUtils.isEmpty(request.getParentId()) || request.getParentId().equals(" ") || categoryRepository.findOne(request.getId()).getParent() == null) {
            category.setParent(null);
        } else {
            category.setParent(categoryRepository.findOne(request.getParentId()).getName().equals(request.getName()) ? null : categoryRepository.findOne(request.getParentId()));
        }
        return categoryRepository.save(category);

    }

    public Feedback saveFeedback(FeedbackRequest request) {
        Feedback feedback = null;
        if (request.getId() == null) {
            List<Feedback> feedbacks = new ArrayList<>();
            Category category = categoryRepository.findOne(request.getCategoryId());
            if (category.getFeedbacks().size() > 0) {
                feedbacks = category.getFeedbacks();
            }
            feedback = request.toEntity();
            feedback.setCategory(category);
            feedbacks.add(feedbackRepository.save(feedback));
            category.setFeedbacks(feedbacks);
            categoryRepository.save(category);
            return feedback;
        } else {
            Category category = categoryRepository.findOne(request.getCategoryId());
            List<Feedback> feedbacks = category.getFeedbacks();
            for (Feedback f : feedbacks) {
                if (request.getId().equals(f.getId())) {
                    f.setMarksMin(request.getMarksMin());
                    f.setMarksMax(request.getMarksMax());
                    f.setExpertAnaylsis(request.getExpertAnaylsis());
                    f.setDevelopementPlan(request.getDevelopementPlan());
                    f.setCategory(category);
                    feedback = feedbackRepository.save(f);
                }
            }
            return feedback;
        }
    }

    public void deleteFeedback(Long categoryId, Long id) {
        Category category = categoryRepository.findOne(categoryId);
        category.getFeedbacks().forEach(f -> {
            if (id.equals(f.getId())) {
                feedbackRepository.delete(f);
            }
        });
    }

    public List<Category> getParentCategory() {
        List<Category> categories = new ArrayList<>();
        for (Category c : categoryRepository.findAllByActiveAndTenant(true, getTenant())) {
            if (StringUtils.isEmpty(c.getParent())) {
                categories.add(c);
            }
        }
        return categories;
    }

    public List<Category> getChildCategory() {
        List<Category> categories = new ArrayList<>();
        for (Category c : categoryRepository.findAllByActiveAndTenant(true, getTenant())) {
            if (!StringUtils.isEmpty(c.getParent())) {
                categories.add(c);
            }
        }
        return categories;
    }

    public List<Category> getChildCategoryByParent(Long id) {
        List<Category> categories = new ArrayList<>();
        if (id != null) {
            return categoryRepository.findAllByParentIdAndActiveAndTenant(id, true, getTenant());
        }
        return categories;
    }

    public List<Category> getChildCategoryByParentName(String name) {
        return categoryRepository.findAllByParentNameAndActiveAndTenant(name, true, getTenant());
    }

    public void deleteCategories(List<Long> list) {
        list.forEach(id -> {
            Category category = categoryRepository.findOne(id);
            if (category.getParent() == null) {
                List<Category> subCategory = categoryRepository.findAllByParentIdAndActiveAndTenant(id, true, getTenant());
                subCategory.forEach(subCat -> {
                    subCat.setActive(false);
                    subCat.setParent(null);
                    list.remove(subCat.getId());
                    categoryRepository.saveAndFlush(subCat);
                });
                category.setActive(false);
                categoryRepository.saveAndFlush(category);
            } else {
                category.setActive(false);
                category.setParent(null);
                categoryRepository.saveAndFlush(category);
            }
        });

    }

    public List<Category> getCategoryByTest(Long id) {
        return categoryRepository.findAllByTestIdAndTenant(id, getTenant().getId());
    }

    public List<Category> getSubCategoryByTest(Long id) {
        return categoryRepository.findAllSubCategoryByTestIdAndTenant(id, getTenant().getId());
    }

    public CategoryResponse saveCategoryScoreRanges(List<ScoreRangeRequest> request, Long categoryId, String applicableFor) {

        if (StringUtils.isEmpty(applicableFor)) {
            throw new ValidationException("for whom this is applicable, Junior or Senior ?");
        }
        Category category = categoryRepository.getOne(categoryId);
        List<ScoreRange> scoreRanges = new ArrayList<>(request.size());
        request.forEach(r -> {

            scoreRanges.add(r.toEntity(new ScoreRange()));
        });
        scoreRangeRepository.save(scoreRanges);
        if (applicableFor.equalsIgnoreCase("senior")) {
            category.getSeniorScoreRanges().clear();
            category.setSeniorScoreRanges(scoreRanges);
        } else {
            category.getJuniorScoreRanges().clear();
            category.setJuniorScoreRanges(scoreRanges);
        }
        return new CategoryResponse(categoryRepository.saveAndFlush(category));
    }

    public CategoryResponse saveCategoryStandardScore(CategoryRequest categoryRequest, String language) {
        if(StringUtils.isEmpty(language)){
            throw new ValidationException("please choose a language");
        }
        Category category = categoryRepository.findOne(categoryRequest.getId());
        Norm norm = normRepository.findByCategoryIdAndLanguageAndTenant(category.getId(), language, getTenant());
        if(norm==null){
            norm = new Norm();
        }
        norm.setLanguage(language);
        norm.setJuniorMeanScore(categoryRequest.getJuniorMeanScore());
        norm.setJuniorStandardDeviation(categoryRequest.getJuniorStandardDeviation());
        norm.setSeniorMeanScore(categoryRequest.getSeniorMeanScore());
        norm.setSeniorStandardDeviation(categoryRequest.getSeniorStandardDeviation());
        norm.setSecondaryMeanScore(categoryRequest.getSecondaryMeanScore());
        norm.setSecondaryStandardDeviation(categoryRequest.getSecondaryStandardDeviation());

        norm.setCategory(category);
        normRepository.saveAndFlush(norm);

        category.setSequence(categoryRequest.getSequence());
        category = categoryRepository.save(category);
        return new CategoryResponse(category);
    }

    public List<CategoryResponse> mergeCategoriesandSubCategories(List<Category> categories, List<Category> subCategories) {
        List<CategoryResponse> categoryResponses = new ArrayList<>(categories.size());
        categories.forEach((Category category) -> {
            CategoryResponse categoryResponse = new CategoryResponse(category);
            subCategories.forEach((Category subCategory) -> {
                CategoryResponse subCategoryResponse = new CategoryResponse(subCategory);
                if (categoryResponse.getId().equals(subCategoryResponse.getParentId())) {
                    categoryResponse.getSubCategories().add(subCategoryResponse);
                }
            });
            categoryResponses.add(categoryResponse);
        });
        return categoryResponses;
    }

//  public List<Long> findSubCategoriesIdByCategoryId(Long CategoryId, List<CategoryResponse> data)
//  {
//    List<Long> subCategoriesIds = new ArrayList<>();
//    data.forEach((CategoryResponse c) -> {
//      if (c.getId().equals(CategoryId))
//      {
//        c.getSubCategories().forEach(s -> {
//          subCategoriesIds.add(s.getId());
//        });
//      }
//    });
//    return subCategoriesIds;
//  }

}
