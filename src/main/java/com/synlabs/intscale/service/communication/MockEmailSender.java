package com.synlabs.intscale.service.communication;

import com.synlabs.intscale.config.Local;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.File;
import java.util.List;

@Local
@Service
public class MockEmailSender extends AbstractEmailSender
{
  private static final Logger logger = LoggerFactory.getLogger(MockEmailSender.class);

  @Override
  protected Boolean sendEmail(String subject, String message, boolean isHtmlContent, List<File> attachments, String[] to, String[] cc, String[] bcc) throws MessagingException
  {
    logger.info("=======================================================");
    logger.info("TO: {} CC: {} BCC: {}", to, cc, bcc);
    logger.info("=======================================================");
    logger.info("TITLE {}", subject);
    logger.info("=======================================================");
    logger.info("BODY: {}", message);
    logger.info("=======================================================");
    if (attachments != null)
    {
      logger.info(String.format("attachments [%s]", attachments.size()));
      logger.info("=======================================================");
    }
    return true;
  }
}
