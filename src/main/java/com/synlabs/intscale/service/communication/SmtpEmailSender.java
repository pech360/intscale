package com.synlabs.intscale.service.communication;

import com.synlabs.intscale.config.Dev;
import com.synlabs.intscale.config.Local;
import com.synlabs.intscale.config.Prod;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.List;

//@Local
@Profile(value="default")
@Prod
@Dev
@Service
public class SmtpEmailSender extends AbstractEmailSender
{
  private static final Logger logger = LoggerFactory.getLogger(SmtpEmailSender.class);

  @Autowired
  private MailSender mailSender;

  @Value("${spring.mail.username}")
  private String from;

  @Value("${intscale.testenv}")
  private boolean testenv;

  @Value("${intscale.testemail}")
  private String testemail;

  @Override
  protected Boolean sendEmail(String subject, String message, boolean isHtmlContent, List<File> attachments, String[] to, String[] cc, String[] bcc) throws MessagingException
  {
    MimeMessage msg = ((JavaMailSender) mailSender).createMimeMessage();
    msg.setSubject(subject);
    msg.setFrom(from);
    MimeMessageHelper helper = (CollectionUtils.isEmpty(attachments))
                               ? new MimeMessageHelper(msg)
                               : new MimeMessageHelper(msg, true);


    if (attachments != null && !attachments.isEmpty())
    {
      for (File file : attachments)
      {
        helper.addAttachment(file.getName(), file);
      }
    }
    helper.setText(message, isHtmlContent);

    if (testenv)
    {
      logger.warn("Sending emails Subject[{}] from test env, replacing actual emails to...{}", subject, testemail);
      helper.setTo(testemail);
    }
    else
    {
      logger.warn("Sending email Subject[{}] to...{}", subject, to);
      if (to != null && to.length > 0)
      {
        helper.setTo(to);
      }
      if (cc != null && cc.length > 0)
      {
        helper.setCc(cc);
      }
      if (bcc != null && bcc.length > 0)
      {
        helper.setBcc(bcc);
      }
    }
    ((JavaMailSender) mailSender).send(msg);
    return true;
  }

}
