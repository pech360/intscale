package com.synlabs.intscale.service.communication;

import com.synlabs.intscale.common.EmailMessage;
import com.synlabs.intscale.entity.Timeline;
import com.synlabs.intscale.entity.threesixty.Stakeholder;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.util.StringUtil;
import com.synlabs.intscale.enums.NotificationType;
import com.synlabs.intscale.service.threesixty.NotificationEmailService;
import com.synlabs.intscale.view.threesixty.email.TestLinkResponse;
import com.synlabs.intscale.view.threesixty.email.VerificationEmailResponse;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.util.StringUtils;

import javax.mail.MessagingException;
import javax.persistence.MappedSuperclass;
import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@MappedSuperclass
public abstract class AbstractEmailSender {
    private static final Logger logger = LoggerFactory.getLogger(AbstractEmailSender.class);

    @Autowired
    protected Configuration configuration;

    @Value("${intscale.threesixty.baseUrl}")
    private String stakeholderBaseUrl;

    abstract protected Boolean sendEmail(String subject, String message, boolean isHtmlContent, List<File> attachments, String[] to, String[] cc, String[] bcc)
            throws MessagingException;

    @Autowired
    private NotificationEmailService notificationEmailService;

    @Async
    protected Boolean userWelcomeEmail(User user, String password, String website) {
        try {
            Template template = configuration.getTemplate("email_user_welcome.ftl");
            Map<String, Object> rootMap = new HashMap<>();
            rootMap.put("name", user.getName());
            rootMap.put("email", user.getEmail());
            rootMap.put("username", user.getUsername());
            rootMap.put("password", password);
            rootMap.put("website", website);
            Writer out = new StringWriter();
            template.process(rootMap, out);
            String message = out.toString();
            String subject = "AIM2EXCEL: Successful Registration";
            sendEmail(subject, message, false, null, new String[]{user.getEmail()}, null, null);
            return true;
        } catch (Exception ex) {
            logger.error("Error sending message!", ex);
            return false;
        }
    }

    @Async
    protected Boolean sendResetPassword(String token, String email, String name) {
        try {
            Template template = configuration.getTemplate("reset_password.ftl");
            Map<String, Object> rootMap = new HashMap<>();
            rootMap.put("name", name);
            rootMap.put("token", token);
            Writer out = new StringWriter();
            template.process(rootMap, out);
            String message = out.toString();
            String subject = "AIM2EXCEL: Forgot Password";
            sendEmail(subject, message, false, null, new String[]{email}, null, null);
            return true;
        } catch (Exception ex) {
            logger.error("Error sending message!", ex);
            return false;
        }
    }

    Boolean sendStakeholderTestLinkEmail(Stakeholder stakeholder) {
        try {
            Template template = configuration.getTemplate("stakeholder_link_email.ftl");
            Map<String, Object> rootMap = new HashMap<>();
            rootMap.put("response", new TestLinkResponse(stakeholder,
                    generate360BaseUrl(stakeholder.getTenant())));

            Writer out = new StringWriter();
            template.process(rootMap, out);
            String message = out.toString();
            String subject = "Intscale: 360° feedback session.";
            sendEmail(subject, message, true, null, new String[]{stakeholder.getEmail()}, null,
                    notificationEmailService.getNotificationEmails(NotificationType.STAKEHOLDER_SEND));
            return true;
        } catch (Exception ex) {
            logger.error("Error sending message!", ex);
            return false;
        }
    }

    Boolean sendStakeholderTestRemindEmail(Stakeholder stakeholder) {
        try {
            Template template = configuration.getTemplate("stakeholder_remind_email.ftl");
            Map<String, Object> rootMap = new HashMap<>();
            rootMap.put("response", new TestLinkResponse(stakeholder,
                    generate360BaseUrl(stakeholder.getTenant())));

            Writer out = new StringWriter();
            template.process(rootMap, out);
            String message = out.toString();
            String subject = "Intscale: Reminder for 360° feedback session.";
            sendEmail(subject, message, true, null, new String[]{stakeholder.getEmail()}, null,
                    notificationEmailService.getNotificationEmails(NotificationType.STAKEHOLDER_REMIND));
            return true;
        } catch (Exception ex) {
            logger.error("Error sending message!", ex);
            return false;
        }
    }

    private String generate360BaseUrl(Tenant tenant) {
        return String.format(stakeholderBaseUrl, tenant.getSubDomain());
    }

    public Boolean sendStakeholderVerification(Stakeholder stakeholder) {
        try {
            Template template = configuration.getTemplate("stakeholder_verification_email.ftl");
            Map<String, Object> rootMap = new HashMap<>();
            rootMap.put("response", new VerificationEmailResponse(stakeholder,
                    generate360BaseUrl(stakeholder.getTenant())));
            Writer out = new StringWriter();
            template.process(rootMap, out);
            String message = out.toString();
            String subject = "Intscale: Verification for 360° feedback.";
            sendEmail(subject, message, true, null, new String[]{stakeholder.getEmail()}, null,
                    notificationEmailService.getNotificationEmails(NotificationType.STAKEHOLDER_VERIFICATION));
            return true;
        } catch (Exception ex) {
            logger.error("Error sending message!", ex);
            return false;
        }
    }

    @Async
    protected Boolean sharePostOnEmail(String email, Timeline timeline, String assets_url, String body) {
        try {
            Template template = configuration.getTemplate("sharePostOnEmail.ftl");
            Map<String, Object> rootMap = new HashMap<>();

            rootMap.put("username", timeline.getUser().getName());
            rootMap.put("heading", StringUtils.isEmpty(timeline.getHeading()) ? "" : timeline.getHeading());
            rootMap.put("description", StringUtils.isEmpty(timeline.getDescription()) ? "" : timeline.getDescription());
            rootMap.put("body", StringUtils.isEmpty(body) ? "" : body);
            rootMap.put("imageUrl", StringUtils.isEmpty(timeline.getImageName()) ? "" : assets_url + timeline.getImageName());


            if (!StringUtils.isEmpty(timeline.getImageName())) {
                rootMap.put("imageUrl", assets_url + timeline.getImageName());
            } else {
                rootMap.put("imageUrl", "");
            }
            Writer out = new StringWriter();
            template.process(rootMap, out);
            String message = out.toString();
            String subject = String.format("AIM2EXCEL: %s has shared something with you", timeline.getUser().getName());
            sendEmail(subject, message, false, null, new String[]{email}, null, null);
            return true;
        } catch (Exception ex) {
            logger.error("Error sending message!", ex);
        }
        return false;
    }

    @Async
    public Boolean sendNotificationUponCompletionOfTests(User user) {

        try {
            Template template = configuration.getTemplate("notification_upon_test_completion.ftl");
            Map<String, Object> rootMap = new HashMap<>();
            rootMap.put("name", user.getName());
            rootMap.put("email", user.getEmail());
            rootMap.put("username", user.getUsername());
            Writer out = new StringWriter();
            template.process(rootMap, out);
            String message = out.toString();
            String subject = "Aim2excel:Completion of Assessment";
            sendEmail(subject, message, true, null, new String[]{user.getEmail()}, null,
                    null);
            return true;
        } catch (Exception ex) {
            logger.error("Error sending message!", ex);
            return false;
        }
    }

    @Async
    public Boolean sendReportToTestTaker(User user, List<File> files) {
        try {
            Template template = configuration.getTemplate("send_report_to_testTaker.ftl");
            Map<String, Object> rootMap = new HashMap<>();
            rootMap.put("name", user.getName());
            rootMap.put("username", user.getUsername());
            rootMap.put("email", user.getEmail());
            Writer out = new StringWriter();
            template.process(rootMap, out);
            String message = out.toString();
            String subject = "Aim2excel:Completion of Assessment";
            sendEmail(subject, message, true, files, new String[]{user.getEmail()}, null,
                    null);
            return true;
        } catch (Exception ex) {
            logger.error("Error sending message!", ex);
            return false;
        }

    }

    @Async
    public Boolean shareReport(User user, String email, String cc, List<File> files) {
        try {
            Template template = configuration.getTemplate("share_report.ftl");
            Map<String, Object> rootMap = new HashMap<>();
            rootMap.put("name", user.getName());
            Writer out = new StringWriter();
            template.process(rootMap, out);
            String message = out.toString();
            StringBuilder subject = new StringBuilder("Aim2excel:Blueprint");
            if (!StringUtils.isEmpty(user.getSchoolName())) {
                subject.append(" | ").append(user.getSchoolName());
            }
            if (!StringUtils.isEmpty(user.getGrade())) {
                subject.append(" | ").append(user.getGrade());
            }

            String[] bccEmails = StringUtils.isEmpty(cc) ? null  : new String[]{cc};
            String[] toEmails = StringUtils.isEmpty(email) ? null  : new String[]{email};

            sendEmail(subject.toString(), message, true, files, toEmails, null , bccEmails);
            return true;
        } catch (Exception ex) {
            logger.error("Error sending message!", ex);
            return false;
        }
    }

    @Async
    public void testUploaded(String[] emails, Integer count) {

        try {
            Template template = configuration.getTemplate("test_Uploaded.ftl");
            Map<String, Object> rootMap = new HashMap<>();
            if (count != null) {
                rootMap.put("count", count);
            } else {
                rootMap.put("count", "all");
            }
            Writer out = new StringWriter();
            template.process(rootMap, out);
            String message = out.toString();
            String subject = "Aim2excel:User-Test Responses Uploaded";
            sendEmail(subject, message, true, null, emails, null,
                    null);

        } catch (Exception ex) {
            logger.error("Error sending message!", ex);
        }
    }


    @Async
    protected Boolean testSignUpMail(User user, String password, String website) {
        try {
            Template template = configuration.getTemplate("demo_test_sign_up.ftl");
            Map<String, Object> rootMap = new HashMap<>();
            rootMap.put("name", user.getName());
            rootMap.put("username", user.getUsername());
            rootMap.put("email", user.getEmail());
            rootMap.put("password", password);
            rootMap.put("website", website);
            Writer out = new StringWriter();
            template.process(rootMap, out);
            String message = out.toString();
            String subject = "AIM2EXCEL: The Journey Begins!";
            sendEmail(subject, message, false, null, new String[]{user.getEmail()}, null, null);
            return true;
        } catch (Exception ex) {
            logger.error("Error sending message!", ex);
            return false;
        }
    }


    @Async
    protected Boolean buyProduct(User user, String password, String name) {
        try {
            Template template = configuration.getTemplate("buy_product.ftl");
            Map<String, Object> rootMap = new HashMap<>();
            rootMap.put("name", name);
            rootMap.put("username", user.getUsername());
            rootMap.put("email", user.getEmail());
            rootMap.put("password", password);
            Writer out = new StringWriter();
            template.process(rootMap, out);
            String message = out.toString();
            String subject = "AIM2EXCEL: Welcome Aboard!";
            sendEmail(subject, message, false, null, new String[]{user.getEmail()}, null, null);
            return true;
        } catch (Exception ex) {
            logger.error("Error sending message!", ex);
            return false;
        }
    }

    @Async
    protected Boolean demoCompleteMail(User user, List<File> files) {
        try {
            Template template = configuration.getTemplate("demo_complete_mail.ftl");
            Map<String, Object> rootMap = new HashMap<>();
            rootMap.put("name", user.getName());
            Writer out = new StringWriter();
            template.process(rootMap, out);
            String message = out.toString();
            String subject = "AIM2EXCEL: Completion of Assessment";
            sendEmail(subject, message, false, files, new String[]{user.getEmail()}, null, null);
            return true;
        } catch (Exception ex) {
            logger.error("Error sending message!", ex);
            return false;
        }
    }

    @Async
    public Boolean send(User user, String file) {
        try {
            Template template = configuration.getTemplate("send.ftl");
            Map<String, Object> rootMap = new HashMap<>();
            rootMap.put("name", user.getName());
            rootMap.put("username", user.getUsername());
            rootMap.put("email", user.getEmail());
            rootMap.put("file", file);
            Writer out = new StringWriter();
            template.process(rootMap, out);
            String message = out.toString();
            String subject = "AIM2EXCEL: report!";
            sendEmail(subject, message, false, null, new String[]{user.getEmail()}, null, null);
            return true;
        } catch (Exception ex) {
            logger.error("Error sending message!", ex);
            return false;
        }
    }

    @Async
    public Boolean sendGeneralEmail(EmailMessage emailMessage) {
        try {
            sendEmail(emailMessage.getSubject(), emailMessage.getMessage(), false, emailMessage.getAttachments(), emailMessage.getTo(), emailMessage.getCc(),
                    emailMessage.getBcc());
            return true;
        } catch (Exception ex) {
            logger.error("Error sending message!", ex);
            return false;
        }
    }


}