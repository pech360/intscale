package com.synlabs.intscale.service.communication;

import com.synlabs.intscale.common.EmailMessage;
import com.synlabs.intscale.entity.Timeline;
import com.synlabs.intscale.entity.threesixty.Stakeholder;
import com.synlabs.intscale.entity.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class CommunicationService {
    private static final Logger logger = LoggerFactory.getLogger(CommunicationService.class);

    @Autowired
    private AbstractEmailSender emailSender;

    @Value("${intscale.s3.assets-url}")
    private String intscale_assets;

    @PostConstruct
    void init() {
        logger.debug("EmailSender " + emailSender.getClass().getName());
    }

    public Boolean registerWelcomeEmail(User user, String password, String website) {
        if (!StringUtils.isEmpty(user.getEmail())) {

            if (StringUtils.isEmpty(website)) {
                website = "www.aim2excel.in";
            } else {
                website = String.format("%s.aim2excel.in", website);
            }

            emailSender.userWelcomeEmail(user, password, website);
        }
        return true;
    }

    public void sendResetPasswordSmsAndEmail(String token, String email, String name) {
        emailSender.sendResetPassword(token, email, name);
    }

    public void sharePostOnEmail(String email, Timeline timeline, String body) {
        emailSender.sharePostOnEmail(email, timeline, intscale_assets, body);
    }

    public void sendStakeholderTestLink(Stakeholder stakeholder) {
        emailSender.sendStakeholderTestLinkEmail(stakeholder);
    }

    public void sendStakeholderTestRemind(Stakeholder stakeholder) {
        emailSender.sendStakeholderTestRemindEmail(stakeholder);
    }

    public void sendStakeholderVerification(Stakeholder stakeholder) {
        emailSender.sendStakeholderVerification(stakeholder);
    }

    public void sendNotificationUponCompletionOfTests(User user) {
        emailSender.sendNotificationUponCompletionOfTests(user);
    }

    public void sendReportToTestTaker(User user, File file) {
        List<File> files = new ArrayList<>(1);
        files.add(file);
        emailSender.sendReportToTestTaker(user, files);
    }

    public void shareReport(User user, String email, String cc, File file) {
        List<File> files = new ArrayList<>(1);
        files.add(file);
        emailSender.shareReport(user, email, cc, files);
    }

    public void send(User user, String file) {
        emailSender.send(user, file);
    }

    public void testUploaded(String[] emails, Integer count) {


        emailSender.testUploaded(emails, count);
    }

    public Boolean testSignUpMail(User user, String password, String website) {
        if (!StringUtils.isEmpty(user.getEmail())) {

            if (StringUtils.isEmpty(website)) {
                website = "www.aim2excel.in";
            } else {
                website = String.format("%s.aim2excel.in", website);
            }

            emailSender.testSignUpMail(user, password, website);
        }
        return true;
    }


    public void buyProduct(User user, String password, String name) {
        emailSender.buyProduct(user, password, name);
    }

    public void demoCompleteMail(User user, List<File> files) {
        emailSender.demoCompleteMail(user, files);
    }

    public void sendGeneralEmail(EmailMessage emailMessage) {
        emailSender.sendGeneralEmail(emailMessage);
    }
}