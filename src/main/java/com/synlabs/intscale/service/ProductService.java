package com.synlabs.intscale.service;

import com.synlabs.intscale.entity.ProductTenant;
import com.synlabs.intscale.entity.test.Product;
import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.entity.user.Tenant;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.ProductRepository;
import com.synlabs.intscale.jpa.ProductTenantRepository;
import com.synlabs.intscale.jpa.TestRepository;
import com.synlabs.intscale.view.ProductRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by India on 1/24/2018.
 */
@Service
public class ProductService extends BaseService {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductTenantRepository productTenantRepository;

    @Autowired
    private TestRepository testRepository;

    public List<Product> getList() {
        List<Product> products = new ArrayList<>();
        products.addAll(productRepository.findAllByTenant(getTenant()));
        List<ProductTenant> productTenants = productTenantRepository.findAllByDomain(getTenant());
        if (!CollectionUtils.isEmpty(productTenants)) {
            productTenants.forEach(pt -> {
                products.add(pt.getProduct());
            });
        }
        return products;
    }

    public Product save(ProductRequest request) {
        Product product = null;
        if (StringUtils.isEmpty(request.getId())) {
            if (!StringUtils.isEmpty(productRepository.findOneByNameAndTenant(request.getName(), getTenant()))) {
                throw new ValidationException("product name is duplicate");
            }
            product = request.toEntity();
        } else {
            product = request.toEntity(productRepository.findOne(request.getId()));
        }
        List<Long> testIds = request.getTest();
        Set<Test> testList = new HashSet<>(testIds.size());
        for (Long id : testIds) {
            Test test = testRepository.findOne(id);
            if (!test.isConstruct() && !test.isLeafNode() && test.isLatestVersion()) {
                testList.add(test);
            } else {
                List<Test> tests = testRepository.findAllByLatestVersionIsTrueAndParentIdAndTenantOrConstructIsTrueAndParentIdAndTenant(id, getTenant(), id, getTenant());
                for (Test test1 : tests) {
                    if (!test1.isConstruct() && !test1.isLeafNode() && test1.isLatestVersion()) {
                        testList.add(test1);
                    } else {
                        List<Test> tests1 = testRepository.findAllByLatestVersionIsTrueAndParentIdAndTenantOrConstructIsTrueAndParentIdAndTenant(test1.getId(), getTenant(), test1.getId(), getTenant());
                        for (Test test2 : tests1) {
                            if (!test2.isConstruct() && !test2.isLeafNode() && test2.isLatestVersion()) {
                                testList.add(test2);
                            } else {
                                List<Test> tests2 = testRepository.findAllByLatestVersionIsTrueAndParentIdAndTenantOrConstructIsTrueAndParentIdAndTenant(test2.getId(), getTenant(), test2.getId(), getTenant());
                                for (Test test3 : tests2) {
                                    if (!test3.isConstruct() && !test3.isLeafNode() && test3.isLatestVersion()) {
                                        testList.add(test3);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        product.setTest(new ArrayList<>(testList));
        return productRepository.save(product);
    }

    public void delete(Long id) {
        productRepository.delete(id);
    }

    public Product findOneByName(String name) {
        return productRepository.findOneByNameAndTenant(name, getTenant());
    }

    public List<Test> getAllTestByProduct(Long id) {
        return productRepository.getOne(id).getTest();
    }
}
