package com.synlabs.intscale.service;

import com.querydsl.jpa.impl.JPAQuery;
import com.synlabs.intscale.entity.Report.QUserReportStatus;
import com.synlabs.intscale.entity.Report.UserReportStatus;
import com.synlabs.intscale.entity.counselling.CounsellingSession;
import com.synlabs.intscale.entity.counselling.CounsellingSessionNotes;
import com.synlabs.intscale.entity.counselling.QCounsellingSession;
import com.synlabs.intscale.entity.masterdata.MasterDataDocument;
import com.synlabs.intscale.entity.test.QSubTag;
import com.synlabs.intscale.entity.test.SubTag;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.enums.CounsellingSessionStatus;
import com.synlabs.intscale.enums.CounsellingSessionType;
import com.synlabs.intscale.ex.NotFoundException;
import com.synlabs.intscale.jpa.*;
import com.synlabs.intscale.view.DashboardRequest;
import com.synlabs.intscale.view.TimelineRequest;
import com.synlabs.intscale.view.UserResponseForCounsellor;
import com.synlabs.intscale.view.counselling.CounsellingSessionNoteRequest;
import com.synlabs.intscale.view.counselling.CounsellingSessionNoteResponse;
import com.synlabs.intscale.view.counselling.CounsellingSessionRequest;
import com.synlabs.intscale.view.counselling.CounsellingSessionResponse;
import com.synlabs.intscale.view.question.SubTagRequest;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTimeComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CounsellorService extends BaseService
{

  @Autowired
  private CounsellingSessionNotesRepository counsellingSessionNotesRepository;
  @Autowired
  private CounsellingSessionRepository      counsellingSessionRepository;
  @Autowired
  private UserRepository                    userRepository;
  @Autowired
  private SubTagRepository                  subTagRepository;
  @Autowired
  private TimelineService                   timelineService;
  @Autowired
  private UserReportStatusRepository userReportStatusRepository;

  @PersistenceContext
  private EntityManager                     entityManager;

  public CounsellingSessionNoteResponse createCounsellorNote(CounsellingSessionNoteRequest request)
  {
    CounsellingSessionNotes counsellingSessionNotes = counsellingSessionNotesRepository.findByIdAndTenant(request.getId(), getTenant());
    counsellingSessionNotes = request.toEntity(counsellingSessionNotes);
    counsellingSessionNotes.setTenant(getTenant());
    counsellingSessionNotes = counsellingSessionNotesRepository.saveAndFlush(counsellingSessionNotes);
    return new CounsellingSessionNoteResponse(counsellingSessionNotes);
  }

  public List<CounsellingSessionNoteResponse> getCounsellorNotes(CounsellingSessionType type)
  {
    List<CounsellingSessionNotes> notes = counsellingSessionNotesRepository.findAllByCounsellingSessionTypeAndSampleAndTenant(type, true, getTenant());
    return notes.stream().map(CounsellingSessionNoteResponse::new).collect(Collectors.toList());
  }

  public CounsellingSessionResponse createCounsellingSession(CounsellingSessionRequest request)
  {
    CounsellingSession session = counsellingSessionRepository.findByIdAndTenant(request.getId(), getTenant());
    session = request.toEntity(session);

    User user = userRepository.findOneByIdAndTenant(request.getUserId(), getTenant());
    if (user == null)
    {
      throw new NotFoundException("user not found");
    }

    session.setUser(user);
    session.setCounsellor(getUser());
    session.getSubTags().clear();
    for (SubTagRequest subTagRequest : request.getTags())
    {
      SubTag subTag = subTagRepository.findOne(subTagRequest.getId());
      session.getSubTags().add(subTag);
    }

    Long sequence = counsellingSessionRepository.countCounsellingSessionsByUserAndCounsellorAndTenant(session.getUser(), session.getCounsellor(), getTenant());
    session.setSequenceNumber(++sequence);
    session.setTenant(getTenant());
    session = counsellingSessionRepository.saveAndFlush(session);

    for (CounsellingSessionNoteRequest counsellingSessionNoteRequest : request.getCounsellingSessionNotes())
    {
      CounsellingSessionNotes note = counsellingSessionNotesRepository.findByIdAndSampleAndTenant(counsellingSessionNoteRequest.getId(), false, getTenant());
      note = counsellingSessionNoteRequest.toEntity(note);
      note.setCounsellingSession(session);
      note.setSample(false);
      note.setCounsellingSessionType(session.getCounsellingSessionType());
      note = counsellingSessionNotesRepository.saveAndFlush(note);
      if (note.isTimelinePost())
      {
        TimelineRequest timelineRequest = new TimelineRequest();
        timelineRequest.setContentType("homework");
        timelineRequest.setHeading(note.getStatement());
        timelineRequest.setDescription(note.getResponse());
        timelineService.savePost(timelineRequest, user);
      }
    }
    session = counsellingSessionRepository.findByIdAndTenant(session.getId(), getTenant());

    session = prepareForNewCounsellingSession(session);

    return new CounsellingSessionResponse(session);
  }

  public List<CounsellingSessionResponse> getCounsellingSessionsByCurrentCounsellor(CounsellingSessionType type, Long userId)
  {
    User user = userRepository.findOneByIdAndTenant(userId, getTenant());
    User counsellor = getUser();
    DateTimeComparator dateTimeComparator = DateTimeComparator.getDateOnlyInstance();
    Date today = DateUtils.addMinutes(new Date(), 330);

    List<CounsellingSessionNotes> sampleNotesByExplore = counsellingSessionNotesRepository
        .findAllBySampleAndCounsellingSessionTypeAndTenant(true, CounsellingSessionType.EXPLORE, getTenant());

    List<CounsellingSessionNotes> sampleNotesByEngage = counsellingSessionNotesRepository
        .findAllBySampleAndCounsellingSessionTypeAndTenant(true, CounsellingSessionType.ENGAGE, getTenant());

    List<CounsellingSessionNotes> sampleNotesByCommit = counsellingSessionNotesRepository
        .findAllBySampleAndCounsellingSessionTypeAndTenant(true, CounsellingSessionType.COMMIT, getTenant());

    List<CounsellingSessionNotes> sampleNotesByFreeFlow = counsellingSessionNotesRepository
        .findAllBySampleAndCounsellingSessionTypeAndTenant(true, CounsellingSessionType.FREE_FLOW, getTenant());

    List<CounsellingSession> sessions = counsellingSessionRepository.findByUserAndCounsellingSessionTypeAndTenant(user, type, getTenant());

    sessions.forEach(session -> {
      if (session.getCounsellingSessionStatus().equals(CounsellingSessionStatus.UPCOMING))
      {
        if (dateTimeComparator.compare(session.getScheduledOn(),today) < 0)
        {
          session.setCounsellingSessionStatus(CounsellingSessionStatus.MISSED);
          session = counsellingSessionRepository.saveAndFlush(session);
        }
      }
    });

    List<CounsellingSessionResponse> sessionResponses = sessions.stream().map(CounsellingSessionResponse::new).collect(Collectors.toList());

    sessionResponses.forEach(sessionResponse -> {

      if (sessionResponse.getCounsellingSessionStatus().equals(CounsellingSessionStatus.UPCOMING) || sessionResponse.getCounsellingSessionStatus().equals(CounsellingSessionStatus.MISSED))
      {
        switch (sessionResponse.getCounsellingSessionType())
        {
          case EXPLORE:
            sessionResponse.getCounsellingSessionNotes().addAll(sampleNotesByExplore.stream().map(CounsellingSessionNoteResponse::new).collect(Collectors.toList()));
            break;
          case COMMIT:
            sessionResponse.getCounsellingSessionNotes().addAll(sampleNotesByCommit.stream().map(CounsellingSessionNoteResponse::new).collect(Collectors.toList()));
            break;
          case ENGAGE:
            sessionResponse.getCounsellingSessionNotes().addAll(sampleNotesByEngage.stream().map(CounsellingSessionNoteResponse::new).collect(Collectors.toList()));
            break;
          case FREE_FLOW:
            sessionResponse.getCounsellingSessionNotes().addAll(sampleNotesByFreeFlow.stream().map(CounsellingSessionNoteResponse::new).collect(Collectors.toList()));
            break;
        }
      }
    });

    return sessionResponses;
  }

  private CounsellingSession prepareForNewCounsellingSession(CounsellingSession session)
  {
    if (session.getCounsellingSessionStatus().equals(CounsellingSessionStatus.UPCOMING) || session.getCounsellingSessionStatus().equals(CounsellingSessionStatus.MISSED))
    {
      List<CounsellingSessionNotes> sampleNotes = counsellingSessionNotesRepository
          .findAllBySampleAndCounsellingSessionTypeAndTenant(true, session.getCounsellingSessionType(), getTenant());
      sampleNotes.forEach(sampleNote -> {
        sampleNote.setId(null);
        sampleNote.setSample(false);
      });
      session.setCounsellingSessionNotes(sampleNotes);
    }

    return session;
  }

  public List<String> getAllLeadsOfUser(Long userId)
  {
    List<String> leads = new ArrayList<>();
    User user = userRepository.findOneByIdAndTenant(userId, getTenant());
    String lead = "";
    SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy");
    for (CounsellingSession session : user.getUserCounsellingSessions())
    {
      if (session.getLeads() != null)
      {
        for (int i = 0; i < session.getLeads().length; i++)
        {
          String leadDate = dateFormat.format(session.getLastModifiedDate().toDate());
          lead = String.format("%s  ||  %s", leadDate, session.getLeads()[i]);
          leads.add(lead);
        }
      }
    }
    return leads;
  }

  public List<UserResponseForCounsellor> findAllForCounsellorAdmin(DashboardRequest request)
  {
    JPAQuery<User> query = new JPAQuery<>(entityManager);
    QSubTag subTag = QSubTag.subTag;
    QCounsellingSession counsellingSession = QCounsellingSession.counsellingSession;
    query.select(counsellingSession.user)
         .distinct()
         .from(counsellingSession)
         .where(counsellingSession.counsellingSessionStatus.eq(CounsellingSessionStatus.COMPLETED)
                                                           .or(counsellingSession.counsellingSessionStatus.eq(CounsellingSessionStatus.DELAYED)));

    if (!CollectionUtils.isEmpty(request.getGrades()))
    {
      query.where(counsellingSession.user.grade.in(request.getGrades()));
    }

    if (!CollectionUtils.isEmpty(request.getGenders()))
    {
      query.where(counsellingSession.user.gender.in(request.getGenders()));
    }

    if (!CollectionUtils.isEmpty(request.getSchools()))
    {
      query.where(counsellingSession.user.schoolName.in(request.getSchools()));
    }

    if (!CollectionUtils.isEmpty(request.getCities()))
    {
      query.where(counsellingSession.user.city.in(request.getCities()));
    }
    if (!CollectionUtils.isEmpty(request.getSubTags()))
    {
      query.innerJoin(counsellingSession.subTags, subTag).where(subTag.id.in(request.getSubTags())).fetch();
    }

    HashSet<User> users = new HashSet<>(query.fetch());

    List<UserResponseForCounsellor> userResponses = users.stream().map(UserResponseForCounsellor::new).collect(Collectors.toList());
    userResponses.forEach(user->{
      UserReportStatus userReportStatus = userReportStatusRepository.findByUserIdAndReportNameAndTenant(user.getId(), "Aim Composite Report", getTenant());
      user.setAimAssessmetsCompletedOn(DateUtils.addMinutes(userReportStatus.getCreatedDate().toDate(), 330));
    });

    return userResponses;
  }
}