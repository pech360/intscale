package com.synlabs.intscale.service;

import com.synlabs.intscale.ImportModel.*;
import org.jxls.reader.ReaderConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.jxls.reader.ReaderBuilder;

import javax.annotation.PostConstruct;

@Service
public class ExcelImportService
{
  private static final Logger logger = LoggerFactory.getLogger(ExcelImportService.class);

  private final String IMPORT_USER_FILE = "user.xml";
  private final String IMPORT_TEST_FILE = "test.xml";
  private final String IMPORT_STUDENT_FILE = "student.xml";
  private final String IMPORT_USER_TEST_ASSIGN_FILE = "userTestAssign.xml";
  private final String IMPORT_AIM_EXPERT_INVENTORY_FILE = "aimExpertInventory.xml";
  private final String IMPORT_TEST_QUESTIONS_FILE = "testQuestion.xml";
  private final String IMPORT_TEST_RESPONSES_FILE = "offline_test_response_data_sync_sheet.xml";
  private final String IMPORT_CAREER_INTEREST_INVENTORY_FILE = "workbook-import-layout/careerInterestInventory.xml";

  @PostConstruct
  public void init()
  {
    ReaderConfig.getInstance().setSkipErrors(true);
  }

  private void fileProcessor(MultipartFile file, String filePath, Map<String, ArrayList> beans)
  {
    try
    {
      ReaderBuilder.buildFromXML(ExcelImportService.class.getClassLoader().getResourceAsStream(filePath)).read(new ByteArrayInputStream(file.getBytes()), beans);
    }
    catch (Exception exp)
    {
      logger.error("Error importing records : {}", exp);
    }
  }

  public Map<String, ArrayList> importStudentRecords(MultipartFile file)
  {
    Map<String, ArrayList> beans = new HashMap<>();
    beans.put("students", new ArrayList<ImportStudentModel>());
    fileProcessor(file, IMPORT_STUDENT_FILE, beans);
    return beans;
  }


  public Map<String, ArrayList> importUserRecords(MultipartFile file)
  {
    Map<String, ArrayList> beans = new HashMap<>();
    beans.put("users", new ArrayList<ImportUserModel>());
    fileProcessor(file, IMPORT_USER_FILE, beans);
    return beans;
  }

  public Map<String, ArrayList> importTestRecords(MultipartFile file)
  {
    Map<String, ArrayList> beans = new HashMap<>();
    beans.put("questions", new ArrayList<ImportQuestionModel>());
    beans.put("categories", new ArrayList<ImportCategoryModel>());
    beans.put("tests", new ArrayList<ImportTestModel>());
    beans.put("answers", new ArrayList<ImportAnswerModel>());
    beans.put("questionTags", new ArrayList<ImportQuestionTagModel>());
    fileProcessor(file, IMPORT_TEST_FILE, beans);
    return beans;
  }

  public Map<String, ArrayList> importUserTestAssignmentRecords(MultipartFile file)
  {
    Map<String, ArrayList> beans = new HashMap<>();
    beans.put("userTestAssignments", new ArrayList<ImportUserTestAssignModel>());
    fileProcessor(file, IMPORT_USER_TEST_ASSIGN_FILE, beans);
    return beans;
  }

  public Map<String, ArrayList> importAimExpertInventoryRecords(MultipartFile file)
  {
    Map<String, ArrayList> beans = new HashMap<>();
    beans.put("aimExpertInventories", new ArrayList<ImportAimExpertInventoryModel>());
    fileProcessor(file, IMPORT_AIM_EXPERT_INVENTORY_FILE, beans);
    return beans;
  }
  public Map<String, ArrayList> importTestQuestionsRecords(MultipartFile file)
  {
    Map<String, ArrayList> beans = new HashMap<>();
    beans.put("testQuestions", new ArrayList<ImportTestQuestionModel>());
    fileProcessor(file, IMPORT_TEST_QUESTIONS_FILE, beans);
    return beans;
  }

  public Map<String, ArrayList> importTestResponses(MultipartFile file)
  {
    Map<String, ArrayList> beans = new HashMap<>();
    beans.put("responses", new ArrayList<ImportTestResponseModel>());
    fileProcessor(file, IMPORT_TEST_RESPONSES_FILE, beans);
    return beans;
  }

  public Map<String, ArrayList> importCareerInterestInventory(MultipartFile file)
  {
    Map<String, ArrayList> beans = new HashMap<>();
    beans.put("careerInterestInventory", new ArrayList<ImportCareerInterestInventoryModel>());
    fileProcessor(file, IMPORT_CAREER_INTEREST_INVENTORY_FILE, beans);
    return beans;
  }
}
