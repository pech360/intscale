package com.synlabs.intscale.service;

import com.synlabs.intscale.controller.SponsorController;
import com.synlabs.intscale.entity.masterdata.Sponsor;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.SponsorRepository;
import com.synlabs.intscale.jpa.UserRepository;
import com.synlabs.intscale.store.FileStore;
import com.synlabs.intscale.view.CommonResponse;
import com.synlabs.intscale.view.SponsorUserAssignRequest;
import com.synlabs.intscale.view.masterdata.SponsorRequest;
import com.synlabs.intscale.view.masterdata.SponsorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
public class SponsorService extends BaseService {

    @Autowired
    private SponsorRepository sponsorRepository;

    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FileStore store;

    private static final Logger logger = LoggerFactory.getLogger(SponsorService.class);

    public List<SponsorResponse> list() {
        List<Sponsor> all = sponsorRepository.findAllByTenant(getTenant());
        List<SponsorResponse> response = all.stream().map(SponsorResponse::new).collect(Collectors.toList());
        return response;
    }

    public SponsorResponse save(SponsorRequest request) {
        Sponsor sponsor = null;
        if (request.getId() != null) {
            sponsor = sponsorRepository.findOneByIdAndTenant(request.getId(), getTenant());
        }
        sponsor = request.toEntity(sponsor);
        sponsorRepository.saveAndFlush(sponsor);
        return new SponsorResponse(sponsor);
    }

    public void uploadSponsorLogo(MultipartFile file, Long id, String imageType) {
        if (file.isEmpty()) {
            throw new ValidationException("Missing file");
        }
        if (id == null) {
            throw new ValidationException("Missing record");
        }
        try {
            String fileExtn = file.getOriginalFilename().split("\\.")[1];
            String filename = UUID.randomUUID().toString() + "." + fileExtn;
            store.store("profileimage", filename, file.getBytes());
            attachSponsorImage(filename, id, imageType);
        } catch (IOException e) {
            logger.error("@uploadimage", e);
            throw new ValidationException("Unknown error!");
        }
    }

    private void attachSponsorImage(String filename, Long id, String imageType) {
        Sponsor sponsor = sponsorRepository.findOneByIdAndTenant(id, getTenant());
        switch (imageType) {
            case "logo":
                sponsor.setLogo(filename);
                break;
            case "reportCoverFrontImage":
                sponsor.setReportCoverFrontImage(filename);
                break;
            case "reportCoverBackImage":
                sponsor.setReportCoverBackImage(filename);
                break;
            default:
                    throw new ValidationException("couldn't identify uploaded image type");
        }
        sponsorRepository.saveAndFlush(sponsor);
    }

    private void attachSponsorWithUserList(List<User> users, List<Sponsor> sponsors) {

        users.forEach(user -> {
            user.getSponsors().clear();
            user.getSponsors().addAll(sponsors);
        });
        userRepository.save(users);

    }

    public void assignSponsorsToUsers(SponsorUserAssignRequest request) {
        if (CollectionUtils.isEmpty(request.getSponsorIds())) {
            throw new ValidationException("no sponsors are selected to assign");
        }
        if (request.isUseSearchQuery()) {
            assignSponsorsWithSearchUser(request.getRoleId(), request.getTenantId(), request.getSchoolName(), request.getQuery(), request.getFromDate(), request.getToDate(), request.getSponsorIds());
        } else {
            if (CollectionUtils.isEmpty(request.getUserIds())) {
                throw new ValidationException("no users are selected to assign");
            }
            assignSponsorsWithUserIds(request.getUserIds(), request.getSponsorIds());
        }

    }

    public void assignSponsorsWithSearchUser(Long roleId, Long tenantId, String schoolName, String searchQuery, String fromDate, String toDate, List<Long> sponsorIds) {
        int page = 1;
        int pageSize = 500;
        int offset = 0;

        List<Sponsor> sponsors = sponsorRepository.findAllByIdInAndTenant(sponsorIds, getTenant());

        while (true) {
            CommonResponse userListPaginated = userService.getUserListPaginated(roleId, tenantId, schoolName, searchQuery, fromDate, toDate, pageSize, page);

            attachSponsorWithUserList(userListPaginated.records, sponsors);
            offset = (page) * pageSize;
            if (userListPaginated.totalRecords <= offset) {
                break;
            }
            page++;
        }
    }

    public void assignSponsorsWithUserIds(List<Long> userIds, List<Long> sponsorIds) {
        List<Sponsor> sponsors = sponsorRepository.findAllByIdInAndTenant(sponsorIds, getTenant());
        List<User> users = userRepository.findAllByIdInAndTenant(userIds, getTenant());
        attachSponsorWithUserList(users, sponsors);
    }
}
