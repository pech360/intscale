package com.synlabs.intscale.scheduler.platformDailySummary;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.synlabs.intscale.enums.ReportStatus;
import com.synlabs.intscale.service.PlatformDailySummaryService;

import com.synlabs.intscale.view.PlatformDailySummary;
//LNT
@Component
public class PlatFormDailySummaryScheduler {
	@Autowired
	PlatformDailySummaryService platformDailySummaryService;

	private static Logger logger = LoggerFactory.getLogger(PlatFormDailySummaryScheduler.class);

	@Scheduled(cron = "0 30 3 * * *")
	public void sendPlatformDailySummary() {
		logger.info("Summary Scheduler started .");
		// "ankitn@intscale.com", "rajeshk@intscale.com", "mayankb@intscale.com",
		List<PlatformDailySummary> platformDailySummaryList = new ArrayList<>();
		Date begin = new DateTime(LocalDateTime.now().minusHours(24).toDate(), DateTimeZone.forID("Asia/Kolkata")).toDate();
		Date end = new DateTime(LocalDateTime.now().toDate(), DateTimeZone.forID("Asia/Kolkata")).toDate();
		platformDailySummaryList.addAll(platformDailySummaryService.findAllPurchasedProductToday(begin, end));
		platformDailySummaryList.addAll(platformDailySummaryService.findAllCompletedAssessment(begin, end, ReportStatus.Generated.toString()));
		platformDailySummaryList.addAll(platformDailySummaryService.findAllSignedUpAndGivenDemoTest(begin, end));
		platformDailySummaryList.addAll(platformDailySummaryService.findAllSignedUpAndNotGivenDemoTest(begin, end));
//		platformDailySummaryList.addAll(platformDailySummaryService.findAllSignedUpAndGivenDemoTest(
//		LocalDateTime.parse("2017-10-16T07:37:30").toDate(),
//		LocalDateTime.parse("2019-10-16T07:37:30").toDate()));
		List<String> columnHeaders = Arrays.asList("S No.", "Name", "Username", "Email", "Phone Number", "Activity");
		String[] mailTo = new String[] { "ankitn@intscale.com", "rajeshk@intscale.com", "mayankb@intscale.com",
				"laxmi.ssj4@gmail.com" };
		platformDailySummaryService.createPdfOfPlatformDailySummary(platformDailySummaryList, "platformDailySummary",mailTo, columnHeaders);

	}

}
