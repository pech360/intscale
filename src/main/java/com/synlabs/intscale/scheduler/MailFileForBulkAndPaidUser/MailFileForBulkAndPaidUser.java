//package com.synlabs.intscale.scheduler.MailFileForBulkAndPaidUser;
//
//import com.synlabs.intscale.entity.Report.UserReportStatus;
//import com.synlabs.intscale.entity.user.User;
//import com.synlabs.intscale.jpa.UserReportStatusRepository;
//import com.synlabs.intscale.jpa.UserRepository;
//import com.synlabs.intscale.service.BaseService;
//import com.synlabs.intscale.service.ReportService;
//import com.synlabs.intscale.service.communication.CommunicationService;
//import com.synlabs.intscale.store.FileStore;
//import org.apache.poi.util.IOUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//import org.springframework.util.StringUtils;
//
//import javax.servlet.http.HttpServletResponse;
//import java.io.File;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.util.ArrayList;
//import java.util.List;
//
//@Component
//public class MailFileForBulkAndPaidUser extends BaseService {
//
//
//    @Autowired
//    ReportService reportService;
//    @Autowired
//    UserReportStatusRepository userReportStatusRepository;
//    @Autowired
//    CommunicationService communicationService;
//    @Autowired
//    UserRepository userRepository;
//    @Autowired
//    private FileStore store;
//    @Autowired
//    HttpServletResponse response;
//    private static final Logger log = LoggerFactory.getLogger(MailFileForBulkAndPaidUser.class);
//
//    @Scheduled(initialDelay = 1000, fixedRate = 1000 * 60 * 5)
//    public void fileStatus() {
//
//        int counter = 0;
//        System.out.println("Run scheduler");
//
//        try {
//
//            List<User> users = userRepository.findAllByFlagAndIsPaid("New", true);
//
//            for (User user : users) {
//                List<UserReportStatus> userReportStatuses = userReportStatusRepository.findAllByStatusAndUserId("Generated", user.getId());
//                for (UserReportStatus userReportStatus : userReportStatuses) {
//                    if (userReportStatus.getFlag() == false) {
//                        log.info("after false flag");
//                        String fileName = reportService.tryToGetReportFromS3Bucket(userReportStatus.getUser().getId());
//                        if (!StringUtils.isEmpty(fileName)) {
//                            response.setContentType("application/pdf");
//                            response.setHeader("Content-disposition", "attachment; filename=" + fileName);
//                            response.setHeader("fileName", fileName);
//                            String file = store.download("profileimage", fileName);
//                            log.info("store");
////                            InputStream is = store.getStream("profileimage", fileName);
////                            OutputStream out = response.getOutputStream();
////                            IOUtils.copy(is, out);
////                            out.flush();
////                            is.close();
//
//
//                            //  File file = reportService.downloadAimReport(userReportStatus.getUser().getId(), userReportStatus.getTenant());
//                            // List<File> files = new ArrayList<>();
//                            //files.add(file);
//                            // communicationService.sendReportToTestTaker(userReportStatus.getUser(), files);
//                            log.info("before mail");
//                            communicationService.send(userReportStatus.getUser(), file);
//                            log.info("after mail");
//                            userReportStatus.setFlag(true);
//                            log.info("set flag");
//                            counter++;
//                        }
//                    }
//                }
//            }
//
//        } catch (Exception ex) {
//            log.info(ex.toString());
//        }
//
//        if (counter == 0)
//            log.info("No record found for sending mails");
//        else
//            log.info("mails have been sent to users");
//    }
//
//
//}
//
//
