package com.synlabs.intscale.scheduler.paymentgateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import com.synlabs.intscale.entity.paymentGateway.TransactionEntry;
import com.synlabs.intscale.jpa.TransactionEntryRepository;
import com.synlabs.intscale.service.UserService;
import com.synlabs.intscale.view.paymentGateway.TransactionResponse;


@Component
public class CapturePayment {
	
	@Autowired
	TransactionEntryRepository transactionEntryRepository;
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	UserService userService;
	
	private static final Logger log = LoggerFactory.getLogger(CapturePayment.class);
	
	@Scheduled(initialDelay = 1000, fixedRate = 1000*60*60*24)
	public void capturePayment() {
		int counter=0;
		System.out.println("Run scheduler");
		List<TransactionEntry> transactionEntryList=transactionEntryRepository.findAll();
		for(TransactionEntry transcationEntry :transactionEntryList) {
			if(transcationEntry.getStatus().equals("authorized")) {
				ResponseEntity<?> responseEntity=null;
				try {
					HttpHeaders httpHeaders = userService.createHttpHeaders(userService.keyId,userService.keySecret);
					HttpEntity<String> entity = new HttpEntity<String>("parameters", httpHeaders);
					responseEntity=restTemplate.exchange("https://api.razorpay.com/v1/payments/"+transcationEntry.getPayId()+"/capture?amount="+transcationEntry.getAmount(),HttpMethod.POST,entity,TransactionResponse.class);
					//ResponseEntity<?> responseEntity=restTemplate.getForEntity("https://api.razorpay.com/v1/payments/"+payId,TransactionResponse.class);
					TransactionResponse transactionResponse=(TransactionResponse)responseEntity.getBody();
					TransactionEntry transactionEntry=transactionEntryRepository.findByPayId(transactionResponse.getId());
					//Update Data
					transactionEntry.setCaptured(transactionResponse.getCaptured());
					transactionEntry.setTax(transactionResponse.getTax());
					transactionEntry.setFee(transactionResponse.getFee());
					transactionEntry.setStatus(transactionResponse.getStatus());
					//Store Data In DB
					transactionEntryRepository.save(transactionEntry);
					counter++;
				}catch(Exception ex) {
					//ex.printStackTrace();
					System.out.println("Already Captured");
				}
			}
		}
		if(counter==0)
		log.info("No record found for capture");
		else
		log.info("Records have captured");	
	}
}
