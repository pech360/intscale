package com.synlabs.intscale.store;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by itrs on 6/15/2017.
 */
public interface FileStore
{
  void store(String category, File file);

  void store(String category, String filename, byte[] data) throws IOException;

  void delete(String filename);

  InputStream getStream(String filename,String fileType) throws IOException;

  String download(String category, String filename) throws IOException;
}