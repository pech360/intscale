package com.synlabs.intscale.store;

import com.synlabs.intscale.config.Local;
import com.synlabs.intscale.ex.DownloadException;
import com.synlabs.intscale.ex.UploadException;
import com.synlabs.intscale.service.BaseService;
import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Profile(value="default")
@Local
@Service
public class FileSystemFileStore extends BaseService implements FileStore
{

  private static Logger logger = LoggerFactory.getLogger(FileSystemFileStore.class);

  @Value("${scale.image.upload.location}")
  private String uploaddir;

  private Path uploadpath;

  @PostConstruct
  public void init()
  {
    uploadpath = Paths.get(uploaddir);
    if (uploadpath.toFile().exists())
    {
      if (!uploadpath.toFile().isDirectory())
      {
        logger.error("{} is not a dir, cant continue", uploaddir);
        throw new UploadException(uploaddir + "is not a dir, cant continue");
      }
    }
    else
    {
      if (!uploadpath.toFile().mkdirs())
      {
        logger.error("cannot create upload directory", uploaddir);
        throw new UploadException(uploaddir + "cannot create upload directory!");
      }
    }
  }

  @Override
  public void store(String category, String filename, byte[] data) throws IOException
  {
    logger.info("local fileSystem called..{}");
    Path finalLocation = uploadpath.resolve(category);
    if (!finalLocation.toFile().exists())
    {
      if (!finalLocation.toFile().mkdirs())
      {
        logger.error("cannot create upload directory", finalLocation);
        throw new UploadException(finalLocation + "cannot create upload directory!");
      }
    }
    String filepath = finalLocation.resolve(filename).toString();
    BufferedOutputStream stream =
        new BufferedOutputStream(new FileOutputStream(new File(filepath)));
    stream.write(data);
    stream.close();
  }

  @Override
  public void delete(String fileName){

  }

  @Override
  public void store(String category, File file)
  {
    Path finalLocation = uploadpath.resolve(category);
    if (!finalLocation.toFile().exists())
    {
      if (!finalLocation.toFile().mkdirs())
      {
        logger.error("cannot create upload directory", finalLocation);
        throw new UploadException(finalLocation + "cannot create upload directory!");
      }
    }

    try
    {
      Files.copy(Paths.get(file.getAbsolutePath()), finalLocation.resolve(file.getName()));
    }
    catch (IOException e)
    {
      logger.error("Error copying file!", e);
      throw new UploadException(e);
    }
  }

  @Override
  public String download(String category, String filename) throws IOException
  {
    Path finalLocation = uploadpath.resolve(category);
    Path dirpath = Paths.get(uploaddir);
    String tempFilename = dirpath.resolve(UUID.randomUUID().toString() + filename).toString();
    if (!finalLocation.toFile().exists())
    {
      throw new DownloadException("location not found");
    }
    String filepath = finalLocation.resolve(filename).toString();
    BufferedInputStream stream =
        new BufferedInputStream(new FileInputStream(new File(filepath)));
    FileOutputStream out = new FileOutputStream(tempFilename);
    IOUtils.copy(stream, out);
    out.flush();
    stream.close();
    return tempFilename;
  }

  @Override
  public InputStream getStream(String fileType,String filename) throws IOException
  {
    logger.info("Fetching on local {} - {}", fileType, filename);
    switch (fileType){
      case "PROFILEIMAGE":
        String profileImageName = uploaddir + "/profileimage/" + filename;
        if (Files.exists(Paths.get(profileImageName)))
        {
          return new FileInputStream(profileImageName);
        }
        throw new IOException("Cannot locate PROFILEIMAGE" + profileImageName);
      case "QUESTIONIMAGE":
        String questionImageName = uploaddir + "/questionimage/" + filename;
        if (Files.exists(Paths.get(questionImageName)))
        {
          return new FileInputStream(questionImageName);
        }
        throw new IOException("Cannot locate QUESTIONIMAGE" + questionImageName);
      case "QUESTIONAUDIO":
        String questionAudioName = uploaddir + "/questionaudio/" + filename;
        if (Files.exists(Paths.get(questionAudioName)))
        {
          return new FileInputStream(questionAudioName);
        }
        throw new IOException("Cannot locate QUESTIONAUDIO" + questionAudioName);
      case "ANSWERIMAGE":
        String answerImageName = uploaddir + "/answerimage/" + filename;
        if (Files.exists(Paths.get(answerImageName)))
        {
          return new FileInputStream(answerImageName);
        }
        throw new IOException("Cannot locate ANSWERIMAGE" + answerImageName);
      case "ANSWERAUDIO":
        String answerAudioName = uploaddir + "/answeraudio/" + filename;
        if (Files.exists(Paths.get(answerAudioName)))
        {
          return new FileInputStream(answerAudioName);
        }
        throw new IOException("Cannot locate ANSWERAUDIO" + answerAudioName);
      case "GROUPACTIVITY":
        String activityMedia = uploaddir + "/groupActivity/" + filename;
        if (Files.exists(Paths.get(activityMedia)))
        {
          return new FileInputStream(activityMedia);
        }
        throw new IOException("Cannot locate GROUPACTIVITY" + activityMedia);
      case "QUESTIONIVIDEO":
        String questionVideo = uploaddir + "/questionvideo/" + filename;
        if (Files.exists(Paths.get(questionVideo)))
        {
          return new FileInputStream(questionVideo);
        }
        throw new IOException("Cannot locate QUESTIONIVIDEO" + questionVideo);
      case "profileimage":
        profileImageName = uploaddir + "/profileimage/" + filename;
        if (Files.exists(Paths.get(profileImageName)))
        {
          return new FileInputStream(profileImageName);
        }
        throw new IOException("Cannot locate PROFILEIMAGE" + profileImageName);
    }
    throw new IOException("unknown exception");
  }

}
