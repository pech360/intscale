package com.synlabs.intscale;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;


@SpringBootApplication
@EnableAsync
@EnableWebSecurity
@EnableScheduling
public class IntScaleServer extends SpringBootServletInitializer
{
  public static void main(String[] args) {
    ApplicationContext ctx = SpringApplication.run(IntScaleServer.class, args);
  }
  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
    return builder.sources(IntScaleServer.class);
  }
  
  @Bean
  public RestTemplate getRestTemplate() {
     return new RestTemplate();
  }
}