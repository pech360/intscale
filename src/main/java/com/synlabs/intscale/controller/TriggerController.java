package com.synlabs.intscale.controller;

import com.synlabs.intscale.service.TriggerService;
import com.synlabs.intscale.view.TriggerRequest;
import com.synlabs.intscale.view.TriggerResponse;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;


import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.WRITE_REPORTS;

@RestController
@RequestMapping("/api/trigger/")
public class TriggerController {
    @Autowired
    private TriggerService service;

    @GetMapping
    @Secured(WRITE_REPORTS)
    public List<TriggerResponse> list() throws SchedulerException {
        return service.list();
    }

    @PutMapping
    @Secured(WRITE_REPORTS)
    public boolean update(@RequestBody TriggerRequest request) throws SchedulerException, ParseException {
        return service.updateAndReschedule(request);
    }

    @GetMapping("/pauseAll")
    @Secured(WRITE_REPORTS)
    public boolean pauseAllTriggers() throws SchedulerException {
        return service.pauseAllTriggers();
    }

    @GetMapping("/resumeAll")
    @Secured(WRITE_REPORTS)
    public boolean resumeAllTriggers() throws SchedulerException {
        return service.resumeAllTriggers();
    }

    @PutMapping("pause")
    @Secured(WRITE_REPORTS)
    public boolean pauseTrigger(@RequestBody TriggerRequest request) throws SchedulerException {
        return service.pauseTrigger(request);
    }

    @PutMapping("resume")
    @Secured(WRITE_REPORTS)
    public boolean resumeTrigger(@RequestBody TriggerRequest request) throws SchedulerException {
        return service.resumeTrigger(request);
    }
}
