package com.synlabs.intscale.controller;

import com.synlabs.intscale.service.ProductService;
import com.synlabs.intscale.view.ProductRequest;
import com.synlabs.intscale.view.ProductResponse;
import com.synlabs.intscale.view.TestDescriptionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.*;


import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by India on 1/24/2018.
 */
@RestController("ProductController")
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    private ProductService productService;
    private static Logger logger = LoggerFactory.getLogger(ProductController.class);

    @GetMapping
    @Secured(READ_PRODUCT)
    public List<ProductResponse> list() {
        return productService.getList().stream().map(ProductResponse::new).collect(Collectors.toList());
    }

    @PostMapping
    @Secured(WRITE_PRODUCT)
    public ProductResponse save(@RequestBody ProductRequest request) {
        return new ProductResponse(productService.save(request));
    }

    @PutMapping
    @Secured(WRITE_PRODUCT)
    public ProductResponse update(@RequestBody ProductRequest request) {
        return new ProductResponse(productService.save(request));
    }

    @DeleteMapping("/{id}")
    @Secured(WRITE_PRODUCT)
    public void deleteProduct(@PathVariable("id") Long id) {
        productService.delete(id);
    }

    @GetMapping("/testByProduct")
    public List<TestDescriptionResponse> listAllTakenTest(@RequestParam Long id) {
        return productService.getAllTestByProduct(id).stream().map(TestDescriptionResponse::new).collect(Collectors.toList());
    }
}
