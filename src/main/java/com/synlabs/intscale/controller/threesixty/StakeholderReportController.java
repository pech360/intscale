package com.synlabs.intscale.controller.threesixty;

import com.synlabs.intscale.service.threesixty.StakeholderReportService;
import com.synlabs.intscale.service.threesixty.StakeholderTestService;
import com.synlabs.intscale.view.threesixty.StakeholderReportRequest;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;

@RestController
@RequestMapping("/api/stakeholder/")
public class StakeholderReportController
{
  @Autowired
  private StakeholderReportService service;


  @PostMapping("/report/")
  public void report(@RequestBody StakeholderReportRequest request, HttpServletResponse response) throws IOException
  {
    response.setContentType("application/octet-stream");
    String fileName = "StakeholderReport";
    response.setHeader("Content-disposition", String.format("attachment; filename=%s.csv",fileName));
    response.setHeader("fileName", String.format("%s.csv",fileName));
    Reader reader = service.generateReport(request);
    PrintWriter writer = response.getWriter();
    IOUtils.copy(reader,writer);
  }
}
