package com.synlabs.intscale.controller.threesixty;

import com.synlabs.intscale.service.threesixty.StakeholderService;
import com.synlabs.intscale.view.UserRequest;
import com.synlabs.intscale.view.UserResponse;
import com.synlabs.intscale.view.UserSummaryResponse;
import com.synlabs.intscale.view.threesixty.StakeholderRequest;
import com.synlabs.intscale.view.threesixty.StakeholderResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.*;

@RestController
@RequestMapping("/api/stakeholder/")
public class StakeholderController
{
  @Autowired
  private StakeholderService stakeholderService;

  @GetMapping("/users/")
  @Secured(READ_STAKEHOLDER)
  public List<UserSummaryResponse> getUsers()
  {
    return stakeholderService.getUsers()
                             .stream().map(UserSummaryResponse::new)
                             .collect(Collectors.toList());
  }

  @GetMapping("/user/{userId}")
  @Secured(READ_STAKEHOLDER)
  public UserResponse getStakeholders(@PathVariable("userId") Long userId)
  {
    return new UserResponse(stakeholderService.getStakeholders(userId), UserResponse.ResponseType.Stakeholder);
  }

  @PostMapping("/student/")
//  TODO :
  @Secured(STUDENT_WRITE_STAKEHOLDER)
  public List<StakeholderResponse> saveStudentStakeholders(@RequestBody List<StakeholderRequest> request)
  {
    return stakeholderService.saveStudentStakeholders(request).stream()
                             .map(StakeholderResponse::new)
                             .collect(Collectors.toList());
  }

  @PostMapping
  @Secured(WRITE_STAKEHOLDER)
  public UserResponse save(@RequestBody UserRequest request)
  {
    return new UserResponse(stakeholderService.save(request), UserResponse.ResponseType.Stakeholder);
  }

  @GetMapping("/send/{stakeholderId}")
  @Secured(SEND_STAKEHOLDER)
  public UserResponse sendStakeholder(@PathVariable("stakeholderId") Long stakeholderId)
  {
    return new UserResponse(stakeholderService.sendStakeholderLink(stakeholderId), UserResponse.ResponseType.Stakeholder);
  }

  @GetMapping("/remind/{stakeholderId}")
  @Secured(SEND_STAKEHOLDER)
  public UserResponse remindStakeholder(@PathVariable("stakeholderId") Long stakeholderId)
  {
    return new UserResponse(stakeholderService.sendStakeholderReminder(stakeholderId), UserResponse.ResponseType.Stakeholder);
  }

  @GetMapping("/verify/{stakeholderId}")
  public void verifyStakeholder(@PathVariable("stakeholderId") Long stakeholderId)
  {
    stakeholderService.sendStakeholderVerification(stakeholderId);
  }
}
