package com.synlabs.intscale.controller.threesixty;

import com.synlabs.intscale.service.threesixty.StakeholderQuestionService;
import com.synlabs.intscale.view.question.QuestionResponse;
import com.synlabs.intscale.view.threesixty.StakeholderQuestionFilterRequest;
import com.synlabs.intscale.view.threesixty.StakeholderQuestionRequest;
import com.synlabs.intscale.view.threesixty.StakeholderQuestionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.READ_STAKEHOLDERQUESTION;
import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.WRITE_STAKEHOLDERQUESTION;

@RestController
@RequestMapping("/api/question/stakeholder/")
public class StakeholderQuestionController
{
  @Autowired
  private StakeholderQuestionService service;

  @RequestMapping("/filter/")
  @Secured(READ_STAKEHOLDERQUESTION)
  public List<StakeholderQuestionResponse> filter(@RequestBody StakeholderQuestionFilterRequest request)
  {
    return service.filter(request).stream().map(StakeholderQuestionResponse::new).collect(Collectors.toList());
  }

  @PostMapping
  @Secured(WRITE_STAKEHOLDERQUESTION)
  public StakeholderQuestionResponse save(@RequestBody StakeholderQuestionRequest request)
  {
    return new StakeholderQuestionResponse(service.save(request));
  }

  @PutMapping
  @Secured(WRITE_STAKEHOLDERQUESTION)
  public StakeholderQuestionResponse update(@RequestBody StakeholderQuestionRequest request)
  {
    return new StakeholderQuestionResponse(service.update(request));
  }

  @PostMapping("/upload/")
  @Secured(WRITE_STAKEHOLDERQUESTION)
  public List<StakeholderQuestionResponse> uploadQuestion(@RequestParam(value = "testId") Long testId,
                                               @RequestParam(value = "file") MultipartFile file) throws IOException
  {
    return service.uploadQuestions(testId, file)
                  .stream().map(StakeholderQuestionResponse::new)
                  .collect(Collectors.toList());
  }
}
