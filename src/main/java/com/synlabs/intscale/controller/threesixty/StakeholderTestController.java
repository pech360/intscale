package com.synlabs.intscale.controller.threesixty;

import com.synlabs.intscale.service.threesixty.StakeholderTestService;
import com.synlabs.intscale.view.threesixty.StakeholderAnswerRequest;
import com.synlabs.intscale.view.threesixty.StakeholderConstructResponse;
import com.synlabs.intscale.view.threesixty.StakeholderQuestionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/stakeholder/")
public class StakeholderTestController
{
  @Autowired
  private StakeholderTestService testService;

  @GetMapping("/questions")
  public List<StakeholderQuestionResponse> listQuestion(@RequestParam Long constructId)
  {
    return testService.getQuestion(constructId).stream()
                      .map(q -> new StakeholderQuestionResponse(q, StakeholderQuestionResponse.ResponseType.Options))
                      .collect(Collectors.toList());
  }

  @GetMapping("/constructors/")
  public List<StakeholderConstructResponse> listConstructors()
  {
    return testService.getConstructs().stream().map(StakeholderConstructResponse::new).collect(Collectors.toList());
  }

  @PostMapping("/answers/")
  public void saveAnswer(@RequestBody List<StakeholderAnswerRequest> requests)
  {
    testService.saveAnswers(requests);
  }

  @GetMapping("/finishTest/")
  public void testFinished(){
    testService.finishTest();
  }
}
