package com.synlabs.intscale.controller;

import com.synlabs.intscale.util.SubscriptionKeyService;
import com.synlabs.intscale.view.SubscriptionRequest;
import com.synlabs.intscale.view.SubscriptionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.WRITE_SUBSCRIPTION_KEY;

/**
 * Created by itrs on 6/21/2017.
 */
@RestController
@RequestMapping("/api/subscriber")
public class SubscriptionController
{
  @Autowired
  private SubscriptionKeyService subscriptionKeyService;

  @PostMapping
  @Secured(WRITE_SUBSCRIPTION_KEY)
  public SubscriptionResponse create(@RequestBody SubscriptionRequest subscriptionRequest)
  {
    return new SubscriptionResponse(subscriptionKeyService.create(subscriptionRequest));
  }
}
