package com.synlabs.intscale.controller;

import com.synlabs.intscale.common.Menu;
import com.synlabs.intscale.service.StudentService;
import com.synlabs.intscale.view.usertest.TenantTestResponse;
import org.apache.poi.util.IOUtils;
import org.joda.time.LocalDateTime;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;

import com.synlabs.intscale.entity.user.Role;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.jpa.UserRepository;
import com.synlabs.intscale.service.UserService;
import com.synlabs.intscale.view.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.print.DocFlavor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.*;

@RestController
@RequestMapping("/api/user/self")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private UserRepository userRepository;

    @PutMapping
    @Secured(SELF_WRITE)
    public UserResponse update(@RequestBody UserRequest request) {
        return new UserResponse(userService.updateUserProfile(request));
    }

    @GetMapping("/userProfile")
    @Secured(SELF_WRITE)
    public UserResponse getUserProfile(@RequestParam Long userId) {
        return new UserResponse(userService.getOneById(userId));
    }

    @PutMapping("/update")
    @Secured(SELF_WRITE)
    public UserResponse updateUser(@RequestBody UserRequest request) {
        return new UserResponse(userService.updateUser(request));
    }

    @DeleteMapping
    @Secured(SELF_WRITE)
    public void deleteUser(@RequestParam Long userId) {
        userService.deleteUser(userId);
    }

    @DeleteMapping("/role")
    @Secured(WRITE_ROLE)
    public void deleteRole(@RequestParam Long roleId) {
        Role role = userService.findOneRoleById(roleId);
        userService.deleteRole(new RoleRequest(roleId));
    }

    @GetMapping("menu")
    @Secured(SELF_READ)
    public Menu getMenu() {
        return userService.getCurrentUserMenu();
    }

    @GetMapping("/userList")
    @Secured(READ_USER)
    public CommonResponse getUserList(@RequestParam(required = false) Long roleId,
                                      @RequestParam(required = false) Long tenantId,
                                      @RequestParam(required = false) String schoolName,
                                      @RequestParam(required = false) String query,
                                      @RequestParam(required = false) String fromDate,
                                      @RequestParam(required = false) String toDate,
                                      @RequestParam(required = true) int pageSize,
                                      @RequestParam(required = true) int page

    ) {
        return userService.getUserResponsePaginated(roleId, tenantId, schoolName,query,fromDate, toDate, pageSize, page);
    }

    @GetMapping("/adminTestDetails")
    @Secured(READ_USER)
    public TenantTestResponse getAdminTestDetails() {
        return new TenantTestResponse(userService.getAllowedTestDetails());
    }

    @GetMapping("/tenants")
    @Secured(READ_TENANT)
    public List<TenantResponse> getTenantList() {
        return userService.getTenantList().stream().map(TenantResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/list")
    @Secured(READ_USER)
    public List<UserResponse> list() {
        return userService.userLists();
    }

    @GetMapping("/userInfoList")
    @Secured(READ_USER)
    public List<UserInfoResponse> userInfoList() {
        return userService.getUserListByTenant().stream().map(UserInfoResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/userByTeacher")
    @Secured(READ_USER)
    public List<UserSummaryResponse> listOfUsersByTeacher() {
        return userService.userListByTeacher().stream().map(UserSummaryResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/userByCounsellor")
    @Secured(READ_USER)
    public List<UserSummaryResponse> listOfUsersByCounsellor() {
        return userService.userByCounsellor().stream().map(UserSummaryResponse::new).collect(Collectors.toList());
    }

    @PostMapping
    @Secured(WRITE_USER)
    public UserResponse createUser(@RequestBody @Validated UserRequest request, BindingResult result) {
        if (result.hasErrors()) {
            throw new ValidationException(result.toString());
        }
        return new UserResponse(userService.saveUser(request));
    }

    @PutMapping("/updatePassword")
    @Secured(WRITE_USER)
    public void resetPassword(@RequestBody UserPasswordChangeRequest request) {
        userService.changePassword(request);
    }

    @GetMapping("roles")
    @Secured(READ_ROLE)
    public List<RoleResponse> roles() {
        return userService.getRoles().stream().map(RoleResponse::new).collect(Collectors.toList());
    }

    @PostMapping("role")
    @Secured(WRITE_ROLE)
    public RoleResponse createRole(@RequestBody @Validated RoleRequest request, BindingResult result) {
        if (result.hasErrors()) {
            throw new ValidationException(result.toString());
        }
        return new RoleResponse(userService.saveRole(request));
    }

    @GetMapping("privileges")
    @Secured(READ_PRIVILEGE)
    public List<PrivilegeResponse> privileges() {
        return userService.getPrivileges().stream().map(PrivilegeResponse::new).collect(Collectors.toList());
    }

    @PutMapping("role")
    @Secured(WRITE_ROLE)
    public RoleResponse updateRole(@RequestBody @Valid RoleRequest request, BindingResult result) {
        if (result.hasErrors()) {
            throw new ValidationException(result.toString());
        }
        return new RoleResponse(userService.updateRole(request));
    }

    @PostMapping("/import/{type}")
    @Secured(WRITE_USER)
    public ResponseEntity<Map<String, String>> importRecords(@RequestParam("file") MultipartFile file,
                                                             @RequestParam(defaultValue = "true") String dryrun,
                                                             @PathVariable String type,
                                                             @RequestParam(defaultValue = "false") String rootAccess,
                                                             @RequestParam(required = false) String email,
                                                             @RequestParam(required = false) boolean allowMainAimReport,
                                                             @RequestParam(required = false) boolean allowMiniAimReport) {
        switch (type) {
            case "Student":
                try {
                    Map<String, String> map = studentService.importStudentRecords(file, dryrun);
                    String isSuccess = map.get("error");
                    map.remove("error");
                    if (isSuccess.equalsIgnoreCase("true")) {
                        return new ResponseEntity<>(map, HttpStatus.EXPECTATION_FAILED);
                    }
                    return new ResponseEntity<>(map, HttpStatus.OK);
                } catch (Exception exp) {
                    exp.printStackTrace();
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            case "User":
                try {
                    Map<String, String> map = userService.importRecords(file, dryrun, rootAccess, email,allowMainAimReport,allowMiniAimReport);
                    String isSuccess = map.get("error");
                    map.remove("error");
                    if (isSuccess.equalsIgnoreCase("true")) {
                        return new ResponseEntity<>(map, HttpStatus.EXPECTATION_FAILED);
                    }
                    return new ResponseEntity<>(map, HttpStatus.OK);
                } catch (Exception exp) {
                    exp.printStackTrace();
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            case "UserTestAssignment":
                try {
                    Map<String, String> map = userService.userTestAssignment(file, dryrun, email);
                    return new ResponseEntity<>(map, HttpStatus.OK);
                } catch (Exception exp) {
                    exp.printStackTrace();
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            default:
                throw new ValidationException("undefined upload type");
        }
    }

    @GetMapping("/export/{type}")
    @Secured(WRITE_USER)
    public void exportExampleFile(@PathVariable String type, HttpServletResponse response) throws IOException {
        File file;
        switch (type) {
            case "User":
                file = userService.exportExampleFile(type);
                break;
            default:
                throw new ValidationException("undefined upload type");
        }
        String filename = "user_signup_sheet.xlsx";
        response.setContentType("text/csv");
        response.setHeader("Content-disposition", String.format("attachment; filename = %s", filename));
        response.setHeader("fileName", filename);
        FileInputStream is = new FileInputStream(file);
        OutputStream out = response.getOutputStream();
        IOUtils.copy(is, out);
        out.flush();
        is.close();
        if (!file.delete()) {
            throw new IOException("Could not delete temporary file after processing: " + file);
        }


    }

    @GetMapping("/downloadUserList")
    @Secured(READ_USER)
    public void downloadUserList(@RequestParam(required = false) Long roleId, @RequestParam(required = false) Long tenantId, HttpServletResponse response) throws IOException {
        String filename = userService.downloadUserList(roleId, tenantId);
        File file = new File(filename);
        filename = "users.csv";
        response.setContentType("text/csv");
        response.setHeader("Content-disposition", String.format("attachment; filename = %s", filename));
        response.setHeader("fileName", filename);
        FileInputStream is = new FileInputStream(file);
        OutputStream out = response.getOutputStream();
        IOUtils.copy(is, out);
        out.flush();
        is.close();
        if (!file.delete()) {
            throw new IOException("Could not delete temporary file after processing: " + file);
        }
    }

    @GetMapping("/getUserByGrade/{grade}")
    @Secured(READ_USER)
    public List<UserSummaryResponse> getUserByGrade(@PathVariable("grade") String grade) {
        return userService.getUserByGrade(grade).stream().map(UserSummaryResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/getUserBySchoolAndGrade")
    @Secured(READ_USER)
    public List<UserSummaryResponse> getUserBySchoolAndGrade(@RequestParam(required = false) String school, @RequestParam(required = false) String grade) {
        return userService.getUserBySchoolAndGrade(school, grade).stream().map(UserSummaryResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/getUserByGradeAndSection/{grade}/{section}")
    @Secured(READ_USER)
    public List<StudentResponse> getUserByGradeAndSection(@PathVariable("grade") String grade, @PathVariable("section") String section) {
        return userService.getUserByGradeAndSection(grade, section).stream().map(StudentResponse::new).collect(Collectors.toList());
    }

    @PutMapping("/saveEmail")
    @Secured(TAKE_TEST)
    public void updateEmailIdOfUser(@RequestBody String email) {
        userService.updateEmailIdOfUser(email);
    }

    @PutMapping("/selfUpdatePassword")
    @Secured(TAKE_TEST)
    public void selfUpdatePassword(@RequestBody String password) {
        userService.selfUpdatePassword(password);
    }

    @GetMapping("/listAllStudents")
    @Secured(READ_STUDENT)
    public List<UserResponseForCounsellor> listAllStudent() {
        return userService.findAll().stream().map(UserResponseForCounsellor::new).collect(Collectors.toList());
    }

    @GetMapping("/filteredUserList")
    @Secured(READ_STUDENT_GRADE)
    public List<UserResponseForCounsellor> listByFilter(@RequestParam(required = false) String schoolId, @RequestParam(required = false) String gradeId,
                                                        @RequestParam(required = false) Long counsellorId) {
        return userService.findAllByParams(schoolId, gradeId, counsellorId).stream().map(UserResponseForCounsellor::new).collect(Collectors.toList());
    }

    @GetMapping("/filter/name")
    @Secured(READ_STUDENT_GRADE)
    public List<UserResponseForCounsellor> listByFilter(@RequestParam String fullName) {
        return userService.findByFullName(fullName).stream().map(UserResponseForCounsellor::new).collect(Collectors.toList());
    }

    @PutMapping("/updateSchoolName")
    @Secured(WRITE_MASTERDATA)
    public int updateSchoolNameOfUsers(@RequestParam String oldName, @RequestParam String newName) {
        return userService.updateSchoolNameOfUsers(oldName, newName);
    }

    @PutMapping("/updateCity")
    @Secured(WRITE_MASTERDATA)
    public int updateCityOfUsers(@RequestParam String oldName, @RequestParam String newName) {
        return userService.updateCityOfUsers(oldName, newName);
    }

    @PostMapping("/download/userProfile")
    @Secured(READ_COMMON_DASHBOARD)
    public void downloadUserProfileInSheet(@RequestBody DashboardRequest request, HttpServletResponse response) throws IOException {
        String filename = userService.downloadUserProfileInSheet(request);
        File file = new File(filename);
        filename = "Sheet1.csv";
        response.setContentType("text/csv");
        response.setHeader("Content-disposition", String.format("attachment; filename = %s", filename));
        response.setHeader("fileName", filename);
        FileInputStream is = new FileInputStream(file);
        OutputStream out = response.getOutputStream();
        IOUtils.copy(is, out);
        out.flush();
        is.close();
        if (!file.delete()) {
            throw new IOException("Could not delete temporary file after processing: " + file);
        }
    }
    
}