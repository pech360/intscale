package com.synlabs.intscale.controller;

import com.synlabs.intscale.service.StudentService;
import com.synlabs.intscale.view.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.*;

/**
 * Created by India on 1/4/2018.
 */
@RestController("studentController")
@RequestMapping("/api/student")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @GetMapping
    @Secured(READ_STUDENT)
    public List<StudentResponse> list() {
        return studentService.findAll().stream().map(StudentResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/listByCounsellor")
    @Secured(READ_COUNSELLOR_STUDENT)
    public List<UserResponseForCounsellor> listByCounsellor() {
        return studentService.findAllByCounsellor();
    }

    @GetMapping("/filterList")
    @Secured(READ_STUDENT_GRADE)
    public List<StudentResponse> listByFilter(@RequestParam(required = false) Long schoolId, @RequestParam(required = false) String gradeId, @RequestParam(required = false) Long counsellorId) {
        return studentService.findAllByFilter(schoolId, gradeId, counsellorId).stream().map(StudentResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/filteredListByCounsellor")
    @Secured(READ_STUDENT)
    public List<UserResponseForCounsellor> listByFilterForCounsellor(@RequestParam(required = false) String schoolId, @RequestParam(required = false) String gradeId, @RequestParam(required = false) String city) {
        return studentService.findAllFilteredDataByCounsellor(schoolId, gradeId, city).stream().map(UserResponseForCounsellor::new).collect(Collectors.toList());
    }

    @PostMapping
    @Secured(WRITE_STUDENT)
    public StudentResponse create(@RequestBody StudentRequest request) {
        return new StudentResponse(studentService.save(request));
    }

    @PutMapping
    @Secured(WRITE_STUDENT)
    public StudentResponse update(@RequestBody StudentRequest request) {
        return new StudentResponse(studentService.update(request));
    }

    @PutMapping("/assignGrade")
    @Secured(WRITE_STUDENT_GRADE)
    public boolean assignGradeToStudents(@RequestBody StudentGradeRequest request) {
        return studentService.assignGradeToStudents(request);
    }

    @PutMapping("/assignCounsellor")
    @Secured(WRITE_COUNSELLOR_STUDENT)
    public boolean assignCounsellorToStudents(@RequestBody StudentCounsellorRequest request) {
        return studentService.assignCounsellorToStudents(request);
    }

    @PutMapping("/removeAll")
    @Secured(WRITE_STUDENT_GRADE)
    public boolean removeGradeToStudents(@RequestBody StudentGradeRequest request) {
        return studentService.removeGradeToStudents(request);
    }

    @PutMapping("/removeCounsellors")
    @Secured(WRITE_COUNSELLOR_STUDENT)
    public boolean removeCounsellorsToStudents(@RequestBody StudentCounsellorRequest request) {
        return studentService.removeCounsellorsToStudents(request);
    }

    @PutMapping("/updateGrade")
    @Secured(WRITE_STUDENT_GRADE)
    public boolean updateGradeToStudents(@RequestParam Long id, @RequestParam String gradeId, @RequestParam String sectionId) {
        return studentService.updateGradeToStudents(id, gradeId, sectionId);
    }

    @DeleteMapping
    @Secured(WRITE_STUDENT)
    public void delete(@RequestParam Long id) {
        studentService.delete(id);
    }

    //Student notes by counsellor
    @GetMapping("/notes")
    @Secured(READ_STUDENT_NOTES)
    public List<StudentNotesResponse> studentNotest(@RequestParam Long id) {
        return studentService.findAllByStudentId(id).stream().map(StudentNotesResponse::new).collect(Collectors.toList());
    }

    @PostMapping("/saveandpushNotes")
    @Secured(WRITE_STUDENT_NOTES)
    public StudentNotesResponse saveAndPushStudentNotest(@RequestBody StudentNotesRequest request) {
        return new StudentNotesResponse(studentService.saveAndPushNotes(request));
    }

    @PostMapping("/saveNotes")
    @Secured(WRITE_STUDENT_NOTES)
    public StudentNotesResponse saveNotes(@RequestBody StudentNotesRequest request) {
        return new StudentNotesResponse(studentService.saveNotes(request));
    }

    @PostMapping("/pushNotes")
    @Secured(WRITE_STUDENT_NOTES)
    public StudentNotesResponse pushNotes(@RequestBody StudentNotesRequest request) {
        return new StudentNotesResponse(studentService.pushNotes(request));
    }

    @PutMapping("/updateNotes")
    @Secured(WRITE_STUDENT_NOTES)
    public StudentNotesResponse updateNotes(@RequestBody StudentNotesRequest request) {
        return new StudentNotesResponse(studentService.updateNotes(request));
    }

    @PutMapping("/pushUpdateNotes")
    @Secured(WRITE_STUDENT_NOTES)
    public StudentNotesResponse pushUpdateNotes(@RequestBody StudentNotesRequest request) {
        return new StudentNotesResponse(studentService.pushUpdateNotes(request));
    }
}
