package com.synlabs.intscale.controller;

import com.synlabs.intscale.service.PilotService;
import com.synlabs.intscale.view.PilotRequest;
import com.synlabs.intscale.view.PilotResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.READ_PILOT;
import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.WRITE_PILOT;

/**
 * Created by India on 1/16/2018.
 */
@RestController("PilotController")
@RequestMapping("/api/pilot")
public class PilotController {
    @Autowired
    private PilotService pilotService;

    @GetMapping
    @Secured(READ_PILOT)
    public List<PilotResponse> list(){
        return pilotService.findAll().stream().map(PilotResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/oneByActive")
    @Secured(READ_PILOT)
    public PilotResponse oneByActive(){
        return new PilotResponse(pilotService.findOneByActive(true));
    }

    @PostMapping
    @Secured(WRITE_PILOT)
    public PilotResponse create(@RequestBody PilotRequest request){
        return new PilotResponse(pilotService.save(request));
    }

}
