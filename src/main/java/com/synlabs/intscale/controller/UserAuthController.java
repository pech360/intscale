package com.synlabs.intscale.controller;

import com.synlabs.intscale.common.Menu;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.ex.AuthException;
import com.synlabs.intscale.ex.NotFoundException;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.service.JwtService;
import com.synlabs.intscale.service.TenantService;
import com.synlabs.intscale.service.UserService;
import com.synlabs.intscale.service.threesixty.StakeholderService;
import com.synlabs.intscale.util.DateUtil;
import com.synlabs.intscale.view.*;
import com.synlabs.intscale.view.paymentGateway.TResponse;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.annotation.Secured;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.LOGIN_PANEL;
import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.SELF_READ;
import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.PROMO_CODE;

@RestController
@RequestMapping("/")
public class UserAuthController {

	@Value("${intscale.auth.secretkey}")
	private String secretkey;

	@Value("${intscale.auth.ttl_hours}")
	private int ttlhours;

	@Value("${intscale.s3.assets-url}")
	private String intscale_assets;

	@Autowired
	private UserService userService;

	@Autowired
	private JwtService jwtService;

	@Autowired
	private StakeholderService stakeholderService;
	
	@Autowired
	private TenantService tenantService ; 

	private static Logger logger = LoggerFactory.getLogger(UserAuthController.class);

	@GetMapping("/api/user/me")
	@Secured(SELF_READ)
	public UserSummaryResponse getLoggedInUser() {
		return new UserSummaryResponse(userService.getUser());
	}

	@GetMapping("/api/user/menu")
	@Secured(SELF_READ)
	public Menu getMenu() {
		return userService.getCurrentUserMenu();
	}

	@GetMapping("/api/user/profile")
	@Secured(SELF_READ)
	public UserResponse getUserProfile() {
		return new UserResponse(userService.getFreshUserFromDb());
	}

	@PostMapping("/api/user/logout")
	public void logout() {
		User user = jwtService.getUser();
		if (user != null) {
			jwtService.removeToken(user);
		}
	}

	@PostMapping("api/user/emergencyLogout/{username}")
	@Secured(LOGIN_PANEL)
	public Set<String> emergencyLogout(@PathVariable("username") String username, HttpServletRequest req) {
		String host = req.getHeader("Host").split("\\.")[0];
		logger.info("host name {}" + host);

		User user = userService.getUserByUsernameOrEmail(username, host);
		if (user != null) {
			if (jwtService.isPresentInCache(user.getUsername())) {
				jwtService.removeToken(user);
			} else {
				throw new NotFoundException("User not found");
			}
		}
		return jwtService.getLoggedInUsers();
	}

	@PostMapping("login")
	public LoginResponse login(@RequestBody LoginRequest login, HttpServletRequest req) {
		String host = req.getHeader("Host").split("\\.")[0];
		logger.info("host name {}" + host);
		User user = userService.validate(login.getUsername(), login.getPassword(), host);
		if (user == null) {
			throw new AuthException("Invalid login by user " + login.getUsername());
		}
		if (StringUtils.isEmpty(host) || !host.equalsIgnoreCase(user.getTenant().getSubDomain())) {
			logger.error("Invalid Login : Host {} user {}", host, login.getEmail());
			throw new AuthException("Invalid login");
		}
		Date expiration;
		if ((expiration = jwtService.getExpirationTimeIfLogin(user)) != null) {
			long expirationTime = expiration.getTime() - (new Date().getTime());
			System.out.println(expiration);
			throw new ValidationException(String.format(
					"You are already logged in the device you were using last. You should try"
							+ "using the same device or you can log in here after %d hour/s and %d minute/s."
							+ "Thank you for your patience.",
					(int) (expirationTime / 3600000), (int) (expirationTime % 3600000) / 60000));
		}
		String roleName = "NonAdmin";
		if (user.getRoles().contains(userService.getOneRoleByName("Admin"))) {
			roleName = "Admin";
		}
		String authToken = Jwts.builder().setSubject(user.getUsername()).claim("assets_url", intscale_assets)
				.setIssuedAt(new Date()).setExpiration(getAuthExpiration())
				.claim("dashboardType", user.getDashBoardType()).claim("userType", roleName).claim("hostName", host)
				.signWith(SignatureAlgorithm.HS512, secretkey).compact();

		// if(user.getFtlCount()==null) {
		return new LoginResponse(authToken);
//        }else if(user.getFtlCount()==1) {
//        	LoginResponse loginResponse =new LoginResponse(authToken,user.getFtlCount());
//        	user.setFtlCount(2);
//        	userService.save(user);
//        	return loginResponse;
//        }else {
//        	return new LoginResponse(authToken);
//        }
	}

	@GetMapping("/login/stakeholder/{loginKey}")
	public LoginResponse login(@PathVariable("loginKey") String loginKey) {
		String token = stakeholderService.getStakeholderToken(loginKey);
		return new LoginResponse(token);
	}

	@PostMapping("socialLogin")
	public LoginResponse socialLogin(@RequestBody SocialLoginRequest login, HttpServletRequest req) {
		String host = req.getHeader("Host").split("\\.")[0];
		logger.info(login.getEmail() + " tried to login with host name :" + host);
		User user = userService.validate(login, host);
		if (jwtService.isAllowedToLogin(user)) {
			throw new ValidationException("User is already logged in.");
		}
		return checkAndPrepare(login.getEmail(), user, host);
	}

	private LoginResponse checkAndPrepare(String email, User user, String host) {
		if (user == null) {
			throw new AuthException("Invalid login by user " + email);
		}

		String roleName = "NonAdmin";
		if (user.getRoles().contains(userService.getOneRoleByName("Admin"))) {
			roleName = "Admin";
		}

		String authToken = Jwts.builder().setSubject(user.getUsername()).claim("assets_url", intscale_assets)
				.setIssuedAt(new Date()).setExpiration(getAuthExpiration())
				.claim("dashboardType", user.getDashBoardType()).claim("userType", roleName).claim("hostName", host)
				.signWith(SignatureAlgorithm.HS512, secretkey).compact();

		return new LoginResponse(authToken);
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public LoginResponse register(@RequestBody RegisterRequest request, HttpServletRequest req) {
		String host = req.getHeader("Host").split("\\.")[0];
		logger.info("host name {}" + host);
		User user = userService.register(request, host);
		return checkAndPrepare(request.getEmail(), user, host);
	}

	@RequestMapping(value = "/resentOTP", method = RequestMethod.POST)
	public LoginResponse resentOTP(@RequestParam("username") String username, HttpServletRequest req) {
		String host = req.getHeader("Host").split("\\.")[0];
		logger.info("host name {}" + host);
		User user = userService.resentOTP(username, host);
		return checkAndPrepare(user.getEmail(), user, host);
	}

	@RequestMapping(value = "/otpVerify", method = RequestMethod.POST)
	public LoginResponse otpVerify(@RequestParam("username") String username,
			@RequestParam("pkgAmount") String pkgAmount, @RequestParam("otp") String otp, HttpServletRequest req) {
		String host = req.getHeader("Host").split("\\.")[0];
		User user = userService.otpVerify(username, pkgAmount, otp, host);
		if (user != null)
			return checkAndPrepare(user.getEmail(), user, host);
		else
			throw new AuthException("Invalid login by user");
	}

	@RequestMapping(value = "getpayament", method = RequestMethod.POST)
	public TResponse getTransaction(@RequestParam("payId") String payId, HttpServletRequest req) {
		String host = req.getHeader("Host").split("\\.")[0];
		if (payId.equals("") || payId == null) {
			return new TResponse("Bad Request", "PayId Not Found");
		}
		return userService.getTransaction(payId, host);
	}

	@PostMapping("socialRegister")
	public LoginResponse socialRegister(@RequestBody SocialLoginRequest login, HttpServletRequest req) {
		String host = req.getHeader("Host").split("\\.")[0];
		User user = userService.register(login, host);
		return checkAndPrepare(login.getEmail(), user, host);
	}

	private Date getAuthExpiration() {
		return new DateTime().plusHours(ttlhours).toDate();
	}

	@PostMapping("emailResetPassword")
	public boolean sendEmail(@RequestBody String username, HttpServletRequest req) {
		String host = req.getHeader("Host").split("\\.")[0];
		userService.generateToken(username, host);
		return true;
	}

	@PostMapping("resetPassword")
	public void resetPassword(@RequestBody PasswordResetRequest request, HttpServletRequest req) {
		String host = req.getHeader("Host").split("\\.")[0];
		userService.resetPassword(request, host);
	}

	@GetMapping("verify/stakeholder/{loginKey}")
	public void verifyStakeholder(@PathVariable("loginKey") String loginKey) {
		stakeholderService.verify(loginKey);
	}

	@GetMapping("api/loggedInUsers")
	@Secured(LOGIN_PANEL)
	public Set<String> getLoggedInUsers() {
		return jwtService.getLoggedInUsers();
	}

	  // Api to get domains or subDomains
    @GetMapping("domains")
    public Set<String> getDomains(){
    	return tenantService.getDomains();
    }
}
