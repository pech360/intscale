package com.synlabs.intscale.controller;

import com.synlabs.intscale.auth.IntScaleAuth;
import com.synlabs.intscale.service.TenantService;
import com.synlabs.intscale.view.TenantRequest;
import com.synlabs.intscale.view.TenantResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.*;

/**
 * Created by India on 12/27/2017.
 */
@RestController
@RequestMapping("/api/tenant")
public class TenantController {
    @Autowired
    private TenantService tenantService;

    @GetMapping
    @Secured(READ_TENANT_MANAGEMENT)
    public List<TenantResponse> list() {
        return tenantService.findAll().stream().map(TenantResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/getTenants")
    @Secured(READ_TENANT_MANAGEMENT)
    public List<TenantResponse> listExceptCurrent() {
        return tenantService.findAll().stream().map(TenantResponse::new).collect(Collectors.toList());
    }

    @PostMapping
    @Secured(WRITE_TENANT_MANAGEMENT)
    public TenantResponse create(@RequestBody TenantRequest request) {
        return new TenantResponse(tenantService.save(request));
    }

    @PutMapping
    @Secured(WRITE_TENANT_MANAGEMENT)
    public TenantResponse update(@RequestBody TenantRequest request) {
        return new TenantResponse(tenantService.update(request));
    }

    @DeleteMapping
    @Secured(WRITE_TENANT_MANAGEMENT)
    public void delete(@RequestParam Long id) {
        tenantService.delete(id);
    }

}
