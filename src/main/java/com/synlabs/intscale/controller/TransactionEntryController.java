package com.synlabs.intscale.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.synlabs.intscale.service.TransactionEntryService;
import com.synlabs.intscale.view.paymentGateway.TransactionEntryView;
import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.TRANSACTION_READ_PRIVILEGE ;
//LNT
@RestController
@RequestMapping("/api/")
public class TransactionEntryController {
	@Autowired
	private TransactionEntryService transactionEntryService;
	
	@GetMapping(value="transaction") 
	@Secured(TRANSACTION_READ_PRIVILEGE)
	 public List<TransactionEntryView> getAllTransactions(){
		return transactionEntryService.getAllTransactions();
		}

}
