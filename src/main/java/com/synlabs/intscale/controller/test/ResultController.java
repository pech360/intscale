package com.synlabs.intscale.controller.test;

import com.synlabs.intscale.service.ResultService;
import com.synlabs.intscale.view.DashboardRequest;
import com.synlabs.intscale.view.TestDescriptionResponse;
import com.synlabs.intscale.view.TestTreeResponse.TestSummaryResponse;
import com.synlabs.intscale.view.leap.TestResultSummaryResponse;
import com.synlabs.intscale.view.usertest.GradeResultResponse;
import com.synlabs.intscale.view.usertest.ResultRequest;
import com.synlabs.intscale.view.usertest.ResultResponse;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.*;

@RestController
@RequestMapping("/api/")
public class ResultController
{

  @Autowired
  private ResultService resultService;

  @GetMapping("result/{id}")
  @Secured(READ_RESULT)
  public ResultResponse get(@PathVariable(name = "id") Long id)
  {
    return new ResultResponse(resultService.get(id));
  }

  @GetMapping("result/{username}/{testId}")
  @Secured(READ_RESULT)
  public ResultResponse getResult(@PathVariable(name = "username") String username, @PathVariable(name = "testId") Long testId)
  {
    return new ResultResponse(resultService.get(username, testId));
  }

  @GetMapping("resultByUserAndTest/{userId}/{testId}")
  @Secured(READ_RESULT)
  public ResultResponse getResultByUserIdAndTestId(@PathVariable("userId") Long userId, @PathVariable("testId") Long testId)
  {
    return resultService.getResultByUserIdAndTestId(userId, testId);
  }

  @GetMapping("result/test")
  @Secured(READ_RESULT)
  public ResultResponse getByCurrentUserAndTestId(@RequestParam(required = false) Long testId, @RequestParam(required = false) Long userId)
  {
    return resultService.getByCurrentUserAndTestId(testId, userId);
  }

  @PostMapping("result")
  @Secured(TAKE_TEST)
  public void saveResult(@RequestBody ResultRequest request)
  {
    resultService.saveResult(request);
  }

  @GetMapping("getResultByGradeAndTest/{grade}/{testId}")
  @Secured(READ_RESULT)
  public void getResultByGradeAndTest(@PathVariable("grade") String grade, @PathVariable("testId") Long testId)
  {
    resultService.getResultByGradeAndTest(grade, testId);
  }

  @GetMapping("getTestsTakenByUser/{id}")
  @Secured(READ_RESULT)
  public List<ResultResponse> getTestsTakenByUser(@PathVariable("id") Long userId)
  {
    return resultService.getTestsTakenByUser(userId);
  }

  @GetMapping("getAverageMarksOfProduct/{productId}/{constructId}/{grade}/{section}/{studentId}")
  @Secured(READ_SCHOOL_DASHBOARD)
  public HashMap<Long, List> getAverageMarksOfProductByGrade(@PathVariable("productId") Long productId, @PathVariable("constructId") Long constructId,
                                                             @PathVariable("grade") String grade, @PathVariable("section") String section,
                                                             @PathVariable("studentId") Long studentId)
  {
    return resultService.getAverageMarksOfProductByGrade(productId, constructId, grade, studentId, section);
  }

  @GetMapping("result/getTestNames")
  @Secured(READ_PRODUCT)
  public Map<Long, TestResultSummaryResponse> getTestIdWithNames()
  {
    return resultService.getTestIdWithNames();
  }

  @PostMapping("result/syncTestResult")
  @Secured(READ_TENANT)
  public void syncTestResult()
  {
    resultService.syncTestResult();
  }

  @PostMapping("result/import/syncUserTestResponse")
  @Secured(READ_TENANT)
  public ResponseEntity<Map<String, String>> importTestResponsesFromSheet(@RequestParam("file") MultipartFile file, @RequestParam(defaultValue = "true") String dryrun)
  {
    try
    {
      Map<String, String> map = resultService.importTestResponsesFromSheet(file, dryrun);
      String isSuccess = map.get("error");
      map.remove("error");
      if (isSuccess.equalsIgnoreCase("true"))
      {
        return new ResponseEntity<>(map, HttpStatus.EXPECTATION_FAILED);
      }
      return new ResponseEntity<>(map, HttpStatus.OK);
    }
    catch (Exception exp)
    {
      exp.printStackTrace();
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/result/download/syncUserTestResponse")
  @Secured(READ_COMMON_DASHBOARD)
  public void downloadTestResponsesInSheet(@RequestBody DashboardRequest request,HttpServletResponse response)throws IOException
  {
    String filename = resultService.downloadTestResponsesInSheet(request);
    File file = new File(filename);
    filename = "Sheet1.csv";
    response.setContentType("text/csv");
    response.setHeader("Content-disposition", String.format("attachment; filename = %s", filename));
    response.setHeader("fileName", filename);
    FileInputStream is = new FileInputStream(file);
    OutputStream out = response.getOutputStream();
    IOUtils.copy(is, out);
    out.flush();
    is.close();
    if (!file.delete())
    {
      throw new IOException("Could not delete temporary file after processing: " + file);
    }
  }
}
