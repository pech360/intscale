package com.synlabs.intscale.controller.test;

import com.synlabs.intscale.entity.test.QuestionTag;
import com.synlabs.intscale.enums.TagType;
import com.synlabs.intscale.service.QuestionTagService;
import com.synlabs.intscale.view.question.QuestionTagRequest;
import com.synlabs.intscale.view.question.QuestionTagResponse;
import com.synlabs.intscale.view.question.SubTagRequest;
import com.synlabs.intscale.view.question.SubTagResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/")
public class QuestionTagController
{
  @Autowired
  private QuestionTagService questionTagService;

  @GetMapping("tag")
  public List<QuestionTagResponse> list(){
    return questionTagService.all().stream().map(QuestionTagResponse::new).collect(Collectors.toList());
  }

  @PostMapping("tag")
  public QuestionTagResponse save(@RequestBody QuestionTagRequest request){
    return new QuestionTagResponse(questionTagService.saveTag(request));
  }

  @PutMapping("tag")
  public QuestionTagResponse update(@RequestBody QuestionTagRequest request){
    return new QuestionTagResponse(questionTagService.updateTag(request));
  }


  @GetMapping("subtag")
  public List<SubTagResponse> listSubTags(){
    return questionTagService.allSubTag().stream().map(SubTagResponse::new).collect(Collectors.toList());
  }

  @PostMapping("subtag")
  public SubTagResponse save(@RequestBody SubTagRequest request){
    return new SubTagResponse(questionTagService.saveSubTag(request));
  }

  @PutMapping("subtag")
  public SubTagResponse updateSubTag(@RequestBody SubTagRequest request){
    return new SubTagResponse(questionTagService.updateSubTag(request));
  }

  @GetMapping("tagByType")
  public List<QuestionTagResponse> listTagsByType(@RequestParam TagType type){
    return questionTagService.listTagsByType(type).stream().map(QuestionTagResponse::new).collect(Collectors.toList());
  }

  @GetMapping("subtagByType")
  public List<SubTagResponse> listSubTagsByType(@RequestParam TagType type){
    return questionTagService.listSubTagsByType(type).stream().map(SubTagResponse::new).collect(Collectors.toList());
  }

}
