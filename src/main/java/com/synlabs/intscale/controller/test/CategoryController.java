package com.synlabs.intscale.controller.test;

import com.synlabs.intscale.entity.Report.Category;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.service.CategoryService;
import com.synlabs.intscale.view.FeedbackRequest;
import com.synlabs.intscale.view.FeedbackResponse;
import com.synlabs.intscale.view.report.CategoryRequest;
import com.synlabs.intscale.view.report.CategoryResponse;
import com.synlabs.intscale.view.report.ScoreRangeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.*;

/**
 * Created by itrs on 7/25/2017.
 */
@RestController("CategoryController")
@RequestMapping("/api/category")
public class CategoryController
{
  @Autowired
  CategoryService categoryService;

  @GetMapping
  @Secured(READ_CATEGORY)
  public List<CategoryResponse> list()
  {
    return categoryService.getCategory().stream().map(CategoryResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/parent")
  @Secured(READ_CATEGORY)
  public List<CategoryResponse> listParent()
  {
    return categoryService.getParentCategory().stream()
                          .map(CategoryResponse::new)
                          .collect(Collectors.toList());
  }

  @GetMapping("/parentWithSubCategory")
  @Secured(READ_CATEGORY)
  public List<com.synlabs.intscale.view.CategoryResponse> listParentWithSubCategory()
  {
    return categoryService.getParentCategory().stream()
                          .map(c -> new com.synlabs.intscale.view.CategoryResponse(c, com.synlabs.intscale.view.CategoryResponse.ResponseType.Full))
                          .collect(Collectors.toList());
  }

  @GetMapping("/child")
  @Secured(READ_CATEGORY)
  public List<CategoryResponse> listChild()
  {
    return categoryService.getChildCategory().stream().map(CategoryResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/childByParent")
  @Secured(READ_CATEGORY)
  public List<CategoryResponse> listChildByParent(@RequestParam Long parentId)
  {
    return categoryService.getChildCategoryByParent(parentId).stream().map(CategoryResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/loadByTest")
  @Secured(READ_CATEGORY)
  public List<CategoryResponse> loadCategoryByTest(@RequestParam Long testId)
  {
    return categoryService.getCategoryByTest(testId).stream().map(CategoryResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/loadSubCategoryByTest")
  @Secured(READ_CATEGORY)
  public List<CategoryResponse> loadSubCategoryByTest(@RequestParam Long testId)
  {
    return categoryService.getSubCategoryByTest(testId).stream().map(CategoryResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/childByParentName")
  @Secured(READ_CATEGORY)
  public List<CategoryResponse> listChildByParentName(@RequestParam String parentName)
  {
    return categoryService.getChildCategoryByParentName(parentName).stream().map(CategoryResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/findOne")
  @Secured(READ_CATEGORY)
  public CategoryResponse findOne(@RequestParam Long id)
  {
    return new CategoryResponse(categoryService.findOneById(id));
  }

  @PostMapping
  @Secured(WRITE_CATEGORY)
  public CategoryResponse createCategory(@RequestBody @Validated CategoryRequest request, BindingResult result)
  {
    if (result.hasErrors())
    {
      throw new ValidationException(result.toString());

    }
    return new CategoryResponse(categoryService.saveCategory(request));
  }

  @DeleteMapping
  @Secured(WRITE_CATEGORY)
  public void deleteCategory(@RequestParam Long categoryId)
  {
    Category category = categoryService.findOneById(categoryId);
    categoryService.deleteCategory(category);
  }

  @PutMapping
  @Secured(WRITE_CATEGORY)
  public CategoryResponse updateCategory(@RequestBody @Validated CategoryRequest request, BindingResult result)
  {
    if (result.hasErrors())
    {
      throw new ValidationException(result.toString());
    }
    return new CategoryResponse(categoryService.updateCategory(request));
  }

  @PutMapping("/feedback")
  @Secured(WRITE_EXPERT_ANAYSIS)
  public FeedbackResponse saveFeedback(@RequestBody @Validated FeedbackRequest request, BindingResult result)
  {
    if (result.hasErrors())
    {
      throw new ValidationException(result.toString());
    }
    return new FeedbackResponse(categoryService.saveFeedback(request));
  }

  @DeleteMapping("/deleteFeedbacks")
  @Secured(WRITE_EXPERT_ANAYSIS)
  public void deleteFeedback(@RequestParam(required = false) Long categoryId, @RequestParam Long id)
  {
    categoryService.deleteFeedback(categoryId, id);
  }

  @PostMapping("/delete/all")
  @Secured(WRITE_CATEGORY)
  public void deleteCategory(@RequestBody @Validated List<Long> request)
  {
    categoryService.deleteCategories(request);
  }

  @PutMapping("/saveCategoryScoreRanges/{id}/{applicableFor}")
  @Secured(WRITE_CATEGORY_SCORE)
  public CategoryResponse saveCategoryScoreRanges(@PathVariable("id") Long id,@PathVariable("applicableFor") String applicableFor, @RequestBody List<ScoreRangeRequest> request)
  {
    return categoryService.saveCategoryScoreRanges(request, id, applicableFor);
  }

  @PutMapping("/saveCategoryStandardScore")
  @Secured(WRITE_CATEGORY_SCORE)
  public CategoryResponse saveCategoryStandardScore(@RequestParam String language, @RequestBody CategoryRequest request)
  {
    return categoryService.saveCategoryStandardScore(request, language);
  }
}
