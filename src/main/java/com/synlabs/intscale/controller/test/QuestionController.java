package com.synlabs.intscale.controller.test;

import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.service.QuestionService;
import com.synlabs.intscale.view.question.QuestionRequest;
import com.synlabs.intscale.view.question.QuestionResponse;
import com.synlabs.intscale.view.question.QuestionSummaryResponse;
import com.synlabs.intscale.view.usertest.GroupTextRequest;
import com.synlabs.intscale.view.usertest.GroupTextResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.READ_QUESTION;
import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.WRITE_QUESTION;

@RestController
@RequestMapping("/api/question")
public class QuestionController
{
  @Autowired
  private QuestionService questionService;

  @GetMapping("/list")
  @Secured(READ_QUESTION)
  public List<QuestionResponse> list()
  {
    return questionService.findAll().stream().map(QuestionResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/listByFilter")
  @Secured(READ_QUESTION)
  public List<QuestionResponse> listByFilter(@RequestParam(required = false) String questionType,@RequestParam(required = false) Long subtagId)
  {
    return questionService.findByCustomSearch(questionType,subtagId).stream().map(QuestionResponse::new).collect(Collectors.toList());
  }

  @PostMapping("/create")
  @Secured(WRITE_QUESTION)
  public QuestionResponse create(@RequestBody @Validated QuestionRequest questionRequest, BindingResult result)
  {
    if (result.hasErrors())
    {
      throw new ValidationException(result.toString());
    }
    return new QuestionResponse(questionService.save(questionRequest));
  }

  @PutMapping("/update")
  @Secured(WRITE_QUESTION)
  public QuestionResponse update(@RequestBody @Validated QuestionRequest questionRequest, BindingResult result)
  {
    if (result.hasErrors())
    {
      throw new ValidationException(result.toString());
    }
    return new QuestionResponse(questionService.save(questionRequest));
  }

  @DeleteMapping("/delete")
  @Secured(WRITE_QUESTION)
  public void deleteQuestion(@RequestParam Long id)
  {
    questionService.deleteQuestion(id);
  }

  @DeleteMapping("/deleteAll")
  @Secured(WRITE_QUESTION)
  public void deleteMultipleQuestion(@RequestParam Long[] arrayOfQuestionId)
  {
    List<Long> questionIds = Arrays.asList(arrayOfQuestionId);
    questionService.deleteMultipleQuestions(questionIds);
  }

  @GetMapping("/byGroupName")
  @Secured(READ_QUESTION)
  public QuestionResponse byGroupName(@RequestParam String groupName)
  {
    return new QuestionResponse(questionService.findOneByGroupName(groupName));
  }

  @PostMapping("/addGroupText")
  @Secured(WRITE_QUESTION)
  public boolean addGroupTextWithQuestion(@RequestBody @Validated GroupTextRequest textRequest, BindingResult result)
  {
    if (result.hasErrors())
    {
      throw new ValidationException(result.toString());
    }
    questionService.saveGroupActivity("",textRequest);
    return true;
  }

  @GetMapping("/getGroupText")
  @Secured(READ_QUESTION)
  public GroupTextResponse getGroupTextActivity(@RequestParam String groupName)
  {
    return new GroupTextResponse(questionService.findOneByGroupText(groupName));
  }

  /*@GetMapping("/byCategory")
  @Secured(READ_QUESTION)
  public List<QuestionSummaryResponse> listByCategory(@RequestParam Long categoryId)
  {
    return questionService.listQuestionsByCategory(categoryId).stream().map(QuestionSummaryResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/bySubCategory")
  @Secured(READ_QUESTION)
  public List<QuestionSummaryResponse> listBySubCategory(@RequestParam Long subCategoryId)
  {
    return questionService.listQuestionsBySubCategory(subCategoryId).stream().map(QuestionSummaryResponse::new).collect(Collectors.toList());
  }*/

  @GetMapping("{id}")
  @Secured(READ_QUESTION)
  public QuestionResponse getOne(@PathVariable Long id)
  {
    return new QuestionResponse(questionService.getOneQuestion(id));
  }
}
