package com.synlabs.intscale.controller;
//LNT
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.synlabs.intscale.entity.PromoCode;
import com.synlabs.intscale.ex.PromocodeSuccess;
import com.synlabs.intscale.service.PromoCodeService;

import com.synlabs.intscale.view.PromoCodeRequest;
import com.synlabs.intscale.view.PromoCodeResponse;
import com.synlabs.intscale.view.PromoCodeUpdateRequest;
import com.synlabs.intscale.view.UserPromoCodeResponse;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.PROMO_CODE;

@RestController
@RequestMapping("api/promoCode")
public class PromoCodeController {

	@Autowired
	private PromoCodeService promoCodeService;

	@GetMapping("all")
	@Secured(PROMO_CODE)
	public List<PromoCodeResponse> getAllAvailableCodes() {
		return promoCodeService.getAllAvailableCodes();
	}
	@GetMapping("all/valid")
	@Secured(PROMO_CODE)
	public List<PromoCodeResponse> getAllValidAvailableCodes() {
		return promoCodeService.getAllValidAvailableCodes();
	}
	@GetMapping
	@Secured(PROMO_CODE)
	public List<PromoCodeResponse> getAllPromoCodeFromDateToDate(@RequestParam String startDate,
			@RequestParam String endDate) {
		return promoCodeService.getAllPromoCodeFromDateToDate(startDate, endDate);
	}
	@GetMapping(value="{promoCodeId}/users")
	public List<UserPromoCodeResponse> getAllUsersOfPromoCode(@PathVariable Long promoCodeId) {	
		 return promoCodeService.getAllUsersOfPromoCode(promoCodeId);
	}
	@GetMapping("{id}")
	@Secured(PROMO_CODE)
	public PromoCodeResponse checkIfValid(@PathVariable Long id) {
		return promoCodeService.checkIfValid(id);
	}
	@PostMapping
	@Secured(PROMO_CODE)
	public PromoCodeResponse createPromoCode(@RequestBody PromoCodeRequest promoCodeRequest) {
		return promoCodeService.createPromoCode(promoCodeRequest);
	}
	@PutMapping("{id}/{isValid}")
	@Secured(PROMO_CODE)
	public PromoCodeResponse updateIsValid(@PathVariable Long id, @PathVariable Boolean isValid) {
		return promoCodeService.updateIsValid(id, isValid);
	}
	@PutMapping("{id}")
	@Secured(PROMO_CODE)
	public PromoCodeResponse update(@RequestBody PromoCodeUpdateRequest promoCodeUpdateRequest, @PathVariable Long id) throws ParseException {
		return promoCodeService.update(promoCodeUpdateRequest, id);
	}
	@PostMapping(value = "apply")
	public PromoCodeResponse applyPromoCode(@RequestParam String name, @RequestParam String productName) {
		return promoCodeService.applyPromoCode(name, productName);
	}

}
