package com.synlabs.intscale.controller;

import com.synlabs.intscale.service.SessionService;
import com.synlabs.intscale.view.SessionRequest;
import com.synlabs.intscale.view.SessionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.*;

/**
 * Created by India on 1/5/2018.
 */
@RestController("sessionController")
@RequestMapping("/api/session")
public class SessionController {
    @Autowired
    private SessionService sessionService;

    @GetMapping
    @Secured(READ_SESSION)
    public List<SessionResponse> list() {
        return sessionService.findAll().stream().map(SessionResponse::new).collect(Collectors.toList());
    }

    @PostMapping
    @Secured(WRITE_SESSION)
    public SessionResponse create(@RequestBody SessionRequest request) {
        return new SessionResponse(sessionService.save(request));
    }

    @PutMapping
    @Secured(WRITE_SESSION)
    public SessionResponse update(@RequestBody SessionRequest request) {
        return new SessionResponse(sessionService.update(request));
    }

    @DeleteMapping
    @Secured(WRITE_SESSION)
    public void delete(@RequestParam Long id) {
        sessionService.delete(id);
    }

}
