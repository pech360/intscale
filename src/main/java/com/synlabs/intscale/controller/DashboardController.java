package com.synlabs.intscale.controller;

import com.synlabs.intscale.enums.ReportStatus;
import com.synlabs.intscale.service.DashboardService;
import com.synlabs.intscale.view.DashboardRequest;
import com.synlabs.intscale.view.DashboardResponse;
import com.synlabs.intscale.view.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.READ_COMMON_DASHBOARD;
import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.WRITE_REPORTS;

@RestController
@RequestMapping("/api/dashboard")
public class DashboardController {

	@Autowired
	private DashboardService dashboardService;

	@GetMapping("/all")
	@Secured(READ_COMMON_DASHBOARD)
	public List<DashboardResponse> getAllDashboard() {
		return dashboardService.getAllDashboards().stream().map(DashboardResponse::new).collect(Collectors.toList());
	}

	@PostMapping("/loadDashboardDataByUserRoles")
	@Secured(READ_COMMON_DASHBOARD)
	public DashboardResponse getDashboard(@RequestBody DashboardRequest request) {
		return dashboardService.getDashboards(request);
	}

	@PostMapping("/getNewUsers")
	@Secured(READ_COMMON_DASHBOARD)
	public List<UserResponse> getNewUsers(@RequestBody DashboardRequest request) {
		return dashboardService.getNewUsers(request).stream().map(UserResponse::new).collect(Collectors.toList());
	}

	@PostMapping("/getNewUsersCount")
	@Secured(READ_COMMON_DASHBOARD)
	public Long getNewUsersCount(@RequestBody DashboardRequest request) {
		return dashboardService.getNewUsersCount(request);
	}

	@PostMapping("/getUsersForAimTestsByStatusCount")
	@Secured(READ_COMMON_DASHBOARD)
	public Long getUsersForAimTestsByStatusCount(@RequestBody DashboardRequest request,
			@RequestParam ReportStatus status) {
		return dashboardService.getUsersForAimTestsByStatusCount(request, status);
	}

	@PostMapping("/getUsersForAimTestsByStatus")
	@Secured(READ_COMMON_DASHBOARD)
	public List<UserResponse> getUsersForAimTestsByStatus(@RequestBody DashboardRequest request,
			@RequestParam ReportStatus status) {
		return dashboardService.getUsersForAimTestsByStatus(request, status).stream().map(UserResponse::new)
				.collect(Collectors.toList());
	}

}
