package com.synlabs.intscale.controller.storage;

import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.service.*;
import com.synlabs.intscale.store.FileStore;
import com.synlabs.intscale.view.UserSummaryResponse;
import com.synlabs.intscale.view.usertest.GroupTextRequest;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.*;

/**
 * Created by itrs on 6/15/2017.
 */
@RestController
@RequestMapping(value = "/api/raw")
public class RawStorageController
{

  private static final Logger logger = LoggerFactory.getLogger(RawStorageController.class);

  @Autowired
  private FileStore store;

  @Autowired
  private UserService userService;

  @Autowired
  private QuestionService questionService;

  @Autowired
  private TestService testService;

  @Autowired
  private SchoolService schoolService;

  @Autowired
  private MasterDataDocumentService masterDataDocumentService;

  @Autowired
  private AimExpertInventoryService aimExpertInventoryService;

  @RequestMapping(value = "/view/{fileType}/{filename:.+}", method = RequestMethod.GET)
  public void getFile(@PathVariable String filename, @PathVariable String fileType,
                      HttpServletResponse response,
                      HttpServletRequest request)
  {

    logger.info("@getFile {} - {} ", filename);
    if (!filename.equals("null"))
    {
      try
      {
        final InputStream inputStream = store.getStream(fileType, filename);
        IOUtils.copy(inputStream, response.getOutputStream());
        response.flushBuffer();
      }
      catch (IOException e)
      {
        logger.error("@getFile > error with request for img: " + filename, e);
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
      }
    }
  }

  @ResponseBody
  @RequestMapping(value = "/{fileType}/{id}", method = RequestMethod.POST)
  @Secured(SELF_WRITE)
  public void uploadimage(@PathVariable String fileType, @PathVariable(required = false) Long id, @RequestParam("file") MultipartFile file)
  {
    if (file.isEmpty())
    {
      throw new ValidationException("Missing file");
    }

    try
    {
      String fileExtn = file.getOriginalFilename().split("\\.")[1];
      String filename = UUID.randomUUID().toString() + "." + fileExtn;
      switch (fileType)
      {
        case "PROFILEIMAGE":
          store.store("profileimage", filename, file.getBytes());
          userService.attachProfileImage(filename);
          break;
        case "QUESTIONIMAGE":
          store.store("questionimage", filename, file.getBytes());
          questionService.attachQuestionImage(filename, id);
          break;
        case "QUESTIONIVIDEO":
          store.store("questionvideo", filename, file.getBytes());
          questionService.attachQuestionVideo(filename, id);
          break;
        case "QUESTIONAUDIO":
          store.store("questionaudio", filename, file.getBytes());
          questionService.attachQuestionAudio(filename, id);
          break;
      }
    }
    catch (IOException e)
    {
      logger.error("@uploadimage", e);
      throw new ValidationException("Unknown error!");
    }
  }

  @ResponseBody
  @RequestMapping(value = "/test/image", method = RequestMethod.POST)
  @Secured(WRITE_TEST)
  public void uploadTestImage(@RequestParam("file") MultipartFile file,
                              @RequestParam("testId") Long testId,@RequestParam("stakeholderImage") boolean stakeholderImage)
  {

    if (file.isEmpty())
    {
      throw new ValidationException("Missing file");
    }

    try
    {
      String fileExtn = file.getOriginalFilename().split("\\.")[1];
      String filename = UUID.randomUUID().toString() + "." + fileExtn;
      store.store("profileimage", filename, file.getBytes());
      testService.attachTestImage(filename, testId,stakeholderImage);
    }
    catch (IOException e)
    {
      logger.error("@uploadimage", e);
      throw new ValidationException("Unknown error!");
//            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }
  }

  @ResponseBody
  @RequestMapping(value = "/interest/image", method = RequestMethod.POST)
  @Secured(WRITE_MEDIA)
  public void uploadInterestImage(@RequestParam("file") MultipartFile file,
                              @RequestParam("interestId") Long interestId)
  {

    if (file.isEmpty())
    {
      throw new ValidationException("Missing file");
    }

    try
    {
      String fileExtn = file.getOriginalFilename().split("\\.")[1];
      String filename = UUID.randomUUID().toString() + "." + fileExtn;
      store.store("profileimage", filename, file.getBytes());
      masterDataDocumentService.attachInterestImage(filename, interestId);
    }
    catch (IOException e)
    {
      logger.error("@uploadimage", e);
      throw new ValidationException("Unknown error!");
//            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }
  }

  @PostMapping("/test/video/")
  @Secured(WRITE_TEST)
  public void uploadTestVideo(@RequestParam("file") MultipartFile file, @RequestParam("id") Long id,@RequestParam("stakeholderVideo") boolean stakeholderVideo)
  {

    if (file.isEmpty())
    {
      throw new ValidationException("Missing file");
    }

    try
    {
      String fileExtn = file.getOriginalFilename().split("\\.")[1];
      String filename = UUID.randomUUID().toString() + "." + fileExtn;
      store.store("profileimage", filename, file.getBytes());
      testService.attachTestVideo(filename, id,stakeholderVideo);

    }
    catch (IOException e)
    {
      logger.error("@uploadimage", e);
      throw new ValidationException("Unknown error!");
//            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }
  }

  @ResponseBody
  @RequestMapping(value = "/school/image", method = RequestMethod.POST)
  @Secured(WRITE_SCHOOL)
  public void uploadSchoolImage(@RequestParam("file") MultipartFile file,
                                @RequestParam("schoolId") Long schoolId)
  {

    if (file.isEmpty())
    {
      throw new ValidationException("Missing file");
    }

    try
    {
      String fileExtn = file.getOriginalFilename().split("\\.")[1];
      String filename = UUID.randomUUID().toString() + "." + fileExtn;
      store.store("profileimage", filename, file.getBytes());

      schoolService.attachTestImage(filename, schoolId);

    }
    catch (IOException e)
    {
      logger.error("@uploadimage", e);
      throw new ValidationException("Unknown error!");
//            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }
  }

  @ResponseBody
  @RequestMapping(value = "/answerMedia/{fileType}/{id}", method = RequestMethod.POST)
  @Secured(WRITE_QUESTION)
  public void uploadAnswerMedia(@PathVariable String fileType, @RequestParam("files") MultipartFile[] files, @PathVariable(required = false) Long id)
  {
    List<MultipartFile> fileList = Arrays.asList(files);
    if (fileList.isEmpty())
    {
      throw new ValidationException("Missing file");
    }

    switch (fileType)
    {
      case "ANSWERIMAGE":
        List<String> imageFileNames = new ArrayList<>();
        for (MultipartFile file : fileList)
        {
          String fileExtn = file.getOriginalFilename().split("\\.")[1];
          String filename = UUID.randomUUID().toString() + "." + fileExtn;
          try
          {
            store.store("answerimage", filename, file.getBytes());
            imageFileNames.add(filename);
            //answerService.attachAnswerImage(filename,answerIds.get(i));
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        questionService.attachAnswerImage(imageFileNames, id);
        break;
      case "ANSWERAUDIO":
        List<String> audioFileNames = new ArrayList<>();
        for (MultipartFile file : fileList)
        {
          String fileExtn = file.getOriginalFilename().split("\\.")[1];
          String filename = UUID.randomUUID().toString() + "." + fileExtn;
          try
          {
            store.store("answeraudio", filename, file.getBytes());
            audioFileNames.add(filename);
            //answerService.attachAnswerAudio(filename,answerIds.get(k));
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        questionService.attachAnswerAudio(audioFileNames, id);
        break;
    }
  }

  @ResponseBody
  @RequestMapping(value = "/update/{fileType}/{updateAnswerIndex}/{id}", method = RequestMethod.POST)
  @Secured(WRITE_QUESTION)
  public void uploadUpdatedAnswerMedia(@PathVariable String fileType, @RequestParam("files") MultipartFile[] files, @PathVariable(required = false) Integer[] updateAnswerIndex,
                                       @PathVariable(required = false) Long id)
  {
    List<MultipartFile> fileList = Arrays.asList(files);
    if (fileList.isEmpty())
    {
      throw new ValidationException("Missing file");
    }
    List<Integer> indexList = Arrays.asList(updateAnswerIndex);
    switch (fileType)
    {
      case "ANSWERIMAGE":
        List<String> imageFileNames = new ArrayList<>();
        for (MultipartFile file : fileList)
        {
          String fileExtn = file.getOriginalFilename().split("\\.")[1];
          String filename = UUID.randomUUID().toString() + "." + fileExtn;
          try
          {
            store.store("answerimage", filename, file.getBytes());
            imageFileNames.add(filename);
            //answerService.attachAnswerImage(filename,answerIds.get(i));
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        questionService.attachAnswerImage(imageFileNames, indexList, id);
        break;
      case "ANSWERAUDIO":
        List<String> audioFileNames = new ArrayList<>();
        for (MultipartFile file : fileList)
        {
          String fileExtn = file.getOriginalFilename().split("\\.")[1];
          String filename = UUID.randomUUID().toString() + "." + fileExtn;
          try
          {
            store.store("answeraudio", filename, file.getBytes());
            audioFileNames.add(filename);
            //answerService.attachAnswerAudio(filename,answerIds.get(k));
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        questionService.attachAnswerAudio(audioFileNames, indexList, id);
        break;
    }
  }

  @ResponseBody
  @RequestMapping(value = "/groupActivity/{type}/{text}/{groupName}", method = RequestMethod.POST)
  @Secured(WRITE_QUESTION)
  public void groupActivityRawData(@PathVariable String type, @PathVariable String text, @PathVariable String groupName, @RequestParam("file") MultipartFile file)
  {
    if (file.isEmpty())
    {
      throw new ValidationException("Missing file");
    }
    GroupTextRequest textRequest = new GroupTextRequest();
    textRequest.setGroupName(groupName);
    textRequest.setCommonText(text);
    textRequest.setCommonType(type);
    try
    {
      String fileExtn = file.getOriginalFilename().split("\\.")[1];
      String filename = UUID.randomUUID().toString() + "." + fileExtn;
      String fileType = textRequest.getCommonType();
      switch (fileType)
      {
        case "Image":
          store.store("groupActivity", filename, file.getBytes());
          questionService.saveGroupActivity(filename, textRequest);
          break;
        case "Audio":
          store.store("groupActivity", filename, file.getBytes());
          questionService.saveGroupActivity(filename, textRequest);
          break;
        case "Video":
          store.store("groupActivity", filename, file.getBytes());
          questionService.saveGroupActivity(filename, textRequest);
          break;
      }
    }
    catch (IOException e)
    {
      logger.error("@uploadGroupActivityMedia", e);
      throw new ValidationException("Unknown error!");
    }
  }
  @PostMapping("/tenant/images/")
  @Secured(WRITE_TEST)
  public void uploadTenantFiles(@RequestParam("file") MultipartFile file, @RequestParam("fileName") String fileName)
  {
    if (file.isEmpty()||fileName==null)
    {
      throw new ValidationException("Missing file");
    }
    try
    {
      //store.delete(fileName+ ".png");
      store.store("profileimage", fileName+ ".png", file.getBytes());
    }
    catch (IOException e)
    {
      logger.error("Error in upload tenant file{}", e);
      throw new ValidationException("Unknown error!");
    }
  }


  @PostMapping("/timeline/store/image")
  @Secured(TAKE_TEST)
  public List<String> savePost(@RequestParam("file") MultipartFile file)
  {
    if (file.isEmpty())
    {
      throw new ValidationException("Missing file");
    }

    try
    {
      String fileExtn = file.getOriginalFilename().split("\\.")[1];
      String filename = UUID.randomUUID().toString() + "." + fileExtn;
      store.store("profileimage", filename, file.getBytes());
      List<String> filenames = new ArrayList<>();
      filenames.add(filename);
      return filenames;
    }
    catch (IOException e)
    {
      logger.error("@uploadimage", e);
      throw new ValidationException("Unknown error!");
    }
  }

  @PostMapping("/aimExpertInventory/image")
  @Secured(WRITE_MEDIA)
  public void uploadAimExpertInventoryImage(@RequestParam("file") MultipartFile file,
                                  @RequestParam("aimExpertInventoryId") Long aimExpertInventoryId)
  {
    if (file.isEmpty())
    {
      throw new ValidationException("Missing file");
    }

    try
    {
      String fileExtn = file.getOriginalFilename().split("\\.")[1];
      String filename = UUID.randomUUID().toString() + "." + fileExtn;
      store.store("profileimage", filename, file.getBytes());
      aimExpertInventoryService.attachAimExpertInventoryImage(filename, aimExpertInventoryId);
    }
    catch (IOException e)
    {
      logger.error("@uploadimage", e);
      throw new ValidationException("Unknown error!");
//            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }
  }




}



