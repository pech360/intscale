package com.synlabs.intscale.controller;

import com.synlabs.intscale.ImportModel.ImportCareerInterestInventoryModel;
import com.synlabs.intscale.entity.Report.CareerInterestInventory;
import com.synlabs.intscale.service.AimExpertInventoryService;
import com.synlabs.intscale.view.report.AimExpertInventoryRequest;
import com.synlabs.intscale.view.report.AimExpertInventoryResponse;
import com.synlabs.intscale.view.report.SubjectResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.READ_REPORTS;
import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.TAKE_TEST;
import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.WRITE_CATEGORY_SCORE;

@RestController
@RequestMapping("/api/aimExpert/")
public class AimExpertInventoryController
{
  @Autowired
  private AimExpertInventoryService aimExpertInventoryService;

  @PostMapping("aimExpertInventory")
  @Secured(WRITE_CATEGORY_SCORE)
  public List<AimExpertInventoryResponse> saveAimExpertInventory(@RequestBody AimExpertInventoryRequest aimExpertInventoryRequest)
  {
    return aimExpertInventoryService.saveAimExpertInventory(aimExpertInventoryRequest);
  }

  @GetMapping("aimExpertInventoryList")
  @Secured(WRITE_CATEGORY_SCORE)
  public List<AimExpertInventoryResponse> getaimExpertInventoryList(@RequestParam String type)
  {
    return aimExpertInventoryService.getAimExpertInventoryList(type);

  }

  @GetMapping("aimExpertInventoryList/forReport")
  @Secured(READ_REPORTS)
  public List<AimExpertInventoryResponse> getaimExpertInventoryListForReport(@RequestParam String type, @RequestParam Long userId)
  {
    return aimExpertInventoryService.getaimExpertInventoryListForReport(type, userId);

  }

  @GetMapping("aimExpertInventoryNameList")
  @Secured(WRITE_CATEGORY_SCORE)
  public List<String> getAimExpertInventoryNameList(@RequestParam String type)
  {
    return aimExpertInventoryService.getAimExpertInventoryNameList(type);
  }

  @PostMapping("careerMatrix")
  @Secured(WRITE_CATEGORY_SCORE)
  public void saveCareerMatrix(@RequestBody AimExpertInventoryRequest aimExpertInventoryRequest, @RequestParam String type)
  {
    aimExpertInventoryService.saveCareerMatrix(aimExpertInventoryRequest, type);
  }

  @GetMapping("subject")
  @Secured(WRITE_CATEGORY_SCORE)
  public List<SubjectResponse> getSubjects()
  {
    return aimExpertInventoryService.getSubjects();
  }

  @GetMapping("aimExpertInventoryItem")
  @Secured(WRITE_CATEGORY_SCORE)
  public AimExpertInventoryResponse getAimExpertInventoryItem(@RequestParam String name)
  {
    return aimExpertInventoryService.getAimExpertInventoryItem(name);
  }

  @GetMapping("aimExpertInventoryById")
  @Secured(WRITE_CATEGORY_SCORE)
  public AimExpertInventoryResponse getAimExpertInventoryItemById(@RequestParam Long id)
  {
    return aimExpertInventoryService.getAimExpertInventoryItem(id);
  }

  @PostMapping("aimExpertInventoryImport")
  @Secured(WRITE_CATEGORY_SCORE)
  public ResponseEntity<Map<String, String>> importAimExpertInventory(@RequestParam("file") MultipartFile file, @RequestParam(defaultValue = "true") String dryrun)
  {

    try
    {
      Map<String, String> map = aimExpertInventoryService.importAimExpertInventory(file, dryrun);
      String isSuccess = map.get("error");
      map.remove("error");
      if (isSuccess.equalsIgnoreCase("true"))
      {
        return new ResponseEntity<>(map, HttpStatus.EXPECTATION_FAILED);
      }
      return new ResponseEntity<>(map, HttpStatus.OK);
    }
    catch (Exception exp)
    {
      exp.printStackTrace();
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("careerInterestInventory/import")
  @Secured(WRITE_CATEGORY_SCORE)
  public ResponseEntity<Map<String, String>> importCareerInterestInventory(@RequestParam("file") MultipartFile file, @RequestParam(defaultValue = "true") String dryrun)
  {

    try
    {
      Map<String, String> map = aimExpertInventoryService.importCareerInterestInventory(file, dryrun);
      String isSuccess = map.get("error");
      map.remove("error");
      if (isSuccess.equalsIgnoreCase("true"))
      {
        return new ResponseEntity<>(map, HttpStatus.EXPECTATION_FAILED);
      }
      return new ResponseEntity<>(map, HttpStatus.OK);
    }
    catch (Exception exp)
    {
      exp.printStackTrace();
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


  @GetMapping("careerInterestInventory")
  @Secured(WRITE_CATEGORY_SCORE)
  public List<ImportCareerInterestInventoryModel> getCareerInterestInventory()
  {


      return aimExpertInventoryService.getCareerInterestInventory();

  }
}
