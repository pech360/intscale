package com.synlabs.intscale.controller;

import com.synlabs.intscale.entity.masterdata.Interest;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.service.MasterDataDocumentService;
import com.synlabs.intscale.view.InterestRequest;
import com.synlabs.intscale.view.InterestResponse;
import com.synlabs.intscale.view.masterdata.*;
import org.apache.poi.util.IOUtils;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ConnectException;
import java.util.List;
import java.util.stream.Collectors;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.*;

@RestController
@RequestMapping("/api/md")
public class MasterDataController
{

  @Autowired
  private MasterDataDocumentService masterDataDocumentService;

  @GetMapping("/list")
  @Secured(READ_MASTERDATA)
  public List<MasterDataDocumentResponse> list()
  {
    return masterDataDocumentService.getRecords().stream().map(MasterDataDocumentResponse::new).collect(Collectors.toList());
  }

  @PostMapping("/create")
  @Secured(WRITE_MASTERDATA)
  public MasterDataDocumentResponse create(@RequestBody @Validated MasterDataDocumentRequest request, BindingResult result)
  {
    if (result.hasErrors())
    {
      throw new ValidationException(result.toString());
    }
    return new MasterDataDocumentResponse(masterDataDocumentService.saveRecord(request));
  }

  @PutMapping("/update")
  @Secured(WRITE_MASTERDATA)
  public MasterDataDocumentResponse update(@RequestBody MasterDataDocumentRequest request, BindingResult result)
  {
    if (result.hasErrors())
    {
      throw new ValidationException(result.toString());
    }
    return new MasterDataDocumentResponse(masterDataDocumentService.updateRecord(request));
  }

  @DeleteMapping("/delete")
  @Secured(WRITE_MASTERDATA)
  public void delete(@RequestParam Long id)
  {
    masterDataDocumentService.deleteRecord(id);
  }

  @GetMapping("/designations/")
  @Secured(READ_MASTERDATA)
  public List<MasterDataDocumentResponse> getDesignations()
  {
    return masterDataDocumentService.getAllDesignations().stream().map(MasterDataDocumentResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/industries/")
  @Secured(READ_MASTERDATA)
  public List<MasterDataDocumentResponse> getIndustries()
  {
    return masterDataDocumentService.getAllIndustries().stream().map(MasterDataDocumentResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/interests/")
  @Secured(READ_MASTERDATA)
  public List<MasterDataDocumentResponse> getInterests()
  {
    return masterDataDocumentService.getAllInterests().stream().map(MasterDataDocumentResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/hobbies/")
  @Secured(READ_MASTERDATA)
  public List<MasterDataDocumentResponse> getHobbies()
  {
    return masterDataDocumentService.getAllHobbies().stream().map(MasterDataDocumentResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/studentTypes/")
  @Secured(READ_MASTERDATA)
  public List<MasterDataDocumentResponse> getStudentTypes()
  {
    return masterDataDocumentService.getAllStudentTypes().stream().map(MasterDataDocumentResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/userTypes/")
  @Secured(READ_MASTERDATA)
  public List<MasterDataDocumentResponse> getUserTypes()
  {
    return masterDataDocumentService.getAllUserTypes().stream().map(MasterDataDocumentResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/languages/")
  @Secured(READ_MASTERDATA)
  public List<MasterDataDocumentResponse> getLanguages()
  {
    return masterDataDocumentService.getAllLanguages().stream().map(MasterDataDocumentResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/gender/")
  @Secured(READ_MASTERDATA)
  public List<MasterDataDocumentResponse> getGender()
  {
    return masterDataDocumentService.getAllGender().stream().map(MasterDataDocumentResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/grades/")
  @Secured(READ_MASTERDATA)
  public List<MasterDataDocumentResponse> getGrades()
  {
    return masterDataDocumentService.getAllGrades().stream().map(MasterDataDocumentResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/subjects/")
  @Secured(READ_MASTERDATA)
  public List<MasterDataDocumentResponse> getSubjects()
  {
    return masterDataDocumentService.getAllSubjects().stream().map(MasterDataDocumentResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/degrees/")
  @Secured(READ_MASTERDATA)
  public List<MasterDataDocumentResponse> getDegrees()
  {
    return masterDataDocumentService.getAllDegrees().stream().map(MasterDataDocumentResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/sections/")
  @Secured(READ_MASTERDATA)
  public List<MasterDataDocumentResponse> getSections()
  {
    return masterDataDocumentService.getAllSections().stream().map(MasterDataDocumentResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/streams/")
  @Secured(READ_MASTERDATA)
  public List<MasterDataDocumentResponse> getStreams()
  {
    return masterDataDocumentService.getAllStreams().stream().map(MasterDataDocumentResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/groups/")
  @Secured(READ_MASTERDATA)
  public List<MasterDataDocumentResponse> getGroups()
  {
    return masterDataDocumentService.getAllGroups().stream().map(MasterDataDocumentResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/teacherTypes/")
  @Secured(READ_MASTERDATA)
  public List<MasterDataDocumentResponse> getTeacherTypes()
  {
    return masterDataDocumentService.getAllTeacherTypes().stream().map(MasterDataDocumentResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/pilots/")
  @Secured(READ_MASTERDATA)
  public List<MasterDataDocumentResponse> getpilots()
  {
    return masterDataDocumentService.getAllpilots().stream().map(MasterDataDocumentResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/allInterest")
  @Secured(READ_MASTERDATA)
  public List<InterestResponse> listInterests()
  {
    return masterDataDocumentService.getAllInterest().stream().map(InterestResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/allParentInterests")
  @Secured(READ_MASTERDATA)
  public List<InterestResponse> listParent()
  {
    return masterDataDocumentService.getParentInterest().stream().map(InterestResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/allChildInterests")
  @Secured(READ_MASTERDATA)
  public List<InterestResponse> listChild()
  {
    return masterDataDocumentService.getChildInterest().stream().map(InterestResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/allChildInterestByParent/{parentId}")
  @Secured(READ_MASTERDATA)
  public List<InterestResponse> listChildByParent(@PathVariable("parentId") Long parentId)
  {
    return masterDataDocumentService.getChildInterestByParent(parentId).stream().map(InterestResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/findOneInterest/{id}")
  @Secured(READ_MASTERDATA)
  public InterestResponse findOne(@PathVariable("id") Long id)
  {
    return new InterestResponse(masterDataDocumentService.findOneById(id));
  }

  @PostMapping
  @Secured(WRITE_MASTERDATA)
  public InterestResponse createInterest(@RequestBody @Validated InterestRequest request)
  {

    return new InterestResponse(masterDataDocumentService.saveInterest(request));
  }

  @DeleteMapping
  @Secured(WRITE_MASTERDATA)
  public void deleteInterest(@RequestParam Long interestId)
  {
    Interest interest = masterDataDocumentService.findOneById(interestId);
    masterDataDocumentService.deleteInterest(interest);
  }

  @PutMapping
  @Secured(WRITE_MASTERDATA)
  public InterestResponse updateInterest(@RequestBody @Validated InterestRequest request, BindingResult result)
  {
    if (result.hasErrors())
    {
      throw new ValidationException(result.toString());
    }
    return new InterestResponse(masterDataDocumentService.updateInterest(request));
  }

  @PostMapping("/delete/all")
  @Secured(WRITE_MASTERDATA)
  public void deleteInterests(@RequestBody @Validated List<Long> request)
  {
    masterDataDocumentService.deleteInterests(request);
  }

  @GetMapping("/allBlogs")
  @Secured(READ_MASTERDATA)
  public List<BlogResponse> listBlogs()
  {
    return masterDataDocumentService.getAllBlogs().stream().map(BlogResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/findBlogById/{id}")
  @Secured(READ_MASTERDATA)
  public BlogResponse findBlogById(@PathVariable("id") Long id)
  {
    return new BlogResponse(masterDataDocumentService.findBlogById(id));
  }

  @PostMapping("/createBlog")
  @Secured(WRITE_MASTERDATA)
  public BlogResponse createBlog(@RequestBody BlogRequest request) throws IOException, JSONException
  {
    return new BlogResponse(masterDataDocumentService.saveBlog(request));
  }

  @PutMapping("/updateBlog")
  @Secured(WRITE_MASTERDATA)
  public BlogResponse updateBlog(@RequestBody BlogRequest request)
  {
    return new BlogResponse(masterDataDocumentService.updateBlog(request));
  }

  @PostMapping("/deleteBlogs/all")
  @Secured(WRITE_MASTERDATA)
  public void deleteBlogs(@RequestBody @Validated List<Long> request)
  {
    masterDataDocumentService.deleteBlogs(request);
  }

  @PostMapping("/saveFeedback")
  @Secured(TAKE_TEST)
  public void create(@RequestBody UserExperienceFeedbackRequest request)
  {
    masterDataDocumentService.saveUserExperienceFeedback(request);
  }

  @GetMapping("/downloadFeedbacks")
  @Secured(READ_USER)
  public void downloadUserExperienceFeedback(HttpServletResponse response) throws IOException
  {
    String filename = masterDataDocumentService.downloadUserExperienceFeedback();
    File file = new File(filename);
    filename = "userExperienceFeedback.csv";
    response.setContentType("text/csv");
    response.setHeader("Content-disposition", String.format("attachment; filename = %s", filename));
    response.setHeader("fileName", filename);
    FileInputStream is = new FileInputStream(file);
    OutputStream out = response.getOutputStream();
    IOUtils.copy(is, out);
    out.flush();
    is.close();
    if (!file.delete())
    {
      throw new IOException("Could not delete temporary file after processing: " + file);
    }
  }

  @GetMapping("/recommendBlogs")
  @Secured(TAKE_TEST)
  public List<BlogResponse> recommendBlogsBasedOnInterest(){
    return masterDataDocumentService.recommendBlogsBasedOnInterest();
  }

  @PostMapping("/likeBlog/{id}")
  @Secured(TAKE_TEST)
  public LikedContentResponse likeBlog(@PathVariable("id") Long id){
    return masterDataDocumentService.likeBlog(id);
  }

  @GetMapping("/cities/")
  @Secured(READ_MASTERDATA)
  public List<MasterDataDocumentResponse> getCities()
  {
    return masterDataDocumentService.getAllCities().stream().map(MasterDataDocumentResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/tags/")
  @Secured(READ_MASTERDATA)
  public List<MasterDataDocumentResponse> getTags()
  {
    return masterDataDocumentService.getAllTags().stream().map(MasterDataDocumentResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/getCitiesFromUserTable")
  @Secured(READ_MASTERDATA)
  public List<String> getSchoolNamesFromUserTable()
  {
    return masterDataDocumentService.getAllCitiesFromUserTable();
  }
}
