package com.synlabs.intscale.controller;

import com.itextpdf.text.DocumentException;
import com.synlabs.intscale.entity.Report.Report;
import com.synlabs.intscale.entity.Report.UserReportStatus;
import com.synlabs.intscale.entity.user.User;
import com.synlabs.intscale.enums.AimReportType;
import com.synlabs.intscale.enums.ReportStatus;
import com.synlabs.intscale.service.ReportService;
import com.synlabs.intscale.service.UserService;
import com.synlabs.intscale.store.FileStore;
import com.synlabs.intscale.view.DashboardRequest;
import com.synlabs.intscale.view.report.*;
import com.synlabs.intscale.view.UserDetails;
import com.synlabs.intscale.view.reportShare.ReportShareRequest;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.*;

/**
 * Created by itrs on 9/14/2017.
 */
@RestController("ReportsController")
@RequestMapping(value = "/api/reports")
public class ReportsController {
    private static Logger logger = LoggerFactory.getLogger(ReportsController.class);

    @Autowired
    private UserService userService;
    @Autowired
    private ReportService reportService;

    @Autowired
    private FileStore store;

    @RequestMapping(value = "/download/{reportName}")
    @Secured(READ_TEST_REPORT)
    public void reportDownload(@PathVariable String reportName, @RequestParam(required = false) Long id, @RequestParam Date fromdate, @RequestParam Date todate,
                               @RequestParam String school, @RequestParam String grade, @RequestParam String reportType,
                               @RequestParam Long category, @RequestParam Long subCategory, HttpServletResponse response) throws IOException {
        String filename = null;
        String testReportName = "";
        XSSFWorkbook workbook = new XSSFWorkbook();
        logger.info("in reportDownload 34 TestReport " + reportName);
        switch (reportType) {
            case "default":
                filename = reportService.generateTestReport(workbook, id, fromdate, todate, school, grade, category, subCategory);
                break;
            case "testCount":
                filename = reportService.generateTestCountReportPerUser(workbook, id, fromdate, todate, school, grade);
                break;
            case "newFormat":
                filename = reportService.generateCustomizeTestReport(workbook, id, fromdate, todate, school, grade);
                break;
            case "ScoreEngine":
                filename = reportService.generateReportForScoringEngine(workbook, id, fromdate, todate, school, grade, category, subCategory);
                break;
            case "ScoreByCategory":
                filename = reportService.generateReportForCategoryWiseScore(workbook, id, fromdate, todate, school, grade, category, subCategory);
                break;
        }
        logger.info("Download request...generated file {}", filename);
        response.setContentType("application/vnd.ms-excel");
        //response.setContentType("text/plain");
        if (reportName != null && !reportName.equals("undefined") && !reportName.equals("")) {
            testReportName = reportName.replaceAll("\\s+", "");
        }
        response.setHeader("Content-Disposition", "attachment; filename=" + testReportName + "TestReport.csv");
        InputStream is = new FileInputStream(filename);
        IOUtils.copy(is, response.getOutputStream());
        response.flushBuffer();
    }

    @RequestMapping(value = "/download/userAssignment")
    @Secured(READ_TEST_REPORT)
    public void reportDownload(@RequestParam Long userTestIds[], HttpServletResponse response) throws IOException {
        String filename = reportService.generatedUserTestAssignmentSheet(Arrays.asList(userTestIds));
        String testReportName = "userTestAssignment.csv";
        logger.info("Download request...generated file {}", filename);
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=" + testReportName);
        InputStream is = new FileInputStream(filename);
        IOUtils.copy(is, response.getOutputStream());
        response.flushBuffer();
    }

    @RequestMapping(value = "/downloaduserInterest")
    @Secured(READ_TEST_REPORT)
    public void reportDownloadUserInterest(@RequestParam Long userIds[], HttpServletResponse response) throws IOException {
        String filename = reportService.generatedUserInterestSheet(Arrays.asList(userIds));
        String testReportName = "userInterestAndRating.csv";
        logger.info("Download request...generated file {}", filename);
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=" + testReportName);
        InputStream is = new FileInputStream(filename);
        IOUtils.copy(is, response.getOutputStream());
        response.flushBuffer();
    }

    @GetMapping("/userdetails")
    @Secured(READ_USER)
    public List<UserDetails> getUserDetails(@RequestParam String username) {
        return reportService.getuserDetails(username);
    }

    @GetMapping("/individual/{id}")
    @Secured(READ_STUDENT_REPORT)
    public Map<String, Float> getIndividualReport(@PathVariable("id") Long id) {
        return reportService.getIndividualReport(id);
    }

    @GetMapping("/individualV1")
    @Secured(READ_STUDENT_REPORT)
    public List<CategoryResponse> getIndividualReportV1(@RequestParam(required = false) Long id, @RequestParam(required = false) Long userId) {
        return reportService.getIndividualReportV1(id, userId);
    }

    @GetMapping("/downloadAimReport/{id}/{updateReport}/{reportType}")
    @Secured(READ_REPORTS)
    public void downloadIndividualReportV1(@PathVariable("id") Long id, @PathVariable("updateReport") boolean updateReport, @PathVariable("reportType") String reportTypeString, HttpServletResponse response)
            throws IOException, DocumentException {
        String fileName = null;
        AimReportType reportType = null;
        if (!StringUtils.isEmpty(reportTypeString)) {
            try {
                reportType = AimReportType.valueOf(reportTypeString);
            } catch (IllegalArgumentException e) {
                logger.error("requested reportType not found", e);
            }
        }

        User user = userService.getOneById(id);

        if (!updateReport) {
            fileName = reportService.tryToGetReportFromS3Bucket(user, reportType);
        }
        Report report = reportService.getReport("Aim Composite Report");

        if (StringUtils.isEmpty(fileName)) {
            UserReportStatus userReportStatus = reportService.setUserReportStatus(reportService.getUser(id), report, ReportStatus.Generating, null, null);
            File file = reportService.downloadAimReport(id, null, reportType);

            if (file == null || file.length() == 0) {
                reportService.setUserReportStatus(userReportStatus.getUser(), report, ReportStatus.Failed, null, null);
                logger.info("Report Status of " + userReportStatus.getUser().getUsername() + " - " + userReportStatus.getStatus());
                return;
            } else {
                reportService.setUserReportStatus(userReportStatus.getUser(), report, ReportStatus.Generated, file.getName(), null);
                store.store("profileimage", file);
            }

            fileName = String.format("%s.pdf", reportService.getUserNameByUserId(id));

            response.setContentType("application/pdf");
            response.setHeader("Content-disposition", "attachment; filename=" + fileName);
            response.setHeader("fileName", fileName);

            FileInputStream is = new FileInputStream(file);

            OutputStream out = response.getOutputStream();
            IOUtils.copy(is, out);
            out.flush();
            is.close();
            if (!file.delete()) {
                throw new IOException("Could not delete temporary file after processing: " + file);
            }

        } else {
            if (user != null) {
                fileName = user.getUsername() + "_" + user.getName() + ".pdf";
                if (!StringUtils.isEmpty(reportType)) {
                    if (reportType.equals(AimReportType.MINI)) {
                        fileName = user.getUsername() + "_" + user.getName() + "MINI.pdf";
                    }
                }
            }

            response.setContentType("application/pdf");
            response.setHeader("Content-disposition", "attachment; filename=" + fileName);
            response.setHeader("fileName", fileName);
            InputStream is = store.getStream("profileimage", fileName);
            OutputStream out = response.getOutputStream();

            IOUtils.copy(is, out);
            out.flush();
            is.close();
        }
    }

    @GetMapping("/talentReport")
    @Secured(READ_REPORTS)
    public List<CategoryResponse> getTalentReport(@RequestParam(required = false) Long id, @RequestParam(required = false) Long userId) {
        return reportService.getTalentReport(id, userId);
    }

    @GetMapping("/aimCompositeReport")
    @Secured(READ_REPORTS)
    public List<ReportSectionResponse> getAimCompositeReport(@RequestParam(required = false) Long userId) {
        return reportService.getAimCompositeReport(userId);
    }

    @PostMapping("/saveReport")
    @Secured(WRITE_REPORTS)
    public List<ReportResponse> saveReport(@RequestBody ReportRequest request) {
        return reportService.saveReport(request);
    }

    @GetMapping("/getReports")
    @Secured(WRITE_REPORTS)
    public List<ReportResponse> getReports() {
        return reportService.getReports();
    }

    @GetMapping("/getReport/{id}")
    @Secured(WRITE_REPORTS)
    public ReportResponse getReport(@PathVariable Long id) {
        return reportService.getReport(id);
    }

    @PostMapping("/saveReportSection")
    @Secured(WRITE_REPORTS)
    public ReportSectionResponse saveReportSection(@RequestBody ReportSectionRequest request) {
        return reportService.saveReportSection(request);
    }

    @GetMapping("/getReportSections")
    @Secured(WRITE_REPORTS)
    public List<ReportSectionResponse> getReportSections() {
        return reportService.getReportSections();
    }

    @GetMapping("/getReportSection/{id}")
    @Secured(WRITE_REPORTS)
    public ReportSectionResponse getReportSection(@PathVariable Long id) {
        return reportService.getReportSection(id);
    }

    @PutMapping("/saveReportSectionScoreRanges/{id}/{applicableFor}")
    @Secured(WRITE_CATEGORY_SCORE)
    public void saveReportSectionScoreRanges(@PathVariable("id") Long id, @PathVariable("applicableFor") String applicableFor, @RequestBody List<ScoreRangeRequest> request) {
        reportService.saveReportSectionScoreRanges(request, id, applicableFor);
    }

    @PostMapping("/saveInterestInventoryitem")
    @Secured(WRITE_CATEGORY_SCORE)
    public List<InterestInventoryResponse> saveInterestInventory(@RequestBody InterestInventoryRequest request) {
        return reportService.saveInterestInventory(request);
    }

    @GetMapping("/interestInventory")
    @Secured(WRITE_REPORTS)
    public List<InterestInventoryResponse> getInterestInventory() {
        return reportService.getInterestInventory();
    }

    @GetMapping("getReportSectionWithCategory")
    @Secured(WRITE_CATEGORY_SCORE)
    public List<ReportSectionResponse> getReportSectionWithCategory() {
        return reportService.getReportSectionWithCategory();
    }

    @GetMapping("/copyJuniorNormsToSenior/{id}")
    @Secured(WRITE_CATEGORY_SCORE)
    public void copyJuniorNormsToSenior(@PathVariable Long id) {
        reportService.copyJuniorNormsToSenior(id);
    }

    @GetMapping("/checkAndAllowAllUserForReport/{id}")
    @Secured(WRITE_REPORTS)
    public void checkAndAllowAllUserForReport(@PathVariable Long id) {
        reportService.checkAndAllowAllUserForReport(id);
    }

    @PostMapping("/userReportStatus")
    @Secured(WRITE_REPORTS)
    public List<UserReportStatusResponse> getUserReportStatus(@RequestParam(required = false) String status, @RequestBody DashboardRequest request) {
        return reportService.getUserReportStatus(status, request);
    }

    @PostMapping("/userReportStatus/byFullName")
    @Secured(WRITE_REPORTS)
    public List<UserReportStatusResponse> getUserReportStatusByFullName(@RequestParam(required = false) String fullName) {
        return reportService.getUserReportStatusByFullName(fullName);
    }

    @GetMapping("/userReportStatus/byUser")
    @Secured(READ_REPORTS)
    public UserReportStatusResponse getUserReportStatusByUser(@RequestParam(required = false) Long userId) throws IOException {
        UserReportStatus userReportStatus = reportService.getUserReportStatusByUser(userId, null);
        return userReportStatus == null ? null : new UserReportStatusResponse(userReportStatus);
    }

    @PostMapping("/shareReport")
    @Secured(WRITE_REPORTS)
    public void shareReport(@RequestParam(required = false) String email, @RequestParam(required = false) Long userId, @RequestParam(required = false) String reportType) {
        reportService.prepareShareReport(email, null, userId, reportType, false);
    }


    @PostMapping("/aimReportInExcel")
    @Secured(READ_REPORTS)
    public void saveAimReportExcelInDb(@RequestBody UserReportStatusRequest request) throws IOException {
        reportService.saveAimReportExcelInDb(request);
    }

    @PostMapping("/downloadAimReportInExcel")
    @Secured(WRITE_REPORTS)
    public void downloadAimReportInExcel(@RequestBody DashboardRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/octet-stream");
        String fileName = "aimReportInExcel";
        response.setHeader("Content-disposition", String.format("attachment; filename=%s.csv", fileName));
        response.setHeader("fileName", String.format("%s.csv", fileName));
        File file = new File(reportService.downloadAimReportInExcel(request));
        FileInputStream is = new FileInputStream(file);
        OutputStream out = response.getOutputStream();
        IOUtils.copy(is, out);
        out.flush();
        is.close();
        if (!file.delete()) {
            throw new IOException("Could not delete temporary file after processing: " + file);
        }
    }

    @GetMapping("/downloadAimReportScoreCardInExcel")
    @Secured(READ_COUNSELLOR_STUDENT)
    public void downloadAimReportScoreCardInExcel(@RequestParam Long userId, HttpServletResponse response) throws IOException {
        response.setContentType("application/octet-stream");
        String fileName = "aimReportInExcel";
        response.setHeader("Content-disposition", String.format("attachment; filename=%s.csv", fileName));
        response.setHeader("fileName", String.format("%s.csv", fileName));
//        File file = new File(reportService.downloadAimReportScoreCardInExcel(userId));
        File file = new File(reportService.downloadAimReportScoreCardInExcel2(userId));
        FileInputStream is = new FileInputStream(file);
        OutputStream out = response.getOutputStream();
        IOUtils.copy(is, out);
        out.flush();
        is.close();
        if (!file.delete()) {
            throw new IOException("Could not delete temporary file after processing: " + file);
        }
    }

    @PutMapping("/saveReportStandardScore")
    @Secured(WRITE_CATEGORY_SCORE)
    public ReportSectionResponse saveReportStandardScore(@RequestParam String language, @RequestBody ReportSectionRequest request) {
        return reportService.saveReportStandardScore(request, language);
    }

    @PostMapping("/userReportStatus/count")
    @Secured(WRITE_REPORTS)
    public Map<String, Long> getUserReportStatusCount(@RequestBody DashboardRequest request) {
        return reportService.getUserReportStatusCount(request);
    }

    @PostMapping("/multiple/process/usernames")
    @Secured(WRITE_REPORTS)
    public void processMultipleReportsWithUsernames(@RequestParam String usernames) {
        reportService.processMultipleReportsWithUsernames(usernames);
    }

    @PostMapping("/multiple/process")
    @Secured(WRITE_REPORTS)
    public void processMultipleReports(@RequestParam List<Long> userIds, @RequestParam String reportType) {
        reportService.prepareForProcessMultipleReports(userIds, reportType);
    }

    @PostMapping("/multiple/share")
    @Secured(WRITE_REPORTS)
    public void shareMultipleReports(@RequestBody ReportShareRequest request) {
        reportService.shareMultipleReportsByEmail(request, false);
    }

    @PostMapping("/multiple/share/testTaker")
    @Secured(WRITE_REPORTS)
    public void shareMultipleReportsWithTestTaker(@RequestBody ReportShareRequest request) {
        reportService.shareMultipleReportsByEmail(request, true);
    }

    @GetMapping("/aimReportDemo")
    @Secured(READ_REPORTS)
    public List<ReportSectionResponse> getAimReportDemoScores(@RequestParam(required = false) Long userId) {
        return reportService.getAimReportDemoScores(userId);
    }

    @GetMapping("/download/aimReport/demo/{id}")
    @Secured(READ_REPORTS)
    public void down(@PathVariable("id") Long id, HttpServletResponse response) throws IOException {


        User user = userService.getOneById(id);
        String fileName = user.getUsername() + "_" + user.getName() + "DEMO.pdf";

        File file = reportService.downloadAimReport(id, null, AimReportType.DEMO);

        if (file == null || file.length() == 0) {
            logger.info("Demo Report " + user.getUsername() + " - " + "failed");
            return;
        } else {
            store.store("profileimage", file);
        }

        response.setContentType("application/pdf");
        response.setHeader("Content-disposition", "attachment; filename=" + fileName);
        response.setHeader("fileName", fileName);

        FileInputStream is = new FileInputStream(file);

        OutputStream out = response.getOutputStream();
        IOUtils.copy(is, out);
        out.flush();
        is.close();
        if (!file.delete()) {
            throw new IOException("Could not delete temporary file after processing: " + file);
        }

    }

}
