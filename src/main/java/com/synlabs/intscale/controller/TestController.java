package com.synlabs.intscale.controller;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.synlabs.intscale.entity.test.Test;
import com.synlabs.intscale.service.TestService;
import com.synlabs.intscale.view.*;
import com.synlabs.intscale.view.TestTreeResponse.TestParent;
import com.synlabs.intscale.view.TestTreeResponse.TestSummaryResponse;
import com.synlabs.intscale.view.usertest.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.*;
import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.TAKE_TEST;

@RestController
@RequestMapping("/api/test")
public class TestController {

    @Autowired
    private TestService testService;

    @GetMapping("/{id}")
    @Secured(READ_TEST)
    public TestDescriptionResponse getTestDescription(@PathVariable("id") Long id) {
        return testService.findOne(id);
    }

    @GetMapping("/fullTest/{id}")
    @Secured(TAKE_TEST)
    public TestResponse getOne(@PathVariable("id") Long id) {
        return new TestResponse(testService.getOne(id));
    }

    @GetMapping("/forTakers/{id}")
    @Secured(TAKE_TEST)
    public TestResponseForTaker getOneForTestTaker(@PathVariable("id") Long id) {
        return new TestResponseForTaker(testService.getOne(id));
    }

    @GetMapping
    @Secured(TAKE_TEST)
    public List<TestDescriptionResponse> listTakenTest() {
        return testService.getUserTests();
    }

    @GetMapping("/listTakenTestByUser")
    @Secured(READ_COUNSELLOR_STUDENT)
    public List<TestDescriptionResponse> listTakenTestByUser(@RequestParam Long userId) {
        return testService.getTakenTestsByUserId(userId);
    }

    @GetMapping("/listAvailableTests")
    @Secured(TAKE_TEST)
    public List<TestParent> listAvailableTests() {
        //return testService.availablePublishTests().stream().map(TestDescriptionResponse::new).collect(Collectors.toList());
        return testService.availablePublishTests();
    }

    @GetMapping("/listAllPublishTests")
    @Secured(TAKE_TEST)
    public List<TestDescriptionResponse> listAvailableAllTests() {
        return testService.availablePublishTestsForAdmin().stream().map(TestDescriptionResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/listAllPublishTestsByParent")
    @Secured(TAKE_TEST)
    public TestSummaryResponse listAvailableAllTestsByParent(@RequestParam Long id) {
        if (testService.getUserAvailableTestEitherFreeAndPaid(id).isEmpty()) {
            return new TestSummaryResponse(new Test());
        }
        return new TestSummaryResponse(testService.getUserAvailableTestEitherFreeAndPaid(id).get(0));
    }

    @GetMapping("/testHistory")
    @Secured(READ_HISTORY)
    public List<TestHistory> testHistory(@RequestParam Long id) {
        return testService.testHistory(id).stream().map(TestHistory::new).collect(Collectors.toList());
    }

    @GetMapping("/goLive")
    @Secured(WRITE_LIVE)
    public boolean goLiveTest(@RequestParam Long id) {
        return testService.goLiveTest(id);
    }

    @GetMapping("/testScore")
    @Secured(READ_ENGINE)
    public List<ScoringResponse> listScoring(@RequestParam Long categoryId, @RequestParam Long subCategoryId, @RequestParam Long testId) {
        return testService.listForScoring(categoryId, subCategoryId, testId);
    }

    @GetMapping("/parentByTenant")
    @Secured(READ_PRODUCT)
    public List<TestSummaryResponse> parentTest() {
        return testService.findAllParentTestByTenant().stream().map(TestSummaryResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/testByParent")
    @Secured(READ_PRODUCT)
    public List<TestSummaryResponse> testByParent(@RequestParam Long id) {
        return testService.findAllchildTestByParent(id).stream().map(TestSummaryResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/testByIds")
    @Secured(READ_PRODUCT)
    public List<TestSummaryResponse> testByIds(@RequestParam Long ids[]) {
        return testService.findAllByIds(ids).stream().map(TestSummaryResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/subjects")
    @Secured(READ_PRODUCT)
    public List<TestSummaryResponse> getAllSubjectsByGrades(@RequestParam String grade) {
        return testService.findAllSubjectByGradeAndTenant(grade).stream().map(TestSummaryResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/subjectsByGradeAndTeacher/{grade}")
    @Secured(READ_PRODUCT)
    public List<TestSummaryResponse> getAllSubjectsByGradeAndTeacher(@PathVariable("grade") String grade) {
        return testService.findAllSubjectByGradeAndTeacher(grade).stream().map(TestSummaryResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/constructList")
    @Secured(READ_CONSTRUCT)
    public List<TestSummaryResponse> constructList() {
        return testService.findAllConstructByTenant().stream().map(TestSummaryResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/allConstructByLeafNodeIsTrue")
    @Secured(READ_CONSTRUCT)
    public List<TestSummaryResponse> constructListByLeaf() {
        return testService.findAllConstructByTenantAndLeafISTrue().stream().map(TestSummaryResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/availableTestForConstruct")
    @Secured(READ_CONSTRUCT)
    public List<TestSummaryResponse> availableTestForConstruct() {
        return testService.findAllTestForConstruct().stream().map(TestSummaryResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/testByConstructId")
    @Secured(READ_CONSTRUCT)
    public List<TestSummaryResponse> testListByConstructId(@RequestParam Long id) {
        return testService.findAllTestByConstructId(id).stream().map(TestSummaryResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/parentByTenantAndConstruct")
    @Secured(READ_CONSTRUCT)
    public List<TestSummaryResponse> parentTestByConstruct() {
        return testService.findAllParentTestByTenantAndConstruct().stream().map(TestSummaryResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/testByParentAndConstruct")
    @Secured(READ_CONSTRUCT)
    public List<TestSummaryResponse> testByParentAndConstruct(@RequestParam Long id) {
        return testService.findAllchildTestByParentAndConstruct(id).stream().map(TestSummaryResponse::new).collect(Collectors.toList());
    }

    @PostMapping("/construct")
    @Secured(WRITE_CONSTRUCT)
    public TestSummaryResponse save(@RequestBody TestConstructRequest request) {
        return new TestSummaryResponse(testService.save(request));
    }

    @PutMapping("/construct")
    @Secured(WRITE_CONSTRUCT)
    public TestSummaryResponse update(@RequestBody TestConstructRequest request) {
        return new TestSummaryResponse(testService.save(request));
    }

    @DeleteMapping("/construct/{id}")
    @Secured(WRITE_CONSTRUCT)
    public void deleteConstruct(@PathVariable("id") Long id) {
        testService.deleteConstruct(id);
    }

    @PutMapping("/updateConstructTest")
    @Secured(WRITE_CONSTRUCT)
    public TestSummaryResponse updateConstructTest(@RequestBody ConstructTestRequest request) {
        return new TestSummaryResponse(testService.updateConstructTest(request));
    }

    @PutMapping("/testSequencing")
    @Secured(WRITE_SEQUENCE)
    public List<TestDescriptionResponse> testSequence(@RequestBody List<TestSequenceRequest> request) {
        return testService.testSequencing(request);
    }

    @PutMapping("/tenantTestAllotment")
    @Secured(WRITE_TENATTESTALLOTMENT)
    public boolean tenantsTestAllotment(@RequestBody TenantTestRequest request) {
        return testService.tenantTestAllotment(request);
    }

    @PutMapping("/tenantTestUpdate")
    @Secured(WRITE_TENATTESTALLOTMENT)
    public boolean tenantsTestUpdate(@RequestBody TenantTestRequest request) {
        return testService.tenantTestUpdate(request);
    }

    @GetMapping("/allotTestListByuser")
    @Secured(READ_USER)
    public List<TenantTestResponse> allotTestListByUser(@RequestParam(required = false) String roleName) {
        return testService.getListAdminTest(roleName);
    }

    @GetMapping("/assignedTest")
    @Secured(READ_USER)
    public List<UserTestResponse> assignedTestByStudent(@RequestParam(required = false) String schoolName, @RequestParam(required = false) String grade,
                                                        @RequestParam(required = false) String gender, @RequestParam(required = false) String status,
                                                        @RequestParam(required = false) String type) {
        return testService.getListUserTest(schoolName, grade, gender, status, type);
    }

    @PutMapping("/usertestAssignment")
    @Secured(WRITE_USERTESTASSIGNMENT)
    public boolean userTestAssignment(@RequestBody UserTestRequest request) {
        return testService.userTestAssignment(request);
    }

    @GetMapping("/hasOnlyOneTest")
    @Secured(TAKE_TEST)
    public boolean hasOnlyOneTest() {
        return testService.hasOnlyOneTestOnUser();
    }

    @PutMapping("/usertestDlinkAll")
    @Secured(WRITE_USERTESTASSIGNMENT)
    public boolean userTestDLinkAll(@RequestBody UserTestRequest request) {
        return testService.userTestDLinkAll(request);
    }

    @PutMapping("/userTestUpdate")
    @Secured(WRITE_USERTESTASSIGNMENT)
    public boolean userTestAssignUpdate(@RequestParam Long id, @RequestParam String[] userTest) {
        return testService.userTestAssignmentUpdate(id, userTest);
    }

    @PostMapping
    @Secured(WRITE_TEST)
    public TestResponse save(@RequestBody TestRequest request) {
        return new TestResponse(testService.saveTest(request));
    }

    @GetMapping("/allTestWithAllVersions")
    @Secured(READ_TEST)
    public List<TestDescriptionResponse> listAllTestWithAllVersions() {
        return testService.listAllTestWithAllVersions();
    }

    @GetMapping("/all")
    @Secured(READ_TEST)
    public List<TestDescriptionResponse> listAllTests() {
        return testService.listAllTests();
    }

    @PutMapping("/{version}")
    @Secured(WRITE_TEST)
    public TestResponse update(@PathVariable(required = false) boolean version, @RequestBody TestRequest request) {
        return new TestResponse(testService.updateTest(request, version));
    }

    @PutMapping("/publish/")
    @Secured(PUBLISH_TEST)
    public TestDescriptionResponse publishTest(@RequestParam Long id, Boolean publish) {
        return new TestDescriptionResponse(testService.publishTest(id, publish));
    }

    @DeleteMapping("/{id}")
    @Secured(WRITE_TEST)
    public void deleteTest(@PathVariable("id") Long id) {
        testService.deleteTest(id);
    }

    @PostMapping("import")
    @Secured(WRITE_TEST)
    public ResponseEntity<Map<String, String>> bulkMCQTestUpload(@RequestParam("file") MultipartFile file,
                                                                 @RequestParam(defaultValue = "true") String dryrun,
                                                                 @RequestParam("testType") String testType) {
        try {
            Map<String, String> map = testService.bulkTestUpload(file, dryrun, testType);
            String isSuccess = map.get("error");
            map.remove("error");
            if (isSuccess.equalsIgnoreCase("true")) {
                return new ResponseEntity<>(map, HttpStatus.EXPECTATION_FAILED);
            }
            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch (Exception exp) {
            exp.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/listAllTakenTest")
    public List<TestDescriptionResponse> listAllTakenTest() {
        return testService.getAllTakenTestByTenant().stream().map(TestDescriptionResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/treeList")
    public List<TestParent> testTreeList() {
        return testService.getTreeTest(null);
    }

    @PutMapping("/saveTestQuestionWithCategory")
    @Secured(WRITE_DERIVE_CATEGORY)
    public List<TestQuestionResponse> updateTestQuestionsWithCategory(@RequestBody List<TestQuestionRequest> request) {
        return testService.updateTestQuestionsWithCategory(request).stream().map(TestQuestionResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/deriveCategories/{id}")
    @Secured(READ_TEST)
    public TestDescriptionWithQuestionAndCategoryResponse getTestWithTestQuestionsWithDerivedCategories(@PathVariable("id") Long id) {
        return new TestDescriptionWithQuestionAndCategoryResponse(testService.getTestWithTestQuestionsWithDerivedCategories(id));
    }

    @GetMapping("/getAllProducts")
    @Secured(READ_RESULT)
    public List<TestDescriptionResponse> getAllProductsOfGrade() {
        return testService.getAllProductsOfGrade().stream().map(TestDescriptionResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/getConstructsByProductId/{productId}")
    @Secured(READ_RESULT)
    public List<TestDescriptionResponse> getConstructsByProductId(@PathVariable("productId") Long productId) {
        return testService.getAllConstructByProductId(productId).stream().map(TestDescriptionResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/getTestsByConstructId/{constructId}")
    @Secured(READ_RESULT)
    public List<TestDescriptionResponse> getTestsByConstructId(@PathVariable("constructId") Long constructId) {
        return testService.getAllTestByCostructId(constructId).stream().map(TestDescriptionResponse::new).collect(Collectors.toList());
    }

    @PutMapping("/scoringFormula")
    @Secured(WRITE_CATEGORY_SCORE)
    public void updateTestWithScoringFormula(@RequestBody TestRequest request) {
        testService.saveTestScoringFormula(request.getId(), request.getScoringFormula());
    }

    @GetMapping("/isUserThreeSixtyEligible/")
    @Secured(READ_RESULT)
    public boolean isUserThreeSixtyEligible() {
        return testService.isUserThreeSixtyEligible();
    }

    @PutMapping("/productAllotment")
    @Secured(WRITE_TENANT_PRODUCT_ALLOTMENT)
    public void productAllotment(@RequestBody TenantProductRequest request) {
        testService.tenantProductAllotment(request);
    }

    @DeleteMapping("/removeProductAllotment")
    @Secured(WRITE_TENANT_PRODUCT_ALLOTMENT)
    public void removeProductAllotment(@RequestParam Long id, @RequestParam Long tenantId) {
        testService.removeTenantProductAllotment(id, tenantId);
    }

    @GetMapping("/getProductAllotment")
    @Secured(WRITE_TENANT_PRODUCT_ALLOTMENT)
    public List<TenantProductResponse> productAllotment() {
        return testService.getProductAllotment();
    }

    @GetMapping("/offerTest")
    @Secured(WRITE_TENANT_OFFER_TEST)
    public List<TestPerTenantResponse> getOfferedTest() {
        return testService.getAllOfferTest();
    }

    @PutMapping("/offerTestPublish")
    @Secured(WRITE_TENANT_OFFER_TEST)
    public void offerTestUpdate(@RequestParam Long id, @RequestParam boolean published) {
        testService.updateOfferTest(id, published);
    }

    @PostMapping("testQuestionImport")
    @Secured(WRITE_CATEGORY_SCORE)
    public ResponseEntity<Map<String, String>> importTestQuestions(@RequestParam("file") MultipartFile file, @RequestParam(defaultValue = "true") String dryrun) {

        try {
            Map<String, String> map = testService.importTestQuestions(file, dryrun);
            String isSuccess = map.get("error");
            map.remove("error");
            if (isSuccess.equalsIgnoreCase("true")) {
                return new ResponseEntity<>(map, HttpStatus.EXPECTATION_FAILED);
            }
            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch (Exception exp) {
            exp.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
