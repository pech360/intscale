package com.synlabs.intscale.controller;

import com.synlabs.intscale.entity.user.Teacher;
import com.synlabs.intscale.service.TeacherService;
import com.synlabs.intscale.view.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.*;

/**
 * Created by India on 1/5/2018.
 */
@RestController("teacherController")
@RequestMapping("/api/teacher")
public class TeacherController {
    @Autowired
    private TeacherService teacherService;

    @GetMapping
    @Secured(READ_TEACHER)
    public List<TeacherResponse> list() {
        return teacherService.findAll().stream().map(TeacherResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/classTeacher")
    @Secured(READ_TEACHER)
    public List<TeacherResponse> listOfClassTeacher() {
        return teacherService.findAllByTeacherType().stream().map(TeacherResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/subjectTeacher")
    @Secured(READ_TEACHER)
    public List<TeacherResponse> listOfSubjectTeacher() {
        return teacherService.findAllSubjectTeacher().stream().map(TeacherResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/filterList")
    @Secured(READ_TEACHER_GRADE)
    public List<TeacherResponse> listByFilter(@RequestParam(required = false) Long schoolId, @RequestParam(required = false) String gradeId) {
        return teacherService.findAllByFilter(schoolId, gradeId).stream().map(TeacherResponse::new).collect(Collectors.toList());
    }

    @PostMapping
    @Secured(WRITE_TEACHER)
    public TeacherResponse create(@RequestBody TeacherRequest request) {
        return new TeacherResponse(teacherService.save(request));
    }

    @PutMapping
    @Secured(WRITE_TEACHER)
    public TeacherResponse update(@RequestBody TeacherRequest request) {
        return new TeacherResponse(teacherService.update(request));
    }

    @PutMapping("/assignGrade")
    @Secured(WRITE_TEACHER_GRADE)
    public boolean assignGradeToTeachers(@RequestBody TeacherGradeRequest request) {
        return teacherService.assignGradeToTeachers(request);
    }

    @PutMapping("/removeAll")
    @Secured(WRITE_TEACHER_GRADE)
    public boolean removeGradeToStudents(@RequestBody TeacherGradeRequest request) {
        return teacherService.removeGradeToTeachers(request);
    }

    @PutMapping("/updateGrade")
    @Secured(WRITE_TEACHER_GRADE)
    public boolean updateGradeToStudents(@RequestParam Long id, @RequestParam Long gradeId) {
        return teacherService.updateGradeToTeachers(id, gradeId);
    }

    @DeleteMapping
    @Secured(WRITE_TEACHER)
    public void delete(@RequestParam Long id) {
        teacherService.delete(id);
    }

    //subjectTeacher crud
    @GetMapping("/subjectTeacherList")
    @Secured(READ_SUBJECT_TEACHER)
    public List<TeacherResponse> subjectTeacherList() {
        return teacherService.findAllsubjectTeacherList().stream().map(TeacherResponse::new).collect(Collectors.toList());
    }

    @DeleteMapping("/deleteSubjectTeacher")
    @Secured(WRITE_SUBJECT_TEACHER)
    public void deleteSubjectTeacher(@RequestParam Long id) {
        teacherService.deleteSubjectTeacher(id);
    }

    @GetMapping("/gradeByTeacher")
    @Secured(READ_SUBJECT_TEACHER)
    public TeacherGradeResponse getAllTeacherInfo() {
        return teacherService.getAllTeacherInfo();
    }
}
