package com.synlabs.intscale.controller.auth;

import com.synlabs.intscale.entity.user.Role;
import com.synlabs.intscale.service.UserService;
import com.synlabs.intscale.view.PrivilegeResponse;
import com.synlabs.intscale.view.RoleRequest;
import com.synlabs.intscale.view.RoleResponse;
import com.synlabs.intscale.view.RoleSummaryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.READ_PRIVILEGE;
import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.READ_ROLE;
import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.WRITE_ROLE;

@RestController
@RequestMapping("/api/role/")
public class RoleController
{

  @Autowired
  private UserService userService;

  @GetMapping
  @Secured(READ_ROLE)
  public List<Role> roles()
  {
    return userService.getRoles();
  }

  @GetMapping("/roleSummary")
  @Secured(READ_ROLE)
  public List<RoleSummaryResponse> rolesNameAndId()
  {
    return userService.getRoles().stream().map(RoleSummaryResponse::new).collect(Collectors.toList());
  }

  @GetMapping("/allRoles")
  @Secured(READ_ROLE)
  public List<RoleResponse> allRoles()
  {
    return userService.getRoles().stream().map(RoleResponse::new).collect(Collectors.toList());
  }

  @PostMapping
  @Secured(WRITE_ROLE)
  public RoleResponse createRole(@RequestBody RoleRequest request)
  {
    return new RoleResponse(userService.saveRole(request));
  }

  @PutMapping
  @Secured(WRITE_ROLE)
  public RoleResponse updateRole(@RequestBody RoleRequest request)
  {
    return new RoleResponse(userService.updateRole(request));
  }

  @DeleteMapping("/{roleId}")
  @Secured(WRITE_ROLE)
  public void deleteRole(@PathVariable Long roleId)
  {
    userService.deleteRole(new RoleRequest(roleId));
  }

  @GetMapping("privileges")
  @Secured(READ_PRIVILEGE)
  public List<PrivilegeResponse> privileges()
  {
    return userService.getPrivileges().stream().map(PrivilegeResponse::new).collect(Collectors.toList());
  }


}
