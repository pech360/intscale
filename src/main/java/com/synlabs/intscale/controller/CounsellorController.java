package com.synlabs.intscale.controller;

import com.synlabs.intscale.entity.counselling.CounsellingSession;
import com.synlabs.intscale.enums.CounsellingSessionType;
import com.synlabs.intscale.service.CounsellorService;
import com.synlabs.intscale.view.DashboardRequest;
import com.synlabs.intscale.view.UserResponseForCounsellor;
import com.synlabs.intscale.view.counselling.CounsellingSessionNoteRequest;
import com.synlabs.intscale.view.counselling.CounsellingSessionNoteResponse;
import com.synlabs.intscale.view.counselling.CounsellingSessionRequest;
import com.synlabs.intscale.view.counselling.CounsellingSessionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.READ_COUNSELLOR_KNOWLEDGE_BASE;
import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.READ_COUNSELLOR_STUDENT;
import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.WRITE_REPORTS;

@RestController
@RequestMapping("/api/counsellor/")
public class CounsellorController
{

  @Autowired
  private CounsellorService counsellorService;

  @PostMapping("counsellorNote")
  @Secured(WRITE_REPORTS)
  public CounsellingSessionNoteResponse createCounsellorNote(@RequestBody CounsellingSessionNoteRequest request)
  {
    return counsellorService.createCounsellorNote(request);
  }

  @GetMapping("counsellorNotes")
  @Secured(WRITE_REPORTS)
  public List<CounsellingSessionNoteResponse> getcounsellorNotes(@RequestParam CounsellingSessionType type)
  {
    return counsellorService.getCounsellorNotes(type);
  }

  @PostMapping("counsellingSession")
  @Secured(READ_COUNSELLOR_STUDENT)
  public CounsellingSessionResponse createCounsellingSession(@RequestBody CounsellingSessionRequest request){
    return counsellorService.createCounsellingSession(request);
  }

  @GetMapping("counsellingSessions")
  @Secured(READ_COUNSELLOR_STUDENT)
  public List<CounsellingSessionResponse> getCounsellingSessions(@RequestParam CounsellingSessionType type,@RequestParam Long userId)
  {
    return counsellorService.getCounsellingSessionsByCurrentCounsellor(type, userId);
  }

  @GetMapping("leads")
  @Secured(READ_COUNSELLOR_STUDENT)
  public List<String> getLeads(@RequestParam Long userId)
  {
    return counsellorService.getAllLeadsOfUser(userId);
  }


  @PostMapping("/listByCounsellorAdmin")
  @Secured(READ_COUNSELLOR_KNOWLEDGE_BASE)
  public List<UserResponseForCounsellor> listByCounsellorAdmin(@RequestBody DashboardRequest request){
    return counsellorService.findAllForCounsellorAdmin(request);
  }

}
