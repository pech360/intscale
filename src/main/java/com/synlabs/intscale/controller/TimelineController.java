package com.synlabs.intscale.controller;

import com.synlabs.intscale.entity.Timeline;
import com.synlabs.intscale.service.TimelineService;
import com.synlabs.intscale.view.TimelineRequest;
import com.synlabs.intscale.view.TimelineResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.TAKE_TEST;

@RestController
@RequestMapping("/api/timeline")
public class TimelineController {
    @Autowired
    private TimelineService timelineService;

    @GetMapping()
    @Secured(TAKE_TEST)
    public List<TimelineResponse> getTimeline() {
        return timelineService.getTimeLine();
    }

    @PostMapping("/savePost")
    @Secured(TAKE_TEST)
    public List<TimelineResponse> savePost(@RequestBody TimelineRequest request) {
        return timelineService.savePost(request);
    }

    @DeleteMapping("/{id}")
    @Secured(TAKE_TEST)
    public List<TimelineResponse> deletePost(@PathVariable("id") Long id) {
        timelineService.deletePost(id);
        return timelineService.getTimeLine();
    }

    @GetMapping("/getPost/{id}")
    @Secured(TAKE_TEST)
    public TimelineResponse getPost(@PathVariable Long id) {
        return timelineService.getPost(id);
    }

    @GetMapping("/getImagePosts")
    @Secured(TAKE_TEST)
    public List<TimelineResponse> getImagePosts() {
        return timelineService.getImagePosts();
    }

    @GetMapping("/getThoughtPosts")
    @Secured(TAKE_TEST)
    public List<TimelineResponse> getThoughtPosts() {
        return timelineService.getThoughtPosts();
    }

    @PostMapping("/sharePostOnEmail")
    @Secured(TAKE_TEST)
    public void sharePostOnEmail(@RequestParam(required = true) Long id, @RequestParam(required = true) String email, @RequestParam(required = false) String body) {
        timelineService.sharePostOnEmail(id, email, body);
    }
}
