package com.synlabs.intscale.controller;



import com.synlabs.intscale.config.BaseConfig;
import com.synlabs.intscale.service.ReportService;
import com.synlabs.intscale.view.JobExecutorRequest;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.sql.SQLException;

@RestController
public class JobController extends BaseConfig {

    @Autowired
    ReportService reportService;

    @PostMapping("/api/job/{jobName}/{email}")
    public void run(@PathVariable("jobName") String name, @PathVariable("email") boolean email, @RequestBody JobExecutorRequest request) throws ClassNotFoundException, IOException, InvalidFormatException, SQLException {

        reportService.testingScheduler();
    }
}
