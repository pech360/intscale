package com.synlabs.intscale.controller;

import com.synlabs.intscale.controller.storage.RawStorageController;
import com.synlabs.intscale.ex.ValidationException;
import com.synlabs.intscale.service.SponsorService;
import com.synlabs.intscale.service.UserService;
import com.synlabs.intscale.store.FileStore;
import com.synlabs.intscale.view.CommonResponse;
import com.synlabs.intscale.view.SponsorUserAssignRequest;
import com.synlabs.intscale.view.masterdata.SponsorRequest;
import com.synlabs.intscale.view.masterdata.SponsorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.*;

@RestController
@RequestMapping("/api/sponsor/")
public class SponsorController {

    @Autowired
    private SponsorService sponsorService;
    @Autowired
    private UserService userService;


    @GetMapping("list")
    @Secured(WRITE_MASTERDATA)
    public List<SponsorResponse> list() {
        return sponsorService.list();
    }

    @PostMapping("save")
    @Secured(WRITE_MASTERDATA)
    public SponsorResponse save(@RequestBody SponsorRequest request) {
        return sponsorService.save(request);
    }

    @PostMapping("image/upload")
    @Secured(WRITE_MEDIA)
    public void uploadSponsorLogo(@RequestParam("file") MultipartFile file,
                                  @RequestParam("id") Long id,
                                  @RequestParam("imageType") String imageType
                                  ) {
        sponsorService.uploadSponsorLogo(file, id, imageType);
    }

    @PostMapping("assign/toUser")
    @Secured(WRITE_USER)
    public void assignSponsorsToUsersBySearch(@RequestBody SponsorUserAssignRequest request) {
        sponsorService.assignSponsorsToUsers(request);
    }

}


