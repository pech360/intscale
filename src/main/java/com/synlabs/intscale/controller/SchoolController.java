package com.synlabs.intscale.controller;

import com.synlabs.intscale.service.SchoolService;
import com.synlabs.intscale.view.DashboardRequest;
import com.synlabs.intscale.view.SchoolRequest;
import com.synlabs.intscale.view.SchoolResponse;
import com.synlabs.intscale.view.leap.OverAllTestResultResponse;
import com.synlabs.intscale.view.leap.TestResultSummaryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.synlabs.intscale.auth.IntScaleAuth.Privileges.*;

@RestController
@RequestMapping("/api/school")
public class SchoolController {
    @Autowired
    SchoolService schoolService;

    @PostMapping
    @Secured(WRITE_SCHOOL)
    public SchoolResponse saveSchool(@RequestBody SchoolRequest request) {
        return new SchoolResponse(schoolService.saveSchool(request));
    }

    @GetMapping("/{id}")
    @Secured(WRITE_SCHOOL)
    public SchoolResponse getSchool(@PathVariable("id") Long id) {
        return new SchoolResponse(schoolService.getSchool(id));
    }

    @GetMapping("/all")
    @Secured(READ_SCHOOL)
    public List<SchoolResponse> getAllSchools() {
        return schoolService.getAllSchools().stream().map(SchoolResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/allByUser")
    @Secured(READ_SCHOOL)
    public List<SchoolResponse> getAllSchoolsByUser() {
        return schoolService.getAllSchoolsByUser();
    }

    @PutMapping
    @Secured(WRITE_SCHOOL)
    public SchoolResponse updateSchool(@RequestBody SchoolRequest request) {
        return new SchoolResponse(schoolService.updateSchool(request));
    }

    @DeleteMapping("/{id}")
    @Secured(WRITE_SCHOOL)
    public void deleteSchool(@PathVariable("id") Long schoolId) {
        schoolService.deleteSchool(schoolId);
    }

    @PostMapping("/overAllTestResult/grades")
    @Secured(READ_SUBJECT_TEACHER)
    public Map<String, List<OverAllTestResultResponse>> getOverAllTestResultForGrades(@RequestBody DashboardRequest request) {
        return schoolService.getOverAllTestResultForGrades(request);
    }

    @PostMapping("/overAllTestResult/sections")
    @Secured(READ_SUBJECT_TEACHER)
    public Map<String, List<OverAllTestResultResponse>> getOverAllTestResultForSections(@RequestBody DashboardRequest request) {
        return schoolService.getOverAllTestResultForSections(request);
    }

    @PostMapping("/overAllTestResult/grade/section")
    @Secured(READ_SUBJECT_TEACHER)
    public Map<String, List<OverAllTestResultResponse>> getOverAllTestResultForGradeAndSection(@RequestBody DashboardRequest request) {
        return schoolService.getOverAllTestResultForGradeAndSectionChapterWise(request);
    }

    @PostMapping("/overAllTestResult/grade/section/user/construct")
    @Secured(READ_SUBJECT_TEACHER)
    public List<TestResultSummaryResponse> getOverAllTestResultForGradeAndSectionAndUserAndConstruct(@RequestBody DashboardRequest request) {
        return schoolService.getOverAllTestResultForGradeAndSectionAndUserAndConstruct(request);
    }

    @PostMapping("/overAllTestResult/grade/section/usersList")
    @Secured(READ_SUBJECT_TEACHER)
    public Map<Long, List<TestResultSummaryResponse>> getOverAllTestResultForGradeAndSectionUserList(@RequestBody DashboardRequest request) {
        return schoolService.getOverAllTestResultForGradeAndSectionsUserList(request);
    }

    @PostMapping("/overAllTestResult/grade/section/constructList")
    @Secured(READ_SUBJECT_TEACHER)
    public List<String> getOverAllTestResultForGradeAndSectionConstructList(@RequestBody DashboardRequest request) {
        return schoolService.getOverAllTestResultForGradeAndSectionConstructList(request);
    }

    @PostMapping("/overAllTestResult/grade/section/construct/attendanceSheet")
    @Secured(READ_TEACHER_DASHBOARD)
    public List<Map<Long, String>> getAttendanceSheetByGradeAndSectionAndConstruct(@RequestBody DashboardRequest request) {
        return schoolService.getAttendanceSheetByGradeAndSectionAndConstruct(request);
    }

    @GetMapping("/getGradesByTeacher")
    @Secured(READ_SUBJECT_TEACHER)
    public Set<String> getGradesByTeacher() {
        return schoolService.getGradesByTeacher();
    }

    @GetMapping("/getSectionsByTeacher")
    @Secured(READ_SUBJECT_TEACHER)
    public Set<String> getSectionsByTeacher() {
        return schoolService.getSectionsByTeacher();
    }

    @PostMapping("/getSubjectsByTeacher")
    @Secured(READ_SUBJECT_TEACHER)
    public Map<Long, String> getSubjectByTeacher(@RequestBody DashboardRequest request) {
        String grade = "";
        String section = "";
        for (String s : request.getSections()) {
            section = s;
        }
        for (String g : request.getGrades()) {
            grade = g;
        }

        if (request.getGrades().iterator().hasNext()) {
            section = request.getSections().iterator().next();
        }
        return schoolService.getSubjectsByTeacher(grade, section);
    }

    @PostMapping("/getNumberOfStudentsByGrade")
    @Secured(READ_SUBJECT_TEACHER)
    public Map<String, Map<String, Long>> getNumberOfStudentsByGrade(@RequestBody DashboardRequest request) {
        return schoolService.getNumberOfStudentsByGrade(request);
    }

    @GetMapping("/getSchoolNamesFromUserTable")
    @Secured(READ_MASTERDATA)
    public List<String> getSchoolNamesFromUserTable() {
        return schoolService.getSchoolNamesFromUserTable();
    }

}
