package com.synlabs.intscale.enums;

/**
 * Created by India on 4/19/2018.
 */
public enum DashboardType
{
  SUPER_ADMIN("SuperAdmin"),
  OPERATION_HEAD("OperationHead"),
  OPERATION_MANAGER("OperationManager"),
  SCHOOL_HEAD("SchoolHead"),
  FIELD_MANAGER("FieldManager"),
  MANAGEMENT("Management"),
  TEACHER("Teacher");

  private final String type;

  DashboardType(String type)
  {
    this.type = type;
  }

  public String getType()
  {
    return type;
  }
}
