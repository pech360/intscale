package com.synlabs.intscale.enums;
//LNT
public enum LearningMode {

	ClearlyVerbal , ModeratelyVerbal , Flexible , ModeratelyVisual , ClearlyVisual
}
