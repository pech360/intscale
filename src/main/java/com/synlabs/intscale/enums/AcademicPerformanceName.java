package com.synlabs.intscale.enums;

public enum AcademicPerformanceName
{
  SCIENCE,
  MATHEMATICS
}
