package com.synlabs.intscale.enums;

public enum AimReportContentType
{
  Category,
  ReportSection,
  Career,
  Subject,
  Leisure,
  Stream,
  //LNT
  LearningMode,
  ThinkingStyle,
  LearningEnvironment

}
