package com.synlabs.intscale.enums;

public enum CounsellingSessionStatus
{
  COMPLETED,
  UPCOMING,
  MISSED,
  DELAYED,
  DRAFTED
}
