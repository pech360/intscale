package com.synlabs.intscale.enums;

public enum  CounsellingSessionType
{
  EXPLORE,
  COMMIT,
  ENGAGE,
  FREE_FLOW
}
