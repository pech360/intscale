package com.synlabs.intscale.enums;

public enum QuestionType
{
  MCQ("multiple choice question"),
  IMAGEQ("image question"),
  AUDIOQ("audio question"),
  PARAQ("paragraph question"),
  GROUPQ("group question"),
  TEXTQ("text question"),
  SLIDERQ("Slider"),
  FILLINTHEBLANKSQ("FillInTheBlanks"),
  WORDCHOICEQ("WordChoice");
  final private String type;

  QuestionType(String type) {
    this.type = type;
  }
  public String getType() {
    return type;
  }
}
