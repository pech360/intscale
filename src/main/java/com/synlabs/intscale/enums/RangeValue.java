package com.synlabs.intscale.enums;

public enum RangeValue
{
  VeryHigh,
  High,
  Average,
  Low,
  VeryLow,
  Moderate,
  A,
  B,
  C
}

