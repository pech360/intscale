package com.synlabs.intscale.enums;

public enum TagType
{
  QUESTION,
  COUNSELLING_SESSION
}
