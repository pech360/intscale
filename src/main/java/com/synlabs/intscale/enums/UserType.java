package com.synlabs.intscale.enums;

public enum UserType
{
  Student,
  Stakeholder,
  Parent,
  Teacher,
  INTSCALE;

  public static boolean matches(String userType)
  {
    for (UserType ut : UserType.values())
    {
      if (ut.name().equals(userType))
      {
        return true;
      }
    }
    return false;
  }
}
