package com.synlabs.intscale.enums;

public enum StakeholderTestStatus
{
  Sent, InProgress, Finished, Created
}
