package com.synlabs.intscale.enums;

public enum TimelinePostType
{
  Test,
  Image,
  Thought
}
