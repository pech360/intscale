package com.synlabs.intscale.enums;

public enum NotificationType
{
  STAKEHOLDER_SEND, STAKEHOLDER_REMIND, STAKEHOLDER_VERIFICATION
}
