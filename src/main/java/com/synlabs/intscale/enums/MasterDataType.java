package com.synlabs.intscale.enums;

public enum MasterDataType
{
  Language,
  Gender,
  UserType,
  StudentType,
  Grade,
  Subject,
  Degree,
  Stream,
  Interest,
  Hobbies,
  Designation,
  Industry,
  Tag
}
