package com.synlabs.intscale.enums;

public enum ReportStatus
{
  Wait, Generating, Generated, Failed, InProgress, Queued
}
