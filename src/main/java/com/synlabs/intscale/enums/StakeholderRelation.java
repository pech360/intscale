package com.synlabs.intscale.enums;

public enum StakeholderRelation
{
  Parent, Peer, Teacher, Sibling
}
