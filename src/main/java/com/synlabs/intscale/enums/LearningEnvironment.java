package com.synlabs.intscale.enums;
//LNT
public enum LearningEnvironment {

   ClearlyAcademic , ModeratelyAcademic , Flexible , ModeratelyPractical , ClearlyPractical
}
