package com.synlabs.intscale.enums;

public enum RegisterType
{
  GOOGLE("Google"),
  FACEBOOK("Facebook"),
  SELF("Self"),
  MANUALLY("Manually");

  private final String type;

  private RegisterType(String type)
  {
    this.type = type;
  }

  public String getType()
  {
    return this.type;
  }
}
