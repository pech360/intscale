package com.synlabs.intscale.enums;

public enum EventType
{
  //Stakeholder Email Events
  StakeholderSentLinkEmail, StakeholderRemindEmail, StakeholderVerificationEmail

}
