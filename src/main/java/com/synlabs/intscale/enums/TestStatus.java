package com.synlabs.intscale.enums;

/**
 * Created by India on 10/3/2017.
 */
public enum TestStatus {
    TNA("test not assigned"),
    NTA("new test assigned"),
    TIA("test in attempted"),
    TCS("test completed successfully"),
    ATT("already taken test");

    final private String status;

    TestStatus(String status) {
        this.status = status;
    }
    public String getStatus() {
        return status;
    }
}
