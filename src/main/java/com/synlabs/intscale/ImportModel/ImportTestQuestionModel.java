package com.synlabs.intscale.ImportModel;

import org.springframework.util.StringUtils;

public class ImportTestQuestionModel
{
  private String testName;
  private Long testVersion;
  private Long questionId;
  private String category;
  private String subCategory;

  public String getTestName()
  {
    return testName;
  }

  public void setTestName(String testName)
  {
    this.testName = testName;
  }

  public Long getTestVersion()
  {
    return testVersion;
  }

  public void setTestVersion(Long testVersion)
  {
    this.testVersion = testVersion;
  }

  public Long getQuestionId()
  {
    return questionId;
  }

  public void setQuestionId(Long questionId)
  {
    this.questionId = questionId;
  }

  public String getCategory()
  {
    return category;
  }

  public void setCategory(String category)
  {
    this.category = category;
  }

  public String getSubCategory()
  {
    return subCategory;
  }

  public void setSubCategory(String subCategory)
  {
    this.subCategory = subCategory;
  }

  public String validate()
  {
    if (StringUtils.isEmpty(this.testName))
    {
      return "Test Name can't be empty.";
    }
     if (StringUtils.isEmpty(this.testVersion))
    {
      return "Test version can't be empty.";
    }
     if (StringUtils.isEmpty(this.questionId))
    {
      return "QuestionId can't be empty.";
    }
     if (StringUtils.isEmpty(this.category))
    {
      return "Category can't be empty.";
    }
     if (StringUtils.isEmpty(this.subCategory))
    {
      return "SubCategory can't be empty.";
    }

    return "OK";
  }
}
