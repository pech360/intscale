package com.synlabs.intscale.ImportModel;

import org.springframework.util.StringUtils;

public class ImportTestModel
{

  private String name;
  private String description;
  private String introduction;
  private String procedure;
  private String participation;
  private String disclaimer;
  private String notes;
  private String grades;

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public String getIntroduction()
  {
    return introduction;
  }

  public void setIntroduction(String introduction)
  {
    this.introduction = introduction;
  }

  public String getProcedure()
  {
    return procedure;
  }

  public void setProcedure(String procedure)
  {
    this.procedure = procedure;
  }

  public String getParticipation()
  {
    return participation;
  }

  public void setParticipation(String participation)
  {
    this.participation = participation;
  }

  public String getDisclaimer()
  {
    return disclaimer;
  }

  public void setDisclaimer(String disclaimer)
  {
    this.disclaimer = disclaimer;
  }

  public String getNotes()
  {
    return notes;
  }

  public void setNotes(String notes)
  {
    this.notes = notes;
  }

  public String getGrades()
  {
    return grades;
  }

  public void setGrades(String grades)
  {
    this.grades = grades;
  }

  public String validate()
  {
    if (StringUtils.isEmpty(this.name))
    {
      return "Test's name can't be empty.";
    }

    if (StringUtils.isEmpty(this.description))
    {
      return "Test's description can't be empty.";
    }

    if (StringUtils.isEmpty(this.introduction))
    {
      return "Test's introduction can't be empty.";
    }

    if (StringUtils.isEmpty(this.procedure))
    {
      return "Test's procedure can't be empty.";
    }

    if (StringUtils.isEmpty(this.participation))
    {
      return "Test's participation can't be empty.";
    }

    if (StringUtils.isEmpty(this.disclaimer))
    {
      return "Test's disclaimer can't be empty.";
    }

    if (StringUtils.isEmpty(this.notes))
    {
      return "Test's notes can't be empty.";
    }

    if(StringUtils.isEmpty(this.grades))
    {
      return "Test's grades can't be empty";
    }
    return "OK";
  }
}
