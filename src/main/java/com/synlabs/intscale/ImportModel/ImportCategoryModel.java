package com.synlabs.intscale.ImportModel;

import org.springframework.util.StringUtils;

public class ImportCategoryModel
{
  private String categoryName;
  private String categoryDescription;
  private String parent;
  private String color;

  public String getCategoryName()
  {
    return categoryName;
  }

  public void setCategoryName(String categoryName)
  {
    this.categoryName = categoryName;
  }

  public String getCategoryDescription()
  {
    return categoryDescription;
  }

  public void setCategoryDescription(String categoryDescription)
  {
    this.categoryDescription = categoryDescription;
  }

  public String getParent()
  {
    return parent;
  }

  public void setParent(String parent)
  {
    this.parent = parent;
  }

  public String getColor()
  {
    return color;
  }

  public void setColor(String color)
  {
    this.color = color;
  }

  public String validate()
  {
    if (StringUtils.isEmpty(this.categoryName))
    {
      return "Category name can't be empty.";
    }

    if (StringUtils.isEmpty(this.categoryDescription))
    {
      return "Category description can't be empty.";
    }

    if (StringUtils.isEmpty(this.parent))
    {
      return "Category parent can't be empty.";
    }

    return "OK";
  }
}
