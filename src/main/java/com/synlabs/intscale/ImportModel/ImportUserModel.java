package com.synlabs.intscale.ImportModel;

import com.synlabs.intscale.entity.masterdata.MasterDataDocument;
import com.synlabs.intscale.util.StringUtil;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

public class ImportUserModel
{
  private String username;
  private String password;
  private String email;
  private String name;
  private String gender;
  private Date   dob;
  private String grade;
  private String section;
  private String language;
  private String school;
  private String city;
  private String stream;
  private String academicScore;
  private String interests;
  private Integer science;
  private Integer maths;
  private String higherEducation;
  private String contactNumber;


public String getUsername()
  {
    return username;
  }

  public void setUsername(String username)
  {
    this.username = username;
  }

  public String getPassword()
  {
    return password;
  }

  public void setPassword(String password)
  {
    this.password = password;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getContactNumber() {
    return contactNumber;
  }

  public void setContactNumber(String contactNumber) {
    this.contactNumber = contactNumber;
  }

  public String getGender()
  {
    return gender;
  }

  public void setGender(String gender)
  {
    this.gender = gender;
  }

  public Date getDob()
  {
    return dob;
  }

  public void setDob(Date dob)
  {
    this.dob = dob;
  }

  public String getGrade()
  {
    return grade;
  }

  public void setGrade(String grade)
  {
    this.grade = grade;
  }

  public String getSection()
  {
    return section;
  }

  public void setSection(String section)
  {
    this.section = section;
  }

  public String getLanguage()
  {
    return language;
  }

  public void setLanguage(String language)
  {
    this.language = language;
  }

  public String getSchool()
  {
    return school;
  }

  public void setSchool(String school)
  {
    this.school = school;
  }

  public String getCity()
  {
    return city;
  }

  public void setCity(String city)
  {
    this.city = city;
  }

  public String getStream()
  {
    return stream;
  }

  public void setStream(String stream)
  {
    this.stream = stream;
  }

  public String getAcademicScore()
  {
    return academicScore;
  }

  public void setAcademicScore(String academicScore)
  {
    this.academicScore = academicScore;
  }

  public String getInterests()
  {
    return interests;
  }

  public void setInterests(String interests)
  {
    this.interests = interests;
  }

  public Integer getScience() {
    return science;
  }

  public void setScience(Integer science) {
    this.science = science;
  }

  public Integer getMaths() {
    return maths;
  }

  public void setMaths(Integer maths) {
    this.maths = maths;
  }

  public String getHigherEducation() {
    return higherEducation;
  }

  public void setHigherEducation(String higherEducation) {
    this.higherEducation = higherEducation;
  }

  public String validate(List<MasterDataDocument> streams )
  {  
	  
    if (StringUtils.isEmpty(this.username))
    {
      return "Username can't be empty.";
    }

    if(!StringUtils.isEmpty(this.name)){

      this.name = StringUtil.capitalizeWord(this.name);
    }

    if (StringUtils.isEmpty(this.password))
    {
      return "Password can't be empty.";
    }

    if (this.password.trim().length() < 6)
    {
      return "Password should be min 6 characters";
    }

//    if (StringUtils.isEmpty(this.school))
//    {
//      return "School can't be null.";
//    }
//
//    if (StringUtils.isEmpty(this.city))
//    {
//      return "City can't be null.";
//    }
//
//    if(!StringUtils.isEmpty(this.stream)){
//      boolean isStreamMatched = streams.stream().anyMatch(s -> s.getValue().equals(this.stream));
//      if(!isStreamMatched){
//        return "Provided stream doesn't matched with any existing stream";
//      }
//    }

//    if(!StringUtils.isEmpty(this.grade)){
//      if(this.grade.equals("9") || this.grade.equals("10") ){
//        if(!StringUtils.isEmpty(this.science)){
//          if(this.science<0 ||this.science>100 ){
//            return "Science score must be between 0 to 100.";
//          }
//        }
//        if(!StringUtils.isEmpty(this.maths)){
//          if(this.maths<0 ||this.maths>100 ){
//            return "Science score must be between 0 to 100.";
//          }
//        }
//      }
//    }
    return "OK";
  }
}
