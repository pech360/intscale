package com.synlabs.intscale.ImportModel;

import org.springframework.util.StringUtils;

public class ImportTestResponseModel
{
  private String username;
  private String name;
  private Long   testId;
  private Long   questionId;
  private Long   option;
  private String text;
  private String testStartTimeStamp;
  private String testFinishTimeStamp;

  public String getUsername()
  {
    return username;
  }

  public void setUsername(String username)
  {
    this.username = username;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public Long getTestId()
  {
    return testId;
  }

  public void setTestId(Long testId)
  {
    this.testId = testId;
  }

  public Long getQuestionId()
  {
    return questionId;
  }

  public void setQuestionId(Long questionId)
  {
    this.questionId = questionId;
  }

  public Long getOption()
  {
    return option;
  }

  public void setOption(Long option)
  {
    this.option = option;
  }

  public String getText()
  {
    return text;
  }

  public void setText(String text)
  {
    this.text = text;
  }

  public String getTestStartTimeStamp()
  {
    return testStartTimeStamp;
  }

  public void setTestStartTimeStamp(String testStartTimeStamp)
  {
    this.testStartTimeStamp = testStartTimeStamp;
  }

  public String getTestFinishTimeStamp()
  {
    return testFinishTimeStamp;
  }

  public void setTestFinishTimeStamp(String testFinishTimeStamp)
  {
    this.testFinishTimeStamp = testFinishTimeStamp;
  }

  public String validate()
  {
    if (StringUtils.isEmpty(this.name))
    {
      return "missing name";
    } if (StringUtils.isEmpty(this.username))
    {
      return "missing username";
    } if (StringUtils.isEmpty(this.testId))
    {
      return "missing testId";
    } if (StringUtils.isEmpty(this.questionId))
    {
      return "missing questionId";
    } if (StringUtils.isEmpty(this.option))
    {
      return "missing option";
    }

    return "OK";
  }
}
