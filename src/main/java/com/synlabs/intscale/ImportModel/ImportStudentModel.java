package com.synlabs.intscale.ImportModel;

import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * Created by India on 1/17/2018.
 */
public class ImportStudentModel
{
  private String name;
  private String username;
  private String email;
  private String gender;
  private String section;
  private String grade;
  private String school;
  private Date   dob;

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getUsername()
  {
    return username;
  }

  public void setUsername(String username)
  {
    this.username = username;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public String getGender()
  {
    return gender;
  }

  public void setGender(String gender)
  {
    this.gender = gender;
  }

  public String getSection()
  {
    return section;
  }

  public void setSection(String section)
  {
    this.section = section;
  }

  public String getGrade()
  {
    return grade;
  }

  public void setGrade(String grade)
  {
    this.grade = grade;
  }

  public String getSchool()
  {
    return school;
  }

  public void setSchool(String school)
  {
    this.school = school;
  }

  public Date getDob()
  {
    return dob;
  }

  public void setDob(Date dob)
  {
    this.dob = dob;
  }

  public String validate()
  {
    if (StringUtils.isEmpty(this.name))
    {
      return "Name can't be empty.";
    }
    if (StringUtils.isEmpty(this.username))
    {
      return "Username can't be empty.";
    }
    if (StringUtils.isEmpty(this.email))
    {
      return "Email can't be empty.";
    }
    if (StringUtils.isEmpty(this.gender))
    {
      return "Gender can't be empty.";
    }
    if (this.section == null)
    {
      return "Section can't be null.";
    }
    if (this.school == null)
    {
      return "School can't be null.";
    }

    if (this.dob == null)
    {
      return "Date of Birth can't be null.";
    }

    return "OK";
  }
}
