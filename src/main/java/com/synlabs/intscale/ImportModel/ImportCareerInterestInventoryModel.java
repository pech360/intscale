package com.synlabs.intscale.ImportModel;

import com.synlabs.intscale.entity.Report.CareerInterestInventory;
import org.springframework.util.StringUtils;

public class ImportCareerInterestInventoryModel {

    private String careerName;
    private String subjects;  // comma separated subjects
    private int region;
    private int r;
    private int i;
    private int a;
    private int s;
    private int e;
    private int c;

    public String validate() {
        if (StringUtils.isEmpty(this.careerName)) {
            return "Empty Career name";

        }
        if (this.region <= 0) {
            return "Region must be greater than 0";
        }
        return "OK";
    }

    public CareerInterestInventory toEntity(CareerInterestInventory careerInterest) {

        if (careerInterest == null) {
            careerInterest = new CareerInterestInventory();
        }
        careerInterest.setCareerName(this.careerName.trim());
        careerInterest.setSubjects(this.subjects.trim());
        careerInterest.setR(this.r);
        careerInterest.setI(this.i);
        careerInterest.setA(this.a);
        careerInterest.setS(this.s);
        careerInterest.setE(this.e);
        careerInterest.setC(this.c);
        careerInterest.setRegion(this.region);
        return careerInterest;
    }

    public ImportCareerInterestInventoryModel() {
    }

    public ImportCareerInterestInventoryModel(CareerInterestInventory careerInterestInventory) {
        this.careerName = careerInterestInventory.getCareerName();
        this.subjects = careerInterestInventory.getSubjects();
        this.region = careerInterestInventory.getRegion();
        this.r = careerInterestInventory.getR();
        this.i = careerInterestInventory.getI();
        this.a = careerInterestInventory.getA();
        this.s = careerInterestInventory.getS();
        this.e = careerInterestInventory.getE();
        this.c = careerInterestInventory.getC();
    }

    public String getCareerName() {
        return careerName;
    }

    public void setCareerName(String careerName) {
        this.careerName = careerName;
    }

    public String getSubjects() {
        return subjects;
    }

    public void setSubjects(String subjects) {
        this.subjects = subjects;
    }

    public int getRegion() {
        return region;
    }

    public void setRegion(int region) {
        this.region = region;
    }

    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getS() {
        return s;
    }

    public void setS(int s) {
        this.s = s;
    }

    public int getE() {
        return e;
    }

    public void setE(int e) {
        this.e = e;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }
}
