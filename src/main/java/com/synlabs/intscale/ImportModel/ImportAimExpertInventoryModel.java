package com.synlabs.intscale.ImportModel;

import org.springframework.util.StringUtils;

public class ImportAimExpertInventoryModel
{
  private String name;
  private String displayName;
  private String description;
  private String summary;
  private String tasks;
  private String scope;
  private String topRecruiters;
  private String education;
  private String topCollegesOfIndia;
  private String topCollegesOfAbroad;
  private String streams;

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getDisplayName()
  {
    return displayName;
  }

  public void setDisplayName(String displayName)
  {
    this.displayName = displayName;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public String getSummary()
  {
    return summary;
  }

  public void setSummary(String summary)
  {
    this.summary = summary;
  }

  public String getTasks()
  {
    return tasks;
  }

  public void setTasks(String tasks)
  {
    this.tasks = tasks;
  }

  public String getScope()
  {
    return scope;
  }

  public void setScope(String scope)
  {
    this.scope = scope;
  }

  public String getTopRecruiters()
  {
    return topRecruiters;
  }

  public void setTopRecruiters(String topRecruiters)
  {
    this.topRecruiters = topRecruiters;
  }

  public String getEducation()
  {
    return education;
  }

  public void setEducation(String education)
  {
    this.education = education;
  }

  public String getTopCollegesOfIndia()
  {
    return topCollegesOfIndia;
  }

  public void setTopCollegesOfIndia(String topCollegesOfIndia)
  {
    this.topCollegesOfIndia = topCollegesOfIndia;
  }

  public String getTopCollegesOfAbroad()
  {
    return topCollegesOfAbroad;
  }

  public void setTopCollegesOfAbroad(String topCollegesOfAbroad)
  {
    this.topCollegesOfAbroad = topCollegesOfAbroad;
  }

  public String getStreams()
  {
    return streams;
  }

  public void setStreams(String streams)
  {
    this.streams = streams;
  }

  public String validate()
  {
    if (StringUtils.isEmpty(this.name))
    {
      return "Name can't be empty.";
    }

    return "OK";
  }
}
