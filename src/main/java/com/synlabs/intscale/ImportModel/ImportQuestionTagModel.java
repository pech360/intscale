package com.synlabs.intscale.ImportModel;

import org.springframework.util.StringUtils;

public class ImportQuestionTagModel
{

  private String tagName;
  private String parentTagName;

  public String getTagName()
  {
    return tagName;
  }

  public void setTagName(String tagName)
  {
    this.tagName = tagName;
  }

  public String getParentTagName()
  {
    return parentTagName;
  }

  public void setParentTagName(String parentTagName)
  {
    this.parentTagName = parentTagName;
  }

  public String validate()    {
    if(StringUtils.isEmpty(this.tagName)){
      return "Tag Name can not br empty";
    }

    if(StringUtils.isEmpty(this.parentTagName)){
      return "ParenTag can not be empty";
    }

    return "OK";
  }
}
