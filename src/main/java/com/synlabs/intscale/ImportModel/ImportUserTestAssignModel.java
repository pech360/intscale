package com.synlabs.intscale.ImportModel;

/**
 * Created by India on 4/2/2018.
 */
public class ImportUserTestAssignModel {
    private String username;
    private String product;
    private String language;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
