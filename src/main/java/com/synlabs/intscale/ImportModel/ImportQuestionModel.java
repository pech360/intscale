package com.synlabs.intscale.ImportModel;

import org.springframework.util.StringUtils;

public class ImportQuestionModel
{
  private String description;

  private String questionType;

  private String groupName;

  private String category;

  private String subCategory;

  private int numberOfAnswers;

  private int questionNumber;

  private String isReverse;

  private String hasDummy;

  private String hint;

  private String questionTag;

  private String subTag;

  private String ignoreQuestion;

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public String getQuestionType()
  {
    return questionType;
  }

  public void setQuestionType(String questionType)
  {
    this.questionType = questionType;
  }

  public String getGroupName()
  {
    return groupName;
  }

  public void setGroupName(String groupName)
  {
    this.groupName = groupName;
  }

  public String getCategory()
  {
    return category;
  }

  public void setCategory(String category)
  {
    this.category = category;
  }

  public String getSubCategory()
  {
    return subCategory;
  }

  public void setSubCategory(String subCategory)
  {
    this.subCategory = subCategory;
  }

  public int getNumberOfAnswers()
  {
    return numberOfAnswers;
  }

  public void setNumberOfAnswers(int numberOfAnswers)
  {
    this.numberOfAnswers = numberOfAnswers;
  }

  public int getQuestionNumber()
  {
    return questionNumber;
  }

  public void setQuestionNumber(int questionNumber)
  {
    this.questionNumber = questionNumber;
  }

  public String getIsReverse()
  {
    return isReverse;
  }

  public void setIsReverse(String isReverse)
  {
    this.isReverse = isReverse;
  }

  public String getHasDummy()
  {
    return hasDummy;
  }

  public void setHasDummy(String hasDummy)
  {
    this.hasDummy = hasDummy;
  }

  public String getHint()
  {
    return hint;
  }

  public void setHint(String hint)
  {
    this.hint = hint;
  }

  public String getQuestionTag()
  {
    return questionTag;
  }

  public void setQuestionTag(String questionTag)
  {
    this.questionTag = questionTag;
  }

  public String getSubTag()
  {
    return subTag;
  }

  public void setSubTag(String subTag)
  {
    this.subTag = subTag;
  }

  public String getIgnoreQuestion()
  {
    return ignoreQuestion;
  }

  public void setIgnoreQuestion(String ignoreQuestion)
  {
    this.ignoreQuestion = ignoreQuestion;
  }

  public String validate()
  {
    if (StringUtils.isEmpty(this.description))
    {
      return " Question Description can't be empty.";
    }

    if (StringUtils.isEmpty(this.questionType))
    {
      return "Question-type can't be empty.";
    }

    if (this.questionType.equalsIgnoreCase("Group"))
    {
      if (StringUtils.isEmpty(this.groupName))
      {
        return "Group name can't be empty.";
      }
    }

    if (this.questionType.equalsIgnoreCase("MCQ"))
    {
      if (StringUtils.isEmpty(this.questionNumber))
      {
        return "Question question Number can't be empty.";
      }

    }

    if (StringUtils.isEmpty(this.category))
    {
      return " Question category can't be empty.";
    }

    if (StringUtils.isEmpty(this.subCategory))
    {
      return "Question subcategory can't be empty.";
    }

    if (StringUtils.isEmpty(this.questionTag))
    {
      return "Question Parent Tag can't be empty.";
    }

    if (StringUtils.isEmpty(this.subTag))
    {
      return "Question subTag can't be empty.";
    }

    return "OK";
  }

}
