package com.synlabs.intscale.ImportModel;

import org.springframework.util.StringUtils;

public class ImportAnswerModel
{
  private String description;
  private int    marks;
  private int    questionNumber;

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public int getMarks()
  {
    return marks;
  }

  public void setMarks(int marks)
  {
    this.marks = marks;
  }

  public int getQuestionNumber()
  {
    return questionNumber;
  }

  public void setQuestionNumber(int questionNumber)
  {
    this.questionNumber = questionNumber;
  }

  public String validateForGroupQuestionAnswers()
  {
    if (StringUtils.isEmpty(this.description))
    {
      return " Answer Description can't be empty.";
    }

    if (StringUtils.isEmpty(this.marks))
    {
      return " Answer marks can't be empty.";
    }
    return "OK";
  }

  public String validateForMCQAnswers()
  {
    if (StringUtils.isEmpty(this.description))
    {
      return " Answer Description can't be empty.";
    }

    if (StringUtils.isEmpty(this.marks))
    {
      return " Answer marks can't be empty.";
    }

    if (StringUtils.isEmpty(this.questionNumber))
    {
      return " QuestionNumber can't be empty.";
    }
    return "OK";
  }

  public ImportAnswerModel(ImportAnswerModel importAnswerModel)
  {
    this.description = importAnswerModel.description;
    this.marks = importAnswerModel.marks;
    this.questionNumber = importAnswerModel.questionNumber;
  }

  public ImportAnswerModel()
  {
  }
}
