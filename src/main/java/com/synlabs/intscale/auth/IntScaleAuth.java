package com.synlabs.intscale.auth;

public final class IntScaleAuth {
    private IntScaleAuth() {
        throw new AssertionError("Not allowed");
    }

    public static final class Privileges {
    	public static final String PROMO_CODE = "ROLE_PROMO_CODE";//for creating Promo Code
    	public static final String TRANSACTION_READ_PRIVILEGE ="ROLE_TRANSACTION_READ_PRIVILEGE";
    	public static final String SELF_READ = "ROLE_SELF_READ";
        public static final String SELF_WRITE = "ROLE_SELF_WRITE";

        public static final String READ_ROLE = "ROLE_READ_ROLE";
        public static final String WRITE_ROLE = "ROLE_WRITE_ROLE";

        public static final String READ_PRIVILEGE = "ROLE_READ_PRIVILEGE";

        public static final String READ_USER = "ROLE_READ_USER";
        public static final String WRITE_USER = "ROLE_WRITE_USER";

        public static final String READ_MASTERDATA = "ROLE_READ_MASTERDATA";
        public static final String WRITE_MASTERDATA = "ROLE_WRITE_MASTERDATA";

        public static final String READ_QUESTION = "ROLE_READ_QUESTION";
        public static final String WRITE_QUESTION = "ROLE_WRITE_QUESTION";

        public static final String READ_TEST = "ROLE_READ_TEST";
        public static final String WRITE_TEST = "ROLE_WRITE_TEST";
        public static final String PUBLISH_TEST = "ROLE_PUBLISH_TEST";

        public static final String TAKE_TEST = "ROLE_TAKE_TEST";

        public static final String READ_RESULT = "ROLE_READ_RESULT";

        public static final String READ_SCHOOL = "ROLE_READ_SCHOOL";
        public static final String WRITE_SCHOOL = "ROLE_WRITE_SCHOOL";

        public static final String READ_CATEGORY = "ROLE_READ_CATEGORY";
        public static final String WRITE_CATEGORY = "ROLE_WRITE_CATEGORY";

        public static final String READ_EXPERT_ANAYSIS = "ROLE_READ_EXPERT_ANAYSIS";
        public static final String WRITE_EXPERT_ANAYSIS = "ROLE_WRITE_EXPERT_ANAYSIS";

        public static final String READ_SUBSCRIPTION_KEY = "ROLE_READ_SUBSCRIPTION_KEY";
        public static final String WRITE_SUBSCRIPTION_KEY = "ROLE_WRITE_SUBSCRIPTION_KEY";

        public static final String WRITE_MEDIA = "ROLE_WRITE_MEDIA";

        public static final String WRITE_REPORTS = "ROLE_WRITE_REPORTS";

        public static final String WRITE_USERTESTASSIGNMENT = "ROLE_WRITE_USERTESTASSIGNMENT";

        public static final String WRITE_DERIVE_CATEGORY = "ROLE_WRITE_DERIVE_CATEGORY";

        public static final String WRITE_TENATTESTALLOTMENT = "ROLE_WRITE_TENATTESTALLOTMENT";

        public static final String WRITE_QUESTION_TAG = "ROLE_WRITE_QUESTION_TAG";
        public static final String READ_QUESTION_TAG = "ROLE_READ_QUESTION_TAG";
        public static final String READ_ENGINE = "ROLE_READ_ENGINE";
        public static final String WRITE_SEQUENCE = "ROLE_WRITE_SEQUENCE";
        public static final String WRITE_LIVE = "ROLE_WRITE_LIVE";
        public static final String READ_HISTORY = "ROLE_READ_HISTORY";
        public static final String SHOW_TENANT_MANAGEMENT = "ROLE_SHOW_TENANT_MANAGEMENT";
        public static final String WRITE_TENANT_MANAGEMENT = "ROLE_WRITE_TENANT_MANAGEMENT";
        public static final String READ_TENANT_MANAGEMENT = "ROLE_READ_TENANT_MANAGEMENT";
        public static final String READ_TENANT = "ROLE_READ_TENANT";
        public static final String SHOW_STUDENT = "ROLE_SHOW_STUDENT";
        public static final String READ_STUDENT = "ROLE_READ_STUDENT";
        public static final String WRITE_STUDENT = "ROLE_WRITE_STUDENT";
        public static final String SHOW_TEACHER = "ROLE_SHOW_TEACHER";
        public static final String READ_TEACHER = "ROLE_READ_TEACHER";
        public static final String WRITE_TEACHER = "ROLE_WRITE_TEACHER";
        public static final String SHOW_SESSION = "ROLE_SHOW_SESSION";
        public static final String READ_SESSION = "ROLE_READ_SESSION";
        public static final String WRITE_SESSION = "ROLE_WRITE_SESSION";
        public static final String SHOW_STUDENT_GRADE = "ROLE_SHOW_STUDENT_GRADE";
        public static final String READ_STUDENT_GRADE = "ROLE_READ_STUDENT_GRADE";
        public static final String WRITE_STUDENT_GRADE = "ROLE_WRITE_STUDENT_GRADE";
        public static final String SHOW_TEACHER_GRADE = "ROLE_SHOW_TEACHER_GRADE";
        public static final String READ_TEACHER_GRADE = "ROLE_READ_TEACHER_GRADE";
        public static final String WRITE_TEACHER_GRADE = "ROLE_WRITE_TEACHER_GRADE";
        public static final String SHOW_PILOT = "ROLE_SHOW_PILOT";
        public static final String READ_PILOT = "ROLE_READ_PILOT";
        public static final String WRITE_PILOT = "ROLE_WRITE_PILOT";
        public static final String WRITE_CATEGORY_SCORE = "ROLE_WRITE_CATEGORY_SCORE";
        public static final String SHOW_PRODUCT = "ROLE_SHOW_PRODUCT";
        public static final String READ_PRODUCT = "ROLE_READ_PRODUCT";
        public static final String WRITE_PRODUCT = "ROLE_WRITE_PRODUCT";
        public static final String SHOW_CONSTRUCT = "ROLE_SHOW_CONSTRUCT";
        public static final String READ_CONSTRUCT = "ROLE_SHOW_CONSTRUCT";
        public static final String WRITE_CONSTRUCT = "ROLE_SHOW_CONSTRUCT";
        public static final String SHOW_SUBJECT_TEACHER = "ROLE_SHOW_SUBJECT_TEACHER";
        public static final String READ_SUBJECT_TEACHER = "ROLE_READ_SUBJECT_TEACHER";
        public static final String WRITE_SUBJECT_TEACHER = "ROLE_WRITE_SUBJECT_TEACHER";
        public static final String READ_TEACHER_DASHBOARD = "ROLE_READ_TEACHER_DASHBOARD";
        public static final String READ_PRINCIPAL_DASHBOARD = "ROLE_READ_PRINCIPAL_DASHBOARD";
        public static final String READ_SCHOOL_DASHBOARD = "ROLE_READ_SCHOOL_DASHBOARD";
        public static final String READ_COUNSELLOR_STUDENT = "ROLE_READ_COUNSELLOR_STUDENT";
        public static final String WRITE_COUNSELLOR_STUDENT = "ROLE_WRITE_COUNSELLOR_STUDENT";
        public static final String SHOW_COUNSELLOR_DASHBOARD = "ROLE_SHOW_COUNSELLOR_DASHBOARD";
        public static final String SHOW_COMMON_DASHBOARD = "ROLE_SHOW_COMMON_DASHBOARD";
        public static final String READ_COMMON_DASHBOARD = "ROLE_READ_COMMON_DASHBOARD";
        public static final String WRITE_COMMON_DASHBOARD = "ROLE_WRITE_COMMON_DASHBOARD";
        public static final String WRITE_BULK = "ROLE_WRITE_BULK";
        public static final String READ_STUDENT_REPORT = "ROLE_READ_STUDENT_REPORT";
        public static final String READ_TEST_REPORT = "ROLE_READ_TEST_REPORT";
        public static final String READ_STUDENT_NOTES = "ROLE_READ_STUDENT_NOTES";
        public static final String WRITE_STUDENT_NOTES = "ROLE_WRITE_STUDENT_NOTES";
        public static final String READ_STAKEHOLDERQUESTION = "ROLE_READ_STAKEHOLDERQUESTION";
        public static final String WRITE_STAKEHOLDERQUESTION = "ROLE_WRITE_STAKEHOLDERQUESTION";

        public static final String WRITE_STAKEHOLDER = "ROLE_WRITE_STAKEHOLDER";
        public static final String READ_STAKEHOLDER = "ROLE_READ_STAKEHOLDER";

        public static final String SEND_STAKEHOLDER = "ROLE_SEND_STAKEHOLDER";
        public static final String WRITE_TENANT_PRODUCT_ALLOTMENT = "ROLE_WRITE_TENANT_PRODUCT_ALLOTMENT";
        public static final String WRITE_TENANT_OFFER_TEST = "ROLE_WRITE_TENANT_OFFER_TEST";

        public static final String STAKEHOLDER = "ROLE_STAKEHOLDER";
        public static final String STAKEHOLDER_REPORT = "ROLE_STAKEHOLDER_REPORT";

        public static final String STUDENT_WRITE_STAKEHOLDER = "ROLE_STUDENT_WRITE_STAKEHOLDER";

        public static final String READ_REPORTS = "ROLE_READ_REPORTS";

        public static final String READ_ATTENDANCE_SHEET = "ROLE_READ_ATTENDANCE_SHEET";

        public static final String READ_COMMON_LEAP = "ROLE_READ_COMMON_LEAP";

        public static final String READ_COUNSELLOR_KNOWLEDGE_BASE = "ROLE_READ_COUNSELLOR_KNOWLEDGE_BASE";
        public static final String READ_RESEARCHER_DASHBOARD = "ROLE_READ_RESEARCHER_DASHBOARD";
        public static final String READ_MARKETEER_DASHBOARD = "ROLE_READ_MARKETEER_DASHBOARD";
        public static final String READ_LEAP_COORDINATOR_DASHBOARD = "ROLE_LEAP_COORDINATOR_DASHBOARD";
        public static final String WRITE_SYNC_OFFLINE_DATA = "ROLE_WRITE_SYNC_OFFLINE_DATA";
        public static final String LOGIN_PANEL = "ROLE_LOGIN_PANEL";

    }

    public static final class Dashboard {
        public static final String TestTakers = "TestTakers";
        public static final String Users = "Users";
        public static final String Tests = "Tests";
    }
}
