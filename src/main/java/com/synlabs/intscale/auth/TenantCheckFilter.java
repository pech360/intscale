package com.synlabs.intscale.auth;
import com.synlabs.intscale.entity.CurrentUser;
import com.synlabs.intscale.entity.user.User;
import org.slf4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by India on 1/2/2018.
 */


public class TenantCheckFilter extends OncePerRequestFilter{

    private static Logger logger = org.slf4j.LoggerFactory.getLogger(TenantCheckFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain filterChain) throws ServletException, IOException {
        String host = req.getHeader("Host").split("\\.")[0];
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication==null || StringUtils.isEmpty(host)){
            logger.info("host or authentication is null");
            return;
        }
//        logger.info("host name {}"+host);
        User user=null;
        if (!(authentication.getPrincipal() instanceof String)) {
            user = ((CurrentUser) authentication.getPrincipal()).getUser();
        }
        if(user==null){
            logger.info("user account is null");
            return;
        }
        if(!host.equalsIgnoreCase(user.getTenant().getSubDomain())){
            res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            logger.info("hostname and user org doesn't match");
            return;
        }
        filterChain.doFilter(req,res);
    }

    @Override
    public void destroy() {
        // Do nothing
    }

}
