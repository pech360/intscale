package com.synlabs.intscale.common;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ValidityIndicesMatrix
{
  private List<ValidityIndicesMatrixItem> validityIndicesMatrix = new ArrayList<>();

  public List<ValidityIndicesMatrixItem> getValidityIndicesMatrix()
  {
    return validityIndicesMatrix;
  }

  public void setValidityIndicesMatrix(List<ValidityIndicesMatrixItem> validityIndicesMatrix)
  {
    this.validityIndicesMatrix = validityIndicesMatrix;
  }
}
