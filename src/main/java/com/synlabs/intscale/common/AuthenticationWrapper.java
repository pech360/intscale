package com.synlabs.intscale.common;

import org.springframework.security.core.Authentication;

public class AuthenticationWrapper
{
  private String username;
  private String token;

  private Authentication authentication;

  public AuthenticationWrapper(String username, Authentication authentication, String token)
  {
    this.username = username;
    this.authentication = authentication;
    this.token = token;
  }

  public String getUsername()
  {
    return username;
  }

  public Authentication getAuthentication()
  {
    return authentication;
  }

  public String getToken()
  {
    return token;
  }
}
