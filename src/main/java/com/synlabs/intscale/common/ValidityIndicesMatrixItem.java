package com.synlabs.intscale.common;

import java.math.BigDecimal;

public class ValidityIndicesMatrixItem
{
  private String test;
  private BigDecimal infrequency[]  = new BigDecimal[100];
  private BigDecimal acquiescence[] = new BigDecimal[100];

  public String getTest()
  {
    return test;
  }

  public void setTest(String test)
  {
    this.test = test;
  }

  public BigDecimal[] getInfrequency()
  {
    return infrequency;
  }

  public void setInfrequency(BigDecimal[] infrequency)
  {
    this.infrequency = infrequency;
  }

  public BigDecimal[] getAcquiescence()
  {
    return acquiescence;
  }

  public void setAcquiescence(BigDecimal[] acquiescence)
  {
    this.acquiescence = acquiescence;
  }
}
