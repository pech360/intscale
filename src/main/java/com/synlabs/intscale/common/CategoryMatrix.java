package com.synlabs.intscale.common;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CategoryMatrix
{
  private List<CategoryMatrixItem> categoryMatrix = new ArrayList<>();

  public List<CategoryMatrixItem> getCategoryMatrix()
  {
    return categoryMatrix;
  }

  public void setCategoryMatrix(List<CategoryMatrixItem> categoryMatrix)
  {
    this.categoryMatrix = categoryMatrix;
  }
}
