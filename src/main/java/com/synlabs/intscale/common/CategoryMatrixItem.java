package com.synlabs.intscale.common;

import java.math.BigDecimal;

public class CategoryMatrixItem
{
  private String Category;
  private BigDecimal normScore[] = new BigDecimal[100];

  public String getCategory()
  {
    return Category;
  }

  public void setCategory(String category)
  {
    Category = category;
  }

  public BigDecimal[] getNormScore()
  {
    return normScore;
  }

  public void setNormScore(BigDecimal[] normScore)
  {
    this.normScore = normScore;
  }
}
