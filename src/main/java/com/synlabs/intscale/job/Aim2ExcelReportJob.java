package com.synlabs.intscale.job;

import com.synlabs.intscale.service.AsyncReportService;
import com.synlabs.intscale.service.ReportService;
import com.synlabs.intscale.util.DateUtil;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.Date;

public class Aim2ExcelReportJob extends BaseJob {

    private static Logger logger = LoggerFactory.getLogger(Aim2ExcelReportJob.class);

    @Autowired
    private AsyncReportService asyncReportService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        try {
            asyncReportService.makeScheduledReport();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
