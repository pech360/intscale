package com.synlabs.intscale.config;

import com.synlabs.intscale.auth.JwtAuthenticationToken;
import com.synlabs.intscale.auth.JwtFilter;
import com.synlabs.intscale.auth.TenantCheckFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter
{

  @Bean
  public JwtFilter authFilter()
  {
    return new JwtFilter();
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception
  {

    http
        .csrf().disable()
        .httpBasic().disable()
        .authorizeRequests().anyRequest().permitAll()
        .and()
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .antMatcher("/api/**")
        .addFilterBefore(authFilter(), UsernamePasswordAuthenticationFilter.class);

    http.headers().frameOptions().disable()
        .cacheControl().and()
        .contentTypeOptions().and()
        .xssProtection().and()
        .httpStrictTransportSecurity();
    http.addFilterAfter(new TenantCheckFilter(),UsernamePasswordAuthenticationFilter.class);
  }
}
