package com.synlabs.intscale.config;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.spi.JobFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.util.CollectionUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

@Configuration
@PropertySource(value = {"classpath:quartz.properties" })
public class SchedulerConfig extends BaseConfig{
        @Autowired
        List<Trigger> triggerList;
    @Bean
    public JobFactory jobFactory(ApplicationContext applicationContext) {
        AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
        jobFactory.setApplicationContext(applicationContext);
        return jobFactory;
    }

    @Bean
    public Scheduler schedulerFactoryBean(DataSource dataSource, JobFactory jobFactory) throws Exception {
        SchedulerFactoryBean factory = new SchedulerFactoryBean();
        // this allows to update triggers in DB when updating settings in config file:
        factory.setOverwriteExistingJobs(true);
        factory.setQuartzProperties(quartzProperties());
        factory.setDataSource(dataSource);
        factory.setJobFactory(jobFactory);
        factory.afterPropertiesSet();
        factory.afterPropertiesSet();

        Scheduler scheduler = factory.getScheduler();
        scheduler.setJobFactory(jobFactory);

//        scheduler.scheduleJob((JobDetail) sampleJobTrigger.getJobDataMap().get("jobDetail"), sampleJobTrigger);

//        if(!CollectionUtils.isEmpty(triggerList)){
//            for (Trigger trigger : triggerList) {
//                scheduler.scheduleJob((JobDetail) trigger.getJobDataMap().get("jobDetail"), trigger);
//                Trigger dbTrigger = scheduler.getTrigger(trigger.getKey());
//
//                if(trigger instanceof SimpleTrigger && dbTrigger instanceof SimpleTrigger){
//                    if(((SimpleTrigger) trigger).getRepeatInterval() != ((SimpleTrigger) dbTrigger).getRepeatInterval()){
//                        scheduler.rescheduleJob(trigger.getKey(),trigger);
//                    }
//                }
//            }
//        }
        //Comment Code finish
        scheduler.start();
        scheduler.pauseAll();
        return scheduler;
    }


    @Bean
    public Properties quartzProperties() throws IOException {
        PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setLocation(new ClassPathResource("/quartz.properties"));
        propertiesFactoryBean.afterPropertiesSet();
        return propertiesFactoryBean.getObject();
    }








}
