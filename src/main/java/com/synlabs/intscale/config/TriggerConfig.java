package com.synlabs.intscale.config;


import com.synlabs.intscale.job.Aim2ExcelReportJob;
import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

@Configuration
public class TriggerConfig extends BaseConfig {

    @Bean
    public JobDetailFactoryBean aim2ExcelReport() {
        return createJobDetail(Aim2ExcelReportJob.class);
    }

    @Bean
    public CronTriggerFactoryBean aim2ExcelReportTrigger
            (@Qualifier("aim2ExcelReport") JobDetail jobDetail,
             @Value("${intscale.scheduler.report.aim2excel.cron}") String cron) {
        CronTriggerFactoryBean cronTrigger = createCronTrigger(jobDetail, cron);
        return cronTrigger;
    }


}
