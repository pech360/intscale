Dear Respondent,
<br/>
<p>Please click on below link to begin your feedback session. This link is valid till ${response.validTill?datetime}.</p>
<br/>
${response.baseUrl}#!/login/${response.loginKey}
<br/>
<br/>
Regards,<br/>
Team AIM2EXCEL
