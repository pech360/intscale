Dear Respondent,
<br/>
<p>Please click on blow link to verify that you are 360° respondent of :-</p>
<br/>
<table>
  <tr>
    <th>Student Name</th>
    <td>${response.studentName}</td>
  </tr>
  <tr>
    <th>Student Id</th>
    <td>${response.studentUsername}</td>
  </tr>
</table>

<br/>
Verification Link :  ${response.baseUrl}#!/verify/${response.loginKey}
<br/>
<br/>
Regards,<br/>
Team AIM2EXCEL