Dear Respondent,
<br/>
<p>This is to remind you that you have only ${response.hoursRemaining()} hours to give feedback. Click in the blow link to begin.</p>
<br/>
${response.baseUrl}#!/login/${response.loginKey}
<br/>
<br/>
Regards,<br/>
Team AIM2EXCEL
