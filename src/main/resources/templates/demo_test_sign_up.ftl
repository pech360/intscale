Dear ${name},

Thank you for exploring the magical world of AIM2EXCEL.
We wish you a pleasant and spellbinding experience.

Your credentials are:-

Username: ${username} OR  ${email}
Password: ${password}

You may begin by visiting us at ${website}

Bon Voyage!

Regards
Team AIM2EXCEL