var app
    = angular.module('app',
    [
        "angular-jwt",
        "ui.router",
        "toaster",
        "moment-picker",
        "ngAnimate",
        "xeditable",
        "ngStorage",
        "ui.bootstrap",
        "ui.utils",
        "btford.markdown",
        "minicolors",
        "chart.js",
        'pascalprecht.translate'
    ]);

app.run(function ($rootScope, $location, editableOptions, editableThemes, $localStorage, jwtHelper, $templateCache) {
    $templateCache.removeAll();
    Chart.defaults.global.colors = [
        {backgroundColor: "#b3c6e7"},
        {backgroundColor: "#ed7d31"},
        {backgroundColor: "#a5a5a5"},
        {backgroundColor: "#ffc000"},
        {backgroundColor: "#5b9bd5"}];

    $rootScope.$on('$translatePartialLoaderStructureChanged', function () {
        $translate.refresh();
    });

});
