app.config(function ($stateProvider, $urlRouterProvider, $httpProvider,$compileProvider,$translateProvider) {
    var currentUrl  = window.location.hostname.split(":")[0];
    var currentUrl1  =  '%3'+currentUrl;
    var currentUrl2 = "^\s*("+currentUrl+"|"+currentUrl1+"|local|http|https|app|tel|ftp|file|blob|content|ms-appx|x-wmapp0|chrome-extension|cdvfile):|data:image\/";
    var urlRegex = new RegExp(currentUrl2);
    $compileProvider.imgSrcSanitizationWhitelist(urlRegex);
    $compileProvider.aHrefSanitizationWhitelist(urlRegex);

    $urlRouterProvider.otherwise("/notfound");
    $stateProvider
        .state('not-found', {
            url: "/notfound",
            templateUrl: '/report/views/404.html'
        })
        .state('unauth', {
            url: "/unauth",
            templateUrl: '/report/views/401.html'
        })
/*****************************************************/
        .state('pdfreport', {
            abstract: true,
            url: "/pdfreport",
            templateUrl: '/report/views/layout/pdf-layout.html'
        })
        .state('pdfreport.cover', {
            url: "/aimReport/{id}/{token}",
            controller: 'AimReportMainController',
            templateUrl: '/report/views/aimReportMain.html'
        })
        .state('pdfreport.aimReportMini', {
            url: "/aimReportMini/{id}/{token}",
            controller: 'AimReportMiniController',
            templateUrl: '/report/views/aimReportMiniMain.html'
        })
        .state('pdfreport.aimReportDemo', {
            url: "/aimReportDemo/{id}/{token}",
            controller: 'AimReportDemoController',
            templateUrl: '/report/views/aimReportDemo.html'
        })
    ;

    $httpProvider.interceptors.push(['$q', '$location', '$localStorage', 'jwtHelper',
        function ($q, $location, $localStorage, jwtHelper) {
            return {
                'request': function (config) {
                    config.headers = config.headers || {};
                    if ($localStorage.token) {
                        config.headers.Authorization = 'Bearer ' + $localStorage.token;
                    }
                    return config;
                }
                ,
                'responseError': function (response) {
                    //do not redirect on app api rejects @ 403 and they may be due to
                    //authorisation error, not authentication error
                    if (response.status === 401) {
                        $location.path('/unauth');
                    }
                    return $q.reject(response);
                }
            };
        }]);

    var language = (window.navigator.userLanguage || window.navigator.language).toLowerCase();

    $translateProvider.useStaticFilesLoader({
        prefix: '/assets/translations/lang_',
        suffix: '.json'
    });

    $translateProvider.preferredLanguage('en');
    $translateProvider.fallbackLanguage('en');
    $translateProvider.addInterpolation('$translateMessageFormatInterpolation');
    $translateProvider.useSanitizeValueStrategy('escape');
    // $translateProvider.addInterpolation($translateMessageFormatInterpolation);
});