app.controller('AimReportMiniController', function ($http, $localStorage, $stateParams, jwtHelper, $location, $scope, $rootScope, DateFormatService, $translate) {


    var token = $rootScope.token ? $rootScope.token : $stateParams.token;
    $rootScope.token = token;
    $localStorage.token = token;
    $localStorage.assets_url = jwtHelper.decodeToken($localStorage.token)["assets_url"];

    /******************* Do Not remove code above this line *********************/

    $scope.finished = false;
    var userId = $stateParams.id;
    if (userId) {
        fetchProfileByUserId(userId);
    } else {
        fetchProfile();
    }

    $scope.assets_url = $localStorage.assets_url;

    var obtainedScoreRange = {};
    $scope.report = [];
    $scope.reportSecondaryType = [];
    $scope.innateTalents = [];
    $scope.innateTraits = [];
    var aimExpertCareerInventory = [];
    var aimExpertSubjectInventory = [];
    var colors = ['#ff7800', '#FFA000', '#FFB300', '#FFC107', '#FFCA28', '#FFD54F', '#FFE082', '#FFECB3', '#FFECB3', '#FFF8E1', '#FFF7E5'];
    var aimReportInExcel = {
        userId: 0,
        aimReportContentRequests: []
    };
    var aimReportContent = {};

    $scope.disclaimerText = "This report represents a consensus interpretation of the meaning and possible implication of  $name$’s scores on a suite of psycho-educational assessments. This report was developed by a team of psychologists. Its sole purpose is to provide you with information about  $name$ so you may perceive him more accurately.\n\nThis report does not pretend to be 100% accurate, nor should it be taken as an absolute; all measurements contain some error. Furthermore, people can and do change. Occasionally, you may see statements in this report that do not appear to be totally consistent with each other. This is likely to occur when  $name$ has unusual or unexpected combinations of scores.\n\nThis report is designed as a tool to use in helping $name$ be successful in school and life and should not be used in isolation for mental health assistance. Reflect on it and use it only for his benefit. This report should not be used as the sole determinant of study-related decisions and use of these scores to estimate $name$'s functioning in others areas should be approached with caution. This report should not be used for used for legal purposes. The scores are only suggestive in nature. AIM report is CONFIDENTIAL, as the results are kept under strict guidelines and cannot be released to any other party, except yourself and those you give explicit consent to release to.\n\nParents, teachers or mentors, referring to this report should interpret these scores according to $name$’s context. They should see it in connection to other relevant information. Finally, it needs to be remembered that  $name$ can have a strength for a particular area and limitation in the other.";
    $scope.specialTalentTextForDevelopmentPlan = "$name$’s current performance is not reflective of his talents. AIM2EXCEL mentor would identify his talents based on his past achievements. Please focus on developing $name$’s strengths, for which development plan has been listed in the next page."
    $scope.commonTextForBuildYourSelfConcept = "Different people have different abilities and personality traits. These abilities and personality traits make us unique. We have identified which of $name$’s talents, traits and abilities stand out. While Cognitive Intelligence abilities contribute to $name$’s talent, his strengths have been derived from Emotional Intelligence, Social Intelligence and Motivation. Since we want him to maximize his academic performance and well-being, we have also specified those abilities where his performance is much higher than other areas. Hence, you may also come across abilities with a “Medium” score listed in Talents and Strengths. ";

    $scope.urls = {
        reportcoverfront: "https://s3.amazonaws.com/intscale-assets/reportcoverfront.png",
        reportcoverback: "https://s3.amazonaws.com/intscale-assets/reportcoverback.png"
    };


    function fetchProfile() {
        $http.get('/api/user/profile').then(function (response) {
            $scope.user = response.data;
            userId = $scope.user.id;
            $scope.user.firstName = $scope.user.name.split(' ');
            $scope.user.firstName = $scope.user.firstName[0];
            $scope.user.dob = DateFormatService.formatDate(new Date(response.data.dob));

            $scope.translationData = {
                $name$: $scope.user.firstName,
                gender: $scope.user.gender.toLowerCase()
            };

            if ($scope.user.higherEducation) {
                $scope.user.education = $scope.user.higherEducation;
            } else {
                $scope.user.education = $scope.user.grade;
            }

            if ($scope.user.sponsors && $scope.user.sponsors[0]) {
                if($scope.user.sponsors[0].reportCoverFrontImage){
                    $scope.urls.reportcoverfront = $scope.assets_url + $scope.user.sponsors[0].reportCoverFrontImage;
                }if($scope.user.sponsors[0].reportCoverBackImage){
                    $scope.urls.reportcoverback = $scope.assets_url + $scope.user.sponsors[0].reportCoverBackImage;
                }
            }
        });
    }

    function fetchProfileByUserId(id) {
        $http.get('/api/user/self/userProfile?userId=' + id).then(function (response) {
            $scope.user = response.data;
            $scope.user.firstName = $scope.user.name.split(' ');
            $scope.user.firstName = $scope.user.firstName[0];
            $scope.user.dob = DateFormatService.formatDate(new Date(response.data.dob));

            $scope.translationData = {
                $name$: $scope.user.firstName,
                gender: $scope.user.gender.toLowerCase()
            };

            if ($scope.user.higherEducation) {
                $scope.user.education = $scope.user.higherEducation;
            } else {
                $scope.user.education = $scope.user.grade;
            }

            if ($scope.user.sponsors && $scope.user.sponsors[0]) {
                if($scope.user.sponsors[0].reportCoverFrontImage){
                    $scope.urls.reportcoverfront = $scope.assets_url + $scope.user.sponsors[0].reportCoverFrontImage;
                }if($scope.user.sponsors[0].reportCoverBackImage){
                    $scope.urls.reportcoverback = $scope.assets_url + $scope.user.sponsors[0].reportCoverBackImage;
                }
            }
        });
    }

    $scope.switchLanguage = function (language) {
        var key = "";
        switch (language) {
            case "English":
                key = "en";
                break;
            case "Hindi":
                key = "hi";
                break;
            case "Marathi":
                // $("body").css({'font-size': '16px'});
                key = "mr";
                break;
            default:
                key = "en";
                break;
        }
        $translate.use(key);
    };

    $scope.getReportV2 = function () {
        $http.get('/api/reports/aimCompositeReport?userId=' + userId).then(
            function (response) {
                $scope.reportSections = response.data;
                //roundOffScores();
                $scope.reportSections.sort(compareBySequence);
                getAimExpertInventoryForReport('career');
                for (var i in $scope.reportSections) {
                    $scope.reportSections[i].categories.sort(compareBySequence);
                    getSummary(i);
                    renderChart($scope.reportSections[i].categories, i, $scope.reportSections[i].name);
                }
                setScoreRangeLabelsOnCategories();
                makeCategoryBucketsBasedOnRangeOfScores();
                getInnateTalents($scope.reportSections);
                getStrengths();
                getWeaknesses();
                getLearningStyles($scope.reportSections);
                $scope.switchLanguage($scope.user.language);
                $scope.finished = true;

            }, function (response) {
                error(response.data.error)
            });
    };


    function renderChart(data, i, reportSectionName) {

        $scope.report[i] = {};
        $scope.report[i].labels = [];
        $scope.report[i].data = [];
        $scope.report[i].textpos = [];
        $scope.report[i].pad = 0;
        $scope.report[i].barmargin = [];
        $scope.report[i].slider = [];


        $scope.reportSecondaryType[i] = {};
        $scope.reportSecondaryType[i].labels = [];
        $scope.reportSecondaryType[i].data = [];
        $scope.reportSecondaryType.pad = 0;
        $scope.reportSecondaryType[i].barmargin = [];
        $scope.reportSecondaryType[i].textpos = [];
        for (var d in data) {
            if (data[d].type === "secondary") {
                $scope.reportSecondaryType[i].labels.push(data[d].displayName);
                $scope.reportSecondaryType[i].data.push(data[d].percentile * 7.5);
            } else {
                if (reportSectionName === "interest") {
                    $scope.report[i].labels.push(data[d].displayName);
                    $scope.report[i].data.push(data[d].rawScore);
                } else {
                    $scope.report[i].labels.push(data[d].displayName);
                    $scope.report[i].data.push((data[d].percentile) * 7.5);
                }
            }
        }

        $scope.report[i].pad = ((318 / (4 * ($scope.report[i].data.length))));//graph properties for primary report type graph

        for (var x = 0; x < $scope.report[i].data.length; x++) {
            $scope.report[i].barmargin[x] = (4 * x + 1) * $scope.report[i].pad;//padding for each bar
            $scope.report[i].textpos[x] = (4 * x + 2) * $scope.report[i].pad;//text positioning
        }

        $scope.reportSecondaryType[i].pad = ((318 / (4 * ($scope.reportSecondaryType[i].data.length))));

        for (var x = 0; x < $scope.reportSecondaryType[i].data.length; x++) {
            $scope.reportSecondaryType[i].barmargin[x] = (4 * x + 1) * $scope.reportSecondaryType[i].pad;//padding for each bar
            $scope.reportSecondaryType[i].textpos[x] = (4 * x + 2) * $scope.reportSecondaryType[i].pad;//text positioning
        }

        for (var x = 0; x < $scope.report[i].data.length; x++) {
            $scope.report[i].slider[x] = 718.5 + (8.36 * (50 - (($scope.report[i].data[x]) / 7.5)));//padding for each bar
        }

        $scope.report[i].options = {
            responsive: true,
            scales: {
                xAxes: [{
                    ticks: {
                        max: 100,
                        min: 0,
                        stepSize: 10,
                        beginAtZero: true
                    }
                    , gridLines: {
                        drawBorder: true,
                        color: colors.reverse()
                    }
                }]
            },
            elements: {
                line: {
                    tension: 0.1,
                    fill: false,
                    borderColor: "rgb(255,165,0)",
                    borderWidth: 5
                }
            }
        };
    }

    function getSummary(i) {
        $scope.reportSections[i].percentile = $scope.reportSections[i].score;
        $scope.reportSections[i].summary = getScoreRangeText($scope.reportSections[i]).text;
        for (var c in $scope.reportSections[i].categories) {
            $scope.reportSections[i].categories[c].summary = getScoreRangeText($scope.reportSections[i].categories[c]).text;
            // $scope.reportSections[i].categories[c].strengths = [];
            // $scope.reportSections[i].categories[c].weaknesses = [];
            // $scope.reportSections[i].categories[c].subCategories.sort(compareBySequence);
            // for (var j in $scope.reportSections[i].categories[c].subCategories) {
            //     obtainedScoreRange = {};
            //     obtainedScoreRange = getScoreRangeText($scope.reportSections[i].categories[c].subCategories[j]);
            //     obtainedScoreRange.highText = $scope.reportSections[i].categories[c].subCategories[j].highText;
            //     obtainedScoreRange.lowText = $scope.reportSections[i].categories[c].subCategories[j].lowText;
            //     obtainedScoreRange.shortHighText = $scope.reportSections[i].categories[c].subCategories[j].shortHighText;
            //     obtainedScoreRange.shortLowText = $scope.reportSections[i].categories[c].subCategories[j].shortLowText;
            //     if (obtainedScoreRange.rangeOf == "VeryHigh" || obtainedScoreRange.rangeOf == "High" || obtainedScoreRange.rangeOf == "Average") {
            //         if ($scope.reportSections[i].categories[c].strengths.length < 3) {
            //             $scope.reportSections[i].categories[c].strengths.push(obtainedScoreRange);
            //         }
            //     } else {
            //         if ($scope.reportSections[i].categories[c].weaknesses.length < 3) {
            //             $scope.reportSections[i].categories[c].weaknesses.push(obtainedScoreRange);
            //         }
            //     }
            // }
            // $scope.reportSections[i].categories[c].strengths.sort(compareStrengths);
            // $scope.reportSections[i].categories[c].weaknesses.sort(compareWeaknesses);
        }
    }

    function roundOffScores() {
        for (var i in $scope.reportSections) {
            $scope.reportSections[i].score = +($scope.reportSections[i].score).toFixed(4);
            for (var c in $scope.reportSections[i].categories) {
                $scope.reportSections[i].categories[c].rawScore = +($scope.reportSections[i].categories[c].rawScore).toFixed(5);
                $scope.reportSections[i].categories[c].percentile = +($scope.reportSections[i].categories[c].percentile).toFixed(5);
            }
        }
    }

    function compareBySequence(a, b) {
        if (a.sequence < b.sequence)
            return -1;
        if (a.sequence > b.sequence)
            return 1;
        return 0;
    }

    function compareStrengths(a, b) {
        if (a.startFrom < b.startFrom)
            return 1;
        if (a.startFrom > b.startFrom)
            return -1;
        return 0;
    }

    function compareWeaknesses(a, b) {
        if (a.startFrom < b.startFrom)
            return -1;
        if (a.startFrom > b.startFrom)
            return 1;
        return 0;
    }

    function comparePercentileScores(a, b) {
        if (a.percentile < b.percentile)
            return 1;
        if (a.percentile > b.percentile)
            return -1;
        return 0;
    }


    function comparePercentageScores(a, b) {
        if (a.score < b.score)
            return -1;
        if (a.score > b.score)
            return 1;
        return 0;
    }

    function comparePercentileDifference(a, b) {
        return (b.percentileDifference - a.percentileDifference);
    }

    function comparePercentageDifference(a, b) {
        return (b.percentageDifference - a.percentageDifference);
    }

    function compareItemsByPercentileDifference(item1, item2) {
        if (item1.percentileDifference > item2.percentileDifference) {
            return item1;
        }
        if (item1.percentileDifference < item2.percentileDifference) {
            return item2;
        }
        if (item1.percentileDifference === item2.percentileDifference) {
            return "same";
        }

    }

    function compareItemsByPercentageDifference(item1, item2) {
        if (item1.percentageDifference > item2.percentageDifference) {
            return item1;
        }
        if (item1.percentageDifference < item2.percentageDifference) {
            return item2;
        }
        if (item1.percentageDifference === item2.percentageDifference) {
            return "same";
        }

    }

    var getAbsoluteDifference = function (a, b) {
        return Math.abs(a - b);
    };

    function findCategoryByName(name, list) {
        for (var i in list) {
            if (name === list[i]) {
                return list[i];
            }
        }
    }

    function findItemsInArrayWithAtrribute(array, attr, value) {
        result = [];
        for (var i = 0; i < array.length; i += 1) {
            if (array[i][attr] === value) {
                result.push(array[i]);
            }
        }
        return result;
    }

    function findIndexWithAttr(array, attr, value) {
        if (!array) {
            return -1;
        }
        for (var i = 0; i < array.length; i += 1) {
            if (array[i][attr] === value) {
                return i;
            }
        }
        return -1;
    }

    function findTopMostItemsFromList(array, attr, numberOfItems, order) {
        var defaultFlag = false;
        if (order) {
            if (order === "ascending") { // for ascending
                array.sort(function (a, b) {
                    return a[attr] - b[attr];
                })
            } else {
                defaultFlag = true
            }
        } else {
            defaultFlag = true
        }

        if (defaultFlag) {  //  descending - By Default
            array.sort(function (a, b) {
                return b[attr] - a[attr];
            })
        }

        array.splice(numberOfItems, array.length);
        return array;
    }

    function filterArrayWithAttribute(array, attr, value) {
        var result = [];
        if (!array) {
            return array;
        }

        for (var i = 0; i < array.length; i += 1) {
            if (array[i][attr] === value) {
                result.push(angular.copy(array[i]));
            }
        }
        return result;
    }

    function removeReferenceItemsFromOrignal(orignalArray, refernceArray, attr) {
        for (var i in orignalArray) {
            for (j in refernceArray) {
                if (orignalArray[i].id === refernceArray[j].id) {
                    orignalArray.splice(i, 1);
                }
            }
        }
        return orignalArray;
    }

    function findOutHowManyAreEqual(list, param, comparison) {
        r = [];
        list.sort(comparison);
        for (var i = 0; i < list.length; i++) {
            if (list[0][param] === list[i][param]) {
                if (list[i]) {
                    r.push(list[i]);
                }
            }
        }
        return r;
    }

    function getScoreRangeText(category) {
        for (var r in category.scoreRanges) {
            if (category.scoreRanges[r].startFrom <= category.percentile && category.scoreRanges[r].endAt >= category.percentile) {
                return category.scoreRanges[r];
            }
        }
    }

    function replaceAll(text, keyword, replacement) {
        var str2 = '';
        var stringArr = text.split(keyword);
        var len = stringArr.length;
        for (var a in stringArr) {
            str2 = str2 + stringArr[a];
            if (a < len - 1) {
                str2 = str2 + replacement;
            }
        }
        return str2;
    }

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function getInnateTalentBySpecificOrder(list) {
        var item = {};
        item = findCategoryByName('Fluid Reasoning', list);
        if (item) {
            $scope.innateTalents.push(item);
        }
        item = findCategoryByName('Verbal Knowledge', list);
        if (item) {
            $scope.innateTalents.push(item);
        }

        item = findCategoryByName('Working Memory', list);
        if (item) {
            $scope.innateTalents.push(item);
        }
    }

    function getInnateTalents(reportSections) {
        var result = {};
        var listOfItemsHavingEqualValue = [];
        var orderList = ["Fluid Reasoning", "Verbal Knowledge", "Working Memory"];
        var index = {};
        for (var i in reportSections) {
            if (reportSections[i].name === 'Cognitive Intelligence') {
                var list = [];
                list = angular.copy($scope.reportSections[i].highBucket.concat($scope.reportSections[i].moderateBucket));
                while ($scope.innateTalents.length <= 3) {
                    listOfItemsHavingEqualValue = findOutHowManyAreEqual(list, 'percentile', comparePercentileScores);
                    if (listOfItemsHavingEqualValue.length === 1) {
                        $scope.innateTalents.push(listOfItemsHavingEqualValue[0]);
                        index = findIndexWithAttr(list, 'name', listOfItemsHavingEqualValue[0].name);
                        if (index > -1) {
                            list.splice(index, 1);
                        }
                    } else {
                        listOfItemsHavingEqualValue = findOutHowManyAreEqual(listOfItemsHavingEqualValue, 'rawScore', comparePercentageScores);
                        if (listOfItemsHavingEqualValue.length === 1) {
                            $scope.innateTalents.push(listOfItemsHavingEqualValue[0]);
                            index = findIndexWithAttr(list, 'name', listOfItemsHavingEqualValue[0].name);
                            if (index > -1) {
                                list.splice(index, 1);
                            }
                        } else {
                            result = findCategoryFromListByOrderFilteredByResultList(orderList, list, $scope.innateTalents, 'innateTalent');
                            if (result) {
                                $scope.innateTalents.push(result);
                                index = findIndexWithAttr(list, 'name', result.name);
                                if (index > -1) {
                                    list.splice(index, 1);
                                }
                            }
                        }
                    }
                    if ($scope.innateTalents.length === 2) {
                        break;
                    }
                    if (list.length < 1) {
                        break;
                    }
                }
            }
        }
        getInnateTraits(reportSections);
    }

    function makeCategoryBucketsBasedOnRangeOfScores() {
        for (var i in $scope.reportSections) {
            $scope.reportSections[i].highBucket = [];
            $scope.reportSections[i].lowBucket = [];
            $scope.reportSections[i].moderateBucket = [];

            for (var j in $scope.reportSections[i].categories) {
                $scope.reportSections[i].categories[j].reportSectionName = $scope.reportSections[i].name;
                $scope.reportSections[i].categories[j].reportSectionScore = $scope.reportSections[i].score;
                if ($scope.reportSections[i].categories[j].type !== "secondary") {
                    switch ($scope.reportSections[i].categories[j].scoreLabel) {
                        case "High":
                            $scope.reportSections[i].highBucket.push($scope.reportSections[i].categories[j]);
                            break;
                        case "Low":
                            $scope.reportSections[i].lowBucket.push($scope.reportSections[i].categories[j]);
                            break;
                        case "Moderate":
                            $scope.reportSections[i].moderateBucket.push($scope.reportSections[i].categories[j]);
                            break;
                    }
                }
            }

            $scope.reportSections[i].highBucket.sort(comparePercentileScores);
            $scope.reportSections[i].lowBucket.sort(comparePercentileScores);
            $scope.reportSections[i].moderateBucket.sort(comparePercentileScores);
        }
    }

    function setScoreRangeLabelsOnCategories() {
        for (var i in $scope.reportSections) {

            for (var r in $scope.reportSections[i].scoreRanges) {
                if ($scope.reportSections[i].scoreRanges[r].startFrom <= $scope.reportSections[i].percentile && $scope.reportSections[i].scoreRanges[r].endAt >= $scope.reportSections[i].percentile) {
                    switch ($scope.reportSections[i].scoreRanges[r].rangeOf) {
                        case "VeryHigh":
                        case "High":
                            $scope.reportSections[i].scoreLabel = "High";
                            $scope.reportSections[i].scoreLabelRating = 3;
                            aimReportContent.name = $scope.reportSections[i].name;
                            aimReportContent.rawScore = $scope.reportSections[i].rawScore;
                            aimReportContent.standardScore = $scope.reportSections[i].percentile;
                            aimReportContent.scoreLabel = $scope.reportSections[i].scoreLabel;
                            aimReportContent.scoreLabelRating = $scope.reportSections[i].scoreLabelRating;
                            aimReportContent.reportSectionName = $scope.reportSections[i].name;
                            aimReportContent.type = "ReportSection";
                            break;
                        case "VeryLow":
                        case "Low":
                            $scope.reportSections[i].scoreLabel = "Low";
                            $scope.reportSections[i].scoreLabelRating = 1;
                            aimReportContent.name = $scope.reportSections[i].name;
                            aimReportContent.rawScore = $scope.reportSections[i].rawScore;
                            aimReportContent.standardScore = $scope.reportSections[i].percentile;
                            aimReportContent.scoreLabel = $scope.reportSections[i].scoreLabel;
                            aimReportContent.scoreLabelRating = $scope.reportSections[i].scoreLabelRating;
                            aimReportContent.reportSectionName = $scope.reportSections[i].name;
                            aimReportContent.type = "ReportSection";
                            break;
                        case "Average":
                            $scope.reportSections[i].scoreLabel = "Moderate";
                            $scope.reportSections[i].scoreLabelRating = 2;
                            aimReportContent.name = $scope.reportSections[i].name;
                            aimReportContent.rawScore = $scope.reportSections[i].rawScore;
                            aimReportContent.standardScore = $scope.reportSections[i].percentile;
                            aimReportContent.scoreLabel = $scope.reportSections[i].scoreLabel;
                            aimReportContent.scoreLabelRating = $scope.reportSections[i].scoreLabelRating;
                            aimReportContent.reportSectionName = $scope.reportSections[i].name;
                            aimReportContent.type = "ReportSection";
                            break;
                    }
                }
            }

            if (!angular.equals(aimReportContent, {})) {
                aimReportInExcel.aimReportContentRequests.push(angular.copy(aimReportContent));
                if (aimReportContent.name === 'Interest') {
                    aimReportContent.name = "Top3CategoriesOfInterest";
                    aimReportInExcel.aimReportContentRequests.push(angular.copy(aimReportContent));
                }
                aimReportContent = {};
            }

            for (var j in $scope.reportSections[i].categories) {
                aimReportContent = {};
                for (var r in $scope.reportSections[i].categories[j].scoreRanges) {
                    if ($scope.reportSections[i].categories[j].scoreRanges[r].startFrom <= $scope.reportSections[i].categories[j].percentile && $scope.reportSections[i].categories[j].scoreRanges[r].endAt >= $scope.reportSections[i].categories[j].percentile) {
                        switch ($scope.reportSections[i].categories[j].scoreRanges[r].rangeOf) {
                            case "VeryHigh":
                            case "High":
                                $scope.reportSections[i].categories[j].scoreLabel = "High";
                                $scope.reportSections[i].categories[j].scoreLabelRating = 3;
                                $scope.reportSections[i].categories[j].innateTalentText = $scope.reportSections[i].categories[j].scoreRanges[r].innateTalent;
                                $scope.reportSections[i].categories[j].strength = $scope.reportSections[i].categories[j].scoreRanges[r].strength;
                                $scope.reportSections[i].categories[j].weakness = $scope.reportSections[i].categories[j].scoreRanges[r].weakness;
                                $scope.reportSections[i].categories[j].developmentPlanForStrength = $scope.reportSections[i].categories[j].scoreRanges[r].developmentPlanForStrength;
                                $scope.reportSections[i].categories[j].developmentPlanForWeakness = $scope.reportSections[i].categories[j].scoreRanges[r].developmentPlanForWeakness;
                                aimReportContent.name = $scope.reportSections[i].categories[j].displayName;
                                aimReportContent.rawScore = $scope.reportSections[i].categories[j].rawScore;
                                aimReportContent.standardScore = $scope.reportSections[i].categories[j].percentile;
                                aimReportContent.scoreLabel = $scope.reportSections[i].categories[j].scoreLabel;
                                aimReportContent.scoreLabelRating = $scope.reportSections[i].categories[j].scoreLabelRating;
                                aimReportContent.reportSectionName = $scope.reportSections[i].name;
                                aimReportContent.type = "Category";
                                break;
                            case "VeryLow":
                            case "Low":
                                $scope.reportSections[i].categories[j].scoreLabel = "Low";
                                $scope.reportSections[i].categories[j].scoreLabelRating = 1;
                                $scope.reportSections[i].categories[j].innateTalentText = $scope.reportSections[i].categories[j].scoreRanges[r].innateTalent;
                                $scope.reportSections[i].categories[j].strength = $scope.reportSections[i].categories[j].scoreRanges[r].strength;
                                $scope.reportSections[i].categories[j].weakness = $scope.reportSections[i].categories[j].scoreRanges[r].weakness;
                                $scope.reportSections[i].categories[j].developmentPlanForStrength = $scope.reportSections[i].categories[j].scoreRanges[r].developmentPlanForStrength;
                                $scope.reportSections[i].categories[j].developmentPlanForWeakness = $scope.reportSections[i].categories[j].scoreRanges[r].developmentPlanForWeakness;
                                aimReportContent.name = $scope.reportSections[i].categories[j].displayName;
                                aimReportContent.rawScore = $scope.reportSections[i].categories[j].rawScore;
                                aimReportContent.standardScore = $scope.reportSections[i].categories[j].percentile;
                                aimReportContent.scoreLabel = $scope.reportSections[i].categories[j].scoreLabel;
                                aimReportContent.scoreLabelRating = $scope.reportSections[i].categories[j].scoreLabelRating;
                                aimReportContent.reportSectionName = $scope.reportSections[i].name;
                                aimReportContent.type = "Category";
                                break;
                            case "Average":
                                $scope.reportSections[i].categories[j].scoreLabel = "Moderate";
                                $scope.reportSections[i].categories[j].scoreLabelRating = 2;
                                $scope.reportSections[i].categories[j].innateTalentText = $scope.reportSections[i].categories[j].scoreRanges[r].innateTalent;
                                $scope.reportSections[i].categories[j].strength = $scope.reportSections[i].categories[j].scoreRanges[r].strength;
                                $scope.reportSections[i].categories[j].weakness = $scope.reportSections[i].categories[j].scoreRanges[r].weakness;
                                $scope.reportSections[i].categories[j].developmentPlanForStrength = $scope.reportSections[i].categories[j].scoreRanges[r].developmentPlanForStrength;
                                $scope.reportSections[i].categories[j].developmentPlanForWeakness = $scope.reportSections[i].categories[j].scoreRanges[r].developmentPlanForWeakness;
                                aimReportContent.name = $scope.reportSections[i].categories[j].displayName;
                                aimReportContent.rawScore = $scope.reportSections[i].categories[j].rawScore;
                                aimReportContent.standardScore = $scope.reportSections[i].categories[j].percentile;
                                aimReportContent.scoreLabel = $scope.reportSections[i].categories[j].scoreLabel;
                                aimReportContent.scoreLabelRating = $scope.reportSections[i].categories[j].scoreLabelRating;
                                aimReportContent.reportSectionName = $scope.reportSections[i].name;
                                aimReportContent.type = "Category";
                                break;
                        }

                    }
                }

                if (!angular.equals(aimReportContent, {})) {
                    aimReportInExcel.aimReportContentRequests.push(angular.copy(aimReportContent));
                    aimReportContent = {};
                }
            }

            if ($scope.reportSections[i].name === 'Interest') {
                for (var j in $scope.reportSections[i].top3CategoriesForInterest) {
                    for (var r in $scope.reportSections[i].top3CategoriesForInterest[j].scoreRanges) {

                        if ($scope.reportSections[i].top3CategoriesForInterest[j].scoreRanges[r].startFrom <= $scope.reportSections[i].top3CategoriesForInterest[j].percentile && $scope.reportSections[i].top3CategoriesForInterest[j].scoreRanges[r].endAt >= $scope.reportSections[i].top3CategoriesForInterest[j].percentile) {
                            switch ($scope.reportSections[i].top3CategoriesForInterest[j].scoreRanges[r].rangeOf) {
                                case "VeryHigh":
                                case "High":
                                    $scope.reportSections[i].top3CategoriesForInterest[j].scoreLabel = "High";
                                    $scope.reportSections[i].top3CategoriesForInterest[j].scoreLabelRating = 3;
                                    aimReportContent.name = $scope.reportSections[i].top3CategoriesForInterest[j].displayName;
                                    aimReportContent.rawScore = $scope.reportSections[i].top3CategoriesForInterest[j].rawScore;
                                    aimReportContent.standardScore = $scope.reportSections[i].top3CategoriesForInterest[j].percentile;
                                    aimReportContent.scoreLabel = $scope.reportSections[i].top3CategoriesForInterest[j].scoreLabel;
                                    aimReportContent.scoreLabelRating = $scope.reportSections[i].top3CategoriesForInterest[j].scoreLabelRating;
                                    aimReportContent.reportSectionName = "Top3CategoriesOfInterest";
                                    aimReportContent.type = "Category";
                                    break;
                                case "VeryLow":
                                case "Low":
                                    $scope.reportSections[i].top3CategoriesForInterest[j].scoreLabel = "Low";
                                    $scope.reportSections[i].top3CategoriesForInterest[j].scoreLabelRating = 1;
                                    aimReportContent.name = $scope.reportSections[i].top3CategoriesForInterest[j].displayName;
                                    aimReportContent.rawScore = $scope.reportSections[i].top3CategoriesForInterest[j].rawScore;
                                    aimReportContent.standardScore = $scope.reportSections[i].top3CategoriesForInterest[j].percentile;
                                    aimReportContent.scoreLabel = $scope.reportSections[i].top3CategoriesForInterest[j].scoreLabel;
                                    aimReportContent.scoreLabelRating = $scope.reportSections[i].top3CategoriesForInterest[j].scoreLabelRating;
                                    aimReportContent.reportSectionName = "Top3CategoriesOfInterest";
                                    aimReportContent.type = "Category";
                                    break;
                                case "Average":
                                    $scope.reportSections[i].top3CategoriesForInterest[j].scoreLabel = "Moderate";
                                    $scope.reportSections[i].top3CategoriesForInterest[j].scoreLabelRating = 2;
                                    aimReportContent.name = $scope.reportSections[i].top3CategoriesForInterest[j].displayName;
                                    aimReportContent.rawScore = $scope.reportSections[i].top3CategoriesForInterest[j].rawScore;
                                    aimReportContent.standardScore = $scope.reportSections[i].top3CategoriesForInterest[j].percentile;
                                    aimReportContent.scoreLabel = $scope.reportSections[i].top3CategoriesForInterest[j].scoreLabel;
                                    aimReportContent.scoreLabelRating = $scope.reportSections[i].top3CategoriesForInterest[j].scoreLabelRating;
                                    aimReportContent.reportSectionName = "Top3CategoriesOfInterest";
                                    aimReportContent.type = "Category";
                                    break;
                            }

                        }
                    }
                    if (!angular.equals(aimReportContent, {})) {
                        aimReportInExcel.aimReportContentRequests.push(angular.copy(aimReportContent));
                        aimReportContent = {};
                    }
                }
            }
        }
    }


    function getInnateTraits(reportSections) {
        var result = {};
        var listOfItemsHavingEqualValue = [];
        var orderList = ["Extroversion", "Agreeableness", "Conscientiousness", "Openness to new Experience"];
        var index = {};
        for (var i in reportSections) {
            if (reportSections[i].name === 'Personality') {
                var list = [];
                list = angular.copy(reportSections[i].categories);
                for (var j in list) {
                    list[j].percentileDifference = getAbsoluteDifference(list[j].percentile, 50);
                    list[j].percentageDifference = getAbsoluteDifference(list[j].rawScore, 50);
                }

                for (var k in list) {
                    if (list[k].name === 'Neuroticism') {
                        if (list[k].scoreLabel === "High") {
                            var tempScoreRange = getScoreRangeText(list[k]);
                            if (tempScoreRange.rangeOf === "High") {
                                $scope.specialTextFlagNeurotism = true;
                                $scope.specialTextForNeurotism = tempScoreRange.text;
                            } else {
                                $scope.specialTextFlagNeurotism = false;
                            }
                        }
                        list.splice(k, 1);
                    }
                }

                list.sort(comparePercentileDifference);

                while ($scope.innateTraits.length <= 3) {
                    listOfItemsHavingEqualValue = findOutHowManyAreEqual(list, 'percentileDifference', comparePercentileDifference);
                    if (listOfItemsHavingEqualValue.length === 1) {
                        if (listOfItemsHavingEqualValue[0]) {
                            $scope.innateTraits.push(listOfItemsHavingEqualValue[0]);
                            index = findIndexWithAttr(list, 'name', listOfItemsHavingEqualValue[0].name);
                            if (index > -1) {
                                list.splice(index, 1);
                            }
                        }
                    } else {
                        listOfItemsHavingEqualValue = findOutHowManyAreEqual(listOfItemsHavingEqualValue, 'percentageDifference', comparePercentageDifference);
                        if (listOfItemsHavingEqualValue.length === 1) {
                            if (listOfItemsHavingEqualValue[0]) {
                                $scope.innateTraits.push(listOfItemsHavingEqualValue[0]);
                                index = findIndexWithAttr(list, 'name', listOfItemsHavingEqualValue[0].name);
                                if (index > -1) {
                                    list.splice(index, 1);
                                }
                            }
                        } else {
                            result = findCategoryFromListByOrderFilteredByResultList(orderList, list, $scope.innateTalents, 'innateTrait');
                            if (result) {
                                $scope.innateTraits.push(result);
                                index = findIndexWithAttr(list, 'name', result.name);
                                if (index > -1) {
                                    list.splice(index, 1);
                                }
                            }
                        }
                    }
                    if ($scope.innateTalents.length === 0 && $scope.innateTraits.length === 3) {
                        break;
                    }
                    if ($scope.innateTalents.length === 1 && $scope.innateTraits.length === 2) {
                        break;
                    }
                    if ($scope.innateTalents.length === 2 && $scope.innateTraits.length === 1) {
                        break;
                    }
                    if (list.length < 1) {
                        break;
                    }
                }
            }
        }
    }


    function findCategoryFromListByOrderFilteredByResultList(orderList, listOfItemsHavingEqualValue, resultList, forWhat) {
        var result;
        var priorityIndexList = [];

        if (listOfItemsHavingEqualValue.length === 2) {
            if (listOfItemsHavingEqualValue[0].name === "Perceiving Emotion" && listOfItemsHavingEqualValue[1].name === "Perceiving Emotion") {

                if (forWhat === "weakness") {
                    listOfItemsHavingEqualValue = findOutHowManyAreEqual(listOfItemsHavingEqualValue, 'reportSectionScore', function (a, b) {
                        return b.reportSectionScore - a.reportSectionScore;
                    });
                } else {
                    listOfItemsHavingEqualValue = findOutHowManyAreEqual(listOfItemsHavingEqualValue, 'reportSectionScore', function (a, b) {
                        return a.reportSectionScore - b.reportSectionScore;
                    });
                }

                if (listOfItemsHavingEqualValue.length == 1) {
                    return listOfItemsHavingEqualValue[0];
                } else {
                    var index = findIndexWithAttr(listOfItemsHavingEqualValue, 'displayName', "Emotional Perception");
                    if (index > -1) {
                        return listOfItemsHavingEqualValue[index];
                    } else {
                        return listOfItemsHavingEqualValue[0];
                    }
                }
            }
        }

        for (var i in listOfItemsHavingEqualValue) {
            for (var j in orderList) {
                if (listOfItemsHavingEqualValue[i].name === orderList[j]) {
                    priorityIndexList.push(i);
                }
            }
        }

        priorityIndexList.sort(function (a, b) {
            return a - b;
        });

        result = checkIfAlreadyExistInList(listOfItemsHavingEqualValue[0], resultList);
        if (!result) {
            return listOfItemsHavingEqualValue[priorityIndexList[0]];
        }
        // for (var i in orderList) {
        //     result = checkIfAlreadyExistInList(orderList[i], resultList);
        //     if (!result) {
        //         for (var j in listOfItemsHavingEqualValue) {
        //             if (listOfItemsHavingEqualValue[j].name == orderList[i].name) {
        //                 return listOfItemsHavingEqualValue[j]
        //             }
        //         }
        //     }
        // }
    }

    function checkIfAlreadyExistInList(item, list) {
        var flag = false;
        for (var i in list) {
            if (list[i].name == item.name) {
                flag = true;
            }
        }
        return flag;
    }

    function getTopCategoriesForStrengths() {
        var reportSectionsToBeConsiderd = [{name: "Emotional Intelligence", categoryCount: 0}, {
            name: "Social Intelligence",
            categoryCount: 0
        }, {name: "Motivation", categoryCount: 0}];
        var topExtractedCategories = [];
        var templist = [];
        for (var i in $scope.reportSections) {
            if ($scope.reportSections[i].name == reportSectionsToBeConsiderd[0].name || $scope.reportSections[i].name == reportSectionsToBeConsiderd[1].name || $scope.reportSections[i].name == reportSectionsToBeConsiderd[2].name) {
                templist = angular.copy($scope.reportSections[i].highBucket.concat($scope.reportSections[i].moderateBucket));
                templist.sort(comparePercentileScores);
                for (var j in templist) {
                    for (var k in reportSectionsToBeConsiderd) {
                        if (templist[j].reportSectionName == reportSectionsToBeConsiderd[k].name) {
                            if (reportSectionsToBeConsiderd[k].categoryCount < 2) {
                                if (templist[j]) {
                                    topExtractedCategories.push(templist[j]);
                                    reportSectionsToBeConsiderd[k].categoryCount++;
                                }
                            }
                        }
                    }
                }
            }
        }

        $scope.specialTextFlagForWeakness = true;
        for (var m in topExtractedCategories) {
            if (topExtractedCategories[m].scoreLabel !== 'High') {
                $scope.specialTextFlagForWeakness = false;
            }
        }
        if ($scope.specialTextFlagForWeakness) {
            $scope.specialTextForWeakness = "$name$’s current performance is not reflective of his limitations.";
        }
        return topExtractedCategories;
    }

    function getTopCategoriesForWeaknesses() {
        var reportSectionsToBeConsiderd = [{
            name: "Cognitive Intelligence",
            categoryCount: 0
        }, {name: "Emotional Intelligence", categoryCount: 0}, {
            name: "Social Intelligence",
            categoryCount: 0
        }, {name: "Motivation", categoryCount: 0}];
        var topExtractedCategories = [];
        var templist = [];
        for (var i in $scope.reportSections) {
            if ($scope.reportSections[i].name === reportSectionsToBeConsiderd[0].name || $scope.reportSections[i].name === reportSectionsToBeConsiderd[1].name || $scope.reportSections[i].name == reportSectionsToBeConsiderd[2].name || $scope.reportSections[i].name == reportSectionsToBeConsiderd[3].name) {
                if ($scope.reportSections[i].name === reportSectionsToBeConsiderd[0].name) {
                    templist = angular.copy($scope.reportSections[i].lowBucket);
                } else {
                    templist = angular.copy($scope.reportSections[i].lowBucket.concat($scope.reportSections[i].moderateBucket));
                }
                templist.sort(function (a, b) {
                    return a.percentile - b.percentile;
                });

                for (var j in templist) {
                    for (var k in reportSectionsToBeConsiderd) {
                        if (templist[j].reportSectionName === reportSectionsToBeConsiderd[k].name) {
                            if (reportSectionsToBeConsiderd[k].categoryCount < 2) {
                                if (templist[j]) {
                                    topExtractedCategories.push(templist[j]);
                                    reportSectionsToBeConsiderd[k].categoryCount++;
                                }
                            }
                        }
                    }
                }
            }
        }
        $scope.specialTextFlagForStrength = true;
        for (var m in topExtractedCategories) {
            if (topExtractedCategories[m].scoreLabel !== 'Low') {
                $scope.specialTextFlagForStrength = false;
            }
        }
        if ($scope.specialTextFlagForStrength) {
            $scope.specialTextForStrength = "$name$’s current performance is not reflective of his strengths.";
        }

        return topExtractedCategories;
    }

    function getStrengths() {
        $scope.strengths = [];
        var result = {};
        var listOfItemsHavingEqualValue = [];
        var index = {};
        var orderList = ["Perceiving Emotion", "Understanding Emotion", "Managing Emotion", "Social Perception", "Social Translation", "Social Inference", "Expectancy", "Instrumentality", "Valence"];
        var list = getTopCategoriesForStrengths();
        list.sort(function (a, b) {
            return b.percentile - a.percentile;
        });

        while ($scope.strengths.length <= 3) {
            listOfItemsHavingEqualValue = findOutHowManyAreEqual(list, 'percentile', comparePercentileScores);
            if (listOfItemsHavingEqualValue.length === 1) {
                if (listOfItemsHavingEqualValue[0]) {
                    $scope.strengths.push(listOfItemsHavingEqualValue[0]);
                }
                // index = findIndexWithAttr(list, 'name', listOfItemsHavingEqualValue[0].name);
                // if (index > -1) {
                //     list.splice(index, 1);
                // }
            } else {
                listOfItemsHavingEqualValue = findOutHowManyAreEqual(listOfItemsHavingEqualValue, 'rawScore', comparePercentageScores);
                if (listOfItemsHavingEqualValue.length === 1) {
                    if (listOfItemsHavingEqualValue[0]) {
                        $scope.strengths.push(listOfItemsHavingEqualValue[0]);
                    }
                    // index = findIndexWithAttr(list, 'name', listOfItemsHavingEqualValue[0].name);
                    // if (index > -1) {
                    //     list.splice(index, 1);
                    // }
                } else {
                    result = findCategoryFromListByOrderFilteredByResultList(orderList, listOfItemsHavingEqualValue, $scope.strengths, 'strength');
                    if (result) {
                        $scope.strengths.push(result);
                    }
                    // index = findIndexWithAttr(list, 'name', result.name);
                    // if (index > -1) {
                    //     list.splice(index, 1);
                    // }
                }
            }

            for (var i in $scope.strengths) {
                for (var j in list) {
                    if ($scope.strengths[i].name === list[j].name) {
                        listOfItemsHavingEqualValue = findOutHowManyAreEqual(list, 'name', function (a, b) {
                            return a.name - b.name;
                        });
                        if (listOfItemsHavingEqualValue.length > 1) {
                            index = findIndexWithAttr(list, 'displayName', $scope.strengths[i].displayName);
                            list.splice(index, 1);
                        } else {
                            list.splice(j, 1);
                        }
                    }
                }
            }
            if (list.length < 1) {
                break;
            }
            if ($scope.strengths.length >= 3) {
                break;
            }
        }
    }

    function getWeaknesses() {
        $scope.weaknesses = [];
        var result = {};
        var listOfItemsHavingEqualValue = [];
        var index = {};
        var list = [];
        var listOfCiCategories = [];
        var listOfNonCiCategories = [];
        var orderList = ["Fluid Reasoning", "Verbal Knowledge", "Working Memory", "Emotional Perception", "Emotional Knowledge", "Emotional Reasoning", "Social Perception", "Social Knowledge", "Social Reasoning", "Expectancy", "Instrumentality", "Valence"];
        var topCategories = getTopCategoriesForWeaknesses();

        for (var t in topCategories) {
            if (topCategories[t].reportSectionName === "Cognitive Intelligence") {
                if (topCategories[t]) {
                    listOfCiCategories.push(topCategories[t]);
                }
            } else {
                if (topCategories[t]) {
                    listOfNonCiCategories.push(topCategories[t])
                }
            }
        }

        list = listOfCiCategories;
        listOfCiCategories = [];

        while ($scope.weaknesses.length <= 3) {
            listOfItemsHavingEqualValue = findOutHowManyAreEqual(list, 'percentile', function (a, b) {
                return a.percentile - b.percentile;
            });

            if (listOfItemsHavingEqualValue.length === 1) {
                if (listOfItemsHavingEqualValue[0]) {
                    $scope.weaknesses.push(listOfItemsHavingEqualValue[0]);
                }
                // index = findIndexWithAttr(list, 'name', listOfItemsHavingEqualValue[0].name);
                // if (index > -1) {
                //     list.splice(index, 1);
                // }

            } else {

                listOfItemsHavingEqualValue = findOutHowManyAreEqual(listOfItemsHavingEqualValue, 'rawScore', function (a, b) {
                    return a.rawScore - b.Score;
                });

                if (listOfItemsHavingEqualValue.length === 1) {
                    if (listOfItemsHavingEqualValue[0]) {
                        $scope.weaknesses.push(listOfItemsHavingEqualValue[0]);
                    }
                    // index = findIndexWithAttr(list, 'name', listOfItemsHavingEqualValue[0].name);
                    // if (index > -1) {
                    //     list.splice(index, 1);
                    // }
                } else {
                    result = findItemsInArrayWithAtrribute(listOfItemsHavingEqualValue, name, "Perceiving Emotion");
                    if (result.length === 2) {
                        var r1 = findItemsInArrayWithAtrribute($scope.reportSections, reportSectionName, result[0].reportSectionName);
                        var r2 = findItemsInArrayWithAtrribute($scope.reportSections, reportSectionName, result[1].reportSectionName);
                        if (r1[0].score < r2[0].score) {
                            if (result[0]) {
                                $scope.weaknesses.push(result[0]);
                            }
                        } else {
                            if (r1[0].score > r2[0].score) {
                                if (result[1]) {
                                    $scope.weaknesses.push(result[1]);
                                }
                            } else {
                                var r3 = findItemsInArrayWithAtrribute(listOfItemsHavingEqualValue, displayName, "Emotional Perception");
                                if (r3[0]) {
                                    $scope.weaknesses.push(r3[0]);
                                }
                            }
                        }
                    } else {
                        result = findCategoryFromListByOrderFilteredByResultList(orderList, listOfItemsHavingEqualValue, $scope.weaknesses, 'weakness');
                        if (result) {
                            $scope.weaknesses.push(result);
                        }
                    }
                    // index = findIndexWithAttr(list, 'name', result.name);
                    // if (index > -1) {
                    //     list.splice(index, 1);
                    // }
                }
            }

            for (var i in $scope.weaknesses) {
                for (var j in list) {
                    if ($scope.weaknesses[i].name === list[j].name) {
                        listOfItemsHavingEqualValue = findOutHowManyAreEqual(list, 'name', function (a, b) {
                            return a.name - b.name;
                        });
                        if (listOfItemsHavingEqualValue.length > 1) {
                            index = findIndexWithAttr(list, 'displayName', $scope.weaknesses[i].displayName);
                            list.splice(index, 1);
                        } else {
                            list.splice(j, 1);
                        }
                    }
                }
            }
            if (list.length < 1) {
                list = listOfNonCiCategories;
                listOfNonCiCategories = [];
                if (list.length < 1) {
                    break;
                }
            }
            if ($scope.weaknesses.length >= 3) {
                break;
            }
        }
    }

    function getLearningStyles(reportSections) {
        $scope.learningStyles = [];
        var categories = [];
        var temp = {};
        for (var i = 0; i < reportSections.length; i++) {
            if (reportSections[i].name === "Cognitive Intelligence") {
                for (var j in reportSections[i].categories) {
                    if (reportSections[i].categories[j].type === "secondary") {
                        categories.push(reportSections[i].categories[j]);
                    }
                }

                categories.sort(function (a, b) {
                    return b.percentile - a.percentile;
                });

                difference = categories[0].percentile - categories[1].percentile;
                switch (true) {
                    case difference >= 10:
                        temp = findItemsInArrayWithAtrribute(categories[0].scoreRanges, 'rangeOf', 'High');
                        $scope.learningStyles.first = temp[0];
                        break;
                    case difference >= 5 && difference < 10:
                        temp = findItemsInArrayWithAtrribute(categories[0].scoreRanges, 'rangeOf', 'Average');
                        $scope.learningStyles.first = temp[0];
                        break;
                    case difference < 5:
                        temp = findItemsInArrayWithAtrribute(categories[0].scoreRanges, 'rangeOf', 'Low');
                        $scope.learningStyles.first = temp[0];
                        break;
                }

                switch ($scope.learningStyles.first.learningStyle) {
                    case "Clearly Verbal":
                        $scope.learningStyles.first.graphScore = 753 + (0);
                        break;
                    case "Moderately Verbal":
                        $scope.learningStyles.first.graphScore = 753 + (200);
                        break;
                    case "No Preference":
                        $scope.learningStyles.first.graphScore = 753 + (2 * 200);
                        break;
                    case "Moderately Visual":
                        $scope.learningStyles.first.graphScore = 753 + (3 * 200);
                        break;
                    case "Clearly Visual":
                        $scope.learningStyles.first.graphScore = 753 + (4 * 200);
                        break;
                }
            }

            if (reportSections[i].name === "Interest") {
                $scope.learningStyles.second = reportSections[i].interestInventoryResponse;
                switch ($scope.learningStyles.second.learningStyle) {
                    case "Clearly Practical":
                        $scope.learningStyles.second.graphScore = 753 + (4 * 200);
                        break;
                    case "Moderately Practical":
                        $scope.learningStyles.second.graphScore = 753 + (3 * 200);
                        break;
                    case "No Preference":
                        $scope.learningStyles.second.graphScore = 753 + (2 * 200);
                        break;
                    case "Moderately Academic":
                        $scope.learningStyles.second.graphScore = 753 + (200);
                        break;
                    case "Clearly Academic":
                        $scope.learningStyles.second.graphScore = 753 + (0);
                        break;
                }
            }

            if (reportSections[i].name === "Personality") {
                for (var k in reportSections[i].categories) {
                    if (reportSections[i].categories[k].name === "Openness to Experience") {
                        var difference = reportSections[i].categories[k].percentile - 50;
                        switch (true) {
                            case difference >= 10:
                                temp = findItemsInArrayWithAtrribute(reportSections[i].categories[k].scoreRanges, 'rangeOf', 'VeryHigh');
                                $scope.learningStyles.third = temp[0];
                                $scope.learningStyles.third.graphScore = 753 + (4 * 200);
                                break;
                            case difference >= 5 && difference < 10:
                                temp = findItemsInArrayWithAtrribute(reportSections[i].categories[k].scoreRanges, 'rangeOf', 'High');
                                $scope.learningStyles.third = temp[0];
                                $scope.learningStyles.third.graphScore = 753 + (3 * 200);
                                break;
                            case difference >= -5 && difference < 5:
                                temp = findItemsInArrayWithAtrribute(reportSections[i].categories[k].scoreRanges, 'rangeOf', 'Average');
                                $scope.learningStyles.third = temp[0];
                                $scope.learningStyles.third.graphScore = 753 + (2 * 200);
                                break;
                            case difference >= -10 && difference < -5:
                                temp = findItemsInArrayWithAtrribute(reportSections[i].categories[k].scoreRanges, 'rangeOf', 'Low');
                                $scope.learningStyles.third = temp[0];
                                $scope.learningStyles.third.graphScore = 753 + (1 * 200);
                                break;
                            case difference < -10:
                                temp = findItemsInArrayWithAtrribute(reportSections[i].categories[k].scoreRanges, 'rangeOf', 'VeryLow');
                                $scope.learningStyles.third = temp[0];
                                $scope.learningStyles.third.graphScore = 753 + (0 * 200);
                                break;
                        }
                    }
                }
            }
        }
        getInterestGraphRender($scope.learningStyles.second.region);
    }

    function getInterestGraphRender(region) {
        region = region - 1;
        $scope.sections = [];

        for (var i = 0; i < 12; i++) {
            if (i === region) {
                $scope.sections[i] = "#00bb7e";
            }
            else {
                $scope.sections[i] = "#ffffff";
            }
        }
    }

    function getPearsonCorrelation(x, y) {
        var shortestArrayLength = 0;

        if (x.length === y.length) {
            shortestArrayLength = x.length;
        } else if (x.length > y.length) {
            shortestArrayLength = y.length;
            console.error('x has more items in it, the last ' + (x.length - shortestArrayLength) + ' item(s) will be ignored');
        } else {
            shortestArrayLength = x.length;
            console.error('y has more items in it, the last ' + (y.length - shortestArrayLength) + ' item(s) will be ignored');
        }

        var xy = [];
        var x2 = [];
        var y2 = [];

        for (var i = 0; i < shortestArrayLength; i++) {
            xy.push(x[i] * y[i]);
            x2.push(x[i] * x[i]);
            y2.push(y[i] * y[i]);
        }

        var sum_x = 0;
        var sum_y = 0;
        var sum_xy = 0;
        var sum_x2 = 0;
        var sum_y2 = 0;

        for (var i = 0; i < shortestArrayLength; i++) {
            sum_x += x[i];
            sum_y += y[i];
            sum_xy += xy[i];
            sum_x2 += x2[i];
            sum_y2 += y2[i];
        }

        var step1 = (shortestArrayLength * sum_xy) - (sum_x * sum_y);
        var step2 = (shortestArrayLength * sum_x2) - (sum_x * sum_x);
        var step3 = (shortestArrayLength * sum_y2) - (sum_y * sum_y);
        var step4 = Math.sqrt(step2 * step3);
        var answer = step1 / step4;

        return answer;
    }

    function filterAimExpertInventory(aimExpertCareerInventory, filterList) {
        for (var h in filterList) {
            for (var i in aimExpertCareerInventory) {
                for (var j in aimExpertCareerInventory[i].reportSections) {
                    if (aimExpertCareerInventory[i].reportSections[j].reportSectionName === filterList[h]) {
                        aimExpertCareerInventory[i].reportSections.splice(j, 1);
                    }
                }
            }
        }
        return aimExpertCareerInventory;
    }

    function getUniversalAimExpertInventory(aimExpertCareerInventory, maxSize) {
        var filterList1 = ['Motivation'];
        var filterList2 = ['Motivation', 'Emotional Intelligence', 'Social Intelligence'];

        aimExpertCareerInventory = makeHierarchyOfReportSections(aimExpertCareerInventory, $scope.reportSections);
        aimExpertCareerInventory = filterAimExpertInventory(aimExpertCareerInventory, filterList1);
        aimExpertCareerInventory = addScoresInAimExpertCareerInventory(aimExpertCareerInventory, $scope.reportSections);

        for (var i in aimExpertCareerInventory) {
            aimExpertCareerInventory[i].subjects = makeHierarchyOfReportSections(aimExpertCareerInventory[i].subjects, $scope.reportSections);
            aimExpertCareerInventory[i].subjects = filterAimExpertInventory(aimExpertCareerInventory[i].subjects, filterList2);
            aimExpertCareerInventory[i].subjects = addScoresInAimExpertCareerInventory(aimExpertCareerInventory[i].subjects, $scope.reportSections);
            aimExpertCareerInventory[i].subjects = multiplyCareerSubjectScoresWithTheirWeights(aimExpertCareerInventory[i].subjects);
            aimExpertCareerInventory[i].recruiters = aimExpertCareerInventory[i].topRecruiters.split(',');

        }
        return getTopAimExpertInventory(aimExpertCareerInventory, maxSize);
    }

    /*
    Both commented methods are equivalent to method "getUniversalAimExpertInventory"
    so, these were commented,
    delete it in near future

    function getCareersInclusiveOfChoosedStream(aimExpertCareerInventory,maxSize) {
         var filterList1 = ['Motivation'];
         var filterList2 = ['Motivation', 'Emotional Intelligence', 'Social Intelligence'];

         aimExpertCareerInventory = makeHierarchyOfReportSections(aimExpertCareerInventory, $scope.reportSections);
         aimExpertCareerInventory = filterAimExpertInventory(aimExpertCareerInventory, filterList1);
         aimExpertCareerInventory = addScoresInAimExpertCareerInventory(aimExpertCareerInventory, $scope.reportSections);

         for (var i in aimExpertCareerInventory) {
             aimExpertCareerInventory[i].subjects = makeHierarchyOfReportSections(aimExpertCareerInventory[i].subjects, $scope.reportSections);
             aimExpertCareerInventory[i].subjects = filterAimExpertInventory(aimExpertCareerInventory[i].subjects, filterList2);
             aimExpertCareerInventory[i].subjects = addScoresInAimExpertCareerInventory(aimExpertCareerInventory[i].subjects, $scope.reportSections);
             aimExpertCareerInventory[i].subjects = multiplyCareerSubjectScoresWithTheirWeights(aimExpertCareerInventory[i].subjects);
             aimExpertCareerInventory[i].recruiters = aimExpertCareerInventory[i].topRecruiters.split(',');

         }

         return getTopAimExpertInventory(aimExpertCareerInventory, maxSize);
     }

     function getCareersExclusiveOfChoosedStream(aimExpertCareerInventory, maxSize) {
         var filterList1 = ['Motivation'];
         var filterList2 = ['Motivation', 'Emotional Intelligence', 'Social Intelligence'];

         aimExpertCareerInventory = makeHierarchyOfReportSections(aimExpertCareerInventory, $scope.reportSections);
         aimExpertCareerInventory = filterAimExpertInventory(aimExpertCareerInventory, filterList1);
         aimExpertCareerInventory = addScoresInAimExpertCareerInventory(aimExpertCareerInventory, $scope.reportSections);

         for (var i in aimExpertCareerInventory) {
             aimExpertCareerInventory[i].subjects = makeHierarchyOfReportSections(aimExpertCareerInventory[i].subjects, $scope.reportSections);
             aimExpertCareerInventory[i].subjects = filterAimExpertInventory(aimExpertCareerInventory[i].subjects, filterList2);
             aimExpertCareerInventory[i].subjects = addScoresInAimExpertCareerInventory(aimExpertCareerInventory[i].subjects, $scope.reportSections);
             aimExpertCareerInventory[i].subjects = multiplyCareerSubjectScoresWithTheirWeights(aimExpertCareerInventory[i].subjects);
             aimExpertCareerInventory[i].recruiters = aimExpertCareerInventory[i].topRecruiters.split(',');

         }

         return getTopAimExpertInventory(aimExpertCareerInventory, maxSize);
     }
     */

    function getAimExpertInventoryForSeniors(aimExpertCareerInventory) {

        return getUniversalAimExpertInventory(aimExpertCareerInventory, 3);

        /*
         This is the logic for getting one recommended career from its choosen stream
         and rest of the recommended stream independent from choosen stream

         var careers = [];
         var totalSize = 3;
         var index = -1;
         var careersWhichHaveOnlyOneStreamAndMatchingOne = findCareersWhichHaveOnlyOneStreamAndMatchesWith(aimExpertCareerInventory, $scope.user.stream);
         var inclusiveCareers = getUniversalAimExpertInventory(careersWhichHaveOnlyOneStreamAndMatchingOne, 2);

         if (inclusiveCareers) {
             for (var i in inclusiveCareers) {
                 index = findIndexWithAttr(aimExpertCareerInventory, 'name', inclusiveCareers[i].name);
                 if (index !== -1) {
                     aimExpertCareerInventory.splice(index, 1);
                 }
             }

             totalSize = totalSize - inclusiveCareers.length;
             careers = inclusiveCareers;
         }

         for (var i in aimExpertCareerInventory) {
             var streams = aimExpertCareerInventory[i].streams.toUpperCase();
             if($scope.user.stream){
                 var userStream = $scope.user.stream.toUpperCase();
                 index = streams.indexOf(userStream);
                 if (index === -1) {
                     index = findIndexWithAttr(aimExpertCareerInventory, 'name', aimExpertCareerInventory[i].name);
                     if (index !== -1) {
                         aimExpertCareerInventory.splice(index, 1);
                     }
                 }
             }else {
             }
         }

         var exclusiveCareers = getUniversalAimExpertInventory(aimExpertCareerInventory, totalSize);
         return careers.concat(exclusiveCareers);*/
    }


    function findCareersWhichHaveOnlyOneStreamAndMatchesWith(careers, stream) {
        var careersWhichHaveOnlyOneStreamAndMatchingOne = [];
        for (var i in careers) {
            careers[i].streamsArray = [];
            careers[i].streamsArray = careers[i].streams.split(',');

            if (careers[i].streamsArray.length === 1 && careers[i].streamsArray[0] === stream) {
                careersWhichHaveOnlyOneStreamAndMatchingOne.push(careers[i]);
            }
        }
        return careersWhichHaveOnlyOneStreamAndMatchingOne;
    }


    function getRecommendedSubjects(aimExpertCareerInventory, filterList2) {
        var subjects = aimExpertCareerInventory[0].subjects;
        subjects = makeHierarchyOfReportSections(subjects, $scope.reportSections);
        subjects = filterAimExpertInventory(subjects, filterList2);
        subjects = addScoresInAimExpertCareerInventory(subjects, $scope.reportSections);
        subjects = findTopMostItemsFromList(subjects, 'score', 3, 'descending');
        return subjects;
    }

    function getRecommendedHobbiesForJuniors(aimExpertCareerInventory, filterList1) {
        var hobbies = aimExpertCareerInventory[0].hobbies;
        hobbies = makeHierarchyOfReportSections(hobbies, $scope.reportSections);
        hobbies = filterAimExpertInventory(hobbies, filterList1);
        hobbies = addScoresInAimExpertCareerInventory(hobbies, $scope.reportSections);
        hobbies = findTopMostItemsFromList(hobbies, 'score', 3, 'descending');
        return hobbies;
    }

    function getAimExpertInventoryForJuniors(aimExpertCareerInventory) {
        var index;
        var tempSubjectWeightedScore = 0;
        var tempHobbyWeightedScore = 0;
        var filterList1 = ['Motivation'];
        var filterList2 = ['Motivation', 'Emotional Intelligence', 'Social Intelligence'];
        $scope.subjects = getRecommendedSubjects(aimExpertCareerInventory, filterList2);
        $scope.hobbies = getRecommendedHobbiesForJuniors(aimExpertCareerInventory, filterList1);


        for (var i in aimExpertCareerInventory) {
            tempHobbyWeightedScore = 0;
            tempSubjectWeightedScore = 0;

            for (var j in $scope.subjects) {
                index = findIndexWithAttr(aimExpertCareerInventory[i].subjects, 'name', $scope.subjects[j].name);
                if (index > -1) {
                    tempSubjectWeightedScore = $scope.subjects[j].score * aimExpertCareerInventory[i].subjects[index].weight + tempSubjectWeightedScore;
                }
            }

            for (var k in $scope.hobbies) {
                index = findIndexWithAttr(aimExpertCareerInventory[i].hobbies, 'name', $scope.hobbies[k].name);
                if (index > -1) {
                    tempHobbyWeightedScore = $scope.hobbies[j].score * aimExpertCareerInventory[i].hobbies[index].weight + tempHobbyWeightedScore;
                }
            }

            aimExpertCareerInventory[i].score = (tempHobbyWeightedScore + tempSubjectWeightedScore) / 2;
            aimExpertCareerInventory[i].subjects = $scope.subjects;
            aimExpertCareerInventory[i].leisures = $scope.hobbies;
            aimExpertCareerInventory[i].recruiters = aimExpertCareerInventory[i].topRecruiters.split(',');
        }

        $scope.aimExpertCareerInventory = findTopMostItemsFromList(aimExpertCareerInventory, 'score', 3, 'descending');
        copyAimExpertInventoryIntoAimReportExcel($scope.aimExpertCareerInventory);
        return $scope.aimExpertCareerInventory
    }


    function getAimExpertInventoryForSecondaryStudents(aimExpertCareerInventory) {
        var filterList1 = ['Motivation'];
        var filterList2 = ['Motivation', 'Emotional Intelligence', 'Social Intelligence'];
        var tempSubjectWeightedScore = 0;
        var tempHobbyWeightedScore = 0;
        var recommendedCareers = [];

        /*************** getting streams started***************/
        // getAllStreams(filterList2);
        $http.get('/api/aimExpert/aimExpertInventoryList/forReport?type=stream&userId=' + $scope.user.id).then(function (response) {
            var streams = getPossibleStreamsForRecommendation(response.data);
            for (var i in streams) {
                streams[i].subjects = makeHierarchyOfReportSections(streams[i].subjects, $scope.reportSections);
                streams[i].subjects = filterAimExpertInventory(streams[i].subjects, filterList2);
                streams[i].subjects = addScoresInAimExpertCareerInventory(streams[i].subjects, $scope.reportSections);
                streams[i].subjects = multiplyCareerSubjectScoresWithTheirWeights(streams[i].subjects);
            }

            for (var i in streams) {
                streams[i].score = 0;
                for (var j in streams[i].subjects) {
                    streams[i].score += streams[i].subjects[j].weightedScore;
                }
            }

            streams.sort(function (a, b) {
                return b.score - a.score;
            });

            $scope.streams = streams;
            $scope.recommendedStreams = filterArrayWithAttribute($scope.streams, 'canBeRecommended', true);

            /*************** getting streams ended***************/

            $scope.hobbies = getRecommendedHobbiesForJuniors(aimExpertCareerInventory, filterList1);


            aimExpertCareerInventory = makeHierarchyOfReportSections(aimExpertCareerInventory, $scope.reportSections);
            aimExpertCareerInventory = filterAimExpertInventory(aimExpertCareerInventory, filterList1);
            aimExpertCareerInventory = addScoresInAimExpertCareerInventory(aimExpertCareerInventory, $scope.reportSections);


            for (var i in aimExpertCareerInventory) {
                aimExpertCareerInventory[i].subjects = makeHierarchyOfReportSections(aimExpertCareerInventory[i].subjects, $scope.reportSections);
                aimExpertCareerInventory[i].subjects = filterAimExpertInventory(aimExpertCareerInventory[i].subjects, filterList2);
                aimExpertCareerInventory[i].subjects = addScoresInAimExpertCareerInventory(aimExpertCareerInventory[i].subjects, $scope.reportSections);
                aimExpertCareerInventory[i].subjects = multiplyCareerSubjectScoresWithTheirWeights(aimExpertCareerInventory[i].subjects);
                aimExpertCareerInventory[i].recruiters = aimExpertCareerInventory[i].topRecruiters.split(',');

            }

            var matchedCareersWithStream = getCareersMatchingByStreams(aimExpertCareerInventory, $scope.recommendedStreams);
            recommendedCareers = findTopMostItemsFromList(matchedCareersWithStream, 'score', 2, 'descending');

            aimExpertCareerInventory = removeReferenceItemsFromOrignal(aimExpertCareerInventory, recommendedCareers);
            aimExpertCareerInventory = removeCareerWithRecommededStream(aimExpertCareerInventory, $scope.recommendedStreams);

            var careersExclusiveOfStream = findTopMostItemsFromList(aimExpertCareerInventory, 'score', 1, 'descending');

            recommendedCareers.push(careersExclusiveOfStream[0]);
            $scope.aimExpertCareerInventory = recommendedCareers;
            // $scope.aimExpertCareerInventory = findTopMostItemsFromList(recommendedCareers, 'score', 3, 'descending');

            copyAimExpertInventoryIntoAimReportExcel($scope.aimExpertCareerInventory);
            return $scope.aimExpertCareerInventory;


        }, function (response) {
            alert(response.data.error);
        });
    }

    function getAimExpertInventoryForReport(type) {
        $http.get('/api/aimExpert/aimExpertInventoryList/forReport?type=' + type + '&userId=' + $scope.user.id).then(function (response) {
            aimExpertCareerInventory = response.data;

            switch ($scope.user.grade) {
                case "6":
                case "7":
                case "8":
                    $scope.aimExpertCareerInventory = getAimExpertInventoryForJuniors(aimExpertCareerInventory);
                    $scope.careerCommonText = "";
                    break;

                case "9":
                case "10":
                    $scope.aimExpertCareerInventory = getAimExpertInventoryForSecondaryStudents(aimExpertCareerInventory);
                    $scope.careerCommonText = "";
                    break;

                case "11":
                case "12":
                    $scope.aimExpertCareerInventory = getAimExpertInventoryForSeniors(aimExpertCareerInventory);
                    $scope.careerCommonText = "For each career path, $name$’s profile was matched with that of successful Indian professionals. To create these original and comprehensive profiles, Aim2Excel researchers conducted in-depth interviews of the professionals having a cumulative experience of 20+ years. This enables us to identify the 3 best-fit Career Paths for $name$. Also, for each Career Path, we have identified which subjects he should focus on and which leisure activities he should pursue. Since these paths do not take into account the subject combination in 10+2, some career paths may seem incongruent to your present subjects/field of study.";
                    copyAimExpertInventoryIntoAimReportExcel($scope.aimExpertCareerInventory);
                    break;
            }


        }, function (response) {
            alert(response.data.error);
        })
    }


    function getTopAimExpertInventory(aimExpertCareerInventory, maxSize) {
        var list = [];
        var subjects = [];
        for (var i in aimExpertCareerInventory) {
            subjects = [];
            if (aimExpertCareerInventory[i]) {
                for (var j in aimExpertCareerInventory[i].subjects) {
                    if (aimExpertCareerInventory[i].subjects[j]) {
                        if (aimExpertCareerInventory[i].subjects[j].weightedScore !== 0) {
                            subjects.push(aimExpertCareerInventory[i].subjects[j]);
                            if (subjects.length >= 3) {
                                break;
                            }
                        }
                    }
                }

                if (aimExpertCareerInventory[i].score !== 0) {
                    aimExpertCareerInventory[i].subjects = subjects;
                    list.push(aimExpertCareerInventory[i]);
                    if (list.length >= maxSize) {
                        break;
                    }
                }
            }
        }

        return list;
    }

    function multiplyCareerSubjectScoresWithTheirWeights(aimExpertInventory) {
        for (var i in aimExpertInventory) {
            aimExpertInventory[i].weightedScore = aimExpertInventory[i].score * aimExpertInventory[i].weight;
        }

        aimExpertInventory.sort(function (a, b) {
            return b.weightedScore - a.weightedScore;
        });

        return aimExpertInventory;
    }

    function makeHierarchyOfReportSections(aimExpertInventory) {
        var index;
        var temp = {};
        var list = [];
        for (var i in aimExpertInventory) {
            temp = {};
            temp.name = aimExpertInventory[i].name;
            temp.description = aimExpertInventory[i].description;
            temp.type = aimExpertInventory[i].type;
            temp.career = aimExpertInventory[i].career;
            temp.displayName = aimExpertInventory[i].displayName;

            if (temp.type === 'career') {
                temp.id = aimExpertInventory[i].id;
                temp.streams = aimExpertInventory[i].streams;
                temp.subjects = aimExpertInventory[i].subjects;
                temp.imageName = aimExpertInventory[i].imageName;
                temp.summary = aimExpertInventory[i].summary;
                temp.tasks = aimExpertInventory[i].tasks;
                temp.scope = aimExpertInventory[i].scope;
                temp.education = aimExpertInventory[i].education;
                temp.topRecruiters = aimExpertInventory[i].topRecruiters;
                temp.topCollegesOfIndia = aimExpertInventory[i].topCollegesOfIndia;
                temp.topCollegesOfAbroad = aimExpertInventory[i].topCollegesOfAbroad;
                temp.leisures = [{name: ""}, {name: ""}, {name: ""}];
                if (aimExpertInventory[i].leisures) {
                    for (var m in aimExpertInventory[i].leisures) {
                        temp.leisures[m].name = aimExpertInventory[i].leisures[m];
                    }
                }
            }
            if (temp.type === 'subject' || temp.type === 'leisure') {
                temp.weight = aimExpertInventory[i].weight;
            }
            temp.reportSections = [];
            list.push(angular.copy(temp));

            for (var j in aimExpertInventory[i].aimExpertItems) {
                index = findIndexWithAttr(list[i].reportSections, 'reportSectionName', aimExpertInventory[i].aimExpertItems[j].reportSection);
                if (index < 0) {
                    temp = {};
                    temp.reportSectionName = aimExpertInventory[i].aimExpertItems[j].reportSection;
                    temp.categories = [];
                    list[i].reportSections.push(angular.copy(temp));

                    index = findIndexWithAttr(list[i].reportSections, 'reportSectionName', aimExpertInventory[i].aimExpertItems[j].reportSection);
                    list[i].reportSections[index].categories.push(aimExpertInventory[i].aimExpertItems[j]);

                } else {
                    list[i].reportSections[index].categories.push(aimExpertInventory[i].aimExpertItems[j]);
                }
            }
        }
        return list;
    }

    function addScoresInAimExpertCareerInventory(aimExpertCareerInventory, reportSections) {
        var l;
        var m;
        for (var i in aimExpertCareerInventory) {
            aimExpertCareerInventory[i].score = 0;

            for (j in aimExpertCareerInventory[i].reportSections) {
                l = findIndexWithAttr(reportSections, 'name', aimExpertCareerInventory[i].reportSections[j].reportSectionName);
                if (l > -1) {
                    aimExpertCareerInventory[i].reportSections[j].score = reportSections[l].score;
                    aimExpertCareerInventory[i].reportSections[j].scoreLabel = reportSections[l].scoreLabel;
                    aimExpertCareerInventory[i].reportSections[j].scoreLabelRating = reportSections[l].scoreLabelRating;
                    aimExpertCareerInventory[i].reportSections[j].scoreLabelProcessedRating = 0;
                    if (reportSections[l].name === 'Interest') {
                        aimExpertCareerInventory[i].reportSections[j].top3CategoriesForInterest = reportSections[l].top3CategoriesForInterest;
                    }
                }

                for (k in aimExpertCareerInventory[i].reportSections[j].categories) {
                    m = findIndexWithAttr(reportSections[l].categories, 'name', aimExpertCareerInventory[i].reportSections[j].categories[k].category);
                    if (m > -1) {
                        aimExpertCareerInventory[i].reportSections[j].categories[k].score = reportSections[l].categories[m].percentile;
                        aimExpertCareerInventory[i].reportSections[j].categories[k].scoreLabel = reportSections[l].categories[m].scoreLabel;
                        aimExpertCareerInventory[i].reportSections[j].categories[k].scoreLabelRating = reportSections[l].categories[m].scoreLabelRating;
                        aimExpertCareerInventory[i].reportSections[j].categories[k].scoreLabelProcessedRating = 0;
                    } else {
                    }
                }

                aimExpertCareerInventory[i].reportSections[j] = processScoreLabelRating(aimExpertCareerInventory[i].reportSections[j]);

                aimExpertCareerInventory[i].score = aimExpertCareerInventory[i].score + aimExpertCareerInventory[i].reportSections[j].scoreLabelProcessedRating;
            }
            aimExpertCareerInventory[i].score = +((aimExpertCareerInventory[i].score / aimExpertCareerInventory[i].reportSections.length).toFixed(4));
            aimExpertCareerInventory[i].score = aimExpertCareerInventory[i].score * 100;
            aimExpertCareerInventory[i].score = +(aimExpertCareerInventory[i].score.toFixed(3));
        }

        aimExpertCareerInventory.sort(function (a, b) {
            var aitems = [];
            var bitems = [];
            var aChildItems = [];
            var bChildItems = [];

            var result = b.score - a.score;
            if (result === 0) {
                aitems = findItemsInArrayWithAtrribute(a.reportSections, 'reportSectionName', 'Interest');
                bitems = findItemsInArrayWithAtrribute(b.reportSections, 'reportSectionName', 'Interest');
                if(aitems[0] && bitems[0]){
                    result = bitems[0].scoreLabelProcessedRating - aitems[0].scoreLabelProcessedRating;
                }
            }
            if (result === 0) {
                aitems = findItemsInArrayWithAtrribute(a.reportSections, 'reportSectionName', 'Personality');
                bitems = findItemsInArrayWithAtrribute(b.reportSections, 'reportSectionName', 'Personality');
                if(aitems[0] && bitems[0]){
                    result = bitems[0].scoreLabelProcessedRating - aitems[0].scoreLabelProcessedRating;
                }
            }
            if (result === 0) {
                aitems = findItemsInArrayWithAtrribute(a.reportSections, 'reportSectionName', 'Cognitive Intelligence');
                bitems = findItemsInArrayWithAtrribute(b.reportSections, 'reportSectionName', 'Cognitive Intelligence');
                if(aitems[0] && bitems[0]){
                    result = bitems[0].scoreLabelProcessedRating - aitems[0].scoreLabelProcessedRating;
                }
            }
            if (result === 0) {
                aitems = findItemsInArrayWithAtrribute(a.reportSections, 'reportSectionName', 'Social Intelligence');
                bitems = findItemsInArrayWithAtrribute(b.reportSections, 'reportSectionName', 'Social Intelligence');
                if(aitems[0] && bitems[0]){
                    result = bitems[0].scoreLabelProcessedRating - aitems[0].scoreLabelProcessedRating;
                }
            }
            if (result === 0) {
                aitems = findItemsInArrayWithAtrribute(a.reportSections, 'reportSectionName', 'Emotional Intelligence');
                bitems = findItemsInArrayWithAtrribute(b.reportSections, 'reportSectionName', 'Emotional Intelligence');
                if(aitems[0] && bitems[0]){
                    result = bitems[0].scoreLabelProcessedRating - aitems[0].scoreLabelProcessedRating;
                }
            }
            if (result === 0) {
                aitems = findItemsInArrayWithAtrribute(a.reportSections, 'reportSectionName', 'Cognitive Intelligence');
                bitems = findItemsInArrayWithAtrribute(b.reportSections, 'reportSectionName', 'Cognitive Intelligence');
                if(aitems[0] && bitems[0]){
                    aChildItems= findItemsInArrayWithAtrribute(aitems[0].categories, 'category', 'Fluid Reasoning');
                    bChildItems = findItemsInArrayWithAtrribute(bitems[0].categories, 'category', 'Fluid Reasoning');
                    if(aChildItems[0] && bChildItems[0]){
                        result = aChildItems[0].scoreLabelProcessedRating - bChildItems[0].scoreLabelProcessedRating;
                    }
                }
            }
            if (result === 0) {
                if(aitems[0] && bitems[0]){
                    aChildItems= findItemsInArrayWithAtrribute(aitems[0].categories, 'category', 'Verbal Knowledge');
                    bChildItems = findItemsInArrayWithAtrribute(bitems[0].categories, 'category', 'Verbal Knowledge');
                    if(aChildItems[0] && bChildItems[0]){
                        result = aChildItems[0].scoreLabelProcessedRating - bChildItems[0].scoreLabelProcessedRating;
                    }
                }
            }
            if (result === 0) {
                if(aitems[0] && bitems[0]){
                    aChildItems= findItemsInArrayWithAtrribute(aitems[0].categories, 'category', 'Working Memory');
                    bChildItems = findItemsInArrayWithAtrribute(bitems[0].categories, 'category', 'Working Memory');
                    if(aChildItems[0] && bChildItems[0]){
                        result = aChildItems[0].scoreLabelProcessedRating - bChildItems[0].scoreLabelProcessedRating;
                    }
                }
            }
            if (result === 0) {
                return 1
            }
            return result
        });
        return aimExpertCareerInventory;
    }

    function processScoreLabelRating(reportSection) {
        var goalStandards = [];
        var scoreLabelRatings = [];
        var correlation;
        var index;
        switch (reportSection.reportSectionName) {

            case 'Cognitive Intelligence':
            case 'Emotional Intelligence':
            case 'Social Intelligence':
                reportSection.scoreLabelProcessedRating = 0;
                for (var i in reportSection.categories) {
                    switch (true) {
                        case reportSection.categories[i].scoreLabelRating >= reportSection.categories[i].goalStandard:
                            reportSection.categories[i].scoreLabelProcessedRating = 1;
                            reportSection.scoreLabelProcessedRating = reportSection.scoreLabelProcessedRating + reportSection.categories[i].scoreLabelProcessedRating;
                            break;
                        case reportSection.categories[i].scoreLabelRating === reportSection.categories[i].goalStandard - 1:
                            reportSection.categories[i].scoreLabelProcessedRating = 0.5;
                            reportSection.scoreLabelProcessedRating = reportSection.scoreLabelProcessedRating + reportSection.categories[i].scoreLabelProcessedRating;

                            break;
                        case reportSection.categories[i].scoreLabelRating === reportSection.categories[i].goalStandard - 2:
                            reportSection.categories[i].scoreLabelProcessedRating = 0;
                            reportSection.scoreLabelProcessedRating = reportSection.scoreLabelProcessedRating + reportSection.categories[i].scoreLabelProcessedRating;
                            break;
                    }

                }
                reportSection.scoreLabelProcessedRating = reportSection.scoreLabelProcessedRating / reportSection.categories.length;


                break;
            case 'Interest':
                reportSection.scoreLabelProcessedRating = 0;
                scoreLabelRatings = [0, 0, 0, 0, 0, 0];
                // reportSection.top3CategoriesForInterest.sort(function (a, b) {
                //     return b.percentile-a.percentile;
                // });

                for (var i in reportSection.categories) {
                    goalStandards.push(reportSection.categories[i].goalStandard);
                    index = findIndexWithAttr(reportSection.top3CategoriesForInterest, 'name', reportSection.categories[i].category);
                    if (index === 0) {
                        scoreLabelRatings[i] = 3;
                    }
                    if (index === 1) {
                        scoreLabelRatings[i] = 2;
                    }
                    if (index === 2) {
                        scoreLabelRatings[i] = 1;
                    }
                }

                correlation = getPearsonCorrelation(goalStandards, scoreLabelRatings);

                reportSection.scoreLabelProcessedRating = (correlation + 1) / 2;

                break;

            case 'Personality':
                reportSection.scoreLabelProcessedRating = 0;
                for (var i in reportSection.categories) {
                    switch (true) {
                        case reportSection.categories[i].scoreLabelRating === reportSection.categories[i].goalStandard:
                            reportSection.categories[i].scoreLabelProcessedRating = 1;
                            reportSection.scoreLabelProcessedRating = reportSection.scoreLabelProcessedRating + reportSection.categories[i].scoreLabelProcessedRating;
                            break;
                        case Math.abs(reportSection.categories[i].scoreLabelRating - reportSection.categories[i].goalStandard) === 1:
                            reportSection.categories[i].scoreLabelProcessedRating = 0.5;
                            reportSection.scoreLabelProcessedRating = reportSection.scoreLabelProcessedRating + reportSection.categories[i].scoreLabelProcessedRating;
                            break;
                        case  Math.abs(reportSection.categories[i].scoreLabelRating - reportSection.categories[i].goalStandard) === 2:
                            reportSection.categories[i].scoreLabelProcessedRating = 0;
                            reportSection.scoreLabelProcessedRating = reportSection.scoreLabelProcessedRating + reportSection.categories[i].scoreLabelProcessedRating;
                            break;
                    }
                }
                reportSection.scoreLabelProcessedRating = reportSection.scoreLabelProcessedRating / reportSection.categories.length;
                break;
        }
        reportSection.scoreLabelProcessedRating = +(reportSection.scoreLabelProcessedRating.toFixed(5));
        return reportSection;
    }

    function copyAimExpertInventoryIntoAimReportExcel(aimExpertInventory) {

        aimReportContent = {};
        aimReportContent.name = aimExpertInventory.map(function (elem) {
            return elem.name;
        }).join(",");
        aimReportContent.type = "Career";
        if (!angular.equals(aimReportContent, {})) {
            aimReportInExcel.aimReportContentRequests.push(angular.copy(aimReportContent));
            aimReportContent = {};
        }

        for (var i in aimExpertInventory) {
            if (aimExpertInventory[i].subjects) {
                aimReportContent.name = aimExpertInventory[i].subjects.map(function (elem) {
                    return elem.name;
                }).join(",");
                aimReportContent.type = "Subject";
                if (!angular.equals(aimReportContent, {})) {
                    aimReportInExcel.aimReportContentRequests.push(angular.copy(aimReportContent));
                    aimReportContent = {};
                }
            }
            if (aimExpertInventory[i].leisures) {
                aimReportContent.name = aimExpertInventory[i].leisures.map(function (elem) {
                    return elem.name;
                }).join(",");
                aimReportContent.type = "Leisure";
                if (!angular.equals(aimReportContent, {})) {
                    aimReportInExcel.aimReportContentRequests.push(angular.copy(aimReportContent));
                    aimReportContent = {};
                }
            }

            if ($scope.user.grade === '9' || $scope.user.grade === '10') {
                if ($scope.recommendedStreams) {
                    aimReportContent.name = $scope.recommendedStreams[0].name;
                    aimReportContent.type = "Stream";
                    if (!angular.equals(aimReportContent, {})) {
                        aimReportInExcel.aimReportContentRequests.push(angular.copy(aimReportContent));
                        aimReportContent = {};
                    }
                }
            }
        }
        saveAimReportExcelInDb();
    }

    function saveAimReportExcelInDb() {
        aimReportInExcel.userId = userId;
        $http.post('/api/reports/aimReportInExcel', aimReportInExcel).then(function (response) {

        }, function (response) {

        })
    }

    function isCutOffCleared(score, cutoff) {
        if (score < cutoff) {
            return false;
        } else {
            return true
        }
    }

    function getPossibleStreamsForRecommendation(allStreams) {
        var cutOff = 60;
        $scope.user.academicScoreRange = $scope.user.academicScore.match(/\d+/g).map(Number);
        for (var i in allStreams) {
            allStreams[i].canBeRecommended = false;
            switch (allStreams[i].name) {
                case "PCM":
                    if (isCutOffCleared($scope.user.academicScoreRange[0], cutOff)) {
                        if (isCutOffCleared($scope.user.academicPerformances[0].value, cutOff)) {
                            if (isCutOffCleared($scope.user.academicPerformances[1].value, cutOff)) {
                                allStreams[i].canBeRecommended = true;
                            }
                        }
                    }
                    break;
                case "PCB":
                    if (isCutOffCleared($scope.user.academicScoreRange[0], cutOff)) {
                        if (isCutOffCleared($scope.user.academicPerformances[0].value, cutOff)) {
                            allStreams[i].canBeRecommended = true;
                        }
                    }
                    break;
                case "Commerce with Mathematics":
                    if (isCutOffCleared($scope.user.academicScoreRange[0], cutOff)) {
                        if (isCutOffCleared($scope.user.academicPerformances[1].value, cutOff)) {
                            allStreams[i].canBeRecommended = true;
                        }
                        if (isCutOffCleared($scope.user.academicPerformances[0].value, cutOff)) {
                            allStreams[i].canBeRecommended = true;
                        }
                    }
                    break;
                case "Commerce without Mathematics":
                    allStreams[i].canBeRecommended = true;
                    break;
                case "Humanities":
                    allStreams[i].canBeRecommended = true;
                    break;
            }
        }
        return allStreams;
    }

    function getCareersMatchingByStreams(careers, streams) {
        var result = [];
        var index = -1;
        for (var j in careers) {
            if (careers[j].streams.indexOf(streams[0].name) !== -1) {
                index = findIndexWithAttr(result, 'id', careers[j].id);
                if (index === -1) {
                    result.push(careers[j]);
                }
            }
        }
        return result;
    }

    function removeCareerWithRecommededStream(careers, stream) {
        var result = [];
        for (var i in careers) {
            var careerStreams = careers[i].streams.split(',');
            if (careerStreams.length !== 1 && careerStreams[0] !== stream.name) {
                result.push(careers[i]);
            }
        }
        return result;
    }

    $scope.getReportV2();
});