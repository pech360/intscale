function renderBarChart(selector, data) {       

        /*var selectorArray = $(selector);


        if($(selector).length == 0) {
          setTimeout(renderBarChart(selector, data), 500); 
        }*/


        $(selector).html('');

        data.unshift({
          name : 'dummy',
          color : 'red',
          score : 10
        });

        var outerWidth = $(selector).width(); //1000;  //size of parent;
        var outerHeight = 50 * data.length;  //set this on basis of no. of bar.
        var margin = { left: 130, top: 0, right: 15, bottom: 60 };
        var barPadding = 0.2;

        var xColumn = "score";
        var yColumn = "name";
        var xAxisLabelText = "Population";
        var xAxisLabelOffset = 55;

        var innerWidth  = outerWidth  - margin.left - margin.right;
        var innerHeight = outerHeight - margin.top  - margin.bottom;

        var svg = d3.select(selector).append("svg")
          .attr("width",  outerWidth)
          .attr("height", outerHeight);
        var g = svg.append("g")
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        var xAxisG = g.append("g")
          .attr("class", "x axis")
          .attr("transform", "translate(0," + innerHeight + ")")
        var xAxisLabel = xAxisG.append("text")
          .style("text-anchor", "middle")
          .attr("x", innerWidth / 2)
          .attr("y", xAxisLabelOffset)
          .attr("class", "label");
         //.text(xAxisLabelText);
        var yAxisG = g.append("g")
          .attr("class", "y axis");

        var xScale = d3.scale.linear().range(      [0, innerWidth]);
        var yScale = d3.scale.ordinal().rangeBands([0, innerHeight], barPadding);

        var xAxis = d3.svg.axis().scale(xScale).orient("bottom")
          .ticks(10)
    //.ticks([10,[1,2,3,4,5,6,7,8,9,10]])                   // Use approximately 5 ticks marks.
          .tickFormat(d3.format("s")) // Use intelligent abbreviations, e.g. 5M for 5 Million
          .outerTickSize(0);          // Turn off the marks at the end of the axis.
        var yAxis = d3.svg.axis().scale(yScale).orient("left")
          .outerTickSize(0);          // Turn off the marks at the end of the axis.

        var count = 0;  

        function render(data){

          xScale.domain([0, d3.max(data, function (d){ return d[xColumn]; })]);
          yScale.domain(       data.map( function (d){ return d[yColumn]; }));

          xAxisG.call(xAxis);
          yAxisG.call(yAxis);

          var bars = g.selectAll("rect").data(data);
          bars.enter().append("rect")
            .attr("height", yScale.rangeBand());
          bars
            .attr("x", 0)
            .attr("y",     function (d){ return yScale(d[yColumn]); })
            .attr("width", function (d){ return xScale(d[xColumn]); })
      .attr("fill", function(d) {return d.color; });  
          bars.exit().remove();
        }

        function type(d){
          d.population = +d.population;
    return d;
        }

        render(data);

      } 