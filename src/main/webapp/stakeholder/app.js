var app
    = angular.module('app',
    [
        "angular-jwt",
        "ui.router",
        "toaster",
        "moment-picker",
        "ngAnimate",
        "xeditable",
        "ngStorage",
        "ui.bootstrap",
        "ui.utils",
        "ui.tree",
        "socialLogin",
        "btford.markdown",
        "minicolors",
        "ngFileUpload",
        "chart.js"
    ]);
app.run(function ($rootScope, $location, editableOptions, editableThemes, $localStorage, jwtHelper) {
    editableOptions.theme = 'bs3';


    $rootScope.$on('$viewContentLoaded', function (event) {
        if (event.targetScope.loadScript) {
            $rootScope.loadScript('/assets/js/now-ui-kit.js', 'text/javascript', 'utf-8');
            $rootScope.loadScript('/assets/js/plugins/nouislider.min.js', 'text/javascript', 'utf-8');

            event.targetScope.loadScript = false;
        }
    });

    $rootScope.loadScript = function (url, type, charset) {
        if (type === undefined) type = 'text/javascript';
        if (url) {
            var script = document.querySelector("script[src*='" + url + "']");
            if (!script) {
                var heads = document.getElementsByTagName("head");
                if (heads && heads.length) {
                    var head = heads[0];
                    if (head) {
                        script = document.createElement('script');
                        script.setAttribute('src', url);
                        script.setAttribute('type', type);
                        if (charset) script.setAttribute('charset', charset);
                        head.appendChild(script);
                    }
                }
            }
            return script;
        }
    };
});