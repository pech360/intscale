app.config(function ($stateProvider, $urlRouterProvider, $httpProvider, socialProvider) {
    $urlRouterProvider.otherwise("/stakeholder/invalid");
    $stateProvider
        .state('stakeholder', {
            abstract: true,
            url: "/stakeholder",
            // controller: "MenuController",
            templateUrl: "stakeholder/views/layout/layout.html"
        })
        .state('stakeholder.home', {
            url: "/home",
            controller: 'StakeholderHomeController',
            templateUrl: "stakeholder/views/stakeholderHome.html"
        })
        .state('stakeholder.intro', {
            url: "/intro/:t?/:v?",
            controller: 'IntroController',
            templateUrl: "stakeholder/views/intro.html"
        })
        .state('stakeholder.testpanel', {
            url: "/testpanel/:id?",
            controller: 'StakeholderTestPanelController',
            templateUrl: "stakeholder/views/testpanel.html"
        })
        .state('verify', {
            url: "/verify/{loginKey}",
            controller: 'VerificationController',
            templateUrl: "stakeholder/views/verification.html"
        })

        .state('login', {
            url: "/login/{loginKey}",
            controller: 'LoginController',
            templateUrl: "stakeholder/views/login.html"
        })
        .state('stakeholder.invalid', {
            url: "/invalid",
            templateUrl: "stakeholder/views/invalidpage.html"
        })
    ;

    $httpProvider.interceptors.push(['$q', '$location', '$localStorage', 'jwtHelper',
        function ($q, $location, $localStorage, jwtHelper) {
            return {
                'request': function (config) {
                    config.headers = config.headers || {};
                    if ($localStorage.token) {
                        config.headers.Authorization = 'Bearer ' + $localStorage.token;
                    }
                    return config;
                },
                'responseError': function (response) {
                    //do not redirect on app api rejects @ 403 and they may be due to
                    //autherisation error, not authentication error
                    if (response.status === 401) {
                        $location.path('/login');
                    }
                    return $q.reject(response);
                }
            };
        }]);
});