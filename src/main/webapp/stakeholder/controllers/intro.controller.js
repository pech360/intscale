app.controller('IntroController', function ($scope, $state, toaster, StakeholderService,$stateParams) {
    $scope.showLoadingIcon = false;
    $scope.videoName=$stateParams.v;
    //$scope.videoName='IntscaleV2.mp4';
    $scope.begin = function () {
        $scope.showLoadingIcon = true;
        var constructId=$stateParams.t;
        $state.go('stakeholder.testpanel',{id: constructId});
        $scope.showLoadingIcon = false;
    };
});