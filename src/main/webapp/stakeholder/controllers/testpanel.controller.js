app.controller("StakeholderTestPanelController", function ($scope, $http, $stateParams, $localStorage,StakeholderService, toaster, StakeholderUtils, Auth) {
    $scope.questions = [];
    $scope.currentPage = {};
    $scope.pages = [];
    $scope.answers = [];
    $scope.currentPage = 0;
    $scope.username = null;
    $scope.panel = 'questions';
    $scope.showLoadingImage=true;

    function initLocalStorageTest() {
        var username = Auth.getUser().sub;
        if (username) {
            $scope.username = username;
            if (typeof $localStorage.usetTests == 'undefined') {
                $localStorage.usetTests = {};
            }
            if (typeof $localStorage.usetTests[$scope.username] == 'undefined') {
                $localStorage.usetTests[$scope.username] = {};
            }
            var data = $localStorage.usetTests[$scope.username];
            if (typeof data.answers == 'undefined') {
                data.answers = [];
            }
            if (typeof data.currentPage == 'undefined') {
                data.currentPage = 0;
            }
            $localStorage.usetTests[$scope.username] = data;
        } else {
            toaster.pop('error', 'Unable get user form token.');
            return;
        }
    }

    initLocalStorageTest();

    function getLocalStorageCurrentPage() {
        return $localStorage.usetTests[$scope.username].currentPage;
    }

    function setLocalStorageCurrentPage(currentPage) {
        $localStorage.usetTests[$scope.username].currentPage = currentPage;
    }

    function findAnswer(answers, question) {
        for (var i = 0; i < answers.length; i++) {
            var answer = answers[i];
            if (answer.questionId == question.id) {
                return answer;
            }
        }
        return null;
    }

    function getLocalStorageAnswers() {
        return $localStorage.usetTests[$scope.username].answers;
    }

    function setLocalStorageAnswers(answers) {
        var data = $localStorage.usetTests[$scope.username];
        data.answers = answers;
        $localStorage.usetTests[$scope.username] = data;
    }

    function clearLocalStorageTest(username) {
        $localStorage.usetTests[$scope.username] = {};
    }

    $scope.next = function () {
        $scope.currentPage = $scope.currentPage + 1;
        setLocalStorageCurrentPage($scope.currentPage);
    };

    $scope.previous = function () {
        $scope.currentPage = $scope.currentPage - 1;
        setLocalStorageCurrentPage($scope.currentPage);
    };

    $scope.submitTest = function () {
        var answers = getLocalStorageAnswers();
        $http.post('/api/stakeholder/answers/', answers).then(
            function (response) {
                $http.get('/api/stakeholder/finishTest/').then(function (response) {
                    toaster.pop('info', 'Test Submitted successfully.');
                    clearLocalStorageTest($scope.username);
                    $scope.panel = 'finished';
                }, function (response) {
                    toaster.pop('error', response.data.error);
                })
            }, function (response) {
                if(response.data.error=='InProcess'){
                    $state.go('stakeholder.home');
                }else {
                    toaster.pop('error', response.data.error);
                }
            }
        )
    };

    $scope.optionClicked = function (question, option) {
        for (var i = 0; i < question.options.length; i++) {
            var o = question.options[i];
            o.selected = false;
        }
        option.selected = true;
        var answers = getLocalStorageAnswers();
        var ans = findAnswer(answers, question);
        if (ans == null) {
            ans = {
                questionId: question.id,
                optionId: option.id
            };
            answers.push(ans);
        }
        ans.optionId = option.id;
        setLocalStorageAnswers(answers);
    };

    function loadQuestions() {
        var constId=$stateParams.id;
        StakeholderService.getQuestionsByConstruct(constId).then(function (response) {
            resumeTest(response.data);
            $scope.pages = pagesFromQuestions(response.data);
                $scope.showLoadingImage=false;
        },function (response) {
            toaster.pop('error', response.data.error);
            }
        )
    }
    loadQuestions();

    function resumeTest(data) {
        var answers = getLocalStorageAnswers();
        for (var i = 0; i < answers.length; i++) {
            var answer = answers[i];
            for (var j = 0; j < data.length; j++) {
                var question = data[j];
                if (question.id == answer.questionId) {
                    for (var k = 0; k < question.options.length; k++) {
                        var option = question.options[k];
                        if (option.id == answer.optionId) {
                            option.selected = true;
                            break;
                        }
                    }
                }
            }
        }
        $scope.currentPage = getLocalStorageCurrentPage();
    }

    function pagesFromQuestions(questions) {
        var frames = StakeholderUtils.getQuestionFrames(questions);
        return StakeholderUtils.getQuestionPagesFromFrames(frames);
    }
});