app.controller('VerificationController', function ($scope, $http, $stateParams, toaster) {
    $scope.showPanel = "";
    $scope.errorMessage = "";
    if($stateParams.loginKey){
        $http.get('/verify/stakeholder/' + $stateParams.loginKey).then(function (response) {
            $scope.showPanel="verified";
        }, function (response) {
            if(response.status == 400){
                $scope.showPanel = "already_verified";
                $scope.errorMessage = response.data.error;
            }else{
                $scope.showPanel = "error";
                $scope.errorMessage = response.data.error;
                toaster.pop("error", response.data.error);
            }
        });

    }
});