app.controller('StakeholderHomeController', function ($scope, $state, toaster, StakeholderService) {
    $scope.show404page=false;
    StakeholderService.getConstructs(
        function (data) {
           $scope.constructs=data;
           if(data.length<1){
               $scope.show404page=true;
           }
        },function (data) {
            toaster.pop('error',data.error);
        }
    );
});