app.controller('LoginController', function ($scope, $http, $stateParams, $state, $localStorage, toaster) {
    $http.get('/login/stakeholder/' + $stateParams.loginKey).then(
        function (response) {
            $localStorage.token = response.data.token;
            $state.go('stakeholder.home');
        }, function (response) {
            toaster.pop('error', response.data.error);
        }
    );
});