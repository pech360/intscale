app.service('StakeholderUtils', function () {
    var service = {};

    service.getQuestionFrames = function (questions) {
        if (questions.length > 0) {
            var out = [];
            var set = {};
            for (var i = 0; i < questions.length; i++) {
                var question = questions[i];
                var count = 0;
                if (typeof question.optionCount != 'undefined') {
                    count = question.optionCount;
                } else {
                    if (question.options && question.options.length > 0) {
                        count = question.options.length;
                    }
                }

                question.options.sort(function (a, b) {
                    return a.marks - b.marks;
                });

                if (typeof set[count] == 'undefined') {
                    var obj = {
                        questions: [],
                        options: []
                    };
                    if (question.options && question.options.length > 0) {
                        for (var j = 0; j < question.options.length; j++) {
                            var option = question.options[j];
                            obj.options.push(option.description);
                        }
                    }
                    set[count] = obj;
                    out.push(obj);
                }
                set[count].questions.push(question);
            }
            return out;
        }
        return [];
    };

    service.getQuestionPagesFromFrames = function (questionFrames, pageSize) {
        pageSize = typeof pageSize == 'undefined' || isNaN(pageSize) ? 5 : pageSize;
        var pages = [];
        for (var i = 0; i < questionFrames.length; i++) {
            var frame = questionFrames[i];
            var questions = frame.questions;
            var options = frame.options;

            while (questions.length > 0) {
                var page = {options: options};
                page.questions = (questions.splice(0, pageSize));
                pages.push(page);
            }
        }
        return pages;
    };


    return service;
});