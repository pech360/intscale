app.service('Auth', function (jwtHelper, $http, $localStorage, $timeout) {
    function urlBase64Decode(str) {
        var output = str.replace('-', '+').replace('_', '/');
        switch (output.length % 4) {
            case 0:
                break;
            case 2:
                output += '==';
                break;
            case 3:
                output += '=';
                break;
            default:
                throw 'Illegal base64url string!';
        }
        return window.atob(output);
    }

    var service={};

    service.getUser = function () {
        var token = $localStorage.token;
        let user = {};
        if (typeof token !== 'undefined') {
            user = JSON.parse(urlBase64Decode(token.split('.')[1]));
        }
        return user;
    };

    return service;
});