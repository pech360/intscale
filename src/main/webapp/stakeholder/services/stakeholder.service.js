app.service('StakeholderService', function ($http) {
    var service = {};
    service.getQuestions = function (success, failure,id) {
        $http.get('/api/stakeholder/questions?constructId='+id).then(
            function (response) {
                success(response.data);
            }, function (response) {
                failure(response.data);
            }
        );
    };
    service.getQuestionsByConstruct = function (parentId) {
        return $http.get('/api/stakeholder/questions?constructId='+parentId);
    };
    service.getConstructs = function (success, failure) {
        $http.get('/api/stakeholder/constructors/').then(
            function (response) {
                success(response.data);
            }, function (response) {
                failure(response.data);
            }
        );
    };
    return service;
});

