var app
    = angular.module('app',
        [
            "angular-jwt",
            "ui.router",
            "toaster",
            "moment-picker",
            "ngAnimate",
            "xeditable",
            "ngStorage",
            "ui.bootstrap",
            "ui.utils",
            "ui.tree",
            "socialLogin",
            "btford.markdown",
            "minicolors",
            "ngFileUpload",
            "chart.js",
            "angular-fullcalendar",
            'pascalprecht.translate'
        ]);

app.run(function ($rootScope, $location, editableOptions, editableThemes, $localStorage, jwtHelper, Auth, $translate) {

    if ($location.protocol() == 'http' && $location.absUrl().indexOf('aim2excel.in') != -1) {
        var httpsUrl = $location.absUrl().replace('http', 'https');
        window.location = httpsUrl;
    }

    editableOptions.theme = 'bs3';

    $rootScope.$on('$locationChangeStart', function (event, next, current) {

        // redirect to login page if not logged in and trying to access a restricted page
        var restrictedPage = $.inArray($location.path(), ['/login', '/register', '/landing', '/welcome']) === -1;
        var loggedIn = $localStorage.token && !jwtHelper.isTokenExpired($localStorage.token);

        if ($location.path() == '/welcome' && !loggedIn) {
            $location.path('/welcome');
        }
        // ($location.path() == '/app/availabletests') ||
        if (restrictedPage && !loggedIn) {
            $location.path('/login');
        }

        if (loggedIn && $location.path() == '/login' && $location.url() == '/login') {
            $location.path('/app')
        }
    });

    $rootScope.$on('$includeContentLoaded', function (event, url) {
    });

    $rootScope.$on('$viewContentLoaded', function (event) {
        if (event.targetScope.loadScript) {
            $rootScope.loadScript('/assets/js/now-ui-kit.js', 'text/javascript', 'utf-8');
            $rootScope.loadScript('/assets/js/plugins/nouislider.min.js', 'text/javascript', 'utf-8');

            event.targetScope.loadScript = false;
        }
    });

    $rootScope.loadScript = function (url, type, charset) {
        if (type === undefined) type = 'text/javascript';
        if (url) {
            var script = document.querySelector("script[src*='" + url + "']");
            if (!script) {
                var heads = document.getElementsByTagName("head");
                if (heads && heads.length) {
                    var head = heads[0];
                    if (head) {
                        script = document.createElement('script');
                        script.setAttribute('src', url);
                        script.setAttribute('type', type);
                        if (charset) script.setAttribute('charset', charset);
                        head.appendChild(script);
                    }
                }
            }
            return script;
        }
    };

    $rootScope.$on('$translatePartialLoaderStructureChanged', function () {
        $translate.refresh();
    });

    $(document).ready(function () {
        $rootScope.animateLoader();
    });

    $rootScope.animateLoader = function () {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");
    };

});