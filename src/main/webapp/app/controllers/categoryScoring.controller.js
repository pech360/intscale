app.controller('CategoryScoringController', function ($http, $scope, toaster) {
    $scope.loading = false;
    $scope.test = {};
    var categories = {};
    var subCategories = {};
    var scoreRanges = {};
    var colors = ["table-active", "table-success", "table-warning", "table-danger", "table-info"];
    $scope.newInterestInventory = false;
    $scope.newItem = {};

    $scope.selectedLanguage = "";

    function refresh() {
        getAllTests();
        getAllParentCategories();
    }

    refresh();

    function getAllTests() {
        $http.get('/api/test/all').then(function (response) {
            $scope.allTests = response.data;
        });
    }

    $scope.getAllCategories = function () {
        getAllParentCategories();
        getAllSubCategories();
        $scope.test = {};
    };

    function getAllParentCategories() {
        $http.get('api/category/parent').then(function (response) {
            $scope.parentCategories = response.data;
        });
    }

    function getAllSubCategories() {
        $http.get('api/category/child').then(function (response) {
            subCategories = {};
            subCategories = response.data;
            mergingSubCategoriesIntoCategories($scope.parentCategories, subCategories);
        });
    }

    $scope.getCatgoriesByTest = function (test) {
        $scope.test = test;
        $http.get('/api/category/loadByTest?testId=' + test.id).then(function (response) {
            categories = {};
            categories = response.data;
            if (categories.length > 0) {

            }
            loadSubCategoryByTest(test.id);
        }, function (response) {
            toaster.pop('error', "Error", response.data.message);
        });
    };

    function loadSubCategoryByTest(testId) {
        $http.get('/api/category/loadSubCategoryByTest?testId=' + testId).then(function (response) {
            subCategories = {};
            subCategories = response.data;
            mergingSubCategoriesIntoCategories(categories, subCategories);
        }, function (response) {
            toaster.pop('error', "Error", response.data.message);
        });
    }

    $scope.filterChildCategoryByParent = function (category) {
        $scope.test = {};
        $http.get('api/category/childByParent?parentId=' + category.id).then(function (response) {
            subCategories = {};
            subCategories = response.data;
            categories = [];
            categories.push(category);
            mergingSubCategoriesIntoCategories(categories, subCategories);
        });
    };

    function mergingSubCategoriesIntoCategories(categories, subcategories) {
        $scope.workingCategories = [];
        var count = 0;
        for (var c in categories) {
            categories[c].subcategories = [];
            categories[c].color = colors[Math.floor(Math.random() * colors.length)];
            $scope.workingCategories[count] = $scope.formatScoreRangesForDisplay(categories[c], "junior");
            for (var s in subcategories) {
                if (categories[c].id == subcategories[s].parentId) {
                    $scope.workingCategories[count].subcategories.push($scope.formatScoreRangesForDisplay(subcategories[s], "junior"));
                }
            }
            count++;
        }
    }

    $scope.formatScoreRangesForDisplay = function (category, applicableFor) {
        category.score = [];
        category.score.applicableFor = applicableFor;
        if (category.score.applicableFor == 'senior') {
            category.scoreRanges = category.seniorScoreRanges;
        } else {
            category.scoreRanges = category.juniorScoreRanges;
        }
        for (var s in category.scoreRanges) {
            switch (category.scoreRanges[s].rangeOf) {
                case 'VeryLow':
                    category.score[category.scoreRanges[s].rangeOf] = category.scoreRanges[s].endAt;
                    category.score[category.scoreRanges[s].rangeOf + "Text"] = category.scoreRanges[s].text;
                    category.score[category.scoreRanges[s].rangeOf + "InnateTalent"] = category.scoreRanges[s].innateTalent;
                    category.score[category.scoreRanges[s].rangeOf + "Strength"] = category.scoreRanges[s].strength;
                    category.score[category.scoreRanges[s].rangeOf + "Weakness"] = category.scoreRanges[s].weakness;
                    category.score[category.scoreRanges[s].rangeOf + "LearningStyle"] = category.scoreRanges[s].learningStyle;
                    category.score[category.scoreRanges[s].rangeOf + "LearningStyleInference"] = category.scoreRanges[s].learningStyleInference;
                    category.score[category.scoreRanges[s].rangeOf + "LearningStyleRecommendation"] = category.scoreRanges[s].learningStyleRecommendation;
                    category.score[category.scoreRanges[s].rangeOf + "DevelopmentPlanForStrength"] = category.scoreRanges[s].developmentPlanForStrength;
                    category.score[category.scoreRanges[s].rangeOf + "DevelopmentPlanForWeakness"] = category.scoreRanges[s].developmentPlanForWeakness;
                    break;
                case 'VeryHigh':
                    category.score[category.scoreRanges[s].rangeOf] = 100;
                    category.score[category.scoreRanges[s].rangeOf + "Text"] = category.scoreRanges[s].text;
                    category.score[category.scoreRanges[s].rangeOf + "InnateTalent"] = category.scoreRanges[s].innateTalent;
                    category.score[category.scoreRanges[s].rangeOf + "Strength"] = category.scoreRanges[s].strength;
                    category.score[category.scoreRanges[s].rangeOf + "Weakness"] = category.scoreRanges[s].weakness;
                    category.score[category.scoreRanges[s].rangeOf + "LearningStyle"] = category.scoreRanges[s].learningStyle;
                    category.score[category.scoreRanges[s].rangeOf + "LearningStyleInference"] = category.scoreRanges[s].learningStyleInference;
                    category.score[category.scoreRanges[s].rangeOf + "LearningStyleRecommendation"] = category.scoreRanges[s].learningStyleRecommendation;
                    category.score[category.scoreRanges[s].rangeOf + "DevelopmentPlanForStrength"] = category.scoreRanges[s].developmentPlanForStrength;
                    category.score[category.scoreRanges[s].rangeOf + "DevelopmentPlanForWeakness"] = category.scoreRanges[s].developmentPlanForWeakness;
                    break;
                default:
                    category.score[category.scoreRanges[s].rangeOf] = category.scoreRanges[s].endAt;
                    category.score[category.scoreRanges[s].rangeOf + "Text"] = category.scoreRanges[s].text;
                    category.score[category.scoreRanges[s].rangeOf + "InnateTalent"] = category.scoreRanges[s].innateTalent;
                    category.score[category.scoreRanges[s].rangeOf + "Strength"] = category.scoreRanges[s].strength;
                    category.score[category.scoreRanges[s].rangeOf + "Weakness"] = category.scoreRanges[s].weakness;
                    category.score[category.scoreRanges[s].rangeOf + "LearningStyle"] = category.scoreRanges[s].learningStyle;
                    category.score[category.scoreRanges[s].rangeOf + "LearningStyleInference"] = category.scoreRanges[s].learningStyleInference;
                    category.score[category.scoreRanges[s].rangeOf + "LearningStyleRecommendation"] = category.scoreRanges[s].learningStyleRecommendation;
                    category.score[category.scoreRanges[s].rangeOf + "DevelopmentPlanForStrength"] = category.scoreRanges[s].developmentPlanForStrength;
                    category.score[category.scoreRanges[s].rangeOf + "DevelopmentPlanForWeakness"] = category.scoreRanges[s].developmentPlanForWeakness;
                    break;

            }
        }
        return category;
    };

    $scope.saveCategoryStandardScore = function (category) {
        var flag = true;
        if (category) {
            flag = false;
            $scope.loading = true;
            $http.put('api/category/saveCategoryStandardScore?language=' + $scope.selectedLanguage, category).then(function (response) {
                $scope.loading = false;
                toaster.pop("success", "saved");
            }, function (response) {
                $scope.loading = false;
                toaster.pop("error", response.data.error);
            });
        }
        if (flag) {
            toaster.pop("error", "please enter correct input");
        }
    };

    $scope.saveCategoryRange = function (category) {
        var flag = true;
        if (category) {
            if (category.score.applicableFor) {
                if (category.score.VeryHigh) {
                    if (category.score.High) {
                        if (category.score.Average) {
                            if (category.score.Low) {
                                if (category.score.VeryLow) {
                                    flag = false;
                                    $scope.loading = true;
                                    scoreRanges = {};
                                    scoreRanges = populateScoreRanges(category.score);
                                    $http.put('api/category/saveCategoryScoreRanges/' + category.id + '/' + category.score.applicableFor, scoreRanges).then(function (response) {
                                        $scope.loading = false;
                                        toaster.pop("success", "saved");
                                    }, function (response) {
                                        $scope.loading = false;
                                        toaster.pop("error", response.data.error);
                                    });
                                }
                            }
                        }
                    }
                }
            }
        }
        if (flag) {
            toaster.pop("error", "please enter correct input");
        }
    };


    function populateScoreRanges(sr) {
        var scoreRanges = [];
        for (var s in sr) {
            if (s == "VeryHigh") {
                scoreRanges.push({
                    startFrom: sr.High,
                    endAt: sr.VeryHigh,
                    rangeOf: s,
                    text: sr.VeryHighText,
                    innateTalent: sr.VeryHighInnateTalent,
                    strength: sr.VeryHighStrength,
                    weakness: sr.VeryHighWeakness,
                    learningStyle: sr.VeryHighLearningStyle,
                    learningStyleInference: sr.VeryHighLearningStyleInference,
                    learningStyleRecommendation: sr.VeryHighLearningStyleRecommendation,
                    developmentPlanForStrength: sr.VeryHighDevelopmentPlanForStrength,
                    developmentPlanForWeakness: sr.VeryHighDevelopmentPlanForWeakness
                });
            }
            if (s == "High") {
                scoreRanges.push({
                    startFrom: sr.Average,
                    endAt: sr.High,
                    rangeOf: s,
                    text: sr.HighText,
                    innateTalent: sr.HighInnateTalent,
                    strength: sr.HighStrength,
                    weakness: sr.HighWeakness,
                    learningStyle: sr.HighLearningStyle,
                    learningStyleInference: sr.HighLearningStyleInference,
                    learningStyleRecommendation: sr.HighLearningStyleRecommendation,
                    developmentPlanForStrength: sr.HighDevelopmentPlanForStrength,
                    developmentPlanForWeakness: sr.HighDevelopmentPlanForWeakness

                });
            }
            if (s == "Average") {
                scoreRanges.push({
                    startFrom: sr.Low,
                    endAt: sr.Average,
                    rangeOf: s,
                    text: sr.AverageText,
                    innateTalent: sr.AverageInnateTalent,
                    strength: sr.AverageStrength,
                    weakness: sr.AverageWeakness,
                    learningStyle: sr.AverageLearningStyle,
                    learningStyleInference: sr.AverageLearningStyleInference,
                    learningStyleRecommendation: sr.AverageLearningStyleRecommendation,
                    developmentPlanForStrength: sr.AverageDevelopmentPlanForStrength,
                    developmentPlanForWeakness: sr.AverageDevelopmentPlanForWeakness
                });
            }
            if (s == "Low") {
                scoreRanges.push({
                    startFrom: sr.VeryLow,
                    endAt: sr.Low,
                    rangeOf: s,
                    text: sr.LowText,
                    innateTalent: sr.LowInnateTalent,
                    strength: sr.LowStrength,
                    weakness: sr.LowWeakness,
                    learningStyle: sr.LowLearningStyle,
                    learningStyleInference: sr.LowLearningStyleInference,
                    learningStyleRecommendation: sr.LowLearningStyleRecommendation,
                    developmentPlanForStrength: sr.LowDevelopmentPlanForStrength,
                    developmentPlanForWeakness: sr.LowDevelopmentPlanForWeakness
                });
            }
            if (s == "VeryLow") {
                scoreRanges.push({
                    startFrom: 0,
                    endAt: sr.VeryLow,
                    rangeOf: s,
                    text: sr.VeryLowText,
                    innateTalent: sr.VeryLowInnateTalent,
                    strength: sr.VeryLowStrength,
                    weakness: sr.VeryLowWeakness,
                    learningStyle: sr.VeryLowLearningStyle,
                    learningStyleInference: sr.VeryLowLearningStyleInference,
                    learningStyleRecommendation: sr.VeryLowLearningStyleRecommendation,
                    developmentPlanForStrength: sr.VeryLowDevelopmentPlanForStrength,
                    developmentPlanForWeakness: sr.VeryLowDevelopmentPlanForWeakness
                });
            }
        }
        return scoreRanges;
    }

    $scope.saveScoringFormula = function (scoringFormula) {
        $http.put('api/test/scoringFormula', $scope.test).then(
            function (response) {
                toaster.pop("success", "ok!");
            }, function (response) {
                toaster.pop("error", response.data.error);
            });
    };


    function getInterestInventory() {
        $http.get('/api/reports/interestInventory').then(function (response) {
            $scope.items = response.data;
        }, function (response) {
            error(response.data.error);
        })
    }

    $scope.saveInterestInventory = function (item) {
        $scope.loading = true;
        $http.post('/api/reports/saveInterestInventoryitem', item).then(function (response) {
            if (item.id) {
                toaster.pop("success", "successfully updated");
            } else {
                toaster.pop("success", "Success");
            }
            $scope.loading = false;
            $scope.items = response.data;
            $scope.newItem = {};

        }, function (response) {
            $scope.loading = false;
            toaster.pop("error", response.data.error);
        })
    };

    function getLanguages() {
        $http.get('/api/md/languages/').then(function (response) {
            $scope.languages = response.data;
        });
    }

    $scope.copyLanguageNormToTempNorm = function (language) {
        for (var i in $scope.workingCategories) {
            var normIndex = findIndexWithAttr($scope.workingCategories[i].norms, 'language', language);

            if(normIndex==-1){
                toaster.pop("info","Info","Not Found For Selected Language");
            }else {
                $scope.workingCategories[i].juniorMeanScore = $scope.workingCategories[i].norms[normIndex].juniorMeanScore;
                $scope.workingCategories[i].juniorStandardDeviation = $scope.workingCategories[i].norms[normIndex].juniorStandardDeviation;
                $scope.workingCategories[i].seniorMeanScore = $scope.workingCategories[i].norms[normIndex].seniorMeanScore;
                $scope.workingCategories[i].seniorStandardDeviation = $scope.workingCategories[i].norms[normIndex].seniorStandardDeviation;
                $scope.workingCategories[i].secondaryMeanScore = $scope.workingCategories[i].norms[normIndex].secondaryMeanScore;
                $scope.workingCategories[i].secondaryStandardDeviation = $scope.workingCategories[i].norms[normIndex].secondaryStandardDeviation;


            }

        }

    };


    function findIndexWithAttr(array, attr, value) {
        if (!array) {
            return -1;
        }
        for (var i = 0; i < array.length; i += 1) {
            if (array[i][attr] === value) {
                return i;
            }
        }
        return -1;
    }

    getLanguages();
    getInterestInventory();
});