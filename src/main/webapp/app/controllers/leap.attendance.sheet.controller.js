app.controller('LeapAttendanceSheetController', function ($scope, $state, $http, DateFormatService, toaster, $uibModal) {

    var request = {
        grades: [],
        sections: [],
        products: [],
        fromDate: {},
        toDate: {}
    };
    var userId = '';
    var testId = '';

    $scope.currentGrade = "";
    $scope.currentSection = "";
    $scope.currentSubject = {};

    var now = new Date();
    var ss = now.getSeconds();
    var mm = now.getMinutes();
    var hh = now.getHours();

    var fromD = new Date();
    fromD = moment(fromD);
    fromD = fromD.set('month',3);
    fromD = fromD.startOf('month');
    fromD = fromD.subtract(1, 'years');
    fromD = fromD.set({ 'hour' : hh, 'minute' : mm, 'second' : ss });
    fromD = fromD.format('MM/DD/YYYY HH:mm:ss');


    $scope.fromDate = new Date(fromD);
    $scope.toDate = new Date();

    $scope.shiftFromDate = function (to) {
        switch (to) {
            case "lastWeek":
                fromD = new Date();
                fromD = moment(fromD).subtract(1, 'weeks');
                fromD = fromD.set({ 'hour' : hh, 'minute' : mm, 'second' : ss });
                fromD = fromD.format('MM/DD/YYYY HH:mm:ss');
                $scope.fromDate = new Date(fromD);
                break;
            case "lastMonth":
                fromD = new Date();
                fromD = moment(fromD).subtract(1, 'months');
                fromD = fromD.set({ 'hour' : hh, 'minute' : mm, 'second' : ss });
                fromD = fromD.format('MM/DD/YYYY HH:mm:ss');
                $scope.fromDate = new Date(fromD);
                break;
        }
    };

    $scope.reset = function () {

        var request = {
            grades: [],
            sections: [],
            products: [],
            fromDate: {},
            toDate: {}
        };
    };

    function getAllGrades() {
        $http.get('/api/school/getGradesByTeacher').then(function (response) {
            $scope.grades = response.data;
            $scope.grades = $scope.grades.sort(function (a, b) {
                return a - b
            });

        });
    }

    function getAllSections() {
        $http.get('/api/school/getSectionsByTeacher').then(function (response) {
            $scope.sections = response.data;
            $scope.sections = $scope.sections.sort();
        });
    }

    $scope.getSubjects = function () {
        var flag = true;
        if (!$scope.currentGrade) {
            flag = false;
        }
        if (!$scope.currentSection) {
            flag = false;
        }
        if (flag) {
            request = populateDashRequest($scope.currentGrade, $scope.currentSection);
            $http.post('/api/school/getSubjectsByTeacher', request).then(function (response) {
                var temp = {};
                $scope.subjects = [];
                for(var i in response.data){
                    temp.id = i;
                    temp.name = response.data[i];
                    $scope.subjects.push(angular.copy(temp));
                }
            });
        }
    };

    $scope.selectItem = function (value, type) {
        switch (type) {
            case"grade":
                $scope.currentGrade = value;
                $scope.getSubjects();
                break;
            case"section":
                $scope.currentSection = value;
                $scope.getSubjects();
                break;
            case"subject":
                $scope.currentSubject = value;
                break;
        }
    };

    function validate() {
        if (!$scope.selectedGrade) {
            error("Please select grade");
            return false;
        }

        if (!$scope.selectedSubject) {

        }

        return true;
    }
    function populateDashRequest(grade, section, constructId) {
        request.grades = [];
        request.sections = [];
        request.products = [];
        request.fromDate = DateFormatService.formatDate9($scope.fromDate);
        request.toDate = DateFormatService.formatDate9($scope.toDate);

        if (grade) {
            request.grades.push(grade);
            $scope.currentGrade = grade;
        }
        if (section) {
            request.sections.push(section);
            $scope.currentSection = section;
        }
        if (constructId) {
            request.products.push(constructId);
        }

        return request;

    }


    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }


    $scope.loader = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelLoader = function () {
        $scope.modalInstance.close();
    };


    $scope.getResult = function () {
        request = populateDashRequest($scope.currentGrade,$scope.currentSection, $scope.currentSubject.id);
        if (request) {
            $http.post('/api/school/overAllTestResult/grade/section/construct/attendanceSheet', request).then(function (response) {
                $scope.attendanceSheetResult = response.data;
                processResult($scope.attendanceSheetResult);

            }, function (response) {
            })
        }

    };


    function makeHeaders(chapters) {
        $scope.headers = [];
        for (var i in chapters) {

            var arr = chapters[i].split("_");
            var v = arr.length<=1?arr[0]:arr[arr.length-1];
            $scope.headers.push(v);
        }
    }

    function makeRows(users, attendance) {
        $scope.rows = [];
        var temp = [];
        for (var i in attendance) {
            temp = [];
            temp.push(users[i]);
            temp = temp.concat(attendance[i].split(','));
            $scope.rows.push(angular.copy(temp));
        }

    }


    function processResult(results) {
        makeHeaders(results[1]);
        makeRows(results[0], results[2]);
    }

    $scope.getStudentTestReport = function (userIndex, testIndex, status) {
        if (status == 'Taken') {
            userId = Object.keys($scope.attendanceSheetResult[2])[userIndex];
            testId = Object.keys($scope.attendanceSheetResult[1])[testIndex];
            getReport(userId, testId);

        }


    };

    $scope.getQuestion = function (id) {
        $http.get('/api/question/' + id).then(function (response) {
            $scope.question = response.data;
            $scope.viewQuestion();
        }, function (response) {
            toaster.pop('error', "Failed to Load Question")
        });
    };

    $scope.viewQuestion = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/viewQuestion.html',
            size: 'lg',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.closeViewQuestion = function () {
        $scope.modalInstance.close();
    };

    function openStudentTestReportModal() {
        $scope.studentTestReportModal = $uibModal.open({
            templateUrl: 'views/userResult.html',
            size: 'lg',
            scope: $scope,
            backdrop: 'true',
            animation:true,
            windowClass :'fade: true'
        });
    }

    function closeStudentTestReportModal() {
        $scope.studentTestReportModal.close();
    }

    function getReport(userId, testId) {

        $http.get('/api/resultByUserAndTest/' + userId + '/' + testId).then(function (response) {
            $scope.result = response.data;
            $scope.result.testTakenDate = DateFormatService.formatDate7(new Date($scope.result.testTakenDate));
            calulatePercentage();
            fetchProfileByUserId(userId);
            openStudentTestReportModal();
        }, function (response) {
            toaster.pop('error', "Failed to load result");
        });
    }

    function fetchProfileByUserId(id) {
        $http.get('/api/user/self/userProfile?userId='+id).then(function (response) {
            $scope.user = response.data;
            $scope.user.dob = DateFormatService.formatDate(new Date(response.data.dob));
        });
    }

    function calulatePercentage() {
        var sumofObtainedMarks = 0;
        var sumofMaxMarks = 0;

        for (var i in $scope.result.userAnswerResponses) {
            sumofObtainedMarks = sumofObtainedMarks + $scope.result.userAnswerResponses[i].marks;
            sumofMaxMarks = sumofMaxMarks + $scope.result.userAnswerResponses[i].maxMarks;
        }
        $scope.result.obtainedMarks = sumofObtainedMarks;
        $scope.result.maxMarks = sumofMaxMarks;
        $scope.result.percentage = ((sumofObtainedMarks / sumofMaxMarks) * 100).toFixed(2);
    }


    getAllGrades();
    getAllSections();
    $scope.reset();

});