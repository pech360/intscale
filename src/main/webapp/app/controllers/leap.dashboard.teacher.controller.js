app.controller('LeapDashboardTeacherController', function ($scope, $http, DateFormatService, toaster, $uibModal, $stateParams) {
    $scope.currentGrade = $stateParams.grade;
    $scope.currentSection = $stateParams.section;

    if ($stateParams.fromDate) {
        $scope.fromDate = new Date($stateParams.fromDate);
    } else {
        var now = new Date();
        var ss = now.getSeconds();
        var mm = now.getMinutes();
        var hh = now.getHours();

        var fromD = new Date();
        fromD = moment(fromD);
        fromD = fromD.set('month', 3);
        fromD = fromD.startOf('month');
        fromD = fromD.subtract(1, 'years');
        fromD = fromD.set({ 'hour': hh, 'minute': mm, 'second': ss });
        fromD = fromD.format('MM/DD/YYYY HH:mm:ss');

        $scope.fromDate = new Date(fromD);
    }

    if ($stateParams.toDate) {
        $scope.toDate = new Date($stateParams.toDate);
    } else {
        $scope.toDate = new Date();
    }

    $scope.orgName = window.location.hostname.split(".")[0];

    if (!$scope.currentGrade || !$scope.currentSection) {
        fetchProfile();
    }
    var request = {

        grades: [],
        products: [],
        fromDate: {},
        toDate: {}
    };

    $scope.shiftFromDate = function (to) {
        switch (to) {
            case "lastWeek":
                fromD = new Date();
                fromD = moment(fromD).subtract(1, 'weeks');
                fromD = fromD.set({ 'hour': hh, 'minute': mm, 'second': ss });
                fromD = fromD.format('MM/DD/YYYY HH:mm:ss');
                $scope.fromDate = new Date(fromD);
                break;
            case "lastMonth":
                fromD = new Date();
                fromD = moment(fromD).subtract(1, 'months');
                fromD = fromD.set({ 'hour': hh, 'minute': mm, 'second': ss });
                fromD = fromD.format('MM/DD/YYYY HH:mm:ss');
                $scope.fromDate = new Date(fromD);
                break;
        }
        $scope.getResult();
    };

    $scope.gradeResults = [];
    var testNames = {};

    $scope.reset = function () {
        var request = {
            grades: [],
            products: [],
            fromDate: {},
            toDate: {}
        };
    };

    function fetchProfile() {
        $http.get('/api/user/profile').then(function (response) {
            $scope.user = response.data;
            $scope.user.dob = new Date(response.data.dob);
            $scope.currentGrade = $scope.user.teacher.grade;
            $scope.currentSection = $scope.user.teacher.section;
        });
    }

    $scope.loader = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelLoader = function () {
        $scope.modalInstance.close();
    };

    function getAllGrades() {
        $http.get('/api/school/getGradesByTeacher').then(function (response) {
            $scope.grades = response.data;
            $scope.grades = $scope.grades.sort(function (a, b) {
                return a - b
            });

        });
    }

    function getAllSections() {
        $http.get('/api/school/getSectionsByTeacher').then(function (response) {
            $scope.sections = response.data;
            $scope.sections = $scope.sections.sort();
        });
    }

    $scope.getSubjects = function () {
        var flag = true;
        if (!$scope.currentGrade) {
            flag = false;
        }
        if (!$scope.currentSection) {
            flag = false;
        }
        if (flag) {
            request = populateDashRequest($scope.currentGrade, $scope.currentSection);
            $http.post('/api/school/getSubjectsByTeacher', request).then(function (response) {
                $scope.subjects = Object.values(response.data);
            });
        }
    };

    $scope.selectItem = function (value, type) {
        switch (type) {
            case "grade":
                $scope.currentGrade = value;
                $scope.getSubjects();
                break;
            case "section":
                $scope.currentSection = value;
                $scope.getSubjects();
                break;
            case "subject":
                $scope.currentSubject = value;
                break;
        }

        $scope.getResult();
    };

    function populateDashRequest(grade, section, constructId) {
        request.grades = [];
        request.sections = [];
        request.products = [];
        request.fromDate = DateFormatService.formatDate9($scope.fromDate);
        request.toDate = DateFormatService.formatDate9($scope.toDate);

        if (grade) {
            request.grades.push(grade);
            $scope.currentGrade = grade;
        }
        if (section) {
            request.sections.push(section);
            $scope.currentSection = section;
        }
        if (constructId) {
            request.products.push(constructId);
        }

        return request;

    }


    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function getOverAllDashboardBySubject(grade, subjects, resultType) {

        for (var i in subjects) {
            renderChart(grade, subjects[i], i, resultType);
        }
    }

    function getOverAllDashboard(result, resultType) {
        for (var i in result) {
            getOverAllDashboardBySubject(i, result[i], resultType);
        }
    }

    function renderChart(grade, subject, index, resultType) {

        var temp = {};
        temp.labels = [];
        temp.data = [];
        var tempName = '';

        switch (resultType) {
            case "grade":
                // temp.labels = Object.keys(subject.values);
                temp.data = Object.values(subject.values);

                Object.keys(subject.values).forEach(function (key, index) {
                    switch (key) {
                        case "C":
                            temp.labels.push("0-40%");
                            break;
                        case "B":
                            temp.labels.push("41-70%");
                            break;
                        case "A":
                            temp.labels.push("71-100%");
                            break;
                    }

                });
                $scope.resultByGrades[grade][index].labels = angular.copy(temp.labels);
                $scope.resultByGrades[grade][index].data = angular.copy(temp.data);

                $scope.resultByGrades[grade][index].syllabusCompleted = testNames[$scope.resultByGrades[grade][index].id].syllabusCompleted;
                $scope.resultByGrades[grade][index].name = testNames[$scope.resultByGrades[grade][index].id].name;
                break;

            case "chapter":
                Object.keys(subject.values).forEach(function (key, index) {
                    tempName = testNames[key].name;
                    // tempName = tempName.slice(0, tempName.indexOf("C"));
                    var arr = tempName.split("_");
                    var v = arr.length <= 1 ? arr[0] : arr[arr.length - 1];
                    temp.labels.push(v);
                });
                temp.data = Object.values(subject.values);

                $scope.resultByChapter[grade][index].labels = angular.copy(temp.labels);
                $scope.resultByChapter[grade][index].data = angular.copy(temp.data);
                break;
        }


    }

    $scope.colors = ['#4158d5', '#00bb7e', '#ffc107'];
    $scope.colors2 = ['#3182bd', '#3182bd', '#3182bd', '#3182bd', '#3182bd', '#3182bd', '#3182bd', '#3182bd', '#3182bd', '#3182bd', '#3182bd', '#3182bd', '#3182bd', '#3182bd', '#3182bd', '#3182bd', '#3182bd', '#3182bd', '#3182bd'];
    $scope.barOptions = {
        responsive: true,
        scales: {
            yAxes: [{
                type: 'linear',
                gridLines: {
                    display: true
                },
                ticks: {
                    max: 100,
                    min: 0,
                    stepSize: 20,
                    beginAtZero: true
                }
            }], xAxes: [{
                gridLines: {
                    display: false
                },
                maxBarThickness: 80
            }]
        },
        tooltips: {
            enabled: true,
            mode: 'single',
            callbacks: {
                label: function (tooltipItems, data) {
                    return tooltipItems.yLabel + '%';
                }
            }
        },
        plugins: {
            datalabels: {
                color: 'white',
                display: function (context) {
                    return context.dataset.data[context.dataIndex] > 15;
                },
                font: {
                    weight: 'bold'
                },
                formatter: function (value) {
                    return value + '%';
                    // eq. return ['line1', 'line2', value]
                }
            }
        }
    };

    $scope.pieOptions = {
        responsive: true,
        legend: {
            display: true,
            position: 'right'
        },

        plugins: {
            datalabels: {
                anchor: 'center',
                backgroundColor: function (context) {
                    return context.dataset.backgroundColor;
                },
                color: 'white',
                font: {
                    weight: 'bold'
                },
                formatter: function (value, data) {
                    var allData = data.dataset.data;
                    var total = 0;
                    for (var i in allData) {
                        total += parseFloat(allData[i]);
                    }
                    var t = Math.round((value / total) * 100);
                    return t + '%';
                }
            }
        }
    };

    function getTestNames() {
        $http.get('/api/result/getTestNames', request).then(function (response) {
            testNames = response.data;
            $scope.getResult();
        }, function (response) {

        })
    }

    $scope.getResultForGrades = function () {
        request = populateDashRequest($scope.currentGrade, $scope.currentSection);

        $http.post('/api/school/overAllTestResult/grades', request).then(function (response) {
            $scope.resultByGrades = response.data;
            getOverAllDashboard($scope.resultByGrades, 'grade');
        }, function (response) {

        })
    };

    $scope.getResultForGradesAndSectionAndChapterWise = function () {
        request = populateDashRequest($scope.currentGrade, $scope.currentSection);

        $http.post('/api/school/overAllTestResult/grade/section', request).then(function (response) {
            $scope.resultByChapter = response.data;

            getOverAllDashboard($scope.resultByChapter, 'chapter');
        }, function (response) {

        })
    };

    function getStudentCountByGrade() {
        request = populateDashRequest();

        $http.post('/api/school/getNumberOfStudentsByGrade', request).then(function (response) {

            $scope.studentCountByGrade = response.data;

        }, function (response) {

        })
    }


    $scope.getResult = function () {
        $scope.getResultForGrades();
        $scope.getResultForGradesAndSectionAndChapterWise();
    };


    function init() {

        getAllGrades();
        getAllSections();
        $scope.getSubjects();
        getTestNames();
        getStudentCountByGrade();
        $scope.reset();
    }

    init();

});