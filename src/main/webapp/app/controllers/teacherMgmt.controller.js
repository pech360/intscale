app.filter('pagination', function () {
    return function (input, start) {
        start = +start;
        if (input instanceof Array)
            return input.slice(start);
    };
});
app.controller('TeacherManagementController', function ($scope, $http, toaster, $uibModal, $state) {
    $scope.curPage = 0;
    $scope.pageSize = 10;
    $scope.TotalRecordCount = 0;
    $scope.goToPage = 1;
    $scope.listOfTeachers = {};
    $scope.teacher = {};
    $scope.teacher.gradeSectionSubjects = [];
    $scope.teacherTypes = {};
    $scope.teacher.userId = '';
    $scope.setCurrentPage = function () {
        $scope.curPage = 0;
    };
    $scope.temp = {};
    $scope.selectedUser = {};

    $scope.numberOfPages = function () {
        $scope.pages = [];
        $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
        for (var i = 1; i <= $scope.totalPages; i++) {
            $scope.pages.push(i);
        }
        return $scope.totalPages;
    };

    $http.get('/api/school/all').then(function (response) {
        $scope.listSchooles = response.data;
    });

    $http.get('/api/md/teacherTypes/').then(function (response) {
        $scope.teacherTypes = response.data
    });

    $scope.findAllUserTeacher = function () {
        $http.get('/api/user/self/userByTeacher').then(function (response) {
            $scope.users = response.data;
        });
    };

    $http.get('/api/md/grades/').then(function (response) {
        $scope.grades = response.data
    });

    $scope.subjectByGrade = function (grade) {
        $http.get('/api/test/subjects?grade=' + grade).then(function (response) {
            $scope.subjects = response.data;
        });
    };

    $http.get('/api/md/sections/').then(function (response) {
        $scope.sections = response.data
    });


    $scope.list = function () {
        $http.get('/api/teacher/').then(function (response) {
            $scope.listOfTeachers = response.data;
            $scope.TotalRecordCount = response.data.length;
        }, function (response) {
            toaster.pop('Info', response.error);
        });
    };



    $scope.openTeacherModal = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'createTeacher.html',
            size: 'lg',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancel = function () {
        $scope.modalInstance.close();
    };

    $scope.createNew = function () {
        $scope.teacher = {};
        $scope.teacher.gradeSectionSubjects = [];

        $scope.openTeacherModal();
    };

    $scope.create = function (teacher) {
        if (!teacher.name) {
            toaster.pop('Info', "Please select a user");
            return;
        }
        if (!teacher.education) {
            toaster.pop('Info', "Teacher Education can't be empty");
            return;
        }
        if (teacher.id) {
            $http.put('/api/teacher/', teacher).then(function (response) {
                $scope.list();
                $scope.findAllUserTeacher();
                $scope.cancel();
                ok('Record successfully updated');
            }, function (response) {
                error(response.data.error);
            });
        } else {
            if (!teacher.userId) {
                toaster.pop('Info', "select a user");
                return;
            }
            $http.post('/api/teacher/', teacher).then(function (response) {
                $scope.list();
                $scope.findAllUserTeacher();
                $scope.cancel();
                ok('Record successfully created');
            }, function (response) {
                error(response.data.error);
            });
        }
    };

    $scope.update = function (teacher) {
        $scope.teacher = teacher;
        $scope.teacher.school = JSON.stringify(teacher.school);
        $scope.teacher.userId = JSON.stringify(teacher.userId);
        $scope.teacher.subject = JSON.stringify(teacher.subject);
        $scope.openTeacherModal();
    };

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.deleteRecord = function (id) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.value) {
                $http.delete('/api/teacher?id=' + id).then(function (response) {
                        $scope.list();
                        $scope.findAllUserTeacher();
                        ok('Record deleted successfully');
                    },
                    function (response) {
                        error(response.data.error);
                    });
            }
        });
    };

    $scope.addGradeSectionSubject = function (item) {
        if(angular.equals(item, {})){
            return;
        }
        item.sub = JSON.parse(item.sub);
        var flag = true;
        item.subjectName = item.sub.name;
        item.subject = item.sub.id;

        for(var i in $scope.teacher.gradeSectionSubjects){
            if(item.grade == $scope.teacher.gradeSectionSubjects[i].grade){
                if(item.section == $scope.teacher.gradeSectionSubjects[i].section){
                    if(item.subject == $scope.teacher.gradeSectionSubjects[i].subject){
                        flag = false;
                    }
                }
            }
        }
        if(flag){
            $scope.teacher.gradeSectionSubjects.push(angular.copy(item));
        }
    };

    $scope.removeGradeSectionSubject = function (index) {
        $scope.teacher.gradeSectionSubjects.splice(index, 1);
    };

    $scope.selectUserForTeacher = function (user) {
        user = JSON.parse(user);
        $scope.teacher.userId = user.id;
        $scope.teacher.name = user.name;
    };

    $scope.list();
    $scope.findAllUserTeacher();
});