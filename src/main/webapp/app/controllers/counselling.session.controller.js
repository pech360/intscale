app.filter('pagination', function () {
    return function (input, start) {
        start = +start;
        if (input instanceof Array)
            return input.slice(start);
    };
});

app.controller('CounsellingSessionController', function ($scope, $http, CommonService, toaster, $uibModal, DateFormatService, $stateParams) {
    var userId = $stateParams.id;
    $scope.counsellorIsAdmin = $stateParams.admin;
    $scope.counsellorIsAdmin = (/true/i).test($scope.counsellorIsAdmin) //returns true or false
    $scope.curPage = 0;
    $scope.pageSize = 10;
    $scope.goToPage = 1;
    $scope.TotalRecordCount = 0;
    $scope.loading = false;
    $scope.counsellingSessionType = 'EXPLORE';
    $scope.counsellingSessions = [];
    $scope.workingSession = {};
    $scope.tempLead = '';

    $scope.request = {
        userId: userId,
        scheduledOn: new Date(),
        name: "",
        counsellingSessionStatus: 'UPCOMING',
        counsellingSessionType: 'EXPLORE'
    };

    $scope.filterTags = [];
    $scope.filterSubTags = [];

    $scope.subTags = [];

    $scope.numberOfPages = function () {
        $scope.pages = [];
        $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
        for (var i = 1; i <= $scope.totalPages; i++) {
            $scope.pages.push(i);
        }
        return $scope.totalPages;
    };

    $scope.fetchProfile = function (id) {
        $http.get('/api/user/self/userProfile?userId=' + id).then(function (response) {
            $scope.user = response.data;
            $scope.user.dob = DateFormatService.formatDate(new Date(response.data.dob));
            if ($scope.user.img) {
                $scope.userImageURI = $scope.assets_url + $scope.user.img;
            } else {
                if ($scope.user.gender) {
                    if ($scope.user.gender.toLowerCase() == "male") {
                        $scope.userImageURI = "/assets/img/b-avtar.png"
                    } else {
                        if ($scope.user.gender.toLowerCase() == "female") {
                            $scope.userImageURI = "/assets/img/g-avtar.png"
                        }
                    }
                } else {
                    $scope.userImageURI = "/assets/img/default-profile-picture.png";
                }
            }
        }, function (response) {
            toaster.pop('error', "Error", "Failed to load profile - " + response.data.error);

            if(response.data.error){
                if(response.data.error=="You are logged in another session, so close this session"){

                }
            }
        });
    };

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.loader = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelLoader = function () {
        $scope.modalInstance.close();
    };

    function isValid(request) {
        var flag = true;
        if (!request.name) {
            flag = false;
        }
        if (!request.scheduledOn) {
            flag = false;
        }
        if (!request.counsellingSessionStatus) {
            flag = false;
        }
        return flag;
    }

    $scope.createNewSession = function () {
        $scope.createSession = $uibModal.open({
            templateUrl: 'createCounsellingSession.html',
            size: 'sm',
            scope: $scope,
            backdrop: true
        });

        if($scope.request.counsellingSessionType=="FREE_FLOW"){
            $scope.request.name = "SINGLE SESSION" + ($scope.counsellingSessions.length + 1).toString();
        }else {
            $scope.request.name = $scope.request.counsellingSessionType + ($scope.counsellingSessions.length + 1).toString();
        }

    };

    $scope.closeNewSession = function () {
        $scope.createSession.close();
    };

    $scope.openCounsellingSessionSummary = function (session) {
        loadTags();
        $scope.workingSession = session;
        $scope.filterSubTags = $scope.workingSession.subTags;
        if (!$scope.workingSession.leads) {
            $scope.workingSession.leads = [];
        }
        switch ($scope.workingSession.counsellingSessionStatus) {
            case 'COMPLETED'   :
                $scope.editWorkingSession = false;
                break;
            case 'UPCOMING'   :
                $scope.editWorkingSession = true;
                break;
            case 'MISSED'   :
                $scope.editWorkingSession = true;
                break;
            case 'DELAYED'   :
                $scope.editWorkingSession = false;
                break;
            case 'DRAFTED'   :
                $scope.editWorkingSession = true;
                break;
        }
        $scope.counsellingSessionSummaryModal = $uibModal.open({
            templateUrl: '/views/modals/counsellingSessionSummary.html',
            size: 'lg',
            scope: $scope,
            backdrop: 'static'
        });


    };

    $scope.closeCounsellingSessionSummary = function () {
        $scope.counsellingSessionSummaryModal.close();
    };


    $scope.getCounsellingSessions = function (type) {
        $scope.loading = true;
        $scope.loader();
        $scope.request.counsellingSessionType = type;
        switch (type) {
            case 'EXPLORE':
                $scope.request.class = 'btn btn-warning btn-simple btn-sm';
                break;
            case 'COMMIT':
                $scope.request.class = 'btn btn-default btn-simple btn-sm';
                break;
            case 'ENGAGE':
                $scope.request.class = 'btn btn-primary btn-simple btn-sm';
                break;
            case 'FREE_FLOW':
                $scope.request.class = 'btn btn-danger btn-simple btn-sm';
                break;

        }
        $http.get('/api/counsellor/counsellingSessions?type=' + type + '&userId=' + userId).then(function (response) {
            $scope.counsellingSessions = response.data;

            for (var i in $scope.counsellingSessions) {
                $scope.counsellingSessions[i].scheduledOn = DateFormatService.formatDate($scope.counsellingSessions[i].scheduledOn);

                switch ($scope.counsellingSessions[i].counsellingSessionStatus) {
                    case 'UPCOMING':
                    case 'MISSED':
                        $scope.counsellingSessions[i].label = "Create Notes";
                        break;
                    case 'DRAFTED':
                        $scope.counsellingSessions[i].label = "Edit Notes";
                        break;
                    case 'COMPLETED':
                    case 'DELAYED':
                        $scope.counsellingSessions[i].label = "View Notes";
                        break;
                }
                if ($scope.counsellorIsAdmin) {
                    switch ($scope.counsellingSessions[i].counsellingSessionStatus) {
                        case 'UPCOMING':
                        case 'MISSED':
                        case 'DRAFTED':
                            $scope.counsellingSessions.splice(i, 1);
                            break;
                    }
                }
            }
            $scope.loading = false;
            $scope.cancelLoader();
        }, function (response) {
            $scope.loading = false;
            $scope.cancelLoader();
            alert(response.error);
        })
    };

    $scope.getLeadsFromAllSessions = function () {
        $http.get('/api/counsellor/leads?userId=' + userId).then(function (response) {
            $scope.leadsFromAllSessions = response.data;

            $scope.leadsFromAllSessionsModal = $uibModal.open({
                templateUrl: 'leadsFromAllSessionsModal.html',
                size: 'md',
                scope: $scope,
                backdrop: 'static'
            });
        }, function (response) {
            error(response.error);
        })
    };

    $scope.closeLeadsFromAllSessionsModal = function () {
        $scope.leadsFromAllSessionsModal.close();
    };

    $scope.saveSession = function (request, status) {
        request = angular.copy(request);
        if (status == 'UPCOMING') {
            request.scheduledOn = DateFormatService.formatDate(request.scheduledOn);
        } else {
            request.tags = $scope.filterSubTags;
        }

        request.counsellingSessionStatus = status;

        if (isValid(request)) {
            $http.post('/api/counsellor/counsellingSession', request).then(function (response) {
                if (!angular.equals(response.data, {}) && !request.id) {
                    response.data.scheduledOn = DateFormatService.formatDate(response.data.scheduledOn);
                    response.data.label = "Create Notes";
                    $scope.counsellingSessions.push(response.data);
                } else {
                    switch (response.data.counsellingSessionStatus) {
                        case 'UPCOMING':
                        case 'MISSED':
                            response.data.label = "Create Notes";
                            break;
                        case 'DRAFTED':
                            response.data.label = "Edit Notes";
                            break;
                        case 'COMPLETED':
                        case 'DELAYED':
                            response.data.label = "View Notes";
                            break;
                    }
                    var index = findIndexWithAttr($scope.counsellingSessions, 'id', request.id);
                    response.data.scheduledOn = DateFormatService.formatDate(response.data.scheduledOn);
                    $scope.counsellingSessions.splice(index, 1);
                    $scope.counsellingSessions.push(response.data);
                }
                if (status == 'UPCOMING') {
                    $scope.createSession.close();
                } else {
                    $scope.closeCounsellingSessionSummary();
                }
                $scope.filterSubTags = [];
                $scope.filterTags = [];
                $scope.request.scheduledOn = new Date();
            }, function (response) {
                error(response.error);
                if (status == 'UPCOMING') {
                    $scope.createSession.close();
                } else {
                    $scope.closeCounsellingSessionSummary();
                }
            })
        } else {
            toaster.pop("info", " Incorrect input!!");
        }
    };

    $scope.addTempLeadToWorkingSessionLead = function (tempLead) {
        var flag = true;
        if (!$scope.workingSession.leads) {
            $scope.workingSession.leads = [];
        }

        for (var i in $scope.workingSession.leads) {
            if ($scope.workingSession.leads[i] == tempLead) {
                flag = false;
                toaster.pop("info", "lead already exist");
                return;
            }
        }
        if (flag) {
            $scope.workingSession.leads.push(tempLead);
            $scope.tempLead = '';
        }
        $scope.tempLead = '';
    };

    $scope.fetchProfile(userId);
    $scope.getCounsellingSessions('EXPLORE');


    $scope.tagSelected = function (tag) {
        if (tag) {
            for (var i = 0; i < $scope.tags.length; i++) {
                if (tag.id == $scope.tags[i].id) {
                    // var v = $scope.tags.splice(i, 1)[0];
                    var v = $scope.tags[i];
                    $scope.filterTags.push(v);
                    addSubTag(v);

                    return;
                }
            }
        }
    };

    $scope.removeTag = function (tag) {
        for (i = 0; i < $scope.filterTags.length; i++) {
            if (tag.id == $scope.filterTags[i].id) {
                $scope.tags.push($scope.filterTags.splice(i, 1)[0]);
                // TODO :
                removeSubTagsFromSubtagFilter(tag);
                return;
            }
        }
    };

    function addSubTag(tag) {
        var subtags = tag.subTags;
        // for (j = 0; j < subtags.length; j++) {
        //     $scope.filterSubTags.push(subtags[j]);
        // }
        for (j = 0; j < subtags.length; j++) {
            $scope.subTags.push(subtags[j]);
        }

    }

    function removeSubTagsFromSubtagFilter(tag) {
        if (tag) {
            var subTags = tag.subTags;
            for (i = 0; i < subTags.length; i++) {
                for (j = 0; j < $scope.subTags.length; j++) {
                    if ($scope.subTags[j].id == subTags[i].id) {
                        $scope.subTags.splice(j, 1);
                        break;
                    }
                }
                for (j = 0; j < $scope.filterSubTags.length; j++) {
                    if ($scope.filterSubTags[j].id == subTags[i].id) {
                        $scope.filterSubTags.splice(j, 1);
                        break;
                    }
                }
            }
        }
    }


    $scope.removeSubTag = function (tag) {
        // TODO :
        for (i = 0; i < $scope.filterSubTags.length; i++) {
            if ($scope.filterSubTags[i].id == tag.id) {
                var subTag = $scope.filterSubTags.splice(i, 1)[0];
                $scope.subTags.push(subTag);
            }
        }
    };

    $scope.addSubTag = function (subTag) {
        if (subTag) {
            for (var i = 0; i < $scope.subTags.length; i++) {
                if (subTag.id == $scope.subTags[i].id) {
                    var v = $scope.subTags.splice(i, 1)[0];
                    var flag = true;
                    for (var j in $scope.filterSubTags) {
                        if ($scope.filterSubTags[j].id == v.id) {
                            flag = false;
                        }
                    }
                    if (flag) {
                        $scope.filterSubTags.push(v);
                    }

                    var flag2 = true;
                    var index = findIndexWithAttr($scope.tags, 'id', v.tagId);
                    var index2 = '';
                    if (index > -1) {
                        for (var m in $scope.tags[index].subTags) {
                            index2 = findIndexWithAttr($scope.filterSubTags, 'id', $scope.tags[index].subTags[m].id);
                            if (index2 == -1) {
                                flag2 = false;
                            }

                        }
                        if (flag2) {
                            $scope.tags.splice(index, 1);
                        }
                    }
                    return;
                }
            }
        }
    };

    function loadTags() {
        $http.get('/api/tagByType?type=COUNSELLING_SESSION').then(
            function (response) {
                $scope.tags = response.data;
            },
            function (response) {
                error(response.data.error)
            }
        );
    }

    function findIndexWithAttr(array, attr, value) {
        if (!array) {
            return -1;
        }
        for (var i = 0; i < array.length; i += 1) {
            if (array[i][attr] === value) {
                return i;
            }
        }
        return -1;
    }

    $scope.createFreeFlowNote = function () {

        var note = {
            "counsellingSessionType":"FREE_FLOW",
            "statement":"",
            "response":"",
            "active":true,
            "sample":false,
            "timelinePost":false
        };

        $scope.workingSession.counsellingSessionNotes.push(note);

    };

    loadTags();


});
