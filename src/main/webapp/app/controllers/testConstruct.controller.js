app.filter('pagination', function () {
    return function (input, start) {
        start = +start;
        if (input instanceof Array)
            return input.slice(start);
    };
});
app.controller('TestConstructController',function($scope,$http,$uibModal,toaster){
   $scope.curPage = 0;
   $scope.pageSize = 10;
   $scope.TotalRecordCount=0;
   $scope.goToPage = 1;
   $scope.addTest=false;
   $scope.parentList={};
   $scope.subParentList={};
   $scope.constructObject={};
   $scope.constructObject.parent='';
   $scope.constructObject.id='';
   $scope.constructAvlTests=[];
   $scope.isParent=false;
   $scope.availableTestListForAddInConstruct=[];
   $scope.constructId='';
   $scope.constructTestRequest={};
   $scope.setCurrentPage = function() { $scope.curPage = 0; };

   $scope.numberOfPages = function() {
      $scope.pages = [];
      $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
      for (var i = 1; i <= $scope.totalPages; i++) {
          $scope.pages.push(i);
      }
      return $scope.totalPages;
  };

     $scope.newRecord=function(){
        $scope.addTest=false;
        $scope.parentList={};
        $scope.subParentList={};
        $scope.constructObject={};
        $scope.constructObject.parent='';
        $scope.constructObject.id='';
        $scope.constructAvlTests=[];
        $scope.availableTestListForAddInConstruct=[];
        $scope.constructId='';
        $scope.constructTestRequest={};
        $scope.parentListTest();
      };
   $http.get('/api/teacher').then(function(response){
    $scope.teacherList=response.data;
   });
   $http.get('api/md/grades/').then(function (response) {
       $scope.grades = response.data;
   });
   $scope.listOfConstruct=function(){
    $http.get('/api/test/constructList').then(
    function(response){
     $scope.constructList=response.data;
     $scope.TotalRecordCount=response.data.length;
    },
    function(response)
    {
    error(response.data.error);
    });
   };

   $scope.listOfConstruct();

  $scope.treeListtest=function(){
   $http.get('/api/test/treeList').then(
    function(response){
      $scope.list=response.data;
      $scope.openTreeModal();
    },
    function(response){
     error(response.data.error);
    });
 };

 $scope.openTreeModal = function () {
             $scope.ModalInstance = $uibModal.open({
             templateUrl: 'testTree.html',
             size: 'lg',
             scope: $scope,
             backdrop: 'static'
         });
       };

 $scope.openTestModal = function () {
              $scope.ModalInstance = $uibModal.open({
              templateUrl: 'constructTest.html',
              size: 'sm',
              scope: $scope,
              backdrop: 'static'
          });
        };

 $scope.close = function (){
   $scope.availableTestListForAddInConstruct=[];
   $scope.addTest=false;
  $scope.ModalInstance.close();
  };

 $scope.parentListTest=function(){
  $http.get('/api/test/parentByTenantAndConstruct').then(
  function(response){
  $scope.parentList=response.data;
   if(response.data.length==0){
        toaster.pop('Info','Construct list is empty');
        return;
     }
  },
  function(response){
   error(response.data.error);
  });
 };

 $scope.parentListTest();

 $scope.loadTestByConstruct=function(id){
  $scope.constructId=id;
  $scope.constructAvlTests=[];
  $http.get('/api/test/testByConstructId?id='+id).then(function(response){
    response.data.forEach(function(e){
      $scope.constructAvlTests.push(e);
    });
    $scope.openTestModal();
  },function(response){
    error(response.data.error);
  });
 };

 $scope.removeConstructTest=function(index){
   $scope.constructAvlTests.splice(index,1)
 };

 $scope.loadAvailableTest=function(){
 $scope.availableTestListForAddInConstruct=[];
  $http.get('/api/test/availableTestForConstruct').then(
  function(response){
   response.data.forEach(function(e){
    $scope.availableTestListForAddInConstruct.push(e);
   });
   $scope.addTest=true;
  },
  function(response){
   error(response.data.error);
  });
 };

 var alreadyExits = false;

 $scope.moveTestToConstruct=function(index){
  $scope.test=$scope.availableTestListForAddInConstruct[index];
  $scope.constructAvlTests.forEach(function (e) {
          if ($scope.test.id == e.id) {
              alreadyExits = true;
          }
      });
      if (!alreadyExits) {
          $scope.constructAvlTests.push($scope.test);
      }
      alreadyExits = false;
      $scope.availableTestListForAddInConstruct.splice(index, 1);
 };

 $scope.removeConstructTest=function(index){
    $scope.test=$scope.constructAvlTests[index];
     $scope.availableTestListForAddInConstruct.forEach(function (e) {
             if ($scope.test.id == e.id) {
                 alreadyExits = true;
             }
         });
         if (!alreadyExits) {
             $scope.availableTestListForAddInConstruct.push($scope.test);
         }
     alreadyExits = false;
     $scope.constructAvlTests.splice(index, 1);
 };

 $scope.updateTestForConstruct=function(){
   $scope.testIds=[];
   $scope.constructAvlTests.forEach(function(e){
    $scope.testIds.push(e.id);
   });
   $scope.constructTestRequest.id=$scope.constructId;
   $scope.constructTestRequest.tests=$scope.testIds;
   $http.put('/api/test/updateConstructTest',$scope.constructTestRequest).then(
   function(response){
   $scope.close();
   $scope.constructTestRequest={};
   $scope.constructId='';
   $scope.addTest=false;
   $scope.constructAvlTests=[];
   ok('Construct Test Updated Successfully');
   },function(response){
    error(response.data.error);
   });
 };

 $scope.subParentListTest=function(id){
   $http.get('/api/test/testByParentAndConstruct?id='+JSON.parse(id).id).then(
          function(response){
           $scope.subParentList=response.data;
           if(response.data.length==0){
               toaster.pop('Info','SubConstruct is not available for selected Construct');
               return;
            }
          },
          function(response){
           error(response.data.error);
          });
  };

  $scope.loadConstructByLeaf=function(leaf){
     if(leaf){
     $http.get('/api/test/allConstructByLeafNodeIsTrue').then(function(response){
                $scope.constructList=response.data;
                $scope.TotalRecordCount=response.data.length;
         },function(response){
       error(response.data.error);
     });
   }else{
     $scope.listOfConstruct();
   }
  };

 $scope.addParent = function (test) {
    if(!test){
    $scope.remove();
    return;
    }
   $scope.constructObject.parent=JSON.parse(test).name;
   $scope.constructObject.parentId=JSON.parse(test).id;
   $scope.constructObject.grades=[];
   $scope.isParent=true;
   };

 $scope.remove= function () {
    $scope.constructObject.parent='';
    $scope.constructObject.parentId='';
    $scope.constructObject.grades=[];
    $scope.isParent=false;
 };

 $scope.saveConstruct=function(constructObject){
  if(!constructObject.name){
    toaster.pop('Info',"Construct name can't be empty");
    return;
  }
  if(!constructObject.introduction){
              toaster.pop('Info',"Construct introduction can't be empty");
              return;
            }
   /* if(!constructObject.procedure){
        toaster.pop('Info',"Construct procedure can't be empty");
        return;
      }
  if(!constructObject.participation){
          toaster.pop('Info',"Construct participation can't be empty");
          return;
        }
  if(!constructObject.disclaimer){
        toaster.pop('Info',"Construct disclaimer can't be empty");
        return;
      }
  if(!constructObject.description){
      toaster.pop('Info',"Construct description can't be empty");
      return;
    }*/
  if(constructObject.grades!=null&&!constructObject.grades.length && !constructObject.parent){
        toaster.pop('Info',"Parent Construct grades can't be empty");
        return;
      }
  if(constructObject.id){
    $http.put('/api/test/construct',constructObject).then(
         function(response){
          ok("Construct updated successfully");
          $scope.constructObject={};
          $scope.parentListTest();
          $scope.listOfConstruct();
         },
         function(response)
         {
         error(response.data.error);
         });
  }else{
   $http.post('/api/test/construct',constructObject).then(
     function(response){
      ok("Construct created successfully");
      $scope.constructObject={};
      $scope.parentListTest();
      $scope.listOfConstruct();
     },
     function(response)
     {
     error(response.data.error);
     });
  }
 };

 $scope.updateConstruct=function(construct){
  $scope.constructObject=angular.copy(construct);
 };

 $scope.deleteConstruct=function(id){
      swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.value) {
            $http.delete('/api/test/construct/'+id).then(function(response){
              ok('Construct deleted successfully');
              $scope.listOfConstruct();
            },
             function (response) {
                 error("Construct can't be delete because it have parent relation");
             });
             }
        });
    };

 function ok(message) {
              swal({
                  title: message,
                  type: 'success',
                  buttonsStyling: false,
                  confirmButtonClass: "btn btn-warning"
              });
          };

 function error(message) {
          swal({
              title: message,
              type: 'error',
              buttonsStyling: false,
              confirmButtonClass: "btn btn-warning"
          });
      };

});
