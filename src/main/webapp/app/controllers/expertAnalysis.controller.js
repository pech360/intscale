app.controller("ExpertAnalysisController",function ($http, $state, $scope,  toaster) {

refresh();

function refresh() {
$scope.feedbackObject={};
$http.get('/api/category').then(function(response) {
                $scope.category=response.data;
});
};

$scope.loadDataByCategory=function(id){
 $http.get('/api/category/findOne?id='+id).then(function(response){
   $scope.listOfdata=response.data.feedbacks;
   });
};

$scope.save = function(feedbackObject) {
$scope.feedbackObject.categoryId=$scope.categoryId;
if(!$scope.feedbackObject.categoryId){
   toaster.pop('Error',"Please select Category ");
   return;
 }
 if(!$scope.feedbackObject.expertAnaylsis){
    toaster.pop('Error',"expertAnaylsis  is empty");
    return;
  }
   if(!$scope.feedbackObject.developementPlan){
      toaster.pop('Error',"developementPlan  is empty");
      return;
    }
  if($scope.feedbackObject.marksMax<$scope.feedbackObject.marksMin){
   toaster.pop('Error',"Please select correctMarks ");
    return;
         }
   $http.put('/api/category/feedback', feedbackObject).then(function(response) {
   $scope.loadDataByCategory($scope.feedbackObject.categoryId);
   if(feedbackObject.id){
     ok("Record updated successfully");
        $scope.addNew=false;
      $scope.feedbackObject={};
   }
   else{
     ok("Record created successfully");
        $scope.addNew=false;
      $scope.feedbackObject={};
   }
   }, function(response) {
   error(response.data.error);
   });
 };


$scope.updateRecord=function(record){
 $scope.addNew=true;
 $scope.feedbackObject=angular.copy(record);
};

$scope.resetRow=function(){
$scope.addNew=true;
$scope.feedbackObject={};
};

$scope.removeRecord=function(record){
   $http.delete('/api/category/deleteFeedbacks?categoryId='+record.categoryId+'&id='+record.id).then(function(response) {
      $scope.loadDataByCategory(record.categoryId);
      ok("Record deleted successfully");
       }, function(response) {
       error(response.data.error);
   });
};

$scope.head=function(){
$scope.tabhead=true;
}

 function ok(message) {
         swal({
             title: message,
             type: 'success',
             buttonsStyling: false,
             confirmButtonClass: "btn btn-warning"
         });
     };
 function error(message) {
     swal({
         title: message,
         type: 'error',
         buttonsStyling: false,
         confirmButtonClass: "btn btn-warning"
     });
 };
});