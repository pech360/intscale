app.filter('pagination', function()
{
return function(input, start)
{
 start = +start;
 if(input instanceof Array)
 return input.slice(start);
};
});
app.controller('StudentGradeController',function($scope,$http,CommonService,toaster,$uibModal){
  $scope.curPage = 0;
  $scope.pageSize = 10;
  $scope.goToPage = 1;
  $scope.TotalRecordCount=0;
  $scope.studentArray=[];
  $scope.studentGrade={};
  var countStudent=0;
  $scope.schoolId='';
  $scope.gradeId='';
  $scope.grade='';
  $scope.section='';
  $scope.studentId='';

  $scope.numberOfPages = function() {
       $scope.pages = [];
       $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
       for (var i = 1; i <= $scope.totalPages; i++) {
           $scope.pages.push(i);
       }
       return $scope.totalPages;
   };

 $http.get('/api/school/all').then(function(response){
          $scope.listSchooles=response.data;
         });

 $http.get('/api/md/grades/').then(function(response){
      $scope.listGrades=response.data;
     });

 $http.get('/api/md/sections/').then(function(response){
      $scope.listSections=response.data;
     });

 $scope.list=function(){
        $http.get('/api/student/').then(function(response){
        $scope.listOfStudents=response.data;
        $scope.TotalRecordCount=response.data.length;
        var x=0;
        $scope.listOfStudents.forEach(function(element){
           $scope.listOfStudents[x].selectStudent = false;
           x=x+1;
          });
       },function(response){
          toaster.pop('Info',response.error);
       });
  };

  $scope.list();

  $scope.filterSearch=function(){
    $http.get('/api/student/filterList?schoolId='+$scope.schoolId+'&gradeId='+$scope.gradeId).then(
    function(response){
      if(response.data.length==0){
         toaster.pop('Info','List is empty..');
         return;
      }
      $scope.listOfStudents=response.data;
      $scope.TotalRecordCount=response.data.length;
      var x=0;
      $scope.listOfStudents.forEach(function(element){
      $scope.listOfStudents[x].selectStudent = false;
      x=x+1;
      });
    },
    function(response){
      toaster.pop('Info',response.error);
    });
  };

  $scope.isSelectAllStudent = function () {
      if ($scope.selectAllStudent) {
          $scope.studentArray=[];
          for (countStudent=0; countStudent<$scope.TotalRecordCount; countStudent++) {
          $scope.listOfStudents[countStudent].selectStudent = true;
          $scope.studentArray.push($scope.listOfStudents[countStudent].id);
         }
      }else{
          for (countStudent=0; countStudent<$scope.TotalRecordCount; countStudent++) {
          $scope.listOfStudents[countStudent].selectStudent = false;
         }
         $scope.studentArray=[];
      }
  };

  $scope.isStudentSelected = function (student) {
      if (student.selectStudent){
       $scope.studentArray.push(student.id);
      }else{
      var i=0;
      $scope.studentArray.forEach(function(element){
      if(element==student.id){
      $scope.studentArray.splice(i, 1);
      }
      i++;
      });
      i=0;
      }
  };

  $scope.saveAll=function(){
    if($scope.studentArray.length<1){
      toaster.pop('Info',"Student is not selected");
       return;
    }
    if(!$scope.grade){
      toaster.pop('Info',"Grade is not selected");
       return;
    }
    if(!$scope.section){
      toaster.pop('Info',"Grade is not selected");
       return;
    }
    $scope.studentGrade.studentIds=$scope.studentArray;
    $scope.studentGrade.grade=$scope.grade;
    $scope.studentGrade.section=$scope.section;
    $http.put('api/student/assignGrade',$scope.studentGrade).then(function(response){
         if(response.data){
           ok("Grade & section Assign successfully");
           $scope.selectAllStudent=false;
           $scope.studentArray=[];
           $scope.filterSearch();
           $scope.studentGrade={};
         }else{
           error(response.data.error);
         }
      }, function (response) {
          error(response.data.error);
      });
  };

  $scope.removeAll=function(){
   if($scope.studentArray.length<1){
     toaster.pop('Info',"Student is not selected");
      return;
   }
   $scope.studentGrade.studentIds=$scope.studentArray;
   $http.put('/api/student/removeAll',$scope.studentGrade).then(
   function(response){
     if(response.data){
          ok("Grade Remove successfully");
          $scope.selectAllStudent=false;
          $scope.studentArray=[];
          $scope.filterSearch();
          $scope.studentGrade={};
     }else{error(response.data.error);}
   },
   function(response){
     error(response.data.error);
   }
   );
  };

  $scope.manageGradeAssignment=function(student){
   $scope.studentId=student.id;
   $scope.studentName=student.name;
   $scope.studentGrade=JSON.stringify(student.grade);
   $scope.studentSection=JSON.stringify(student.section);
   $scope.openStudentModal();
  };

  $scope.openStudentModal =  function () {
       $scope.modalInstance = $uibModal.open({
           templateUrl: 'studentGradeView.html',
           size: 'sm',
           scope: $scope,
           backdrop: 'static'
       });
  };

   $scope.cancel = function() {
      $scope.studentId='';
      $scope.modalInstance.close();
   };

  $scope.updateStudentGrade=function(grade,section){
       if(!grade){
          toaster.pop('Info',"Grade is not selected");
          return;
       }
       if(!section){
         toaster.pop('Info',"Grade is not selected");
         return;
      }
       if(!$scope.studentId){
          toaster.pop('Info',"Student is not selected");
          return;
       }
    $http.put('/api/student/updateGrade?id='+$scope.studentId+'&gradeId='+grade+'&sectionId='+section).then(
    function(response){
       if(response.data){
           $scope.cancel();
           ok("Record Updated successfully");
           $scope.studentArray=[];
           $scope.filterSearch();
       }else{error(response.data.error);}
    },
    function(response){
       error(response.data.error);
    });
  };

  function ok(message) {
          swal({
              title: message,
              type: 'success',
              buttonsStyling: false,
              confirmButtonClass: "btn btn-warning"
          });
      };
  function error(message) {
      swal({
          title: message,
          type: 'error',
          buttonsStyling: false,
          confirmButtonClass: "btn btn-warning"
      });
  };
});