app.controller('DashboardController', function ($scope, $http, DateFormatService) {
    var request = {
        grades: [],
        schools: [],
        products: [],
        tests: [],
        fromDate: {},
        toDate: {}
    };
    $scope.fromDate = new Date;
    $scope.toDate = new Date;

    function getAllProducts() {
        $http.get('/api/product').then(function (response) {
            $scope.products = response.data;
            $scope.products.isAllSelected = function () {
                for (var i = 0; i < this.length; i++) {
                    var item = this[i];
                    if (!item.selected) {
                        return false;
                    }
                }
                return true;
            }
        }, function (response) {
            error(response.data.error);
        })
    }

    $scope.loadTestByProduct = function (productId) {
        $http.get('/api/product/testByProduct?id=' + productId).then(function (response) {
            $scope.tests = response.data;
            $scope.tests.isAllSelected = function () {
                for (var i = 0; i < this.length; i++) {
                    var item = this[i];
                    if (!item.selected) {
                        return false;
                    }
                }
                return true;
            }
        });
    };

    function getAllTests() {
        $http.get('/api/test/listAllTakenTest').then(function (response) {
            $scope.tests = response.data;
            $scope.tests.isAllSelected = function () {
                for (var i = 0; i < this.length; i++) {
                    var item = this[i];
                    if (!item.selected) {
                        return false;
                    }
                }
                return true;
            }
        });
    }

    function getAllGrades() {
        $http.get('api/md/grades/').then(function (response) {
            $scope.grades = response.data;
            $scope.grades.isAllSelected = function () {
                for (var i = 0; i < this.length; i++) {
                    var item = this[i];
                    if (!item.selected) {
                        return false;
                    }
                }
                return true;
            }
        });
    }

    function getAllSchools() {
        $http.get("/api/school/all").then(function (response) {
            $scope.schools = response.data;
            $scope.schools.isAllSelected = function () {
                for (var i = 0; i < this.length; i++) {
                    var item = this[i];
                    if (!item.selected) {
                        return false;
                    }
                }
                return true;
            }
        });
    }

    function getAllGender() {
        $scope.genders = [
            {gender: "Male", "selected": false},
            {gender: "Female", "selected": false}
        ];
        $scope.genders.isAllSelected = function () {
            for (var i = 0; i < this.length; i++) {
                var item = this[i];
                if (!item.selected) {
                    return false;
                }
            }
            return true;
        }
    }

    $scope.selectAll = function (items, selected) {
        if (selected) {
            for (var i in items) {
                items[i].selected = true;
            }
            return items;
        } else {
            unselectAll(items);
        }
    };

    function unselectAll(items) {
        for (var i in items) {
            items[i].selected = false;
        }
        return items;
    }

    function getDashboard() {
        $http.post('/api/dashboard/loadDashboardDataByUserRoles', request).then(function (response) {
            $scope.dashboardResponse = response.data;
        }, function (response) {
            error(response.data.error);
        })
    }

    $scope.populateDashRequest = function () {
        request.grades = [];
        request.schools = [];
        request.products = [];
        request.tests = [];
        request.fromDate = DateFormatService.formatDate9($scope.fromDate);
        request.toDate = DateFormatService.formatDate9($scope.toDate);

        for (var i in $scope.grades) {
            if ($scope.grades[i].selected) {
                if ($scope.grades[i].value) {
                    request.grades.push($scope.grades[i].value);
                }
            }
        }

        for (var i in $scope.schools) {
            if ($scope.schools[i].selected) {
                if ($scope.schools[i].name) {
                    request.schools.push($scope.schools[i].name);
                }
            }
        }
        for (var i in $scope.products) {
            if ($scope.products[i].selected) {
                if ($scope.products[i].id) {
                    request.products.push($scope.products[i].id);
                }
            }
        }
        for (var i in $scope.tests) {
            if ($scope.tests[i].selected) {
                if ($scope.tests[i].id) {
                    request.tests.push($scope.tests[i].id);
                }
            }
        }
        getDashboard();
    };

    getAllProducts();
    //getAllTests();
    getAllGrades();
    getAllSchools();
    $scope.populateDashRequest();

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

});