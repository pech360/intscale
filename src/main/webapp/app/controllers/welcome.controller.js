app.controller('WelcomeController', function ($scope, $http, Auth, $state, $rootScope, toaster, $localStorage, $uibModal, DateFormatService) {

    $scope.currentSlide = 'welcome'
    $scope.password = {
        newpswd: '',
        cnfimPswd: ''
    }
    $scope.pswdNotMatch = false;

    $scope.goTo = function (slide) {
        $scope.currentSlide = slide;
        

    };
    $scope.changePassword = function (goto) {


        // if ($scope.password.newpswd !== $scope.password.cnfimPswd) {
        //     $scope.pswdNotMatch = true;
        //     $scope.password.newpswd = '';
        //     $scope.password.cnfimPswd = '';
        //     error('Password Dosn\'t match, Please Try Again ');

        // } else {

        $http.put('/api/user/self/selfUpdatePassword', $scope.password.newpswd).then(function () {

            if (goto === 'Logout') {
                $scope.currentSlide = 'comeBack';
            } else {

                $state.go('app.availabletests');
            }




        }, function (response) {
            error(response.data.error);
        });
        // };

    }

    $scope.logOut = function () {
        Auth.logout(function () {
            $rootScope.user = null;
            // window.open("https://aim2excel.in/success-stories/");
            location.href = "https://aim2excel.in";
        });

        // window.open("https://aim2excel.in/success-stories/");


    }
    function ok(message) {
        return swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    };
    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    };
});