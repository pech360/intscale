app.controller('CreateReportSectionController', function ($scope, $http, toaster) {
    $scope.report = {};
    $scope.newReportSection = {};
    $scope.reportSection = {};
    $scope.showScoreRange = false;
    $scope.selectedTests = [];
    var selectedTestsIds = [];
    $scope.selectedLanguage = "";

    $scope.saveReportSection = function (reportSection) {
        reportSection.idsOfTestForEvaluation = getIdsOfSelectedTests();
        if (!reportSection.name) {
            error("Name can not be empty");
        } else {
            if (!reportSection.sequence) {
                reportSection.sequence = 0;
            }
            $http.post('/api/reports/saveReportSection', reportSection).then(function (response) {
                if (reportSection.id) {
                    ok("successfully updated")
                } else {
                    ok("Success! Now, add score ranges to it");
                }
                $scope.reportSection = response.data;
                $scope.showScoreRange = true;
            }, function (response) {
                error(response.data.error);
            })
        }
    };

    $scope.saveReportSectionRange = function (reportSection) {
        var flag = true;
        if (reportSection) {
            if (reportSection.score.applicableFor) {
                if (reportSection.score.VeryHigh) {
                    if (reportSection.score.High) {
                        if (reportSection.score.Average) {
                            if (reportSection.score.Low) {
                                if (reportSection.score.VeryLow) {
                                    flag = false;
                                    $scope.loading = true;
                                    scoreRanges = {};
                                    scoreRanges = populateScoreRanges(reportSection.score);
                                    $http.put('api/reports/saveReportSectionScoreRanges/' + reportSection.id + '/' + reportSection.score.applicableFor, scoreRanges).then(function (response) {
                                        $scope.loading = false;
                                        $scope.reportSection = {};
                                        $scope.showScoreRange = false;
                                        getReportSections();
                                        toaster.pop("success", "saved");
                                    }, function (response) {
                                        $scope.loading = false;
                                        toaster.pop("error", response.data.error);
                                    });
                                }
                            }
                        }
                    }
                }
            }
        }
        if (flag) {
            toaster.pop("error", "please enter correct input");
        }
    };

    function populateScoreRanges(sr) {
        var scoreRanges = [];
        for (var s in sr) {
            if (s == "VeryHigh") {
                scoreRanges.push({startFrom: sr.High, endAt: sr.VeryHigh, rangeOf: s, text: sr.VeryHighText});
            }
            if (s == "High") {
                scoreRanges.push({startFrom: sr.Average, endAt: sr.High, rangeOf: s, text: sr.HighText});
            }
            if (s == "Average") {
                scoreRanges.push({startFrom: sr.Low, endAt: sr.Average, rangeOf: s, text: sr.AverageText});
            }
            if (s == "Low") {
                scoreRanges.push({startFrom: sr.VeryLow, endAt: sr.Low, rangeOf: s, text: sr.LowText});
            }
            if (s == "VeryLow") {
                scoreRanges.push({startFrom: 0, endAt: sr.VeryLow, rangeOf: s, text: sr.VeryLowText});
            }
        }
        return scoreRanges;
    }

    $scope.formatScoreRangesForDisplay = function (reportSection, applicableFor) {
        reportSection.score = [];
        reportSection.score.applicableFor = applicableFor;
        if (reportSection.score.applicableFor == 'senior') {
            reportSection.scoreRanges = reportSection.seniorScoreRanges;
        } else {
            reportSection.scoreRanges = reportSection.juniorScoreRanges;
        }
        for (var s in reportSection.scoreRanges) {
            switch (reportSection.scoreRanges[s].rangeOf) {
                case 'VeryLow':
                    reportSection.score[reportSection.scoreRanges[s].rangeOf] = reportSection.scoreRanges[s].endAt;
                    reportSection.score[reportSection.scoreRanges[s].rangeOf + "Text"] = reportSection.scoreRanges[s].text;
                    break;
                case 'VeryHigh':
                    reportSection.score[reportSection.scoreRanges[s].rangeOf] = 100;
                    reportSection.score[reportSection.scoreRanges[s].rangeOf + "Text"] = reportSection.scoreRanges[s].text;
                    break;
                default:
                    reportSection.score[reportSection.scoreRanges[s].rangeOf] = reportSection.scoreRanges[s].endAt;
                    reportSection.score[reportSection.scoreRanges[s].rangeOf + "Text"] = reportSection.scoreRanges[s].text;
            }
        }
        return reportSection;
    };

    function getReportSections() {
        $http.get('/api/reports/getReportSections').then(function (response) {
            $scope.reportSections = response.data;
            for (var i in $scope.reportSections) {
                $scope.reportSections[i] = $scope.formatScoreRangesForDisplay($scope.reportSections[i], "junior");
            }
        }, function (response) {
            error(response.data.error);
        })
    }

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.getReportSection = function (id) {
        $http.get('/api/reports/getReportSection/' + id).then(function (response) {
            $scope.reportSection = response.data;
        }, function (response) {
            error(response.data.error);
        })
    };

    function getTests() {
        $http.get('/api/test/listAllTakenTest/').then(function (response) {
            $scope.tests = response.data;
        }, function (response) {
            toaster.pop('error', "Error", response.data.message);
        });
    }

    function getIdsOfSelectedTests() {
        selectedTestsIds = [];
        for (var i in $scope.selectedTests) {
            selectedTestsIds.push($scope.selectedTests[i].id);
        }
        return selectedTestsIds;
    }

    $scope.addSelectedTest = function (test) {
        test = JSON.parse(test);
        var alreadyThere = false;
        for (var i = 0; i < $scope.selectedTests.length; i++) {
            if ($scope.selectedTests[i].id == test.id) {
                alreadyThere = true;
            }
        }
        if (!alreadyThere) {
            $scope.selectedTests.push(test);
        }
    };

    $scope.removeSelectedTest = function (index) {
        $scope.selectedTests.splice(index, 1)
    };


    $scope.saveCategoryStandardScore = function (reportSection,selectedLanguage) {
        var flag = true;
        if (reportSection) {
            flag = false;
            $scope.loading = true;
            $http.put('api/reports/saveReportStandardScore?language=' + selectedLanguage, reportSection).then(function (response) {
                $scope.loading = false;
                toaster.pop("success", "saved");
            }, function (response) {
                $scope.loading = false;
                toaster.pop("error", response.data.error);
            });
        }
        if (flag) {
            toaster.pop("error", "please enter correct input");
        }
    };

    function getLanguages() {
        $http.get('/api/md/languages/').then(function (response) {
            $scope.languages = response.data;
        });
    }

    $scope.copyLanguageNormToTempNorm = function (language) {
        for (var i in $scope.reportSections) {
            var normIndex = findIndexWithAttr($scope.reportSections[i].norms, 'language', language);

            if(normIndex==-1){
                toaster.pop("info","Info","Not Found For Selected Language");
            }else {
                $scope.reportSections[i].juniorMeanScore = $scope.reportSections[i].norms[normIndex].juniorMeanScore;
                $scope.reportSections[i].juniorStandardDeviation = $scope.reportSections[i].norms[normIndex].juniorStandardDeviation;
                $scope.reportSections[i].seniorMeanScore = $scope.reportSections[i].norms[normIndex].seniorMeanScore;
                $scope.reportSections[i].seniorStandardDeviation = $scope.reportSections[i].norms[normIndex].seniorStandardDeviation;
                $scope.reportSections[i].secondaryMeanScore = $scope.reportSections[i].norms[normIndex].secondaryMeanScore;
                $scope.reportSections[i].secondaryStandardDeviation = $scope.reportSections[i].norms[normIndex].secondaryStandardDeviation;
            }

        }

    };


    function findIndexWithAttr(array, attr, value) {
        if (!array) {
            return -1;
        }
        for (var i = 0; i < array.length; i += 1) {
            if (array[i][attr] === value) {
                return i;
            }
        }
        return -1;
    }
    getTests();
    getReportSections();
    getLanguages();
});



