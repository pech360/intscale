app.filter('pagination', function()
{
    return function(input, start)
    {
        start = +start;
        if(input instanceof Array)
            return input.slice(start);
    };
});
app.controller('tenantProductAllotmentController',function($scope,$http,CommonService,toaster,$uibModal){
    $scope.curPage = 0;
    $scope.pageSize = 10;
    $scope.goToPage = 1;
    $scope.TotalRecordCount=0;
    $scope.productAllotment={};

    $scope.numberOfPages = function() {
        $scope.pages = [];
        $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
        for (var i = 1; i <= $scope.totalPages; i++) {
            $scope.pages.push(i);
        }
        return $scope.totalPages;
    };
    function getList() {
        $http.get('/api/test/getProductAllotment').then(function (response) {
            $scope.listOfAllotments=response.data;
            $scope.TotalRecordCount=response.data.length;
        });
    };
    getList();
    $http.get('/api/tenant/getTenants').then(function (response) {
        $scope.tenants=response.data;
    });
    $http.get('/api/product/').then(function (response) {
       $scope.products=response.data;
    });

    $scope.saveTenantProducts=function (request) {
        if(!request.tenantIds){
            toaster.pop('info','Please select Tenants.');
            return;
        }
        if(!request.productIds){
            toaster.pop('info','Please select Products.');
            return;
        }
        $http.put('/api/test/productAllotment',request).then(function (response) {
            getList();
           ok('Product Allotted Successfully');
        },function (response) {
            error(response.data.error);
        })
    };

    $scope.remove = function (id,tenantId) {
        swal({
            title: 'Are you sure for Delete?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.value) {
                $http.delete('/api/test/removeProductAllotment?id='+id+'&tenantId='+tenantId).then(function (response) {
                    getList();
                    ok('Record Deleted successfully');
                },
                function (response) {
                    error(response.data.error);
                }
                );
            }
        });
    };

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    };
    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    };

});