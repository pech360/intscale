app.controller('AfterTakingtestController', function ($scope, $http, $state, $stateParams, toaster) {

    var productId = $stateParams.productId;
    var constructId = $stateParams.constructId;
    var products = {};
    var product = {};
    var construct = {};

    function getAllAvailableTest() {
        $http.get('/api/test/listAvailableTests').then(function (response) {
            products = response.data;
            product = getSelectedComponent(productId, products);
            if (product) {
                construct = getSelectedComponent(constructId, product.subParents);
                if (construct) {
                    startTakingTest(construct);
                } else {
                    $state.go("app.product({id: product.id})");
                }
            } else {
                $state.go("app.availableTests")
            }
        }, function (response) {
            toaster.pop('error', "Error", response.data.message);
        });
    }

    function getSelectedComponent(id, component) {
        for (var i in component) {
            if (component[i].id == id) {
                return component[i];
            }
        }
    }

    function startTakingTest(construct) {
        var id = '';
        if (construct.testChild[0].id) {
            id = construct.testChild[0].id;
        }
        $state.go('app.testPanel', { 'testId': id, 'productId': productId, 'constructId': construct.id });
    }

    getAllAvailableTest();


});


