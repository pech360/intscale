app.controller('ImportTestController', function ($http, $scope, $uibModal, toaster) {

    $scope.dryrun = true;
    $scope.testType = '';
    $scope.showDisplayResponse = false;
    var records = {};

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            text: 'Please upload the same file again with correction',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.loader = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelLoader = function () {
        $scope.modalInstance.close();
    };

    $scope.setFiles = function (element) {
        $scope.testFile = element.files[0];
    };

    $scope.upload = function () {
        if ($scope.testFile) {
            if ($scope.testType) {
                $scope.loader();
                var r = new FileReader();
                var fd = new FormData();
                fd.append('file', $scope.testFile);
                fd.append('dryrun', $scope.dryrun);
                fd.append('testType', $scope.testType);
                var request = {
                    method: 'POST',
                    url: '/api/test/import',
                    data: fd,
                    headers: {
                        'Content-Type': undefined
                    }
                };
                $http(request).then(function (response) {
                        $scope.responseList = response.data;
                        $scope.cancelLoader();
                        displayResponse();
                        ok("success");

                    }, function (response) {
                        $scope.cancelLoader();
                        $scope.responseList = response.data;
                        displayResponse();
                        error(response.data.error);
                    }
                );


            } else {
                toaster.pop('error', "Please select testType");
            }

        } else {
            toaster.pop('error', "Please select file");
        }

    };

    $scope.uploadTestQuestions = function () {
        if ($scope.testFile) {
                $scope.loader();
                var r = new FileReader();
                var fd = new FormData();
                fd.append('file', $scope.testFile);
                fd.append('dryrun', $scope.dryrun);
                var request = {
                    method: 'POST',
                    url: '/api/test/testQuestionImport',
                    data: fd,
                    headers: {
                        'Content-Type': undefined
                    }
                };
                $http(request).then(function (response) {
                        $scope.responseList = response.data;
                        $scope.cancelLoader();
                        displayResponse();
                        ok("success");

                    }, function (response) {
                        $scope.cancelLoader();
                        $scope.responseList = response.data;
                        displayResponse();
                        error(response.data.error);
                    }
                );
        } else {
            toaster.pop('error', "Please select file");
        }

    };

    var displayResponse = function () {
        $scope.res = [];
        $scope.showDisplayResponse = true;
        for (property in $scope.responseList) {
            records = {};
            records.name = property;
            records.value = $scope.responseList[property];
            $scope.res.push(records);
        }
    }
});