app.controller('CareerInterestInventoryController', function ($http, $scope, $uibModal, toaster, DateFormatService) {


    var records = {};
    $scope.dryrun = true;

    $scope.showDisplayResponse = false;

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: 'error',
            type: 'error',
            text: message,
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.loader = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelLoader = function () {
        $scope.modalInstance.close();
    };

    $scope.setFiles = function (element) {
        $scope.careerInterestInventoryFile = element.files[0];
    };


    $scope.uploadCareerInterestInventoryFile = function () {

        if ($scope.careerInterestInventoryFile) {
            $scope.loader();
            var r = new FileReader();
            var fd = new FormData();
            fd.append('file', $scope.careerInterestInventoryFile);
            fd.append('dryrun', $scope.dryrun);


            var request = {
                method: 'POST',
                url: '/api/aimExpert/careerInterestInventory/import',
                data: fd,
                headers: {
                    'Content-Type': undefined
                }
            };
            $http(request).then(function (response) {
                    $scope.responseList = response.data;
                    $scope.cancelLoader();
                    ok("success");
                    if ($scope.responseList) {
                        displayResponse();
                    }
                }, function (response) {
                    $scope.cancelLoader();
                    $scope.responseList = response.data;
                    displayResponse();
                    error(response.data.error);
                }
            );
        } else {
            toaster.pop('error', "Please select file");
        }
    }


    $scope.getCareerInterestInventory = function () {

        $scope.loader();

        $http.get('/api/aimExpert/careerInterestInventory').then(function (response) {
                $scope.careerInterestRecords = response.data;
                $scope.cancelLoader();

            }, function (response) {
                $scope.cancelLoader();
                error(response.data.error);
            }
        );

    }


    var displayResponse = function () {
        $scope.res = [];
        $scope.showDisplayResponse = true;
        for (var property in $scope.responseList) {
            records = {};
            records.name = property;
            records.value = $scope.responseList[property];
            $scope.res.push(records);
            $scope.res.sort(function (a, b) {
                return a.name - b.name;
            })
        }
    };


    $scope.getCareerInterestInventory();
});