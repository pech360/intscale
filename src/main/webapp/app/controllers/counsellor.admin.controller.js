app.filter('pagination', function () {
    return function (input, start) {
        start = +start;
        if (input instanceof Array)
            return input.slice(start);
    };
});
app.controller('CounsellorAdminController', function ($scope, $http, CommonService, toaster, $uibModal, DateFormatService) {
    $scope.curPage = 0;
    $scope.pageSize = 10;
    $scope.goToPage = 1;
    $scope.TotalRecordCount = 0;
    $scope.counsellorStudent = {};
    $scope.loading = false;

    var request = {
        grades: [],
        schools: [],
        genders: [],
        subTags: [],
        cities: []
    };

    $scope.filterTags = [];
    $scope.filterSubTags = [];
    $scope.subTags = [];

    $scope.numberOfPages = function () {
        $scope.pages = [];
        $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
        for (var i = 1; i <= $scope.totalPages; i++) {
            $scope.pages.push(i);
        }
        return $scope.totalPages;
    };


    $scope.listUsersByCounsellorAdmin = function () {
        request = populateDashRequest();
        $http.post('/api/counsellor/listByCounsellorAdmin', request).then(function (response) {
            $scope.listOfStudents = response.data;
            var sessions = [];
            for (var i in $scope.listOfStudents) {
                $scope.listOfStudents[i].aimAssessmetsCompletedOn = DateFormatService.formatDate(new Date($scope.listOfStudents[i].aimAssessmetsCompletedOn))
                $scope.listOfStudents[i].counsellorAssignedOn = DateFormatService.formatDate(new Date($scope.listOfStudents[i].counsellorAssignedOn))

                sessions = $scope.listOfStudents[i].userCounsellingSessions
                for (var j in sessions) {
                    switch (sessions[j].counsellingSessionStatus) {
                        case 'UPCOMING' :
                        case 'MISSED'   :
                        case 'DRAFTED'  :
                            $scope.listOfStudents[i].userCounsellingSessions.splice(j, 1);
                            break;
                        case 'COMPLETED':
                        case 'DELAYED'  :
                            break;
                    }
                }
            }

            $scope.TotalRecordCount = response.data.length;
        }, function (response) {
            toaster.pop('Info', response.error);
        });
    };


    $scope.openStudentProfileModal = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'studentProfile.html',
            size: 'lg',
            scope: $scope,
            backdrop: true
        });
    };

    $scope.close = function () {
        $scope.modalInstance.close();
    };

    $scope.viewStudentProfile = function (userId) {
        $scope.fetchProfile(userId);
    };

    $scope.fetchProfile = function (id) {
        $http.get('/api/user/self/userProfile?userId=' + id).then(function (response) {
            $scope.user = response.data;
            $scope.user.dob = DateFormatService.formatDate(new Date(response.data.dob));
            $scope.openStudentProfileModal();
        }, function (response) {
            toaster.pop('error', "Error", "Failed to load profile - " + response.data.error);

            if(response.data.error){
                if(response.data.error=="You are logged in another session, so close this session"){

                }
            }
        });
    };


    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.downloadIndividualReport = function (id, updateReport) {
        $scope.loader();
        $http.get('api/reports/downloadAimReport/' + id + '/' + updateReport + '/MAIN', {responseType: 'arraybuffer'}).then(function (response) {
            $scope.cancelLoader();
            var blob = new Blob([response.data], {
                type: 'application/octet-stream'
            });
            saveAs(blob, response.headers("fileName"));
        }, function (response) {
            $scope.cancelLoader();
            error("download failed");
        })
    };

    $scope.downloadAimReportScoreCardInExcel = function (userId) {
        $scope.loader();
        $http.get('/api/reports/downloadAimReportScoreCardInExcel?userId=' + userId).then(function (response) {
            var blob = new Blob([response.data], {
                type: 'application/octet-stream'
            });
            $scope.cancelLoader();
            saveAs(blob, response.headers("fileName"));

        }, function (response) {
            $scope.cancelLoader();
        })
    };

    $scope.loader = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelLoader = function () {
        $scope.modalInstance.close();
    };

    function getAllGrades() {
        $http.get('api/md/grades/').then(function (response) {
            $scope.grades = response.data;
            $scope.grades.isAllSelected = function () {
                for (var i = 0; i < this.length; i++) {
                    var item = this[i];
                    if (!item.selected) {
                        return false;
                    }
                }
                return true;
            }
        });
    }

    function getAllCities() {
        $http.get('api/md/cities/').then(function (response) {
            $scope.cities = response.data;
            $scope.cities.isAllSelected = function () {
                for (var i = 0; i < this.length; i++) {
                    var item = this[i];
                    if (!item.selected) {
                        return false;
                    }
                }
                return true;
            }
        });
    }

    function getAllSchools() {
        $http.get("/api/school/all").then(function (response) {
            $scope.schools = response.data;
            $scope.schools.isAllSelected = function () {
                for (var i = 0; i < this.length; i++) {
                    var item = this[i];
                    if (!item.selected) {
                        return false;
                    }
                }
                return true;
            }
        });
    }

    function getAllGender() {
        $scope.genders = [
            {gender: "Male", "selected": false},
            {gender: "Female", "selected": false}
        ];
        $scope.genders.isAllSelected = function () {
            for (var i = 0; i < this.length; i++) {
                var item = this[i];
                if (!item.selected) {
                    return false;
                }
            }
            return true;
        }
    }

    $scope.tagSelected = function (tag) {
        if (tag) {
            for (var i = 0; i < $scope.tags.length; i++) {
                if (tag.id == $scope.tags[i].id) {
                    var v = $scope.tags.splice(i, 1)[0];
                    // var v = $scope.tags[i];
                    $scope.filterTags.push(v);
                    addSubTag(v);
                    return;
                }
            }
        }
    };

    $scope.removeTag = function (tag) {
        for (i = 0; i < $scope.filterTags.length; i++) {
            if (tag.id == $scope.filterTags[i].id) {
                $scope.tags.push($scope.filterTags.splice(i, 1)[0]);
                // $scope.filterTags.splice(i, 1)[0];
                removeSubTagsFromSubtagFilter(tag);
                return;
            }
        }
    };

    function addSubTag(tag) {
        var subtags = tag.subTags;
        // for (j = 0; j < subtags.length; j++) {
        //     $scope.filterSubTags.push(subtags[j]);
        // }
        for (j = 0; j < subtags.length; j++) {
            $scope.subTags.push(subtags[j]);
        }
    }

    function removeSubTagsFromSubtagFilter(tag) {
        if (tag) {
            var subTags = tag.subTags;
            for (i = 0; i < subTags.length; i++) {
                for (j = 0; j < $scope.subTags.length; j++) {
                    if ($scope.subTags[j].id == subTags[i].id) {
                        $scope.subTags.splice(j, 1);
                        break;
                    }
                }
                for (j = 0; j < $scope.filterSubTags.length; j++) {
                    if ($scope.filterSubTags[j].id == subTags[i].id) {
                        $scope.filterSubTags.splice(j, 1);
                        break;
                    }
                }
            }
        }
    }


    $scope.removeSubTag = function (tag) {
        // TODO :
        for (i = 0; i < $scope.filterSubTags.length; i++) {
            if ($scope.filterSubTags[i].id == tag.id) {
                var subTag = $scope.filterSubTags.splice(i, 1)[0];
                $scope.subTags.push(subTag);
            }
        }
    };

    $scope.addSubTag = function (subTag) {
        if (subTag) {
            for (var i = 0; i < $scope.subTags.length; i++) {
                if (subTag.id == $scope.subTags[i].id) {
                    var v = $scope.subTags.splice(i, 1)[0];
                    var flag = true;
                    for (var j in $scope.filterSubTags) {
                        if ($scope.filterSubTags[j].id == v.id) {
                            flag = false;
                        }
                    }
                    if (flag) {
                        $scope.filterSubTags.push(v);
                    }

                    var flag2 = true;
                    var index = findIndexWithAttr($scope.tags, 'id', v.tagId);
                    var index2 = '';
                    if (index > -1) {
                        for (var m in $scope.tags[index].subTags) {
                            index2 = findIndexWithAttr($scope.filterSubTags, 'id', $scope.tags[index].subTags[m].id);
                            if (index2 == -1) {
                                flag2 = false;
                            }

                        }
                        if (flag2) {
                            $scope.tags.splice(index, 1);
                        }
                    }
                    return;
                }
            }
        }
    };


    function loadTags() {
        $http.get('/api/tagByType?type=COUNSELLING_SESSION').then(
            function (response) {
                $scope.tags = response.data;
                $scope.filterSubTags = [];
                $scope.filterTags = [];
                $scope.listUsersByCounsellorAdmin();
            },
            function (response) {
                error(response.data.error)
            }
        );
    }

    function findIndexWithAttr(array, attr, value) {
        if (!array) {
            return -1;
        }
        for (var i = 0; i < array.length; i += 1) {
            if (array[i][attr] === value) {
                return i;
            }
        }
        return -1;
    }

    $scope.selectAll = function (items, selected) {
        if (selected) {
            for (var i in items) {
                items[i].selected = true;
            }
            return items;
        } else {
            unselectAll(items);
        }
    };

    function unselectAll(items) {
        for (var i in items) {
            items[i].selected = false;
        }
        return items;
    }


    $scope.reset = function () {

        var request = {
            grades: [],
            schools: [],
            genders: [],
            subTags: [],
            cities: []

        };


        unselectAll($scope.genders, false);
        unselectAll($scope.grades, false);
        unselectAll($scope.schools, false);
        unselectAll($scope.cities, false);
        loadTags();

    };

    function populateDashRequest() {
        request.grades = [];
        request.schools = [];
        request.genders = [];
        request.subTags = [];
        request.cities = [];

        for (var i in $scope.grades) {
            if ($scope.grades[i].selected) {
                if ($scope.grades[i].value) {
                    request.grades.push($scope.grades[i].value);
                }
            }
        }

        for (var i in $scope.schools) {
            if ($scope.schools[i].selected) {
                if ($scope.schools[i].name) {
                    request.schools.push($scope.schools[i].name);
                }
            }
        }
        for (var i in $scope.genders) {
            if ($scope.genders[i].selected) {
                if ($scope.genders[i].gender) {
                    request.genders.push($scope.genders[i].gender);
                }
            }
        }
        for (var i in $scope.cities) {
            if ($scope.cities[i].selected) {
                if ($scope.cities[i].value) {
                    request.cities.push($scope.cities[i].value);
                }
            }
        }
        for (var i in $scope.filterSubTags) {
            request.subTags.push($scope.filterSubTags[i].id);
        }

        return request;

    }

    getAllGrades();
    getAllGender();
    getAllSchools();
    getAllCities();
    $scope.reset();



});
