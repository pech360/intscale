app.controller("SubscriptionKeyController",function($scope, $http, Auth, CommonService, $state, toaster){
         $scope.selectedTest=[];
         $scope.selectedHobby=[];
       $scope.userMasterData = function () {
          CommonService.listGrades().then(function (response) {
              $scope.grades = response.data;
          });
          CommonService.listHobbies().then(function (response) {
              $scope.hobbiesData = response.data;
          });
          CommonService.listStudentTypes().then(function (response) {
              $scope.studentTypes = response.data;
           });
       };
       $http.get('/api/test/').then(function(response){
        $scope.testData=response.data;
        //alert(JSON.stringify($scope.testData));
       });

       $scope.userMasterData();

       $scope.addTest=function(multipleTest) {
           //alert(JSON.stringify(multipleTest));
           multipleTest=JSON.parse(multipleTest);
                   var alreadyThere = false;
                   if($scope.selectedTest==null){
                     $scope.selectedTest=[];
                   }
                   for (var i = 0; i < $scope.selectedTest.length; i++) {
                       if ($scope.selectedTest[i].name == multipleTest.name) {
                           alreadyThere = true;
                           toaster.pop('info',multipleTest.name+" is already added");
                       }
                   }
                   if (!alreadyThere) {
                           $scope.selectedTest.push(multipleTest);
                           alreadyThere = false;
                       }
            };
         $scope.removeTest = function (index) {
                 $scope.selectedTest.splice(index, 1)
             };

        $scope.addHobby=function(multipleHobbies) {
            //alert(JSON.stringify(multipleTest));
            multipleHobbies=JSON.parse(multipleHobbies);
                    var alreadyThere = false;
                    if($scope.selectedHobby==null){
                      $scope.selectedHobby=[];
                    }
                    for (var i = 0; i < $scope.selectedHobby.length; i++) {
                        if ($scope.selectedHobby[i].name == multipleHobbies.name) {
                            alreadyThere = true;
                            toaster.pop('info',multipleHobbies.name+" is already added");
                        }
                    }
                    if (!alreadyThere) {
                            $scope.selectedHobby.push(multipleHobbies);
                            alreadyThere = false;
                        }
             };
          $scope.removeHobby = function (index) {
                  $scope.selectedHobby.splice(index, 1)
              };

          $scope.generateKeys=function(){
                 $scope.testkey.tests=$scope.selectedTest;
                 $scope.testkey.hobbies=$scope.selectedHobby;
                 $http.post('/api/subscriber',$scope.testkey).then(function(response){
                   $scope.keysData=response.data.uniqueTestKey;
                   toaster.pop('success',"Key("+$scope.keysData+") Generated Successfully..");
                 });
             };

});