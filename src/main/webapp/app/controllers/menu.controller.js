app.controller('MenuController', function ($scope, $state, $http) {

    $http.get('/api/user/menu').then(function (response) {
        $scope.menu = response.data;
    });
});