app.controller('IndividualReportController', function ($http, $scope, $stateParams, DateFormatService) {
    var testId = $stateParams.id;
    loadCategoryByTest(testId);
    fetchTestDetails(testId);
    $scope.userId='0';
    fetchProfile();

    $scope.getReport = function () {
        $http.get('/api/reports/individualV1?id='+testId+'&userId='+$scope.userId).then(
            function (response) {
                var categoryDataForGraph = response.data;
                renderRadarChart(categoryDataForGraph);
                renderBarChart(categoryDataForGraph);
            }, function () {
            });

    };

    $scope.getReport();

    function loadCategoryByTest(testId) {
        $http.get('/api/category/loadByTest?testId=' + testId).then(function (response) {
            $scope.categories = response.data;
        }, function (response) {
            toaster.pop('error', "Error", response.data.message);
        })
    }

    function fetchProfile() {
        $http.get('/api/user/profile').then(function (response) {
            $scope.user = response.data;
            $scope.user.dob = DateFormatService.formatDate(new Date(response.data.dob));
        });
    }

    function fetchTestDetails() {
        $http.get('/api/result/test?testId='+testId+'&userId='+$scope.userId).then(function (response) {
            $scope.testDetails = response.data;
            $scope.testDetails.testTakenDate = DateFormatService.formatDate7(new Date($scope.testDetails.testTakenDate));
        });
    }

    function renderRadarChart(categoryDataForGraph) {

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////RadarChart  start here////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var w = 200,
            h = 200;

        var colorscale = d3.scaleOrdinal(d3.schemeCategory10);

//Legend titles
        var LegendOptions = ['Components'];
        var catData = [];
        for (var c in categoryDataForGraph) {
            categoryDataForGraph[c] = categoryDataForGraph[c].toFixed(2);
            catData.push({axis: c, value: categoryDataForGraph[c]});
        }
//Data
        var d = [catData];

//Options for the Radar chart, other than default
        var mycfg = {
            w: w,
            h: h,
            maxValue: 0.6,
            levels: 6,
            ExtraWidthX: 300
        };

//Call function to draw the Radar chart
//Will expect that data is in %'s
        RadarChart.draw("#radarChart", d, mycfg);

////////////////////////////////////////////
/////////// Initiate legend ////////////////
////////////////////////////////////////////

        var svg = d3.select('#radarChartDiv')
            .selectAll('svg')
            .append('svg')
            .attr("width", w + 300)
            .attr("height", h)

//Create the title for the legend
        var text = svg.append("text")
            .attr("class", "title")
            .attr('transform', 'translate(90,0)')
            .attr("x", w - 70)
            .attr("y", 10)
            .attr("font-size", "12px")
            .attr("fill", "#404040")
            .text("What we have found");

//Initiate Legend
        var legend = svg.append("g")
            .attr("class", "legend")
            .attr("height", 100)
            .attr("width", 200)
            .attr('transform', 'translate(90,20)')
        ;
        //Create colour squares
        legend.selectAll('rect')
            .data(LegendOptions)
            .enter()
            .append("rect")
            .attr("x", w - 65)
            .attr("y", function (d, i) {
                return i * 20;
            })
            .attr("width", 10)
            .attr("height", 10)
            .style("fill", function (d, i) {
                return colorscale(i);
            })
        ;
        //Create text next to squares
        legend.selectAll('text')
            .data(LegendOptions)
            .enter()
            .append("text")
            .attr("x", w - 52)
            .attr("y", function (d, i) {
                return i * 20 + 9;
            })
            .attr("font-size", "11px")
            .attr("fill", "#737373")
            .text(function (d) {
                return d;
            })
        ;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////RadarChart  ends here////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }


    function renderBarChart(categoryDataForGraph) {
        data = [];
        for (var c in categoryDataForGraph) {
            data.push({component: c, value: categoryDataForGraph[c] * 100});
        }

        var svg = dimple.newSvg("#chartContainer", 490, 300);

        var myChart = new dimple.chart(svg, data);
        // myChart.setBounds("5%", "5%", "80%", "80%");
        // myChart.setBounds("90px", "20px", "60%", "60%");
        myChart.setMargins("20%", "5%", "5%", "15%");
        var x = myChart.addMeasureAxis("x", "value");
        var y = myChart.addCategoryAxis("y", "component");
        x.ticks = 5;
        myChart.addSeries("component", dimple.plot.bar);
        myChart.defaultColors = [
            new dimple.color("#03A9F4", "#2980b9", 1), // blue
            new dimple.color("#F44336", "#c0392b", 1), // red
            new dimple.color("#2ecc71", "#27ae60", 1), // green
            new dimple.color("#9C27B0", "#8e44ad", 1), // purple
            new dimple.color("#e67e22", "#d35400", 1), // orange
            new dimple.color("#f1c40f", "#f39c12", 1), // yellow
            new dimple.color("#1abc9c", "#16a085", 1), // turquoise
            new dimple.color("#95a5a6", "#7f8c8d", 1)  // gray
        ];
        myChart.draw();
    }
});