app.controller('MyNewsController', function ($scope) {
    $scope.newsFeed = [
        // {
        //     "SNO": "0",
        //     "UserId": "3",
        //     "Interest": "Politics",
        //     "PageLink": [
        //         {
        //             'url': 'https://www.moneycontrol.com/news/india/lok-sabha-election-polls-campaign-tracker-live-updates-3631201.html',
        //             'imgLink': 'https://www.static-news.moneycontrol.com/static-mcnews/2019/03/Narendra-Modi-3-378x213.jpg',
        //             'date': '12 march 2019',
        //             'title': 'Lok Sabha election tracker LIVE: SP-BSP tie-up won\'t last beyond May 23, says PM Modi',
        //         },
        //         {
        //             'url': 'https://www.moneycontrol.com/news/politics/mha-notice-on-rahul-gandhis-citizenship-rubbish-says-priyanka-gandhi-3915271.html',
        //             'imgLink': 'https://www.static-news.moneycontrol.com/static-mcnews/2019/04/Rahul-Gandhi_priyanka-378x213.jpg',
        //             'date': '12 march 2019',
        //             'title': 'MHA notice on Rahul Gandhi\'s citizenship rubbish, says Priyanka Gandhi',
        //         },
        //         {
        //             'url': 'https://www.moneycontrol.com/news/politics/sp-bsp-leaders-will-tear-each-others-clothes-after-poll-results-pm-modi-3915451.html',
        //             'imgLink': 'https://www.static-news.moneycontrol.com/static-mcnews/2019/04/Narendra-Modi-1-378x213.jpg',
        //             'date': '12 march 2019',
        //             'title': '  SP, BSP leaders will tear each others clothes after poll results: PM Modi',
        //         },
        //         {
        //             'url': 'https://www.moneycontrol.com/news/politics/timing-of-mha-notice-to-rahul-gandhi-not-relevant-its-normal-process-rajnath-singh-3915281.html',
        //             'imgLink': 'https://www.static-news.moneycontrol.com/static-mcnews/2018/12/Rajnath-Singh-4-378x213.jpg',
        //             'date': '12 march 2019',
        //             'title': ' Timing of MHA notice to Rahul Gandhi not relevant, it\'s normal process: Rajnath Singh',
        //         },
        //         {
        //             'url': 'https://www.sciencemag.org/news/2019/04/hitchhiking-bacteria-might-help-their-host-navigate-magnetic-fields',
        //             'imgLink': 'https://www.static-news.moneycontrol.com/static-mcnews/2018/10/Bharatiya-Janata-Party-BJP-supporters-at-a-public-address-rally-378x213.jpg',
        //             'date': '12 march 2019',
        //             'title': '  UP BJP leader heard threatening cop with "hit list" in viral video',
        //         },
        //     ],
        //     "ThumpsUp": " 234",
        //     "ThumpsDown": '16'
        // },

        {
            "SNO": "5",
            "UserId": "3",
            "Interest": "Science",
            "PageLink": [
                {
                    'url': 'https://www.sciencemag.org/news/2019/04/hitchhiking-bacteria-might-help-their-host-navigate-magnetic-fields',
                    'imgLink': 'https://www.sciencemag.org/sites/default/files/styles/article_main_teaser/public/protists042919withbutton.jpg?itok=peMVWKXc',

                    'date': '12 march 2019',
                    'title': 'Hitchhiking bacteria might help their host navigate via magnetic fields',
                },
                {
                    'url': 'https://www.sciencemag.org/news/2019/04/house-panel-proposes-2-billion-increase-nih',
                    'imgLink': 'https://www.sciencemag.org/sites/default/files/styles/article_main_teaser/public/Capitol_CREDIT_iStock.comWLDavies%20.jpg?itok=QZZa6Xif',

                    'date': '12 march 2019',
                    'title': ' House panel proposes $2 billion increase for NIH',
                },
                {
                    'url': 'https://www.sciencemag.org/news/2019/04/medical-dna-sequencing-leads-lawsuits-and-legal-questions',
                    'imgLink': 'https://www.sciencemag.org/sites/default/files/styles/article_main_teaser/public/seq_16x9.jpg?itok=xzi7Uwqx',

                    'date': '12 march 2019',
                    'title': ' Medical DNA sequencing leads to lawsuits and legal questions',
                },
                {
                    'url': 'https://www.sciencemag.org/news/2019/04/nih-bars-physicians-who-criticized-sepsis-trial-speaking-investigators-wall-street',
                    'imgLink': 'https://www.sciencemag.org/sites/default/files/styles/article_main_teaser/public/NIH_building_1_CREDIT_NIH.jpg?itok=3pNhlmzh',

                    'date': '12 march 2019',
                    'title': '  Report: NIH bars physicians who criticized sepsis trial from speaking with investigators',
                },
                {
                    'url': 'https://www.sciencemag.org/news/2019/04/sudanese-geneticist-released-prison-after-revolution-i-m-very-optimistic',
                    'imgLink': 'https://www.sciencemag.org/sites/default/files/styles/article_main_teaser/public/sudan_16x9.jpg?itok=OqWlr5HN',

                    'date': '12 march 2019',
                    'title': ' Sudanese geneticist released from prison after revolution: ‘I’m very optimistic’',
                },
            ],
            "ThumpsUp": " 234",
            "ThumpsDown": '16'
        },

        {
            "SNO": "10",
            "UserId": "3",
            "Interest": "Literature",
            "PageLink": [
                {
                    'url': 'https://www.nytimes.com/2019/04/29/books/amelie-wen-zhao-blood-heir.html',
                    'imgLink': 'https://static01.nyt.com/images/2019/04/29/books/review/29bloodheir/29bloodheir-mediumThreeByTwo210.jpg',

                    'date': '12 march 2019',
                    'title': 'She Pulled Her Debut Book When Critics Found It Racist. Now She Plans to Publish.',
                },
                {
                    'url': 'https://www.nytimes.com/2019/04/29/books/review-spring-ali-smith.html',
                    'imgLink': 'https://static01.nyt.com/images/2019/04/30/books/30bookalismith1/29bookalismith1-mediumThreeByTwo210.jpg',

                    'date': '12 march 2019',
                    'title': ' In ‘Spring,’ Ali Smith’s Series Takes Its Most Political Turn',
                },
                {
                    'url': 'https://www.nytimes.com/2019/04/29/books/review/the-octopus-museum-poems-brenda-shaughnessy.html',
                    'imgLink': 'https://static01.nyt.com/images/2019/04/05/books/review/gabbert02/gabbert02-mediumThreeByTwo210.jpg',

                    'date': '12 march 2019',
                    'title': ' In ‘The Octopus Museum,’ Brenda Shaughnessy Sees a Cephalopod Future',
                },
                {
                    'url': 'https://www.nytimes.com/2019/04/29/theater/laura-linney-broadway-lucy-barton.html',
                    'imgLink': 'https://static01.nyt.com/images/2019/04/30/arts/30linney1/merlin_139393998_2575a9ee-ea62-413f-944c-00aa148c66ff-mediumThreeByTwo210.jpg',

                    'date': '12 march 2019',
                    'title': '  Laura Linney to Return to Broadway in ‘My Name Is Lucy Barton’',
                },
                {
                    'url': 'https://www.nytimes.com/2019/04/30/books/review/everything-in-its-place-oliver-sacks.html',
                    'imgLink': 'https://static01.nyt.com/images/2019/05/03/books/review/03menaker/03menaker-mediumThreeByTwo210.jpg',

                    'date': '12 march 2019',
                    'title': 'Oliver Sacks’s Final, Posthumous Work',
                },
            ],
            "ThumpsUp": " 234",
            "ThumpsDown": '16'
        },


        {
            "SNO": "15",
            "UserId": "3",
            "Interest": "Biology",
            "PageLink": [
                {
                    'url': 'http://www.sci-news.com/biology/male-female-bees-floral-preference-07134.html',
                    'imgLink': 'http://cdn.sci-news.com/images/2019/04/image_7134-Agapostemon-virescens-195x110.jpg',

                    'date': '12 march 2019',
                    'title': 'Study: Male and Female Bees Frequent Different Flowers',
                },
                {
                    'url': 'http://www.sci-news.com/biology/marine-viruses-07133.html',
                    'imgLink': 'http://cdn.sci-news.com/images/2019/04/image_7133-Marine-Viruses-195x110.jpg',

                    'date': '12 march 2019',
                    'title': ' Biologists Find Nearly 196,000 Virus Species in Earth’s Oceans',
                },
                {
                    'url': 'http://www.sci-news.com/biology/odor-receptors-tongue-07124.html',
                    'imgLink': 'http://cdn.sci-news.com/images/2019/04/image_7124f-Odor-Receptors-Tongue-195x110.jpg',

                    'date': '12 march 2019',
                    'title': ' Researchers Find Odor Receptors on Human Tongue',
                },
                {
                    'url': 'http://www.sci-news.com/biology/tube-dwelling-anemone-biggest-mitochondrial-genome-07141.html',
                    'imgLink': 'http://cdn.sci-news.com/images/2019/04/image_7141f-Isarachnanthus-nocturnus-195x110.jpg',

                    'date': '12 march 2019',
                    'title': ' Tube-Dwelling Anemone Has Biggest Mitochondrial Genome Ever Sequenced',
                },
                {
                    'url': 'http://www.sci-news.com/biology/wakatobi-wangi-wangi-white-eyes-07125.html',
                    'imgLink': 'http://cdn.sci-news.com/images/2019/04/image_7125f-White-Eyes-195x110.jpg',

                    'date': '12 march 2019',
                    'title': '  Two New Species of White-Eyes Discovered in Indonesia',
                },
            ],
    
            "ThumpsUp": " 234",
            "ThumpsDown": '16'
        },

        {
            "SNO": "20",
            "UserId": "3",
            "Interest": "Music",
            "PageLink": [
                {
                    'url': 'https://www.npr.org/2019/04/26/717346125/watch-taylor-swifts-video-for-me-featuring-brendon-urie',
                    'imgLink':                 'https://media.npr.org/assets/img/2019/04/25/gettyimages-1145079869_wide-24daffb1bd40367000181922100ce2dbfab6db3d-s300-c15.jpg',

                    'date': '12 march 2019',
                    'title': 'Watch Taylor Swift\'s Video For \'ME!\' Featuring Brendon Urie ',
                },
                {
                    'url': 'https://www.npr.org/2019/04/26/717428612/schoolboy-qs-crash-talk-album-is-woozy-world-weary-trip',
                    'imgLink':                 'https://media.npr.org/assets/img/2019/04/26/gettyimages-1136941401_wide-e0747156f858f24e9653b3872ec1c51cfcaffe36-s300-c15.jpg',

                    'date': '12 march 2019',
                    'title': 'ScHoolboy Q\'s \'CrasH Talk\' Album Is A Woozy, World-Weary Trip',
                },
                {
                    'url': 'https://www.npr.org/2019/04/26/717584228/cage-the-elephant-processes-grief-with-social-cues',
                    'imgLink':                 'https://media.npr.org/assets/img/2019/04/26/cge_neilkrug_wide-fd1728fa0f2a7542fb4b8042c3684d2ff4a258d6-s300-c15.jpg',

                    'date': '12 march 2019',
                    'title': 'Cage The Elephant Processes Grief With \'Social Cues\'',
                },
                {
                    'url': 'https://www.npr.org/2019/04/29/714448593/from-betty-boop-to-popeye-franz-von-supp-survives-in-cartoons',
                    'imgLink':                 'https://media.npr.org/assets/img/2019/04/17/betty-boop-opening-title_wide-7f6ab631979cfbe9e33bb26c5b41f3236eb14fd2-s300-c15.jpg',

                    'date': '12 march 2019',
                    'title': 'From Betty Boop To Popeye, Franz Von Suppé Survives In Cartoons',
                },
                {
                    'url': 'https://www.npr.org/2019/04/29/718321891/woodstock-50-canceled-by-its-investors',
                    'imgLink':                 'https://media.npr.org/assets/img/2019/04/29/gettyimages-1145572057_wide-8c1e6f96f189fd40f14f50dcac42fe3996fd97e3-s300-c15.jpg',

                    'date': '12 march 2019',
                    'title': 'Woodstock 50 Canceled By Its Investors',
                },
            ],
            "ThumpsUp": " 234",
            "ThumpsDown": '16'
        },
    ]
    setTimeout(() => {
        $(".owl").owlCarousel({
        
            // autoPlay: 3000, //Set AutoPlay to 3 seconds
      
            items: 3,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [979, 2]
      
          });
        
    }, 1000);
});