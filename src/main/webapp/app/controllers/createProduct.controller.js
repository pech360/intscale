app.filter('pagination', function () {
    return function (input, start) {
        start = +start;
        if (input instanceof Array)
            return input.slice(start);
    };
});
app.controller('ProductManagementController',function($scope,$http,$uibModal,toaster){
   $scope.curPage = 0;
   $scope.pageSize = 10;
   $scope.TotalRecordCount=0;
   $scope.goToPage = 1;
   $scope.selectedTests = [];
   $scope.parentList={};
   $scope.subParentList={};
   $scope.childList={};
   $scope.subChildList={};
   $scope.product={};
   $scope.updateMode=false;

   $scope.setCurrentPage = function() {
    $scope.curPage = 0;
   };

  $scope.numberOfPages = function() {
      $scope.pages = [];
      $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
      for (var i = 1; i <= $scope.totalPages; i++) {
          $scope.pages.push(i);
      }
      return $scope.totalPages;
  };

    $scope.newRecord=function(){
         $scope.selectedTests = [];
         $scope.parentList={};
         $scope.subParentList={};
         $scope.childList={};
         $scope.subChildList={};
         $scope.product={};
         $scope.parentListTest();
         $scope.updateMode=false;
    };

   $scope.listOfProduct=function(){
    $http.get('/api/product/').then(
    function(response){
     $scope.productList=response.data;
     $scope.TotalRecordCount=response.data.length;
    },
    function(response)
    {
    error(response.data.error);
    });
   };
   $scope.listOfProduct();

  $scope.treeListtest=function(){
     $http.get('/api/test/treeList').then(
      function(response){
        $scope.list=response.data;
        $scope.openTreeModal();
      },
      function(response){
       error(response.data.error);
      });
   };

   $scope.openTreeModal = function () {
               $scope.ModalInstance = $uibModal.open({
               templateUrl: 'testTree.html',
               size: 'lg',
               scope: $scope,
               backdrop: 'static'
           });
         };

  $scope.close = function () { $scope.ModalInstance.close(); };

 $scope.parentListTest=function(){
  $http.get('/api/test/parentByTenantAndConstruct').then(
  function(response){
  $scope.parentList=response.data;
   var x=0;
   $scope.parentList.forEach(function(element){
          if(element.construct){
           $scope.parentList[x].testName = element.name+' Construct>>>';
          }else{
           $scope.parentList[x].testName = element.name;
          }
          x=x+1;
         });
   if(response.data.length==0){
        toaster.pop('Info','Test list is empty');
        return;
     }
  },
  function(response){
   error(response.data.error);
  });
 };

 $scope.parentListTest();

 $scope.subParentListTest=function(id){
   $http.get('/api/test/testByParent?id='+JSON.parse(id).id).then(
   function(response){
    $scope.subParentList=response.data;
       var x=0;
       $scope.subParentList.forEach(function(element){
              if(element.construct){
               $scope.subParentList[x].testName = element.name+' Construct>>>';
              }else{
               $scope.subParentList[x].testName = element.name;
              }
              x=x+1;
             });
    if(response.data.length==0){
        toaster.pop('Info','SubTest is not available for selected test');
        return;
     }
   },
   function(response){
    error(response.data.error);
   });
  };

  $scope.childListTest=function(id){
     $http.get('/api/test/testByParent?id='+JSON.parse(id).id).then(
     function(response){
      $scope.childList=response.data;
             var x=0;
             $scope.childList.forEach(function(element){
                    if(element.construct){
                     $scope.childList[x].testName = element.name+' Construct>>>';
                    }else{
                     $scope.childList[x].testName = element.name;
                    }
                    x=x+1;
                   });
      if(response.data.length==0){
          toaster.pop('Info','SubTest is not available for selected test');
          return;
       }
     },
     function(response){
      error(response.data.error);
     });
    };

  $scope.subChildListTest=function(id){
   $http.get('/api/test/testByParent?id='+JSON.parse(id).id).then(
   function(response){
    $scope.subChildList=response.data;
    if(response.data.length==0){
        toaster.pop('Info','SubTest is not available for selected test');
        return;
     }
   },
   function(response){
    error(response.data.error);
   });
  };

 $scope.addTest = function (test) {
    if(!test){return;}
       var alreadyThere = false;
       test=JSON.parse(test);
       for (var i = 0; i < $scope.selectedTests.length; i++) {
             if ($scope.selectedTests[i].name == test.name) {
                 alreadyThere = true;
             }
         }
         if (!alreadyThere) {
             $scope.selectedTests.push(test);
          }
      };

 $scope.removeTest = function (index) {
    $scope.selectedTests.splice(index,1);
 };

 $scope.saveProduct=function(product){
  $scope.test=[];
  $scope.map={};
  if(!product.name){
    toaster.pop('Info',"Product name can't be empty");
    return;
  }
  if(!product.description){
    toaster.pop('Info',"Product description can't be empty");
    return;
  }
  if($scope.selectedTests.length==0){
    toaster.pop('Info',"Test list can't be empty");
    return;
  }
  for(var i=0;i<$scope.selectedTests.length; i++){
    $scope.key=$scope.selectedTests[i].id;
    $scope.value=(i+1);
    $scope.map[$scope.key]=$scope.value;
    $scope.test.push($scope.map);
  }
  $scope.testIds=[];
  $scope.selectedTests.forEach(function(e){
    $scope.testIds.push(e.id);
  });
  $scope.product.test=$scope.testIds;
  if(product.id){
    $http.put('/api/product/',product).then(
         function(response){
          ok("Product updated successfully");
          $scope.selectedTests=[];
          $scope.product={};
          $scope.listOfProduct();
         },
         function(response)
         {
         error(response.data.error);
         });
  }else{
   $http.post('/api/product/',product).then(
     function(response){
      ok("Product created successfully");
      $scope.selectedTests=[];
      $scope.product={};
      $scope.listOfProduct();
     },
     function(response)
     {
     error(response.data.error);
     });
  }
 };

 $scope.updateProduct=function(product){
  $scope.selectedTests=[];
  $scope.updateMode=true;
  $http.get('/api/test/testByIds?ids='+product.testId).then(
  function(response){
   for(var i=0;i<response.data.length;i++){
      $scope.selectedTests.push(response.data[i]);
   }
   $scope.product=product;
  },function(response){
    error(response.data.error);
  }
  );
 };

 $scope.deleteProduct=function(id){
      swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.value) {
            $http.delete('/api/product/'+id).then(function(response){
              ok('Product deleted successfully');
              $scope.listOfProduct();
            },
             function (response) {
                 error('Can not delete product,mapped with schoolAdmin');
             });
             }
        });
    };

 function ok(message) {
              swal({
                  title: message,
                  type: 'success',
                  buttonsStyling: false,
                  confirmButtonClass: "btn btn-warning"
              });
          };
 function error(message) {
          swal({
              title: message,
              type: 'error',
              buttonsStyling: false,
              confirmButtonClass: "btn btn-warning"
          });
      };

});
