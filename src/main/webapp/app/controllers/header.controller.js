app.controller('headerController', function ($scope, $state, $rootScope, $http, Auth, $localStorage, toaster, $translate) {

    $rootScope.showTestNavBar = false;
    $rootScope.showDefaultNavBar = true;
    $scope.assets_url = $localStorage.assets_url;
    $scope.internalLogo = window.location.hostname.split(".")[0] + 'intlogo.png';
    $http.get('/api/user/me').then(function (response) {
        $rootScope.user = response.data;

    });

    $scope.sponsorLogoImageUrls = [];

    $scope.assets_url = $localStorage.assets_url;

    $scope.logout = function () {
        Auth.logout(function () {
            $rootScope.user = null;
            // window.location = "/";
            $state.go('login');
            window.location.reload();
        });
    };

    function fetchProfile() {
        $http.get('/api/user/profile').then(function (response) {
            $scope.user = response.data;
            if ($scope.user.img) {
                $scope.userImageURI = $scope.assets_url + $scope.user.img;
            } else {
                if ($scope.user.gender) {
                    if ($scope.user.gender.toLowerCase() == "male") {
                        $scope.userImageURI = "/assets/img/b-avtar.png"
                    } else {
                        if ($scope.user.gender.toLowerCase() == "female") {
                            $scope.userImageURI = "/assets/img/g-avtar.png"
                        }
                    }
                } else {
                    $scope.userImageURI = "/assets/img/default-profile-picture.png";
                }
            }

            if ($scope.user.sponsors) {
                for (var i in $scope.user.sponsors) {
                    if ($scope.user.sponsors[i] && $scope.user.sponsors[i].logo) {
                        $scope.sponsorLogoImageUrls.push($scope.assets_url + $scope.user.sponsors[i].logo);
                        console.log("wefewf");
                    }
                }

            }
            console.log("wefewf");

            // $scope.switchLanguage($scope.user.language);
        }, function (response) {
            toaster.pop('error', "Error", "Failed to load profile - " + response.data.error);

            if (response.data.error) {
                if (response.data.error == "You are logged in another session, so close this session") {
                    $scope.logout();
                }
            }
        });
    }

    $scope.switchLanguage = function (language) {
        var key = "";
        switch (language) {
            case "English":
                key = "en";
                break;
            case "Hindi":
                key = "hi";
                break;
            case "Marathi":
                key = "mr";
                break;

            default:
                key = "en";
                break;
        }
        $translate.use(key);
    };


    fetchProfile();

});


