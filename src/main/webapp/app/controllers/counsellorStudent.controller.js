app.filter('pagination', function()
{
return function(input, start)
{
 start = +start;
 if(input instanceof Array)
 return input.slice(start);
};
});
app.controller('counsellorStudentController',function($scope,$http,CommonService,toaster,$uibModal){
  $scope.curPage = 0;
  $scope.pageSize = 10;
  $scope.goToPage = 1;
  $scope.TotalRecordCount=0;
  $scope.studentArray=[];
  $scope.counsellorStudent={};
  var countStudent=0;
  $scope.schoolId='';
  $scope.gradeId='';
  $scope.counsellorId='';
  $scope.studentId='';
  $scope.searchText='';

  $scope.numberOfPages = function() {
       $scope.pages = [];
       $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
       for (var i = 1; i <= $scope.totalPages; i++) {
           $scope.pages.push(i);
       }
       return $scope.totalPages;
   };

 $http.get('/api/school/all').then(function(response){
          $scope.listSchooles=response.data;
         });

 $http.get('/api/md/grades/').then(function(response){
      $scope.listGrades=response.data;
     });

 $http.get('/api/user/self/userByCounsellor').then(function(response){
       $scope.listOfCounsellor=response.data;
      });
 $scope.openLoader=function () {
     $scope.modalInstance = $uibModal.open({
         templateUrl: 'views/loader.html',
         size: 'sm',
         scope: $scope,
         backdrop: 'static'
     });
 }

 $scope.list=function(){
        $scope.openLoader();
        $http.get('/api/user/self/listAllStudents').then(function(response){
        $scope.listOfStudents=response.data;
        $scope.TotalRecordCount=response.data.length;
        $scope.modalInstance.close();
        var x=0;
        $scope.listOfStudents.forEach(function(element){
           $scope.listOfStudents[x].selectStudent = false;
           x=x+1;
          });
       },function(response){
          toaster.pop('Info',response.error);
       });
  };

  // $scope.list();

  $scope.filterSearch=function(){
    $scope.openLoader();
    $http.get('/api/user/self/filteredUserList?schoolId='+$scope.schoolId+'&gradeId='+$scope.gradeId+'&counsellorId='+$scope.counsellorId).then(
    function(response){
        $scope.modalInstance.close();
      if(response.data.length==0){
         $scope.listOfStudents={};
         toaster.pop('Info','Student list is empty');
         return;
      }
      $scope.listOfStudents=response.data;
      $scope.TotalRecordCount=response.data.length;
      var x=0;
      $scope.listOfStudents.forEach(function(element){
      $scope.listOfStudents[x].selectStudent = false;
      x=x+1;
      });
    },
    function(response){
      toaster.pop('Info',response.error);
    });
  };

    $scope.filterUsersByFullName = function (fullName) {

        if (fullName) {
            $scope.openLoader();
            $http.get('/api/user/self/filter/name?fullName=' + fullName).then(
                function(response){
                    $scope.modalInstance.close();
                    if(response.data.length==0){
                        $scope.listOfStudents={};
                        toaster.pop('Info','Student list is empty');
                        return;
                    }
                    $scope.listOfStudents=response.data;
                    $scope.TotalRecordCount=response.data.length;
                    var x=0;
                    $scope.listOfStudents.forEach(function(element){
                        $scope.listOfStudents[x].selectStudent = false;
                        x=x+1;
                    });
                },
                function(response){
                    toaster.pop('Info',response.error);
                });

        } else {
            error("Please enter fullName for which you want see the report");
        }

    };

  $scope.isSelectAllStudent = function () {
      if ($scope.selectAllStudent) {
          $scope.studentArray=[];
          for (countStudent=0; countStudent<$scope.TotalRecordCount; countStudent++) {
          $scope.listOfStudents[countStudent].selectStudent = true;
          $scope.studentArray.push($scope.listOfStudents[countStudent].id);
         }
      }else{
          for (countStudent=0; countStudent<$scope.TotalRecordCount; countStudent++) {
          $scope.listOfStudents[countStudent].selectStudent = false;
         }
         $scope.studentArray=[];
      }
  };

  $scope.isStudentSelected = function (student) {
      if (student.selectStudent){
       $scope.studentArray.push(student.id);
      }else{
      var i=0;
      $scope.studentArray.forEach(function(element){
      if(element==student.id){
      $scope.studentArray.splice(i, 1);
      }
      i++;
      });
      i=0;
      }
  };

  $scope.saveAll=function(){
    if($scope.studentArray.length<1){
      toaster.pop('Info',"Student is not selected");
       return;
    }
    if(!$scope.counsellor){
      toaster.pop('Info',"Counsellor is not selected");
       return;
    }
    $scope.counsellorStudent.studentIds=$scope.studentArray;
    $scope.counsellorStudent.counsellor=$scope.counsellor;
    $http.put('api/student/assignCounsellor',$scope.counsellorStudent).then(function(response){
         if(response.data){
           ok("Counsellor Assign successfully");
           $scope.selectAllStudent=false;
           $scope.studentArray=[];
           //$scope.filterSearch();
           $scope.counsellorStudent={};
         }else{
           error(response.data.error);
         }
      }, function (response) {
          error(response.data.error);
      });
  };

  $scope.removeAll=function(){
   if($scope.studentArray.length<1){
     toaster.pop('Info',"Student is not selected");
      return;
   }
   $scope.counsellorStudent.studentIds=$scope.studentArray;
   $http.put('/api/student/removeCounsellors',$scope.counsellorStudent).then(
   function(response){
     if(response.data){
          ok("Counsellor Remove successfully");
          $scope.selectAllStudent=false;
          $scope.studentArray=[];
          $scope.filterSearch();
          $scope.counsellorStudent={};
     }else{error(response.data.error);}
   },
   function(response){
     error(response.data.error);
   }
   );
  };

  function ok(message) {
          swal({
              title: message,
              type: 'success',
              buttonsStyling: false,
              confirmButtonClass: "btn btn-warning"
          });
      };
  function error(message) {
      swal({
          title: message,
          type: 'error',
          buttonsStyling: false,
          confirmButtonClass: "btn btn-warning"
      });
  };
});