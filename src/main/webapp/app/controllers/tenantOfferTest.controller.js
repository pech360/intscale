app.filter('pagination', function()
{
    return function(input, start)
    {
        start = +start;
        if(input instanceof Array)
            return input.slice(start);
    };
});
app.controller('offeredTestController',function($scope,$http,CommonService,toaster,$uibModal){
    $scope.curPage = 0;
    $scope.pageSize = 10;
    $scope.goToPage = 1;
    $scope.TotalRecordCount=0;
    $scope.productAllotment={};

    $scope.numberOfPages = function() {
        $scope.pages = [];
        $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
        for (var i = 1; i <= $scope.totalPages; i++) {
            $scope.pages.push(i);
        }
        return $scope.totalPages;
    };
    function getList() {
        $http.get('/api/test/offerTest').then(function (response) {
            $scope.listOfferTest=response.data;
            $scope.TotalRecordCount=response.data.length;
        });
    };
    getList();

    $scope.publishTest= function (id,publish) {
        $http.put('/api/test/offerTestPublish?id='+id+'&published='+publish).then(function (response) {
                getList();
                ok('Record updated successfully');
            },
            function (response) {
                error(response.data.error);
            }
        );
    };

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    };
    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    };

});


