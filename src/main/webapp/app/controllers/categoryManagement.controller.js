app.controller('categoryManagement', function ($scope, $http, toaster) {
    refresh();
    $scope.customSettings = {
        control: 'brightness',
        theme: 'bootstrap',
        position: 'bottom right',
        defaultValue: '',
        animationSpeed: 50,
        animationEasing: 'swing',
        change: null,
        changeDelay: 0,
        hide: null,
        hideSpeed: 100,
        inline: false,
        letterCase: 'lowercase',
        opacity: false,
        show: null,
        showSpeed: 100
    };

    $scope.categoryTypes = ["primary","secondary"];

    $scope.myFunction = function () {
        $scope.workingCategory = {
            mode: 'New',
            color: '',
            type: 'primary'
        };
    };

    $scope.updateCategory = function (cat) {
        $scope.workingCategory = angular.copy(cat);
    };

    $scope.saveCategory = function (category) {
        if (!$scope.workingCategory.name) {
            toaster.pop('Error', "Category Name cannot be empty ");
            return;

        }
        if (!$scope.workingCategory.description) {
            toaster.pop('Error', "Description cannot be empty ");
            return;

        }
        if (!$scope.workingCategory.color) {
            toaster.pop('Error', "Color cannot be empty ");
            return;

        }
        if (category.id) {
            $http.put('/api/category', category).then(function (response) {
                ok("Category updated successfully");
                refresh();
            }, function (response) {
                error(response.data.error);
            });
        } else {
            $http.post('/api/category', category).then(function (response) {
                ok("Category created successfully");
                refresh();
            }, function (response) {
                error(response.data.error);
            });
        }
    };

    $scope.check = function () {
        var flag = true;
        for (i = 0; i < $scope.category.length; i++) {
            flag = false;
            break;
        }
        $scope.all = flag;
    };

    $scope.remove = function () {
        var toDelete = [];
        for (i = 0; i < $scope.category.length; i++) {
            if ($scope.category[i].checked) {
                toDelete.push($scope.category[i].id);
            }
        }

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Delete!'
        }).then(function (result) {
            if (result.value) {
                deleteCategories(toDelete);
            }
        });
    };

    function deleteCategories(list) {
        $http.post('/api/category/delete/all', list).then(
            function (response) {
                refresh();
            }, function (response) {
                error(response.data.error)
            }
        );
    }

    $scope.selectAllCategory = function (all) {
        angular.forEach($scope.category, function (cat) {
            cat.checked = all;
        });
    };


    function refresh() {
        $scope.workingCategory = {
            mode: 'New',
            color: '',
            type: 'primary'
        };
        $http.get('/api/category').then(function (response) {
            $scope.category = response.data;
            angular.forEach($scope.category, function (cat) {
                cat.checked = false;
            });
        });
        $http.get('/api/category/parent').then(function (response) {
            $scope.parentCategory = response.data;

        });
    }

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }
});