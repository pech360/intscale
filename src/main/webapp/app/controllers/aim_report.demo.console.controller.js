app.controller('AimReportDemoConsoleController', function ($scope, $http, CommonService, toaster, $uibModal) {

    $scope.users = [];
    $scope.openLoader = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    };


    $scope.filterUsersByFullName = function (fullName) {

        if (fullName) {
            $scope.openLoader();
            $http.get('/api/user/self/filter/name?fullName=' + fullName).then(
                function (response) {
                    $scope.modalInstance.close();
                    if (response.data.length == 0) {
                        $scope.users = [];
                        toaster.pop('Info', 'Users not found');
                        return;
                    }
                    $scope.users = response.data;

                },
                function (response) {
                    toaster.pop('Info', response.error);
                });

        } else {
            error("Please enter fullName for which you want see the report");
        }

    };

    $scope.downloadDemoReport = function (id) {
        $scope.openLoader();
        $http.get('api/reports/download/aimReport/demo/' + id , {responseType: 'arraybuffer'}).then(function (response) {
            $scope.modalInstance.close();
            var blob = new Blob([response.data], {
                type: 'application/octet-stream'
            });
            saveAs(blob, response.headers("fileName"));
        }, function (response) {
            $scope.modalInstance.close();
            error("download failed");
        })
    };



    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }
});