app.controller('ProfileController', function ($scope, $http, CommonService, $state, toaster, $localStorage) {
    $scope.userImageURI = '';
    $scope.assets_url = $localStorage.assets_url;

    $scope.orgName = window.location.hostname.split(".")[0];
    $scope.selectedInterest = [];


    $scope.userMasterData = function () {

        CommonService.listDesignations().then(function (response) {
            $scope.designations = response.data;
        });
        CommonService.listIndustries().then(function (response) {
            $scope.industries = response.data;
        });

        CommonService.listLanguages().then(function (response) {
            $scope.languages = response.data;
        });

        CommonService.listGender().then(function (response) {
            $scope.genders = response.data;
        });

        CommonService.listUserTypes().then(function (response) {
            $scope.userTypes = response.data;
        });

        // CommonService.listInterests().then(function (response) {
        //     $scope.interests = response.data;
        //     // alert(JSON.stringify($scope.interests));
        // });

        $http.get('/api/md/allChildInterests').then(function (response) {
            $scope.interests = [];
            angular.forEach(response.data, function (i) {
                $scope.interests.push(i.name)
            });
        });

        $http.get('/api/md/grades/').then(function (response) {
            $scope.grades = response.data;
        });

        $scope.streams = ["PCM", "PCB","Commerce with Mathematics","Commerce without Mathematics", "Humanities"];
    };

    $scope.browseImage = function () {
        $('#userimg').trigger('click');
    };

    $scope.setFiles = function (element) {
        $scope.imgfile = element.files[0];
        displayImage($scope.imgfile);
    };

    function displayImage(file) {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            $scope.$apply(function () {
                $scope.userImageURI = reader.result;
            });
        };
        reader.onerror = function (error) {
            error("Failed to load image:" + error);
        };
    }

    $scope.userMasterData();

    $scope.upload = function () {
        // var f = document.getElementById('file').files[0];
        var r = new FileReader();
        var fd = new FormData();
        fd.append('file', $scope.imgfile);
        var request = {
            method: 'POST',
            url: '/api/raw/PROFILEIMAGE/' + $scope.user.id,
            data: fd,
            headers: {
                'Content-Type': undefined
            }
        };
        $http(request).then(function (response) {
            toaster.pop('success', "Image uploaded successfully");
            // fetchProfile();
        });
    };

    function fetchProfile() {
        $http.get('/api/user/profile').then(function (response) {
            $scope.user = response.data;
            $scope.user.dob = new Date(response.data.dob);
            if ($scope.user.gender) {
                $scope.user.gender = $scope.user.gender.toLowerCase();
            }
            $scope.selectedInterest = response.data.interest;
            if ($scope.user.img) {
                $scope.userImageURI = $scope.assets_url + $scope.user.img;
            } else {
                if ($scope.user.gender) {
                    if ($scope.user.gender.toLowerCase() == "male") {
                        $scope.userImageURI = "/assets/img/b-avtar.png"
                    } else {
                        if ($scope.user.gender.toLowerCase() == "female") {
                            $scope.userImageURI = "/assets/img/g-avtar.png"
                        }
                    }
                } else {
                    $scope.userImageURI = "/assets/img/default-profile-picture.png";
                }
            }
        }, function (response) {
            toaster.pop('error', "Error", "Failed to load profile - " + response.data.error);

            if(response.data.error){
                if(response.data.error=="You are logged in another session, so close this session"){

                }
            }
        });
    }

    function arrayBufferToBase64(buffer) {
        var binary = '';
        var bytes = new Uint8Array(buffer);
        var len = bytes.byteLength;
        for (var i = 0; i < len; i++) {
            binary += String.fromCharCode(bytes[i]);
        }
        return window.btoa(binary);
    }

    function base64ToArrayBuffer(base64) {
        var binary_string = window.atob(base64);
        var len = binary_string.length;
        var bytes = new Uint8Array(len);
        for (var i = 0; i < len; i++) {
            bytes[i] = binary_string.charCodeAt(i);
        }
        return bytes.buffer;
    }


    function createStringByArray(array) {
        var output = '';
        angular.forEach(array, function (object) {
            angular.forEach(object, function (value, key) {
                output += key + ',';
                output += value + ',';
            });
        });
        return output;
    }

    $scope.updateUserInfo = function () {

        $scope.user.interest = $scope.selectedInterest;
        var isAllowed = validateInput($scope.user, "user");
        if (isAllowed) {
            $http.put('/api/user/self', $scope.user).then(function (response) {
                toaster.pop('success', "user updated successfully");
                fetchProfile();
                $state.go("app.availabletests");
            }, function (response) {
                toaster.pop('error', "Error", response.data.error);
            });
        }
    };

    $scope.addInterest = function (selectedInt) {

        var alreadyThere = false;
        if ($scope.selectedInterest == null) {
            $scope.selectedInterest = [];
        }
        for (var i = 0; i < $scope.selectedInterest.length; i++) {
            if ($scope.selectedInterest[i] == selectedInt.value) {
                alreadyThere = true;
            }
        }
        if (!alreadyThere) {
            if ($scope.selectedInterest.length > 4) {
                toaster.pop("info", "you cannot choose more than 5 interests");
            } else {
                $scope.selectedInterest.push(selectedInt);
            }
            alreadyThere = false;
        }
    };
    $scope.removeInterest = function (index) {
        $scope.selectedInterest.splice(index, 1)
    };

    $scope.resetPassword = function () {
        swal({
            title: "New Password",
            input: "password",
            showCancelButton: true,
            closeOnConfirm: false,
            buttonsStyling: true,
            inputPlaceholder: "New Password"
        }).then(function (inputValue) {
            if (inputValue.value) {
                var password = inputValue.value;

                if (password.length < 7) {
                    toaster.pop("info", 'Info', "Password should be min 6 characters");
                }else {
                    $http.put('/api/user/self/selfUpdatePassword', password).then(function () {
                        ok("Password changed successfully");
                    }, function (response) {
                        error(response.data.error);
                    });
                }

            } else {
                // if (inputValue === "") {
                //     swal.showInputError("Missing password");
                //     return false;
                // }

            }
        });
    };

    function validateInput(input, type) {
        switch (type) {
            case "user":
                if (!input.username) {
                    toaster.pop('info', "Username missing!");
                    return false;
                }
                if (!input.name) {
                    toaster.pop('info', "Name missing!");
                    return false;
                }
                if (!input.email) {
                    toaster.pop('info', "Email missing!");
                    return false;
                }
                if (!input.gender) {
                    toaster.pop('info', "Gender missing!");
                    return false;
                }
                // if (!input.language) {
                //     toaster.pop('info', "Language missing!");
                //     return false;
                // }
                if (!input.grade) {
                    toaster.pop('info', "Grade missing!");
                    return false;
                }
                if (input.grade == '11' || input.grade == '12') {
                    if (!input.stream) {
                        toaster.pop('info', "Stream missing!");
                        return false;
                    }
                }
                if (!input.school) {
                    toaster.pop('info', "School name missing!");
                    return false;
                }
                // if (!input.interest || input.interest.length < 2) {
                //     toaster.pop('info', "Please select at least 2 interests!");
                //     return false;
                // }
                if (!input.city) {
                    toaster.pop('info', "City missing!");
                    return false;
                }
                return true;
                break;
            case "password":

                break;
        }
    }

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }


    fetchProfile();
});
