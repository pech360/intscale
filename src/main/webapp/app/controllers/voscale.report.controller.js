app.controller('VoscaleReportController', function ($http, $scope, $stateParams, DateFormatService,UserId,TestId) {
    $scope.userId='0';
    if(TestId>0&&UserId>0){
      var testId = TestId;
      $scope.userId=UserId;
      fetchProfileByUserId(UserId);
    }else if(UserId.length==undefined&&TestId.length==undefined&&UserId){
      var testId = $stateParams.id;
      $scope.userId='0';
      fetchProfile();
    }
    fetchTestDetails(testId);
    var obtainedScoreRange ={};
    //alert(UserId+' '+TestId);
    $scope.getReportV1 = function () {
        $http.get('/api/reports/voscaleReport?id='+testId+'&userId='+$scope.userId).then(
            function (response) {
                $scope.categories = response.data;
                $scope.categories.sort(compareBySequence);
                renderChart($scope.categories);
                getStrengthAndWeakness();
            }, function (response) {
                error(response.data.error)
            });
    };

    $scope.getReportV1();

    function fetchProfile() {
        $http.get('/api/user/profile').then(function (response) {
            $scope.user = response.data;
            $scope.user.dob = DateFormatService.formatDate(new Date(response.data.dob));
        });
    }
    function fetchProfileByUserId(id) {
            $http.get('/api/user/self/userProfile?userId='+id).then(function (response) {
                $scope.user = response.data;
                $scope.user.dob = DateFormatService.formatDate(new Date(response.data.dob));
            });
        }
    function fetchTestDetails() {
        $http.get('/api/result/test?testId='+testId+'&userId='+$scope.userId).then(function (response) {
            $scope.testDetails = response.data;
            $scope.testDetails.testTakenDate = DateFormatService.formatDate7(new Date($scope.testDetails.testTakenDate));

            if ($scope.testDetails.infrequency < 95 && $scope.testDetails.acquiescence < 95) {
                $scope.testDetails.validityIndicesText = "may indicate that the individual has not simply agreed with each statement. The individual has endorsed most items in a way that is similar to other people; it is unlikely that they have responded randomly.";
            } else if ($scope.testDetails.infrequency > 95 && $scope.testDetails.acquiescence < 95) {
                $scope.testDetails.validityIndicesText = "may indicate that the individual either (a) experienced consistent indecisiveness\n" +
                    "about the a or c response choices (perhaps because of an ambiguous\n" +
                    "self-picture), or (b) tried to avoid making the wrong impression by choosing\n" +
                    "the ? middle answer rather than one of the more definitive or extreme\n" +
                    "answers.";
            } else if ($scope.testDetails.infrequency < 95 && $scope.testDetails.acquiescence > 95) {
                $scope.testDetails.validityIndicesText = "\"may indicate that the individual either (a) experienced consistent indecisiveness.\n" +
                    "about the \"\"Agree\"\" or \"\"Disagree\"\" choices (perhaps because of an ambiguous self-picture), or (b) tried to avoid making the wrong impression by choosing the ? middle answer rather than one of the more definitive or extreme answers.\"\n";
            } else if ($scope.testDetails.infrequency > 95 && $scope.testDetails.acquiescence > 95) {
                $scope.testDetails.validityIndicesText = "\"may indicate that the individual (a) had trouble reading or comprehending\n" +
                    "the questions or (b) responded randomly\"\n";
            }

        });
    }

    var randomScalingFactor = function () {
        return Math.round(Math.random() * 10);
    };

    $scope.neoGraphs = {};
    $scope.neoGraphs.data =
        [[{
            x: -30,
            y: -40
        }]];


    $scope.neoGraphs.options = {

        responsive: true,
        scales: {
            xAxes: [{
                type: 'linear',
                position: 'bottom',
                ticks: {
                    max: 50,
                    min: -50,
                    stepSize: 10
                }
            }],
            yAxes: [{
                ticks: {
                    max: 50,
                    min: -50,
                    stepSize: 10
                }
            }]
        },
        elements: {
            borderWidth: 5
        },
        title: {
            display: true,
            text: 'NEO'
        }
    };

    function renderChart(data) {
        $scope.report = {};
        $scope.report.labels = [];
        $scope.report.data = [];
        for (var d in data) {
            // $scope.report.labels.push(data[d].displayName);
            $scope.report.labels.push(data[d].name);
            $scope.report.data.push(data[d].percentile);
        }

        $scope.report.options = {
            responsive: true,
            scales: {
                xAxes: [{
                    ticks: {
                        max: 100,
                        min: 0,
                        stepSize: 10,
                        beginAtZero:true
                    }
                    // , gridLines: {
                    //     drawBorder: true,
                    //     color: ['#FF6F00', '#FFA000', '#FFB300', '#FFC107', '#FFCA28', '#FFD54F', '#FFE082', '#FFECB3', '#FFF8E1', '#FFF7E5']
                    // }
                }]
            },

            elements: {
                line: {
                    tension: 0.1,
                    fill: false,
                    borderColor: "rgb(255,165,0)",
                    borderWidth: 5
                }
            }
        };
    }

    function getStrengthAndWeakness() {
        for (var c in $scope.categories) {
            $scope.categories[c].strengths = [];
            $scope.categories[c].weaknesses = [];
            $scope.categories[c].summary = neutralizeGender(getScoreRangeText($scope.categories[c]).text);
            $scope.categories[c].subCategories.sort(compareBySequence);
            for (var i in $scope.categories[c].subCategories) {
                obtainedScoreRange ={};
                obtainedScoreRange = getScoreRangeText($scope.categories[c].subCategories[i]);
                obtainedScoreRange.highText = $scope.categories[c].subCategories[i].highText;
                obtainedScoreRange.lowText = $scope.categories[c].subCategories[i].lowText;
                obtainedScoreRange.shortHighText = $scope.categories[c].subCategories[i].shortHighText;
                obtainedScoreRange.shortLowText = $scope.categories[c].subCategories[i].shortLowText;
                if (obtainedScoreRange.rangeOf == "VeryHigh" || obtainedScoreRange.rangeOf == "High" || obtainedScoreRange.rangeOf == "Average") {
                    if ($scope.categories[c].strengths.length < 3) {
                        obtainedScoreRange.text = neutralizeGender(obtainedScoreRange.text);
                        $scope.categories[c].strengths.push(obtainedScoreRange);
                    }
                } else {
                    if ($scope.categories[c].weaknesses.length < 3) {
                        obtainedScoreRange.text = neutralizeGender(obtainedScoreRange.text);
                        $scope.categories[c].weaknesses.push(obtainedScoreRange);
                    }
                }

            }
            $scope.categories[c].strengths.sort(compareStrengths);
            $scope.categories[c].weaknesses.sort(compareWeaknesses);
        }
    }

    function compareBySequence(a, b) {
        if (a.sequence < b.sequence)
            return -1;
        if (a.sequence > b.sequence)
            return 1;
        return 0;
    }

    function compareStrengths(a, b) {
        if (a.startFrom < b.startFrom)
            return 1;
        if (a.startFrom > b.startFrom)
            return -1;
        return 0;
    }

    function compareWeaknesses(a, b) {
        if (a.startFrom < b.startFrom)
            return -1;
        if (a.startFrom > b.startFrom)
            return 1;
        return 0;
    }

    function getScoreRangeText(category) {
        for (var r in category.scoreRanges) {
            if (category.scoreRanges[r].startFrom <= category.percentile && category.scoreRanges[r].endAt >= category.percentile) {
                return category.scoreRanges[r];
            }
        }
    }

    function neutralizeGender(text) {
        if ($scope.user.gender.toLowerCase() == "female") {
            text = text.replace("He", "She");
            text = text.replace("he", "she");
            text = text.replace("him", "her");
            text = text.replace("His", "Her");
            text = text.replace("his", "her");
            text = text.replace("Himself", "Herself");
            text = text.replace("himself", "herself");
        }
        return text;
    }

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }
    // function getStrengthAndWeakness() {
    //
    //     for (var c in $scope.categories) {
    //         $scope.categories[c].strengths = [];
    //         $scope.categories[c].weaknesses = [];
    //         $scope.categories[c].summary = getScoreRangeText($scope.categories[c]);
    //         $scope.categories[c].subCategories.sort(compareBySequence);
    //         var midValue = Math.ceil($scope.categories[c].subCategories.length / 2);
    //         for (var i in $scope.categories[c].subCategories) {
    //             if (i <= midValue) {
    //                 $scope.categories[c].strengths.push(getScoreRangeText($scope.categories[c]));
    //             } else {
    //                 $scope.categories[c].weaknesses.push(getScoreRangeText($scope.categories[c]));
    //             }
    //         }
    //     }
    // }

    // function loadCategoryByTest(testId, data) {
    //     $http.get('/api/category/loadByTest?testId=' + testId).then(function (response) {
    //         $scope.categories = response.data;
    //         for (var c in $scope.categories) {
    //             for (var d in data) {
    //                 if ($scope.categories[c].name == d) {
    //                     $scope.categories[c].percentile = data[d];
    //                 }
    //             }
    //         }
    //         loadSubCategoryByTest(testId);
    //
    //     }, function (response) {
    //         toaster.pop('error', "Error", response.data.message);
    //     })
    // }

    // function loadSubCategoryByTest(testId) {
    //     $http.get('/api/category/loadSubCategoryByTest?testId=' + testId).then(function (response) {
    //         subCategories = response.data;
    //         mergingSubCategoriesIntoCategories(subCategories);
    //
    //     }, function (response) {
    //         toaster.pop('error', "Error", response.data.message);
    //     });
    // }

    // function mergingSubCategoriesIntoCategories(subcategories) {
    //     for (var c in $scope.categories) {
    //         $scope.categories[c].subcategories = [];
    //         for (var s in subcategories) {
    //             if ($scope.categories[c].id == subcategories[s].parentId) {
    //                 $scope.categories[c].subcategories.push(subcategories[s]);
    //             }
    //         }
    //     }
    // }
});