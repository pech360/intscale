app.controller('RefineMasterDataController', function ($scope, $http, toaster) {
    $scope.selectedType = '';
    $scope.oldName = '';
    $scope.newName = '';
    $scope.refinedRecords = [];
    $scope.searchText = '';

    $scope.components = ["City", "School"];

    $scope.saveRecord = function (selectedType) {

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#00bb7e',
            cancelButtonColor: '#fab400',
            confirmButtonText: 'Yes, Update!'
        }).then(function (result) {
            if (result.value) {

                if (selectedType == 'SchoolName') {
                    $http.put('/api/user/self/updateSchoolName?oldName=' + $scope.oldName + '&newName=' + $scope.newName).then(function (response) {
                        ok(response.data + " users updated");
                        $scope.getMasterData("School");
                        $scope.newName = '';
                    }, function (response) {
                        error(response.data.error);
                    });
                }
                if (selectedType == 'CityName') {
                    $http.put('/api/user/self/updateCity?oldName=' + $scope.oldName + '&newName=' + $scope.newName).then(function (response) {
                        ok(response.data + " users updated");
                        $scope.getMasterData("City");
                        $scope.newName = '';
                    }, function (response) {
                        error(response.data.error);
                    });
                }
            }
        })
    };

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.getMasterData = function (selectedType) {
        $scope.selectedType = selectedType;

        if (selectedType == "City") {
            $http.get('/api/md/cities/').then(function (response) {
                $scope.refinedRecords = [];
                for (var i in response.data) {
                    if (response.data[i].value) {
                        $scope.refinedRecords.push(response.data[i].value);
                    }
                }
            }, function (response) {
                error("failed");
            });
            $scope.getMasterData("CityName");
        }
        if (selectedType == "CityName") {
            $http.get('/api/md/getCitiesFromUserTable').then(function (response) {
                $scope.records = response.data;
            }, function (response) {
                error("failed");
            });
        }
        if (selectedType == "School") {
            $http.get("/api/school/all").then(function (response) {
                $scope.refinedRecords = [];
                for (var i in response.data) {
                    if (response.data[i].name) {
                        $scope.refinedRecords.push(response.data[i].name);
                    }
                }
                $scope.getMasterData("SchoolName");
            }, function (response) {
                error("failed");
            });
        }
        if (selectedType == "SchoolName") {
            $http.get("/api/school/getSchoolNamesFromUserTable").then(function (response) {
                $scope.records = response.data;
            }, function (response) {
                error("failed");
            });
        }
    };

    $scope.selectRecord = function (record, selectedType) {

        if(selectedType=='oldName'){
            $scope.oldName = record;
        }
        if(selectedType=='newName'){
            $scope.searchText = '';
            $scope.newName = record
        }

    }

});
