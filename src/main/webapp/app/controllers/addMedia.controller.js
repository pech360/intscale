app.controller('AddMediaController', function ($scope, $http, $state, $stateParams, toaster, Upload, $uibModal) {

    $scope.testImageURI = {};
    $scope.showTestOnCard = false;
    $scope.showStakeholderImage = false;
    $scope.stakeholderImage = false;
    $scope.stakeholderVideo = false;
    $scope.progress = { hide: true, percent: '0%' };
    $scope.components = ['Test', 'Construct', 'Interest', 'School', 'Career', 'Subject'];

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.getComponent = function (componentName) {
        $scope.componentName = componentName;
        $scope.showTestOnCard = false;
        switch ($scope.componentName) {
            case 'Test':
                getAllTest();
                break;
            case 'Construct':
                getAllConstruct();
                break;
            case 'Interest':
                getAllInterest();
                break;
            case 'School':
                getAllSchool();
                break;
            case 'Career':
                getAimExpertInventoryNamesList("career");
                break;
            case 'Subject':
                getAimExpertInventoryNamesList("subject");
        }
    };

    function getAllTest() {
        $http.get('api/test/all').then(function (response) {
            $scope.items = response.data;
            if ($scope.items.length == 0) {
                error("No Tests available in database");
            }
        }, function (response) {
            error("error");
        });
    }

    function getAllConstruct() {
        $http.get('api/test/constructList').then(function (response) {
            $scope.showStakeholderImage = true;
            $scope.items = response.data;
            if ($scope.items.length == 0) {
                error("No Construct available in database");
            }
        }, function (response) {
            error("error");
        });
    }

    function getAllSchool() {
        $http.get("/api/school/all").then(function (response) {
            $scope.items = response.data;
            if ($scope.items.length == 0) {
                error("No Schools available in database");
            }
        }, function (response) {
            error("error");
        });
    }

    function getAllInterest() {
        $http.get('/api/md/allParentInterests').then(function (response) {
            $scope.items = response.data;
            if ($scope.items.length == 0) {
                error("No Interests available in database");
            }
        });
    }

    $scope.getItem = function (id) {
        switch ($scope.componentName) {
            case 'Test':
                showTest(id);
                break;
            case 'Construct':
                showTest(id);
                break;
            case 'Interest':
                showInterest(id);
                break;
            case 'School':
                showSchool(id);
                break;
            case 'Career':
                showCareer(id);
                break;
            case 'Subject':
                showCareer(id);
        }
    };

    $scope.browseImage = function () {
        $('#testImg').trigger('click');
    };

    $scope.browseVideo = function () {
        $('#testVideo').trigger('click');
    };

    $scope.setFiles = function (element) {
        $scope.imgfile = element.files[0];
        displayImage($scope.imgfile);
    };

    $scope.setVideoFile = function (element) {
        $scope.videoFile = element.files[0];
    };

    function displayImage(file) {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            $scope.$apply(function () {
                $scope.testImageURI = reader.result;
            });
        };
        reader.onerror = function (error) {
            error("Failed to load image:" + error);
        };
    }

    $scope.upload = function (id) {
        var r = new FileReader();
        var fd = new FormData();
        fd.append('file', $scope.imgfile);
        if ($scope.uploadTestImage) {
            var testId = id;
            fd.append('testId', [testId]);
            fd.append('stakeholderImage', [$scope.stakeholderImage]);
            var request = {
                method: 'POST',
                url: '/api/raw/test/image/',
                data: fd,
                headers: {
                    'Content-Type': undefined
                }
            };
        } else {
            if ($scope.uploadSchoolImage) {
                var schoolId = id;
                fd.append('schoolId', [schoolId]);
                var request = {
                    method: 'POST',
                    url: '/api/raw/school/image/',
                    data: fd,
                    headers: {
                        'Content-Type': undefined
                    }
                };
            } else {
                if ($scope.uploadInterestImage) {
                    var interestId = id;
                    fd.append('interestId', [id]);
                    var request = {
                        method: 'POST',
                        url: '/api/raw/interest/image/',
                        data: fd,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                } else {
                    if ($scope.uploadCareerImage) {
                        var aimExpertInventoryId = id;
                        fd.append('aimExpertInventoryId', [id]);
                        var request = {
                            method: 'POST',
                            url: '/api/raw/aimExpertInventory/image/',
                            data: fd,
                            headers: {
                                'Content-Type': undefined
                            }
                        };
                    }
                }
            }
        }

        $http(request).then(function (response) {
            toaster.pop('success', "Image uploaded successfully");
            ok("Image uploaded");
            if ($scope.uploadSchoolImage) {
                showSchool(schoolId);
            } else {
                if ($scope.uploadTestImage) {
                    showTest(testId);
                } else {
                    if ($scope.uploadInterestImage) {
                        showInterest(interestId);
                    } else {
                        if ($scope.uploadCareerImage) {
                            showCareer(aimExpertInventoryId)
                        }
                    }
                }
            }

        }, function (response) {
            toaster.pop('error', "Image not uploaded successfully");
            error(error);
            if ($scope.uploadSchoolImage) {
                showSchool(schoolId);
            } else {
                if ($scope.uploadTestImage) {
                    showTest(testId);
                } else {
                    if ($scope.uploadInterestImage) {
                        showInterest(interestId);
                    } else {
                        if ($scope.uploadCareerImage) {
                            showCareer(aimExpertInventoryId)
                        }
                    }
                }
            }
        }
        );
    };

    function showTest(testId) {
        $scope.showTestOnCard = true;
        $http.get('api/test/' + testId).then(function (response) {
            $scope.testOnCard = response.data;
            $scope.uploadSchoolImage = false;
            $scope.uploadTestImage = true;
            $scope.uploadInterestImage = false;
            $scope.uploadCareerImage = false;

            $http({
                method: 'GET',
                url: '/api/raw/view/PROFILEIMAGE/' + $scope.testOnCard.logo,
                responseType: 'arraybuffer'
            }).then(function (response) {
                $scope.imgdata = btoa(String.fromCharCode.apply(null, new Uint8Array(response.data)));
                $scope.testImageURI = "data:image/PNG;base64," + $scope.imgdata;
            }, function (response) {
                toaster.pop('error', "Failed to load test's image");
                $scope.testImageURI = {};
            }
            );
        }, function (response) {
            toaster.pop('error', "Error", "Failed to load Test" + response.message);
        });
    }

    function showSchool(schoolId) {
        $scope.showTestOnCard = true;
        $http.get('api/school/' + schoolId).then(function (response) {
            $scope.testOnCard = response.data;
            $scope.uploadTestImage = false;
            $scope.uploadSchoolImage = true;
            $scope.uploadInterestImage = false;
            $scope.uploadCareerImage = false;
            $http({
                method: 'GET',
                url: '/api/raw/view/PROFILEIMAGE/' + $scope.testOnCard.logo,
                responseType: 'arraybuffer'
            }).then(function (response) {
                $scope.imgdata = btoa(String.fromCharCode.apply(null, new Uint8Array(response.data)));
                $scope.testImageURI = "data:image/PNG;base64," + $scope.imgdata;
            }, function (response) {
                toaster.pop('error', "Failed to load school's image");
                $scope.testImageURI = {};
            }
            );
        }, function (response) {
            toaster.pop('error', "Error", "Failed to load School" + response.message);
        });
    }

    function showCareer(id) {
        $scope.showTestOnCard = true;
        $http.get('/api/aimExpert/aimExpertInventoryById?id=' + id).then(function (response) {
            $scope.testOnCard = response.data;
            $scope.uploadSchoolImage = false;
            $scope.uploadTestImage = false;
            $scope.uploadInterestImage = false;
            $scope.uploadCareerImage = true;

            $http({
                method: 'GET',
                url: '/api/raw/view/PROFILEIMAGE/' + $scope.testOnCard.imageName,
                responseType: 'arraybuffer'
            }).then(function (response) {
                $scope.imgdata = btoa(String.fromCharCode.apply(null, new Uint8Array(response.data)));
                $scope.testImageURI = "data:image/PNG;base64," + $scope.imgdata;
            }, function (response) {
                toaster.pop('error', "Failed to load school's image");
                $scope.testImageURI = {};
            }
            );
        }, function (response) {
            toaster.pop('error', "Error", "Failed to load School" + response.message);
        });
    }

    function showInterest(id) {
        $scope.showTestOnCard = true;
        $http.get('api/md/findOneInterest/' + id).then(function (response) {
            $scope.testOnCard = response.data;
            $scope.uploadSchoolImage = false;
            $scope.uploadTestImage = false;
            $scope.uploadInterestImage = true;
            $scope.uploadCareerImage = false;


            $http({
                method: 'GET',
                url: '/api/raw/view/PROFILEIMAGE/' + $scope.testOnCard.image,
                responseType: 'arraybuffer'
            }).then(function (response) {
                $scope.imgdata = btoa(String.fromCharCode.apply(null, new Uint8Array(response.data)));
                $scope.testImageURI = "data:image/PNG;base64," + $scope.imgdata;
            }, function (response) {
                toaster.pop('error', "Failed to load interest's image");
                $scope.testImageURI = {};
            }
            );
        }, function (response) {
            toaster.pop('error', "Error", "Failed to load interest" + response.message);
        });
    }

    function createStringByArray(array) {
        var output = '';
        angular.forEach(array, function (object) {
            angular.forEach(object, function (value, key) {
                output += key + ',';
                output += value + ',';
            });
        });
        return output;
    }

    $scope.uploadVideo = function (id) {
        var postobject = {};
        postobject.id = id;
        postobject.stakeholderVideo = $scope.stakeholderVideo;
        postobject.file = $scope.videoFile;
        $scope.disableUpload = true;
        $scope.progress.hide = false;
        $scope.progress.percent = 0;
        loader();
        Upload.upload(
            {
                url: '/api/raw/test/video/',
                data: postobject,
                disableProgress: false
            }
        ).then(
            function (response) {
                cancelLoader();
                $scope.progress.hide = true;
                $scope.progress.percent = 0;
                $scope.disableUpload = false;
                ok("Successfully video saved");
            }, function (response) {
                cancelLoader();
                $scope.progress.hide = true;
                $scope.progress.percent = 0;
                $scope.disableUpload = false;
                error("error");
            }, function (evt) {
                // $scope.progress.percent = ((event.loaded * 100) / event.total).toFixed(2) + "%";
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            }
        );
    };

    function loader() {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    }

    function cancelLoader() {
        $scope.modalInstance.close();
    }

    function getAimExpertInventoryNamesList(type) {
        $http.get('/api/aimExpert/aimExpertInventoryList?type=' + type).then(function (response) {
            $scope.items = response.data;
        }, function (response) {
            alert(response.error);
        })
    }


});



