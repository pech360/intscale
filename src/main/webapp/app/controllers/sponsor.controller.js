app.controller('SponsorController', function ($scope, $http, toaster, $uibModal, $state) {

    $scope.loadingUpload = false;
    $scope.loadingSave = false;
    $scope.showForm = false;
    $scope.imageTypes = ["logo", "reportCoverBackImage", "reportCoverBackImage"];
    $scope.selectedImageType = "logo";
    $scope.list = function () {
        $http.get('/api/sponsor/list').then(function (response) {
            $scope.items = response.data;
        }, function (response) {
            toaster.pop('Info', response.error);
        });
    };

    $scope.list();

    $scope.openLoadingModal = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'createSession.html',
            size: 'lg',
            scope: $scope,
            backdrop: 'static'
        });
    };


    $scope.cancelLoadingModal = function () {
        $scope.modalInstance.close();
    };

    $scope.viewSponsorImage = function (imageName) {
        if (imageName) {
            $http({
                method: 'GET',
                url: '/api/raw/view/PROFILEIMAGE/' + imageName,
                responseType: 'arraybuffer'
            }).then(function (response) {
                    var temp = btoa(String.fromCharCode.apply(null, new Uint8Array(response.data)));
                    $scope.mediaFileURI = "data:image/PNG;base64," + temp;
                }, function (response) {
                    toaster.pop('error', "Failed to load sponsor's "+ imageName);
                    $scope.mediaFileURI = {};
                }
            );
        } else {
            $scope.mediaFileURI = {};
            toaster.pop('info', "selected image not uploaded yet");
        }
    };
    $scope.edit = function (item) {
        $scope.workingSponsor = item;
        $scope.showForm = true;
        $scope.viewSponsorImage(item.logo);
    };

    $scope.cancel = function () {
        $scope.workingSponsor = {};
        $scope.mediaFileURI = {};
        $scope.showForm = false;
    };
    $scope.new = function () {
        $scope.workingSponsor = {};
        $scope.mediaFileURI = {};
        $scope.showForm = true;
    };

    $scope.save = function () {
        if ($scope.workingSponsor) {
            $scope.loadingSave = true;
            $scope.openLoadingModal();
            $http.post('/api/sponsor/save', $scope.workingSponsor).then(function (response) {
                $scope.workingSponsor = response.data;
                $scope.cancelLoadingModal();
                $scope.list();
                ok('sponsor saved!');
                $scope.loadingSave = false;
            }, function (response) {
                $scope.loadingSave = false;
                error(response.data.error);
            });
        } else {
            toaster.pop('Info', "Empty request");
        }
    };

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.browseImage = function () {
        $('#mediaFilePlaceholder').trigger('click');
    };

    $scope.setFiles = function (element) {
        $scope.mediaFile = element.files[0];
        displayImage($scope.mediaFile);
    };

    function displayImage(file) {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            $scope.$apply(function () {
                $scope.mediaFileURI = reader.result;
            });
        };
        reader.onerror = function (error) {
            error("Failed to load image:" + error);
        };
    }


    $scope.uploadMedia = function (sponsorId, imageType) {
        if ($scope.mediaFile) {
            var fd = new FormData();
            fd.append('file', $scope.mediaFile);
            fd.append('id', [sponsorId]);
            fd.append('imageType', [imageType]);
            var request = {
                method: 'POST',
                url: '/api/sponsor/image/upload',
                data: fd,
                headers: {
                    'Content-Type': undefined
                }
            };
            $scope.loadingUpload = true;

            $http(request).then(function (response) {
                    $scope.loadingUpload = false;
                    toaster.pop('success', "Image uploaded successfully");
                    // ok("Image uploaded");
                }, function (response) {
                    $scope.loadingUpload = false;
                    toaster.pop('error', "Image not uploaded successfully");
                    // error(error);
                }
            );
        }
    }


});