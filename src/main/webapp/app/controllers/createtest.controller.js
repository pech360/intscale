app.filter('pagination', function () {
    return function (input, start) {
        start = +start;
        if (input instanceof Array)
            return input.slice(start);
    };
});

app.controller('CreateTestController', function ($scope, $http, $state, $stateParams, $uibModal, toaster) {

    $scope.filterTags = [];
    $scope.filterSubTags = [];

    $scope.subTags = [];
    $scope.testVersion=0;
    $scope.showStep1 = true;
    $scope.showStep3 = false;
    $scope.showStep2 = false;
    $scope.showStep4 = false;
    $scope.showStep5 = false;
    $scope.showStep6 = false;
    $scope.totalSteps = 6;
    $scope.percentVal = 0;
    $scope.progressLength = 0;
    $scope.randomizeQuestions = true;
    $scope.ageGroupValue = [];
    var countQuestion = 0;
    var tempQuestion = {};
    $scope.curPage = 0;
    $scope.pageSize = 20;
    $scope.goToPage = 1;
    $scope.showLoadingIcon = false;
    $scope.readUser=$stateParams.readUser;
    $scope.version=$stateParams.version;
    $scope.defaultPicture = "/assets/img/default-test-picture.jpeg";
    if ($stateParams.testId) {
        $scope.testId = $stateParams.testId;
    }
    $scope.test = {
        name: "",
        logo: "",
        introduction: "",
        participation: "",
        disclaimer: "",
        procedure: "",
        description: "",
        ageMin: 0,
        ageMax: 100,
        scoreCategory: "",
        scoreSubCategory: "",
        notes: "",
        redirectTo: "",
        version:0,
        paid: false,
        showResults: false,
        random: true,
        autoIncrementQuestion: false,
        navigationAllowed: false,
        skipQuestions: false,
        published: false,
        tenant: null,
        questions: [],
        disableSubmit: false,
        submitLimit: 80,
        primaryReportText:"",
        secondaryReportText:""
    };

    $scope.hideNextButton = true;
    $scope.showPreviousButton = false;

       $scope.parentLeafConstruct=function(){
        $http.get('/api/test/allConstructByLeafNodeIsTrue').then(function(response){
                    $scope.constructList=response.data;
                 },function(response){
               error(response.data.error);
             });
       };
       $scope.parentLeafConstruct();
       $scope.treeListtest=function(){
         $http.get('/api/test/treeList').then(
          function(response){
            $scope.list=response.data;
            $scope.openTreeModal();
          },
          function(response){
           error(response.data.error);
          });
       };

       $scope.openTreeModal = function () {
                   $scope.ModalInstance = $uibModal.open({
                   templateUrl: 'testTree.html',
                   size: 'lg',
                   scope: $scope,
                   backdrop: 'static'
               });
             };

      $scope.close = function () { $scope.ModalInstance.close(); };

    $scope.getAllQuestions = function () {

        $http.get('/api/question/list').then(function (response) {
            $scope.questionList = response.data;
            for (countQuestion = 0; countQuestion < $scope.questionList.length; countQuestion++) {
                $scope.questionList[countQuestion].selected = false;
                $scope.TotalRecordCount = $scope.questionList.length;
            }
        }, function (response) {
            error("error in loading questions");
        })
    };

    if ($scope.testId) {
        $http.get('/api/test/fullTest/' + $scope.testId).then(function (response) {
            $scope.test = response.data;
            $scope.test.parentId=JSON.stringify(response.data.parentId);
            //if($scope.version){$scope.testVersion=$scope.testVersion+1;}
        });
    }

    $scope.selectQuestions = function () {
        $scope.modalInstance1 = $uibModal.open({
            templateUrl: 'views/selectQuestionsForTest.html',
            size: 'lg',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.closeSelectQuestions = function () {
        $scope.modalInstance1.close();
    };

    $scope.setCurrentPage = function () {
        $scope.curPage = 0;
    };

    $scope.numberOfPages = function () {
        $scope.pages = [];
        $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
        for (var i = 1; i <= $scope.totalPages; i++) {
            $scope.pages.push(i);
        }
        return $scope.totalPages;
    };

    $scope.loadMasterData = function () {
        $http.get('api/category/parent').then(function (response) {
            $scope.categories = response.data;
        });
        $http.get('api/category/child').then(function (response) {
            $scope.childCategories = response.data;
        });
        $http.get('api/md/userTypes/').then(function (response) {
            $scope.userTypes = response.data;
        });
        $http.get('api/md/studentTypes/').then(function (response) {
            $scope.studentTypes = response.data;
        });
        $http.get('api/md/gender/').then(function (response) {
            $scope.genders = response.data;
        });
        $http.get('api/md/languages/').then(function (response) {
            $scope.languages = response.data;
        });
        $http.get('api/md/grades/').then(function (response) {
            $scope.grades = response.data;
        });
        $scope.getAllQuestions();
        $scope.progressLength = 0;
    };

    $scope.loadMasterData();

    $scope.reportsTypes=['AimReport','StudentReport','NoneReport','TalentReport'];

    $scope.step1 = function () {
        $scope.showStep2 = false;
        $scope.showStep3 = false;
        $scope.showStep4 = false;
        $scope.showStep5 = false;
        $scope.showStep6 = false;
        $scope.showStep1 = true;
        $scope.percentVal = 16.666;
        $scope.calculateProgress();
    };
    $scope.step2 = function (notes) {
        if((!notes||notes.length<5)&&notes!='back1'){
            toaster.pop('Error',"please fill notes properly");
            return;
          }
        $scope.showStep1 = false;
        $scope.showStep3 = false;
        $scope.showStep4 = false;
        $scope.showStep5 = false;
        $scope.showStep6 = false;
        $scope.showStep2 = true;
        $scope.percentVal = 33.33333;
        $scope.calculateProgress();
    };
    $scope.step3 = function (reportType) {
        if(!reportType){
            toaster.pop('Error',"please select report type");
            return;
          }
        var proceedStep3 = false;
        if ($scope.test.disableSubmit) {
            if ($scope.test.submitLimit < 101) {
                proceedStep3= true;
            } else {
                toaster.pop('Info', "Submit limit must not exceed 100.");
            }
        }else{
            proceedStep3 =true;
        }
        if(proceedStep3){
            $scope.showStep1 = false;
            $scope.showStep2 = false;
            $scope.showStep4 = false;
            $scope.showStep5 = false;
            $scope.showStep6 = false;
            $scope.showStep3 = true;
            $scope.percentVal = 49.9999;
            $scope.calculateProgress();
        }
    };
    $scope.step4 = function () {
        if(!$scope.test.parentId){
            toaster.pop('Error',"please select parent construct");
            return;
          }
        $scope.showStep1 = false;
        $scope.showStep2 = false;
        $scope.showStep3 = false;
        $scope.showStep5 = false;
        $scope.showStep6 = false;
        $scope.showStep4 = true;
        $scope.percentVal = 66.6664;
        $scope.readSlider();
        $scope.calculateProgress();
    };
    $scope.step5 = function () {
        $scope.showStep1 = false;
        $scope.showStep2 = false;
        $scope.showStep3 = false;
        $scope.showStep4 = false;
        $scope.showStep6 = false;
        $scope.showStep5 = true;
        if ($scope.testId) {
            for (var countTestQuestions = 0; countTestQuestions < $scope.test.questions.length; countTestQuestions++) {
                for (var countAllQuestions = 0; countAllQuestions < $scope.questionList.length; countAllQuestions++) {
                    if ($scope.test.questions[countTestQuestions].id == $scope.questionList[countAllQuestions].id) {
                        $scope.questionList[countAllQuestions].selected = true;
                    }
                }
            }
        }
        $scope.percentVal = 83.6666;

        $scope.calculateProgress();
    };
    $scope.step6 = function () {
        $scope.showStep1 = false;
        $scope.showStep2 = false;
        $scope.showStep3 = false;
        $scope.showStep4 = false;
        $scope.showStep5 = false;
        $scope.showStep6 = true;
        $scope.percentVal = 100;
        $scope.calculateProgress();
    };

    $scope.filteredChildCategoryByParent = function (parentId) {
        $http.get('api/category/childByParent?parentId=' + parentId).then(function (response) {
            $scope.childCategories = response.data;
        });
    };

    $scope.filteredChildCategoryByParentName = function (parentName) {
        $http.get('api/category/childByParentName?parentName=' + parentName).then(function (response) {
            $scope.childCategories = response.data;
        });
    };

    $scope.filterQuestionsByCategory = function (categoryId) {
        $scope.filteredChildCategoryByParent(categoryId);
        $http.get('/api/question/byCategory?categoryId=' + categoryId).then(function (response) {
            $scope.questionList = response.data;
            if ($scope.questionList.length > 0) {
                for (countQuestion = 0; countQuestion < $scope.questionList.length; countQuestion++) {
                    $scope.questionList[countQuestion].selected = false;
                }
            } else {
                ok("No questions for this category");
            }

        }, function (response) {
            error("error in loading questions for Cat");
        })
    };

    $scope.filterQuestionsBySubCategory = function (subCategoryId) {
        $http.get('/api/question/bySubCategory?subCategoryId=' + subCategoryId).then(function (response) {
            $scope.questionList = response.data;
            if ($scope.questionList.length > 0) {
                for (countQuestion = 0; countQuestion < $scope.questionList.length; countQuestion++) {
                    $scope.questionList[countQuestion].selected = false;
                }
            } else {
                ok("No questions for this subCategory");
            }

        }, function (response) {
        })
    };

    // $scope.isSelectAllQuestions = function (selectAllQuestionsForTest) {
    //     if (selectAllQuestionsForTest) {
    //         $scope.test.questions = [];
    //         for (countQuestion = 0; countQuestion < $scope.questionList.length; countQuestion++) {
    //             $scope.questionList[countQuestion].selected = true;
    //             $scope.test.questions.push($scope.questionList[countQuestion]);
    //         }
    //     } else {
    //         for (countQuestion = 0; countQuestion < $scope.questionList.length; countQuestion++) {
    //             $scope.questionList[countQuestion].selected = false;
    //             $scope.test.questions = [];
    //         }
    //     }
    // };

    $scope.isQuestionSelected = function (question) {
        // if (question.selected) {
        //     question.selected = true;
        //     $scope.test.questions.push(question);
        // } else {
        //     if (!question.selected) {
        //         for (var i = 0; i < $scope.test.questions.length; i++) {
        //             if ($scope.test.questions[i].id == question.id) {
        //                 $scope.test.questions.splice(i, 1);
        //             }
        //         }
        //     }
        // }
        for (i = 0; i < $scope.questionList.length; i++) {
            if (question.id == $scope.questionList[i].id) {
                $scope.test.questions.push($scope.questionList.splice(i, 1)[0]);
                break;
            }
        }
    };

    $scope.removeQuestionSelected = function (question) {
        for (i = 0; i < $scope.test.questions.length; i++) {
            if (question.id == $scope.test.questions[i].id) {
                $scope.test.questions[i].selected = false;
                $scope.questionList.push($scope.test.questions.splice(i, 1)[0]);
                break;
            }
        }
    };

    $scope.cancelTest = function () {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.value) {
                $scope.cancelled();
            }
        })
    };

    $scope.cancelled = function () {
        ok("Cancelled");
        $scope.progressLength = 0;
        $scope.step1();
        $scope.test = {};
        $scope.$apply();


    };

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function displayImage(file) {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            $scope.$apply(function () {
                $scope.testImage = reader.result;
            });
        };
        reader.onerror = function (error) {
            error("Failed to load image:" + error);
        };
    }

    var ageGroupSliderObj = document.getElementById('ageGroupSlider');

    noUiSlider.create(ageGroupSliderObj, {
        start: [0, 20],
        behaviour: 'tap',
        connect: true,
        tooltips: true,
        format: wNumb({
            decimals: 0
        }),
        range: {
            'min': 0,
            'max': 100
        },
        pips: {
            mode: 'positions',
            values: [0, 10, 20, 30, 50, 75, 100],
            density: 4,
            stepped: true
        }
    });

    $scope.readSlider = function () {
        $scope.ageGroupValue = ageGroupSliderObj.noUiSlider.get();
        $scope.test.ageMin = $scope.ageGroupValue[0];
        $scope.test.ageMax = $scope.ageGroupValue[1];
    };

    $scope.resetSlider = function () {
        ageGroupSliderObj.noUiSlider.set(0, 20);
    };

    $scope.saveTest = function () {
        if ($scope.test.name) {
            $scope.showLoadingIcon = true;
            if ($stateParams.testId) {
                $http.put('/api/test/'+$scope.version+'/', $scope.test).then(function (response) {
                    ok("Test updated");
                    $scope.showLoadingIcon = false;
                    $scope.test = {};
                    $scope.step1();
                    $scope.progressLength = 0;
                }, function (response) {
                    error("error");
                });
            } else {
                $http.post('/api/test/', $scope.test).then(function (response) {
                    ok("Test Created");
                    $scope.showLoadingIcon = false;
                    $scope.test = {};
                    $scope.step1();
                    $scope.progressLength = 0;
                }, function (response) {
                    error("error");
                });
            }
        }
        else {
            toaster.pop('error', "Please enter Test's name");
        }
    };

    $scope.tagSelected = function (tag) {
        if (tag) {
            for (var i = 0; i < $scope.tags.length; i++) {
                if (tag.id == $scope.tags[i].id) {
                    var v = $scope.tags.splice(i, 1)[0];
                    $scope.filterTags.push(v);
                    addSubTag(v);
                    return;
                }
            }
        }
    };

    $scope.removeTag = function (tag) {
        for (i = 0; i < $scope.filterTags.length; i++) {
            if (tag.id == $scope.filterTags[i].id) {
                $scope.tags.push($scope.filterTags.splice(i, 1)[0]);
                // TODO :
                removeSubTagsFromSubtagFilter(tag);
                return;
            }
        }
    };

    function addSubTag(tag) {
        var subtags = tag.subTags;
        for (j = 0; j < subtags.length; j++) {
            $scope.filterSubTags.push(subtags[j]);
        }
    }

    function removeSubTagsFromSubtagFilter(tag) {
        if (tag) {
            var subTags = tag.subTags;
            for (i = 0; i < subTags.length; i++) {
                for (j = 0; j < $scope.subTags.length; j++) {
                    if ($scope.subTags[j].id == subTags[i].id) {
                        $scope.subTags.splice(j, 1);
                        break;
                    }
                }
                for (j = 0; j < $scope.filterSubTags.length; j++) {
                    if ($scope.filterSubTags[j].id == subTags[i].id) {
                        $scope.filterSubTags.splice(j, 1);
                        break;
                    }
                }
            }
        }
    }


    $scope.removeSubTag = function (tag) {
        // TODO :
        for (i = 0; i < $scope.filterSubTags.length; i++) {
            if ($scope.filterSubTags[i].id == tag.id) {
                var subTag = $scope.filterSubTags.splice(i, 1)[0];
                $scope.subTags.push(subTag);
            }
        }
    };

    $scope.addSubTag = function (subTag) {
        if (subTag) {
            for (var i = 0; i < $scope.subTags.length; i++) {
                if (subTag.id == $scope.subTags[i].id) {
                    var v = $scope.subTags.splice(i, 1)[0];
                    $scope.filterSubTags.push(v);
                    return;
                }
            }
        }
    };


    function loadTags() {
        $http.get('/api/tagByType?type=QUESTION').then(
            function (response) {
                $scope.tags = response.data;
            },
            function (response) {
                error(response.data.error)
            }
        );
    }


    loadTags();


// calculates progress bar
    $scope.calculateProgress = function () {
        $scope.progressLength = Math.round($scope.percentVal);
        $scope.width = $scope.progressLength + "%";
    };

    $scope.getQuestion = function (id) {
        $http.get('/api/question/' + id).then(function (response) {
            $scope.currentQuestion = response.data;
            $scope.viewQuestion();
        }, function (response) {
            toaster.pop('error', "Failed to Load Question...")
        });
    };

    $scope.viewQuestion = function () {
        $scope.viewQuestionmodalInstance = $uibModal.open({
            templateUrl: 'views/viewQuestion.html',
            size: 'lg',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.closeViewQuestion = function () {
        $scope.viewQuestionmodalInstance.close();
    };
});

app.filter('subTagFilter', function () {
        return function (input, subTags) {
            if(subTags && subTags.length == 0){
                return input;
            }
            var out = [];
            for (i = 0; i < input.length; i++) {
                p1 : for (j = 0; j < input[i].subTags.length; j++) {
                    var tag = input[i].subTags[j];
                    for (k = 0; k < subTags.length; k++) {
                        if (tag.id == subTags[k].id) {
                            out.push(input[i]);
                            break p1;
                        }
                    }
                }
            }
            return out;
        };
    }
);
