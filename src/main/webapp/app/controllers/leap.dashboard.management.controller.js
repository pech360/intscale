app.controller('LeapDashboardManagementController', function ($scope, $http, DateFormatService, toaster, $uibModal) {

    $scope.orgName = window.location.hostname.split(".")[0];


    var request = {

        grades: [],
        products: [],
        fromDate: {},
        toDate: {}
    };
    $scope.gradeResults = [];
    $scope.currentSubjectName = '';
    var testNames = {};

    var now = new Date();
    var ss = now.getSeconds();
    var mm = now.getMinutes();
    var hh = now.getHours();

    var fromD = new Date();
    fromD = moment(fromD);
    fromD = fromD.set('month',3);
    fromD = fromD.startOf('month');
    fromD = fromD.subtract(1, 'years');
    fromD = fromD.set({ 'hour' : hh, 'minute' : mm, 'second' : ss });
    fromD = fromD.format('MM/DD/YYYY HH:mm:ss');

    $scope.fromDate = new Date(fromD);
    $scope.toDate = new Date();

    $scope.shiftFromDate = function (to) {
        switch (to) {
            case "lastWeek":
                fromD = new Date();
                fromD = moment(fromD).subtract(1, 'weeks');
                fromD = fromD.set({ 'hour' : hh, 'minute' : mm, 'second' : ss });
                fromD = fromD.format('MM/DD/YYYY HH:mm:ss');
                $scope.fromDate = new Date(fromD);
                break;
            case "lastMonth":
                fromD = new Date();
                fromD = moment(fromD).subtract(1, 'months');
                fromD = fromD.set({ 'hour' : hh, 'minute' : mm, 'second' : ss });
                fromD = fromD.format('MM/DD/YYYY HH:mm:ss');
                $scope.fromDate = new Date(fromD);
                break;
        }
       $scope.getResultForGrades();
    };

    $scope.reset = function () {
        var request = {
            grades: [],
            products: [],
            fromDate: {},
            toDate: {}
        };
    };

    function getAllGrades() {
        $http.get('/api/school/getGradesByTeacher').then(function (response) {
            $scope.grades = response.data;
        });
    }

    function getAllSections() {
        $http.get('/api/school/getSectionsByTeacher').then(function (response) {
            $scope.sections = response.data;
        });
    }

    $scope.getSubjects = function () {
        var flag = true;
        if (!$scope.currentGrade) {
            flag = false;
        }
        if (!$scope.currentSection) {
            flag = false;
        }
        if (flag) {
            $http.get('/api/school/getSectionsByTeacher').then(function (response) {
                $scope.sections = response.data;
            });
        }

    };

    function populateDashRequest(grade, constructId) {
        request.grades = [];
        request.products = [];
        request.fromDate = DateFormatService.formatDate9($scope.fromDate);
        request.toDate = DateFormatService.formatDate9($scope.toDate);
        request.resultType = 'management';
        if (grade) {
            request.grades.push(grade);
            $scope.currentGrade = grade;
        }
        if (constructId) {
            request.products.push(constructId);
        }

        return request;

    }


    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function getOverAllDashboardBySubject(grade, subjects, resultType) {

        for (var i in subjects) {
            renderChart(grade, subjects[i], i, resultType);
        }
    }

    function getOverAllDashboard(resultByGrades, resultType) {
        for (var i in resultByGrades) {
            getOverAllDashboardBySubject(i, resultByGrades[i], resultType);
        }
    }

    function renderChart(grade, subject, index, resultType) {

        var temp = {};
        temp.labels = [];
        temp.data = [];

        // temp.labels = Object.keys(subject.values);
        temp.data = Object.values(subject.values);

        Object.keys(subject.values).forEach(function (key, index) {
            switch (key) {
                case "C":
                    temp.labels.push("0-40%");
                    break;
                case "B":
                    temp.labels.push("41-70%");
                    break;
                case "A":
                    temp.labels.push("71-100%");
                    break;
            }
        });

        switch (resultType) {
            case "grade":
                $scope.resultByGrades[grade][index].labels = angular.copy(temp.labels);
                $scope.resultByGrades[grade][index].data = angular.copy(temp.data);
                $scope.resultByGrades[grade][index].name = testNames[$scope.resultByGrades[grade][index].id].name;
                break;
            case "section":
                $scope.resultBySections[grade][index].labels = angular.copy(temp.labels);
                $scope.resultBySections[grade][index].data = angular.copy(temp.data);
                $scope.resultBySections[grade][index].name = testNames[$scope.resultBySections[grade][index].id].name;
                break;
        }
    }

    $scope.colors = ['#4158d5', '#00bb7e', '#ffc107'];
    $scope.pieOptions = {
        responsive: true,
        legend: {
            display: true,
            position: 'right'
        },
        tooltips: {
            callbacks: {
                title: function (tooltipItem, chartData) {
                    return "No. of students"
                }
            }
        },

        plugins: {
            datalabels: {
                anchor: 'center',
                backgroundColor: function (context) {
                    return context.dataset.backgroundColor;
                },
                color: 'white',
                font: {
                    weight: 'bold'
                },
                formatter: function (value, data) {
                    var allData = data.dataset.data;
                    var total = 0;
                    for (var i in allData) {
                        total += parseFloat(allData[i]);
                    }
                    var t = Math.round((value / total) * 100);
                    return t + '%';
                }
            }
        }
    };

    $scope.getResultForGrades = function () {
        request = populateDashRequest();

        $http.post('/api/school/overAllTestResult/grades', request).then(function (response) {


            $scope.resultByGrades = response.data;

            getOverAllDashboard($scope.resultByGrades, 'grade');
        }, function (response) {
        })
    };

    $scope.getResultForSections = function (grade, constructId) {
        request = populateDashRequest(grade, constructId);


        $http.post('/api/school/overAllTestResult/sections', request).then(function (response) {
            $scope.resultBySections = response.data;

            getOverAllDashboard($scope.resultBySections, 'section');
            openSectionsPerformanceModal();

            $scope.currentSubjectName = testNames[constructId].name;
        }, function (response) {
        });
    };

    function getStudentCountByGrade() {
        request = populateDashRequest();

        $http.post('/api/school/getNumberOfStudentsByGrade', request).then(function (response) {

            $scope.studentCountByGrade = response.data;

        }, function (response) {
        })
    }


    function getTestNames() {
        $http.get('/api/result/getTestNames', request).then(function (response) {
            testNames = response.data;
            $scope.getResultForGrades();
        }, function (response) {
        })
    }


    $scope.loader = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.cancelLoader = function () {
        $scope.modalInstance.close();
    };

    function openSectionsPerformanceModal() {
        $scope.sectionsPerformanceModal = $uibModal.open({
            templateUrl: 'views/modals/sectionsPerformance.html',
            size: 'lg',
            scope: $scope,
            backdrop: 'true'
        });
    }

    $scope.closeSectionsPerformanceModal = function () {
        $scope.sectionsPerformanceModal.close();
    };


    function init() {
        getAllGrades();
        getAllSections();
        $scope.getSubjects();
        getTestNames();
        $scope.reset();
        getStudentCountByGrade();
    }

    init();

})
;