app.controller('InterestsController', function ($http, $scope, toaster) {

    $scope.newInterest = function () {
        $scope.workingInterest = {}
    };

    $scope.updateInterest = function (interest) {
        $scope.workingInterest = angular.copy(interest);
        if (!$scope.workingInterest.parent) {
            $scope.workingInterest.parent = "";
        }

    };

    $scope.saveInterest = function () {

        if (!$scope.workingInterest.name) {
            toaster.pop('Error', "Interest Name cannot be empty ");
            return;

        }
        if ($scope.workingInterest.id) {
            $http.put('/api/md', $scope.workingInterest).then(function (response) {
                ok("Interest updated successfully");
                refresh();

            }, function (response) {
                error(response.data.error);
            });
        } else {
            $http.post('/api/md', $scope.workingInterest).then(function (response) {
                ok("Interest created successfully");

                refresh();
            }, function (response) {
                error(response.data.error);
            });
        }
    };
    $scope.check = function () {
        var flag = true;
        for (i = 0; i < $scope.interest.length; i++) {
            flag = false;
            break;
        }
        $scope.all = flag;
    };

    $scope.remove = function () {
        var toDelete = [];
        for (i = 0; i < $scope.interests.length; i++) {
            if ($scope.interests[i].checked) {
                toDelete.push($scope.interests[i].id);
            }
        }

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Delete!'
        }).then(function (result) {
            if (result.value) {
                deleteInterests(toDelete);
            }
        });
    };

    function deleteInterests(list) {
        $http.post('/api/md/delete/all', list).then(
            function (response) {
                refresh();
            }, function (response) {
                error(response.data.error)
            }
        );
    }

    $scope.selectAllInterests = function (all) {
        angular.forEach($scope.interests, function (intersetVar) {
            intersetVar.checked = all;
        });
    };

    function refresh() {
        $scope.workingInterest = {};
        $http.get('/api/md/allInterest').then(function (response) {
            $scope.interests = response.data;
            angular.forEach($scope.interest, function (intersetVar) {
                intersetVar.checked = false;
            });
        });

        $http.get('/api/md/allParentInterests').then(function (response) {
            $scope.parentInterests = response.data;
        });
    }


    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    refresh();
});