app.filter('pagination', function()
{
return function(input, start)
{
 start = +start;
 if(input instanceof Array)
 return input.slice(start);
};
});
app.controller('PilotManagementController',function($scope,$http,toaster,$uibModal,$state){
      $scope.curPage = 0;
      $scope.pageSize = 5;
      $scope.TotalRecordCount=0;
      $scope.goToPage = 1;
      $scope.listOfPilots={};
      $scope.pilot={};

     $scope.setCurrentPage = function() {
         $scope.curPage = 0;
     };

     $scope.numberOfPages = function() {
         $scope.pages = [];
         $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
         for (var i = 1; i <= $scope.totalPages; i++) {
             $scope.pages.push(i);
         }
         return $scope.totalPages;
     };

   $scope.list=function(){
        $http.get('/api/pilot/').then(function(response){
         $scope.listOfPilots=response.data;
         $scope.TotalRecordCount=response.data.length;
        },function(response){
           toaster.pop('Info',response.data.error);
        });
   };

   $scope.list();

   $scope.openPilotModal =  function () {
           $scope.modalInstance = $uibModal.open({
               templateUrl: 'createPilot.html',
               size: 'lg',
               scope: $scope,
               backdrop: 'static'
           });
       };

   $scope.cancel = function() {
          $scope.modalInstance.close();
      };

   $scope.createNew = function() {
       $scope.pilot={};
       $scope.openPilotModal();
   };
   $scope.create=function(pilot){
          if(!pilot.name){
          toaster.pop('Info',"Pilot name can't be empty");
          return;
          }
          if(!pilot.active){
          toaster.pop('Info',"Please checked pilot status");
          return;
          }
     if(pilot.id){
       $http.put('/api/pilot/',pilot).then(function(response){
           $scope.list();
           $scope.cancel();
           ok('Record successfully updated');
          },function(response){
           error(response.data.error);
          });
       }else{
          $http.post('/api/pilot/',pilot).then(function(response){
          $scope.list();
          $scope.cancel();
          ok('Record successfully created');
          },function(response){
          error(response.data.error);
          });
     }
   };

   function ok(message) {
             swal({
                 title: message,
                 type: 'success',
                 buttonsStyling: false,
                 confirmButtonClass: "btn btn-warning"
             });
         };
     function error(message) {
         swal({
             title: message,
             type: 'error',
             buttonsStyling: false,
             confirmButtonClass: "btn btn-warning"
         });
     };
});