app.controller('AvailabletestsController', function ($scope, $http, Auth, CommonService, toaster, $localStorage, $rootScope, $uibModal, DateFormatService, $window, $state) {
    $scope.assets_url = $localStorage.assets_url;
    // $scope.show404page = false;

    $rootScope.showTestNavBar = false;
    $rootScope.showDefaultNavBar = true;
    var askForCaptureProfile = false;
    $scope.showStep0 = false;
    $scope.showStep01 = false;
    $scope.showStep1 = false;
    $scope.showStep3 = false;
    $scope.showStep2 = false;
    $scope.showStep4 = false;
    $scope.showStep5 = false;
    $scope.showStep6 = false;
    $scope.showStep7 = false;
    $scope.showStep8 = false;
    $scope.showStep9 = false;
    $scope.selectedDay = '';
    $scope.selectedMonth = '';
    $scope.selectedYear = '';
    $scope.highSchool = false;
    $scope.middleSchool = false;
    $scope.selectedInterests = [];
    $scope.childInterests = [];
    $scope.showAddMore = false;
    $scope.customInterestName = "";
    $scope.languages = [];
    $scope.orgName = window.location.hostname.split(".")[0];
    $scope.disableFinishButton = false;
    $scope.grades = ["6", "7", "8", "9", "10", "11", "12", "Higher Education"];


    $scope.threeSixtyEligible = false;
    $scope.stakeholders = [];
    $scope.allStakeholderRelations = [
        { name: 'Select Relation', value: null },
        { name: 'Parent', value: 'Parent' },
        { name: 'Sibling', value: 'Sibling' },
        { name: 'Peer', value: 'Peer' },
        { name: 'Teacher', value: 'Teacher' }
    ];
    $scope.stakeholderRelations = angular.copy($scope.allStakeholderRelations);
    $scope.new = {};

    $scope.captureProfile = function () {
        $scope.captureProfileModal = $uibModal.open({
            templateUrl: 'views/modals/profileCapture.html',
            size: 'lg',
            scope: $scope,
            backdrop: 'static',
            keyboard: false
        });

        $scope.step0();
    };

    $scope.closeCaptureProfile = function () {
        $scope.captureProfileModal.close();
        updateUserInfo();
    };


    $scope.capturePhoneNoForGoogleUser = function () {
        $scope.capturePhoneNoForGoogleUserModal = $uibModal.open({
            templateUrl: 'views/modals/askPhoneNoForGoogleUser.html',
            size: 'lg',
            scope: $scope,
            backdrop: 'static',
            keyboard: false
        });


    };


    $scope.closecapturePhoneNoForGoogleUser = function () {
        $scope.capturePhoneNoForGoogleUserModal.close();
        updateUserInfo();
    };
    function fetchProfile() {
        $http.get('/api/user/profile').then(function (response) {
            $scope.user = response.data;
            if ($scope.user.img) {
                $scope.userImageURI = $scope.assets_url + $scope.user.img;
            } else {
                if ($scope.user.gender) {
                    if ($scope.user.gender.toLowerCase() == "male") {
                        $scope.userImageURI = "/assets/img/b-avtar.png"
                    } else {
                        if ($scope.user.gender.toLowerCase() == "female") {
                            $scope.userImageURI = "/assets/img/g-avtar.png"
                        }
                    }
                } else {
                    $scope.userImageURI = "/assets/img/default-profile-picture.png";
                }
            }
        }, function (response) {
            toaster.pop('error', "Error", "Failed to load profile - " + response.data.error);

        });
    }

    function doIneedToCaptureProfile() {
        $http.get('/api/user/profile').then(function (response) {


            $scope.user = response.data;
            if ($scope.user.dob) {
                $scope.user.dob = new Date(response.data.dob);
            }
            if ($scope.user) {
                askForCaptureProfile = false;
                if (($scope.user.premium == true || $scope.user.premium == null) && $scope.user.roles[0] == 'TestTaker' && $scope.orgName != 'www' && $scope.orgName != 'talent') {
                    if (!$scope.user.language || !$scope.user.name || !$scope.user.gender || !$scope.user.dob || !$scope.user.grade || !$scope.user.school || !$scope.user.academicScore) {
                        askForCaptureProfile = true;
                    }


                    if ($scope.user.academicPerformances.length == 0) {
                        var temp = {
                            type: "SUBJECT",
                            name: "SCIENCE",
                            value: ""
                        };
                        $scope.user.academicPerformances.push(angular.copy(temp));
                        temp.name = "MATHEMATICS";
                        $scope.user.academicPerformances.push(angular.copy(temp));
                        askForCaptureProfile = true;
                    }

                    if (!$scope.user.contactNumber) {
                        askForCaptureProfile = true;

                    }
                    // if ($scope.user.interest) {
                    //     if ($scope.user.interest.length < 2) {
                    //         askForCaptureProfile = true;
                    //     }
                    // } else {
                    //     askForCaptureProfile = true;
                    // }

                    if (askForCaptureProfile) {

                        $scope.show404page = false;
                        getLanguages();
                        $scope.captureProfile();
                    } else {
                        $scope.askForEmail();
                    }
                } else {
                    $scope.askForEmail();
                    if ($scope.orgName == 'assess' || $scope.orgName == 'localhost') {

                        if (!$scope.user.contactNumber) {

                            $scope.capturePhoneNoForGoogleUser();
                        }
                    }


                }
                //                $scope.getAllAvailableTest();
            }
        }, function (error) {
            // if (error.status == 401) {

            //     delete $localStorage.token;
            //     delete $localStorage.ngStorage - assets_url
            //     localStorage.clear();
            // }

        });
    }

    $scope.step0 = function () {

        if ($scope.user.name) {
            $scope.step01();
        } else {
            $scope.showStep1 = false;
            $scope.showStep2 = false;
            $scope.showStep3 = false;
            $scope.showStep4 = false;
            $scope.showStep5 = false;
            $scope.showStep6 = false;
            $scope.showStep7 = false;
            $scope.showStep8 = false;
            $scope.showStep01 = false;
            $scope.showStep0 = true;


        }
    };

    // for contact no
    $scope.step01 = function () {

        if ($scope.user.contactNumber) {
            $scope.step1();
        } else {
            $scope.showStep1 = false;
            $scope.showStep2 = false;
            $scope.showStep3 = false;
            $scope.showStep4 = false;
            $scope.showStep5 = false;
            $scope.showStep6 = false;
            $scope.showStep7 = false;
            $scope.showStep8 = false;
            $scope.showStep0 = false;
            // for contact no
            $scope.showStep01 = true;


        }
    };

    $scope.step1 = function () {

        if ($scope.user.gender && $scope.user.gender != null && $scope.user.gender != undefined && $scope.user.gender != "") {
            $scope.step2();
        } else {
            $scope.showStep0 = false;
            $scope.showStep01 = false;
            $scope.showStep2 = false;
            $scope.showStep3 = false;
            $scope.showStep4 = false;
            $scope.showStep5 = false;
            $scope.showStep6 = false;
            $scope.showStep7 = false;
            $scope.showStep8 = false;
            $scope.showStep1 = true;

        }
    };

    $scope.step2 = function () {

        $scope.prepareDateFormat();
        if ($scope.user.dob) {
            $scope.step3();
        } else {
            $scope.showStep0 = false;
            $scope.showStep01 = false;
            $scope.showStep1 = false;
            $scope.showStep3 = false;
            $scope.showStep4 = false;
            $scope.showStep5 = false;
            $scope.showStep6 = false;
            $scope.showStep7 = false;
            $scope.showStep8 = false;
            $scope.showStep2 = true;
        }
    };

    $scope.step3 = function () {
        var flag = false;
        if ($scope.user.grade) {
            if ($scope.user.grade != "") {
                $scope.step4();
            } else {
                flag = true;
            }
        } else {
            flag = true;
        }
        if (flag) {
            $scope.showStep0 = false;
            $scope.showStep01 = false;
            $scope.showStep1 = false;
            $scope.showStep2 = false;
            $scope.showStep4 = false;
            $scope.showStep5 = false;
            $scope.showStep6 = false;
            $scope.showStep7 = false;
            $scope.showStep8 = false;
            $scope.showStep3 = true;
        }
    };



    function validateAcademicPerformances(alert) {
        debugger
        var isValid = true;
        if ($scope.user.grade == '9' || $scope.user.grade == '10') {
            if ($scope.user.academicPerformances.length == 0) {
                var temp = {
                    type: "SUBJECT",
                    name: "SCIENCE",
                    value: ""
                };
                $scope.user.academicPerformances.push(angular.copy(temp));
                temp.name = "MATHEMATICS";
                $scope.user.academicPerformances.push(angular.copy(temp));
                isValid = false;
            } else {
                if (!$scope.user.academicPerformances[0].value || $scope.user.academicPerformances[0].value < 1 || $scope.user.academicPerformances[0].value > 100) {

                    if (alert) {
                        toaster.pop('error', 'Error', $scope.user.academicPerformances[0].name.charAt(0) + $scope.user.academicPerformances[0].name.slice(1).toLowerCase() + "'s Marks must be between 1 - 100")
                    }
                    isValid = false;
                }
                if (!$scope.user.academicPerformances[1].value || $scope.user.academicPerformances[1].value < 1 || $scope.user.academicPerformances[1].value > 100) {
                    if (alert) {
                        toaster.pop('error', 'Error', $scope.user.academicPerformances[1].name.charAt(0) + $scope.user.academicPerformances[1].name.slice(1).toLowerCase() + "'s Marks must be between 1 - 100")
                    }
                    isValid = false;
                }
                if ($scope.user.academicScore != null && ($scope.user.academicScore < 1 || $scope.user.academicScore > 100)) {
                    toaster.pop('error', 'Error', "Aggregate(all subjects)'s Marks must be between 1 - 100");
                    isValid = false;
                }

            }
        }
        return isValid;
    }

    $scope.step4 = function () {
        var flag = false;
        if ($scope.user.grade) {
            debugger
            validateAcademicPerformances(false);
            if ($scope.user.school) {
                if ($scope.user.city) {
                    $scope.step5();
                } else {
                    flag = true;
                }
            } else {
                flag = true;
            }
            if (flag) {
                $scope.showStep0 = false;
                $scope.showStep01 = false;
                $scope.showStep1 = false;
                $scope.showStep2 = false;
                $scope.showStep3 = false;
                $scope.showStep5 = false;
                $scope.showStep6 = false;
                $scope.showStep7 = false;
                $scope.showStep8 = false;
                $scope.showStep4 = true;
            }
        }
    };

    $scope.step5 = function () {
        var flag = false;
        if ($scope.user.school) {
            if ($scope.user.city) {
                if ($scope.user.academicScore) {
                    var isPreviousInputValid = validateAcademicPerformances(false);
                    if (isPreviousInputValid) {
                        flag = true;
                    }
                }
                if (flag) {
                    $scope.step6();
                } else {
                    $scope.showStep0 = false;
                    $scope.showStep01 = false;
                    $scope.showStep1 = false;
                    $scope.showStep2 = false;
                    $scope.showStep3 = false;
                    $scope.showStep4 = false;
                    $scope.showStep6 = false;
                    $scope.showStep7 = false;
                    $scope.showStep8 = false;
                    $scope.showStep5 = true;
                    $scope.user.academicScore = null;
                }
            } else {
                toaster.pop('info', 'Enter city');
            }
        } else {
            toaster.pop('info', 'Enter school name');
        }
    };

    $scope.step6 = function (academicScore) {

        var isPreviousInputValid = validateAcademicPerformances(true);

        if ($scope.user.academicScore) {
            if (isPreviousInputValid) {
                // $scope.step7();
                $scope.step09($scope.user.language)
            } else {
                $scope.step5();
            }
        } else {
            $scope.user.academicScore = academicScore;
            if ($scope.user.academicScore) {
                if (isPreviousInputValid) {
                    $scope.step09($scope.user.language);
                }
            } else {
                toaster.pop('info', 'Choose your academic score');
            }
        }
    };

    $scope.step7 = function () {
        // for language

        if ($scope.user.language) {
            $scope.closeCaptureProfile();
        } else {
            $scope.showStep0 = false;
            $scope.showStep01 = false;
            $scope.showStep1 = false;
            $scope.showStep2 = false;
            $scope.showStep3 = false;
            $scope.showStep4 = false;
            $scope.showStep5 = false;
            $scope.showStep6 = false;
            $scope.showStep8 = false;
            $scope.showStep7 = true;
        }

    };

    $scope.step8 = function () {

        // for interest
        // if($scope.user.language){
        var flag = false;
        if ($scope.user.interest) {
            if ($scope.user.interest.length > 1) {
                $scope.step9();
            } else {
                flag = true;
            }
        } else {
            flag = true;
        }
        if (flag) {
            $scope.user.interest = [];
            $scope.showStep0 = false;
            $scope.showStep01 = false;
            $scope.showStep1 = false;
            $scope.showStep2 = false;
            $scope.showStep3 = false;
            $scope.showStep4 = false;
            $scope.showStep5 = false;
            $scope.showStep6 = false;
            $scope.showStep7 = false;
            $scope.showStep8 = true;
        }
        // }else {
        //     toaster.pop('info', 'Choose a language');
        // }

    };

    $scope.step9 = function () {
        populateSelectedInterests();

        if ($scope.user.interest.length > 1) {
            $scope.closeCaptureProfile();
        } else {
            toaster.pop('info', "please choose at least 2 interests");
            $scope.disableFinishButton = true;

        }
    };
    $scope.step09 = function (language) {

        if (language) {
            $scope.user.language = language;
            if ($scope.user.ftlCount) {
                $scope.showStep0 = false;
                $scope.showStep01 = false;
                $scope.showStep1 = false;
                $scope.showStep2 = false;
                $scope.showStep3 = false;
                $scope.showStep4 = false;
                $scope.showStep5 = false;
                $scope.showStep6 = false;
                $scope.showStep8 = false;
                $scope.showStep7 = false;
                if (!$scope.user.email) {
                    $scope.captureProfileModal.close();

                    $scope.askForEmailModal = $uibModal.open({
                        templateUrl: 'views/modals/askForEmail.html',
                        size: 'sm',
                        scope: $scope,
                        backdrop: 'static',
                        keyboard: false
                    });
                } else {
                    $scope.showStep9 = true;
                }
            } else {
                $scope.closeCaptureProfile();
            }

        } else {
            // toaster.pop('info', 'Choose a language');
            $scope.step09('English');

        }

    };
    $scope.step10 = function () {
        $scope.user.ftlCount = false;
        $scope.showStep9 = false;


        $scope.closeCaptureProfile();

    };

    $scope.backToPreviousOption = function (fieldName) {
        $scope.showStep0 = false;
        $scope.showStep01 = false;
        $scope.showStep1 = false;
        $scope.showStep2 = false;
        $scope.showStep3 = false;
        $scope.showStep4 = false;
        $scope.showStep5 = false;
        $scope.showStep6 = false;
        $scope.showStep7 = false;
        $scope.showStep8 = false;
        $scope[fieldName] = true;

    };

    $scope.getAllAvailableTest = function () {
        $http.get('/api/test/listAvailableTests').then(function (response) {


            $scope.products = response.data;
            if ($scope.products.length) {
                $scope.show404page = false;
                // $scope.getLogoForAllAvailableTest();
            }
            else {
                if ($scope.user.premium || $scope.user.premium == null) {
                    // $scope.show404page = true;
                    if ($scope.user.interest && $scope.user.interest.length) {
                        $state.go("app.userTimeline");
                        isUserThreeSixtyEligible();
                    } else {
                        if ($scope.orgName == 'assess' || $scope.orgName == 'localhost') {

                            $scope.captureProfileModal = $uibModal.open({
                                templateUrl: 'views/modals/profileCapture.html',
                                size: 'lg',
                                scope: $scope,
                                backdrop: 'static',
                                keyboard: false
                            });

                            $scope.step8();
                        }
                    }

                } else {
                    $scope.openBuyTestModal();
                }
            }

        }, function (response) {

            toaster.pop('error', "Error", response.data.message);
        });
    };

    $scope.openBuyTestModal = function () {
        $scope.buytest = $uibModal.open({
            templateUrl: 'views/modals/buytest.html',
            size: 'md',
            controller: function ($scope, $uibModalInstance) {
                $scope.ok = function () {
                    $uibModalInstance.close();
                };

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $state.go("app.userTimeline");
                };
            },
            scope: $scope,
            backdrop: 'static'
            // keyboard: false
        });
    };

    $scope.closeBuyTestModal = function () {
        $scope.buytest.close();
    };

    function isUserThreeSixtyEligible() {
        $http.get('/api/test/isUserThreeSixtyEligible/').then(
            function (response) {
                $scope.threeSixtyEligible = response.data;
                if ($scope.threeSixtyEligible) {
                    $scope.saveStakeholders([]);
                }
            }, function (response) {
                toaster("error", "Unable to get user is threesixty eligible or not.");
            }
        );
    }

    $scope.addStakeholder = function (stakeholder) {
        if (!validateStakeholder(stakeholder)) {
            return;
        }
        $scope.stakeholders.push(angular.copy(stakeholder));
        $scope.new = {};
        for (var i = 0; i < $scope.stakeholderRelations.length; i++) {
            var relation = $scope.stakeholderRelations[i];
            if (relation.value == stakeholder.relation) {
                $scope.stakeholderRelations.splice(i, 1);
                return;
            }
        }
    };

    function validateStakeholder(stakeholder) {
        if (!stakeholder.name) {
            toaster.pop("error", "Please add stakeholder name.");
            return false;
        }
        if (!stakeholder.email) {
            toaster.pop("error", "Please add stakeholder email.");
            return false;
        }
        if (!stakeholder.mobile) {
            toaster.pop("error", "Please add stakeholder mobile.");
            return false;
        }
        if (!stakeholder.relation) {
            toaster.pop("error", "Please add stakeholder relation.");
            return false;
        }
        return true;
    }

    $scope.saveStakeholders = function (stakeholders) {
        if (stakeholders) {
            if (stakeholders.length > 0) {
                $scope.saveStakeholdersBtn = true;
            }
            $http.post('/api/stakeholder/student/', $scope.stakeholders).then(
                function (response) {
                    $scope.saveStakeholdersBtn = false;
                    if (stakeholders.length > 0) {
                        toaster.pop("info", "Stakeholders add.");
                    }
                    $scope.stakeholderRelations = angular.copy($scope.allStakeholderRelations);
                    $scope.stakeholders = [];
                    for (var i = 0; i < response.data.length; i++) {
                        var stakeholder = response.data[i];
                        $scope.addStakeholder(stakeholder);
                    }
                    $scope.closeStakeholderModal();
                }, function (response) {
                    $scope.saveStakeholdersBtn = false;
                    toaster.pop("error", "Unable to save stakeholders.");
                }
            );
        }
    };

    $scope.getLogoForAllAvailableTest = function () {
        $scope.testCount = 0;
        $scope.innerTestCount = 0;
        for ($scope.testCount; $scope.testCount < $scope.products.length; $scope.testCount++) {
            $http({
                method: 'GET',
                url: '/api/raw/view/PROFILEIMAGE/' + $scope.products[$scope.testCount].logo,
                responseType: 'arraybuffer'
            }).then(function (response) {
                $scope.testImgData = btoa(String.fromCharCode.apply(null, new Uint8Array(response.data)));
                if ($scope.testImgData) {
                    $scope.testImageURI = "data:image/PNG;base64," + $scope.testImgData;
                    $scope.products[$scope.innerTestCount].logo = $scope.testImageURI;
                }
                else {
                    $scope.products[$scope.innerTestCount].logo = "/assets/img/default-test-picture.jpeg";
                }
                $scope.innerTestCount++;
            }, function (response) {
                $scope.products[$scope.innerTestCount].logo = "/assets/img/default-test-picture.jpeg";
                $scope.innerTestCount++;
                toaster.pop('error', "Failed to load test's image");
            });
        }
    };

    $scope.prepareDateFormat = function () {
        $scope.years = [];
        $scope.days = [];
        $scope.months = [];
        var current_year = new Date().getFullYear();
        for (var i = current_year - 10; i >= current_year - 68; i--) {
            $scope.years.push(i);
        }

        for (i = 1; i <= 31; i++) {
            $scope.days.push(i);
        }
        $scope.months.push("Jan");
        $scope.months.push("Feb");
        $scope.months.push("Mar");
        $scope.months.push("Apr");
        $scope.months.push("May");
        $scope.months.push("Jun");
        $scope.months.push("Jul");
        $scope.months.push("Aug");
        $scope.months.push("Sep");
        $scope.months.push("Oct");
        $scope.months.push("Nov");
        $scope.months.push("Dec");
    };

    $scope.setDate = function (selectedDay, selectedMonth, selectedYear) {
        if (selectedDay) {
            if (selectedMonth) {
                if (selectedYear) {
                    $scope.step3();
                    $scope.user.dob = new Date(selectedMonth + " " + selectedDay + ", " + selectedYear);
                    // var newDate = selectedDay + selectedMonth + selectedYear + "";
                    // $scope.user.dob = DateFormatService.formatDate6(newDate);
                    // $scope.user.dob = moment($scope.user.dob).toDate();
                } else {
                    toaster.pop('info', 'select year');
                }
            } else {
                toaster.pop('info', 'select month');
            }
        } else {
            toaster.pop('info', 'select day');
        }
    };

    function getAllParentInterests() {
        $http.get('/api/md/allParentInterests').then(function (response) {
            $scope.parentInterests = response.data;
            for (var x in $scope.parentInterests) {
                $scope.parentInterests[x].childInterests = getChildIntrestsByParent(x);
            }
        });
    }

    function getChildIntrestsByParent(x) {
        parentId = $scope.parentInterests[x].id;
        $scope.showAddMore = false;
        $http.get('/api/md/allChildInterestByParent/' + parentId).then(function (response) {
            $scope.parentInterests[x].childInterests = response.data;
            for (var i in $scope.parentInterests[x].childInterests) {
                for (var j in $scope.selectedInterests) {
                    if ($scope.parentInterests[x].childInterests[i].id == $scope.selectedInterests[j].id) {
                        $scope.parentInterests[x].childInterests[i].selected = true;
                    }
                }
            }
            for (var k in $scope.parentInterests[x].childInterests) {
                if ($scope.parentInterests[x].childInterests[i].selected != true) {
                    $scope.parentInterests[x].childInterests[i].selected = false;
                }
            }

        }, function (response) {
        });
    }

    /**************** Capturing Intreset Start******************/
    $scope.selectInterest = function (interest) {
        if (interest.selected) {
            if ($scope.selectedInterests.length < 5) {
                $scope.selectedInterests.push(interest);
            } else {
                error("You can't select more than 5 interests");
            }
        } else {
            for (var i in $scope.selectedInterests) {
                if ($scope.selectedInterests[i].id == interest.id) {
                    $scope.selectedInterests.splice(i, 1);
                }
            }
        }
    };

    $scope.addMoreInterest = function () {
        $scope.childInterests = [];
        $scope.showAddMore = true;
    };

    $scope.selectCustomInterest = function (customInterestName) {
        if (customInterestName) {
            var customInt = {};
            customInt.id = Math.floor(Math.random() * 1000);
            customInt.name = customInterestName;
            customInt.image = "default-interest.png";
            customInt.selected = false;
            $scope.selectInterest(customInt);
        } else {
            toaster.pop('Info', "Interest can not be blank");
        }
    };

    function populateSelectedInterests() {

        for (var i in $scope.selectedInterests) {
            $scope.user.interest.push($scope.selectedInterests[i].name);
        }
    }

    getAllParentInterests();

    /**************** Capturing Intreset Ends******************/
    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function updateUserInfo() {

        $http.put('/api/user/self', $scope.user).then(function (response) {
            // toaster.pop('success', "user updated successfully");
            $scope.askForEmail();
        }, function (response) {
            toaster.pop('error', "Error", "Failed to update profile");
            $scope.askForEmail();
        });
    }

    $scope.scrollToTop = function () {
        $window.scrollTo(0, 0);
    };

    $scope.askForEmail = function () {

        if (!$scope.user.email) {
            $scope.askForEmailModal = $uibModal.open({
                templateUrl: 'views/modals/askForEmail.html',
                size: 'sm',
                scope: $scope,
                backdrop: 'static',
                keyboard: false
            });
        } else {
            $scope.getAllAvailableTest();
        }
    };

    $scope.closeAskForEmail = function (email) {
        saveEmail();
    };

    $scope.openStakeholderModel = function () {
        $scope.stakeholderModal = $uibModal.open({
            templateUrl: 'views/modals/stakeholder_modal.html',
            size: 'lg',
            scope: $scope,
            backdrop: 'backdrop'
        });
    };

    $scope.closeStakeholderModal = function () {
        $scope.stakeholderModal.close();
    };

    $scope.loader = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelLoader = function () {
        $scope.modalInstance.close();
    };

    function getLanguages() {

        var classes = ["btn-default", "btn-warning", "btn-primary"];
        $http.get('/api/md/languages/').then(function (response) {
            $scope.languages = response.data;

            $scope.languages.sort(function (a, b) {
                return (a.value.charAt(0) < b.value.charAt(0)) ? -1 : (a.value.charAt(0) > b.value.charAt(0)) ? 1 : 0;
            });

            for (var i in $scope.languages) {
                $scope.languages[i].class = classes[i];
            }
        });
    }

    function saveEmail() {
        $http.put('/api/user/self/saveEmail', $scope.user.email).then(function (response) {
            $scope.askForEmailModal.close();
            // toaster.pop('success', "email updated successfully");

            $scope.captureProfileModal = $uibModal.open({
                templateUrl: 'views/modals/profileCapture.html',
                size: 'lg',
                scope: $scope,
                backdrop: 'static',
                keyboard: false
            });
            $scope.showStep5 = false;
            $scope.showStep9 = true;
            $state.reload();
        }, function (response) {
            toaster.pop('error', response.data.error);
            $state.reload();
        });
    }

    doIneedToCaptureProfile();

});

