app.filter('pagination', function () {
    return function (input, start) {
        start = +start;
        if (input instanceof Array)
            return input.slice(start);
    };
});
app.controller('subjectTeacherManagement', function ($scope, $http, toaster, $uibModal, $state) {
    $scope.curPage = 0;
    $scope.pageSize = 10;
    $scope.TotalRecordCount = 0;
    $scope.goToPage = 1;
    $scope.teachers = {};
    $scope.subjectTeacher = {};
    $scope.subjectTeacher.gradeSectionSubjects = [];
    $scope.listOfTeachers = {};
    $scope.newSubjectTeacher = {};

    $scope.setCurrentPage = function () {
        $scope.curPage = 0;
    };

    $scope.numberOfPages = function () {
        $scope.pages = [];
        $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
        for (var i = 1; i <= $scope.totalPages; i++) {
            $scope.pages.push(i);
        }
        return $scope.totalPages;
    };

    $http.get('/api/teacher/subjectTeacher').then(function (response) {
        $scope.teachers = response.data;
    });

    $http.get('/api/md/grades/').then(function (response) {
        $scope.grades = response.data
    });

    $scope.subjectByGrade = function (grade) {
        $http.get('/api/test/subjects?grade=' + grade).then(function (response) {
            $scope.subjects = response.data;
        });
    };

    $http.get('/api/md/sections/').then(function (response) {
        $scope.sections = response.data
    });

    $scope.list = function () {
        $http.get('/api/teacher/subjectTeacherList').then(function (response) {
            $scope.listOfTeachers = response.data;
            $scope.TotalRecordCount = response.data.length;
        }, function (response) {
            toaster.pop('Info', response.error);
        });
    };

    $scope.list();

    $scope.openTeacherModal = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'createTeacher.html',
            size: 'lg',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancel = function () {
        $scope.modalInstance.close();
    };

    $scope.createNew = function () {
        $scope.subjectTeacher = {};
        $scope.subjectTeacher.gradeSectionSubjects = [];
        $scope.openTeacherModal();
    };
    $scope.create = function (subjectTeacher) {

        if (subjectTeacher.id) {
            $http.put('/api/teacher/subjectTeacher', subjectTeacher).then(function (response) {
                $scope.list();
                $scope.cancel();
                ok('Record successfully updated');
            }, function (response) {
                error(response.data.error);
            });
        } else {
            $http.post('/api/teacher/subjectTeacher', subjectTeacher).then(function (response) {
                $scope.list();
                $scope.cancel();
                ok('Record successfully created');
            }, function (response) {
                error(response.data.error);
            });
        }
    };

    $scope.update = function (subjectTeacher) {
        if(angular.equals(subjectTeacher, {})){
            return;
        }
        $scope.subjectTeacher = subjectTeacher;
        $scope.subjectTeacher.teacher = JSON.stringify(subjectTeacher.id);
        $scope.subjectTeacher.subject = JSON.stringify(subjectTeacher.subject);
        $scope.subjectTeacher.gradeSectionSubjects =subjectTeacher.gradeSectionSubjects;
        $scope.openTeacherModal();
    };

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.deleteRecord = function (id) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.value) {
                $http.delete('/api/teacher/deleteSubjectTeacher?id=' + id).then(function (response) {
                        $scope.list();
                        ok('Record deleted successfully');
                    },
                    function (response) {
                        error(response.data.error);
                    });
            }
        });
    };

    $scope.addGradeSectionSubject = function (item) {
        if(angular.equals(item, {})){
            return;
        }
        var flag = true;
        for(var i in $scope.subjectTeacher.gradeSectionSubjects){
            if(item.grade == $scope.subjectTeacher.gradeSectionSubjects[i].grade){
                if(item.section == $scope.subjectTeacher.gradeSectionSubjects[i].section){
                    if(item.subject == $scope.subjectTeacher.gradeSectionSubjects[i].subject){
                        flag = false;
                    }
                }
            }
        }
        if(flag){
            $scope.subjectTeacher.gradeSectionSubjects.push(angular.copy(item));
        }
    };

    $scope.removeGradeSectionSubject = function (index) {
        $scope.subjectTeacher.gradeSectionSubjects.splice(index, 1);
    }
});