app.controller('DeriveCategoryController', function ($scope, $http, $state, $uibModal, toaster) {

    $scope.alltests = {};
    $scope.selectedQuestions = [];
    $scope.selectedCategories = [];
    $scope.selectedSubCategories=[];
    $scope.testQuestions = [];
    $scope.testQuestionsWithCategoriesList = [];

    $scope.emptyTestQuestionObject = function () {
        $scope.testQuestion = {
            test: "",
            question: "",
            subCategory:[],
            category: []
        };
    };

    $scope.emptyTestQuestionObject();

    $scope.getAllTests = function () {
        $http.get('api/test/allTestWithAllVersions').then(function (response) {
            $scope.alltests = response.data;
        });
    };

    $http.get('api/category/parent').then(function (response) {
        $scope.categories = response.data;
    });

    $http.get('api/category/child').then(function (response) {
        $scope.childCategories = response.data;
    });

    $scope.getAllTests();

    $scope.getTestQuestions = function (id) {
        $http.get('api/test/deriveCategories/' + id).then(function (response) {
            $scope.test = response.data;
            $scope.remainingQuestionsForAssigningCategory = $scope.test.questions;
            for (countQuestion = 0; countQuestion < $scope.remainingQuestionsForAssigningCategory.length; countQuestion++) {
                $scope.remainingQuestionsForAssigningCategory[countQuestion].question.selected = false;
            }
        });
    };

    $scope.isQuestionSelected = function (question) {
        if (question.selected) {
            question.selected = true;
            $scope.selectedQuestions.push(question);
        } else {
            if (!question.selected) {
                for (var i = 0; i < $scope.selectedQuestions.length; i++) {
                    if ($scope.selectedQuestions[i].id == question.id) {
                        $scope.selectedQuestions.splice(i, 1);
                    }
                }
            }
        }
    };

    $scope.filteredChildCategoryByParentName = function (selectedCategory) {
        $http.get('api/category/childByParentName?parentName=' + selectedCategory.name).then(function (response) {
            $scope.childCategories = response.data;
        });
    };

    $scope.openAssignCategoryModal = function () {
            $scope.assignCategoryModalInstance = $uibModal.open({
            templateUrl: 'views/assignCategory.html',
            size: 'lg',
            scope: $scope,
            backdrop: 'static'
        });
      };

    $scope.closeAssignCategoryModal = function () {
        $scope.selectedQuestions = [];
        $scope.assignCategoryModalInstance.close();
    };

    $scope.removeCategory = function (parentIndex,index) {
      if ($scope.selectedQuestions[parentIndex].category) {
          $scope.selectedQuestions[parentIndex].category.splice(index, 1)
        }
      };

    $scope.addCategory = function (selectedCategory) {
       $scope.selectedQuestions.forEach(function(e){
          if(e.category){
          var alreadyThere = false;
          for (var i = 0; i < e.category.length; i++) {
              if (e.category[i].name == selectedCategory.name) {
                  alreadyThere = true;
              }
          }
          if (!alreadyThere) {
              e.category.push(selectedCategory);
          }
          }
       });
    };
    $scope.addSubCategory = function (selectedSubCategory) {
         $scope.selectedQuestions.forEach(function(e){
                   if(e.subcategory){
                   var alreadyThere = false;
                   for (var i = 0; i < e.subcategory.length; i++) {
                       if (e.subcategory[i].name == selectedSubCategory.name) {
                           alreadyThere = true;
                       }
                   }
                   if (!alreadyThere) {
                       e.subcategory.push(selectedSubCategory);
                   }
                   }
                });
        };
    $scope.removeSubCategory = function (parentIndex,index) {
            if ($scope.selectedQuestions[parentIndex].subcategory) {
                $scope.selectedQuestions[parentIndex].subcategory.splice(index, 1)
            }
        };
    $scope.saveTestQuestionWithCategories = function () {
        $scope.attachTestQuestionsWithCategory();

        if($scope.testQuestions.length>0){
            $http.put('api/test/saveTestQuestionWithCategory', $scope.testQuestions).then(function (response) {
                $scope.testQuestionsWithCategories = response.data;
                toaster.pop("success", "Success");
                $scope.populateTestQuestionsWithCategoriesList();
                $scope.populateRemainingQuestionsForAssigningCategory();
                $scope.closeAssignCategoryModal();
                $scope.selectedQuestions = [];
                $scope.testQuestions = [];
                $scope.selectedCategories = [];
            }, function (response) {
                toaster.pop("error", "Error", "error in assigning categories");
            });
        }else {
            error("Questions are not selected");
        }

    };

    $scope.attachTestQuestionsWithCategory = function () {
        angular.forEach($scope.selectedQuestions, function (selectedQuestion, key) {
            $scope.emptyTestQuestionObject();
            $scope.testQuestion.question = selectedQuestion.id;
            $scope.testQuestion.test = $scope.test.id;
            $scope.testQuestion.category=selectedQuestion.category;
            $scope.testQuestion.subCategory=selectedQuestion.subcategory;
            $scope.testQuestions.push($scope.testQuestion)
        });
    };

    $scope.populateTestQuestionsWithCategoriesList = function () {
        angular.forEach($scope.testQuestionsWithCategories, function (testQuestionWithCategories, key) {
            if (testQuestionWithCategories) {
                var alreadyThere = false;
                for (var i = 0; i < $scope.testQuestionsWithCategoriesList.length; i++) {
                    if ($scope.testQuestionsWithCategoriesList[i].question.id == testQuestionWithCategories.question.id) {
                        alreadyThere = true;
                    }
                }
                if (!alreadyThere) {
                    $scope.testQuestionsWithCategoriesList.push(testQuestionWithCategories);
                }
            }
        });
    };

    $scope.populateRemainingQuestionsForAssigningCategory = function () {
        angular.forEach($scope.testQuestionsWithCategoriesList, function (testQuestionWithCategory, key) {
            if (testQuestionWithCategory) {
                for (var i = 0; i < $scope.remainingQuestionsForAssigningCategory.length; i++) {
                    if ($scope.remainingQuestionsForAssigningCategory[i].question.id == testQuestionWithCategory.question.id) {
                        $scope.remainingQuestionsForAssigningCategory.splice(i, 1);
                    }
                }
            }
        });
    };

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    };
});