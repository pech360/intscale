app.controller('ManageBlogsController', function ($scope, $state, $http) {
    $scope.dataLoading = false;

    $scope.newBlog = function () {
        $scope.workingBlog = {};
    };

    $scope.updateBlog = function (blog) {
        $scope.workingBlog = angular.copy(blog);
        $scope.workingBlog.interestsIds = [];
        $scope.workingBlog.interestsName = [];
        for (var i = 0; i < $scope.workingBlog.interests.length; i++) {
            $scope.workingBlog.interestsIds.push($scope.workingBlog.interests[i].id);
            $scope.workingBlog.interestsName.push($scope.workingBlog.interests[i].name);
        }
    };

    $scope.saveBlog = function () {
        if (!$scope.workingBlog.name) {
            toaster.pop('Error', "Blog Name cannot be empty ");
            return;
        }
        if (!$scope.workingBlog.url) {
            toaster.pop('Error', "Blog url cannot be empty ");
            return;
        }

        // for (var i = 0; i < $scope.workingBlog.interests.length; i++) {
        //     $scope.workingBlog.interestsIds.push($scope.workingBlog.interests[i].id);
        // }

        $scope.dataLoading = true;

        if ($scope.workingBlog.id) {
            $http.put('/api/md/updateBlog', $scope.workingBlog).then(function (response) {
                $scope.dataLoading = false;
                ok("Blog updated successfully");
                refresh();

            }, function (response) {
                $scope.dataLoading = false;
                error(response.data.error);
            });
        } else {
            $http.post('/api/md/createBlog', $scope.workingBlog).then(function (response) {
                $scope.dataLoading = false;
                ok("Blog created successfully");
                refresh();
            }, function (response) {
                $scope.dataLoading = false;
                error(response.data.error);
            });
        }
    };
    $scope.check = function () {
        var flag = true;
        for(i = 0; i < $scope.blogs.length; i++) {
            flag = false;
            break;
        }
        $scope.all = flag;
    };

    $scope.remove = function () {
        var toDelete = [];
        for (i = 0; i < $scope.blogs.length; i++) {
            if ($scope.blogs[i].checked) {
                toDelete.push($scope.blogs[i].id);
            }
        }

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Delete!'
        }).then(function (result) {
            if (result.value) {
                deleteBlogs(toDelete);
            }
        });
    };

    function deleteBlogs(list) {
        $http.post('/api/md/deleteBlogs/all', list).then(
            function (response) {
                refresh();
            }, function (response) {
                error(response.data.error)
            }
        );
    }

    $scope.selectAllBlogs = function (all) {
        angular.forEach($scope.blogs, function (b) {
            b.checked = all;
        });
    };

    function refresh() {
        $scope.workingBlog = {};
        $scope.workingBlog.interests = [];

        $http.get('/api/md/allBlogs').then(function (response) {
            $scope.blogs = response.data;
            angular.forEach($scope.blogs, function (b) {
                b.checked = false;
            });
        });

        $http.get('/api/md/allChildInterests').then(function (response) {
            $scope.interests = response.data;
        });
    }

    $scope.addInterest = function (interest) {
        if ($scope.workingBlog) {
            var alreadyThere = false;
            for (var i = 0; i < $scope.workingBlog.interests.length; i++) {
                if ($scope.workingBlog.interests[i] == interest) {
                    alreadyThere = true;
                }
            }
            if (!alreadyThere) {
               $scope.workingBlog.interests.push(interest);
            }
        }
    };

    $scope.removeInterest = function (index) {
        if ($scope.workingBlog) {
            $scope.workingBlog.interests.splice(index, 1)
        }
    };


    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    refresh();
});