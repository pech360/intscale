app.controller('roleManagement', function ($scope, $http) {

    $scope.selectedPrivilege = '';
    $scope.showRole = function (role) {
        $scope.workingRole = angular.copy(role);
        $scope.workingRole.mode = "Show";
        $scope.editUserPanel = true;
    };

    $scope.removePrivilege = function (index) {
        if ($scope.workingRole) {
            $scope.workingRole.privileges.splice(index, 1)
        }
    };

    $scope.addPrivilege = function (selectedPrivilege) {
        if ($scope.workingRole) {
            var alreadyThere = false;
            for (var i = 0; i < $scope.workingRole.length; i++) {
                if ($scope.workingRole.privileges[i].name == selectedPrivilege.name) {
                    alreadyThere = true;
                }
            }
            if (!alreadyThere) {
                $scope.workingRole.privileges.push(selectedPrivilege);
            }
        }
    };

    $scope.addDashboard = function (dashboard) {
        if ($scope.workingRole) {
            var alreadyThere = false;
            for (var i = 0; i < $scope.workingRole.dashboards.length; i++) {
                if ($scope.workingRole.dashboards[i] == dashboard.name) {
                    alreadyThere = true;
                }
            }
            if (!alreadyThere) {
                $scope.workingRole.dashboards.push(dashboard.name);
            }
        }
    };

    $scope.removeDashboard = function (index) {
        if ($scope.workingRole) {
            $scope.workingRole.dashboards.splice(index, 1)
        }
    };

    $scope.cancelRole = function () {
        $scope.workingUser = {};
        $scope.showUserPanel = false;
        $scope.editUserPanel = false;
    };

    $scope.saveRole = function (role) {
        if (role.id) {
            $http.put('/api/user/self/role/', role).then(function (response) {
                refresh();
                ok("Role updated!");
            }, function (response) {
                error(response.data.error);
            });
        } else {
            $http.post('/api/user/self/role/', role).then(function (response) {
                refresh();
                ok("Role created!");
            }, function (response) {
                error(response.data.error);
            });
        }
    };

    $scope.deleteRole = function (role) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Delete!'
        }).then(function (result) {
            if (result.value) {
                $http.delete('/api/user/self/role?roleId=' + role.id).then(function (response) {
                    refresh();
                    ok("Delete OK!")
                }, function (response) {
                    error(response.data.error);
                });
            }
        })
    };

    $scope.newRole = function () {
        $scope.workingRole = {
            mode: 'New',
            privileges: [],
            dashboards:[]
        };
        $scope.editUserPanel = true;
    };

    function refresh() {
        $scope.workingRole = {
            mode: 'New',
            privileges: [],
            dashboards:[]
        };

        $http.get('/api/role/allRoles').then(function (response) {
            $scope.roles = response.data;
        });

        $http.get('/api/role/privileges').then(function (response) {
            $scope.privileges = response.data;
        });
    }

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function getAllDashboards() {
        $http.get('api/dash/all/').then(function (response) {
            $scope.dashboards = response.data;
        })
    }

    refresh();
    getAllDashboards();

});