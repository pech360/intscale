app.filter('pagination', function()
{
return function(input, start)
{
 start = +start;
 if(input instanceof Array)
 return input.slice(start);
};
});
app.controller('tenantTestAllotmentController',function($scope,$http,CommonService,toaster,$uibModal,DateFormatService){

  $scope.curPage = 0;
  $scope.pageSize = 10;
  $scope.goToPage = 1;
  $scope.TotalRecordCount=0;
  var countUser=0;
  var countTest=0;
  $scope.userAssignArray=[];
  $scope.testAssignArray=[];
  $scope.userTestAssignment={};
  $scope.updateUserTest={};
    $scope.userAvlTests=[];
    $scope.testFlag=false;
    $scope.addTest=false;
    $scope.availableTest=[];
    $scope.testCount=0;
    $scope.testNames=[];
    $scope.email='';
    $scope.realCount=0;
    $scope.currentPilotName='';
    $scope.roleName='';

   $scope.numberOfPages = function() {
       $scope.pages = [];
       $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
       for (var i = 1; i <= $scope.totalPages; i++) {
           $scope.pages.push(i);
       }
       return $scope.totalPages;
   };

 /*$http.get('/api/user/self/tenants').then(function(response){
  $scope.listTenants=response.data;
 });*/

 /*$http.get('/api/role/roleSummary').then(function(response){
   $scope.listRoles=response.data;
  });*/

  $http.get('/api/pilot/oneByActive').then(function(response){
     $scope.currentPilotName=response.data.name;
    });

  CommonService.listPilots().then(function (response) {
      $scope.listPilots = response.data;
  });

  $scope.fetchUserList=function(){
   if(!$scope.roleName){
     toaster.pop('info','please select assigner type')
   }
   $http.get('/api/test/allotTestListByuser?roleName='+$scope.roleName).then(function(response){
   $scope.listUsers=response.data;
   $scope.TotalUserCount=response.data.length;
   $scope.TotalRecordCount=response.data.length;
   var x=0;
   $scope.listUsers.forEach(function(element){
       $scope.listUsers[x].selectUser = false;
       x=x+1;
      });
   });
  };

  $scope.listAllPublishTest=function(){
  $http.get('/api/product/').then(function(response){
    $scope.listTests=response.data;
    $scope.TotalTestCount=response.data.length;
    for (countTest=0; countTest<$scope.TotalTestCount; countTest++) {
                $scope.listTests[countTest].selectTest = false;
              }
  });
  };

  $scope.listAllPublishTest();

  $scope.isSelectAllUser = function () {
      if ($scope.selectAllUsers) {
          $scope.userAssignArray=[];
          for (countUser=0; countUser<$scope.TotalUserCount; countUser++) {
          $scope.listUsers[countUser].selectUser = true;
          $scope.userAssignArray.push($scope.listUsers[countUser].userId);
         }
      }else{
          for (countUser=0; countUser<$scope.TotalUserCount; countUser++) {
          $scope.listUsers[countUser].selectUser = false;
         }
         $scope.userAssignArray=[];
      }
  };

  $scope.isUserSelected = function (user) {
      if (user.selectUser){
       $scope.userAssignArray.push(user.userId);
      }else{
      var i=0;
      $scope.userAssignArray.forEach(function(element){
      if(element==user.userId){
      $scope.userAssignArray.splice(i, 1);
      }
      i++;
      });
      i=0;
      }
  };

   $scope.isSelectAllTest = function () {
        if ($scope.selectAllTests) {
            $scope.testAssignArray=[];
            for (countTest=0; countTest<$scope.TotalTestCount; countTest++) {
            $scope.listTests[countTest].selectTest = true;

            $scope.testAssignArray.push($scope.listTests[countTest].id);
           }
        }else{
            for (countTest=0; countTest<$scope.TotalTestCount; countTest++) {
            $scope.listTests[countTest].selectTest = false;
           }
           $scope.testAssignArray=[];
        }
    };

  $scope.isTestSelected=function(test){
        if (test.selectTest){
          $scope.testAssignArray.push(test.id);
         }else{
         var i=0;
         $scope.testAssignArray.forEach(function(element){
         if(element==test.id){
         $scope.testAssignArray.splice(i, 1);
         }
         i++;
         });
         i=0;
         }
  };

  $scope.saveUserTestAssignment=function(userTestAssignment){
    if($scope.userAssignArray.length<1){
      toaster.pop('Info',"User is not selected");
       return;
    }
    $scope.userTestAssignment.userIds=$scope.userAssignArray;
    $scope.userTestAssignment.testIds=$scope.testAssignArray;
    //$scope.userTestAssignment.validTillDate=DateFormatService.formatDate4($scope.userTestAssignment.validTillDate);
    $http.put('api/test/tenantTestAllotment',userTestAssignment).then(function(response){
         if(response.data){
           ok("Test Assign successfully");
           $scope.selectAllUsers=false;
           $scope.selectAllTests=false;
           $scope.listAllPublishTest();
           $scope.userAssignArray=[];
           $scope.testAssignArray=[];
           $scope.fetchUserList();
           $scope.userTestAssignment={};
         }else{
           error(response.data.error);
         }
      }, function (response) {
          error(response.data.error);
      });
  };

  function ok(message) {
          swal({
              title: message,
              type: 'success',
              buttonsStyling: false,
              confirmButtonClass: "btn btn-warning"
          });
      };
  function error(message) {
      swal({
          title: message,
          type: 'error',
          buttonsStyling: false,
          confirmButtonClass: "btn btn-warning"
      });
  };

    $scope.viewAssignedTest=function(){
         $scope.modalInstance = $uibModal.open({
                  templateUrl: 'adminTestView.html',
                  size: 'sm',
                  scope: $scope
              });
         };
    $scope.cancel = function() {
         $scope.modalInstance.close();
     };

    $scope.manageAssignment=function(user){
      if(user.tests.length<1){
        toaster.pop('Info',"Test list is empty");
        return;
        }
      $scope.addTest=false;
      $scope.testFlag=false;
      $scope.userAvlTests=user.products;
      $scope.email=user.email;
      $scope.availableTest=[];
      $scope.testCount=user.testCount;
      $scope.pilotName=user.pilotName;
      //$scope.validTillDate = new Date(user.validTillDate);
      $scope.realCount=user.testCount;
      $scope.listTests.forEach(function(e){
        $scope.availableTest.push(e);
      });
      //$scope.availableTest=$scope.adminTestList;
      $scope.viewAssignedTest();
    };

    $scope.removeUserTest=function(i){
     $scope.userAvlTests.splice(i, 1);
     $scope.testFlag=true;
    };
    $scope.removeAllUserTest=function(){
     $scope.userAvlTests=[];
     $scope.testFlag=true;
    };

    var alreadyExits=false;
    $scope.addUserTest=function(i){
     $scope.userAvlTests.forEach(function(e){
      if($scope.availableTest[i].id==e.id){
         alreadyExits=true;
      }
     });
     if(!alreadyExits){
      $scope.userAvlTests.push($scope.availableTest[i]);
     }
     alreadyExits=false;
     $scope.availableTest.splice(i, 1);
    };

    $scope.showTest=function(){if($scope.addTest){$scope.addTest=false;}else{$scope.addTest=true;}};

    $scope.autoUpdate=function(){$scope.testFlag=true;};

     $scope.updateAssignTest=function(testCount){
      if(testCount<$scope.realCount){
        toaster.pop('Info',"TestCount can't be smaller from previous count");
        return
      }
      $scope.productIds=[];
      $scope.updateUserTest.email=$scope.email;
      $scope.updateUserTest.numberOfAttempt=testCount;
      $scope.userAvlTests.forEach(function(e){
       $scope.productIds.push(e.id);
      });
      $scope.updateUserTest.testIds=$scope.productIds;
      //$scope.updateUserTest.pilotName=pilotName;
      //$scope.updateUserTest.validTillDate=DateFormatService.formatDate4(validTillDate);
      $http.put('api/test/tenantTestUpdate',$scope.updateUserTest).then(function(response){
         if(response.data){
            $scope.cancel();
            ok("Test Update successfully");
            $scope.selectAllUsers=false;
            $scope.selectAllTests=false;
            $scope.listAllPublishTest();
            $scope.userAssignArray=[];
            $scope.testAssignArray=[];
            $scope.fetchUserList();
            $scope.userTestAssignment={};
          }else{
            error(response.data.error);
          }
      },function (response) {
                  error(response.data.error);
              });
    };
});