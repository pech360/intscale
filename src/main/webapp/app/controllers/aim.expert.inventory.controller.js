app.controller('AimExpertInventoryController', function ($http, $scope, toaster, $uibModal, DateFormatService) {

    $scope.type = '';
    $scope.types = ['career', 'lesiure', 'subject'];
    $scope.new = {};
    $scope.new.aimExpertItems = [];
    $scope.new.leisures = [];
    $scope.dryrun = true;
    $scope.showDisplayResponse = false;
    var records = {};

    $scope.loader = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelLoader = function () {
        $scope.modalInstance.close();
    };

    $scope.openUserProfileModal = function () {
        fetchProfile();

        $scope.userProfileModal = $uibModal.open({
            templateUrl: 'views/modals/userProfileModal.html',
            size: 'lg',
            scope: $scope,
            backdrop: ''
        });
    };

    function fetchProfile() {
        $http.get('/api/user/profile').then(function (response) {
            $scope.user = response.data;
            $scope.user.dob = DateFormatService.formatDate(new Date(response.data.dob));
        });
    }

    function loadReportSections() {
        $http.get('/api/reports/getReportSectionWithCategory').then(function (response) {
            var categories = [];
            $scope.reportSections = response.data;
            for (var i in $scope.reportSections) {
                categories = [];
                for (var j in $scope.reportSections[i].categories) {
                    $scope.reportSections[i].categories[j].reportSectionName = $scope.reportSections[i].name;
                    $scope.reportSections[i].categories[j].goalStandard = 0;

                    if ($scope.reportSections[i].categories[j].type) {
                        if ($scope.reportSections[i].categories[j].type != 'secondary') {
                            categories.push($scope.reportSections[i].categories[j]);
                        }
                    } else {
                        categories.push($scope.reportSections[i].categories[j]);
                    }
                }
                $scope.reportSections[i].categories = categories;
            }
        }, function (response) {
            alert(response.data.error);

        })

    }


    function validateItem(item) {
        if (item) {
            if (item.reportSection) {
                if (item.category) {
                    if (item.goalStandard) {
                        return true
                    } else {
                        toaster.pop("info", "goalStandard can't be blank");
                    }
                } else {
                    toaster.pop("info", "category can't be blank");
                }
            } else {
                toaster.pop("info", "reportSection can't be blank");
            }
        } else {
            toaster.pop("info", "blank input");
        }
        return false;
    }

    $scope.save = function (item) {
        if (item) {
            $http.post('/api/aimExpert/aimExpertInventory', item).then(function (response) {
                // $scope.aimExpertInventories = response.data;
                toaster.pop("success", "success");
            }, function (response) {
                alert(response.error);
            })
        } else {
            toaster.pop("info", "blank input");
        }

    };

    $scope.create = function (type) {

        $scope.new.aimExpertItems = [];
        $scope.new.leisures = [];

        $scope.new.type = type;
        var temp = {
            category: "",
            reportSection: "",
            goalStandard: 0
        };

        for (var i in $scope.reportSections) {
            for (var j in $scope.reportSections[i].categories) {
                if ($scope.reportSections[i].categories[j].type) {
                    if ($scope.reportSections[i].categories[j].type != 'secondary') {
                        temp.category = $scope.reportSections[i].categories[j].name;
                        temp.reportSection = $scope.reportSections[i].categories[j].reportSectionName;
                        temp.goalStandard = $scope.reportSections[i].categories[j].goalStandard;
                    }
                } else {
                    temp.category = $scope.reportSections[i].categories[j].name;
                    temp.reportSection = $scope.reportSections[i].categories[j].reportSectionName;
                    temp.goalStandard = $scope.reportSections[i].categories[j].goalStandard;
                }
                $scope.new.aimExpertItems.push(angular.copy(temp));
            }
        }

        $scope.save($scope.new);
    };

    $scope.getAimExpertInventory = function (type, child) {
        $scope.new.aimExpertItems = [];
        $scope.new.type = type;
        $scope.new.name = '';
        $scope.new.description = '';
        $scope.loader();
        $http.get('/api/aimExpert/aimExpertInventoryList?type=' + type).then(function (response) {
            $scope.aimExpertInventories = response.data;
            if (type = 'career') {
                for (var i in  $scope.aimExpertInventories) {
                    if (!$scope.aimExpertInventories[i].leisures) {
                        $scope.aimExpertInventories[i].leisures = [];
                    }
                }
            }
            if (child) {
                if (child != 'NA') {
                    getAimExpertInventoryNamesList(child);
                }
            }
            $scope.cancelLoader();
        }, function (response) {
            $scope.cancelLoader();
            alert(response.error);
        })
    };

    function getAimExpertInventoryNamesList(type) {
        $http.get('/api/aimExpert/aimExpertInventoryNameList?type=' + type).then(function (response) {
            $scope.subjects = response.data;
            mergeCareersWithSubjects($scope.subjects, type);
        }, function (response) {
            alert(response.error);
        })
    }

    // function getAimExpertInventory(name) {
    //     $http.get('/api/aimExpert/aimExpertInventoryItem?type=' + name).then(function (response) {
    //         $scope.subjects = response.data;
    //         mergeCareersWithSubjects();
    //     }, function (response) {
    //         alert(response.error);
    //     })
    // }

    function mergeCareersWithSubjects(subjects, type) {
        var index = 0;
        var temp = {};
        for (var i in $scope.aimExpertInventories) {

            if(type=='leisure'){
                $scope.aimExpertInventories[i].subjects = $scope.aimExpertInventories[i].hobbies;
            }
            if(!$scope.aimExpertInventories[i].subjects){
                $scope.aimExpertInventories[i].subjects = [];
            }
            if($scope.aimExpertInventories[i].subjects.length<1){
                $scope.aimExpertInventories[i].subjects = [];
            }

            for (var j in subjects) {
                index = findIndexWithAttr($scope.aimExpertInventories[i].subjects, 'name', subjects[j]);
                if (index == -1) {
                    if (!$scope.aimExpertInventories[i].subjects) {
                        $scope.aimExpertInventories[i].subjects = [];
                    }
                    temp = {
                        name: subjects[j],
                        career: $scope.aimExpertInventories[i].name,
                        weight: 0
                    };
                    $scope.aimExpertInventories[i].subjects.push(temp);
                }
            }
        }
    }

    function findItemsInArrayWithAtrribute(array, attr, value) {
        result = [];
        for (var i = 0; i < array.length; i += 1) {
            if (array[i][attr] === value) {
                result.push(array[i]);
            }
        }
        return result;
    }

    function findIndexWithAttr(array, attr, value) {
        if (!array) {
            return -1;
        }
        for (var i = 0; i < array.length; i += 1) {
            if (array[i][attr] === value) {
                return i;
            }
        }
        return -1;
    }

    loadReportSections();

    $scope.getAimExpertInventory('career');

    $scope.saveCareerSubjectMarixItem = function (item, type) {
        if (item) {
            $http.post('/api/aimExpert/careerMatrix?type=' + type, item).then(function (response) {
                toaster.pop("success", "success");
            }, function (response) {
                alert(response.data.error);
            })
        } else {
            toaster.pop("info", "blank input");
        }
    };

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }


    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.setFiles = function (element) {
        $scope.testFile = element.files[0];
    };

    $scope.upload = function () {
        if ($scope.testFile) {

            $scope.loader();
            var r = new FileReader();
            var fd = new FormData();
            fd.append('file', $scope.testFile);
            fd.append('dryrun', $scope.dryrun);
            var request = {
                method: 'POST',
                url: '/api/aimExpert/aimExpertInventoryImport',
                data: fd,
                headers: {
                    'Content-Type': undefined
                }
            };
            $http(request).then(function (response) {
                    $scope.responseList = response.data;
                    $scope.cancelLoader();
                    displayResponse();
                    ok("success");

                }, function (response) {
                    $scope.cancelLoader();
                    $scope.responseList = response.data;
                    displayResponse();
                    error(response.data.error);
                }
            );


        } else {
            toaster.pop('error', "Please select file");
        }

    };

    var displayResponse = function () {
        $scope.res = [];
        $scope.showDisplayResponse = true;
        for (property in $scope.responseList) {
            records = {};
            records.name = property;
            records.value = $scope.responseList[property];
            $scope.res.push(records);
        }
    }
});