app.controller("Paymentgateway", function ($scope, $window, $state, $stateParams, $http, $rootScope, toaster, Auth) {
    $scope.pkgAmount = $stateParams.pkgAmount;
    $scope.errorCode = "";
    $scope.productAmount;
    if ($scope.pkgAmount == 'Blueprint') {
        $scope.productAmount = 3500;

    } else if ($scope.pkgAmount == 'Explore') {
        $scope.productAmount = 7500;

    } else {
        location.href = "https://aim2excel.in/product-class-9-10th/#packages";

    }
    let user = Auth.getUser();
    var options = {
        "key": "rzp_test_aqkNubYfA9GbEz",//"rzp_live_Pl1gxwGFQsiIIZ",//"rzp_test_aqkNubYfA9GbEz",
        "amount": $scope.productAmount * 100, //2 * 100, // 2000 paise = INR 20//2 * 100,
        "name": "AIM2EXCEL",
        "description": "Rise & Shine",
        "image": "/assets/img/aim2excel.png",
        "handler": function (response) {
            // alert(response.razorpay_payment_id);
            // $state.go("app.availabletests");
            $http.post('/getpayament?payId=' + response.razorpay_payment_id).then(function (response) {
                toaster.pop('success', "Payment Done");
                // $state.go("app.availabletests");

                // below code is commented becouse we want to user loging and can change thepassword
                // Auth.logout(function(){
                //     $state.go("welcome");
                // });
                $state.go("welcome");
            });
        },
        "theme": {
            "color": "#4158d5"
        },
        "modal.ondismiss": function (error) {
            toaster.pop('error', "Payment refused");
            // $window.open('https://aim2excel.in/#ourplans');
            // $window.open('https://aim2excel.in/#ourplans');       
            location.href = "https://aim2excel.in/#ourplans";

            // $state.go("login");
            // Auth.logout(function () {
            //     $rootScope.user = null;
            //     window.location = "/";
            // });
        },
        "prefill": {
            "name": "",
            "email": ""
        },
        "notes": {
            "address": "",
            "username": user.sub
        }
    };


    if ($stateParams.pkgAmount) {
        if (user) {
            $http.get('/api/user/profile').then((response) => {
                $scope.userId = response.data.id;
                if (response.data.premium) {
                    error("You have already bought the product with this email id. Try registering with a new email id if you wish to buy another. You will now be directed to your current account.");
                    $state.go("app.availabletests");
                } else {
                    $scope.userprofile = response.data;
                    options.notes.username = user.sub;
                    if (isNumeric($scope.userprofile.contactNumber)) {
                        options.prefill.contact = $scope.userprofile.contactNumber;
                    } else {
                        options.prefill.contact = '';
                    }
                    options.prefill.email = $scope.userprofile.email;
                    // var rzp1 = new Razorpay(options);
                    // rzp1.open();
                    $("#myModal").modal();
                }
            }, function (error) {
                $scope.userprofile = {}
            });

        }
    }

    function isNumeric(str) {
        if (str == null) {
            return false;

        }
        var code;
        for (var i = 0, len = str.length; i < len; i++) {
            code = str.charCodeAt(i);
            if (!(code > 47 && code < 58)) // numeric (0-9)
                return false;
        }
        return true;
    };


    function ok(message) {
        return swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    };
    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    };

    $scope.applyCoupon = (couponCode) => {
        if ($scope.couponCode) {
            $http.post('api/promoCode/apply?name=' + $scope.couponCode + '&productName=' + $scope.pkgAmount).then(function (response) {
                $scope.errorCode = "";
                $scope.discountOffered = $scope.productAmount * response.data.discount / 100;
                $scope.finalAmount = $scope.productAmount - ($scope.productAmount * response.data.discount / 100);
                options.notes.promoCode = $scope.couponCode;
                options.notes.productName = $scope.pkgAmount;
                options.amount -= options.amount * response.data.discount / 100;
                // $("#myModal").modal('hide');
            }, function (err) {
                $scope.errorCode = err.data.message;
                // error(err.data.message)
            })
        }
    }

    $scope.proceedToPayment = function () {
        $("#myModal").modal('hide');
    }

    $scope.couponCode = null;
    $('#myModal').on('hidden.bs.modal', function () {
        $("#myModal").modal('hide');
        var rzp1 = new Razorpay(options);
        rzp1.open();
    })
    $("#applyPromo").click(function () {
        $(".coupon_code_input").css('display', 'block');
    });
});