app.filter('pagination', function()
{
return function(input, start)
{
 start = +start;
 if(input instanceof Array)
 return input.slice(start);
};
});
app.controller('TeacherGradeController',function($scope,$http,CommonService,toaster,$uibModal){
  $scope.curPage = 0;
  $scope.pageSize = 10;
  $scope.goToPage = 1;
  $scope.TotalRecordCount=0;
  $scope.teacherArray=[];
  $scope.teacherGrade={};
  var countTeacher=0;
  $scope.schoolId='';
  $scope.gradeId='';
  $scope.grade='';
  $scope.teacherId='';

  $scope.numberOfPages = function() {
       $scope.pages = [];
       $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
       for (var i = 1; i <= $scope.totalPages; i++) {
           $scope.pages.push(i);
       }
       return $scope.totalPages;
   };

 $http.get('/api/school/all').then(function(response){
          $scope.listSchooles=response.data;
         });

 $http.get('/api/md/grades/').then(function(response){
      $scope.listGrades=response.data;
     });

 $scope.list=function(){
        $http.get('/api/teacher/classTeacher').then(function(response){
        $scope.listOfTeachers=response.data;
        $scope.TotalRecordCount=response.data.length;
        var x=0;
        $scope.listOfTeachers.forEach(function(element){
           $scope.listOfTeachers[x].selectTeacher = false;
           x=x+1;
          });
       },function(response){
          toaster.pop('Info',response.error);
       });
  };

  $scope.list();

  $scope.filterSearch=function(){
    $http.get('/api/teacher/filterList?schoolId='+$scope.schoolId+'&gradeId='+$scope.gradeId).then(
    function(response){
      if(response.data.length==0){
         toaster.pop('Info','List is empty..');
         return;
      }
      $scope.listOfTeachers=response.data;
      $scope.TotalRecordCount=response.data.length;
      var x=0;
      $scope.listOfTeachers.forEach(function(element){
      $scope.listOfTeachers[x].selectTeacher = false;
      x=x+1;
      });
    },
    function(response){
      toaster.pop('Info',response.error);
    });
  };

  $scope.isSelectAllTeacher = function () {
      if ($scope.selectAllTeacher) {
          $scope.teacherArray=[];
          for (countTeacher=0; countTeacher<$scope.TotalRecordCount; countTeacher++) {
          $scope.listOfTeachers[countTeacher].selectTeacher = true;
          $scope.teacherArray.push($scope.listOfTeachers[countTeacher].id);
         }
      }else{
          for (countTeacher=0; countTeacher<$scope.TotalRecordCount; countTeacher++) {
          $scope.listOfTeachers[countTeacher].selectTeacher = false;
         }
         $scope.teacherArray=[];
      }
  };

  $scope.isTeacherSelected = function (teacher) {
      if (teacher.selectTeacher){
       $scope.teacherArray.push(teacher.id);
      }else{
      var i=0;
      $scope.teacherArray.forEach(function(element){
      if(element==teacher.id){
      $scope.teacherArray.splice(i, 1);
      }
      i++;
      });
      i=0;
      }
  };

  $scope.saveAll=function(){
    if($scope.teacherArray.length<1){
      toaster.pop('Info',"teacher is not selected");
       return;
    }
    if(!$scope.grade){
      toaster.pop('Info',"Grade is not selected");
       return;
    }
    $scope.teacherGrade.teacherIds=$scope.teacherArray;
    $scope.teacherGrade.grade=$scope.grade;
    $http.put('api/teacher/assignGrade',$scope.teacherGrade).then(function(response){
         if(response.data){
           ok("Grade Assign successfully");
           $scope.selectAllTeacher=false;
           $scope.teacherArray=[];
           $scope.filterSearch();
           $scope.teacherGrade={};
         }else{
           error(response.data.error);
         }
      }, function (response) {
          error(response.data.error);
      });
  };

  $scope.removeAll=function(){
   if($scope.teacherArray.length<1){
     toaster.pop('Info',"teacher is not selected");
      return;
   }
   $scope.teacherGrade.teacherIds=$scope.teacherArray;
   $http.put('/api/teacher/removeAll',$scope.teacherGrade).then(
   function(response){
     if(response.data){
          ok("Grade Remove successfully");
          $scope.selectAllTeacher=false;
          $scope.teacherArray=[];
          $scope.filterSearch();
          $scope.teacherGrade={};
     }else{error(response.data.error);}
   },
   function(response){
     error(response.data.error);
   }
   );
  };

  $scope.manageGradeAssignment=function(teacher){
   $scope.teacherId=teacher.id;
   $scope.teacherName=teacher.name;
   $scope.teacherGrad=JSON.stringify(teacher.grade);
   if($scope.teacherGrad==0){$scope.teacherGrad='';}
   $scope.openTeacherModal();
  };

  $scope.openTeacherModal =  function () {
       $scope.modalInstance = $uibModal.open({
           templateUrl: 'teacherGradeView.html',
           size: 'sm',
           scope: $scope,
           backdrop: 'static'
       });
  };

   $scope.cancel = function() {
      $scope.teacherId='';
      $scope.modalInstance.close();
   };

   $scope.openStudentModal =  function () {
          $scope.modalInstance = $uibModal.open({
              templateUrl: 'student.html',
              size: 'lg',
              scope: $scope,
              backdrop: 'static'
          });
     };

  $scope.schoolId='';
  $scope.loadStudentByGrade=function(gradeId){
    if(!gradeId){
      toaster.pop('Info',"Grade is not Assigned to teacher");
      return;
    }
    $http.get('/api/student/filterList?schoolId='+$scope.schoolId+'&gradeId='+gradeId).
    then(function(response){
     $scope.studentList=response.data;
     $scope.openStudentModal();
    });
  };

  $scope.updateTeacherGrade=function(grade){
       if(!grade){
          toaster.pop('Info',"Grade is not selected");
          return;
       }
       if(!$scope.teacherId){
          toaster.pop('Info',"teacher is not selected");
          return;
       }
    $http.put('/api/teacher/updateGrade?id='+$scope.teacherId+'&gradeId='+grade).then(
    function(response){
       if(response.data){
           $scope.cancel();
           ok("Grade Update successfully");
           $scope.teacherArray=[];
           $scope.filterSearch();
       }else{error(response.data.error);}
    },
    function(response){
       error(response.data.error);
    });
  };

  function ok(message) {
          swal({
              title: message,
              type: 'success',
              buttonsStyling: false,
              confirmButtonClass: "btn btn-warning"
          });
      };
  function error(message) {
      swal({
          title: message,
          type: 'error',
          buttonsStyling: false,
          confirmButtonClass: "btn btn-warning"
      });
  };
});