app.controller('ProductController', function ($scope, $http, toaster, $localStorage, $rootScope, $stateParams, $uibModal, $state) {
    $scope.assets_url = $localStorage.assets_url;
    var productId = $stateParams.id;
    $scope.show404page = false;
    $rootScope.showTestNavBar = false;
    $rootScope.showDefaultNavBar = true;

    // $timeout(function () {
    //     document.getElementById("proDiv").scrollIntoView();
    // }, 100);

    function getAllAvailableTest() {
        $http.get('/api/test/listAvailableTests').then(function (response) {
            $scope.products = response.data;
            getSelectedComponent(productId);
            if ($scope.products.length) {
                $scope.show404page = false;
            }
            else {
                $scope.show404page = true;
                if ($scope.user.roles[0] == 'TestTaker') {
                    toaster.pop('error', "Tests are not available right now!");
                }
            }
        }, function (response) {
            toaster.pop('error', "Error", response.data.message);
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function getSelectedComponent(productId) {
        for (var i in $scope.products) {
            if ($scope.products[i].id == productId) {
                $scope.product = $scope.products[i];
                $scope.productAll = $scope.products[i].subParents;
            }
        }
        $scope.productCore=[];
        for (var k in $scope.productAll) {
            if ($scope.productAll[k].testChild.length) {
                $scope.productCore.push($scope.productAll[k]);
            }
        }
        if($scope.productCore.length<1){
         $state.go('app.availabletests');
         return;
        }
    }
    $scope.countTestByConstruct=function(id){
      $http.get('/api/test/countTestByConstruct?id='+id).then(function(response){
       $scope.realCount=response.data;
      });
    };

    $scope.startTakingTest = function (constructId) {
        $http.get('/api/test/listAllPublishTestsByParent?id='+constructId).then(function(response){
           $scope.currentTestId=response.data.id;
           if($scope.currentTestId==0||!$scope.currentTestId){
              toaster.pop('Info','You have completed all test');
              return;
           }
           $state.go('app.testPanel', {'testId': $scope.currentTestId,'productId':productId,'constructId':constructId});
        });
    };

    $scope.openBuyProductModal = function () {
        $scope.buyProduct = $uibModal.open({
            templateUrl: 'views/modals/buyProduct.html',
            size: 'md',
            scope: $scope,
            backdrop: 'static'
            // keyboard: false
        });
        $scope.step1();
    };


    $scope.closeBuyProductModal = function () {
        $scope.buyProduct.close();
    };

    getAllAvailableTest();

});

