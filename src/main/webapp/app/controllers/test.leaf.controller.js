app.controller('TestLeafController', function ($scope, $http, toaster, $localStorage, $rootScope, $stateParams) {
    $scope.assets_url = $localStorage.assets_url;
    $scope.id = $stateParams.id1;
    $scope.show404page = false;
    $rootScope.showTestNavBar = false;
    $rootScope.showDefaultNavBar = true;
    $scope.firstLevelTests = {};
    $scope.secondLevelTests = {};
    $scope.thirdLevelTests = {};



    $scope.exploreFirstLevel = function (test) {
        $scope.firstLevelTests = test;
        $scope.secondLevelTests = {};
        $scope.thirdLevelTests = {};
        $timeout(function () {
            document.getElementById("proDiv").scrollIntoView();
        }, 100);
    };
    $scope.exploreSecondLevel = function (test) {
        $scope.secondLevelTests = test;
        $scope.thirdLevelTests = {};
        $timeout(function () {
            document.getElementById("proDiv1").scrollIntoView();
        }, 100);
    };
    $scope.exploreThirdLevel = function (test) {
        $scope.thirdLevelTests = test;
        $timeout(function () {
            document.getElementById("proDiv2").scrollIntoView();
        }, 100);
    };

    $scope.closeAll = function () {
        $scope.firstLevelTests = {};
        $scope.secondLevelTests = {};
        $scope.thirdLevelTests = {};

    };

    $scope.getAllAvailableTest = function (id) {
        $http.get('/api/test/listAvailableTests').then(function (response) {
            $scope.firstLevelTests = {};
            $scope.secondLevelTests = {};
            $scope.thirdLevelTests = {};
            $scope.tests = response.data;
            getSelectedComponent(id);
            if ($scope.tests.length) {
                $scope.show404page = false;
            }
            else {
                $scope.show404page = true;
                if ($scope.user.roles[0] == 'TestTaker') {
                    toaster.pop('error', "Tests are not available right now!");
                }
            }
        }, function (response) {
            toaster.pop('error', "Error", response.data.message);
        });
    };



    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function getSelectedComponent(id){
        for(var i in $scope.tests){

        }

    }
    $scope.getAllAvailableTest($scope.id);

});

