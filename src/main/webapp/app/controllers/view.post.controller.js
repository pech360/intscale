app.controller('ViewPostController', function ( $http, $scope, $state, $stateParams, DateFormatService, $localStorage) {
   postId = $stateParams.id;
    $scope.assets_url = $localStorage.assets_url;

    function fetchProfile() {
        $http.get('/api/user/profile').then(function (response) {
            $scope.user = response.data;
            if ($scope.user.img) {
                $scope.userImageURI = $scope.assets_url + $scope.user.img;
            } else {
                if ($scope.user.gender) {
                    if ($scope.user.gender.toLowerCase() == "male") {
                        $scope.userImageURI = "/assets/img/b-avtar.png"
                    } else {
                        if ($scope.user.gender.toLowerCase() == "female") {
                            $scope.userImageURI = "/assets/img/g-avtar.png"
                        }
                    }
                } else {
                    $scope.userImageURI = "/assets/img/default-profile-picture.png"
                }
            }
        }, function (response) {
            toaster.pop('error', "Error", "Failed to load profile - " + response.data.error);

            if(response.data.error){
                if(response.data.error=="You are logged in another session, so close this session"){

                }
            }
        });
    }

    function getPost(id) {
        $http.get('/api/timeline/getPost/'+ id).then(function (response) {
            $scope.post = response.data;
            $scope.post.createdDate = DateFormatService.formatDate8(new Date($scope.post.createdDate));
            // $scope.post.createdDate = new Date($scope.post.createdDate);
            // $scope.post.createdDate = ($scope.post.createdDate);
        },function (response) {
            error(response.data.error);
        })
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function getRecommendedBlogs() {
        $http.get('/api/md/recommendBlogs').then(function (response) {
            $scope.blogs = response.data;
        },function (response) {
            error(response.data.error);
        })
    }

    $scope.like = function (id) {
        $http.post('/api/md/likeBlog/'+id).then(function (response) {
            updateBlogView(response.data);
        },function (response) {
            error("error in like")
        })
    };

    function updateBlogView(blog){
        for(var i in $scope.blogs){
            if($scope.blogs[i].id==blog.blogId){
                $scope.blogs[i].liked = blog.liked;
                $scope.blogs[i].likes = blog.likes;
            }
        }
    }
    getPost(postId);
    fetchProfile();
    getRecommendedBlogs();
});