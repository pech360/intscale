app.filter('pagination', function()
{
return function(input, start)
{
 start = +start;
 if(input instanceof Array)
 return input.slice(start);
};
});
app.controller('StudentManagementController',function($scope,$http,toaster,$uibModal,$state){
    $scope.curPage = 0;
    $scope.pageSize = 10;
    $scope.TotalRecordCount=0;
    $scope.goToPage = 1;
    $scope.listOfStudents={};
    $scope.student={};
    $scope.updateStudent=false;

   $scope.setCurrentPage = function() {
       $scope.curPage = 0;
   };

   $scope.numberOfPages = function() {
       $scope.pages = [];
       $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
       for (var i = 1; i <= $scope.totalPages; i++) {
           $scope.pages.push(i);
       }
       return $scope.totalPages;
   };

    $http.get('/api/school/all').then(function(response){
             $scope.listSchooles=response.data;
            });

    $http.get('/api/md/sections/').then(function(response){
         $scope.listSections=response.data;
        });

    $http.get('/api/md/grades/').then(function(response){
          $scope.listGrades=response.data;
         });

 $scope.list=function(){
      $http.get('/api/student/').then(function(response){
       $scope.listOfStudents=response.data;
       $scope.TotalRecordCount=response.data.length;
      },function(response){
         toaster.pop('Info',response.error);
      });
 };

 $scope.list();

 $scope.openStudentModal =  function () {
         $scope.modalInstance = $uibModal.open({
             templateUrl: 'createStudent.html',
             size: 'lg',
             scope: $scope,
             backdrop: 'static'
         });
     };

 $scope.cancel = function() {
        $scope.modalInstance.close();
    };

 $scope.createNew = function() {
     $scope.student={};
     $scope.updateStudent=false;
     $scope.openStudentModal();
 };
 $scope.create=function(student){
        if(!student.name){
        toaster.pop('Info',"Student name can't be empty");
        return;
        }
        if(!student.username){
        toaster.pop('Info',"Student username can't be empty");
        return;
        }
        if(!student.gender){
        toaster.pop('Info',"Student gender can't be empty");
        return;
        }
        if(!student.school){
        toaster.pop('Info',"Student school can't be empty");
        return;
        }
   if(student.id){
     $http.put('/api/student/',student).then(function(response){
         $scope.list();
         $scope.cancel();
         ok('Record successfully updated');
        },function(response){
         error(response.data.error);
        });
     }else{
        $http.post('/api/student/',student).then(function(response){
        $scope.list();
        $scope.cancel();
        ok('Record successfully created');
        },function(response){
        error(response.data.error);
        });
   }
 };

 $scope.update=function(student){
   $scope.student=angular.copy(student);
   $scope.student.section=JSON.stringify(student.section);
   $scope.student.school= JSON.stringify(student.school);
   $scope.student.grade= JSON.stringify(student.grade);
   $scope.updateStudent=true;
   $scope.openStudentModal();
 };

 function ok(message) {
           swal({
               title: message,
               type: 'success',
               buttonsStyling: false,
               confirmButtonClass: "btn btn-warning"
           });
       };
   function error(message) {
       swal({
           title: message,
           type: 'error',
           buttonsStyling: false,
           confirmButtonClass: "btn btn-warning"
       });
   };

   $scope.deleteRecord=function(id){
     swal({
           title: 'Are you sure?',
           text: "You won't be able to revert this!",
           type: 'warning',
           showCancelButton: true,
           confirmButtonColor: '#3085d6',
           cancelButtonColor: '#d33',
           confirmButtonText: 'Yes'
       }).then(function (result) {
           if (result.value) {
           $http.delete('/api/student?id='+id).then(function(response){
             $scope.list();
             ok('Record deleted successfully');
           },
            function (response) {
                error(response.data.error);
            });
            }
       });
   };
});