app.controller('RegisterController', function ($scope, $rootScope, Auth, $state, $stateParams, toaster, $uibModal, $timeout) {
    $scope.orgName = '';
    $scope.pkgAmount = $stateParams.pkgAmount;
    $scope.user = {};
    $scope.userprofile = {};
    $scope.f = false;
    $scope.g = false;
    $scope.orgName = window.location.hostname.split(".")[0];

    let user = Auth.getUser();
    if ($stateParams.pkgAmount) {
       
        $scope.pkgAmount = $stateParams.pkgAmount;
        if (user) {
            $state.go("paymentgateway", { 'pkgAmount': $scope.pkgAmount });
        }
        //  else {
        //     $state.go("login", { 'pkgAmount': $scope.pkgAmount }); 
        // }
    }

    $scope.signUp = function () {

        let user = $scope.user;

        if (!user.firstname) {
            toaster.pop('error', "First Name missing!");
            return;
        }

        // if (!user.username) {
        //     toaster.pop('error', "Username missing!");
        //     return;
        // }

        // if (!user.password) {
        //     toaster.pop('error', "Password missing!");
        //     return;
        // }

        // if (user.password !== user.repassword) {
        //     toaster.pop('error', "passwords do not match!");
        //     return;
        // }

        if (!user.email) {
            toaster.pop('error', "Email missing!");
            return;
        }

        if (!user.mobileNum) {
            toaster.pop('error', "Mobile Number missing!");
            return;
        }

        $scope.username = $scope.user.mobileNum;
        $scope.email = $scope.user.email;
        $scope.dataLoading = true;
        $scope.submitted = true;
        // $scope.otpVerificationDialog();
        Auth.signup(user,
            function (response) {
                // toaster.pop('success', "you are successfully registered");
                // $state.go("app.availabletests");
                toaster.pop('success', ' OTP has been send at the entered phone number.');
                $scope.dataLoading = false;
                $scope.otpVerificationDialog();
                $timeout(function () {
                    $scope.gettingOtp = true;
                    $scope.resendOtp = false;
                    // $scope.otpVerificationDialog();
                }, 10000);
            },
            function (response) {
                toaster.pop('error', response.data.message);
                $scope.dataLoading = false;
                $scope.submitted = false;
            }
        );
    };

    $scope.otpVerificationDialog = function () {
        return $scope.otpVerification = $uibModal.open({
            templateUrl: 'views/modals/otpVerificationDialog.html',
            size: 'md',
            controller: 'RegisterController',
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.otpVerifiacation = false;
    $scope.otpVerify = function () {
        const object = {
            'username': $scope.username,
            'pkgAmount': $scope.pkgAmount ? $scope.pkgAmount : null,
            'otp': $scope.otp
        }
        Auth.verifyOtp(object, function () {
            toaster.pop('success', "You are successfully registered");
        
            if ($scope.pkgAmount) {
                // options.prefill.email = $scope.email;
                // if(isNumeric($scope.username)){
                //     options.prefill.contact = $scope.username;
                // }else{
                //     options.prefill.contact = '';                
                // }
                // options.notes.username = $scope.username;
                // $scope.otpVerification.close();
                // var rzp1 = new Razorpay(options);
                // rzp1.open();
                $state.go("paymentgateway", { 'pkgAmount': $scope.pkgAmount });
            } else {
                $scope.otpVerification.close();
                $state.go("app.availabletests");
                // $state.go("welcome");
            }
        }, function (error) {
            $scope.otpVerifiacation = true;
            toaster.pop('error', error.data.message);
        });
    }

    $scope.resendOTP = function () {
        $scope.resendOtp = true;
        Auth.resendOtp($scope.username, function (response) {
            // toaster.pop('success', "you are successfully registered");
            // $state.go("app.availabletests");
            toaster.pop('success', ' OTP has been send at the entered phone number.');
            $scope.dataLoading = false;
            $timeout(function () {
                $scope.resendOtp = false;
            }, 10000);
        },
            function (response) {
                toaster.pop('error', response.data.message);
                $scope.dataLoading = false;
            })
    }

    $scope.cancelotpVerificationDialog = function () {
        $scope.otpVerification.close();
    };

    $rootScope.flow = 'register';

    $rootScope.$on('event:social-sign-in-success', function (event, user) {
        if ($rootScope.flow === 'register') {
            $scope.dataLoading = true;
            Auth.socialRegister(user, registerSuccess, function (response) {
                toaster.pop('error', response.data.message);
                $scope.dataLoading = false;
                $scope.f = false;
                $scope.g = false;
            });

            function registerSuccess(res) {
                let user = Auth.getUser();
                // $state.go("app.availabletests");
                $scope.dataLoading = false;

                //pankaj
                if ($scope.pkgAmount) {
                    // options.prefill.email = res.email;
                    // options.notes.username = user.sub;
                    // var rzp1 = new Razorpay(options);
                    // rzp1.open();
                    $state.go("paymentgateway", { 'pkgAmount': $scope.pkgAmount })
                } else {
                    $state.go("app.availabletests");
                    // $state.go("welcome");
                }

            }
        } else {
        }
    });

});