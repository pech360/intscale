// app.filter('pagination', function () {
//     return function (input, start) {
//         start = +start;
//         if (input instanceof Array)
//             return input.slice(start);
//     };
// });
app.controller('CounsellorCalenderController', function ($scope, $http, CommonService, toaster, $uibModal, DateFormatService) {

    $scope.events = [];

    $scope.getStudentsListByCounsellor = function () {
        $http.get('/api/student/listByCounsellor').then(function (response) {
            $scope.listOfStudents = response.data;

            for (var i in $scope.listOfStudents) {


                for (var j in $scope.listOfStudents[i].userCounsellingSessions) {


                    var tmp = {
                        title: $scope.listOfStudents[i].username,
                        start: new Date($scope.listOfStudents[i].userCounsellingSessions[j].scheduledOn),
                        description: $scope.listOfStudents[i].userCounsellingSessions[j].name
                    };

                    switch ($scope.listOfStudents[i].userCounsellingSessions[j].counsellingSessionStatus) {
                        case 'UPCOMING':
                            tmp.color = '#ffb300';
                            break;
                        case 'MISSED':
                            tmp.color = '#d9534f;';
                            break;
                        case 'COMPLETED':
                            tmp.color = '#00bb7e';
                            break;
                        case 'DELAYED':
                            tmp.color = '#4158d5';
                            break;
                        case 'DRAFTED':
                            tmp.color = '#202c54';
                            break;

                    }

                    $scope.events.push(angular.copy(tmp));
                }
            }

        }, function (response) {
            toaster.pop('Info', response.error);
        });
    };


    $scope.calendarOptions = {
        header: {
            left: 'title',
            center: 'month,list',
            right: 'prev,next today'
        },
        editable: false,
        firstDay: 1, //  1(Monday) this can be changed to 0(Sunday) for the USA system
        selectable: true,
        defaultView: 'month',
        themeSystem: 'bootstrap4',
        displayEventTime: false
    };





    $scope.getStudentsListByCounsellor();


    /********************************************   PREVIOUS CODE   **************************************************************/

    // $scope.curPage = 0;
    // $scope.pageSize = 10;
    // $scope.goToPage = 1;
    // $scope.TotalRecordCount = 0;
    // $scope.studentArray = [];
    // $scope.counsellorStudent = {};
    // var countStudent = 0;
    // $scope.schoolId = '';
    // $scope.gradeId = '';
    // $scope.studentId = '';
    // $scope.comment = '';
    // $scope.currentTime = '';
    // $scope.counsellorComments = [];
    // $scope.loading = false;
    // $scope.id = '';
    // $scope.ctreateNew = false;
    // $scope.studentNotesList = {};
    // $scope.studentNotes = {};
    //
    // $scope.numberOfPages = function () {
    //     $scope.pages = [];
    //     $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
    //     for (var i = 1; i <= $scope.totalPages; i++) {
    //         $scope.pages.push(i);
    //     }
    //     return $scope.totalPages;
    // };
    //
    // $http.get('/api/school/all').then(function (response) {
    //     $scope.listSchooles = response.data;
    // });
    //
    // $http.get('/api/md/grades/').then(function (response) {
    //     $scope.listGrades = response.data;
    // });
    // $scope.listTakenTestbyuser = function (id) {
    //     $http.get('/api/test/listTakenTestByUser?userId=' + id).then(function (response) {
    //         $scope.userTestList = response.data;
    //         if (response.data.length < 1) {
    //             toaster.pop('info', 'Taken test is not available')
    //         }
    //     })
    // };
    //
    // $scope.list = function () {
    //     $http.get('/api/student/listByCounsellor').then(function (response) {
    //         $scope.listOfStudents = response.data;
    //         $scope.TotalRecordCount = response.data.length;
    //     }, function (response) {
    //         toaster.pop('Info', response.error);
    //     });
    // };
    //
    // $scope.list();
    //
    // $scope.filterSearch = function () {
    //     $http.get('/api/student/filteredListByCounsellor?schoolId=' + $scope.schoolId + '&gradeId=' + $scope.gradeId).then(
    //         function (response) {
    //             if (response.data.length == 0) {
    //                 toaster.pop('Info', 'List is empty..');
    //                 return;
    //             }
    //             $scope.listOfStudents = response.data;
    //             $scope.TotalRecordCount = response.data.length;
    //         },
    //         function (response) {
    //             toaster.pop('Info', response.error);
    //         });
    // };
    //
    // $scope.openStudentProfileModal = function () {
    //     $scope.modalInstance = $uibModal.open({
    //         templateUrl: 'studentProfile.html',
    //         size: 'lg',
    //         scope: $scope,
    //         backdrop: 'static'
    //     });
    // };
    //
    // $scope.close = function () {
    //     $scope.modalInstance.close();
    // };
    //
    // $scope.viewStudentProfile = function (userId) {
    //     $scope.fetchProfile(userId);
    // };
    //
    // $scope.fetchProfile = function (id) {
    //     $http.get('/api/user/self/userProfile?userId=' + id).then(function (response) {
    //         $scope.user = response.data;
    //         $scope.user.dob = DateFormatService.formatDate(new Date(response.data.dob));
    //         $scope.openStudentProfileModal();
    //     }, function (response) {
    //         toaster.pop('error', "Error", "Failed to load profile... " + response.message);
    //     });
    // };
    //
    // $scope.openNotesModal = function () {
    //     $scope.modalInstance = $uibModal.open({
    //         templateUrl: 'studentnotes.html',
    //         size: 'lg',
    //         scope: $scope,
    //         backdrop: 'static'
    //     });
    // };
    // $scope.openEditNotesModal = function () {
    //     $scope.editModalInstance = $uibModal.open({
    //         templateUrl: 'editStudentNotes.html',
    //         size: 'lg',
    //         scope: $scope,
    //         backdrop: 'static'
    //     });
    // };
    // $scope.closeEditModal = function () {
    //     $scope.studentNotes = {};
    //     $scope.editModalInstance.close();
    // };
    // $scope.loadStudentNotes = function (id) {
    //     $scope.id = id;
    //     $http.get('/api/student/notes?id=' + id).then(function (response) {
    //         $scope.studentNotesList = response.data;
    //         $scope.openNotesModal();
    //     }, function (response) {
    //         toaster.pop('info', response.data.error);
    //     })
    // };
    //
    // $scope.saveAndPushStudentNotes = function () {
    //     $scope.studentNotes.studentId = $scope.id;
    //     $http.post('/api/student/saveandpushNotes', $scope.studentNotes).then(function () {
    //         $scope.ctreateNew = false;
    //         $scope.studentNotes = {};
    //         $scope.close();
    //         $scope.closeEditModal();
    //         ok('Notes save ans push successfully');
    //     })
    // };
    //
    // $scope.saveStudentNotes = function () {
    //     $scope.studentNotes.studentId = $scope.id;
    //     $http.post('/api/student/saveNotes', $scope.studentNotes).then(function () {
    //         $scope.ctreateNew = false;
    //         $scope.studentNotes = {};
    //         $scope.close();
    //         $scope.closeEditModal();
    //         ok('Notes save successfully');
    //     })
    // };
    //
    // $scope.pushStudentNotes = function (note) {
    //     $scope.studentNotes.studentId = $scope.id;
    //     $scope.studentNotes.id = note.id;
    //     $scope.studentNotes.notes = note.notes;
    //     $scope.studentNotes.selfNotes = note.selfNotes;
    //     $scope.studentNotes.sessionName = note.sessionName;
    //     $http.post('/api/student/pushNotes', $scope.studentNotes).then(function () {
    //         $scope.ctreateNew = false;
    //         $scope.studentNotes = {};
    //         $scope.close();
    //         $scope.closeEditModal();
    //         ok('Notes push successfully');
    //     })
    // };
    //
    // $scope.updateStudentNotes = function (note) {
    //     $scope.studentNotes = angular.copy(note);
    //     $scope.openEditNotesModal();
    // };
    //
    // $scope.saveUpdateStudentNotes = function (note) {
    //     $scope.studentNotes.studentId = $scope.id;
    //     $scope.studentNotes.id = note.id;
    //     $scope.studentNotes.notes = note.notes;
    //     $scope.studentNotes.selfNotes = note.selfNotes;
    //     $scope.studentNotes.sessionName = note.sessionName;
    //     $http.put('/api/student/updateNotes', $scope.studentNotes).then(function () {
    //         $scope.ctreateNew = false;
    //         $scope.studentNotes = {};
    //         $scope.close();
    //         $scope.closeEditModal();
    //         ok('Notes updated successfully');
    //     })
    // };
    //
    // $scope.saveAndPushUpdateStudentNotes = function (note) {
    //     $scope.studentNotes.studentId = $scope.id;
    //     $scope.studentNotes.id = note.id;
    //     $scope.studentNotes.notes = note.notes;
    //     $scope.studentNotes.selfNotes = note.selfNotes;
    //     $scope.studentNotes.sessionName = note.sessionName;
    //     $http.put('/api/student/pushUpdateNotes', $scope.studentNotes).then(function () {
    //         $scope.ctreateNew = false;
    //         $scope.studentNotes = {};
    //         $scope.close();
    //         $scope.closeEditModal();
    //         ok('Notes update & pushed successfully');
    //     })
    // };
    //
    // $scope.pushComment = function (comment) {
    //     $scope.comment = '';
    //     var date = new Date;
    //     $scope.currentTime = DateFormatService.formatDate5(date);
    //     $scope.counsellorComments.push(comment);
    // };
    //
    // $scope.openStudentReportModal = function (userId, testId) {
    //     if (!userId) {
    //         toaster.pop('info', 'User Id is not found');
    //         return;
    //     }
    //     if (!testId) {
    //         toaster.pop('info', 'Test Id is not found');
    //         return;
    //     }
    //     $scope.modalInstance = $uibModal.open({
    //         templateUrl: 'views/reports/talentReport.html',
    //         controller: 'TalentReportController',
    //         resolve: {
    //             UserId: function () {
    //                 return userId;
    //             },
    //             TestId: function () {
    //                 return testId;
    //             }
    //         },
    //         size: 'lg',
    //         scope: $scope,
    //         backdrop: 'static'
    //     });
    // };
    //
    // $scope.openCounsellorReportModal = function (userId, testId) {
    //     if (!userId) {
    //         toaster.pop('info', 'User Id is not found');
    //         return;
    //     }
    //     if (!testId) {
    //         toaster.pop('info', 'Test Id is not found');
    //         return;
    //     }
    //     $scope.modalInstance = $uibModal.open({
    //         templateUrl: 'views/reports/counsellorReport.html',
    //         controller: 'CounsellorReportController',
    //         resolve: {
    //             UserId: function () {
    //                 return userId;
    //             },
    //             TestId: function () {
    //                 return testId;
    //             }
    //         },
    //         size: 'lg',
    //         scope: $scope,
    //         backdrop: 'static'
    //     });
    // };
    //
    // //ui-sref="app.individualReportV1({id: test.id})"
    // //ui-sref="app.counsellorReport({id: test.id})"
    //
    // function ok(message) {
    //     swal({
    //         title: message,
    //         type: 'success',
    //         buttonsStyling: false,
    //         confirmButtonClass: "btn btn-warning"
    //     });
    // }
    //
    // function error(message) {
    //     swal({
    //         title: message,
    //         type: 'error',
    //         buttonsStyling: false,
    //         confirmButtonClass: "btn btn-warning"
    //     });
    // }
    //
    // $scope.downloadIndividualReport = function (id) {
    //     $scope.dataLoading = true;
    //     $scope.loader();
    //     $http.get('api/reports/downloadIndividualV1/' + id, {responseType: 'arraybuffer'}).then(function (response) {
    //
    //         $scope.cancelLoader();
    //         $scope.dataLoading = false;
    //         var blob = new Blob([response.data], {
    //             type: 'application/octet-stream'
    //         });
    //         saveAs(blob, response.headers("fileName"));
    //     }, function (response) {
    //         $scope.cancelLoader();
    //         $scope.dataLoading = false;
    //         error("download failed");
    //     })
    // };
    //
    // $scope.loader = function(){
    //     $scope.modalInstance = $uibModal.open({
    //         templateUrl: 'views/loader.html',
    //         size: 'sm',
    //         scope: $scope,
    //         backdrop: 'static'
    //     });
    // };
    //
    // $scope.cancelLoader = function() {
    //     $scope.modalInstance.close();
    // };

});
