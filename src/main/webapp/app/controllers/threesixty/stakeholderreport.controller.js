app.controller('StakeholderReportController', function ($scope, $http, toaster,DateFormatService) {
    $scope.schools = [];
    $scope.grades = [];
    $scope.disableDownload = false;

    $scope.downloadReport = function () {
        var req = validate();
        $scope.disableDownload = true;
        $http.post('/api/stakeholder/report/', req, {responseType: 'arraybuffer'}).then(
            function (response) {
                $scope.disableDownload = false;
                var blob = new Blob([response.data], {
                    type: 'application/octet-stream'
                });
                saveAs(blob, response.headers("fileName"));
            }, function (response) {
                $scope.disableDownload = false;
                toaster.pop('error', response.data.error);
            }
        );
    };

    function validate() {
        var out = {};
        if ($scope.fromDate) {
            out.from = DateFormatService.formatDate4($scope.fromDate);
        }
        if ($scope.toDate) {
            out.to = DateFormatService.formatDate4($scope.toDate);
        }
        if ($scope.selectedSchool && $scope.selectedSchool.id) {
            out.schoolId = $scope.selectedSchool.id;
        }
        if ($scope.selectedGrade && $scope.selectedGrade.id) {
            out.gradeId = $scope.selectedGrade.id;
        }
        return out;
    }

    function loadSchools() {
        $http.get('/api/school/all').then(
            function (response) {
                $scope.schools = response.data;
            }, function (response) {
                toaster.pop('error', response.data.error);
            }
        );
    }

    function loadGrades() {
        $http.get('/api/md/grades/').then(
            function (response) {
                $scope.grades = response.data;
            }, function (response) {
                toaster.pop('error', response.data.error);
            }
        );
    }

    function refresh() {
        loadSchools();
        loadGrades();
    }

    refresh();
});