app.controller('StakeholderController', function ($scope, $http, $filter, toaster) {
    $scope.users = [];
    $scope.user = {};
    $scope.new = {relation: null};
    $scope.relations = [
        {name: 'Select Relation', value: null},
        {name: 'Parent', value: 'Parent'},
        {name: 'Sibling', value: 'Sibling'},
        {name: 'Peer', value: 'Peer'},
        {name: 'Teacher', value: 'Teacher'}
    ];
    $scope.disableSave = false;

    $scope.currentPage = 0;
    $scope.pageSize = 10;
    $scope.barsize = 5;//odd value
    $scope.pagebar = [];
    $scope.search = '';
    $scope.getData = function () {
        var f1 = $filter('filter')($scope.users, $scope.search);
        return f1;
    };
    $scope.numberOfPages = function () {
        return Math.ceil($scope.getData().length / $scope.pageSize);
    };
    $scope.$watch('search', function (newValue, oldValue) {
        if (oldValue != newValue) {
            $scope.currentPage = 0;
        }
    }, true);
    $scope.changePage = function (page) {
        $scope.currentPage = page.number;
    };


    $scope.addStakeholder = function (user, stakeholder) {
        if (!user.stakeholders) {
            user.stakeholders = [];
        }
        user.stakeholders.push(stakeholder);
        $scope.new = {relation: null};
    };

    $scope.sendVerification = function (stakeholder) {
        if (stakeholder && stakeholder.id) {
            stakeholder.verifyDisabled = true;
            $http.get('/api/stakeholder/verify/' + stakeholder.id).then(
                function (response) {
                    stakeholder.verifyDisabled = false;
                    $scope.user = {};
                    toaster.pop('info', 'send email verification link to ' + stakeholder.email);
                }, function (response) {
                    stakeholder.verifyDisabled = false;
                    toaster.pop('error', response.data.error);
                }
            );
        }
    };

    $scope.send = function (stakeholder) {
        if (stakeholder && stakeholder.id) {
            stakeholder.sendDisabled = true;
            $http.get('/api/stakeholder/send/' + stakeholder.id).then(
                function (response) {
                    stakeholder.sendDisabled = false;
                    $scope.user = {};
                    toaster.pop('info', 'send test link to ' + stakeholder.email);
                }, function (response) {
                    stakeholder.sendDisabled = false;
                    toaster.pop('error', response.data.error);
                }
            );
        }
    };
    $scope.remind = function (stakeholder) {
        if (stakeholder && stakeholder.id) {
            stakeholder.remindDisabled = true;
            $http.get('/api/stakeholder/remind/' + stakeholder.id).then(
                function (response) {
                    stakeholder.remindDisabled = false;
                    $scope.user = {};
                    toaster.pop('info', 'send test remind to ' + stakeholder.email);
                }, function (response) {
                    stakeholder.remindDisabled = false;
                    toaster.pop('error', response.data.error);
                }
            );
        }
    };


    $scope.save = function (user) {
        if (user) {
            $scope.disableSave = true;
            $http.post('/api/stakeholder/', user).then(
                function (response) {
                    $scope.disableSave = false;
                    toaster.pop('info', 'Saved successfully.');
                    $scope.user = {};
                }, function (response) {
                    $scope.disableSave = false;
                    toaster.pop('error', response.data.error);
                }
            );
        }
    };

    $scope.loadStakeholder = function (user) {
        if (user) {
            $http.get('/api/stakeholder/user/' + user.id).then(
                function (response) {
                    $scope.user = response.data;
                    $scope.user.mode = "add";
                }, function (response) {
                    toaster.pop('error', response.data.error);
                }
            );
        }
    };

    function loadUsers() {
        $http.get('/api/stakeholder/users/').then(
            function (response) {
                $scope.users = response.data;
                $scope.currentPage = 0;
            }, function (response) {
                toaster.pop('error', response.data.error);
            }
        );
    }

    function refresh() {
        loadUsers();
    }

    refresh();
});

app.filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return typeof input == 'undefined' ? [] : input.slice(start);
    }
});