app.controller('Upload360QuestionController', function ($scope, $http, toaster) {

    $scope.uploadDisable = false;

    $scope.cancel = function () {
        $scope.selectedTest = {};
        $scope.file = null;
    };

    $scope.upload = function () {
        if (!($scope.selectedTest && $scope.selectedTest.id)) {
            toaster.pop('error', 'Please select test.');
            return;
        }
        if (!$scope.file) {
            toaster.pop('error', 'Please select file.');
            return;
        }

        $scope.uploadDisable = true;
        var fd = new window.FormData();
        fd.append('file', $scope.file);
        fd.append('testId', $scope.selectedTest.id);
        $http.post('/api/question/stakeholder/upload/', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function (response) {
            $scope.uploadDisable = false;
            toaster.pop('info', 'Sheet Successfully imported.');
        }, function (response) {
            $scope.uploadDisable = false;
            toaster.pop('error', response.data.error);
        });


    };

    $scope.pushTest = function (test) {
        if (test) {
            $scope.selectedTest = test;
        } else {
            toaster.pop("Info", "Please select test");
        }
    };

    $scope.constructChanged = function (construct) {
        if (construct && construct.id) {
            loadTestById(construct.id, function (data) {
                $scope.subConstructs = data;
            });
        }
    };

    $scope.subConstructChanged = function (subConstruct) {
        if (subConstruct && subConstruct.id) {
            loadTestById(subConstruct.id, function (data) {
                $scope.tests = data;
            });
        }
    };

    $scope.setFiles = function (element) {
        $scope.file = element.files[0];
    };

    function loadTestById(id, success) {
        if (id) {
            $http.get('/api/test/testByParent?id=' + id).then(
                function (response) {
                    success(response.data);
                }, function (response) {
                    toaster.pop(response.data.error);
                }
            );
        }
    }

    function loadConstructs() {
        $http.get('/api/test/parentByTenantAndConstruct').then(
            function (response) {
                $scope.constructs = response.data;
            }, function (response) {
                toaster.pop('error', response.data.error);
            }
        );
    }

    function refresh() {
        loadConstructs();
    }


    refresh();
});