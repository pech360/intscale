app.controller('360QuestionController', function ($scope, $http, toaster) {
    $scope.saveDisable = false;
    $scope.categories = [];
    $scope.subTags = [];

    function newQuestion() {
        return {
            subTags: [],
            options: []
        };
    }

    function resetAllDropDowns() {
        $scope.selectedConstruct = null;
        $scope.selectedSubConstruct = null;
        $scope.selectedTest = null;
        $scope.selectedTag = null;
    }

    $scope.addQuestion = function () {
        resetAllDropDowns();
        $scope.question = newQuestion();
        $scope.question.mode = 'New';
    };
    $scope.editQuestion = function (question) {
        resetAllDropDowns();
        $scope.question = angular.copy(question);
        $scope.question.mode = 'Edit';
        $scope.question.category = getCategoryById($scope.question.category.id);
    };

    $scope.save = function (question) {
        question = angular.copy(question);
        if (!question) {
            return;
        }
        //TODO : Validations
        if (!question.description) {
            toaster.pop("error", "Question description is required.");
            return;
        }
        if (!question.test) {
            toaster.pop("error", "Please select test and push it using >> icon.");
            return;
        }
        if (!question.subCategory) {
            toaster.pop("error", "Please select category and then select subcategory.");
            return;
        }

        if (question.subTags.length > 0) {
            question.subTagIds = [];
            for (var i = 0; i < question.subTags.length; i++) {
                question.subTagIds.push(question.subTags[i].id);
            }
        }

        if (question.options.length > 0) {
            for (var i = 0; i < question.options.length; i++) {
                var option = question.options[i];
                if (typeof option.marks == 'undefined' || option.marks == null) {
                    toaster.pop("error", "Option marks is required.");
                    return;
                }
            }
        }

        if (question.test && question.test.id) {
            question.testId = question.test.id;
        }
        if (question.subCategory && question.subCategory.id) {
            question.subCategoryId = question.subCategory.id;
        }

        delete question.test;
        delete question.subTags;
        delete question.category;
        delete question.subCategory;

        if (question.id) {
            $scope.saveDisable = true;
            $http.put('/api/question/stakeholder/', question).then(
                function (response) {
                    $scope.saveDisable = false;
                    toaster.pop('info', "Question saved.");
                    loadQuestions();
                    $scope.question = newQuestion();
                }, function (response) {
                    $scope.saveDisable = false;
                    toaster.pop('error', response.data.error);
                }
            );
        } else {
            $scope.saveDisable = true;
            $http.post('/api/question/stakeholder/', question).then(
                function (response) {
                    $scope.saveDisable = false;
                    toaster.pop('info', "Question saved.");
                    loadQuestions();
                    $scope.question = newQuestion();
                }, function (response) {
                    $scope.saveDisable = false;
                    toaster.pop('error', response.data.error);
                }
            );
        }

    };

    $scope.optionCountChanged = function (optionCount) {
        if (!optionCount || optionCount == $scope.question.options.length) return;

        if (optionCount < $scope.question.options.length) {
            $scope.question.options.splice(optionCount, $scope.question.options.length - optionCount)
        } else {
            var count = optionCount - $scope.question.options.length;
            for (var i = 0; i < count; i++) {
                $scope.question.options.push({});
            }
        }
    };

    $scope.pushSubTag = function (tag) {
        var flag = false;
        for (var i = 0; i < $scope.question.subTags.length; i++) {
            var obj = $scope.question.subTags[i];
            if (obj.id == tag.id) {
                flag = true;
                break;
            }
        }
        if (!flag) {
            $scope.question.subTags.push(tag);
        }
    };

    $scope.popSubTag = function (index) {
        $scope.question.subTags.splice(index, 1);
    };

    $scope.pushTest = function (test) {
        if (test) {
            $scope.question.test = test;
        } else {
            toaster.pop("Info", "Please select test");
        }
    };

    $scope.constructChanged = function (construct) {
        if (construct && construct.id) {
            loadTestById(construct.id, function (data) {
                $scope.subConstructs = data;
            });
        }
    };

    $scope.subConstructChanged = function (subConstruct) {
        if (subConstruct && subConstruct.id) {
            loadTestById(subConstruct.id, function (data) {
                $scope.tests = data;
            });
        }
    };

    function loadTestById(id, success) {
        if (id) {
            $http.get('/api/test/testByParent?id=' + id).then(
                function (response) {
                    success(response.data);
                }, function (response) {
                    toaster.pop(response.data.error);
                }
            );
        }
    }

    function loadConstructs() {
        $http.get('/api/test/parentByTenantAndConstruct').then(
            function (response) {
                $scope.constructs = response.data;
            }, function (response) {
                toaster.pop('error', response.data.error);
            }
        );
    }

    function loadQuestions() {
        $http.post('/api/question/stakeholder/filter/', {}).then(
            function (response) {
                $scope.questions = response.data;
                if ($scope.questions.length == 0) {
                    toaster.pop('Info', "Empty question list.");
                } else {
                    for (var i = 0; i < $scope.questions.length; i++) {
                        var subTags = $scope.questions[i].subTags;
                        var options = $scope.questions[i].options;
                    }
                }
            }, function (response) {
                toaster.pop('error', response.data.error);
            }
        );
    }

    function loadSubTags() {
        $http.get('/api/subtag').then(
            function (response) {
                $scope.subTags = response.data;
            }, function (response) {
                toaster('error', response.data.error);
            }
        );
    }

    function loadCategories() {
        $http.get('/api/category/parentWithSubCategory').then(
            function (response) {
                $scope.categories = response.data;
            }, function (response) {
                toaster.pop('error', response.data.error);
            }
        );
    }

    function refresh() {
        loadCategories();
        loadSubTags();
        loadConstructs();
        loadQuestions();
    }

    function getCategoryById(id) {
        if (id) {
            for (var i = 0; i < $scope.categories.length; i++) {
                var category = $scope.categories[i];
                if (category.id == id) {
                    return category;
                }
            }
        }
        return null;
    }

    refresh();
});



