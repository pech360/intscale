app.controller('TalentReportController', function ($http, $scope, $stateParams, DateFormatService,UserId,TestId) {
    $scope.userId='0';
    if(TestId>0&&UserId>0){
      var testId = TestId;
      $scope.userId=UserId;
      fetchProfileByUserId(UserId);
    }else if(UserId.length==undefined&&TestId.length==undefined&&UserId){
      var testId = $stateParams.id;
      $scope.userId='0';
      fetchProfile();
    }
    fetchTestDetails(testId);
    var obtainedScoreRange ={};
    //alert(UserId+' '+TestId);
    $scope.getReportV1 = function () {
        $http.get('/api/reports/talentReport?id='+testId+'&userId='+$scope.userId).then(
            function (response) {
                $scope.categories = response.data;
                $scope.categories.sort(compareBySequence);
                renderChart($scope.categories);
                // getStrengthAndWeakness();
            }, function (response) {
                error(response.data.error)
            });
    };

    $scope.getReportV1();

    function fetchProfile() {
        $http.get('/api/user/profile').then(function (response) {
            $scope.user = response.data;
            $scope.user.dob = DateFormatService.formatDate(new Date(response.data.dob));
        });
    }
    function fetchProfileByUserId(id) {
            $http.get('/api/user/self/userProfile?userId='+id).then(function (response) {
                $scope.user = response.data;
                $scope.user.dob = DateFormatService.formatDate(new Date(response.data.dob));
            });
        }
    function fetchTestDetails() {
        $http.get('/api/result/test?testId='+testId+'&userId='+$scope.userId).then(function (response) {
            $scope.testDetails = response.data;
            $scope.testDetails.testTakenDate = DateFormatService.formatDate7(new Date($scope.testDetails.testTakenDate));
        });
    }

    function renderChart(data) {
        $scope.report = {};
        $scope.report.labels = [];
        $scope.report.data = [];
        for (var d in data) {
            $scope.categories[d].rawScore = $scope.categories[d].rawScores[0];
            $scope.categories[d].rawScore = $scope.categories[d].rawScores[0];
            // $scope.report.labels.push(data[d].displayName);
            $scope.report.labels.push(data[d].name);
            $scope.report.data.push(data[d].rawScore);
        }

        $scope.report.options = {
            responsive: true,
            scales: {
                xAxes: [{
                    ticks: {
                        max: 5,
                        min: 1,
                        stepSize: 1,
                        beginAtZero:false
                    }
                    // , gridLines: {
                    //     drawBorder: true,
                    //     color: ['#FF6F00', '#FFA000', '#FFB300', '#FFC107', '#FFCA28', '#FFD54F', '#FFE082', '#FFECB3', '#FFF8E1', '#FFF7E5']
                    // }
                }]
            }

            // elements: {
            //     line: {
            //         tension: 0.1,
            //         fill: false,
            //         borderColor: "rgb(255,165,0)",
            //         borderWidth: 5
            //     }
            // }
        };
    }


    function compareBySequence(a, b) {
        if (a.sequence < b.sequence)
            return -1;
        if (a.sequence > b.sequence)
            return 1;
        return 0;
    }

    function compareStrengths(a, b) {
        if (a.startFrom < b.startFrom)
            return 1;
        if (a.startFrom > b.startFrom)
            return -1;
        return 0;
    }

    function compareWeaknesses(a, b) {
        if (a.startFrom < b.startFrom)
            return -1;
        if (a.startFrom > b.startFrom)
            return 1;
        return 0;
    }


    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

});