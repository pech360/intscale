app.controller('LeapDashboardStudentsPerformanceController', function ($scope, $http, DateFormatService, toaster, $uibModal, $stateParams, $localStorage) {
    $scope.assets_url = $localStorage.assets_url;

    $scope.currentGrade = $stateParams.grade;
    $scope.currentSection = $stateParams.section;
    $scope.currentConstruct = '';
    $scope.orgName = window.location.hostname.split(".")[0];


    var request = {
        grades: [],
        products: [],
        fromDate: {},
        toDate: {}
    };

    $scope.results = [];

    var now = new Date();
    var ss = now.getSeconds();
    var mm = now.getMinutes();
    var hh = now.getHours();

    var fromD = new Date();
    fromD = moment(fromD);
    fromD = fromD.set('month', 3);
    fromD = fromD.startOf('month');
    fromD = fromD.subtract(1, 'years');
    fromD = fromD.set({ 'hour': hh, 'minute': mm, 'second': ss });
    fromD = fromD.format('MM/DD/YYYY HH:mm:ss');

    $scope.fromDate = new Date(fromD);
    $scope.toDate = new Date();

    $scope.shiftFromDate = function (to) {
        switch (to) {
            case "lastWeek":
                fromD = new Date();
                fromD = moment(fromD).subtract(1, 'weeks');
                fromD = fromD.set({ 'hour': hh, 'minute': mm, 'second': ss });
                fromD = fromD.format('MM/DD/YYYY HH:mm:ss');
                $scope.fromDate = new Date(fromD);
                break;
            case "lastMonth":
                fromD = new Date();
                fromD = moment(fromD).subtract(1, 'months');
                fromD = fromD.set({ 'hour': hh, 'minute': mm, 'second': ss });
                fromD = fromD.format('MM/DD/YYYY HH:mm:ss');
                $scope.fromDate = new Date(fromD);
                break;
        }
    };

    $scope.gradeResults = [];
    var testNames = {};
    var subjects = [];

    $scope.colors = ['#4158d5', '#00bb7e', '#ffc107'];


    $scope.reset = function () {
        var request = {
            grades: [],
            products: [],
            fromDate: {},
            toDate: {}
        };
    };

    $scope.loader = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelLoader = function () {
        $scope.modalInstance.close();
    };

    function getAllGrades() {
        $http.get('/api/school/getGradesByTeacher').then(function (response) {
            $scope.grades = response.data;
            $scope.grades = $scope.grades.sort(function (a, b) {
                return a - b;
            });

        });
    }

    function getAllSections() {
        $http.get('/api/school/getSectionsByTeacher').then(function (response) {
            $scope.sections = response.data;
            $scope.sections = $scope.sections.sort();
        });
    }

    $scope.getSubjects = function () {
        var flag = true;
        if (!$scope.currentGrade) {
            flag = false;
        }
        if (!$scope.currentSection) {
            flag = false;
        }
        if (flag) {
            request = populateDashRequest($scope.currentGrade, $scope.currentSection);
            $http.post('/api/school/getSubjectsByTeacher', request).then(function (response) {
                $scope.subjects = Object.values(response.data);
            });
        }
    };

    $scope.selectItem = function (value, type) {
        switch (type) {
            case "grade":
                $scope.currentGrade = value;
                // $scope.getSubjects();
                break;
            case "section":
                $scope.currentSection = value;
                // $scope.getSubjects();
                break;
            case "subject":
                $scope.currentSubject = value;
                break;
        }

        $scope.getResult();
    };

    function populateDashRequest(grade, section, constructId, userId) {
        request.grades = [];
        request.sections = [];
        request.products = [];
        request.userIds = [];
        request.fromDate = DateFormatService.formatDate9($scope.fromDate);
        request.toDate = DateFormatService.formatDate9($scope.toDate);

        if (grade) {
            request.grades.push(grade);
            $scope.currentGrade = grade;
        }
        if (section) {
            request.sections.push(section);
        }
        if (constructId) {
            request.products.push(constructId);
        }
        if (userId) {
            request.userIds.push(userId);
        }
        return request;
    }


    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }


    function getTestNames() {
        $http.get('/api/result/getTestNames', request).then(function (response) {
            testNames = response.data;
        }, function (response) {

        })
    }

    $scope.getResult = function () {
        request = populateDashRequest($scope.currentGrade, $scope.currentSection);
        $http.post('/api/school/overAllTestResult/grade/section/usersList', request).then(function (response) {
            var usersList = response.data;
            getConstructList(usersList);
        }, function (response) {

        })
    };

    $scope.barOptions = {
        responsive: true,
        scales: {
            yAxes: [{
                type: 'linear',
                gridLines: {
                    display: true
                },
                ticks: {
                    max: 100,
                    min: 0,
                    stepSize: 20,
                    beginAtZero: true
                }
            }], xAxes: [{
                gridLines: {
                    display: false
                },
                maxBarThickness: 80
            }]
        },
        tooltips: {
            enabled: true,
            mode: 'single',
            callbacks: {
                label: function (tooltipItems, data) {
                    return tooltipItems.yLabel + '%';
                }
            }
        },
        plugins: {
            datalabels: {
                color: 'white',
                display: function (context) {
                    return context.dataset.data[context.dataIndex] > 15;
                },
                font: {
                    weight: 'bold'
                },
                formatter: function (value) {
                    return value + '%';
                    // eq. return ['line1', 'line2', value]
                }
            }
        }
    };

    function getConstructList(usersList) {
        request = populateDashRequest($scope.currentGrade, $scope.currentSection);
        // $http.post('/api/school/overAllTestResult/grade/section/constructList', request).then(function (response) {
        $http.post('/api/school/getSubjectsByTeacher', request).then(function (response) {
            var subjects = Object.values(response.data);

            processResult(usersList, subjects);
        }, function (response) {

        })
    }

    $scope.getStudentPerformanceBySubject = function (grade, section, constructId, userId, construct, syllabusCompleted) {

        if (construct) {
            $scope.currentConstruct = construct;
            $scope.syllabusCompleted = syllabusCompleted;
        } else {
            $scope.currentConstruct = "";
        }

        request = populateDashRequest(grade, section, constructId, userId);

        $http.post('/api/school/overAllTestResult/grade/section/user/construct/', request).then(function (response) {
            $scope.userResult = response.data;

            renderChart($scope.userResult);
            fetchProfileByUserId(userId);
            openStudentPerformanceBySubjectModal();
        }, function (response) {

        })
    };

    function fetchProfileByUserId(id) {
        $http.get('/api/user/self/userProfile?userId=' + id).then(function (response) {
            $scope.student = response.data;
            $scope.student.dob = DateFormatService.formatDate(new Date(response.data.dob));
            if ($scope.student.img) {
                $scope.studentImageURI = $scope.assets_url + $scope.student.img;
            } else {
                if ($scope.student.gender) {
                    if ($scope.student.gender.toLowerCase() == "male") {
                        $scope.studentImageURI = "/assets/img/b-avtar.png"
                    } else {
                        if ($scope.student.gender.toLowerCase() == "female") {
                            $scope.studentImageURI = "/assets/img/g-avtar.png"
                        }
                    }
                } else {
                    $scope.studentImageURI = "/assets/img/default-profile-picture.png";
                }
            }
        });
    }

    function renderChart(results) {
        $scope.subjectGraph = {
            labels: [],
            data: []
        };
        var tempName = "";

        for (var i in results) {
            tempName = results[i].name;
            var arr = tempName.split("_");
            var v = arr.length <= 1 ? arr[0] : arr[arr.length - 1];

            $scope.subjectGraph.labels.push(v);
            $scope.subjectGraph.data.push(results[i].percentageScore);
        }
    }


    function findItemsInArrayWithAtrribute(array, attr, value) {
        result = [];
        for (var i = 0; i < array.length; i += 1) {
            if (array[i][attr] === value) {
                result.push(array[i]);
            }
        }
        return result;
    }

    function processResult(students, subjects) {
        $scope.subjects = {};
        $scope.results = [];
        var temp = {};

        for (var x in subjects) {
            $scope.subjects[subjects[x]] = 0
        }

        for (var i in students) {
            temp.name = students[i][0].name;
            temp.subjects = [];
            for (var k in subjects) {
                var items = findItemsInArrayWithAtrribute(students[i], 'type', subjects[k]);
                if (items.length > 0) {
                    temp.subjects.push(items[0]);
                    if (items[0].syllabusCompleted > $scope.subjects[subjects[k]]) {
                        $scope.subjects[subjects[k]] = items[0].syllabusCompleted;
                    }
                } else {
                    var tempItem = {};
                    tempItem.percentageScore = 0;
                    temp.subjects.push(tempItem);
                }
            }

            temp.overallScore = calculateOverallScoreForEachStudent(temp);
            $scope.results.push(angular.copy(temp));
        }
    }

    function calculateOverallScoreForEachStudent(student) {
        var avg = 0;
        for (var i in student.subjects) {
            avg = student.subjects[i].percentageScore + avg;
        }
        avg = avg / student.subjects.length;
        return avg;
    }

    function openStudentPerformanceBySubjectModal() {

        $scope.sectionsPerformanceBySubjectModal = $uibModal.open({
            templateUrl: 'views/modals/studentPerformanceBySubject.html',
            size: 'lg',
            scope: $scope,
            backdrop: 'true'
        });
    }

    function closeStudentPerformanceBySubjectModal() {
        $scope.sectionsPerformanceBySubjectModal.close();
    }

    function getStudentCountByGrade() {
        request = populateDashRequest();

        $http.post('/api/school/getNumberOfStudentsByGrade', request).then(function (response) {

            $scope.studentCountByGrade = response.data;

        }, function (response) {

        })
    }


    function init() {
        getTestNames();
        getAllGrades();
        getAllSections();
        // $scope.getSubjects();
        $scope.getResult();
    }

    $scope.reset();
    init();
});