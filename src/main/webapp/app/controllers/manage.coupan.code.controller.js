app.controller('ManageCouponCodeController', function ($http, $scope, $uibModal, toaster, DateFormatService) {
  $scope.promoCodes = [];
  $scope.coupon = {
    productName: []
  };
  $scope.productNames = ['Blueprint', 'Explore'];
  $http.get('api/promoCode/all').then(function (response) {
    $scope.promoCodes = response.data;
  }, function (error) {
    error(error.data.message);
  })

  $scope.createPromoCode = () => {
    if (!$scope.coupon.name)
      error('Enter Coupon code');
    else if (!$scope.coupon.discount)
      error('Set discount rate');
    else if (!$scope.coupon.couponStartDate)
      error('Set date to valid from');
    else if (!$scope.coupon.couponEndDate)
      error('Set date to valid thru');
    else if (!$scope.coupon.totalCount)
      error('Enter No of times promocode code can be applied');
    else {
      const object = {
        name: $scope.coupon.name,
        discount: $scope.coupon.discount,
        couponStartDate: new Date($scope.coupon.couponStartDate).toISOString(),
        couponEndDate: new Date($scope.coupon.couponEndDate).toISOString(),
        totalCount: $scope.coupon.totalCount,
        productName: $scope.coupon.productName
      };
      if ($scope.coupon.id) {
        delete object['discount'];
        delete object['id'];
        $http.put('api/promoCode/' + $scope.coupon.id, object).then(function (response) {
          $("#myModal").modal('hide');
          ok('Promo code updated successfully');
        }, function (err) {
          error(err.data.message);
        });
      } else {
        $http.post('api/promoCode', object).then(function (response) {
          $scope.promoCodes.push(response.data);
          $("#myModal").modal('hide');
          ok('Promo code created successfully');
        }, function (err) {
          error(err.data.message);
        });
      }
    }
  }

  $scope.validate = function (event) {
    $http.put(`api/promoCode/${event.id}/${event.isValid}`).then(function (response) {
      event.isValid ? ok('Promocode Activated') : ok('Promocode Deactivated');
    }, function (err) {
      error(err.data.message)
    })
  }

  $scope.edit = (event) => {
    $scope.coupon = event;
    $scope.coupon.couponStartDate = new Date(event.couponStartDate);
    $scope.coupon.couponEndDate = new Date(event.couponEndDate);

  }

  // Toggle selection for a given product by name
  $scope.toggleSelection = (productName) => {
    if (!$scope.coupon.productName)
      $scope.coupon.productName = [];
    var idx = $scope.coupon.productName.indexOf(productName);

    // Is currently selected
    if (idx > -1) {
      $scope.coupon.productName.splice(idx, 1);
    }

    // Is newly selected
    else {
      $scope.coupon.productName.push(productName);
    }
  };

  function ok(message) {
    swal({
      title: message,
      type: 'success',
      buttonsStyling: false,
      confirmButtonClass: "btn btn-warning"
    });
  }

  function error(message) {
    swal({
      title: message,
      type: 'error',
      buttonsStyling: false,
      confirmButtonClass: "btn btn-warning"
    });
  }
});