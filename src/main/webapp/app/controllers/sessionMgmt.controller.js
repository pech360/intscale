app.filter('pagination', function()
{
return function(input, start)
{
 start = +start;
 if(input instanceof Array)
 return input.slice(start);
};
});
app.controller('SessionManagementController',function($scope,$http,toaster,$uibModal,$state){
      $scope.curPage = 0;
      $scope.pageSize = 10;
      $scope.TotalRecordCount=0;
      $scope.goToPage = 1;
      $scope.listOfSessions={};
      $scope.session={};

     $scope.setCurrentPage = function() {
         $scope.curPage = 0;
     };

     $scope.numberOfPages = function() {
         $scope.pages = [];
         $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
         for (var i = 1; i <= $scope.totalPages; i++) {
             $scope.pages.push(i);
         }
         return $scope.totalPages;
     };

   $scope.list=function(){
        $http.get('/api/session/').then(function(response){
         $scope.listOfSessions=response.data;
         $scope.TotalRecordCount=response.data.length;
        },function(response){
           toaster.pop('Info',response.error);
        });
   };

   $scope.list();

   $scope.openSessionModal =  function () {
           $scope.modalInstance = $uibModal.open({
               templateUrl: 'createSession.html',
               size: 'lg',
               scope: $scope,
               backdrop: 'static'
           });
       };

   $scope.cancel = function() {
          $scope.modalInstance.close();
      };

   $scope.createNew = function() {
       $scope.session={};
       $scope.openSessionModal();
   };
   $scope.create=function(session){
          if(!session.session){
          toaster.pop('Info',"Session can't be empty");
          return;
          }
     if(session.id){
       $http.put('/api/session/',session).then(function(response){
           $scope.list();
           $scope.cancel();
           ok('Record successfully updated');
          },function(response){
           error(response.data.error);
          });
       }else{
          $http.post('/api/session/',session).then(function(response){
          $scope.list();
          $scope.cancel();
          ok('Record successfully created');
          },function(response){
          error(response.data.error);
          });
     }
   };

   $scope.update=function(session){
     $scope.session=session;
     $scope.openSessionModal();
   };

   function ok(message) {
             swal({
                 title: message,
                 type: 'success',
                 buttonsStyling: false,
                 confirmButtonClass: "btn btn-warning"
             });
         };
     function error(message) {
         swal({
             title: message,
             type: 'error',
             buttonsStyling: false,
             confirmButtonClass: "btn btn-warning"
         });
     };

     $scope.deleteRecord=function(id){
       swal({
             title: 'Are you sure?',
             text: "You won't be able to revert this!",
             type: 'warning',
             showCancelButton: true,
             confirmButtonColor: '#3085d6',
             cancelButtonColor: '#d33',
             confirmButtonText: 'Yes'
         }).then(function (result) {
             if (result.value) {
             $http.delete('/api/session?id='+id).then(function(response){
               $scope.list();
               ok('Record deleted successfully');
             },
              function (response) {
                  error(response.data.error);
              });
              }
         });
     };
});