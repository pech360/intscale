app.controller('userManagement', function ($scope, $http, Auth, CommonService, $state, toaster, $uibModal, DateFormatService) {

    $scope.orgName = window.location.hostname.split(".")[0];
    $scope.isEnable = false;
    $scope.page = 1;
    $scope.pageSize = 100;
    $scope.totalRecords = 0;
    $scope.roleId = '';
    $scope.tenantId = '';
    $scope.schoolName = '';
    $scope.query = '';
    $scope.streams = ["Science", "Commerce", "Humanities"];
    $scope.selectAllWithSearch = false;
    $scope.loadingAssignSponsors = false;

    var date = new Date();
    var ss = date.getSeconds();
    var mm = date.getMinutes();
    var hh = date.getHours();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();


    $scope.fromDate = new Date(y, m, d - 1, hh, mm, ss);
    $scope.toDate = new Date();


    $scope.shiftFromDate = function (to) {
        switch (to) {
            case "lastWeek":
                fromD = new Date();
                fromD = moment(fromD).subtract(1, 'weeks');
                fromD = fromD.set({'hour': hh, 'minute': mm, 'second': ss});
                fromD = fromD.format('MM/DD/YYYY HH:mm:ss');
                $scope.fromDate = new Date(fromD);
                break;
            case "lastMonth":
                fromD = new Date();
                fromD = moment(fromD).subtract(1, 'months');
                fromD = fromD.set({'hour': hh, 'minute': mm, 'second': ss});
                fromD = fromD.format('MM/DD/YYYY HH:mm:ss');
                $scope.fromDate = new Date(fromD);
                break;
        }

        userListForTenant($scope.roleId, $scope.tenantId, $scope.schoolName, false);
    };


    function refresh() {

        // $http.get('/api/user/self/list').then(function (response) {
        //     $scope.users = response.data;
        //     $scope.TotalRecordCount = response.data.length;
        //
        // });
        $http.get('/api/user/self/roles/').then(function (response) {
            $scope.roles = response.data;
        });

        $http.get('/api/school/all').then(function (response) {
            $scope.listSchools = response.data;
        });

        $http.get('/api/user/self/tenants').then(function (response) {
            $scope.listTenants = response.data;
        });


        $scope.userListForTenant($scope.roleId, $scope.tenantId, $scope.schoolName, false);

    }

    $scope.isAllSelected = function (items) {
        if (items) {
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                if (!item.selected) {
                    return false;
                }
            }
            return true;
        }

    };

    $scope.toggleSelectAllWithSearch = function () {
        $scope.selectAllWithSearch = !$scope.selectAllWithSearch;
        $scope.selectAll($scope.users, $scope.selectAllWithSearch)
    };

    $scope.selectAll = function (items, selected) {

        if (selected) {
            for (var i = 0; i < items.length; i++) {
                items[i].selected = true;
            }

            return items;
        } else {
            $scope.selectAllWithSearch = false;
            unselectAll(items);
        }
    };

    function unselectAll(items) {
        for (var i = 0; i < items.length; i++) {
            items[i].selected = false;
        }
        return items;
    }


    $scope.userListForTenant = function (roleId, tenantId, schoolName, loadMore) {
        if (loadMore) {
            $scope.page = $scope.page + 1;
        } else {
            $scope.page = 1;
            $scope.users = [];
        }

        var fromDate = DateFormatService.formatDate9($scope.fromDate);
        var toDate = DateFormatService.formatDate9($scope.toDate);


        $scope.loader();
        $http.get('/api/user/self/userList?roleId=' + roleId + '&tenantId=' + tenantId + '&schoolName=' + schoolName + '&query=' + $scope.query + '&fromDate=' + fromDate + '&toDate=' + toDate + '&pageSize=' + $scope.pageSize + '&page=' + $scope.page).then(function (response) {

            $scope.pageSize = response.data.pageSize;
            $scope.page = response.data.page;
            $scope.totalRecords = response.data.totalRecords;
            if (response.data.records && response.data.records.length > 0) {
                for (var i in response.data.records) {
                    response.data.records[i].selected = false;
                    $scope.users.push(response.data.records[i]);
                }
            }


            $scope.users.selectedCount = function () {
                var count = 0;
                for (var i = 0; i < this.length; i++) {
                    if (this[i].selected) {
                        count++;
                    }
                }
                return count;
            };
            $scope.cancelLoader();
        }, function (response) {
            $scope.cancelLoader();
        });
    };

    $scope.checkUser = function () {
        var user = Auth.getUser();
        if (user.userType == 'Admin') {
            $scope.isEnable = true;
        }
    };
    $scope.checkUser();

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.newUser = function () {
        $scope.workingUser = {
            mode: 'New',
            active: true,
            roles: []
        };
        $scope.editUserPanel = true;
        $scope.showResetPassword = false;
    };

    $scope.updateUser = function (user) {
        $scope.editUserPanel = true;
        $scope.workingUser = user;
        $scope.showResetPassword = true;

    };

    $scope.cancelUser = function () {
        $scope.workingUser = {};
        $scope.showUserPanel = false;
        $scope.editUserPanel = false;
    };

    $scope.removeUser = function (user) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Deactivate!'
        }).then(function (result) {
            if (result.value) {
                $http.delete('/api/user/self?userId=' + user.id).then(function () {
                    ok("User has been deactivated.");
                    refresh();
                }, function (response) {
                    error(response.data.error);
                });
            }
        })
    };

    $scope.selectedRole = {
        name: ""
    };

    $scope.removeRole = function (index) {
        if ($scope.workingUser) {
            $scope.workingUser.roles.splice(index, 1)
        }
    };

    $scope.addRole = function (selectedRole) {
        if ($scope.workingUser) {

            var alreadyThere = false;
            for (var i = 0; i < $scope.workingUser.roles.length; i++) {
                if ($scope.workingUser.roles[i] == selectedRole.name) {
                    alreadyThere = true;
                }
            }
            if (!alreadyThere) {
                $scope.workingUser.roles.push(selectedRole.name);
            }
        }
    };

    $scope.saveUser = function (user) {
        if (user.id) {
            $http.put('/api/user/self/update', user).then(function () {
                ok("User updated!");
                refresh();
            }, function (response) {
                error(response.data.error);
            });
        } else {
            $http.post('/api/user/self', user).then(function () {
                refresh();
                ok("User created!");
            }, function (response) {
                error(response.data.error);
            });
        }
    };

    $scope.resetPassword = function (user) {
        swal({
            title: "New Password",
            input: "text",
            showCancelButton: true,
            closeOnConfirm: false,
            buttonsStyling: false,
            inputPlaceholder: "New Password"
        }).then(function (inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("Missing password");
                return false;
            }
            $http.put('/api/user/self/updatePassword', {
                id: user.id,
                password: inputValue.value
            }).then(function () {
                ok("Password changed successfully");
            }, function (response) {
                error(response.data.error);
            });
        });
    };


    $scope.downloadUsersCsv = function (roleId, tenantId) {
        $http.get('/api/user/self/downloadUserList?roleId=' + roleId + '&tenantId=' + tenantId, {responseType: 'arraybuffer'}).then(function (response) {
            var blob = new Blob([response.data], {
                type: 'application/octet-stream'
            });
            saveAs(blob, response.headers("fileName"));
        });
    };

    $scope.downloadIndividualReport = function (id, updateReport) {
        $scope.dataLoading = true;
        $scope.loader();
        $http.get('api/reports/downloadAimReport/' + id + '/' + updateReport + '/MAIN', {responseType: 'arraybuffer'}).then(function (response) {
            $scope.cancelLoader();
            $scope.dataLoading = false;
            var blob = new Blob([response.data], {
                type: 'application/octet-stream'
            });
            saveAs(blob, response.headers("fileName"));
        }, function (response) {
            $scope.cancelLoader();
            $scope.dataLoading = false;
            error("download failed");
        })
    };


    $scope.openSponsorsModal = function () {
        $scope.sponsorSelectModal = $uibModal.open({
            templateUrl: 'views/modals/sponsorSelectModal.html',
            size: 'lg',
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.closeSponsorsModal = function () {
        $scope.sponsorSelectModal.close();
    };


    $scope.loader = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.cancelLoader = function () {
        $scope.modalInstance.close();
    };

    $scope.listSponsors = function () {
        $http.get('/api/sponsor/list').then(function (response) {
            $scope.sponsors = response.data;
            if ($scope.sponsors && $scope.sponsors.length > 0) {
                for (var i in response.data) {
                    $scope.sponsors[i].selected = false;
                }
            }

            $scope.sponsors.selectedCount = function () {
                var count = 0;
                for (var i = 0; i < this.length; i++) {
                    if (this[i].selected) {
                        count++;
                    }
                }
                return count;
            };

        }, function (response) {
            toaster.pop('Info', response.error);
        });
    };

    function getSelectedItemIds(items) {
        var selectedIds = [];
        for (var i = 0; i < items.length; i++) {
            if (items[i].selected) {
                selectedIds.push(items[i].id);
            }
        }
        return selectedIds;
    }

    $scope.assignSponsorsToUsers = function () {
        var userIds = getSelectedItemIds($scope.users);
        var sponsorIds = getSelectedItemIds($scope.sponsors);
        var fromDate = DateFormatService.formatDate9($scope.fromDate);
        var toDate = DateFormatService.formatDate9($scope.toDate);



        $scope.loadingAssignSponsors = true;
        $http.post('/api/sponsor/assign/toUser', {
            roleId: $scope.roleId,
            tenantId: $scope.tenantId,
            schoolName: $scope.schoolName,
            query: $scope.query,
            useSearchQuery: $scope.selectAllWithSearch,
            fromDate:fromDate,
            toDate:toDate,
            userIds: userIds,
            sponsorIds: sponsorIds
        }).then(function () {
            $scope.loadingAssignSponsors = false;
            ok("sponsors assinged succesfully");
            $scope.closeSponsorsModal();
        }, function (response) {
            $scope.loadingAssignSponsors = false;
            error(response.data.error);
        });
    };


    $scope.listSponsors();

    refresh();


});
