app.controller('LandingController', function ($scope, $rootScope) {
    $scope.loadScript = true;

    $scope.readMore1 = false;
    $scope.readMore2 = false;
    $scope.readMore3 = false;

    $(document).ready(function () {
        // the body of this function is in assets/js/now-ui-kit.js
        nowuiKit.initSliders();
    });

    function scrollToDownload() {

        if ($('.section-download').length != 0) {
            $("html, body").animate({
                scrollTop: $('.section-download').offset().top
            }, 1000);
        }
    }


    $(function () {
        $('#typed').typed({
            strings: ["You discover your strengths?", "You find your best fit?", "You do what you love?"],
            typeSpeed: 100,
            backDelay: 1500,
            loop: true,
            callback: function () {
            }
        });
    });
});