app.controller('QuestionTagController', function ($http, $scope) {
    $scope.tags = [];
    $scope.subtags = [];
    $scope.workingTag = {};
    $scope.workingSubTag = {};
    $scope.selectedType = 'QUESTION';

    $scope.selectTab = function (index) {
        switch (index) {
            case 1:
                $('#tag').addClass("active");
                $('#subtag').removeClass("active");
                $scope.showTag = true;
                $scope.showSubTag = false;
                break;
            case 2:
                $('#subtag').addClass("active");
                $('#tag').removeClass("active");
                $scope.showTag = false;
                $scope.showSubTag = true;
                break;
        }
    };

    $scope.addTag = function () {
        $scope.workingTag = {mode: "New"};
        $scope.showTag = true;
        $scope.showSubTag = false;

    };
    $scope.addSubTag = function () {
        $scope.workingSubTag = {mode: "New"};
        $scope.showTag = false;
        $scope.showSubTag = true;

    };

    $scope.saveTag = function (tag) {
        tag.type = $scope.selectedType;
        if (tag && tag.id) {
            $http.put("/api/tag", tag).then(
                function (response) {
                    ok("Saved");
                    $scope.loadTags($scope.selectedType);
                },
                function (response) {
                    error(response.data.error)
                }
            );
        } else {
            $http.post("/api/tag", tag).then(
                function (response) {
                    ok("Saved");
                    $scope.loadTags($scope.selectedType);
                },
                function (response) {
                    error(response.data.error)
                }
            );
        }
    };

    $scope.saveSubTag = function (subtag) {
        subtag.type = $scope.selectedType;
        if (subtag && subtag.id) {
            $http.put("/api/subtag", subtag).then(
                function (response) {
                    // TODO : change message
                    ok("Saved");
                    $scope.loadSubTags($scope.selectedType);
                },
                function (response) {
                    error(response.data.error)
                }
            );
        } else {
            $http.post("/api/subtag", subtag).then(
                function (response) {
                    // TODO : change message
                    ok("Saved");
                    $scope.loadSubTags($scope.selectedType);
                },
                function (response) {
                    error(response.data.error)
                }
            );
        }
    };

    $scope.tagChanged = function (id) {
        $scope.workingSubTag.tagId = id;
    };

    $scope.editTag = function (tag) {
        $scope.addTag();
        $scope.workingTag = tag;
    };

    $scope.editSubTag = function (subtag) {
        $scope.addSubTag();
        $scope.workingSubTag = subtag;
    };


    $scope.loadTags = function (type) {
        $scope.selectedType = type;
        $http.get('/api/tagByType?type=' + $scope.selectedType).then(
            function (response) {
                $scope.tags = response.data;
            },
            function (response) {
                error(response.data.error)
            }
        );
    };

    $scope.loadSubTags = function (type) {
        $scope.selectedType = type;
        $http.get('/api/subtagByType?type=' + $scope.selectedType).then(
            function (response) {
                $scope.subtags = response.data;
            },
            function (response) {
                error(response.data.error)
            }
        );
    };


    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

     $scope.loadTags('QUESTION');
     $scope.loadSubTags('QUESTION');

});