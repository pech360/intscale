app.filter('pagination', function()
{
return function(input, start)
{
 start = +start;
 if(input instanceof Array)
 return input.slice(start);
};
});
app.controller('questionsController', function ($scope,$parse, $http, Auth, CommonService, $state, toaster,$uibModal) {
  $scope.active='';
  $scope.quesTag='';
  $scope.showLoadingIcon = false;
  $scope.noNeedQuesImageUpdate=true;
  $scope.noNeedQuesAudioUpdate=true;
  $scope.questionUpdate=false;
  $scope.questionData={};
  $scope.mainAnsArray=[];
  $scope.updateAnswerIndex=[];
  $scope.quesImageURI;
  $scope.quesAudio;
  $scope.answerObject={};
  $scope.answerAudio=[];
  $scope.audioName=[];
  $scope.answerImage=[];
  $scope.imageName=[];
  $scope.groupData=false;
  $scope.curPage = 0;
  $scope.pageSize = 20;
  $scope.goToPage = 1;
  $scope.TotalRecordCount=0;
  $scope.deleteQuestionArray=[];
  var countQuestion = 0;
  $scope.questionData.questionType='';
  $scope.index0=false;
  $scope.index1=false;
  $scope.index2=false;
  $scope.index3=false;
  $scope.index4=false;
  $scope.index5=false;
  $scope.index6=false;

  $scope.selectedSubtags = [];

  $scope.setCurrentPage = function() {
      $scope.curPage = 0;
  };

  $scope.numberOfPages = function() {
      $scope.pages = [];
      $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
      for (var i = 1; i <= $scope.totalPages; i++) {
          $scope.pages.push(i);
      }
      return $scope.totalPages;
  };

  $scope.fetchQuestionListByFilter=function(){
     $scope.loader();
     $http.get('/api/question/listByFilter?questionType='+$scope.quesType+'&subtagId='+$scope.quesTag).then(function(response){
      $scope.tableOfQuestions=response.data;
      $scope.TotalRecordCount=response.data.length;
      for (countQuestion = 0; countQuestion <$scope.TotalRecordCount; countQuestion++) {
              $scope.tableOfQuestions[countQuestion].selected = false;
          }
      $scope.cancel();
      if(response.data.length==0){toaster.pop('Info',"Question list is empty.");}
    });
    };
    $http.get('/api/subtag').then(function(response){
     $scope.questionTags=response.data;
    });
    $scope.isSelectAllQuestions = function () {
           if ($scope.selectAllQuestions) {
               $scope.deleteQuestionArray=[];
               for (countQuestion=0; countQuestion<$scope.TotalRecordCount; countQuestion++) {
                $scope.tableOfQuestions[countQuestion].selected = true;
                $scope.deleteQuestionArray.push($scope.tableOfQuestions[countQuestion].id);
               }
           } else {
               for (countQuestion=0; countQuestion<$scope.TotalRecordCount; countQuestion++) {
                    $scope.tableOfQuestions[countQuestion].selected = false;
                }
                $scope.deleteQuestionArray=[];
           }
    };
    $scope.isQuestionSelected = function (record) {
            if (record.selected){
              $scope.deleteQuestionArray.push(record.id);
            }else{
               var i=0;
              $scope.deleteQuestionArray.forEach(function(element){
               if(element==record.id){
                $scope.deleteQuestionArray.splice(i, 1);
               }
               i++;
              });
              i=0;
            }
        };
   $scope.removeAllQuestion=function(){
       if($scope.deleteQuestionArray.length<1){
          toaster.pop('Info',"Question is not selected");
                 return;
         }
        swal({
             title: 'Are you sure delete all?',
             text: "You won't be able to revert this!",
             type: 'warning',
             showCancelButton: true,
             confirmButtonColor: '#3085d6',
             cancelButtonColor: '#d33',
             confirmButtonText: 'Yes, Delete!'
         }).then(function (result) {
            if (result.value) {
                $scope.processingLoader();
                $http.delete('/api/question/deleteAll?arrayOfQuestionId=' + $scope.deleteQuestionArray).then(function () {
                    $scope.cancelProcessLoader();
                    ok("All Record has been deleted.");
                    //$scope.fetchQuestionListByFilter();
                }, function (response) {
                    error(response.data.error);
                });
            }
         })
       };
   $scope.loader = function(){
       $scope.modalInstance = $uibModal.open({
           templateUrl: 'views/loader.html',
           size: 'sm',
           scope: $scope,
           backdrop: 'static'
       });
   };
  $scope.cancel = function() {
          $scope.modalInstance.close();
      };
  $scope.processingLoader=function(){
      $scope.modalInstanceProcess = $uibModal.open({
               templateUrl: 'processing.html',
               size: 'sm',
               scope: $scope,
               backdrop: 'static'
           });
      };
   $scope.cancelProcessLoader = function() {
             $scope.modalInstanceProcess.close();
         };

  $scope.questionTypes=["Mcq","Group","Paragraph","Audio","Image","Text","Slider"];

  $scope.showTab=function(num){
       $scope.index0=false;
       $scope.index1=false;
       $scope.index2=false;
       $scope.index3=false;
       $scope.index4=false;
       $scope.index5=false;
       $scope.index6=false;
       if(num==0){
       $scope.index0=true;
       } else if(num==1){
       $scope.index1=true;
       }else if(num==2){
       $scope.index2=true;
       }else if(num==3){
       $scope.index3=true;
       }else if(num==4){
       $scope.index4=true;
       }else if(num==5){
       $scope.index5=true;
       }else if(num==6){
       $scope.index6=true;
       }
  };

  $scope.showTabByType=function(typ){
         $scope.index0=false;
         $scope.index1=false;
         $scope.index2=false;
         $scope.index3=false;
         $scope.index4=false;
         $scope.index5=false;
         $scope.index6=false;
         if(typ=='Mcq'){
         $scope.index0=true;
         }else if(typ=='Group'){
         $scope.index1=true;
         }else if(typ=='Paragraph'){
         $scope.index2=true;
         }else if(typ=='Audio'){
         $scope.index3=true;
         }else if(typ=='Image'){
         $scope.index4=true;
         }else if(typ=='Text'){
         $scope.index5=true;
         }if(typ=='Slider'){
         $scope.index6=true;
         }
    };

  $scope.selectTab=function(index){
    $scope.active=parseInt(index);
    $scope.questionData.questionType=$scope.questionTypes[parseInt(index)];
    $scope.showTab(index);
    $scope.buildDynamicArray($scope.questionData.numberOfAnswers);
  };

  $scope.buildDynamicArray=function(numb){
  if(numb>10){numb=10}
  $scope.mainAnsArray=[];
    for(var i=0;i<numb;i++){
     var newAnswer = {"id":"","description":'',"marks":'',"hasText":false,"hasImage":false,"imageName":null,"hasAudio":false,"audioName":null,"startPoint":'',"endPoint":'',"sliderType":''};
     $scope.mainAnsArray.push(newAnswer);
     //$scope.answerImage.push("/assets/img/default-profile-picture.png");
    }
    $scope.answerObject=newAnswer;
  };
  $scope.selectAnswerType=function(num){
   $scope.questionType=parseInt(num);
   $scope.questionData.questionType=$scope.questionTypes[parseInt(num)];
   $scope.showTab(num);
   $scope.buildDynamicArray($scope.questionData.numberOfAnswers);
  };

  $scope.clearData=function(inputType,flag){
   if(inputType=='questionTime'&&!flag){
       $scope.questionData.timeInSeconds='';
   }else if(inputType=='questionImage'&&!flag){
    $scope.imgfile='';
    $scope.quesImageURI='';
   }else if(inputType=='questionAudio'&&!flag){
    $scope.audiofile='';
    $scope.quesAudio='';
   }else if(inputType=='questionPara'&&!flag){
    $scope.questionData.paraWord='';
   }else if(inputType=='questionVideoCreate'&&!flag){
    $scope.quesVideoURI='';
    $scope.quesVideoFile='';
   }
  };
   $scope.resetAllFunctionData=function(){
      $scope.buildDynamicArray($scope.questionData.numberOfAnswers);
       $scope.active='';
       $scope.noNeedQuesImageUpdate=true;
       $scope.noNeedQuesAudioUpdate=true;
       $scope.questionUpdate=false;
       $scope.questionData={};
       $scope.mainAnsArray=[];
       $scope.updateAnswerIndex=[];
       $scope.quesImageURI;
       $scope.quesAudio;
       $scope.answerObject={};
       $scope.answerAudio=[];
       $scope.audioName=[];
       $scope.answerImage=[];
       $scope.imageName=[];
       $scope.groupData=false;
       $scope.imgfile=null;
       $scope.quesImageURI='';
       $scope.audiofile='';
       $scope.quesAudio=null;
       $scope.questionType=null;
        $scope.index0=false;
        $scope.index1=false;
        $scope.index2=false;
        $scope.index3=false;
        $scope.index4=false;
        $scope.index5=false;
        $scope.index6=false;
   };
  $scope.fetchAnswerDataByGroupName=function(groupData){
   if(groupData){
    if(!$scope.questionData.groupName){
      toaster.pop('Error',"Please Enter Group Name");
      return;
    }
    $http.get('/api/question/byGroupName?groupName='+$scope.questionData.groupName).then(function(response){
       $scope.answerList=response.data.answerRequests;
       if($scope.answerList.length!=0){
       for(var x in $scope.answerList){
           var ansArray=$scope.mainAnsArray[x];
           var ansValue=$scope.answerList[x];
           ansArray.id='';
           ansArray.description=ansValue.description;
           ansArray.marks=parseInt(ansValue.marks);
           ansArray.hasText=false;
           ansArray.hasImage=false;
           ansArray.imageName=null;
           ansArray.hasAudio=false;
           ansArray.audioName=null;
          }}else{
           toaster.pop('Info',"Group Name is not exist");
          }
       });
       }/*else{
       $scope.buildDynamicArray($scope.questionData.numberOfAnswers);
       }*/
  };
  $scope.realRandomOptions=function(){
     var length=$scope.mainAnsArray.length;
     var preAnswerArray=$scope.mainAnsArray;
     $scope.buildDynamicArray($scope.questionData.numberOfAnswers);
     $scope.mainAnsArray.forEach(function(element){
     var preAnswer=preAnswerArray[length-1];
     element.description=preAnswer.description;
     element.marks=preAnswer.marks;
     length=length-1;});
  };
  $scope.randomAnswerOptions=function(randomAnswer){
   if(randomAnswer){
    $scope.realRandomOptions();
   }else{
    $scope.realRandomOptions();
   }
  };
  $scope.browseQuestionImage = function () {
      $('#quesImg').trigger('click');
  };
  $scope.setFiles = function (element) {
              $scope.imgfile = element.files[0];
              $scope.noNeedQuesImageUpdate=false;
              displayImage($scope.imgfile);
          };
  function displayImage(file) {
          var reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = function () {
              $scope.$apply(function () {
                  $scope.quesImageURI = reader.result;
              });
          };
          reader.onerror = function (error) {
              error("Failed to load image:" + error);
          };
      };

  $scope.uploadQuestionImage = function (id) {
            var r = new FileReader();
            var fd = new FormData();
            fd.append('file', $scope.imgfile);
            var request = {
                method: 'POST',
                url: '/api/raw/QUESTIONIMAGE/'+id,
                data: fd,
                headers: {
                    'Content-Type': undefined
                }
            };
            $http(request).then(function (response) {
             if($scope.questionData.questionType=='Image'){
               $scope.uploadAnswerImage(id);
               }
               else if($scope.questionData.questionType=='Audio'){
                $scope.uploadAnswerAudio(id);
               }
               else{
                $scope.resetAllFunctionData();
                $scope.updateQuestion=false;
                $scope.showLoadingIcon = false;
                ok("Question created successfully");
               }
               //if($scope.updateQuestion){$scope.fetchQuestionListByFilter();}
            });
        };

  $scope.browseQuestionAudio = function () {
        $('#quesAudio').trigger('click');
    };

  $scope.setAudio = function (element) {
          $scope.audiofile = element.files[0];
          $scope.noNeedQuesAudioUpdate=false;
          playAudio($scope.audiofile);
      };

  function playAudio(file) {
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function () {
          $scope.$apply(function () {
              $scope.quesAudio = reader.result;
          });
      };
      reader.onerror = function (error) {
          error("Failed to load image:" + error);
      };
  };

  $scope.uploadQuestionAudio = function (id) {
            var r = new FileReader();
            var fd = new FormData();
            fd.append('file', $scope.audiofile);
            var request = {
                method: 'POST',
                url: '/api/raw/QUESTIONAUDIO/'+id,
                data: fd,
                headers: {
                    'Content-Type': undefined
                }
            };
            $http(request).then(function (response) {
              if($scope.questionData.questionType=='Audio'){
               $scope.uploadAnswerAudio(id);
              }
              else if($scope.questionData.questionType=='Image'){
               $scope.uploadAnswerImage(id);
              }
              else{
              $scope.resetAllFunctionData();
              $scope.updateQuestion=false;
              $scope.showLoadingIcon = false;
              ok("Question created successfully");
              }
               //if($scope.updateQuestion){$scope.fetchQuestionListByFilter();}
            });
        };
   $scope.quesVideoURI='';
   $scope.openVideoModal=function(flag,type){
   if(!flag){
    toaster.pop('Info',"Please select HasVideo..");
    return;
   }
   if(flag&&type=='create'){
    $scope.showVideoButton=false;
    $scope.modalForQuestionVideo();
   }else if(flag&&type=='update'){
    $scope.showVideoButton=true;
    $scope.modalForQuestionVideo();
   }
   };
   $scope.modalForQuestionVideo=function(){
     $scope.modalInstanceVideo = $uibModal.open({
             templateUrl: 'views/questions/questionVideo.html',
             size: 'lg',
             scope: $scope
         });
   };
   $scope.cancelVideo = function(type) {
         if(type=='cancel'){
          $scope.quesVideoURI='';
          $scope.quesVideoFile='';
          $scope.questionData.hasAnimatedGif=false;
          $scope.modalInstanceVideo.close();
         }else if(type=='close'){
          $scope.modalInstanceVideo.close();
         }

     };
   $scope.browseQuestionVideo = function () {
         $('#questionVideo').trigger('click');
     };

   $scope.setQuestionVideo = function (element) {
           $scope.noNeedQuesImageUpdate=false;
           $scope.quesVideoFile = element.files[0];
           playQuestionVideo($scope.quesVideoFile);
       };

   function playQuestionVideo(file) {
       var reader = new FileReader();
       reader.readAsDataURL(file);
       reader.onload = function () {
           $scope.$apply(function () {
               $scope.quesVideoURI = reader.result;
           });
       };
       reader.onerror = function (error) {
           error("Failed to load image:" + error);
       };
   };

   $scope.uploadQuestionVideo = function (id) {
               var r = new FileReader();
               var fd = new FormData();
               fd.append('file', $scope.quesVideoFile);
               var request = {
                   method: 'POST',
                   url: '/api/raw/QUESTIONIVIDEO/'+id,
                   data: fd,
                   headers: {
                       'Content-Type': undefined
                   }
               };
               $http(request).then(function (response) {
                 if($scope.questionData.questionType=='Image'){
                    $scope.uploadAnswerImage(id);
                    }
                    else if($scope.questionData.questionType=='Audio'){
                     $scope.uploadAnswerAudio(id);
                    }
                    else{
                     $scope.resetAllFunctionData();
                     $scope.updateQuestion=false;
                     $scope.showLoadingIcon = false;
                     ok("Question created successfully");
                    }
                    //if($scope.updateQuestion){$scope.fetchQuestionListByFilter();}
               });
           };

   $scope.save=function(questionData){
         /*if(!$scope.questionData.description){
             toaster.pop('Error',"Please enter question description");
             return;
           }
        if($scope.questionData.description.length<1){
            toaster.pop('Error',"Question description is too short");
            return;
          }*/
         if($scope.questionData.hasAnimatedGif&&!$scope.quesVideoFile||$scope.questionData.hasAnimatedGif&&$scope.quesVideoFile==''){
             toaster.pop('Error',"Please choose Video file OR Unchecked hasVideo box");
             return;
           }
        if($scope.questionData.hasTimer&&!$scope.questionData.timeInSeconds||$scope.questionData.hasTimer&&$scope.questionData.timeInSeconds==''){
          toaster.pop('Error',"Please enter time OR Unchecked hasTimer box");
          return;
        }
        if($scope.questionData.hasImage&&!$scope.imgfile||$scope.questionData.hasImage&&$scope.imgfile==''){
          toaster.pop('Error',"Please choose image file OR Unchecked Image box");
          return;
        }
        if($scope.questionData.hasParagraph&&!$scope.questionData.paraWord||$scope.questionData.hasParagraph&&$scope.questionData.paraWord==''){
          toaster.pop('Error',"Please enter para word OR Unchecked hasPara box");
          return;
        }
        if($scope.questionData.hasAudio&&!$scope.quesAudio||$scope.questionData.hasAudio&&$scope.quesAudio==''){
          toaster.pop('Error',"Please choose audio file OR Unchecked Audio box");
          return;
        }
        if(!$scope.questionData.numberOfAnswers||$scope.questionData.numberOfAnswers==''){
          toaster.pop('Error',"Please enter number of answers");
          return;
        }
        if(!$scope.questionData.questionType){
          toaster.pop('Error',"Please select question type");
          return;
        }
        if($scope.questionData.questionType=='Group'&&!$scope.questionData.groupName){
            toaster.pop('Error',"Please enter Group name");
            return;
          }
        for(var x=0;x<$scope.questionData.numberOfAnswers;x++)
        {
         var ansArray=$scope.mainAnsArray[x];
         if((!ansArray.description||ansArray.description=='')&&($scope.questionData.questionType!='Text')&&($scope.questionData.questionType!='Slider')){
           toaster.pop('Error',"Please enter Answer "+(x+1)+" description");
           return;
         }
         /*if((!ansArray.marks||ansArray.marks==''||ansArray.marks<0)&&($scope.questionData.questionType!='Text')&&($scope.questionData.questionType!='Slider')){
           toaster.pop('Error',"Please enter Answer"+(x+1)+" marks");
           return;
         }*/
         if(($scope.questionData.questionType=='Audio'&&(!$scope.audioName[x]||$scope.audioName[x]==''))&&$scope.questionData.questionType!='Text'){
           toaster.pop('Error',"Please choose Answer"+(x+1)+" Audio file");
           return;
         }
         if(($scope.questionData.questionType=='Image'&&(!$scope.imageName[x]||$scope.imageName[x]==''))&&$scope.questionData.questionType!='Text'){
           toaster.pop('Error',"Please choose Answer"+(x+1)+" Image file");
           return;
         }
        }

       $scope.questionData.subTagIds = getSelectedSubtagsId($scope.selectedSubtags);
       $scope.questionData.answerRequests=$scope.mainAnsArray;
       if (questionData.id) {
           $scope.showLoadingIcon = true;
           $http.put('/api/question/update', questionData).then(function (response) {
               if(questionData.hasImage&&!$scope.noNeedQuesImageUpdate){
                $scope.uploadQuestionImage(response.data.id);
                 //ok("Question created successfully");
                }else if(questionData.hasAnimatedGif&&!$scope.noNeedQuesImageUpdate){
                   $scope.uploadQuestionVideo(response.data.id);
                }
                else if(questionData.hasAudio&&!$scope.noNeedQuesAudioUpdate)
                  {
                   $scope.uploadQuestionAudio(response.data.id);
                   //ok("Question created successfully");
                  }
                else if($scope.questionData.questionType=='Image'){
                $scope.uploadAnswerImage(response.data.id);
                //ok("Question created successfully");
                }
                else if($scope.questionData.questionType=='Audio'){
                 $scope.uploadAnswerAudio(response.data.id);
                 //ok("Question created successfully");
                }else {
                $scope.showLoadingIcon = false;
                ok("Question updated successfully");
                $scope.updateQuestion=false;
                //$scope.fetchQuestionListByFilter();
                $scope.resetAllFunctionData();
               }
           }, function (response) {
               error(response.data.error);
           });
       } else {
           $scope.showLoadingIcon = true;
           $http.post('/api/question/create', questionData).then(function (response) {
              if(questionData.hasImage){
               $scope.uploadQuestionImage(response.data.id);
               //ok("Question created successfully");
              }else if(questionData.hasAnimatedGif){
                  $scope.uploadQuestionVideo(response.data.id);
               }
              else if(questionData.hasAudio)
                {
                 $scope.uploadQuestionAudio(response.data.id);
                 //ok("Question created successfully");
                }
              else if($scope.questionData.questionType=='Image'){
               $scope.uploadAnswerImage(response.data.id);
               //ok("Question created successfully");
              }
              else if($scope.questionData.questionType=='Audio')
              {
               $scope.uploadAnswerAudio(response.data.id);
               //ok("Question created successfully");
              }else {
              $scope.showLoadingIcon = false;
               ok("Question created successfully");
               $scope.resetAllFunctionData();
              }
           }, function (response) {
               error(response.data.error);
           });
       }
    };
  //update and remove Question
   $scope.newRecord = function () {
     $scope.resetAllFunctionData();
     $scope.questionData = {mode: 'New' };
   };

    $scope.updateAndManageQuestions=function(record)
    {
      $scope.updateQuestion=true;
      $scope.questionUpdate=true;
      $scope.questionData = angular.copy(record);
      $scope.selectedSubtags = $scope.questionData.subTags;
      loadSubTags();
      filterSubTags();
      $scope.showTabByType(record.questionType);
      $scope.answerAudio=[];
      $scope.audioName=[];
      $scope.answerImage=[];
      $scope.imageName=[];
      if($scope.questionData.hasImage){
      $http({
            method: 'GET',
            url: '/api/raw/view/QUESTIONIMAGE/' + $scope.questionData.imageName,
            responseType: 'arraybuffer'
        }).then(function (response) {
            $scope.imgfile=$scope.questionData.imageName;
            $scope.quesimgdata=arrayBufferToBase64(response.data);
            $scope.quesImageURI = "data:image/PNG;base64," + $scope.quesimgdata;
        });
    }
    else if($scope.questionData.hasAudio){
     $http({
           method: 'GET',
           url: '/api/raw/view/QUESTIONAUDIO/' + $scope.questionData.audioName,
           responseType: 'arraybuffer'
         }).then(function (response) {
             $scope.quesAudio=$scope.questionData.audioName;
             $scope.quesAudiodata=arrayBufferToBase64(response.data);
             $scope.quesAudio = "data:audio/mpeg;base64," + $scope.quesAudiodata;
         });
    }else if($scope.questionData.hasAnimatedGif){
     $http({
             method: 'GET',
             url: '/api/raw/view/QUESTIONIVIDEO/' + $scope.questionData.videoName,
             responseType: 'arraybuffer'
             }).then(function (response) {
                 $scope.quesVideoFile=$scope.questionData.videoName;
                 $scope.quesVideoData=arrayBufferToBase64(response.data);
                 $scope.quesVideoURI = "data:video/mp4;base64," + $scope.quesVideoData;
             });
    }
    var index = $scope.questionTypes.indexOf(record.questionType);
    $scope.active=parseInt(index);
    $scope.questionType=parseInt(index);
    $scope.buildDynamicArray(record.numberOfAnswers);
      var x=0;
      record.answerRequests.forEach(function(element) {
        if(record.questionType=='Image'&&element.hasImage){
          $http({
                  method: 'GET',
                  url: '/api/raw/view/ANSWERIMAGE/' + element.imageName,
                  responseType: 'arraybuffer'
                }).then(function (response) {
                    $scope.ansimagedata=arrayBufferToBase64(response.data);
                    $scope.ansImage = "data:image/PNG;base64," + $scope.ansimagedata;
                    $scope.answerImage.push($scope.ansImage);
                    $scope.imageName.push(element.imageName);
                     var ansArray=$scope.mainAnsArray[x];
                     ansArray.description=element.description;
                     ansArray.marks=parseInt(element.marks);
                     ansArray.hasText=element.hasText;
                     ansArray.hasImage=element.hasImage;
                     ansArray.imageName=element.imageName;
                     ansArray.hasAudio=element.hasAudio;
                     ansArray.audioName=element.audioName;
                     x=x+1;
                });
          }else if(record.questionType=='Audio'&&element.hasAudio){
          $http({
             method: 'GET',
             url: '/api/raw/view/ANSWERAUDIO/' + element.audioName,
             responseType: 'arraybuffer'
           }).then(function (response) {
               $scope.ansAudiodata=arrayBufferToBase64(response.data);
               $scope.ansAudio = "data:audio/mpeg;base64," + $scope.ansAudiodata;
               $scope.answerAudio.push($scope.ansAudio);
               $scope.audioName.push(element.audioName);
                var ansArray=$scope.mainAnsArray[x];
                ansArray.description=element.description;
                ansArray.marks=parseInt(element.marks);
                ansArray.hasText=element.hasText;
                ansArray.hasImage=element.hasImage;
                ansArray.imageName=element.imageName;
                ansArray.hasAudio=element.hasAudio;
                ansArray.audioName=element.audioName;
                x=x+1;
           });
          }else{
                 var ansArray=$scope.mainAnsArray[x];
                 if(ansArray!=undefined){
                 ansArray.description=element.description;
                 ansArray.marks=parseInt(element.marks);
                 ansArray.hasText=element.hasText;
                 ansArray.hasImage=element.hasImage;
                 ansArray.imageName=element.imageName;
                 ansArray.hasAudio=element.hasAudio;
                 ansArray.audioName=element.audioName;
                 ansArray.startPoint=element.startPoint;
                 ansArray.endPoint=element.endPoint;
                 ansArray.sliderType=element.sliderType;
                 x=x+1;
      }}
    });
  };
    /*$scope.questionPreview=function(){
    $scope.modalInstance = $uibModal.open({
             templateUrl: 'questionPreView.html',
             size: 'lg',
             scope: $scope
         });
    };*/

    $scope.removeQuestion=function(id){
     swal({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, Delete!'
      }).then(function (result) {
         if (result.value) {
             $http.delete('/api/question/delete?id=' + id).then(function () {
                 ok("Record has been deleted.");
                 //$scope.fetchQuestionListByFilter();
             }, function (response) {
                 error(response.data.error);
             });
         }
      })
    };
//******************************************--------------------Audio type answer script start-------------********************************************
$scope.browseAnswerAudio = function (num) {
          $scope.countAudio=num;
          $('#ansAudio-'+num).trigger('click');
      };

    $scope.setAnswerAudio = function (element) {
            $scope.ansAudiofile = element.files[0];
            playAnswerAudio($scope.ansAudiofile);
        };

    function playAnswerAudio(file) {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        if($scope.questionUpdate){
         $scope.updateAnswerIndex[$scope.countAudio]=$scope.countAudio;
        }
        reader.onload = function () {
            $scope.$apply(function () {
              $scope.audioName[$scope.countAudio]=file.name;
              $scope.answerAudio[$scope.countAudio]=reader.result;
            });
        };
        reader.onerror = function (error) {
            error("Failed to load audio:" + error);
        };
    };

$scope.uploadAnswerAudio=function(id){
      var formData = new FormData();
      if($scope.questionUpdate){
           for(var i=0;i<$scope.questionData.numberOfAnswers;i++){
                     var file=document.getElementById('ansAudio-'+i).files[0];
                     if(file){
                      formData.append('files',file);
                     }
                 }
           var audioFiles = formData.getAll("files");//formData.get("files");
             if(audioFiles.length){
                 var request ={
                  method: 'POST',
                  url: '/api/raw/update/ANSWERAUDIO/'+$scope.updateAnswerIndex+'/'+id,
                  data: formData,
                  headers: {
                      'Content-Type': undefined
                  }
                  };
                  $http(request).then(function (response) {
                   //ok("Question updated successfully");
                   $scope.updateQuestion=false;
                   //$scope.fetchQuestionListByFilter();
                   $scope.resetAllFunctionData();
                   $scope.showLoadingIcon = false;
                   ok("Question update successfully");
                  });
             }else{
               //ok("Question updated successfully");
                  $scope.updateQuestion=false;
                  //$scope.fetchQuestionListByFilter();
                  $scope.resetAllFunctionData();
                  $scope.showLoadingIcon = false;
                  ok("Question update successfully");
             }
           }
       else{
        for(var i=0;i<$scope.questionData.numberOfAnswers;i++){
            var file=document.getElementById('ansAudio-'+i).files[0];
            formData.append('files',file);
        }
       var request = {
       method: 'POST',
       url: '/api/raw/answerMedia/ANSWERAUDIO/'+id,
       data: formData,
       headers: {
           'Content-Type': undefined
       }
       };
       $http(request).then(function (response) {
        //ok("Question created successfully");
        //$scope.fetchQuestionList();
        $scope.resetAllFunctionData();
        $scope.showLoadingIcon = false;
        ok("Question created successfully");
       });
       }
    };
//******************************************--------------------Audio type answer script end-------------********************************************

//******************************************--------------------Image type answer script start-------------********************************************
$scope.browseAnswerImage = function (num) {
          $scope.countImage=num;
          $('#ansImage-'+num).trigger('click');
      };

    $scope.setAnswerImage = function (element) {
            $scope.ansImagefile = element.files[0];
            showAnswerImage($scope.ansImagefile);
        };

    function showAnswerImage(file) {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        if($scope.questionUpdate){
                 $scope.alreadyThere = false;
                 for (var ind in $scope.updateAnswerIndex) {
                     if ($scope.updateAnswerIndex[ind] === $scope.countImage) {
                         $scope.alreadyThere = true;
                     }
                 }
                 if (!$scope.alreadyThere) {
                     $scope.updateAnswerIndex.push($scope.countImage);
                     $scope.alreadyThere = false;
                 }
        }
        reader.onload = function () {
            $scope.$apply(function () {
               $scope.imageName[$scope.countImage]=file.name;
               $scope.answerImage[$scope.countImage]=reader.result;
            });
        };
        reader.onerror = function (error) {
            error("Failed to load image:" + error);
        };
    };

$scope.uploadAnswerImage=function(id){
         var formData = new FormData();
         if($scope.questionUpdate){
           for(var i=0;i<$scope.questionData.numberOfAnswers;i++){
                  var file=document.getElementById('ansImage-'+i).files[0];
                  if(file){
                    formData.append('files',file);
                  }
              }
             var imageFiles = formData.getAll("files");
             if(imageFiles.length){
             var request ={
             method: 'POST',
             url: '/api/raw/update/ANSWERIMAGE/'+$scope.updateAnswerIndex+'/'+id,
             data: formData,
             headers: {
                 'Content-Type': undefined
             }
             };
             $http(request).then(function (response) {
               //ok("Question updated successfully");
               $scope.updateQuestion=false;
               //$scope.fetchQuestionListByFilter();
               $scope.resetAllFunctionData();
               $scope.showLoadingIcon = false;
               ok("Question updated successfully");
             });
             }else{
                //ok("Question updated successfully");
                $scope.updateQuestion=false;
                //$scope.fetchQuestionListByFilter();
                $scope.resetAllFunctionData();
                $scope.showLoadingIcon = false;
                ok("Question updated successfully");
             }
         }
         else{
         for(var i=0;i<$scope.questionData.numberOfAnswers;i++){
             var file=document.getElementById('ansImage-'+i).files[0];
             formData.append('files',file);
         }
         var request = {
         method: 'POST',
         url: '/api/raw/answerMedia/ANSWERIMAGE/'+id,
         data: formData,
         headers: {
             'Content-Type': undefined
         }
         };
         $http(request).then(function (response) {
           //ok("Question created successfully");
           //$scope.fetchQuestionList();
           $scope.resetAllFunctionData();
           $scope.showLoadingIcon = false;
           ok("Question created successfully");
         });
       }
    };
//******************************************--------------------Image type answer script end-------------********************************************
//**********************************Add Additional activity for groups************************************
 $scope.activity={};
 $scope.actImageURI='';
 $scope.actAudioURI='';
 $scope.actVideoURI='';

 $scope.fetchGroupActivityData=function(groupName){
  $http.get('/api/question/getGroupText?groupName='+groupName).then(function(response){
   $scope.groupTextData=response.data;
   $scope.activity = angular.copy($scope.groupTextData);
   if($scope.groupTextData.commonType=='Image'){
        $http({
        method: 'GET',
        url: '/api/raw/view/GROUPACTIVITY/' + $scope.groupTextData.fileName,
        responseType: 'arraybuffer'
        }).then(function (response) {
            $scope.actImgData=arrayBufferToBase64(response.data);
            $scope.actImageURI = "data:image/PNG;base64," + $scope.actImgData;
            toaster.pop('Info',"Group Activity data loaded successfully");
            $scope.modalForCommonActivity();
        });
   }else if($scope.groupTextData.commonType=='Audio'){
        $http({
        method: 'GET',
        url: '/api/raw/view/GROUPACTIVITY/' + $scope.groupTextData.fileName,
        responseType: 'arraybuffer'
        }).then(function (response) {
            $scope.actAudioData=arrayBufferToBase64(response.data);
            $scope.actAudioURI = "data:audio/mpeg;base64," + $scope.actAudioData;
            toaster.pop('Info',"Group Activity data loaded successfully");
            $scope.modalForCommonActivity();
        });
   }else if($scope.groupTextData.commonType=='Video'){
        $http({
        method: 'GET',
        url: '/api/raw/view/GROUPACTIVITY/' + $scope.groupTextData.fileName,
        responseType: 'arraybuffer'
        }).then(function (response) {
            $scope.actVideoData=arrayBufferToBase64(response.data);
            $scope.actVideoURI = "data:video/mp4;base64," + $scope.actVideoData;
            toaster.pop('Info',"Group Activity data loaded successfully");
            $scope.modalForCommonActivity();
        });
   }else if($scope.groupTextData.commonType=='Text'){
        toaster.pop('Info',"Group Activity data loaded successfully");
        $scope.modalForCommonActivity();
   }else{
        toaster.pop('Info',"Group Activity not found..");
        $scope.resetCommonActivity();
        $scope.modalForCommonActivity();
   }
  });
 };
 $scope.resetCommonActivity=function(){
     $scope.actVideoFile='';
     $scope.actAudioFile='';
     $scope.actImgFile='';
     $scope.activity={};
 };
 $scope.addCommonActivity=function(){
      if(!$scope.questionData.groupName){
        toaster.pop('Error',"Group name can't be empty");
        return;
      }
      $scope.fetchGroupActivityData($scope.questionData.groupName);
 };

 $scope.modalForCommonActivity=function(){
       $scope.modalInstanceCommon = $uibModal.open({
                templateUrl: 'commonActivity.html',
                size: 'lg',
                scope: $scope
            });
       };
    $scope.cancelCommon = function() {
              $scope.modalInstanceCommon.close();
          };
  $scope.activityTypes=['Video','Audio','Image','Text'];

    $scope.browseActivityImage = function () {
        $('#activityImg').trigger('click');
    };
    $scope.setActivityImage = function (element) {
                $scope.actImgFile = element.files[0];
                displayActivityImage($scope.actImgFile);
            };
    function displayActivityImage(file) {
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {
                $scope.$apply(function () {
                    $scope.actImageURI = reader.result;
                });
            };
            reader.onerror = function (error) {
                error("Failed to load image:" + error);
            };
        };
   $scope.uploadActivityImage=function(activity){
       var r = new FileReader();
       var fd = new FormData();
       fd.append('file', $scope.actImgFile);
       var request = {
       method: 'POST',
       url: '/api/raw/groupActivity/'+activity.commonType+'/'+activity.commonText+'/'+$scope.questionData.groupName,
       data: fd,
       headers: {
       'Content-Type': undefined
       }
       };
       $http(request).then(function (response) {
       ok('Group Activity Image save successfully');
       $scope.actImgFile='';
       $scope.cancelCommon();
       $scope.activity={};
       });
    };

    $scope.browseActivityAudio = function () {
            $('#activityAudio').trigger('click');
        };

      $scope.setActivityAudio = function (element) {
              $scope.actAudioFile = element.files[0];
              playActivityAudio($scope.actAudioFile);
          };

      function playActivityAudio(file) {
          var reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = function () {
              $scope.$apply(function () {
                  $scope.actAudioURI = reader.result;
              });
          };
          reader.onerror = function (error) {
              error("Failed to load image:" + error);
          };
      };
   $scope.uploadActivityAudio = function (activity) {
           var r = new FileReader();
           var fd = new FormData();
           fd.append('file', $scope.actAudioFile);
           var request = {
               method: 'POST',
               url: '/api/raw/groupActivity/'+activity.commonType+'/'+activity.commonText+'/'+$scope.questionData.groupName,
               data: fd,
               headers: {
                   'Content-Type': undefined
               }
           };
           $http(request).then(function (response) {
             ok('Group Activity Audio save successfully');
             $scope.actAudioFile='';
             $scope.cancelCommon();
             $scope.activity={};
           });
       };

   $scope.browseActivityVideo = function () {
              $('#activityVideo').trigger('click');
          };

    $scope.setActivityVideo = function (element) {
            $scope.actVideoFile = element.files[0];
            playActivityVideo($scope.actVideoFile);
        };

    function playActivityVideo(file) {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            $scope.$apply(function () {
                $scope.actVideoURI = reader.result;
            });
        };
        reader.onerror = function (error) {
            error("Failed to load image:" + error);
        };
    };
   $scope.uploadActivityVideo = function (activity) {
          var r = new FileReader();
          var fd = new FormData();
          fd.append('file', $scope.actVideoFile);
          var request = {
              method: 'POST',
              url: '/api/raw/groupActivity/'+activity.commonType+'/'+activity.commonText+'/'+$scope.questionData.groupName,
              data: fd,
              headers: {
                  'Content-Type': undefined
              }
          };
          $http(request).then(function (response) {
            ok('Group Activity Video save successfully');
            $scope.actVideoFile='';
            $scope.cancelCommon();
            $scope.activity={};
          });
      };

    $scope.saveActivityText=function(activity){
     $http.post('/api/question/addGroupText',activity).then(function(response){
      if(response.data){
         ok('Group Activity Text save successfully');
         $scope.cancelCommon();
         $scope.activity={};
      }
     });
    };
    $scope.saveCommonActivity=function(activity){
      $scope.activity.groupName=$scope.questionData.groupName;
      if(!activity.commonType){
        toaster.pop('Error',"please select Group activity type");
        return;
      }
      if(activity.commonType=='Image'){
        if(!$scope.actImgFile||$scope.actImgFile==''){
           toaster.pop('Error',"please choose Group activity Image");
           return;
        }
       $scope.uploadActivityImage(activity);
      }else if(activity.commonType=='Audio'){
        if(!$scope.actAudioFile||$scope.actAudioFile==''){
           toaster.pop('Error',"please choose Group activity Audio");
           return;
        }
      $scope.uploadActivityAudio(activity);
      }else if(activity.commonType=='Video'){
        if(!$scope.actVideoFile||$scope.actVideoFile==''){
           toaster.pop('Error',"please choose Group activity Video");
           return;
        }
      $scope.uploadActivityVideo(activity);
      }else if(activity.commonType=='Text'){
       if(!activity.commonText){
          toaster.pop('Error',"Text field can't be blank");
          return;
       }
      $scope.saveActivityText(activity);
     }
    };

//******************************************End script common activity for group question***************************

 function arrayBufferToBase64( buffer ) {
         var binary = '';
         var bytes = new Uint8Array( buffer );
         var len = bytes.byteLength;
         for (var i = 0; i < len; i++) {
             binary += String.fromCharCode( bytes[ i ] );
         }
         return window.btoa( binary );
     };
 function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    };
function error(message) {
    swal({
        title: message,
        type: 'error',
        buttonsStyling: false,
        confirmButtonClass: "btn btn-warning"
    });
};

    function loadSubTags() {
        $http.get('/api/subtag').then(
            function (response) {
                $scope.subtags = response.data;
            },
            function (response) {
                error(response.data.error)
            }
        );
    }

    loadSubTags();

    $scope.addSubTag = function (subtag) {
        if (subtag) {
            for (var i = 0; i < $scope.subtags.length; i++) {
                if (subtag== $scope.subtags[i].id) {
                    var v = $scope.subtags.splice(i, 1)[0];
                    $scope.selectedSubtags.push(v);
                    return;
                }
            }
        }
    };

    function filterSubTags() {
        for (var i = 0; i < $scope.subtags.length; i++) {
            for (var j = 0; j < $scope.selectedSubtags.length; j++) {
                if ($scope.subtags[i].id == $scope.selectedSubtags[j].id) {
                    $scope.subtags.splice(i, 1);
                }
            }
        }
    }

    $scope.removeSubTag = function (tag) {
        if (tag) {
            for (i = 0; i < $scope.selectedSubtags.length; i++) {
                if ($scope.selectedSubtags[i].id == tag.id) {
                    var subTag = $scope.selectedSubtags.splice(i, 1)[0];
                    $scope.subtags.push(subTag);
                }
            }
        }
    };

   function getSelectedSubtagsId (tags) {
       var subTagIds = [];
       for (i = 0; i < tags.length; i++) {
            subTagIds.push(tags[i].id);
       }

       return subTagIds;
   }
});