app.controller('BulkSignupController', function ($scope, $http, $uibModal,toaster) {

    $scope.responseList = {};
    $scope.res = [];
    var records = {};
    $scope.dryrun = true;
    $scope.showDisplayResponse = false;
    $scope.email = "";
    $scope.allowMainAimReport = false;
    $scope.allowMiniAimReport = false;

    $scope.uploadTypes=["User","UserTestAssignment"];
    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            text: 'Please upload the same file again with correction',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.loader = function(){
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelLoader = function() {
        $scope.modalInstance.close();
    };

    var displayResponse = function () {
        $scope.res = [];
        $scope.showDisplayResponse = true;
        for (property in $scope.responseList) {
            records = {};
            records.name = property;
            records.value = $scope.responseList[property];
            $scope.res.push(records);
        }
    }

    $scope.setFiles = function (element) {
        $scope.signupFile = element.files[0];
    };

    $scope.upload = function (type) {
        if(!type){
            toaster.pop('Error',"Upload type is not selected");
            return;
        }
        $scope.loader();
        $scope.showDisplayResponse = false;
        var r = new FileReader();
        var fd = new FormData();
        fd.append('file', $scope.signupFile);
        fd.append('dryrun', $scope.dryrun);
        fd.append('rootAccess', 'false');
        fd.append('email', $scope.email);
        fd.append('allowMainAimReport', $scope.allowMainAimReport);
        fd.append('allowMiniAimReport', $scope.allowMiniAimReport);
        var request = {
            method: 'POST',
            url: '/api/user/self/import/'+type+'/',
            data: fd,
            headers: {
                'Content-Type': undefined
            }
        };
        $http(request).then(function (response) {
            $scope.cancelLoader();
                ok("success");
                $scope.responseList = response.data;
                displayResponse();
            }, function (response) {
                $scope.cancelLoader();
                $scope.responseList = response.data;

                    displayResponse();
                    error(response.data.error);

            }
        );

    };

    $scope.exportExampleFile = function (type) {



        $http.get('/api/user/self/export/'+type+'/',{responseType: 'arraybuffer'}
        ).then(function (response) {
            var header = response.headers('Content-Disposition');
            var fileName = header.split("=")[1].replace(/\"/gi,'');

            var blob = new Blob([response.data],
                {type : 'application/vnd.openxmlformats-officedocument.presentationml.presentation;charset=UTF-8'});
            var objectUrl = (window.URL || window.webkitURL).createObjectURL(blob);
            var link = angular.element('<a/>');
            link.attr({
                href : objectUrl,
                download : fileName
            })[0].click();
        })

    }
});