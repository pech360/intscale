app.filter('pagination', function () {
    return function (input, start) {
        start = +start;
        if (input instanceof Array)
            return input.slice(start);
    };
});
app.controller('AllTestsController', function ($scope, $http, $state, $stateParams, $uibModal, toaster) {
    $scope.curPage = 0;
    $scope.pageSize = 10;
    $scope.TotalRecordCount=0;
    $scope.goToPage = 1;
    $scope.alltests = {};

      $scope.setCurrentPage = function() {
        $scope.curPage = 0;
       };

      $scope.numberOfPages = function() {
          $scope.pages = [];
          $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
          for (var i = 1; i <= $scope.totalPages; i++) {
              $scope.pages.push(i);
          }
          return $scope.totalPages;
      };

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.refreshAllTest = function () {
        $http.get('api/test/all').then(function (response) {
            $scope.alltests = response.data;
            $scope.TotalRecordCount=response.data.length;
        });
    };

    $scope.refreshAllTest();


    $scope.editTest = function (testId,goLive) {
        $state.go('app.createTest', {'testId': testId,'version':goLive,'readUser':true});
    }

    /*$scope.createVersion = function (testId,goLive) {
            $state.go('app.createTest', {'testId': testId,'testVersion': goLive,'version':true});
        }*/
    $scope.goLiveTest = function (id) {
        swal({
            title: 'Are you sure for GoLive?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.value) {
                $http.get('/api/test/goLive?id=' + id).then(function (response) {
                        if (response.data) {
                            $scope.refreshAllTest();
                            ok('Test Go Live successfully');
                        }
                    },
                    function (response) {
                        error(response.data.error);
                    });
                    }
               });
       };

      $scope.viewTestHistory=function(id){
           $http.get('/api/test/testHistory?id='+id).then(function(response){
             $scope.testHistoryList=response.data;
             $scope.modalInstance = $uibModal.open({
                 templateUrl: 'testHistory.html',
                 size: 'lg',
                 scope: $scope
             });
           });
           };
      $scope.cancel = function() {
           $scope.modalInstance.close();
       };

    $scope.deleteTest = function (testId) {
        var testId = testId;
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.value) {
                $http.delete('/api/test/' + testId).then(function (response) {
                        ok("deleted");
                        $scope.refreshAllTest();
                    },
                    function (response) {
                        error(response.data.error);
                    });
            }
        })

    };

    $scope.publishTest = function (testId, published) {
        $http.put('/api/test/publish/?id=' + testId + '&publish=' + !published).then(function (response) {
                $scope.refreshAllTest();
            },
            function (response) {
                error(response.data.error);
            });
    };

    $scope.openAddVideoDialog = function () {
        $scope.addVideoDialog = $uibModal.open({
            templateUrl: 'views/addVideoForTest.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });

    };

    $scope.closeAddVideoDialog = function () {
        $scope.addVideoDialog.close();
    };


    $scope.browseVideo = function () {
        $('#testVideo').trigger('click');
    };
    $scope.videoFile = {};

    $scope.setFiles = function (element) {
        $scope.videoFile = element.files[0];
        // displayImage($scope.imgfile);
    };
});


