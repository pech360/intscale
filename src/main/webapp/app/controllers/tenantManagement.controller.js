app.filter('pagination', function()
{
return function(input, start)
{
 start = +start;
 if(input instanceof Array)
 return input.slice(start);
};
});
app.controller('TenantManagementController',function($scope,$http,toaster,$uibModal,$state){
    $scope.curPage = 0;
    $scope.pageSize = 10;
    $scope.TotalRecordCount=0;
    $scope.goToPage = 1;
    $scope.listOfTenants={};
    $scope.tenant={};
    $scope.backImage='';
    $scope.logoImage='';
    $scope.inLogoImage='';
    $scope.tenant.domain='';
    $scope.logoFile='';
    $scope.inLogoFile='';

   $scope.setCurrentPage = function() {
       $scope.curPage = 0;
   };

   $scope.numberOfPages = function() {
       $scope.pages = [];
       $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
       for (var i = 1; i <= $scope.totalPages; i++) {
           $scope.pages.push(i);
       }
       return $scope.totalPages;
   };

 $scope.list=function(){
      $http.get('/api/tenant/').then(function(response){
       $scope.listOfTenants=response.data;
       $scope.TotalRecordCount=response.data.length;
      },function(response){
         toaster.pop('Info',response.error);
      });
 };

 $scope.list();

 $scope.openTenantModal =  function () {
         $scope.modalInstance = $uibModal.open({
             templateUrl: 'createTenant.html',
             size: 'lg',
             scope: $scope,
             backdrop: 'static'
         });
     };

 $scope.cancel = function() {
        $scope.modalInstance.close();
    };

 $scope.createNew = function() {
     $scope.tenant={};
     $scope.openTenantModal();
 };

 $scope.create=function(tenant){
        if(!tenant.name){
        toaster.pop('Info',"Tenant name can't be empty");
        return;
        }
        if(!tenant.description){
        toaster.pop('Info',"Tenant description can't be empty");
        return;
        }
        if(!tenant.domain){
        toaster.pop('Info',"Tenant domain can't be empty");
        return;
        }
        if(!tenant.address){
        toaster.pop('Info',"Tenant address can't be empty");
        return;
        }
        if(!tenant.schoolLimit){
        toaster.pop('Info',"Tenant schoolLimit can't be empty");
        return;
        }
   if(tenant.id){
     $http.put('/api/tenant/',tenant).then(function(response){
         $scope.list();
         $scope.cancel();
         ok('Record successfully updated');
        },function(response){
         toaster.pop('Info',response.error);
        });
     }else{
        $http.post('/api/tenant/',tenant).then(function(response){
        $scope.list();
        $scope.cancel();
        ok('Record successfully created');
        },function(response){
        toaster.pop('Info',response.error);
        });
   }
 };

 $scope.update=function(tenant){
   $scope.tenant=tenant;
   $scope.openTenantModal();
   $scope.backFile=tenant.domain+'back.png';
   $scope.logoFile=tenant.domain+'logo.png';
   $scope.inLogoFile=tenant.domain+'intlogo.png';
   $scope.getBackImage($scope.backFile);
 };

 $scope.getBackImage=function(filName){
  $http({
     method: 'GET',
     url: '/api/raw/view/PROFILEIMAGE/' + filName,
     responseType: 'arraybuffer'
 }).then(function (response) {
     $scope.backImage = "data:image/PNG;base64," + arrayBufferToBase64(response.data);
     $scope.getLogoImage($scope.logoFile);
 },function(response){
    $scope.backImage='d:/tmp/intscale/none';
    $scope.logoImage='d:/tmp/intscale/none';
    $scope.inLogoImage='d:/tmp/intscale/none';
 }
 );
 };

 $scope.getLogoImage=function(filName){
   $http({
      method: 'GET',
      url: '/api/raw/view/PROFILEIMAGE/' + filName,
      responseType: 'arraybuffer'
  }).then(function (response) {
      $scope.logoImage = "data:image/PNG;base64," + arrayBufferToBase64(response.data);
      $scope.getInLogoImage($scope.inLogoFile);
  });
  };

 $scope.getInLogoImage=function(filName){
    $http({
       method: 'GET',
       url: '/api/raw/view/PROFILEIMAGE/' + filName,
       responseType: 'arraybuffer'
   }).then(function (response) {
       $scope.inLogoImage = "data:image/PNG;base64," + arrayBufferToBase64(response.data);
   });
   };

 function arrayBufferToBase64( buffer ) {
          var binary = '';
          var bytes = new Uint8Array( buffer );
          var len = bytes.byteLength;
          for (var i = 0; i < len; i++) {
              binary += String.fromCharCode( bytes[ i ] );
          }
          return window.btoa( binary );
      };

 function ok(message) {
           swal({
               title: message,
               type: 'success',
               buttonsStyling: false,
               confirmButtonClass: "btn btn-warning"
           });
       };
   function error(message) {
       swal({
           title: message,
           type: 'error',
           buttonsStyling: false,
           confirmButtonClass: "btn btn-warning"
       });
   };

   $scope.deleteRecord=function(id){
     swal({
           title: 'Are you sure?',
           text: "You won't be able to revert this!",
           type: 'warning',
           showCancelButton: true,
           confirmButtonColor: '#3085d6',
           cancelButtonColor: '#d33',
           confirmButtonText: 'Yes'
       }).then(function (result) {
           if (result.value) {
           $http.delete('/api/tenant?id='+id).then(function(response){
             $scope.list();
             ok('Record deleted successfully');
           },
            function (response) {
                error(response.data.error);
            });
            }
       });
   };
   //Back image upload
   $scope.browseBackImage = function () {
                $('#backImageId').trigger('click');
            };

   $scope.setBackImage = function (element) {
                  if(!$scope.tenant.domain){
                  toaster.pop('Info',"Tenant domain can't be empty");
                  return;
                  }
                  $scope.backImagefile = element.files[0];
                  $scope.backFilename=$scope.tenant.domain+'back';
                  $scope.uploadBackFile($scope.backImagefile,$scope.backFilename);
              };
   function showBackImage(file) {
              var backReader = new FileReader();
              backReader.readAsDataURL(file);
              backReader.onload = function () {
                  $scope.$apply(function () {
                     $scope.backImage=backReader.result;
                  });
              };
              backReader.onerror = function (error) {
                  error("Failed to load image:" + error);
              };
          };
   $scope.uploadBackFile = function (imgFile,filename) {
                var fdb = new FormData();
                fdb.append('file', imgFile);
                var fileName1=filename;
                fdb.append('fileName', [fileName1]);
                var backRequest = {
                     method: 'POST',
                     url: '/api/raw/tenant/images/',
                     data: fdb,
                     headers: {
                         'Content-Type': undefined
                     }
                 };
                $http(backRequest).then(function (response) {
                        showBackImage(imgFile);
                       ok("Back Image uploaded successfully");
                    }, function (response) {
                        toaster.pop('error', "Image not uploaded successfully");
                        error(error);
                    }
                );
            };
   //Logo Image upload
   $scope.browseLogoImage = function () {
                   $('#logoImageId').trigger('click');
               };

      $scope.setLogoImage = function (element) {
                     if(!$scope.tenant.domain){
                       toaster.pop('Info',"Tenant domain can't be empty");
                       return;
                       }
                     $scope.logoImagefile = element.files[0];
                     $scope.logofilename=$scope.tenant.domain+'logo';
                     $scope.uploadLogoFile($scope.logoImagefile,$scope.logofilename);
                 };
      function showLogoImage(file) {
                 var logoReader = new FileReader();
                 logoReader.readAsDataURL(file);
                 logoReader.onload = function () {
                     $scope.$apply(function () {
                        $scope.logoImage=logoReader.result;
                     });
                 };
                 logoReader.onerror = function (error) {
                     error("Failed to load image:" + error);
                 };
             };
      $scope.uploadLogoFile = function (imgFile,filename) {
                        var fdl = new FormData();
                        fdl.append('file', imgFile);
                        var fileName2=filename;
                        fdl.append('fileName', [fileName2]);
                        var logoRequest = {
                             method: 'POST',
                             url: '/api/raw/tenant/images/',
                             data: fdl,
                             headers: {
                                 'Content-Type': undefined
                             }
                         };
                        $http(logoRequest).then(function (response) {
                                showLogoImage(imgFile);
                                ok("Logo Image uploaded successfully");
                            }, function (response) {
                                toaster.pop('error', "Image not uploaded successfully");
                                error(error);
                            }
                        );
                    };
      //In Logo image uploaded
          $scope.browseInLogoImage = function () {
                       $('#inLogoImageId').trigger('click');
                   };

          $scope.setInLogoImage = function (element) {
                         if(!$scope.tenant.domain){
                           toaster.pop('Info',"Tenant domain can't be empty");
                           return;
                           }
                         $scope.inLogoImagefile = element.files[0];
                         $scope.inlogofilename=$scope.tenant.domain+'intlogo';
                         $scope.uploadInLogoFile($scope.inLogoImagefile,$scope.inlogofilename);
                     };
          function showIntImage(file) {
                     var inLogoReader = new FileReader();
                     inLogoReader.readAsDataURL(file);
                     inLogoReader.onload = function () {
                         $scope.$apply(function () {
                            $scope.inLogoImage=inLogoReader.result;
                         });
                     };
                     inLogoReader.onerror = function (error) {
                         error("Failed to load image:" + error);
                     };
                 };
     $scope.uploadInLogoFile = function (imgFile,filename) {
                  var fdil = new FormData();
                  fdil.append('file', imgFile);
                  var fileName3=filename;
                  fdil.append('fileName', [fileName3]);
                  var inLogoRequest = {
                       method: 'POST',
                       url: '/api/raw/tenant/images/',
                       data: fdil,
                       headers: {
                           'Content-Type': undefined
                       }
                   };
                  $http(inLogoRequest).then(function (response) {
                          showIntImage(imgFile);
                          ok("In Logo image uploaded successfully");
                      }, function (response) {
                          toaster.pop('error', "Image not uploaded successfully");
                          error(error);
                      }
                  );
              };

});