app.filter('pagination', function () {
    return function (input, start) {
        start = +start;
        if (input instanceof Array)
            return input.slice(start);
    };
});
app.controller('usertestAssignmentController', function ($scope, $http, CommonService, toaster, $uibModal) {
    $scope.schoolname = '';
    $scope.grade = '';
    $scope.userIdForTest = '';
    $scope.curPage = 0;
    $scope.pageSize = 50;
    $scope.goToPage = 1;
    $scope.TotalRecordCount = 0;
    var countUser = 0;
    var countTest = 0;
    $scope.userCount = 0;
    $scope.allowedCount = 0;
    $scope.usedCount = 0;
    $scope.remainCount = 0;
    $scope.showLoadingUser = false;
    $scope.showLoadingAssign = false;
    $scope.showLoadingDlink = false;
    $scope.userAssignArray = [];
    $scope.userAvlTests = [];
    $scope.testFlag = false;
    $scope.addTest = false;
    $scope.reAssignTest=false;
    $scope.availableTest = [];
    $scope.listOFAllUserTestIds = [];

    $scope.studentAssignment = {};
    $scope.testStatus = [
                           {
                             id: 1,
                             name : "Test not assigned",
                             value : "test not assigned"
                           }, {
                             id: 2,
                             name : "New test assigned",
                             value : "new test assigned"
                           }, {
                             id: 3,
                             name : "Test in attempted",
                             value : "test in attempted"
                           }, {
                             id: 4,
                             name : "Already taken test",
                             value : "already taken test"
                           }, {
                             id: 5,
                             name : "Test completed successfully",
                             value : "test completed successfully"
                           }
                         ];
    $scope.numberOfPages = function () {
        $scope.pages = [];
        $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
        for (var i = 1; i <= $scope.totalPages; i++) {
            $scope.pages.push(i);
        }
        return $scope.totalPages;
    };

    /*$http.get('/api/role/roleSummary').then(function (response) {
        $scope.listRoles = response.data;
    });*/

    $http.get('/api/school/allByUser').then(function (response) {
        $scope.listSchooles = response.data;
    });
    $http.get('/api/md/grades/').then(function (response) {
        $scope.grades = response.data;
    });
    $scope.fetchAdminTestDetails = function () {
        $http.get('/api/user/self/adminTestDetails').then(function (response) {
            $scope.allowedCount = response.data.testCount;
            $scope.usedCount = response.data.usedTestCount;
            $scope.remainCount = response.data.availableTestCount;
            $scope.adminTestList = response.data.tests;
        });
    };

    $scope.fetchAdminTestDetails();

    $scope.fetchStudentList = function () {
        if (!$scope.schoolname) {
            toaster.pop('Error', "please select school..");
            return;
        }
        if(!$scope.userGender){
         $scope.userGender='';
        }
        if(!$scope.userStatus){
         $scope.userStatus='';
        }
        if(!$scope.userType){
         $scope.userType='';
        }
        $scope.showLoadingUser = true;
        $http.get('/api/test/assignedTest?schoolName=' + $scope.schoolname + '&grade=' + $scope.grade+ '&gender=' + $scope.userGender+ '&status=' + $scope.userStatus+'&type='+$scope.userType).then(function (response) {
            $scope.showLoadingUser = false;
            $scope.listStudents = response.data;
            $scope.TotalUserCount = response.data.length;
            $scope.TotalRecordCount = response.data.length;
            var x = 0;
            $scope.listStudents.forEach(function (element) {
                $scope.listStudents[x].selectUser = false;
                $scope.listOFAllUserTestIds.push(JSON.stringify($scope.listStudents[x].id));
                x = x + 1;
            });
        });
    };

    $scope.downloadByExcelSheet=function(){
     if($scope.listOFAllUserTestIds.length<1){
       toaster.pop('info','Data list is empty');
       return;
     }
     window.location.href = "/api/reports/download/userAssignment?userTestIds="+$scope.listOFAllUserTestIds+"&token=Bearer "+localStorage.getItem('ngStorage-token').slice(1,-1);
    };

    $scope.isSelectAllUser = function () {
        if ($scope.selectAllUsers) {
            $scope.userAssignArray = [];
            for (countUser = 0; countUser < $scope.TotalUserCount; countUser++) {
                $scope.listStudents[countUser].selectUser = true;
                $scope.userAssignArray.push($scope.listStudents[countUser].userId);
                $scope.userCount = $scope.TotalUserCount;
            }
        } else {
            for (countUser = 0; countUser < $scope.TotalUserCount; countUser++) {
                $scope.listStudents[countUser].selectUser = false;
            }
            $scope.userAssignArray = [];
            $scope.userCount = 0;
        }
    };

    $scope.isUserSelected = function (user) {
        if (user.selectUser) {
            $scope.userAssignArray.push(user.userId);
            $scope.userCount = $scope.userCount + 1;
        } else {
            $scope.userCount = $scope.userCount - 1;
            var i = 0;
            $scope.userAssignArray.forEach(function (element) {
                if (element == user.userId) {
                    $scope.userAssignArray.splice(i, 1);
                }
                i++;
            });
            i = 0;
        }
    };

    $scope.saveUserTestAssignment = function () {
        if ($scope.userAssignArray.length < 1) {
            toaster.pop('Info', "User is not selected");
            return;
        }
        if ($scope.adminTestList.length < 1) {
            toaster.pop('Info', "Admin Test list is empty");
            return;
        }
        if ($scope.userCount > $scope.remainCount) {
            toaster.pop('Info', "Exceed limit of allowed test count");
            return;
        }
        $scope.showLoadingAssign = true;
        $scope.studentAssignment.userIds = $scope.userAssignArray;
        $scope.studentAssignment.reAssignTest = $scope.reAssignTest;
        $http.put('api/test/usertestAssignment', $scope.studentAssignment).then(function (response) {
            if (response.data) {
                $scope.showLoadingAssign = false;
                ok("UserTest Assign successfully");
                $scope.selectAllUsers = false;
                $scope.fetchStudentList();
                $scope.userAssignArray = [];
                $scope.studentAssignment = {};
                $scope.fetchAdminTestDetails();
                $scope.userCount = 0;
            } else {
                $scope.showLoadingAssign = false;
                error("Exceed limit of allowed test count");
            }
        }, function (response) {
            error(response.data.error);
        });
    };

    $scope.dLinkAllTest = function () {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, DLink All Test!'
        }).then(function (result) {
                if (result.value) {
                    if ($scope.userAssignArray.length < 1) {
                        toaster.pop('Info', "User is not selected");
                        return;
                    }
                    $scope.showLoadingDlink = true;
                    $scope.studentAssignment.userIds = $scope.userAssignArray;
                    $http.put('api/test/usertestDlinkAll', $scope.studentAssignment).then(function (response) {
                        if (response.data) {
                            $scope.showLoadingDlink = false;
                            ok("All UserTest DLink successfully");
                            $scope.selectAllUsers = false;
                            $scope.fetchStudentList();
                            $scope.userAssignArray = [];
                            $scope.studentAssignment = {};
                            $scope.fetchAdminTestDetails();
                            $scope.userCount = 0;
                        } else {
                            $scope.showLoadingDlink = false;
                            error("Exceed limit of allowed test count");
                        }
                    }, function (response) {
                        error(response.data.error);
                    });
                }
            }
        );
    };

    $scope.viewAssignedTest = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'testView.html',
            size: 'sm',
            scope: $scope
        });
    };
    $scope.cancel = function () {
        $scope.modalInstance.close();
    };

    $scope.dLinkTest = function (user) {
        if (user.testStatus == 'test in attempt') {
            toaster.pop('Info', "Test in process so can't update");
            return;
        }
        $scope.addTest = false;
        $scope.testFlag = false;
        $scope.userAvlTests = user.tests;
        $scope.userIdForTest = user.userId;
        $scope.availableTest = [];
        $scope.adminTestList.forEach(function (e) {
            $scope.availableTest.push(e);
        });
        $scope.viewUserAssignedTest();
    };

    $scope.removeUserTest = function (i) {
        $scope.userAvlTests.splice(i, 1);
        $scope.testFlag = true;
    };
    $scope.removeAllUserTest = function () {
        $scope.userAvlTests = [];
        $scope.testFlag = true;
    };

    var alreadyExits = false;
    $scope.addUserTest = function (i) {
        $scope.userAvlTests.forEach(function (e) {
            if ($scope.availableTest[i] == e) {
                alreadyExits = true;
            }
        });
        if (!alreadyExits) {
            $scope.userAvlTests.push($scope.availableTest[i]);
            $scope.testFlag = true;
        }
        alreadyExits = false;
        $scope.availableTest.splice(i, 1);
    };

    $scope.updateSingleUser = function () {
        if ($scope.testFlag) {
            $http.put('api/test/userTestUpdate?id=' + $scope.userIdForTest + '&userTest=' + $scope.userAvlTests).then(function (response) {
                if (response.data) {
                    ok("UserTest updated successfully");
                    $scope.cancel();
                    $scope.selectAllUsers = false;
                    $scope.fetchStudentList();
                    $scope.fetchAdminTestDetails();
                    $scope.userCount = 0;
                }
            },function(response){
              error(response.data.error);
            });
        } else {
            $scope.cancel();
        }
    };

    $scope.showAddTest = function () {
        if ($scope.addTest) {
            $scope.addTest = false;
        } else {
            $scope.addTest = true;
        }
    };
    $scope.viewUserAssignedTest = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'usertestview.html',
            size: 'sm',
            scope: $scope
        });
    };
    $scope.cancel = function () {
        $scope.modalInstance.close();
    };

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    };

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    };

});