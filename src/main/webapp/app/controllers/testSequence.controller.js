app.controller('testSequenceController',function($scope,$http,$state,toaster){
  $scope.testList={};
  $scope.testForce=false;
  $scope.updateSequence=true;
  $scope.loadTestByType=function(type){
   if(!type){
   return;
   }
   if(type=='test'){
     $http.get('/api/test/listAllPublishTests').then(function(response){
        $scope.testList=response.data;
       });
   }else if(type=='construct'){
     $http.get('/api/test/constructList').then(function(response){
         $scope.testList=response.data;
        });
   }
  };

  $scope.saveSequence=function(){
    $http.put('/api/test/testSequencing',$scope.testList).then(function(response){
       $scope.testList=response.data;
        /*$scope.testList.forEach(function(e){
            $scope.testForce=e.testForce;
         });*/
       ok('Sequencing update successfully');
    },function(response){
       error(response.data.error);
    });
  };

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    };
    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    };
});