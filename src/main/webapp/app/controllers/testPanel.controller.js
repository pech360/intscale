app.controller("TestPanelController", function ($scope, $http, $state, $localStorage, $stateParams, toaster, $rootScope, $window, TestPanelService, $timeout, $uibModal) {



    if ($stateParams.testId) {
        $localStorage.testId = $stateParams.testId;
        $localStorage.productId = $stateParams.productId;
        $localStorage.constructId = $stateParams.constructId;
    }
    var testId = $localStorage.testId; //level third
    var productId = $localStorage.productId;//level first
    var constructId = $localStorage.constructId;//level second

    $scope.assets_url = $localStorage.assets_url;
    $scope.orgName = window.location.hostname.split(".")[0];
    $scope.result = {};
    $scope.update = false;
    $scope.disablePlayAudio = false;
    $scope.checkDisabled = true;
    $rootScope.submitTestIcon = false;
    $rootScope.showDefaultNavBar = true;
    $rootScope.showTestNavBar = false;
    $scope.answersArray = [];
    $scope.showIntro = true;
    $scope.takeTest = false;
    $scope.showLoadingIcon = true;
    $scope.currentIndex = 1;
    $scope.disableNextButton = true;
    $scope.currentGroup = {};
    var askForSubmitAnswers = true;
    var percentValue = {};
    $scope.showAnswerOption = true;
    $rootScope.disableSubmitAnswer = false;
    var noOfSubmitedAnswersOfQuestionsOfSameGroup = 0;
    $scope.questionImageNameList = [];
    $scope.questionAudioNameList = [];
    $scope.questionVideoNameList = [];
    $scope.submitFlag = true;
    var wrongSelectedOptionCountForDummy = 0;
    var wrongSelectedOptionCountForNonDummy = 0;
    var forceExitCount = 0;
    // var tempQuestion={};
    $scope.displayCorrectOptionMsgForPractice = true;
    $rootScope.showTestProcedureVar = false;
    $scope.width = ' 0%';
    var testLoadedAt = new Date();
    var testBeginAt = {};
    $scope.request = {
        rating: 0
    };

    fetchProfile();

    function fetchProfile() {
        $http.get('/api/user/profile').then(function (response) {
            $scope.user = response.data;
            $scope.premium = $scope.user.premium;
        }, function (response) {
            toaster.pop('error', "Error", "Failed to load profile - " + response.data.error);

        });
    }

    $scope.openDecisionDialog = function () {
        swal({
            title: "Well done! You did a great job. Now it's time to move to the next assessment.",
            text: "Would you like to",
            imageUrl: $scope.assets_url + 'greatJob.png',
            imageWidth: 170,
            imageHeight: 150,
            // type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#ffcb3c',
            cancelButtonColor: '#00bb7e',
            cancelButtonText: 'Proceed now',
            confirmButtonText: 'Proceed later',
            icon: ""
        }).then(function (result) {
            if (result.value) {
                $state.go('app.availabletests');
            } else {
                $scope.startTakingTest(constructId);

            }
        })
    };

    function setTimer() {
        $scope.currentTime = {};
        $scope.timeTakenInQuestion = 0;
        $scope.timeWhenQuestionStarted = new Date().getTime();
        $scope.questionTimerMinutes = 0;
        $scope.questionTimerSeconds = 0;
        $scope.clockToBeShown = "";
        $scope.runningTimer = setInterval(function () {
            $scope.currentTime = new Date().getTime();
            $scope.questionTimerObject = $scope.currentTime - $scope.timeWhenQuestionStarted;
            $scope.questionTimerMinutes = Math.floor(($scope.questionTimerObject % (1000 * 60 * 60)) / (1000 * 60));
            $scope.questionTimerSeconds = Math.floor(($scope.questionTimerObject % (1000 * 60)) / 1000);
            $scope.countTotalSeconds = Math.floor($scope.questionTimerObject / 1000);
            $scope.timeTakenInQuestion = $scope.countTotalSeconds;
            if (true) {
                $scope.clockToBeShown = $scope.questionTimerMinutes + "m " + $scope.questionTimerSeconds + "s ";
                $scope.$apply();
            }
        }, 1000);
    }

    function stopTimer() {
        clearInterval($scope.runningTimer);
    }

    function getFullTest() {

        if (testId) {
            $http.get('/api/test/forTakers/' + testId).then(
                function (response) {
                    $scope.test = response.data;
                    $scope.questions = $scope.test.questions;
                    if ($scope.test.disableSubmit) {
                        $rootScope.disableSubmitAnswer = true;
                    }
                    if ($scope.test.random) {
                        $rootScope.currentTestName = $scope.test.name;
                        TestPanelService.shuffle($scope.questions);
                        var questions = TestPanelService.dummyFirst($scope.questions);
                        questions = TestPanelService.indexing(questions);
                        $scope.totalQuestionIndex = 0;
                        for (i = 0; i < questions.length; i++) {
                            if (questions[i].index > $scope.totalQuestionIndex) {
                                $scope.totalQuestionIndex = questions[i].index;
                            }
                        }
                        $scope.questions = questions;
                    } else {
                        TestPanelService.indexBySequence($scope.questions);
                        var questions = $scope.questions;
                        $scope.totalQuestionIndex = 0;
                        for (i = 0; i < questions.length; i++) {
                            if (questions[i].index > $scope.totalQuestionIndex) {
                                $scope.totalQuestionIndex = questions[i].index;
                            }
                        }
                    }
                    getAllQuestionImages();

                    $scope.currentQuestion = findQuestion(1, $scope.questions); // for getting current que
                    if ($scope.currentQuestion.hasAnimatedGif) { // if the current has any video
                        loadElementByIdAndEnableBeginButton('questionVideo'); // ensure video load and then enable the begin button

                    } else {
                        $scope.showLoadingIcon = false;
                    }
                }, function (response) {
                    error("Failed to load test!");
                    $scope.showLoadingIcon = false;
                }
            );
        } else {
            $window.history.back();
        }

    }

    getFullTest();

    function loadElementByIdAndEnableBeginButton(elementId) {
        if (document.getElementById(elementId) == null) { //recursive call for checking if element is loaded
            setTimeout(function () {
                loadElementByIdAndEnableBeginButton(elementId);
            }, 500);
        } else {
            $scope.$apply(function () { // if loaded then enable begin  button and digest
                $scope.showLoadingIcon = false;
            });
        }

    }

    function okForSubmitAnswers(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        }).then(function (result) {
            if (result.value) {
                $timeout(function () {
                    $http.get('/api/test/hasOnlyOneTest').then(function (response) {
                        if (response.data) {
                            if ($scope.orgName != 'assess') {
                                $state.go('app.home');
                            } else {
                                $scope.openAskUserExperienceFeedbackModal();
                            }
                        } else {
                            $scope.openDecisionDialog();
                        }
                    });
                }, 500);
            }
        });
    }

    $scope.openSubmitDialog = function () {
        window.onbeforeunload = null;
        swal({
            title: 'Assessment completed successfully!',
            type: 'success',
            confirmButtonColor: '#202c54',
            confirmButtonText: 'OK'
        }).then(function (result) {
            if (result.value) {
                $timeout(function () {
                    $http.get('/api/test/hasOnlyOneTest').then(function (response) {
                        if ($scope.premium == true || $scope.premium == null) {
                            if (response.data) {
                                if ($scope.orgName != 'assess') {
                                    $state.go('app.home');
                                } else {
                                    $scope.openAskUserExperienceFeedbackModal();
                                }
                            } else {
                                $scope.openDecisionDialog();
                            }
                        } else {
                            $state.go('app.availabletests');
                        }
                    });
                }, 500);
            } else {
                $timeout(function () {
                    $http.get('/api/test/hasOnlyOneTest').then(function (response) {
                        if ($scope.premium == true || $scope.premium == null) {
                            if (response.data) {
                                if ($scope.orgName != 'assess') {
                                    $state.go('app.home');
                                } else {
                                    $scope.openAskUserExperienceFeedbackModal();
                                }
                            } else {
                                $scope.openDecisionDialog();
                            }
                        } else {
                            $state.go('app.availabletests');
                        }
                    });
                }, 500);
            }
        })
    };

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function hideAnswersWhenVideoAudioPlayed() {
        if ($scope.currentQuestion.hasAnimatedGif) { // It will hide options in Image Type Questions
            if ($scope.currentQuestion.hideOptionsUntilVideoEnded) {
                $scope.showAnswerOption = false;
                setTimeout(function () {
                    var qVideo = document.getElementById('questionVideo');
                    qVideo.src = $scope.assets_url + $scope.currentQuestion.videoName;
                    qVideo.autoplay = false;
                    qVideo.load();
                    document.getElementById('questionVideo').addEventListener("canplaythrough", videoLoaded, false);
                    qVideo.webkitExitFullscreen()
                }, 1000);
            } else {
                setTimeout(function () {
                    var qVideo = document.getElementById('questionVideo');
                    qVideo.src = $scope.assets_url + $scope.currentQuestion.videoName;
                    qVideo.autoplay = true;
                    qVideo.load();
                    qVideo.webkitExitFullscreen()
                }, 1000);
            }
        }
        if ($scope.currentQuestion.hasAudio) { // It will only hide TextBox in Text FreeFlow Type Questions
            $scope.showAnswerOption = false;
        }
    }

    function prepareForPlayAudio() {
        $scope.disablePlayAudio = true;
        var audio = document.getElementById('questionAudio');
        audio.load();
        audio.addEventListener("canplaythrough", audioLoaded, false);
    }

    $scope.playAudio = function () {
        var audio = document.getElementById('questionAudio');
        audio.play();
        $scope.disablePlayAudio = true;
        document.getElementById('questionAudio').addEventListener('ended', showAnswersWhenVideoAudioEnded);
        audio = {};
    };

    function audioLoaded() {
        $scope.disablePlayAudio = false;
    }

    function videoLoaded() {
        var qVideo = document.getElementById('questionVideo');
        qVideo.play();
        qVideo.addEventListener('ended', showAnswersWhenVideoAudioEnded);
        qVideo.autoplay = false;

    }

    $scope.begin = function () {
        window.onbeforeunload = function () {
            return "Dude, are you sure you want to leave? Think of the kittens!";
        }
        if ($scope.test.videoName) {
            document.getElementById('testVideo').pause();
        }
        $scope.currentIndex = 1;
        $scope.currentQuestion = findQuestion(1, $scope.questions);
        if ($scope.currentQuestion.questionType.toLowerCase() == 'group') {
            $scope.arrayOfQuestionsOfSameGroup = findAllQuestions(1, $scope.questions);
            $scope.currentGroup = $scope.currentQuestion.group;
        }
        if ($scope.test.skipQuestions) {
            $scope.disableNextButton = false;
        }
        $scope.showIntro = false;
        $scope.takeTest = true;
        stopTimer();
        setTimer();
        hideAnswersWhenVideoAudioPlayed();
        updateButtons();
        testBeginAt = new Date();
        $window.scrollTo(0, 0);
    };

    function enableNextButtonIfAnswerIsAlreadyGiven(question) {
        var flag = false;
        for (var answerCounter in $scope.answersArray) {
            if ($scope.answersArray[answerCounter].questionId == question.id) {
                flag = true;
                break;
            }
        }
        if (flag) {
            $scope.disableNextButton = false;
        } else {
            $scope.disableNextButton = true;
        }
    }

    function findCorrectOption(question) {
        var tempCorrectOption = question.answerRequests[0];
        for (var i in question.answerRequests) {
            if (tempCorrectOption.marks < question.answerRequests[i].marks) {
                tempCorrectOption = question.answerRequests[i];
            }
        }
        return tempCorrectOption;
    }

    function practiceQuestionAction(code) {
        if (code == 0) {
            toaster.pop('info', "Incorrect Answer", "Please try again");
        }
        if (code == 1) {
            $scope.displayHintCorrectOption();
        }
        if (code == 2) {
            $scope.displayCorrectOption();
        }
        if (code >= 3) {
            forceExitCount++;
            wrongSelectedOptionCountForDummy = 0;
            nextPlus();
        }
    }

    function checkForForceExit() {
        if (forceExitCount == 2) {

            $scope.update = false;
            $scope.disablePlayAudio = false;
            $scope.checkDisabled = true;
            $rootScope.submitTestIcon = false;
            $rootScope.showDefaultNavBar = true;
            $rootScope.showTestNavBar = false;
            $scope.showIntro = true;
            $scope.takeTest = false;
            $scope.currentIndex = 1;
            $scope.disableNextButton = true;
            $scope.currentGroup = {};
            askForSubmitAnswers = true;
            percentValue = {};
            $scope.showAnswerOption = true;
            noOfSubmitedAnswersOfQuestionsOfSameGroup = 0;
            $scope.submitFlag = true;
            wrongSelectedOptionCountForDummy = 0;
            wrongSelectedOptionCountForNonDummy = 0;
            forceExitCount = 0;
            $scope.displayCorrectOptionMsgForPractice = true;
            toaster.pop('info', "Please read instructions carefully");
            $scope.width = ' 0%';
            $scope.answersArray = [];

        } else {
            if (forceExitCount > 2) {
                $scope.displayInfoForAutomaticSubmit();
                $timeout(function () {
                    finalSubmitAnswer();
                }, 3000);
            }
        }
    }

    $scope.selectAnswer = function (questionId, option, autoIncrementQuestion) {
        var flag = false;
        for (var answerCounter in $scope.answersArray) {
            if ($scope.answersArray[answerCounter].questionId == questionId) {
                flag = true;
                break;
            }
        }
        if (flag) {
            $scope.answersArray[answerCounter].questionId = questionId;
            $scope.answersArray[answerCounter].answers = option;
            $scope.answersArray[answerCounter].answerId = option.id;
            $scope.answersArray[answerCounter].timeTaken = Math.abs($scope.timeTakenInQuestion + $scope.answersArray[answerCounter].timeTaken);

            stopTimer();
            setTimer();
        } else {
            answerForPush = {};
            answerForPush.questionId = questionId;
            answerForPush.answers = option;
            answerForPush.answerId = option.id;
            answerForPush.timeTaken = Math.abs($scope.timeTakenInQuestion);
            $scope.answersArray.push(answerForPush);
            stopTimer();
            setTimer();
            if ($scope.currentQuestion.questionType.toLowerCase() == 'group') {
                noOfSubmitedAnswersOfQuestionsOfSameGroup++;
                if ($scope.arrayOfQuestionsOfSameGroup.length == noOfSubmitedAnswersOfQuestionsOfSameGroup) {
                    $scope.disableNextButton = false;
                    noOfSubmitedAnswersOfQuestionsOfSameGroup = 0;
                } else {
                    if (!$scope.test.skipQuestions) {
                        $scope.disableNextButton = true;
                    }
                }
            } else {
                $scope.disableNextButton = false;
            }
        }
        updateButtons();
        // if ($scope.answersArray.length == ($scope.questions.length)) {
        // if (askForSubmitAnswers) {
        // $scope.submitAnswer();
        // }
        // }
        if (autoIncrementQuestion) {
            if ($scope.hideNextButton) {
                // if (askForSubmitAnswers) {
                // $scope.submitAnswer();
                // }
                $rootScope.submitAnswer();
            } else {
                $scope.next();
            }

        }
        calculateProgress();
        // scrollToBottom();
    };

    function findChoosenOption(questionId) {
        var tempChoosenOption = {};
        for (var findChoosenOptionCounter in $scope.answersArray) {
            if ($scope.answersArray[findChoosenOptionCounter].questionId == questionId) {
                tempChoosenOption = $scope.answersArray[findChoosenOptionCounter].answers;
                return tempChoosenOption;
            }
        }
    }

    $rootScope.submitAnswer = function () {
        askForSubmitAnswers = false;
        // swal({
        //     title: 'Are you sure?',
        //     text: "Do you want to submit the Test?",
        //     type: 'warning',
        //     showCancelButton: true,
        //     confirmButtonColor: '#3085d6',
        //     cancelButtonColor: '#d33',
        //     confirmButtonText: 'Yes!'
        // }).then(function (result) {
        //     if (result.value) {
        //         finalSubmitAnswer();
        //     }
        // })

        finalSubmitAnswer();
    };

    $scope.validateAnswer = function (questionId, option, autoIncrementQuestion) {
        if ($scope.currentQuestion.hasValidation) {
            var text = option.text;
            var validateText = '';
            $scope.submitFlag = true;
            if (text == "" || text == undefined) {
                error("Answer is empty");
                $scope.submitFlag = false;
            }
            if ($scope.currentQuestion.minLength) {
                if (text.length < $scope.currentQuestion.minLength) {
                    error("Please enter at least " + $scope.currentQuestion.minLength + " characters");
                    $scope.submitFlag = false;
                }
            }
            if ($scope.currentQuestion.maxLength) {
                if (text.length > $scope.currentQuestion.maxLength) {
                    error("Please enter less than " + $scope.currentQuestion.maxLength + " characters");
                    $scope.submitFlag = false;
                }
            }
            if (!$scope.currentQuestion.specialCharAllowed) {
                if (validateText = text.match(/\`|\~|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\+|\=|\[|\{|\]|\}|\||\\|\'|\<|\,|\>|\/|\""|\;|\:/g)) {
                    error("Do not enter special symbol");
                    $scope.submitFlag = false;
                }
            }
            if (!$scope.currentQuestion.alphabetAllowed) {
                if (validateText = text.match(/\s/g)) {
                    error("Do not enter space or new line");
                    $scope.submitFlag = false;
                }
                if (validateText = text.match(/[:-}]/g)) {
                    error("Do not enter alphabets");
                    $scope.submitFlag = false;
                }
            }
            if (!$scope.currentQuestion.numericAllowed) {
                if (validateText = text.match(/[0-9]/g)) {
                    error("Do not enter numbers");
                    $scope.submitFlag = false;
                }
            }
        }
        if ($scope.submitFlag) {
            $scope.selectAnswer(questionId, option, true);
        }
    };

    //   *************************************************************** feedback modal
    var oneStarStatements = [
        { selected: false, statement: "Video and instructions were not clear." },
        { selected: false, statement: "Questions were difficult to understand." },
        { selected: false, statement: "Session was lengthy and boring." },
        // {selected: false, statement: "Platform was not so easy to use."},
        { selected: false, statement: "There were errors in the test." }
    ];
    var twoStarStatements = [{ selected: false, statement: "2 star1" },
    { selected: false, statement: "2 star2" },
    { selected: false, statement: "2 star3" },
    { selected: false, statement: "2 star4" }
    ];
    var thirdStarStatements = [
        { selected: false, statement: "Video and the instructions should be more clear." },
        { selected: false, statement: "Test should be made less difficult." },
        { selected: false, statement: "Duration of the session should be reduced." },
        { selected: false, statement: "Platform should be more easy to use." }
    ];
    var fourStarStatements = [{
        selected: false,
        statement: "4 star1"
    },
    { selected: false, statement: "4 star2" },
    { selected: false, statement: "4 star3" },
    { selected: false, statement: "4 star4" }
    ];
    var fiveStarStatements = [
        { selected: false, statement: "Video and the instructions were clear to understand." },
        { selected: false, statement: "Test was easy to attempt." },
        { selected: false, statement: "Session was interesting." },
        { selected: false, statement: "Platform was very easy to use." }
    ];

    $scope.updateFeedbackStatements = function (rating) {
        if (rating == 1) {
            unselectAllStmts(thirdStarStatements);
            unselectAllStmts(fiveStarStatements);
            $scope.feedbackStatements = oneStarStatements;
        }
        if (rating == 2) {
            unselectAllStmts(thirdStarStatements);
            unselectAllStmts(fiveStarStatements);
            $scope.feedbackStatements = oneStarStatements;
        }
        if (rating == 3) {
            unselectAllStmts(oneStarStatements);
            unselectAllStmts(fiveStarStatements);
            $scope.feedbackStatements = thirdStarStatements;
        }
        if (rating == 4) {
            unselectAllStmts(oneStarStatements);
            unselectAllStmts(fiveStarStatements);
            $scope.feedbackStatements = thirdStarStatements;
        }
        if (rating == 5) {
            unselectAllStmts(thirdStarStatements);
            unselectAllStmts(oneStarStatements);
            $scope.feedbackStatements = fiveStarStatements;
        }

        $scope.request.rating = rating;
    };

    function unselectAllStmts(items) {
        for (var i in items) {
            items[i].selected = false;
        }
        return items;
    }

    function convertArrayItemsToText(items) {
        var experienceDescription = "";
        for (var i in items) {
            if (items[i].selected) {
                experienceDescription = experienceDescription.concat(items[i].statement);
                experienceDescription = experienceDescription.concat(",");
            }
        }
        return experienceDescription;
    }

    $scope.openAskUserExperienceFeedbackModal = function () {
        $scope.askUserExperienceFeedbackModal = $uibModal.open({
            templateUrl: 'views/modals/userExperienceFeedbackModal.html',
            size: 'lg',
            scope: $scope,
            backdrop: '',
            keyboard: false
        });

    };

    $scope.closeAskUserExperienceFeedbackModal = function () {
        $scope.askUserExperienceFeedbackModal.close();
    };

    $scope.saveUserExperienceFeedback = function (request) {
        request.experienceDescription = convertArrayItemsToText($scope.feedbackStatements);
        $http.post('/api/md/saveFeedback', request).then(function (response) {
            $scope.askUserExperienceFeedbackModal.close();
            toaster.pop('success', 'feedback save successfully');
            $state.go('app.availabletests');
        }, function (response) {
            $scope.askUserExperienceFeedbackModal.close();
        })
    };

    function finalSubmitAnswer() {
        $scope.result = {};
        $scope.result.testId = testId;
        $scope.result.answers = $scope.answersArray;
        $scope.result.testBeginAt = testBeginAt;
        $scope.result.testLoadedAt = testLoadedAt;
        if ($scope.result.answers.length) {
            $rootScope.submitTestIcon = true;
            $http.post('/api/result', $scope.result).then(function (success) {
                $scope.takeTest = false;
                $scope.openSubmitDialog();
                $rootScope.submitTestIcon = false;
                $rootScope.showTestNavBar = false;
                $rootScope.showDefaultNavBar = true;
                //$scope.openDecisionDialog();
                //$scope.startTakingTest(constructId);
                //$state.go('app.availabletests');
            }, function (response) {
                $rootScope.submitTestIcon = false;
                if (response.status <= 0) {
                    error("You don't have an active Internet Connection.");
                } else {
                    error("Error in saving and completing the assessment.");
                }
            });
        } else {
            toaster.pop('error', "You have not answered any questions");
        }
    }

    $scope.startTakingTest = function (constructId) {
        $http.get('/api/test/listAllPublishTestsByParent?id=' + constructId).then(function (response) {
            $scope.currentTestId = response.data.id;
            if ($scope.currentTestId == 0 || !$scope.currentTestId) {
                //$state.go('app.product({id: productId})');
                $state.go('app.product', { 'id': productId });
                //$state.go('app.availabletests');
                //toaster.pop('Info','You have completed all test');
                return;
            }
            $scope.showIntro = true;
            $scope.takeTest = false;
            $state.go('app.testPanel', { 'testId': $scope.currentTestId, 'constructId': constructId });
        });
    };
    $scope.next = function () {
        // if ($scope.currentIndex > 1) {
        //     tempQuestion = $scope.currentQuestion = findQuestion($scope.currentIndex, $scope.questions);
        // }

        var correctOption = findCorrectOption($scope.currentQuestion);
        $scope.correctOption = correctOption;
        var choosenOption = findChoosenOption($scope.currentQuestion.id);
        if (choosenOption != undefined) {
            if (correctOption.marks != choosenOption.marks) {
                if ($scope.currentQuestion.hasDummy) {
                    practiceQuestionAction(wrongSelectedOptionCountForDummy);
                    wrongSelectedOptionCountForDummy++;
                } else {
                    if ($scope.test.discontinueTest) {
                        if (wrongSelectedOptionCountForNonDummy >= $scope.test.discontinueTestCriteria) {
                            // toaster.pop("success", "Your responses for 5 questions are incorrect continuously");
                            $scope.displayInfoForAutomaticSubmit();
                            $timeout(function () {
                                finalSubmitAnswer();
                            }, 3000);
                        }
                        wrongSelectedOptionCountForNonDummy++;
                        nextPlus();
                    } else {
                        nextPlus();
                    }
                }
            } else {
                wrongSelectedOptionCountForNonDummy = 0;
                wrongSelectedOptionCountForDummy = 0;
                $scope.displayCorrectOptionMsgForPractice = true;
                nextPlus();
            }
        } else {
            wrongSelectedOptionCountForNonDummy = 0;
            wrongSelectedOptionCountForDummy = 0;
            $scope.displayCorrectOptionMsgForPractice = true;
            nextPlus();
        }

    };

    function nextPlus() {
        $window.scrollTo(0, 0);
        $scope.disablePlayAudio = true;
        if ($scope.currentIndex < $scope.totalQuestionIndex) {
            $scope.currentIndex++;
            $scope.currentQuestion = findQuestion($scope.currentIndex, $scope.questions);
            if ($scope.currentQuestion.questionType.toLowerCase() == 'group') {
                $scope.arrayOfQuestionsOfSameGroup = findAllQuestions($scope.currentIndex, $scope.questions);
                $scope.currentGroup = $scope.currentQuestion.group;

            }
            calculateProgress();
            updateButtons();
            if (!$scope.test.skipQuestions) {
                $scope.disableNextButton = true;
                enableNextButtonIfAnswerIsAlreadyGiven($scope.currentQuestion);
            }

            hideAnswersWhenVideoAudioPlayed();
            stopTimer();
            setTimer();
            if (!$scope.currentQuestion.hasDummy) {
                checkForForceExit();
            }
            if ($scope.currentQuestion.hasAudio) {
                setTimeout(function () { prepareForPlayAudio(); }, 1000);

            }
        }
    }

    $scope.previous = function () {
        if ($scope.currentIndex > 1) {
            $scope.currentIndex--;
            $scope.currentQuestion = findQuestion($scope.currentIndex, $scope.questions);
            if ($scope.currentQuestion.questionType.toLowerCase() == 'group') {
                $scope.arrayOfQuestionsOfSameGroup = findAllQuestions($scope.currentIndex, $scope.questions);
            }
            if (!$scope.test.skipQuestions) {
                enableNextButtonIfAnswerIsAlreadyGiven($scope.currentQuestion);
            }
            updateButtons();
            stopTimer();
            setTimer();
        }
    };

    function updateButtons() {
        if ($scope.test.navigationAllowed) {
            $scope.showPreviousButton = $scope.currentIndex > 1;
        }
        // $scope.hideNextButton = $scope.answersArray.length == ($scope.questions.length);

        // if(!$scope.currentQuestion.hasAudio){
        //     $scope.hideNextButton = $scope.totalQuestionIndex == $scope.currentIndex;
        // }

        $scope.hideNextButton = $scope.totalQuestionIndex == $scope.currentIndex;

        $rootScope.showDefaultNavBar = false;
        $timeout(function () {
            /*adding 0.5 sec avg timeout
             *to avoid flickering
             *caused by switching nav bar
             **/
            $rootScope.showTestNavBar = true;
        }, 550);
    }

    function showAnswersWhenVideoAudioEnded() {
        $scope.showAnswerOption = true;

    }

    function findQuestion(index, questions) {
        for (var i in questions) {
            if (questions[i].index == index) {
                return questions[i];
            }
        }
    }

    function findAllQuestions(index, questions) {
        var obj = [];
        for (var i in questions) {
            if (questions[i].index == index) {
                obj.push(questions[i]);
            }
        }
        return obj;
    }

    function calculateProgress() {
        if ($scope.test.skipQuestions) {
            percentValue = ($scope.currentIndex / $scope.totalQuestionIndex) * 100;
        } else {
            percentValue = ($scope.answersArray.length / $scope.questions.length) * 100;
        }
        $scope.progressLength = Math.round(percentValue);
        if ($scope.test.disableSubmit) {
            if (percentValue >= $scope.test.submitLimit) {
                $rootScope.disableSubmitAnswer = false;
            }
        }
        $scope.width = $scope.progressLength + "%";
    }

    function getAllQuestionImages() {
        $scope.questionImageNameList = [];
        var imgAlreadyExist = false;
        for (var k in $scope.questions) {
            if ($scope.questions[k].hasImage) {
                $scope.questionImageNameList.push($scope.questions[k].imageName);
            }
            // if ($scope.questions[k].hasAudio) {
            //     $scope.questionAudioNameList.push($scope.questions[k].audioName);
            // }
            if ($scope.questions[k].hasAnimatedGif) {
                $scope.questionVideoNameList.push($scope.questions[k].videoName);
            }
            if ($scope.questions[k].questionType.toLowerCase() == 'group') {
                if ($scope.questions[k].group) {
                    if ($scope.questions[k].group.commonType.toLowerCase() == 'image') {
                        imgAlreadyExist = false;
                        for (var imgCounter in $scope.questionImageNameList) {
                            if ($scope.questionImageNameList[imgCounter] == $scope.questions[k].group.fileName) {
                                imgAlreadyExist = true;
                                break;
                            }
                        }
                        if (!imgAlreadyExist) {
                            $scope.questionImageNameList.push($scope.questions[k].group.fileName);
                        }
                    }
                }
            }
            if ($scope.questions[k].answerRequests[0].imageName) {
                for (var a in $scope.questions[k].answerRequests) {
                    $scope.questionImageNameList.push($scope.questions[k].answerRequests[a].imageName);

                }
            }
        }
    }

    $scope.scrollToIntro = function () {
        window.scrollTo(0, 750);
    };

    function scrollToBottom() {
        window.scrollTo(0, document.body.scrollHeight);
    }

    $scope.enableNextButton = function () {
        $scope.disableNextButton = false;
    };

    $scope.displayCorrectOption = function () {
        $scope.displayCorrectOptionModal = $uibModal.open({
            templateUrl: 'views/modals/correctOption.html',
            size: 'md',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelDisplayCorrectOption = function () {
        $scope.displayCorrectOptionMsgForPractice = true;
        $scope.displayCorrectOptionModal.close();
    };

    $scope.displayHintCorrectOption = function () {
        $scope.displayHintCorrectOptionModal = $uibModal.open({
            templateUrl: 'views/modals/hintForCorrectOption.html',
            size: 'md',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelDisplayHintCorrectOption = function () {
        $scope.displayHintCorrectOptionModal.close();
    };

    $scope.displayInfoForAutomaticSubmit = function () {
        $scope.displayInfoForAutomaticSubmitModal = $uibModal.open({
            templateUrl: 'views/modals/infoForAutomaticSubmitModal.html',
            size: 'md',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelDisplayInfoForAutomaticSubmit = function () {
        $scope.displayInfoForAutomaticSubmitModal.close();
    };

    $rootScope.showTestProcedure = function () {
        $rootScope.showTestProcedureVar = true;
    };

    $rootScope.hideTestProcedure = function () {
        $rootScope.showTestProcedureVar = false;
    };

    // $scope.setHeight = function (id) {
    //     const parent = document.getElementById('parent' + id).parentElement;
    //     if (document.getElementById(id).height < 50) {
    //         parent.classList.remove('d-flex');
    //     } else {
    //         parent.classList.add('d-flex');
    //     }
    // };
});

