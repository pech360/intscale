app.controller('LeapController', function ($scope, $http, DateFormatService,toaster, $uibModal) {

    $scope.orgName=window.location.hostname.split(".")[0];


    var request = {
        tenants: [],
        grades: [],
        schools: [],
        products: [],
        tests: [],
        fromDate: {},
        toDate: {}
    };

    $scope.fromDate = new Date();
    $scope.toDate = new Date();

    $scope.reset = function () {
        var request = {
            tenants: [],
            grades: [],
            schools: [],
            products: [],
            tests: [],
            fromDate: {},
            toDate: {}
        };

        $scope.getNewUsers();
    };

    function getAllGrades() {
        $http.get('api/md/grades/').then(function (response) {
            $scope.grades = response.data;
            $scope.grades.isAllSelected = function () {
                for (var i = 0; i < this.length; i++) {
                    var item = this[i];
                    if (!item.selected) {
                        return false;
                    }
                }
                return true;
            }
        });
    }

    function getAllCities() {
        $http.get('api/md/cities/').then(function (response) {
            $scope.cities = response.data;
            $scope.cities.isAllSelected = function () {
                for (var i = 0; i < this.length; i++) {
                    var item = this[i];
                    if (!item.selected) {
                        return false;
                    }
                }
                return true;
            }
        });
    }

    function getAllSchools() {
        $http.get("/api/school/all").then(function (response) {
            $scope.schools = response.data;
            $scope.schools.isAllSelected = function () {
                for (var i = 0; i < this.length; i++) {
                    var item = this[i];
                    if (!item.selected) {
                        return false;
                    }
                }
                return true;
            }
        });
    }

    function getAllGender() {
        $scope.genders = [
            {gender: "Male", "selected": false},
            {gender: "Female", "selected": false}
        ];
        $scope.genders.isAllSelected = function () {
            for (var i = 0; i < this.length; i++) {
                var item = this[i];
                if (!item.selected) {
                    return false;
                }
            }
            return true;
        }
    }

    function getAllTenant(){
        $http.get('/api/user/self/tenants').then(function(response){
            $scope.tenants=response.data;
            $scope.tenants.isAllSelected = function () {
                for (var i = 0; i < this.length; i++) {
                    var item = this[i];
                    if (!item.selected) {
                        return false;
                    }
                }
                return true;
            }
        });
    }

    $scope.selectAll = function (items, selected) {
        if (selected) {
            for (var i in items) {
                items[i].selected = true;
            }
            return items;
        } else {
            unselectAll(items);
        }
    };

    function unselectAll(items) {
        for (var i in items) {
            items[i].selected = false;
        }
        return items;
    }

    function populateDashRequest(status) {
        request.grades = [];
        request.schools = [];
        request.tenants = [];
        request.fromDate = DateFormatService.formatDate9($scope.fromDate);
        request.toDate = DateFormatService.formatDate9($scope.toDate);

        for (var i in $scope.grades) {
            if ($scope.grades[i].selected) {
                if ($scope.grades[i].value) {
                    request.grades.push($scope.grades[i].value);
                }
            }
        }

        for (var i in $scope.schools) {
            if ($scope.schools[i].selected) {
                if ($scope.schools[i].name) {
                    request.schools.push($scope.schools[i].name);
                }
            }
        }
        for (var i in $scope.cities) {
            if ($scope.cities[i].selected) {
                if ($scope.cities[i].value) {
                    request.cities.push($scope.cities[i].value);
                }
            }
        }


        for (var i in $scope.tenants) {
            if ($scope.tenants[i].selected) {
                if ($scope.tenants[i].id) {
                    request.tenants.push($scope.tenants[i].id);
                }
            }
        }
    }


    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }


    $scope.getNewUsers = function () {
        $scope.loader();
        populateDashRequest();
        $scope.activeItem = 1;
        $http.post('/api/dashboard/getNewUsers', request).then(function (response) {
            $scope.users = response.data;
            $scope.newUsersCount = $scope.users.length;
            $scope.cancelLoader();
        }, function (response) {
            $scope.cancelLoader();
        })


    };

    $scope.syncTestResult = function () {
        $scope.loader();

        $http.post('/api/result/syncTestResult').then(function (response) {
            $scope.cancelLoader();
        }, function (response) {
            $scope.cancelLoader();
        })
    };


    $scope.loader = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelLoader = function () {
        $scope.modalInstance.close();
    };

    $scope.init = function () {
        getAllGrades();
        getAllSchools();
        getAllCities();
        getAllTenant();
        $scope.reset();
    };

    $scope.init();
});