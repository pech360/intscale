app.controller('LoginPanelController', function ($scope, $http, toaster) {

    $scope.items = [];
    $scope.users = [];

    function initializeItem() {
        return {
            name: "",
            loggedIn: true
        };
    }

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function getLoggedInUsers() {
        $http.get('/api/loggedInUsers').then(function (response) {
            var item = {};
            for (var i in response.data) {
                item = initializeItem();
                item.name = response.data[i];
                $scope.users.push(angular.copy(item));
            }
        });
    }

    $scope.forceLogout = function (username) {
        $http.post('/api/user/emergencyLogout/'+username).then(function (response) {
            var item = {};
            $scope.users = [];
            for (var i in response.data) {
                item = initializeItem();
                item.name = response.data[i];
                $scope.users.push(angular.copy(item));
            }
        },function (response) {
            error(response.data.error);
        });
    };

    getLoggedInUsers();

});


