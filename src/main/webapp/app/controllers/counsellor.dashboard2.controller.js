app.filter('pagination', function () {
    return function (input, start) {
        start = +start;
        if (input instanceof Array)
            return input.slice(start);
    };
});
app.controller('CounsellorDashboard2Controller', function ($scope, $http, CommonService, toaster, $uibModal, DateFormatService) {
    $scope.curPage = 0;
    $scope.pageSize = 10;
    $scope.goToPage = 1;
    $scope.TotalRecordCount = 0;
    $scope.studentArray = [];
    $scope.counsellorStudent = {};
    var countStudent = 0;
    $scope.schoolId = '';
    $scope.gradeId = '';
    $scope.city = '';
    $scope.studentId = '';
    $scope.loading = false;
    $scope.id = '';
    var calendar = {};
    var abcdef = [];

    $scope.numberOfPages = function () {
        $scope.pages = [];
        $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
        for (var i = 1; i <= $scope.totalPages; i++) {
            $scope.pages.push(i);
        }
        return $scope.totalPages;
    };

    function getAllSchools() {
        $http.get('/api/school/all').then(function (response) {
            $scope.listSchools = response.data;
        });
    }


    function getAllGrades() {
        $http.get('/api/md/grades/').then(function (response) {
            $scope.listGrades = response.data;
        });
    }

    function getAllCities() {
        $http.get('api/md/cities/').then(function (response) {
            $scope.cities = response.data;
        });
    }


    // $scope.listTakenTestbyuser = function (id) {
    //     $http.get('/api/test/listTakenTestByUser?userId=' + id).then(function (response) {
    //         $scope.userTestList = response.data;
    //         if (response.data.length < 1) {
    //             toaster.pop('info', 'Taken test is not available')
    //         }
    //     })
    // };

    $scope.getStudentsListByCounsellor = function () {
        $http.get('/api/student/listByCounsellor').then(function (response) {
            $scope.listOfStudents = response.data;
            for (var i in $scope.listOfStudents) {
                $scope.listOfStudents[i].aimAssessmetsCompletedOn = DateFormatService.formatDate(new Date($scope.listOfStudents[i].aimAssessmetsCompletedOn))
                $scope.listOfStudents[i].counsellorAssignedOn = DateFormatService.formatDate(new Date($scope.listOfStudents[i].counsellorAssignedOn))
                $scope.listOfStudents[i].totalSessions = getAttendedSessionsNumber($scope.listOfStudents[i].userCounsellingSessions);

            }

            $scope.TotalRecordCount = response.data.length;
        }, function (response) {
            toaster.pop('Info', response.error);
        });
    };

    function getAttendedSessionsNumber(sessions) {
        var count = 0;
        for (var i in sessions) {
            switch (sessions[i].counsellingSessionStatus) {
                case 'COMPLETED':
                case 'DELAYED':
                    count += 1;
                    break;
                case 'UPCOMING':
                case 'MISSED':
                case 'DRAFTED':
                    break;
            }
        }

        return count;
    }


    $scope.filterSearch = function () {
        $http.get('/api/student/filteredListByCounsellor?schoolId=' + $scope.schoolId + '&gradeId=' + $scope.gradeId + '&city=' + $scope.city).then(
            function (response) {
                if (response.data.length == 0) {
                    toaster.pop('Info', 'List is empty..');
                    return;
                }
                $scope.listOfStudents = response.data;
                $scope.TotalRecordCount = response.data.length;
                for (var i in $scope.listOfStudents) {
                    $scope.listOfStudents[i].aimAssessmetsCompletedOn = DateFormatService.formatDate(new Date($scope.listOfStudents[i].aimAssessmetsCompletedOn))
                    $scope.listOfStudents[i].counsellorAssignedOn = DateFormatService.formatDate(new Date($scope.listOfStudents[i].counsellorAssignedOn))
                }
            },
            function (response) {
                toaster.pop('Info', response.error);
            });
    };

    $scope.openStudentProfileModal = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'studentProfile.html',
            size: 'lg',
            scope: $scope,
            backdrop: true
        });
    };

    $scope.close = function () {
        $scope.modalInstance.close();
    };

    $scope.viewStudentProfile = function (userId) {
        $scope.fetchProfile(userId);
    };

    $scope.fetchProfile = function (id) {
        $http.get('/api/user/self/userProfile?userId=' + id).then(function (response) {
            $scope.user = response.data;
            $scope.user.dob = DateFormatService.formatDate(new Date(response.data.dob));
            $scope.openStudentProfileModal();
        }, function (response) {
            toaster.pop('error', "Error", "Failed to load profile - " + response.data.error);

            if(response.data.error){
                if(response.data.error=="You are logged in another session, so close this session"){

                }
            }
        });
    };


    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.downloadIndividualReport = function (id, updateReport) {
        $scope.loader();
        $http.get('api/reports/downloadAimReport/' + id + '/' + updateReport + '/MAIN', {responseType: 'arraybuffer'}).then(function (response) {
            $scope.cancelLoader();
            var blob = new Blob([response.data], {
                type: 'application/octet-stream'
            });
            saveAs(blob, response.headers("fileName"));
        }, function (response) {
            $scope.cancelLoader();
            error("download failed");
        })
    };

    $scope.downloadAimReportScoreCardInExcel = function (userId) {
        $scope.loader();
        $http.get('/api/reports/downloadAimReportScoreCardInExcel?userId=' + userId).then(function (response) {
            var blob = new Blob([response.data], {
                type: 'application/octet-stream'
            });
            $scope.cancelLoader();
            saveAs(blob, response.headers("fileName"));

        }, function (response) {
            $scope.cancelLoader();
        })
    };

    $scope.loader = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelLoader = function () {
        $scope.modalInstance.close();
    };

    getAllGrades();
    getAllSchools();
    getAllCities();
    $scope.getStudentsListByCounsellor();


});
