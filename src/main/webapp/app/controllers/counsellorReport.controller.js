app.controller('CounsellorReportController', function ($http, $scope, $stateParams, DateFormatService,UserId,TestId) {
    $scope.userId='0';
    if(TestId>0&&UserId>0){
      var testId = TestId;
      $scope.userId=UserId;
      fetchProfileByUserId(UserId);
    }else if(UserId.length==undefined&&TestId.length==undefined&&UserId){
      var testId = $stateParams.id;
      $scope.userId='0';
      fetchProfile();
    }
    fetchTestDetails(testId);

    $scope.getReportV1 = function () {
        $http.get('/api/reports/individualV1?id='+testId+'&userId='+$scope.userId).then(
            function (response) {
                $scope.categories = response.data;
                $scope.categories.sort(compareBySequence);

            }, function (response) {
                error(response.data.error)
            });
    };

    $scope.getReportV1();

    function fetchProfile() {
        $http.get('/api/user/profile').then(function (response) {
            $scope.user = response.data;
            $scope.user.dob = DateFormatService.formatDate(new Date(response.data.dob));
        });
    }
    function fetchProfileByUserId(id) {
        $http.get('/api/user/self/userProfile?userId='+id).then(function (response) {
            $scope.user = response.data;
            $scope.user.dob = DateFormatService.formatDate(new Date(response.data.dob));
        });
    }
    function fetchTestDetails() {
        $http.get('/api/result/test?testId='+testId+'&userId='+$scope.userId).then(function (response) {
            $scope.testDetails = response.data;
            $scope.testDetails.testTakenDate = DateFormatService.formatDate7(new Date($scope.testDetails.testTakenDate));

            if ($scope.testDetails.infrequency < 95 && $scope.testDetails.acquiescence < 95) {
                $scope.testDetails.validityIndicesText = "may indicate that the individual has not simply agreed with each statement. The individual has endorsed most items in a way that is similar to other people; it is unlikely that they have responded randomly.";
            } else if ($scope.testDetails.infrequency > 95 && $scope.testDetails.acquiescence < 95) {
                $scope.testDetails.validityIndicesText = "may indicate that the individual either (a) experienced consistent indecisiveness\n" +
                    "about the a or c response choices (perhaps because of an ambiguous\n" +
                    "self-picture), or (b) tried to avoid making the wrong impression by choosing\n" +
                    "the ? middle answer rather than one of the more definitive or extreme\n" +
                    "answers.";
            } else if ($scope.testDetails.infrequency < 95 && $scope.testDetails.acquiescence > 95) {
                $scope.testDetails.validityIndicesText = "\"may indicate that the individual either (a) experienced consistent indecisiveness.\n" +
                    "about the \"\"Agree\"\" or \"\"Disagree\"\" choices (perhaps because of an ambiguous self-picture), or (b) tried to avoid making the wrong impression by choosing the ? middle answer rather than one of the more definitive or extreme answers.\"\n";
            } else if ($scope.testDetails.infrequency > 95 && $scope.testDetails.acquiescence > 95) {
                $scope.testDetails.validityIndicesText = "\"may indicate that the individual (a) had trouble reading or comprehending\n" +
                    "the questions or (b) responded randomly\"\n";
            }

        });
    }

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function compareBySequence(a, b) {
        if (a.sequence < b.sequence)
            return -1;
        if (a.sequence > b.sequence)
            return 1;
        return 0;
    }

});