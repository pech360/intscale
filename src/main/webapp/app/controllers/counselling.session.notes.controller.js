app.filter('pagination', function () {
    return function (input, start) {
        start = +start;
        if (input instanceof Array)
            return input.slice(start);
    };
});
app.controller('CounsellingSessionNotesController', function ($scope, $http, CommonService, toaster) {
    $scope.curPage = 0;
    $scope.pageSize = 10;
    $scope.goToPage = 1;
    $scope.TotalRecordCount = 0;
    $scope.loading = false;
    $scope.notes = [];

    function refresh(type) {
        $scope.request = {
            statement: "",
            active: true,
            sample: true,
            counsellingSessionType: type
        };

        switch (type) {
            case 'EXPLORE':
                $scope.request.class = 'btn btn-warning btn-simple btn-sm';
                break;
            case 'COMMIT':
                $scope.request.class = 'btn btn-default btn-simple btn-sm';
                break;
            case 'ENGAGE':
                $scope.request.class = 'btn btn-primary btn-simple btn-sm';
                break;
        }
    }

    refresh('EXPLORE');

    $scope.numberOfPages = function () {
        $scope.pages = [];
        $scope.totalPages = Math.ceil(($scope.TotalRecordCount) / ($scope.pageSize));
        for (var i = 1; i <= $scope.totalPages; i++) {
            $scope.pages.push(i);
        }
        return $scope.totalPages;
    };


    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }


    $scope.loader = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelLoader = function () {
        $scope.modalInstance.close();
    };

    $scope.getNotes = function (type) {
        $scope.request.counsellingSessionType = type;
        switch (type) {
            case 'EXPLORE':
                $scope.request.class = 'btn btn-warning btn-simple btn-sm';
                break;
            case 'COMMIT':
                $scope.request.class = 'btn btn-default btn-simple btn-sm';
                break;
            case 'ENGAGE':
                $scope.request.class = 'btn btn-primary btn-simple btn-sm';
                break;
            case 'FREE_FLOW':
                $scope.request.class = 'btn btn-success btn-simple btn-sm';
                break;
        }
        $http.get('/api/counsellor/counsellorNotes?type=' + type).then(function (response) {
            $scope.notes = response.data;

        }, function (response) {
            alert(response.error);
        })
    };

    $scope.saveNote = function (request) {
        if(isValid(request)){
            $http.post('/api/counsellor/counsellorNote', request).then(function (response) {
                if (!angular.equals(response.data, {}) && !request.id) {
                    $scope.notes.push(response.data)
                }
                refresh(request.counsellingSessionType);
            }, function (response) {
                alert(response.error);
            })
        }else {
            toaster.pop("info","enter correct input !!");
        }

    };

    function isValid(request) {
        var flag  = true;

        if(!request.counsellingSessionType){
            flag = false;
        }else {
            if(request.counsellingSessionType!='FREE_FLOW'){
                if(!request.statement){
                    flag = false;
                }
            }
        }


        return flag;
    }

    $scope.getNotes('EXPLORE');
});
