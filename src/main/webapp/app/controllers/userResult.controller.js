app.controller('UserResultController', function ($scope, $http, Auth, toaster, $state, $stateParams, $uibModal, DateFormatService, $rootScope) {
    var testId = $state.params.t;
    var userId = $state.params.u;
    $scope.orgName=window.location.hostname.split(".")[0];

    $scope.getQuestion = function (id) {
        $http.get('/api/question/' + id).then(function (response) {
            $scope.question = response.data;
            $scope.viewQuestion();
        }, function (response) {
            toaster.pop('error', "Failed to Load Question...")
        });
    };

    $scope.viewQuestion = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/viewQuestion.html',
            size: 'lg',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.closeViewQuestion = function () {
        $scope.modalInstance.close();
    };

    function getReport() {
        $http.get('/api/resultByUserAndTest/' + userId +'/'+ testId).then(function (response) {
            $scope.result = response.data;
            $scope.result.testTakenDate = DateFormatService.formatDate7(new Date($scope.result.testTakenDate));
            calulatePercentage();
        }, function (response) {
            toaster.pop('error', "Failed to load result");
        });
    }


    function calulatePercentage() {
        var sumofObtainedMarks = 0;
        var sumofMaxMarks = 0;

        for (var i in $scope.result.userAnswerResponses) {
            sumofObtainedMarks = sumofObtainedMarks + $scope.result.userAnswerResponses[i].marks;
            sumofMaxMarks = sumofMaxMarks + $scope.result.userAnswerResponses[i].maxMarks;
        }
        $scope.result.obtainedMarks = sumofObtainedMarks;
        $scope.result.maxMarks = sumofMaxMarks;
        $scope.result.percentage = ((sumofObtainedMarks / sumofMaxMarks)*100).toFixed(2);
    }


    getReport();

});