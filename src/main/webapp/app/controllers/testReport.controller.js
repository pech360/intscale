app.controller('reportsController',function($scope,$http, CommonService,toaster,$uibModal,DateFormatService){
  $scope.testName='allTest';
  $scope.fromDate='';
  $scope.toDate='';
  $scope.school='';
  $scope.grade='';
  $scope.category='';
  $scope.subCategory='';
  $scope.customized=false;
  $scope.reportType='default';
  $scope.listSubCategories={};
  $scope.users={};
  $scope.usersdetails={};
  $scope.interestperuser=false;
  $scope.showLoadingIcon=false;
  $scope.userIds=[];
  $scope.isDownloading = false;
    $scope.getAllTakenTestByTenant = function () {
        $http.get('/api/test/listAllTakenTest/').then(function (response) {
            $scope.testList = response.data;
        }, function (response) {
            toaster.pop('error', "Error", response.data.message);
        });
    };
    $scope.getAllTakenTestByTenant();

    $http.get('/api/school/all').then(function(response){
         $scope.listSchooles=response.data;
        });

    $http.get('/api/md/grades/').then(function(response){
         $scope.listGrades=response.data;
        });
    $scope.loaduserList=function () {
        $scope.showLoadingIcon=true;
        $http.get('/api/user/self/userInfoList').then(function (response){
            $scope.users=response.data;
            $scope.showLoadingIcon=false;
        });
    }

    /*CommonService.listCategories().then(function(response){
         $scope.listCategories=response.data;
      });
    $scope.loadSubCategoryByCategory=function(id){
      CommonService.listSubCategoriesByParent(id).then(function(response){
               $scope.listSubCategories=response.data;
            });
    };*/

    $scope.fetchUserDetails=function (username) {
        $http.get('/api/reports/userdetails?username='+username).then(function (response) {
           $scope.usersdetails=response.data;
            if($scope.usersdetails.length<1){
                toaster.pop('info','User Details not found..');
            }
        });
    }
    $scope.loadCategoryByTest=function(testId){
      $http.get('/api/category/loadByTest?testId='+testId).then(function (response) {
                   $scope.listCategories = response.data;
                   $scope.loadSubCategoryByTest(testId);
               }, function (response) {
                   toaster.pop('error', "Error", response.data.message);
               })
      };
      $scope.loadSubCategoryByTest=function(testId){
        $http.get('/api/category/loadSubCategoryByTest?testId='+testId).then(function (response) {
                     $scope.listSubCategories = response.data;
                 }, function (response) {
                     toaster.pop('error', "Error", response.data.message);
                 })
        };

    $scope.loadUsersBySchool=function (flag,school) {
        if(flag){
            if(!school){
                toaster.pop('Error',"School is not selected");
                return;
            }
            if(school=='alluser'){
                $scope.loaduserList();
                return;
            }
            $http.get('/api/user/self/getUserBySchoolAndGrade?school='+school+'&grade='+'').then(function (response){
                $scope.users=response.data;
            })
        }
    }

    $scope.loadUsersBySchoolAndGrade=function (flag,school,grade) {
        if(flag){
            if(!school){
                toaster.pop('Error',"School is not selected");
                return;
            }
           $http.get('/api/user/self/getUserBySchoolAndGrade?school='+school+'&grade='+grade).then(function (response){
               $scope.users=response.data;
           })
        }
    }

    $scope.processReportType=function(type){
     $scope.reportType=type;
    };

    $scope.downloadReports=function(id){
      if(!id&&!($scope.testPerUser)){
        toaster.pop('Error',"Test is not selected");
        return;
      }
     if(!$scope.fromDate){
        toaster.pop('Error',"please select FromDate");
        return;
      }
     if(!$scope.toDate){
         toaster.pop('Error',"please select ToDate");
         return;
       }
       /*if($scope.listSubCategories.length>0&&!$scope.subCategory&&!$scope.scoringEngine){
          toaster.pop('Error',"please select subCategory");
          return;
        }*/

     $scope.testList.forEach(function(e){
          if(e.id==id){
           $scope.testName=e.name;
          }
         });
     if(!$scope.customized&&!$scope.scoringEngine&&!$scope.testPerUser&&!$scope.scoreperuser){
     $scope.reportType='default';
     }
     if(!id){id=0;}
     var fmdate=DateFormatService.formatDate3($scope.fromDate);
     var tdate=DateFormatService.formatDate3($scope.toDate);
     window.location.href = "/api/reports/download/"+$scope.testName+"?id="+id+'&fromdate='+fmdate+'&todate='+tdate+'&school='+$scope.school+'&grade='+$scope.grade+'&reportType='+$scope.reportType+'&category='+$scope.category+'&subCategory='+$scope.subCategory+"&token=Bearer "+localStorage.getItem('ngStorage-token').slice(1,-1);
    };

    $scope.downloadInterestReports=function (userIds) {
        if(userIds.length>0&&userIds[0]==''){
            userIds.splice(0,1);
        }
        if(userIds.length<1){
            toaster.pop('info',"please select Users");
            return;
        }
        window.location.href = "/api/reports/downloaduserInterest?userIds="+userIds+"&token=Bearer "+localStorage.getItem('ngStorage-token').slice(1,-1);
    }

    $scope.components = ['user selected interests', 'user feedbacks'];

    $scope.getComponent = function (componentName) {
        $scope.componentName = componentName;
        $scope.showTestOnCard = false;
        switch ($scope.componentName) {
            case 'user selected interests':
                downloadUsersCsv();
                break;
            case 'user feedbacks':
                downloadUsersFeedback();
                break;
        }
    };


    function downloadUsersCsv(roleId,tenantId){
        $scope.isDownloading = true;
        $http.get('/api/user/self/downloadUserList', {responseType: 'arraybuffer'}).then(function (response) {
            $scope.isDownloading = false;
            var blob = new Blob([response.data], {
                type: 'application/octet-stream'
            });
            saveAs(blob, response.headers("fileName"));
        },function(response){
            $scope.isDownloading = false;
        });
    }

    function downloadUsersFeedback(){
        $scope.isDownloading = true;
        $http.get('/api/md/downloadFeedbacks', {responseType: 'arraybuffer'}).then(function (response) {
            $scope.isDownloading = false;
            var blob = new Blob([response.data], {
                type: 'application/octet-stream'
            });
            saveAs(blob, response.headers("fileName"));
        },function(response){
            $scope.isDownloading = false;
        });
    }
});