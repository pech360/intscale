app.controller('UserTimelineController', function ($scope, $http, toaster, $localStorage, $uibModal, DateFormatService) {
    $scope.assets_url = $localStorage.assets_url;

    $scope.loading = false;
    var fileType = "";
    var fileName = "";
    var imageName = "";
    var videoName = "";
    var allowedImageExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    var allowedVideoExtensions = /(\.mp4)$/i;
    $scope.request = {};

    $scope.galleryColumn1Items = [];
    $scope.galleryColumn2Items = [];
    $scope.galleryColumn3Items = [];
    var postToBeShare = {};

    function fetchProfile() {
        $http.get('/api/user/profile').then(function (response) {
            $scope.user = response.data;
            if ($scope.user.img) {
                $scope.userImageURI = $scope.assets_url + $scope.user.img;
            } else {
                if ($scope.user.gender) {
                    if ($scope.user.gender.toLowerCase() == "male") {
                        $scope.userImageURI = "/assets/img/b-avtar.png"
                    } else {
                        if ($scope.user.gender.toLowerCase() == "female") {
                            $scope.userImageURI = "/assets/img/g-avtar.png"
                        }
                    }
                } else {
                    $scope.userImageURI = "/assets/img/default-profile-picture.png";
                }
            }
            getUserReportStatus($scope.user.id);

        }, function (response) {
            toaster.pop('error', "Error", "Failed to load profile - " + response.data.error);

            if (response.data.error) {
                if (response.data.error == "You are logged in another session, so close this session") {

                }
            }
        });
    }

    $scope.openAddPostModal = function (post) {
        if (post) {
            $scope.request.id = post.id;
            $scope.request.heading = post.heading;
            $scope.request.description = post.description;
            $scope.request.imageName = post.imageName;
            $scope.request.videoName = post.videoName;
            $scope.request.contentType = post.contentType;

            if (post.imageName) {
                $http({
                    method: 'GET',
                    url: '/api/raw/view/PROFILEIMAGE/' + post.imageName,
                    responseType: 'arraybuffer'
                }).then(function (response) {
                    var postImageData = btoa(String.fromCharCode.apply(null, new Uint8Array(response.data)));
                    if (postImageData) {
                        $scope.postImageURI = "data:image/PNG;base64," + postImageData;
                        postImageData = {};
                    }
                }
                );
            }
        }
        $scope.addPostModal = $uibModal.open({
            templateUrl: 'views/modals/addPostModal.html',
            size: 'lg',
            scope: $scope,
            backdrop: 'backdrop'
            // ,
            // backdrop: 'static',
            // keyboard: false
        });
    };

    $scope.closeAddPostModal = function () {
        $scope.clearImage();
        $scope.addPostModal.close();
        $scope.request.id = "";
        $scope.request.heading = "";
        $scope.request.description = "";
        $scope.request.imageName = "";
        $scope.request.videoName = "";
        $scope.request.contentType = "";


    };

    $scope.openMyGalleryModal = function () {
        $scope.galleryColumn1Items = [];
        $scope.galleryColumn2Items = [];
        $scope.galleryColumn3Items = [];
        $http.get('api/timeline/getImagePosts').then(function (response) {
            var galleryItems = response.data;
            galleryItems.reverse();
            var i = 0;
            while (i < galleryItems.length) {
                if (galleryItems[i]) {
                    $scope.galleryColumn1Items.push(galleryItems[i]);
                    i++;
                }
                if (galleryItems[i]) {
                    $scope.galleryColumn2Items.push(galleryItems[i]);
                    i++;
                }
                if (galleryItems[i]) {
                    $scope.galleryColumn3Items.push(galleryItems[i]);
                    i++;
                }
            }

            $scope.myGalleryModal = $uibModal.open({
                templateUrl: 'views/modals/myGalleryModal.html',
                size: 'lg',
                scope: $scope,
                backdrop: 'backdrop'
                // ,
                // backdrop: 'static',
                // keyboard: false
            });
        }, function (response) {
            error(response.data.error);
        });
    };

    $scope.closeMyGalleryModal = function () {
        $scope.myGalleryModal.close();
    };

    $scope.openMyThoughtsModal = function () {
        $http.get('api/timeline/getThoughtPosts').then(function (response) {
            $scope.thoughts = changeCreatedDateFormatOfPosts(response.data);
            $scope.myThoughtsModal = $uibModal.open({
                templateUrl: 'views/modals/myThoughtsModal.html',
                size: 'lg',
                scope: $scope,
                backdrop: 'backdrop'
                // ,
                // backdrop: 'static',
                // keyboard: false
            });
        }, function () {
            error(response.data.error);
        });

    };

    $scope.closeMyThoughtsModal = function () {
        $scope.myThoughtsModal.close();
    };

    $scope.browseImage = function () {
        $('#postImg').trigger('click');
    };

    $scope.setFiles = function (element) {
        $scope.imgfile = element.files[0];
        $scope.upload($scope.imgfile);

    };

    function displayImage(file) {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            $scope.$apply(function () {
                $scope.postImageURI = reader.result;
            });
        };
        reader.onerror = function (error) {
            error("Failed to load image:" + error);
        };
    }

    $scope.upload = function (file) {
        if (file) {
            $scope.loading = true;
            var r = new FileReader();
            var fd = new FormData();
            fd.append('file', file);
            var request = {
                method: 'POST',
                url: '/api/raw/timeline/store/image',
                data: fd,
                headers: {
                    'Content-Type': undefined
                }
            };
            $http(request).then(function (response) {
                $scope.loading = false;
                fileName = response.data[0];
                fileType = checkFileType(fileName);
                if (fileType == "image") {
                    imageName = fileName;
                    videoName = "";
                    $scope.request.videoName = videoName;
                    displayImage($scope.imgfile);
                    toaster.pop('success', "Image uploaded");
                } else {
                    if (fileType == "video") {
                        videoName = fileName;
                        imageName = "";
                        $scope.request.videoName = videoName;
                        toaster.pop('success', "Video uploaded");
                    } else {
                        toaster.pop('error', "File type not supported.");
                    }
                }

            }, function (response) {
                $scope.loading = false;
                toaster.pop('error', response);
            });
        }
    };

    $scope.savePost = function (request) {
        if (!request) {
            toaster.pop('error', "Input can not be blank!");
        }
        request.imageName = "";
        request.videoName = "";
        request.imageName = imageName;
        request.videoName = videoName;

        $http.post('api/timeline/savePost', request).then(function (response) {
            toaster.pop('success', 'You just posted on your timeline.');
            $scope.posts = changeCreatedDateFormatOfPosts(response.data);
            $scope.closeAddPostModal();
            request = {};
            $scope.request = {};
        }, function (response) {
            toaster.pop('error', response.data.error);
            $scope.closeAddPostModal();
        })
    };

    function deletePost(id) {
        $http.delete('api/timeline/' + id).then(function (response) {
            $scope.posts = changeCreatedDateFormatOfPosts(response.data);
            toaster.pop('success', 'Your post is deleted');
        }, function (response) {
            toaster.pop('error', response.data.error)
        })
    }

    function getPosts() {
        $http.get('api/timeline').then(function (response) {
            $scope.posts = changeCreatedDateFormatOfPosts(response.data);
        }, function (response) {
            toaster.pop('error', response.data.error);
        })
    }

    $scope.clearImage = function () {
        $scope.postImageURI = {};
        imageName = "";
        videoName = "";
        angular.element("input[type='file']").val(null);

    };

    function changeCreatedDateFormatOfPosts(posts) {
        for (var i in posts) {
            posts[i].createdDate = DateFormatService.formatDate8(new Date(posts[i].createdDate));
        }
        return posts;
    }


    function checkFileType(fileName) {
        if (allowedImageExtensions.exec(fileName)) {
            return "image";
        } else {
            if (allowedVideoExtensions.exec(fileName)) {
                return "video";
            } else {
                return null;
            }
        }
    }

    function getRecommendedBlogs() {
        $http.get('/api/md/recommendBlogs').then(function (response) {
            $scope.blogs = response.data
        }, function (response) {
            error(response.data.error);
        })
    }

    $scope.like = function (id) {
        $http.post('/api/md/likeBlog/' + id).then(function (response) {
            updateBlogView(response.data);
        }, function (response) {
            error("error in like")
        })
    };

    function updateBlogView(blog) {
        for (var i in $scope.blogs) {
            if ($scope.blogs[i].id == blog.blogId) {
                $scope.blogs[i].liked = blog.liked;
                $scope.blogs[i].likes = blog.likes;
            }
        }
    }

    $scope.sharePostOnFb = function (post) {
        var url = "";
        var title = "";
        var description = "";
        var image = "";
        url = 'https://www.intscale.com';
        if (post.heading) {
            title = post.heading;
        }
        if (post.description) {
            description = post.description;
        }
        if (post.imageName) {
            image = $scope.assets_url + post.imageName;
        }

        FB.ui({
            method: 'share_open_graph',
            action_type: 'og.shares',
            display: 'popup',
            action_properties: JSON.stringify({
                object: {
                    'og:url': url,
                    'og:title': title,
                    'og:description': description,
                    'og:image': image
                }
            })
        }, function (response) {

        });
    };

    $scope.sharePostOnEmail = function (post) {
        $scope.askForEmailForPostShare = $uibModal.open({
            templateUrl: 'views/modals/askForEmailForPostShare.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static',
            keyboard: false
        });
        postToBeShare = post;
    };

    $scope.closeSharePostOnEmail = function (email, body) {
        if (email) {
            $http.post('/api/timeline/sharePostOnEmail?id=' + postToBeShare.id + '&email=' + email + '&body=' + body).then(function (response) {
                ok("email sent");
                $scope.askForEmailForPostShare.close();
                postToBeShare = {};
            }, function (response) {
                error(response.data.error);
                $scope.askForEmailForPostShare.close();
            });
        } else {
            error("email-id is missing!");
        }
    };

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.askForDelete = function (id) {
        swal({
            title: 'Do you really want to delete this post?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#fab400',
            cancelButtonColor: '#4158d5',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.value) {
                deletePost(id)
            }
        })
    };

    $scope.downloadIndividualReport = function (id, update) {
        $scope.dataLoading = true;
        $scope.loader();
        $http.get('api/reports/downloadAimReport/' + id + '/' + update + '/MAIN', { responseType: 'arraybuffer' }).then(function (response) {
            $scope.cancelLoader();
            $scope.dataLoading = false;
            var blob = new Blob([response.data], {
                type: 'application/octet-stream'
            });
            saveAs(blob, response.headers("fileName"));
        }, function (response) {
            $scope.cancelLoader();
            $scope.dataLoading = false;
            error("download failed");
        })
    };


    $scope.loader = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelLoader = function () {
        $scope.modalInstance.close();
    };

    function getUserReportStatus(userId) {
        $http.get('/api/reports/userReportStatus/byUser?userId=' + userId).then(
            function (response) {
                $scope.userReportStatus = response.data;

            }, function (response) {
            });
    }

    getPosts();
    fetchProfile();
    getRecommendedBlogs();
});