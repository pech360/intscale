app.controller('HomeController', function ($scope, $http, toaster, $uibModal, $localStorage, $stateParams) {
    $scope.assets_url = $localStorage.assets_url;
    $scope.show404page = false;



    $scope.getAllTakenTestByUser = function () {
        $http.get('/api/test/').then(function (response) {
            $scope.tests = response.data;
            $scope.show404page = !$scope.tests.length;

        }, function (response) {
            toaster.pop('error', "Error", response.data.message);
        });
    };

    $scope.getAllTakenTestByUser();

    $scope.getLogoForAllAlreadyTakenTest = function () {
        $scope.testCount = 0;
        $scope.innerTestCount = 0;
        for ($scope.testCount; $scope.testCount < $scope.tests.length; $scope.testCount++) {

            $http({
                method: 'GET',
                url: '/api/raw/view/PROFILEIMAGE/' + $scope.tests[$scope.testCount].logo,
                responseType: 'arraybuffer'
            }).then(function (response) {
                $scope.testImgData = btoa(String.fromCharCode.apply(null, new Uint8Array(response.data)));
                if ($scope.testImgData) {
                    $scope.testImageURI = "data:image/PNG;base64," + $scope.testImgData;
                    $scope.tests[$scope.innerTestCount].logo = $scope.testImageURI;
                    $scope.testImgData = {};
                }
                else {
                    $scope.tests[$scope.innerTestCount].logo = "/assets/img/default-test-picture.jpeg"
                }
                $scope.innerTestCount++;
            }, function (response) {
                $scope.tests[$scope.innerTestCount].logo = "/assets/img/default-test-picture.jpeg";
                $scope.innerTestCount++;
                toaster.pop('error', "Failed to load test's image ");
            }
            );
        }
    };


    // $scope.subscriptionKeyModal = function () {
    //     modalInstance = $uibModal.open({
    //         templateUrl: 'CustomModal.html',
    //         size: 'lg',
    //         scope: $scope
    //     });
    //     modalInstance.result.then(function (data) {
    //     }, function () {
    //     });
    //
    // };
    // $scope.close = function () {
    //     modalInstance.dismiss('close');
    // };

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.downloadIndividualReport = function (id, update) {
        $scope.dataLoading = true;
        $scope.loader();
        $http.get('api/reports/downloadAimReport/' + id + '/' + update + '/MAIN', { responseType: 'arraybuffer' }).then(function (response) {
            $scope.cancelLoader();
            $scope.dataLoading = false;
            var blob = new Blob([response.data], {
                type: 'application/octet-stream'
            });
            saveAs(blob, response.headers("fileName"));
        }, function (response) {
            $scope.cancelLoader();
            $scope.dataLoading = false;
            error("download failed");
        })
    };



    function fetchProfile() {
        $http.get('/api/user/profile').then(function (response) {
            $scope.user = response.data;
            $scope.user.dob = new Date(response.data.dob);
            if ($scope.user.gender) {
                $scope.user.gender = $scope.user.gender.toLowerCase();
            }
            $scope.selectedInterest = response.data.interest;
            if ($scope.user.img) {
                $scope.userImageURI = $scope.assets_url + $scope.user.img;
            } else {
                if ($scope.user.gender) {
                    if ($scope.user.gender.toLowerCase() == "male") {
                        $scope.userImageURI = "/assets/img/b-avtar.png"
                    } else {
                        if ($scope.user.gender.toLowerCase() == "female") {
                            $scope.userImageURI = "/assets/img/g-avtar.png"
                        }
                    }
                } else {
                    $scope.userImageURI = "/assets/img/default-profile-picture.png";
                }
            }

            getUserReportStatus($scope.user.id);
        }, function (response) {
            toaster.pop('error', "Error", "Failed to load profile - " + response.data.error);

            if (response.data.error) {
                if (response.data.error == "You are logged in another session, so close this session") {

                }
            }
        });
    }
    $scope.loader = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelLoader = function () {
        $scope.modalInstance.close();
    };

    function getUserReportStatus(userId) {
        $http.get('/api/reports/userReportStatus/byUser?userId=' + userId).then(
            function (response) {
                $scope.userReportStatus = response.data;

            }, function (response) {
                error(response.data.error)
            });
    }



    fetchProfile();
});
