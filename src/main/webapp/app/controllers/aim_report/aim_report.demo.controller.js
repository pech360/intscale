app.controller('AimReportDemoController', function ($http, $scope, $stateParams, DateFormatService, $localStorage, $translate) {


    $scope.finished = false;
    var userId = $stateParams.id;
    if (userId) {
        fetchProfileByUserId(userId);
    } else {
        fetchProfile();
    }

    $scope.assets_url = $localStorage.assets_url;
    $scope.bestFit = {regions: [], careers: []};
    $scope.moderateFit = {regions: [], careers: []};
    $scope.worstFit = {regions: [], careers: [], careersBuckets: []};

    function fetchProfile() {
        $http.get('/api/user/profile').then(function (response) {
            $scope.user = response.data;
            userId = $scope.user.id;
            $scope.user.firstName = $scope.user.name.split(' ');
            $scope.user.firstName = $scope.user.firstName[0];
            $scope.user.dob = DateFormatService.formatDate(new Date(response.data.dob));

            $scope.translationData = {
                $name$: $scope.user.firstName,
                gender: $scope.user.gender.toLowerCase()
            };

            if ($scope.user.higherEducation) {
                $scope.user.education = $scope.user.higherEducation;
            } else {
                $scope.user.education = $scope.user.grade;
            }


        });
    }

    function fetchProfileByUserId(id) {
        $http.get('/api/user/self/userProfile?userId=' + id).then(function (response) {
            $scope.user = response.data;
            $scope.user.firstName = $scope.user.name.split(' ');
            $scope.user.firstName = $scope.user.firstName[0];
            $scope.user.dob = DateFormatService.formatDate(new Date(response.data.dob));

            $scope.translationData = {
                $name$: $scope.user.firstName,
                gender: $scope.user.gender.toLowerCase()
            };

            if ($scope.user.higherEducation) {
                $scope.user.education = $scope.user.higherEducation;
            } else {
                $scope.user.education = $scope.user.grade;
            }


        });
    }

    $scope.switchLanguage = function (language) {
        var key = "";
        switch (language) {
            case "English":
                key = "en";
                break;
            case "Hindi":
                key = "hi";
                break;
            case "Marathi":
                // $("body").css({'font-size': '16px'});
                key = "mr";
                break;

            default:
                key = "en";
                break;
        }
        $translate.use(key);
    };

    $scope.getScores = function () {
        $http.get('/api/reports/aimReportDemo?userId=' + userId).then(
            function (response) {
                $scope.reportSections = response.data;
                $scope.getCareerInterestInventory();
                getInterestGraphRender($scope.reportSections[0].interestInventoryResponse.region);
                $scope.switchLanguage($scope.user.language);
                $scope.finished = true;
            }, function (response) {
                error(response.data.error)
            });
    };


    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }


    function getInterestGraphRender(bestFitRegion, moderateFitRegion1, moderateFitRegion2) {
        var selectedSegment = document.getElementById("section-" + bestFitRegion);
        if (selectedSegment) {
            selectedSegment.style.fill = '#00bb7e';
        }
        selectedSegment = document.getElementById("section-" + moderateFitRegion1);
        if (selectedSegment) {
            selectedSegment.style.fill = '#fab400';
        }
        selectedSegment = document.getElementById("section-" + moderateFitRegion2);
        if (selectedSegment) {
            selectedSegment.style.fill = '#fab400';
        }


    }

    $scope.getCareerInterestInventory = function () {

        $http.get('/api/aimExpert/careerInterestInventory').then(function (response) {
                var careerInterestRecords = response.data;


                var region = $scope.reportSections[0].interestInventoryResponse.region;

                $scope.bestFit.regions.push(region);


                switch (region) {
                    case 1 :
                        $scope.moderateFit.regions.push(2);
                        $scope.moderateFit.regions.push(12);
                        break;
                    case 12 :
                        $scope.moderateFit.regions.push(1);
                        $scope.moderateFit.regions.push(11);
                        break;
                    default:
                        $scope.moderateFit.regions.push(region - 1);
                        $scope.moderateFit.regions.push(region + 1);
                        break;
                }


                getInterestGraphRender($scope.bestFit.regions[0], $scope.moderateFit.regions[0], $scope.moderateFit.regions[1]);


                for (var i in careerInterestRecords) {
                    if (region === careerInterestRecords[i].region) {
                        $scope.bestFit.careers.push(careerInterestRecords[i]);
                    } else {
                        if (((region - 1) === careerInterestRecords[i].region ) || ((region + 1) === careerInterestRecords[i].region)) {
                            $scope.moderateFit.careers.push(careerInterestRecords[i]);
                        } else {
                            $scope.worstFit.careers.push(careerInterestRecords[i]);
                        }
                    }
                }

                var worstFitCareers = angular.copy($scope.worstFit.careers);

                // make 3 buckets;
                var size = worstFitCareers.length / 3;
                var remainder = worstFitCareers.length % 3;
                if (remainder > 0) {
                    size++;
                }

                $scope.worstFit.careersBuckets.push(worstFitCareers.splice(0, size));
                $scope.worstFit.careersBuckets.push(worstFitCareers.splice(0, size));
                $scope.worstFit.careersBuckets.push(worstFitCareers);


            }, function (response) {
                error(response.data.error);
            }
        );

    };


    $scope.getScores();

});