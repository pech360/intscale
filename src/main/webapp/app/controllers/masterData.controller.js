app.controller('createMasterDataController', function ($scope, $http, Auth, CommonService, $state, toaster) {
    $scope.selectedType = '';

    $scope.refresh = function () {
        $http.get('/api/md/list').then(function (response) {
            $scope.records = response.data;
            $scope.selectedType = '';

        });
    };

    $scope.refresh();

    $scope.newRecord = function () {
        $scope.masterRecords = {
            mode: 'New'
        };
        $scope.editMasterDataPanel = true;
    };
    $scope.updateRecord = function (record) {
        $scope.editMasterDataPanel = true;
        $scope.masterRecords = record;
    };

    $scope.cancelUser = function () {
        $scope.masterRecords = {};
        $scope.showMasterDataPanel = false;
        $scope.editMasterDataPanel = false;
    };
    $scope.removeRecord = function (record) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Delete!'
        }).then(function (result) {
            if (result.value) {

                $http.delete('/api/md/delete?id=' + record.id).then(function () {
                    ok("Record has been deleted.");
                    $scope.refresh();
                }, function (response) {
                    error(response.data.error);
                });
            }
        })

    };
    $scope.types = ["Degree", "Designation", "Gender", "Grade","Section","Hobby", "Industry", "Interest", "Language", "Stream", "StudentType","TeacherType", "Subject", "UserType", "Group", "Pilot", "City", "Tag"];

    $scope.saveRecord = function (record) {
        if (record.id) {
            $http.put('/api/md/update', record).then(function (response) {
                if (response.data.id == null) {
                    error("Record is not updated!");
                } else {
                    ok("Record updated!");
                    $scope.refresh();
                }
            }, function (response) {
                error(response.data.error);
            });
        } else {
            $http.post('/api/md/create', record).then(function (response) {
                if (response.data.id == null) {
                    error("Record is not created!");
                } else {
                    $scope.refresh();
                    ok("Record created!");
                }
            }, function (response) {
                error(response.data.error);
            });
        }
    };

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.getDesi = function () {

        alert("got it");
    };

    $scope.getFilteredMasterData = function (selectedType) {
        if (selectedType == "Designation") {
            $http.get('/api/md/designations/').then(function (response) {
                $scope.records = response.data;
            })
        }
        if (selectedType == "Degree") {
            $http.get('/api/md/degrees/').then(function (response) {
                $scope.records = response.data;
            })
        }
        if (selectedType == "Gender") {
            $http.get('/api/md/gender/').then(function (response) {
                $scope.records = response.data;
            })
        }
        if (selectedType == "Grade") {

            $http.get('/api/md/grades/').then(function (response) {
                $scope.records = response.data;
            })
        }
        if (selectedType == "Hobby") {
            $http.get('/api/md/hobbies/').then(function (response) {
                $scope.records = response.data;
            })
        }
        if (selectedType == "Industry") {
            $http.get('/api/md/industries/').then(function (response) {
                $scope.records = response.data;
            })
        }
        if (selectedType == "Interest") {
            $http.get('/api/md/interests/').then(function (response) {
                $scope.records = response.data;
            })
        }
        if (selectedType == "Language") {
            $http.get('/api/md/languages/').then(function (response) {
                $scope.records = response.data;
            })
        }
        if (selectedType == "Stream") {
            $http.get('/api/md/streams/').then(function (response) {
                $scope.records = response.data;
            })
        }
        if (selectedType == "Subject") {
            $http.get('/api/md/subjects/').then(function (response) {
                $scope.records = response.data;
            })
        }
        if (selectedType == "StudentType") {
            $http.get('/api/md/studentTypes/').then(function (response) {
                $scope.records = response.data;
            })
        }
        if (selectedType == "UserType") {
            $http.get('/api/md/userTypes/').then(function (response) {
                $scope.records = response.data;
            })
        }
        if (selectedType == "Group") {
            $http.get('/api/md/groups/').then(function (response) {
                $scope.records = response.data;
            })
        }
        if (selectedType == "Pilot") {
            $http.get('/api/md/pilots/').then(function (response) {
                $scope.records = response.data;
            })
        }
        if (selectedType == "Section") {
            $http.get('/api/md/sections/').then(function (response) {
                $scope.records = response.data;
            })
        }

        if (selectedType == "City") {
            $http.get('/api/md/cities/').then(function (response) {
                $scope.records = response.data;
            })
        }
    };

});
