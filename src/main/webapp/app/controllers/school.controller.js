app.controller("SchoolController", function ($scope, $http, $state, toaster) {
    $scope.school = {};
    $scope.schoolOnCards = {};
    $scope.showform = false;
    $scope.showCard = true;
    $scope.showedit = false;

    $scope.getAllSchool = function () {
        $http.get("/api/school/all").then(function (response) {
            $scope.schoolOnCards = {};
            $scope.schoolOnCards = response.data;
            $scope.schoolCount = 0;
            $scope.innerSchoolCount = 0;
            for ($scope.schoolCount; $scope.schoolCount < $scope.schoolOnCards.length; $scope.schoolCount++) {

                $http({
                    method: 'GET',
                    url: '/api/raw/view/PROFILEIMAGE/' + $scope.schoolOnCards[$scope.schoolCount].logo,
                    responseType: 'arraybuffer'
                }).then(function (response) {
                        $scope.schoolImgData = btoa(String.fromCharCode.apply(null, new Uint8Array(response.data)));
                        if ($scope.schoolImgData) {
                            $scope.schoolImageURI = "data:image/PNG;base64," + $scope.schoolImgData;
                            $scope.schoolOnCards[$scope.innerSchoolCount].logo = $scope.schoolImageURI;
                        } else {
                            $scope.schoolOnCards[$scope.innerSchoolCount].logo = "/assets/img/default-school-picture.jpeg"
                        }
                        $scope.innerSchoolCount++;
                    }, function (response) {
                        toaster.pop('error', "Failed to load school's image");
                    }
                );
            }
        }, function (response) {
            error("failed");
        });
    };

    $scope.getAllSchool();

    $scope.cancelSchool = function () {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.value) {
                $scope.school = {};
                ok("Cancelled");
                $scope.showSchool();
                $scope.school = {};
            }
        })
    };

    $scope.deleteSchool = function (schoolId) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Delete!'
        }).then(function (result) {
            if (result.value) {
                $scope.showedit = false;
                $scope.showform = false;
                $scope.showCard = true;
                $http.delete("/api/school/" + schoolId).then(function (response) {
                    ok("success");
                    $scope.getAllSchool();
                }, function (response) {
                    error("failed");
                });
            }
        })


    };

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.addSchool = function () {
        $scope.showform = true;
        $scope.showCard = false;
    };

    $scope.showSchool = function () {
        $scope.showform = false;
        $scope.showCard = true;
    };

    $scope.editSchool = function (schoolId) {
        $scope.showedit = true;
        $scope.showform = true;
        $scope.showCard = false;
        $http.get("/api/school/" + schoolId).then(function (response) {
            $scope.school = response.data;
        }, function (response) {
            error("failed");
        });
    };

    $scope.deleteSchoolRestCall = function (schoolId) {
        $scope.showedit = false;
        $scope.showform = false;
        $scope.showCard = true;
        $http.delete("/api/school/" + schoolId).then(function (response) {
            ok("success");
            $scope.getAllSchool();
        }, function (response) {
            error("failed");
        });
    };

    $scope.save = function () {
     if($scope.school.mobile)
        {
          $scope.school.mobile=JSON.stringify($scope.school.mobile);
        }
        $scope.school.logo='';
        if ($scope.showedit) {
            $http.put('/api/school/', $scope.school).then(function (response) {
                ok("School updated");
                $scope.school = {};
                $scope.getAllSchool();
            }, function (response) {
                error("failed");
            });
            $scope.showedit = false;
        } else {
            $http.post("/api/school", $scope.school).then(function (response) {
                ok("success");
                $scope.school = {};
                $scope.getAllSchool();
            }, function (response) {
                error("error");
            });
        }
    }
});