app.controller('MarketeerController', function ($scope, $http, DateFormatService, toaster, $uibModal) {

    var request = {
        tenants: [],
        grades: [],
        cities: [],
        schools: [],
        products: [],
        tests: [],
        fromDate: {},
        toDate: {}
    };

    var date = new Date();
    var ss = date.getSeconds();
    var mm = date.getMinutes();
    var hh = date.getHours();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();


    $scope.fromDate = new Date(y, m, d - 1, hh, mm, ss);
    $scope.toDate = new Date();

    $scope.reset = function () {
        var request = {
            tenants: [],
            grades: [],
            schools: [],
            products: [],
            tests: [],
            fromDate: {},
            toDate: {}
        };

        $scope.fromDate = new Date(y, m, d - 1, hh, mm, ss);
        $scope.toDate = new Date();
        $scope.loading = false;
        $scope.newUserLoading = false;
    };

    function getAllGrades() {
        $http.get('api/md/grades/').then(function (response) {
            $scope.grades = response.data;
            $scope.grades.isAllSelected = function () {
                for (var i = 0; i < this.length; i++) {
                    var item = this[i];
                    if (!item.selected) {
                        return false;
                    }
                }
                return true;
            }
        });
    }

    function getAllCities() {
        $http.get('api/md/cities/').then(function (response) {
            $scope.cities = response.data;
            $scope.cities.isAllSelected = function () {
                for (var i = 0; i < this.length; i++) {
                    var item = this[i];
                    if (!item.selected) {
                        return false;
                    }
                }
                return true;
            }
        });
    }

    function getAllSchools() {
        $http.get("/api/school/all").then(function (response) {
            $scope.schools = response.data;
            $scope.schools.isAllSelected = function () {
                for (var i = 0; i < this.length; i++) {
                    var item = this[i];
                    if (!item.selected) {
                        return false;
                    }
                }
                return true;
            }
        });
    }

    function getAllGender() {
        $scope.genders = [
            { gender: "Male", "selected": false },
            { gender: "Female", "selected": false }
        ];
        $scope.genders.isAllSelected = function () {
            for (var i = 0; i < this.length; i++) {
                var item = this[i];
                if (!item.selected) {
                    return false;
                }
            }
            return true;
        }
    }

    function getAllTenant() {
        $http.get('/api/user/self/tenants').then(function (response) {
            $scope.tenants = response.data;
            $scope.tenants.isAllSelected = function () {
                for (var i = 0; i < this.length; i++) {
                    var item = this[i];
                    if (!item.selected) {
                        return false;
                    }
                }
                return true;
            }
        });
    }

    $scope.selectAll = function (items, selected) {
        if (selected) {
            for (var i in items) {
                items[i].selected = true;
            }
            return items;
        } else {
            unselectAll(items);
        }
    };

    function unselectAll(items) {
        for (var i in items) {
            items[i].selected = false;
        }
        return items;
    }

    function populateDashRequest() {


        request.grades = [];
        request.schools = [];
        request.cities = [];
        request.tenants = [];
        request.fromDate = DateFormatService.formatDate9($scope.fromDate);
        request.toDate = DateFormatService.formatDate9($scope.toDate);

        for (var i in $scope.grades) {
            if ($scope.grades[i].selected) {
                if ($scope.grades[i].value) {
                    request.grades.push($scope.grades[i].value);
                }
            }
        }

        for (var i in $scope.schools) {
            if ($scope.schools[i].selected) {
                if ($scope.schools[i].name) {
                    request.schools.push($scope.schools[i].name);
                }
            }
        }
        for (var i in $scope.cities) {
            if ($scope.cities[i].selected) {
                if ($scope.cities[i].value) {
                    request.cities.push($scope.cities[i].value);
                }
            }
        }
        for (var i in $scope.tenants) {
            if ($scope.tenants[i].selected) {
                if ($scope.tenants[i].id) {
                    request.tenants.push($scope.tenants[i].id);
                }
            }
        }

        return request;
    }


    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }


    $scope.getNewUsersCount = function () {
        // $scope.newUserLoading = true;
        // $scope.activeItem = 1;

        populateDashRequest();

        $http.post('/api/dashboard/getNewUsersCount', request).then(function (response) {
            $scope.newUsersCount = response.data;
            $scope.getUsersForAimTestsByStatusCount('Generated');
            $scope.newUserLoading = false;

        }, function (response) {



            // $scope.newUserLoading = false;
            // $scope.loading = false;
        })
    };

    $scope.getNewUsers = function () {
        $scope.newUserLoading = true;
        $scope.activeItem = 1;
        loader();
        $http.post('/api/dashboard/getNewUsers', request).then(function (response) {
            $scope.users = response.data;
            $scope.newUsersCount = $scope.users.length;

            $scope.newUserLoading = false;
            cancelLoader();
        }, function (response) {

            $scope.newUserLoading = false;
            cancelLoader();
        })
    };

    $scope.getUsersForAimTestsByStatusCount = function (status) {

        $http.post('/api/dashboard/getUsersForAimTestsByStatusCount?status=' + status, request).then(function (response) {
            if (status == "InProgress") {
                $scope.activeItem = 2;
                $scope.inProgressCount = response.data;
            }
            if (status == "Generated") {
                $scope.activeItem = 3;
                $scope.generatedCount = response.data;
                $scope.getUsersForAimTestsByStatusCount('InProgress');
            }
        }, function (response) {

        })
    };

    $scope.getUsersForAimTestsByStatus = function (status) {
        loader();
        $http.post('/api/dashboard/getUsersForAimTestsByStatus?status=' + status, request).then(function (response) {
            if (status == "InProgress") {
                $scope.activeItem = 2;
                $scope.users = response.data;
                $scope.inProgressCount = $scope.users.length;
            }
            if (status == "Generated") {
                $scope.activeItem = 3;
                $scope.users = response.data;
                $scope.generatedCount = $scope.users.length;
                // $scope.getUsersForAimTestsByStatus('InProgress');
            }
            cancelLoader();
        }, function (response) {

            cancelLoader();
        })
    };

    function loader() {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    }

    function cancelLoader() {
        $scope.modalInstance.close();
    }

    $scope.getResult = function () {
        populateDashRequest();
        $scope.getNewUsersCount();
    };


    $scope.init = function () {
        getAllGrades();
        getAllSchools();
        getAllCities();
        getAllTenant();
        $scope.reset();
        $scope.getResult();
    };

    $scope.init();

});