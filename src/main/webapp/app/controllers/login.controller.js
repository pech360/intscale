app.controller('LoginController', function ($scope, $state, $http, Auth, $stateParams, $rootScope, toaster, $uibModal, $translate) {

    $scope.switchLanguage = function (key) {
        $translate.use(key);
    };


    $scope.pkgAmount = $stateParams.pkgAmount;
    if ($stateParams.pkgAmount) {
        $scope.pkgAmount = $stateParams.pkgAmount;
        if (Auth.getUser()) {
            $state.go("paymentgateway", { 'pkgAmount': $scope.pkgAmount });
        }
    } 
    // else {
    //     if (Auth.getUser()) {
    //         $state.go("app.availabletests");
    //     }
    // }

    $scope.buyTest = function () {
        if ($stateParams.pkgAmount) {
            $scope.pkgAmount = $stateParams.pkgAmount;
            // $state.go('RegisterController'); 

            $state.go('register', { 'pkgAmount': $scope.pkgAmount });
        } else {
            $state.go('register');

        }
    }


    $scope.disappearNavbar = true;
    $rootScope.flow = 'login';
    $scope.tokenString = "";
    $scope.newPassword = "";
    $scope.reNewPassword = "";
    $scope.logoname = '';
    $scope.backGroundimageName = '';
    $scope.orgName = '';
    $scope.username = '';
    $scope.password = '';
    $scope.emailSent = false;
    $scope.f = false;
    $scope.g = false;
    $scope.logoname = window.location.hostname.split(".")[0] + 'logo.png';
    $scope.backGroundimageName = window.location.hostname.split(".")[0] + 'back.png';
    $scope.internalLogo = window.location.hostname.split(".")[0] + 'intlogo.png';
    $scope.orgName = window.location.hostname.split(".")[0];

    $scope.showLoginCard = !($scope.orgName == 'assess' || $scope.orgName == 'localhost');
    $rootScope.$on('event:social-sign-in-success', function (event, user) {

        if ($rootScope.flow === 'login') {

            Auth.socialLogin(user, authSuccess, function (res) {
                $scope.f = false;
                $scope.g = false;
                toaster.pop("Error", res.data.error);
            });

            function authSuccess(res) {
                let user = Auth.getUser();
                if (user.dashboardType == 'Counsellor') {
                    $state.go('app.counsellorCalender');
                    return;
                }
                if (user.dashboardType == 'CommonDashboard') {
                    $state.go('app.dashboard');
                    return;
                }
                if ($scope.pkgAmount) {
                    // options.prefill.email = res.email;
                    // options.notes.username = user.sub;
                    // var rzp1 = new Razorpay(options);
                    // rzp1.open();
                    $state.go("paymentgateway", { 'pkgAmount': $scope.pkgAmount })
                } else {
                    $state.go("app.availabletests");
                    // $state.go("welcome");
                }
                // $state.go('app.availabletests');
            }
        } else {
        }
    }, function (error) {

    });

    $scope.login = function () {
        var formData = {
            username: $scope.username,
            password: $scope.password
        };

        $scope.dataLoading = true;
        Auth.login(formData,
            authSuccess,
            function (res) {
                $scope.error = res.data.error;
                $scope.dataLoading = false;
                toaster.pop("Error", res.data.error);

            }
        );

        function authSuccess(res) {
            if ($stateParams.pkgAmount) {
                $scope.pkgAmount = $stateParams.pkgAmount;
                $scope.dataLoading = false;
                $state.go('register', { 'pkgAmount': $scope.pkgAmount });
                return;
            }
            let user = Auth.getUser();
            if (user.dashboardType == 'Counsellor') {
                $state.go('app.counsellorDashboard2');
                $scope.dataLoading = false;
                return;
            }
            if (user.dashboardType == 'CommonDashboard') {
                $state.go('app.dashboard');
                $scope.dataLoading = false;
                return;
            }
            if (user.dashboardType == 'Management') {
                $state.go('app.leapDashboardManagement');
                $scope.dataLoading = false;
                return;
            }
            if (user.dashboardType == 'Teacher') {
                $state.go('app.leapDashboardTeacher');
                $scope.dataLoading = false;
                return;
            }

            $state.go('app.availabletests');
            $scope.dataLoading = false;
        }
    };


    $scope.displayForgotPassword = function () {
        if (!$scope.username) {
            toaster.pop('error', "Please, type in your username or registered email and then click on Send Email");

        } else {
            $http.post('/emailResetPassword', $scope.username).then(
                function (response) {
                    toaster.pop('success', "Email Sent");
                    $scope.emailSent = true;

                }, function (response) {
                    toaster.pop('error', response.data.error);
                    $scope.emailSent = false;
                }
            );

        }
    };

    $scope.openDisplayForgotPassword = function () {
        $scope.displayForgotPasswordModal = $uibModal.open({
            templateUrl: 'views/modals/forgotPassword.html',
            size: 'md',
            controller: 'LoginController',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelDisplayForgotPasswordModal = function () {
        $scope.displayForgotPasswordModal.close();
    };

    $scope.resetPassword = function () {
        var reset = {};
        reset.token = $scope.tokenString;
        reset.password = $scope.newPassword;

        if (!$scope.tokenString) {
            toaster.pop('error', "Token missing!");
            return;
        }

        if (!$scope.newPassword) {
            toaster.pop('error', "Password missing!");
            return;
        }

        if (reset.password != $scope.reNewPassword) {
            toaster.pop('error', "passwords do not match!");
            return;
        }
        $http.post('/resetPassword', reset).then(
            function (response) {
                toaster.pop('success', "Password Reset Successfully");
                $scope.cancelDisplayForgotPasswordModal();
            }, function (response) {
                -
                    toaster.pop('error', response.data.error);
            }
        );
    };

    $scope.showLoginCardFunction = function () {
        $scope.showLoginCard = true;
    }

});