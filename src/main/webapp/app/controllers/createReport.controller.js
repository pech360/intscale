app.controller('CreateReportController', function ($scope, $http, $uibModal) {

    $scope.selectedReportSections = [];
    var selectedReportSectionsIds = [];
    $scope.report = {};

    function getReportSections() {
        $http.get('/api/reports/getReportSections').then(function (response) {
            $scope.reportSections = response.data;
        }, function (response) {
            error(response.data.error);
        })
    }

    function getReports() {
        $http.get('/api/reports/getReports').then(function (response) {
            $scope.reports = response.data;
        }, function (response) {
            error(response.data.error);
        })
    }


    function getIdsOfSelectedReportSections() {
        for (var i in $scope.selectedReportSections) {
            selectedReportSectionsIds.push($scope.selectedReportSections[i].id);
        }
        return selectedReportSectionsIds;
    }

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.getReport = function (id) {
        $http.get('/api/reports/getReport/' + id).then(function (response) {
            $scope.report = response.data;
        }, function (response) {
            error(response.data.error);
        })
    };

    $scope.copyJuniorNormsToSenior = function (id) {
        $scope.loader();
        $http.get('/api/reports/copyJuniorNormsToSenior/' + id).then(function (response) {
            ok("success");
            $scope.cancelLoader();
        }, function (response) {
            $scope.cancelLoader();
            error(response.data.error);
        })
    };

    $scope.checkAndAllowAllUserForReport = function (id) {
        $scope.loader();
        $http.get('/api/reports/checkAndAllowAllUserForReport/' + id).then(function (response) {
            $scope.cancelLoader();
            ok("success");
        }, function (response) {
            $scope.cancelLoader();
            error(response.data.error);

        })
    };


    $scope.addSelectedReportSection = function (reportSection) {
        reportSection = JSON.parse(reportSection);
        var alreadyThere = false;
        for (var i = 0; i < $scope.selectedReportSections.length; i++) {
            if ($scope.selectedReportSections[i].id == reportSection.id) {
                alreadyThere = true;
            }
        }
        if (!alreadyThere) {
            $scope.selectedReportSections.push(reportSection);
        }
    };

    $scope.removeSelectedReportSection = function (index) {
        $scope.selectedReportSections.splice(index, 1)
    };


    $scope.save = function () {
        if ($scope.report.name) {

            $scope.report.idsOfReportSection = getIdsOfSelectedReportSections();
            $http.post('/api/reports/saveReport', $scope.report).then(function (response) {
                $scope.reports = response.data;
                ok("success")
            }, function (response) {
                error(response.data.error);
            })
        } else {
            error("Report name can not be empty")
        }
    };

    $scope.loader = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelLoader = function () {
        $scope.modalInstance.close();
    };

    $scope.downloadAimreportInExcel = function () {
        $http.get('/api/reports/downloadAimReportInExcel').then(function (response) {
            var blob = new Blob([response.data], {
                type: 'application/octet-stream'
            });
            saveAs(blob, response.headers("fileName"));
        }, function (response) {
        })
    };


    getReportSections();
    getReports();
});



