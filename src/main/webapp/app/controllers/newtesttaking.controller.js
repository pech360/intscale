app.controller('NewtesttakingController', function ($scope, $http, $state, $stateParams) {

    var slider1 = document.getElementById('sliderScale');

    noUiSlider.create(slider1, {
        start: [0],
        step: 1,
        behaviour: 'drag-tap',
        connect: [true,false],
        tooltips: true,
        range: {
            'min': 0,
            'max': 5
        },
        pips: {
            mode: 'steps',
            stepped: true,
            density: 4
        }
    });
    document.getElementById('write-button').addEventListener('click', function () {
        slider1.noUiSlider.set(0);
    });
    document.getElementById('read-button').addEventListener('click', function () {
        alert(Math.round(slider1.noUiSlider.get()));
    });
});
