app.controller('UserReportStatusController', function ($scope, $http, $uibModal, DateFormatService, $localStorage) {

    $scope.assets_url = $localStorage.assets_url;

    $scope.reportStatus = ['Wait', 'Generating', 'Queued', 'Generated', 'Failed', 'InProgress', 'All'];

    $scope.statusCountChart = {
        data: [],
        labels: []
    };
    $scope.colors = ["#fab400#", "#00bb7e", "#4158d5", "#00b0ff", "#e91e63", "#ff5722"];

    var request = {
        tenants: [],
        grades: [],
        schools: [],
        products: [],
        tests: [],
        fromDate: {},
        toDate: {}
    };

    var date = new Date();
    var ss = date.getSeconds();
    var mm = date.getMinutes();
    var hh = date.getHours();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $scope.fromDate = new Date(y, m, d - 1, hh, mm, ss);
    $scope.toDate = new Date();

    $scope.ccEmail = "";
    $scope.toEmail = "";
    $scope.selectedItemsCount = 0;
    $scope.shareMultipleReportsLoading = false;
    $scope.isSendToTestTaker = false;
    $scope.loader = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelLoader = function () {
        $scope.modalInstance.close();
    };

    function scrollToDiv(selector) {
        $('html,body').animate({
                scrollTop: $(selector).offset().top
            },
            'slow');
    }

    $scope.getUserReportStatus = function (status) {
        populateDashRequest();
        if (status) {
            if (request) {
                $scope.loader();
                $http.post('/api/reports/userReportStatus?status=' + status, request).then(function (response) {
                    $scope.cancelLoader();
                    $scope.status = status;
                    $scope.items = response.data;
                    for (var i in $scope.items) {
                        switch ($scope.items[i].status) {
                            case 'Wait':
                                $scope.items[i].statusClass = 'badge  badge-info';
                                break;
                            case 'Queued':
                                $scope.items[i].statusClass = 'badge  badge-success';
                                break;
                            case 'Generating':
                                $scope.items[i].statusClass = 'badge  badge-default';
                                break;
                            case 'Generated':
                                $scope.items[i].statusClass = 'badge  badge-default';
                                break;
                            case 'Failed':
                                $scope.items[i].statusClass = 'badge  badge-danger';
                                break;
                            case 'All':
                                $scope.items[i].statusClass = '';
                                break;
                        }
                    }
                    $scope.items.isAllSelected = function () {
                        for (var i = 0; i < this.length; i++) {
                            var item = this[i];
                            if (!item.selected) {
                                return false;
                            }
                        }
                        return true;
                    }
                }, function (response) {
                    error(response.data.error);
                    $scope.cancelLoader();
                });
            }
        }
    };

    $scope.getUserReportStatusByUserFullName = function (fullName) {

        if (fullName) {
            $scope.loader();
            $http.post('/api/reports/userReportStatus/byFullName?fullName=' + fullName, request).then(function (response) {
                $scope.cancelLoader();
                $scope.items = response.data;
                if ($scope.items) {
                    scrollToDiv("#userReportStatusTable");
                    $scope.status = status;
                    for (var i in $scope.items) {
                        switch ($scope.items[i].status) {
                            case 'Wait':
                                $scope.items[i].statusClass = 'badge  badge-info';
                                break;
                            case 'Generating':
                                $scope.items[i].statusClass = 'badge  badge-default';
                                break;
                            case 'Generated':
                                $scope.items[i].statusClass = 'badge  badge-default';
                                break;
                            case 'Failed':
                                $scope.items[i].statusClass = 'badge  badge-danger';
                                break;
                            case 'All':
                                $scope.items[i].statusClass = '';
                                break;
                        }
                    }
                    $scope.items.isAllSelected = function () {
                        for (var i = 0; i < this.length; i++) {
                            var item = this[i];
                            if (!item.selected) {
                                return false;
                            }
                        }
                        return true;
                    }
                } else {
                    error("report not found with provided user names");
                }

            }, function (response) {
                error(response.data.error);
                $scope.cancelLoader();
            });

        } else {
            error("Please enter fullName for which you want see the report");
        }

    };

    $scope.checkAll = function (allChecked) {

        angular.forEach($scope.filteredItems, function (item) {

            item.selected = allChecked;
        });

    };

    $scope.downloadIndividualReport = function (id, updateReport, type) {
        $scope.loader();
        $http.get('api/reports/downloadAimReport/' + id + '/' + updateReport + '/' + type, {responseType: 'arraybuffer'}).then(function (response) {
            $scope.cancelLoader();
            var blob = new Blob([response.data], {
                type: 'application/octet-stream'
            });
            saveAs(blob, response.headers("fileName"));
        }, function (response) {
            $scope.cancelLoader();
            error("download failed");
        })
    };

    $scope.checkAndAllowAllUserForReport = function (id) {
        $scope.loader();
        $http.get('/api/reports/checkAndAllowAllUserForReport/' + id).then(function (response) {
            $scope.cancelLoader();
            ok("success");
        }, function (response) {
            $scope.cancelLoader();
            error(response.data.error);
        })
    };

    $scope.addSelectedReportSection = function (reportSection) {
        reportSection = JSON.parse(reportSection);
        var alreadyThere = false;
        for (var i = 0; i < $scope.selectedReportSections.length; i++) {
            if ($scope.selectedReportSections[i].id == reportSection.id) {
                alreadyThere = true;
            }
        }
        if (!alreadyThere) {
            $scope.selectedReportSections.push(reportSection);
        }
    };

    $scope.removeSelectedReportSection = function (index) {
        $scope.selectedReportSections.splice(index, 1)
    };

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: message,
            type: 'error',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.askForEmail = function (userId, reportType) {
        $scope.reportType = reportType;
        $scope.userId = userId;
        $scope.askForEmailModal = $uibModal.open({
            templateUrl: 'views/modals/askForEmail.html',
            size: 'sm',
            scope: $scope,
            resolve: {
                reportType: function () {
                    return reportType;
                }
            }
        });
    };

    $scope.openShareMultipleReportForm = function (reportType, isSendToTestTaker) {
        $scope.reportType = reportType;
        $scope.isSendToTestTaker = isSendToTestTaker;

        var selectedItems = getSelectedItems($scope.filteredItems, 'userId');
        if (selectedItems && selectedItems.length > 0) {
            $scope.selectedItemsCount = selectedItems.length;
            if (isSendToTestTaker) {
                $scope.toEmail = $scope.selectedItemsCount + "Test Takers";
            }
            $scope.shareMultipleReportFormModal = $uibModal.open({
                templateUrl: 'views/modals/reportShareMultiple.html',
                size: 'md',
                scope: $scope,
                resolve: {
                    reportType: function () {
                        return reportType;
                    }, toEmail: function () {
                        return $scope.toEmail;
                    }
                }
            });
        } else {
            $scope.selectedItemsCount = 0;
            error("You have not selected any user");
        }

    };

    $scope.closeShareMultipleReportsByEmail = function (reportType, toEmail) {
        var selectedItems = getSelectedItems($scope.filteredItems, 'userId');
        if (!selectedItems) {
            error("You have not selected any user");
            return;
        }

        var requestBody = {
            userIds: selectedItems,
            reportType: reportType,
            cc: $scope.ccEmail,
            emailId: toEmail
        };

        $scope.shareMultipleReportsLoading = true;


        var url = '/api/reports/multiple/share';
        if ($scope.isSendToTestTaker) {
            url = '/api/reports/multiple/share/testTaker'
        }
        $http.post(url, requestBody).then(
            function (response) {
                $scope.shareMultipleReportsLoading = false;
                ok(selectedItems.length + " ̥reports shared!");
                $scope.shareMultipleReportFormModal.close();
            }, function (response) {
                $scope.shareMultipleReportsLoading = false;
                error(response.data.error);
                $scope.shareMultipleReportFormModal.close();
            });
    };


    $scope.closeAskForEmail = function (email, reportType) {
        $http.post('/api/reports/shareReport?email=' + email + '&userId=' + $scope.userId + '&reportType=' + reportType).then(
            function (response) {
                ok("success");
                $scope.askForEmailModal.close();
            }, function (response) {
                error(response.data.error);
                $scope.askForEmailModal.close();
            });
    };

    $scope.shareWithTestTaker = function (userId, email, reportType) {
        $http.post('/api/reports/shareReport?email=' + email + '&userId=' + userId + '&reportType=' + reportType).then(
            function (response) {
                ok("success");
            }, function (response) {
                error(response.data.error);
            });
    };

    $scope.reset = function () {
        var request = {
            tenants: [],
            grades: [],
            cities: [],
            schools: [],
            products: [],
            tests: [],
            fromDate: {},
            toDate: {}
        };

        $scope.fromDate = new Date(y, m, d - 1, hh, mm, ss);
        $scope.toDate = new Date();
        $scope.loading = false;
        $scope.newUserLoading = false;
        $scope.getResult();
    };

    $scope.shiftFromDate = function (to) {
        switch (to) {
            case "lastWeek":
                fromD = new Date();
                fromD = moment(fromD).subtract(1, 'weeks');
                fromD = fromD.set({'hour': hh, 'minute': mm, 'second': ss});
                fromD = fromD.format('MM/DD/YYYY HH:mm:ss');
                $scope.fromDate = new Date(fromD);
                break;
            case "lastMonth":
                fromD = new Date();
                fromD = moment(fromD).subtract(1, 'months');
                fromD = fromD.set({'hour': hh, 'minute': mm, 'second': ss});
                fromD = fromD.format('MM/DD/YYYY HH:mm:ss');
                $scope.fromDate = new Date(fromD);
                break;
        }
        $scope.getUserReportStatusCount();
    };

    function getAllGrades() {
        $http.get('api/md/grades/').then(function (response) {
            $scope.grades = response.data;
            $scope.grades.isAllSelected = function () {
                for (var i = 0; i < this.length; i++) {
                    var item = this[i];
                    if (!item.selected) {
                        return false;
                    }
                }
                return true;
            }
        });
    }

    function getAllCities() {
        $http.get('api/md/cities/').then(function (response) {
            $scope.cities = response.data;
            $scope.cities.isAllSelected = function () {
                for (var i = 0; i < this.length; i++) {
                    var item = this[i];
                    if (!item.selected) {
                        return false;
                    }
                }
                return true;
            }
        });
    }

    function getAllSchools() {
        $http.get("/api/school/all").then(function (response) {
            $scope.schools = response.data;
            $scope.schools.isAllSelected = function () {
                for (var i = 0; i < this.length; i++) {
                    var item = this[i];
                    if (!item.selected) {
                        return false;
                    }
                }
                return true;
            }
        });
    }

    function getAllGender() {
        $scope.genders = [
            {gender: "Male", "selected": false},
            {gender: "Female", "selected": false}
        ];
        $scope.genders.isAllSelected = function () {
            for (var i = 0; i < this.length; i++) {
                var item = this[i];
                if (!item.selected) {
                    return false;
                }
            }
            return true;
        }
    }

    function getAllTenant() {
        $http.get('/api/user/self/tenants').then(function (response) {
            $scope.tenants = response.data;
            $scope.tenants.isAllSelected = function () {
                for (var i = 0; i < this.length; i++) {
                    var item = this[i];
                    if (!item.selected) {
                        return false;
                    }
                }
                return true;
            }
        });
    }

    $scope.selectAll = function (items, selected) {
        if (selected) {
            for (var i in items) {
                items[i].selected = true;
            }
            return items;
        } else {
            unselectAll(items);
        }
        $scope.getUserReportStatusCount();
    };

    function unselectAll(items) {
        for (var i in items) {
            items[i].selected = false;
        }
        return items;
    }

    function populateDashRequest() {
        request.grades = [];
        request.schools = [];
        request.tenants = [];
        request.cities = [];
        request.fromDate = DateFormatService.formatDate9($scope.fromDate);
        request.toDate = DateFormatService.formatDate9($scope.toDate);

        for (var i in $scope.grades) {
            if ($scope.grades[i].selected) {
                if ($scope.grades[i].value) {
                    request.grades.push($scope.grades[i].value);
                }
            }
        }

        for (var i in $scope.schools) {
            if ($scope.schools[i].selected) {
                if ($scope.schools[i].name) {
                    request.schools.push($scope.schools[i].name);
                }
            }
        }
        for (var i in $scope.cities) {
            if ($scope.cities[i].selected) {
                if ($scope.cities[i].value) {
                    request.cities.push($scope.cities[i].value);
                }
            }
        }
        for (var i in $scope.tenants) {
            if ($scope.tenants[i].selected) {
                if ($scope.tenants[i].id) {
                    request.tenants.push($scope.tenants[i].id);
                }
            }
        }

        return request;
    }

    $scope.getUserReportStatusCount = function () {
        populateDashRequest();
        $http.post('/api/reports/userReportStatus/count', request).then(function (response) {

            $scope.statusCounts = response.data;
            $scope.statusCountChart = {
                data: [],
                labels: []
            };

            for (var i in $scope.statusCounts) {

                $scope.statusCountChart.labels.push(i + " : " + $scope.statusCounts[i]);
                $scope.statusCountChart.data.push($scope.statusCounts[i]);
            }
        }, function (response) {

        })
    };

    $scope.pieOptions = {
        responsive: true,
        legend: {
            display: true,
            position: 'right'
        },
        tooltips: {
            callbacks: {
                title: function (tooltipItem, chartData) {
                    return "No. of students"
                },
                label: function (tooltipItems, data) {
                    return data.labels[tooltipItems.index]
                }

            }
        },

        plugins: {
            datalabels: {
                anchor: 'center',
                backgroundColor: function (context) {
                    return context.dataset.backgroundColor;
                },
                color: 'white',
                font: {
                    weight: 'bold'
                },
                formatter: function (value, data) {
                    return $scope.statusCountChart.labels[data.dataIndex];
                }
            }
        }
    };

    $scope.init = function () {
        fetchProfile();
        getAllGrades();
        getAllSchools();
        getAllCities();
        getAllTenant();
        $scope.reset();

    };

    $scope.getResult = function () {
        $scope.getUserReportStatusCount();
    };

    function getSelectedItems(items, param) {
        if (!items) {
            error("You have not selected any user");
            return;
        }
        var selectedItems = [];
        for (var i = 0; i < items.length; i++) {
            if (items[i].selected) {
                if (items[i].id) {
                    selectedItems.push(items[i][param]);
                }
            }
        }
        return selectedItems;
    }

    $scope.downloadSelectedReports = function (reportType) {
        var tempfileName;
        var links = [];

        if (!$scope.filteredItems) {
            error("You have not selected any user");
            return;
        }

        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if ($scope.filteredItems[i].selected) {
                if ($scope.filteredItems[i].id) {
                    tempfileName = $scope.assets_url + $scope.filteredItems[i].username + '_' + $scope.filteredItems[i].name;
                    if (reportType) {
                        if (reportType === "MINI") {
                            tempfileName = tempfileName + "MINI.pdf";
                        } else {
                            tempfileName = tempfileName + '.pdf';
                        }
                    }
                    links.push(angular.copy(tempfileName));
                }
            }
        }
        if (links.length === 0) {
            error("You have not selected any user");
            return;
        }
        downloadReportFromS3(links);
    };

    function downloadReportFromS3(urls) {

        var link = document.createElement('a');

        link.setAttribute('download', null);
        link.style.display = 'none';

        document.body.appendChild(link);

        for (var i = 0; i < urls.length; i++) {
            link.setAttribute('href', urls[i]);
            link.setAttribute('target', "_blank");
            link.click();
        }

        document.body.removeChild(link);

    }

    $scope.processSelectedReports = function (reportType) {
        var selectedItems = getSelectedItems($scope.filteredItems, 'userId');
        if (!selectedItems) {
            error("You have not selected any user");
            return;
        }

        $http.post('/api/reports/multiple/process?userIds=' + selectedItems + "&reportType=" + reportType).then(
            function (response) {
                ok("process started successfully");
            }, function (response) {
                error(response.data.error);

            });
    };

    $scope.processUserNameReports = function (usernames) {
        $http.post('/api/reports/multiple/process/usernames?usernames=' + usernames);
        ok("process started");
    };

    function getTriggers() {
        $http.get('/api/trigger/').then(
            function (response) {
                $scope.triggers = response.data;
            }, function (response) {
                error(response.data.error);
            });
    }

    $scope.changeTriggerState = function (state) {

        if (state) {
            if (state === "PAUSED") {
                state = "resumeAll";
            } else {
                state = "pauseAll";
            }
        }
        $http.get('/api/trigger/' + state).then(
            function (response) {
                getTriggers();
            }, function (response) {
                error(response.data.error);
            });
    };

    function fetchProfile() {
        $http.get('/api/user/profile').then(function (response) {
            $scope.user = response.data;
            $scope.ccEmail = $scope.user.email;
            if ($scope.user.img) {
                $scope.userImageURI = $scope.assets_url + $scope.user.img;
            } else {
                if ($scope.user.gender) {
                    if ($scope.user.gender.toLowerCase() == "male") {
                        $scope.userImageURI = "/assets/img/b-avtar.png"
                    } else {
                        if ($scope.user.gender.toLowerCase() == "female") {
                            $scope.userImageURI = "/assets/img/g-avtar.png"
                        }
                    }
                } else {
                    $scope.userImageURI = "/assets/img/default-profile-picture.png";
                }
            }

            // $scope.switchLanguage($scope.user.language);
        }, function (response) {
            toaster.pop('error', "Error", "Failed to load profile - " + response.data.error);

            if (response.data.error) {
                if (response.data.error == "You are logged in another session, so close this session") {
                    $scope.logout();
                }
            }
        });
    }

    $scope.init();

    getTriggers();

});



