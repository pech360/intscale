app.controller('CouponCodeDashboardController', function ($scope, $http) {
  $scope.startDate = new Date();
  $scope.endDate = new Date();
  $scope.getCouponCode = function () {
    const startDate = new Date($scope.startDate).toISOString();
    const endDate = new Date($scope.endDate).toISOString();

    $http.get('api/promoCode?startDate=' + startDate + '&endDate=' + endDate).then(function (response) {
      $scope.couponCodes = response.data;
    }, function (err) {
      error(err.data.message);
    })
  }

  $scope.getUsers = function (coupon) {
    $http.get('api/promoCode/' + coupon.id + '/users').then(function (response) {
      $scope.users = response.data;
    }, function (err) {
      $scope.users = [];
      error(err.data.message);
    })
  }


  function ok(message) {
    swal({
      title: message,
      type: 'success',
      buttonsStyling: false,
      confirmButtonClass: "btn btn-warning"
    });
  }

  function error(message) {
    swal({
      title: message,
      type: 'error',
      buttonsStyling: false,
      confirmButtonClass: "btn btn-warning"
    });
  }
})