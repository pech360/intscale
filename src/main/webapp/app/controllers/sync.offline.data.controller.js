app.controller('SyncOfflineDataController', function ($http, $scope, $uibModal, toaster, DateFormatService) {

    var request = {
        fromDate: {},
        toDate: {},
        schools: []
    };

    var records = {};
    $scope.dryrun = true;
    $scope.toDate = new Date();
    $scope.fromDate = new Date();
    $scope.showDisplayResponse = false;

    function ok(message) {
        swal({
            title: message,
            type: 'success',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    function error(message) {
        swal({
            title: 'error',
            type: 'error',
            text: message,
            buttonsStyling: false,
            confirmButtonClass: "btn btn-warning"
        });
    }

    $scope.loader = function () {
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'views/loader.html',
            size: 'sm',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelLoader = function () {
        $scope.modalInstance.close();
    };

    $scope.setFiles = function (element) {
        $scope.testFile = element.files[0];
    };

    function uploadUserTestResponses() {
        if ($scope.testFile) {
            $scope.loader();
            var r = new FileReader();
            var fd = new FormData();
            fd.append('file', $scope.testFile);
            fd.append('dryrun', $scope.dryrun);
            var request = {
                method: 'POST',
                url: '/api/result/import/syncUserTestResponse',
                data: fd,
                headers: {
                    'Content-Type': undefined
                }
            };
            $http(request).then(function (response) {
                    $scope.responseList = response.data;
                    $scope.cancelLoader();
                    ok("success");
                    if ($scope.responseList) {
                        displayResponse();
                    }
                }, function (response) {
                    $scope.cancelLoader();
                    if(response.status==502){
                        error(response.statusText);
                    }else {
                        $scope.responseList = response.data;
                        displayResponse();
                        error(response.data.error);
                    }

                }
            );
        } else {
            toaster.pop('error', "Please select file");
        }
    }

    function uploadUserProfiles() {
        var type = "User";
        if ($scope.testFile) {
            $scope.loader();
            var r = new FileReader();
            var fd = new FormData();
            fd.append('file', $scope.testFile);
            fd.append('dryrun', $scope.dryrun);
            fd.append('rootAccess', 'true');

            var request = {
                method: 'POST',
                url: '/api/user/self/import/' + type + '/',
                data: fd,
                headers: {
                    'Content-Type': undefined
                }
            };
            $http(request).then(function (response) {
                    $scope.responseList = response.data;
                    $scope.cancelLoader();
                    ok("success");
                    if ($scope.responseList) {
                        displayResponse();
                    }
                }, function (response) {
                    $scope.cancelLoader();
                    $scope.responseList = response.data;
                    displayResponse();
                    error(response.data.error);
                }
            );
        } else {
            toaster.pop('error', "Please select file");
        }
    }

    $scope.upload = function (type) {
        switch (type) {
            case "USER_TEST_RESPONSE":
                uploadUserTestResponses();
                break;
            case "USER_PROFILE":
                uploadUserProfiles();
                break;
        }
    };

    var displayResponse = function () {
        $scope.res = [];
        $scope.showDisplayResponse = true;
        for (property in $scope.responseList) {
            records = {};
            records.name = property;
            records.value = $scope.responseList[property];
            $scope.res.push(records);
        }
    };

    function getAllSchools() {
        $http.get("/api/school/all").then(function (response) {
            $scope.schools = response.data;
            $scope.schools.isAllSelected = function () {
                for (var i = 0; i < this.length; i++) {
                    var item = this[i];
                    if (!item.selected) {
                        return false;
                    }
                }
                return true;
            }
        });
    }

    $scope.selectAll = function (items, selected) {
        if (selected) {
            for (var i in items) {
                items[i].selected = true;
            }
            return items;
        } else {
            unselectAll(items);
        }
    };

    function unselectAll(items) {
        for (var i in items) {
            items[i].selected = false;
        }
        return items;
    }

    function populateDashRequest() {
        request.schools = [];
        request.fromDate = DateFormatService.formatDate9($scope.fromDate);
        request.toDate = DateFormatService.formatDate9($scope.toDate);

        for (var i in $scope.schools) {
            if ($scope.schools[i].selected) {
                if ($scope.schools[i].name) {
                    request.schools.push($scope.schools[i].name);
                }
            }
        }
        return request;
    }

    function downloadUserProfiles(request) {
        $http.post('/api/user/self/download/userProfile', request).then(function (response) {
            $scope.cancelLoader();
            var blob = new Blob([response.data], {
                type: 'application/octet-stream'
            });
            saveAs(blob, response.headers("fileName"));
        }, function (response) {
            $scope.cancelLoader();
            error(response.data.error);
        });
    }

    function downloadUserTestResponses(request) {
        $http.post('/api/result/download/syncUserTestResponse', request).then(function (response) {
            $scope.cancelLoader();
            var blob = new Blob([response.data], {
                type: 'application/octet-stream'
            });
            saveAs(blob, response.headers("fileName"));
        }, function (response) {
            $scope.cancelLoader();
            error(response.data.error);
        });
    }

    $scope.download = function (type) {
        $scope.loader();
        request = populateDashRequest();

        switch (type) {
            case "USER_TEST_RESPONSE":
                downloadUserTestResponses(request);
                break;
            case "USER_PROFILE":
                downloadUserProfiles(request);
                break;
        }
    };


    getAllSchools();
});