app.controller('PrincipalDashboardController', function ($rootScope, $scope, $http, toaster, $uibModal) {

    var marksBrackets = [0, 20, 21, 40, 41, 60, 61, 80, 81, 100];
    var productId = 0;
    var constructId = 0;
    $scope.grades = [];
    $scope.sections = [];

    function error(message) {
        alert(message);
    }

    function compareByValue(a, b) {
        return a.value - b.value;
    }

    // sort by name
    function compareByName(a, b) {
        var nameA = a.name.toUpperCase(); // ignore upper and lowercase
        var nameB = b.name.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        // names must be equal
        return 0;
    }

    $scope.selectItem = function (index, items, itemType) {
        var flag = false;
        if (items[index].selected) {
            flag = true;
            if (itemType == "section") {
                $scope.selectedProduct = '';
                $scope.selectedConstruct = '';
            }
            if (itemType == "grade") {
                $scope.selectedGrade = '';
                $scope.selectedProduct = '';
                $scope.selectedConstruct = '';
                getProducts();
            }
            if (itemType == "product") {
                $scope.selectedProduct = '';
                $scope.selectedConstruct = '';
                $scope.constructs = [];

            }
            if (itemType == "construct") {
                $scope.selectedConstruct = '';

            }
            if (itemType == "student") {
                $scope.selectedStudent = '';
            }
        }
        for (var i in items) {
            items[i].selected = false;
        }
        if (!flag) {
            items[index].selected = true;
            if (itemType == "section") {
                $scope.selectedSection = items[index];
                getProducts();
                $scope.getUserByGradeAndSection($scope.selectedGrade, $scope.selectedSection);
            }
            if (itemType == "grade") {
                $scope.selectedGrade = items[index];
                getProducts();
                $scope.getUserByGradeAndSection($scope.selectedGrade, $scope.selectedSection);
                // $scope.getUserByGrade($scope.selectedGrade);
            }
            if (itemType == "product") {
                $scope.selectedProduct = items[index].name;
                getSelectedComponent(items[index].id);
            }
            if (itemType == "construct") {
                $scope.selectedConstruct = items[index].name;
            }
            if (itemType == "student") {
                $scope.selectedStudent = items[index].name;
            }
        }
    };

    function unselectAll(items) {
        for (var i in items) {
            items[i].selected = false;
        }
        return items;
    }

    function getSelectedItem(items) {
        for (var i in items) {
            if (items[i].selected) {
                return items[i].id;
            }
        }
        return 0;
    }

    function getProducts() {
        $http.get('/api/test/subjectsByGradeAndTeacher/' + $scope.selectedGrade).then(function (response) {
            $scope.products = response.data;
            $scope.products.sort(compareByName);
            // if (!$scope.products.length) {
            //     toaster.pop('error', "Tests are not available right now!");
            // }
            unselectAll($scope.products);
        }, function (response) {
            toaster.pop('error', "Error", response.data.message);
        });
    }

    function getSelectedComponent(productId) {
        $http.get('/api/test/getConstructsByProductId/' + productId).then(function (response) {
            $scope.constructs = response.data;
        }, function (response) {
            error(response.data.error);
        })
    }


    function getAllGrades() {
        $http.get('/api/teacher/gradeByTeacher').then(function (response) {
            $scope.teacher = response.data;
            $scope.grades = response.data.subjectGrades;
            $scope.sections = response.data.subjectSections;

            if ($scope.teacher.principal) {
                getGradesFromMasterData();
                getSectionsFromMasterData();
            }
            if ($scope.grades) {
                $scope.grades.sort();
            }

            if ($scope.sections) {
                $scope.sections.sort();
            }

            selectDefaultGrade($scope.teacher);

        });
    }

    function getGradesFromMasterData() {
        var grades = [];
        $http.get('/api/md/grades/').then(function (response) {
            $scope.grades = response.data;
            var grades = [];
            for (var i in $scope.grades) {
                grades.push($scope.grades[i].value);
            }
            $scope.grades = grades;
        });

    }

    function getSectionsFromMasterData() {
        var sections = [];
        $http.get('/api/md/sections/').then(function (response) {
            $scope.sections = response.data;
            for (var i in $scope.sections) {
                sections.push($scope.sections[i].value);
            }
            $scope.sections = sections;
        });

    }


    function selectDefaultGrade(teacher) {
        $scope.selectedGrade = teacher.grade;
        $scope.selectedSection = teacher.section;
        $scope.getUserByGradeAndSection($scope.selectedGrade, $scope.selectedSection);

    }

    $scope.getResult = function () {
        var studentId = 0;
        productId = getSelectedItem($scope.products);
        constructId = getSelectedItem($scope.constructs);
        if (!$scope.selectedGrade) {
            selectDefaultGrade($scope.teacher);
        }
        $http.get('/api/getAverageMarksOfProduct/' + productId + '/' + constructId + '/' + $scope.selectedGrade + '/' + $scope.selectedSection + '/' + studentId).then(function (response) {
            $scope.resultList = response.data;
            var graphData1 = {};
            graphData1.series = [];
            graphData1.numberOfStudentsInMarksBrackets = [];
            graphData1.marksBrackets = ["0-20%", "21-40%", "41-60%", "61-80%", "81-100%"];

            for (var i  in $scope.resultList) {
                graphData1.numberOfStudentsInMarksBrackets.push(putCountsOfResultInBuckets($scope.resultList[i]));
                graphData1.series.push($scope.resultList[i][0].productName);
            }

            renderBarChart(graphData1);
        }, function (response) {
            error(response.data.error);
        })
    };

    function putCountsOfResultInBuckets(data) {
        var buckets = [0, 0, 0, 0, 0];
        for (var i in data) {
            if (marksBrackets[0] <= data[i].percentScores && marksBrackets[1] >= data[i].percentScores) {
                buckets[0] += 1;
            }
            if (marksBrackets[2] <= data[i].percentScores && marksBrackets[3] >= data[i].percentScores) {
                buckets[1] += 1;
            }
            if (marksBrackets[4] <= data[i].percentScores && marksBrackets[5] >= data[i].percentScores) {
                buckets[2] += 1;
            }
            if (marksBrackets[6] <= data[i].percentScores && marksBrackets[7] >= data[i].percentScores) {
                buckets[3] += 1;
            }
            if (marksBrackets[8] <= data[i].percentScores && marksBrackets[9] >= data[i].percentScores) {
                buckets[4] += 1;
            }
        }
        return buckets;
    }

    $scope.openStudentPerformance = function (studentId, flag) {
        if (flag) {
            $scope.getStudentResult(studentId);
        }
    };

    $scope.closeStudentPerformance = function () {
        $scope.studentPerformanceModal.close();
    };

    function renderBarChart(data) {
        $scope.report = {};
        $scope.report.labels = [];
        $scope.report.data = [];
        $scope.report.labels = data.marksBrackets;
        $scope.report.data = data.numberOfStudentsInMarksBrackets;
        $scope.report.series = data.series;
        $scope.report.options = {
            responsive: true,
            title: {
                display: true,
                text: 'Class Performance'
            },
            legend: {display: true},
            scales: {
                yAxes: [{

                    scaleLabel: {
                        display: true,
                        labelString: 'Number of students'
                    },
                    gridLines: {
                        display: false
                    },
                    ticks: {

                        userCallback: function (label, index, labels) {
                            // when the floored value is the same as the value we have a whole number
                            if (Math.floor(label) === label) {
                                return label;
                            }

                        }
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Marks range'
                    },
                    gridLines: {
                        display: false
                    }
                }],
                gridLines: {
                    drawBorder: false,
                    drawOnChartArea: false,
                    display: false
                }
            }
        };
    }

    function renderSingleStudentResult(data) {
        $scope.report1 = {};
        $scope.report1.labels = [];
        $scope.report1.data = [];
        $scope.report1.labels = data.xAxis;
        $scope.report1.data = data.yAxis;
        $scope.report1.series = data.series;
        $scope.report1.options = {
            responsive: true,
            title: {
                display: true,
                text: $scope.studentName + "'s academic performance"
            },
            legend: {display: false},
            scales: {
                yAxes: [{

                    scaleLabel: {
                        display: true,
                        labelString: 'Marks in Percent'
                    },
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        max: 100,
                        min: 0,
                        stepSize: 10,
                        beginAtZero: true
                        ,
                        userCallback: function (label, index, labels) {
                            // when the floored value is the same as the value we have a whole number
                            if (Math.floor(label) === label) {
                                return label;
                            }

                        }
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Tests'
                    },
                    gridLines: {
                        display: false
                    }
                }],
                gridLines: {
                    drawBorder: false,
                    drawOnChartArea: false,
                    display: false
                }
            }
        };
    }

    function reset() {
        productId = 0;
        constructId = 0;
        $scope.selectedGrade = '';
        $scope.selectedProduct = '';
        $scope.selectedConstruct = '';
        $scope.products = unselectAll($scope.products);
        $scope.constructs = unselectAll($scope.constructs);
    }

    $scope.resetAndGetResult = function () {
        reset();
        $scope.getResult();
    };

    $scope.getUserByGrade = function (grade) {
        $http.get('/api/user/self/getUserByGrade/' + grade).then(function (response) {
            $scope.users = response.data;
            $scope.users = $scope.users.sort(compareByName);
        }, function (response) {
            error(response.data.error);
        })
    };

    $scope.getUserByGradeAndSection = function (grade, section) {
        $http.get('/api/user/self/getUserByGradeAndSection/' + grade + '/' + section).then(function (response) {
            $scope.users = response.data;
            $scope.users = $scope.users.sort(compareByName);
        }, function (response) {
            error(response.data.error);
        })
    };

    $scope.getUserResultForSelectedTest = function (user) {
        if (user.selected) {
            constructId = getSelectedItem($scope.constructs);
            $http.get('/api/test/getTestsByConstructId/' + constructId).then(function (response) {
                var test = response.data;
                $state.go("app.userResult({t: test.id, u:user.id})");
            }, function (response) {
                error(response.data.error);
            })
        }

    };


    $scope.getStudentResult = function (studentId) {
        productId = getSelectedItem($scope.products);
        constructId = getSelectedItem($scope.constructs);
        if (!$scope.selectedGrade) {
            selectDefaultGrade($scope.teacher);
        }
        if (!studentId) {
            studentId = 0;
        }
        $http.get('/api/getAverageMarksOfProduct/' + productId + '/' + constructId + '/' + $scope.selectedGrade + '/' + $scope.selectedSection + '/' + studentId).then(function (response) {
            $scope.studentResults = response.data;
            var graphData2 = {};
            graphData2.series = [];
            graphData2.yAxis = [];
            graphData2.xAxis = [];
            // graphData2.series.push($scope.resultList[i][0].productName);

            for (var i  in $scope.studentResults) {
                var r = $scope.studentResults[i];
                for (var j in r) {

                    graphData2.xAxis.push(r[j].productName);
                    graphData2.yAxis.push(r[j].percentScores);
                    $scope.studentName = r[j].studentName;
                }
            }
            $scope.studentPerformanceModal = $uibModal.open({
                templateUrl: 'views/modals/studentPerformance.html',
                size: 'lg',
                scope: $scope,
                backdrop: 'backdrop'
                // keyboard: false
            });
            renderSingleStudentResult(graphData2);
        }, function (response) {
            error(response.data.error);
        });


    };

    getAllGrades();


});

