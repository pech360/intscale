app.factory('DateFormatService', function () {
        var service = {};

        service.formatDate = function (date) {
            return moment(date).format('DD/MM/YYYY');
        };
        service.formatDate2 = function (date) {
            return moment(date).format('YYYY/MM/DD');
        };
        service.formatDate3 = function (date) {
            return moment(date).format('MM/DD/YYYY');
        };
        service.formatDate4 = function (date) {
            return moment(date).format('YYYY-MM-DD');
        };
        service.formatDate5 = function (date) {
            return moment(date).format('DD/MM/YYYY HH:MM:SS');
        };
        service.formatDate6 = function (date) {
            return moment(date).format('MM/DD/YYYY HH:MM:SS');
        };
        service.formatDate7 = function (date) {
            return moment(date).format('MMMM Do YYYY, h:mm:ss a');
        };
        service.formatDate8 = function (date) {
            return moment(date).format('MMMM Do YYYY, h:mm a');
        };
        service.formatDate9 = function (date) {
            return moment(date).format('YYYY-MM-DD HH:mm:ss');
        };
        return service;
    }
);
