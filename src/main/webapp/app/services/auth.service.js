angular.module('app')
    .factory('Auth', ['jwtHelper', '$http', '$localStorage', '$timeout', '$rootScope',
        function (jwtHelper, $http, $localStorage, $timeout, $rootScope) {

            function urlBase64Decode(str) {
                var output = str.replace('-', '+').replace('_', '/');
                switch (output.length % 4) {
                    case 0:
                        break;
                    case 2:
                        output += '==';
                        break;
                    case 3:
                        output += '=';
                        break;
                    default:
                        throw 'Illegal base64url string!';
                }
                return window.atob(output);
            }

            return {
                login: function (data, success, error) {

                    $http.post('/login', data).then(
                        function (response) {
                            if (response.data.token && !jwtHelper.isTokenExpired(response.data.token)) {
                                $localStorage.token = response.data.token;
                                $localStorage.assets_url = jwtHelper.decodeToken($localStorage.token)["assets_url"];
                                success(response);

                            }
                        },
                        error
                    );

                },
                socialLogin: function (user, success, error) {
                    $http.post('/socialLogin', user).then(
                        function (response) {
                            if (response.data.token && !jwtHelper.isTokenExpired(response.data.token)) {
                                $localStorage.token = response.data.token;
                                $localStorage.assets_url = jwtHelper.decodeToken($localStorage.token)["assets_url"];
                                success(response);
                            }
                        },
                        error
                    );
                },
                socialRegister: function (user, success, error) {
                    $http.post('socialRegister', user).then(function (response) {
                        if (response.data.token && !jwtHelper.isTokenExpired(response.data.token)) {
                            $localStorage.token = response.data.token;
                            $localStorage.assets_url = jwtHelper.decodeToken($localStorage.token)["assets_url"];
                            success(response);
                        }
                    },
                        error)
                },
                signup: function (data, success, error) {
                    $http.post('/register', data).then(function (response) {

                        if (response.data.token && !jwtHelper.isTokenExpired(response.data.token)) {
                            // $localStorage.token = response.data.token;
                            // $localStorage.assets_url=jwtHelper.decodeToken($localStorage.token)["assets_url"];
                        }
                        success(response);
                    },
                        error
                    );
                },
                logout: function (success) {
                    $http.post('/api/user/logout').then(function () {
                        delete $localStorage.token;
                        // delete $localStorage.refresh_token;
                      
                        success();
                    }, function (response) {
                       ;
                    });

                },
                getUser: function () {
                    let token = $localStorage.token;
                    let user;
                    if (typeof token !== 'undefined') {
                        user = JSON.parse(urlBase64Decode(token.split('.')[1]));
                    }
                    return user;
                },
                verifyOtp: function (data, success, error) {
                    $http.post('/otpVerify?username=' + data.username + '&otp=' + data.otp + '&pkgAmount=' + data.pkgAmount).then(function (response) {
                        if (response.data.token && !jwtHelper.isTokenExpired(response.data.token)) {
                            $localStorage.token = response.data.token;
                            $localStorage.assets_url = jwtHelper.decodeToken($localStorage.token)["assets_url"];
                        }
                        success(response);
                    }, error
                    );
                },
                resendOtp: function (username, success, error) {
                    $http.post('/resentOTP?username=' + username).then(function (response) {
                        success(response);
                    }, error
                    );
                }
            };
        }
    ]);
/* .factory('Auth', ['jwtHelper', '$http', '$localStorage', '$timeout',
 function (jwtHelper, $http, $localStorage, $timeout) {

 function urlBase64Decode(str) {
 var output = str.replace('-', '+').replace('_', '/');
 switch (output.length % 4) {
 case 0:
 break;
 case 2:
 output += '==';
 break;
 case 3:
 output += '=';
 break;
 default:
 throw 'Illegal base64url string!';
 }
 return window.atob(output);
 }

 return {
 login: function (data, success, error) {

 $http.post('/login', data).then(
 function (response) {
 if (response.data.token && !jwtHelper.isTokenExpired(response.data.token)) {
 $localStorage.token = response.data.token;
 success(response);

 }
 },
 error
 );

 },
 signup: function (data, success, error) {
 $http.post('/register', data).then(function(response){
 success(response);
 },
 error
 );
 },
 logout: function (success) {
 delete $localStorage.token;
 delete $localStorage.refresh_token;
 success();
 },
 getUser: function () {
 var token = $localStorage.token;
 let user = {};
 if (typeof token !== 'undefined') {
 user = JSON.parse(urlBase64Decode(token.split('.')[1]));
 }
 return user;
 }
 };
 }
 ]);*/
