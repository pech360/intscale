angular.module('app')
    .service('CommonService', ['$http', function ($http) {

        var CommonService = {};

        CommonService.listDesignations = function () {
            return $http.get('/api/md/designations/');
        };
        CommonService.listIndustries = function () {
            return $http.get('/api/md/industries/');
        };
        CommonService.listInterests = function () {
            return $http.get('/api/md/interests/');
        };
         CommonService.userLists = function () {
             return $http.get('/api/user/self/list');
         };
        CommonService.listGrades = function () {
            return $http.get('/api/md/grades/');
        };
        CommonService.listHobbies = function () {
            return $http.get('/api/md/hobbies/');
        };
        CommonService.listPilots = function () {
                    return $http.get('/api/md/pilots/');
                };
        CommonService.listStudentTypes = function () {
            return $http.get('/api/md/studentTypes/');
        };
        CommonService.listCategories = function () {
            return $http.get('/api/category/parent/');
        };
        CommonService.listSubCategories = function () {
            return $http.get('/api/category/child/');
        };
        CommonService.listSubCategoriesByParent = function (parentId) {
            return $http.get('/api/category/childByParent?parentId='+parentId);
        };
        CommonService.listUserTypes = function () {
            return $http.get('/api/md/userTypes/');
        };
        CommonService.listLanguages = function () {
            return $http.get('/api/md/languages/');
        };
        CommonService.listGender = function () {
            return $http.get('/api/md/gender/');
        };
        CommonService.listSubjects = function () {
            return $http.get('/api/md/subjects/');
        };
        CommonService.listDegrees = function () {
            return $http.get('/api/md/degrees/');
        };
        CommonService.listStreams = function () {
            return $http.get('/api/md/streams/');
        };
        CommonService.listCategory = function () {
             $http.get('/api/category');
        };
        CommonService.listParentCategory = function () {
            return $http.get('/api/category/parent');
        };
        CommonService.listOfGroups = function () {
            return $http.get('/api/md/groups/');
        };

        return CommonService;
    }
    ]);