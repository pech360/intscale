angular.module('app').service('TestPanelService', function () {
    function separateDummies(questions) {
        //TODO : do not copy questions to questionsCopy
        var questionsCopy = angular.copy(questions);
        var size = questionsCopy.length;
        var dummies = [];
        for (var i = 0; i < size; i++) {
            questionCopy = questionsCopy[i];
            if (questionCopy.hasDummy) {
                dummies.push(questionsCopy.splice(i, 1)[0]);
                i--;
                size--;
            }
        }
        return {
            nonDummies: questionsCopy,
            dummies: dummies
        };
    }

    function arrangeDummies(dummies) {
        var size = dummies.length;
        var obj = {
            mcq: [],
            audio: [],
            image: [],
            text: [],
            slider: [],
            group: {}
        };
        for (i = 0; i < size; i++) {
            switch (dummies[i].questionType.toLowerCase()) {
                case 'mcq':
                    obj.mcq.push(dummies[i]);
                    break;
                case 'image':
                    obj.image.push(dummies[i]);
                    break;
                case 'audio':
                    obj.audio.push(dummies[i]);
                    break;
                case 'text':
                    obj.text.push(dummies[i]);
                    break;
                case 'slider':
                    obj.slider.push(dummies[i]);
                    break;
            }
        }
        for (i = 0; i < size; i++) {
            if (dummies[i].questionType.toLowerCase() == 'group') {
                if (typeof obj.group[dummies[i].groupName] == 'undefined') {
                    obj.group[dummies[i].groupName] = [];
                }
                obj.group[dummies[i].groupName].push(dummies[i]);
            }
        }
        return obj;
    }

    function sequenceComparator(a,b){
        return a.sequence-b.sequence;
    }

    function orderBySequence(questions){
     //    TODO :
        questions.sort(sequenceComparator);
    }




    //service starts here
    var service={};

    service.indexBySequence=function (questions) {
        orderBySequence(questions);
        var count=1;
        var size=questions.length;
        var groupIndex={};
        for(i=0;i<size;i++){
            var question = questions[i];
            switch (question.questionType.toLowerCase()){
                case 'mcq':
                case 'image':
                case 'audio':
                case 'text':
                case 'slider':
                    question.index=count;
                    count++;
                    break;
                case 'group':
                    var groupName=question.groupName;
                    if(typeof groupIndex[groupName] =='undefined'){
                        groupIndex[groupName]=count++;
                    }
                    question.index=groupIndex[groupName];
                    break;
            }
        }
    };
    service.shuffle=function (array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    };
    service.indexing=function(questions){
        var count = 1;
        var size = questions.length;
        var groupIndexes={};
        for (i = 0; i < size; i++) {
            var question = questions[i];
            switch (question.questionType.toLowerCase()) {
                case 'group':
                    if(typeof groupIndexes[question.groupName] == 'undefined'){
                        groupIndexes[question.groupName]=count++;
                    }
                    question.index=groupIndexes[question.groupName];
                    break;
                case 'mcq':
                    question.index = count++;
                    break;
                case 'image':
                    question.index = count++;
                    break;
                case 'audio':
                    question.index = count++;
                    break;
                case 'text':
                    question.index = count++;
                    break;
                case 'slider':
                    question.index = count++;
                    break;
            }
        }
        return questions;
    };
    service.dummyFirst=function (questions) {
        var q = separateDummies(questions);
        nonDummies = q.nonDummies;
        dummies = q.dummies;
        dummies = arrangeDummies(dummies);
        var final =[];
        var types={};
        for(i=0;i<nonDummies.length;i++){
            var type=nonDummies[i].questionType.toLowerCase();
            if(typeof types[type] == 'undefined'){
                switch (type){
                    case 'mcq':
                    case 'image':
                    case 'audio':
                    case 'text':
                    case 'slider':
                        var dummyType=nonDummies[i].questionType.toLowerCase();
                        for(j=0;j<dummies[dummyType].length;j++){
                            final.push(dummies[dummyType][j]);
                        }
                        final.push(nonDummies[i]);
                        types[type]='';
                        break;
                    case 'group':
                        for (var property in dummies.group) {
                            if (dummies.group.hasOwnProperty(property)) {
                                for(j=0;j<dummies.group[property].length;j++){
                                    final.push(dummies.group[property][j]);
                                }
                            }
                        }
                        final.push(nonDummies[i]);
                        types[type]='';
                        break;
                }
            }else{
                final.push(nonDummies[i]);
            }
        }
        return final;
    };
    return service;
});

angular.module('app').service('UserId', function () {});
angular.module('app').service('TestId', function () {});