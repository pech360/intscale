app.config(function ($stateProvider, $urlRouterProvider, $httpProvider, socialProvider, $translateProvider, $compileProvider) {

    var currentUrl = window.location.hostname.split(":")[0];
    var currentUrl1 = '%3' + currentUrl;
    var currentUrl2 = "^\s*(" + currentUrl + "|" + currentUrl1 + "|local|http|https|app|tel|ftp|file|blob|content|ms-appx|x-wmapp0|chrome-extension|cdvfile):|data:image\/";
    var urlRegex = new RegExp(currentUrl2);
    $compileProvider.imgSrcSanitizationWhitelist(urlRegex);
    $compileProvider.aHrefSanitizationWhitelist(urlRegex);

    $urlRouterProvider.otherwise("/app/availabletests");
    $stateProvider
        .state('app', {
            abstract: true,
            url: "/app",
            controller: "MenuController",
            templateUrl: "views/layout/layout.html"
        })
        .state('login', {
            url: "/login?pkgAmount",
            params: {
                pkgAmount: null
            },
            controller: 'LoginController',
            templateUrl: "views/login.html"
        })
        .state('register', {
            url: "/register?pkgAmount",
            params: {
                pkgAmount: null
            },
            controller: 'RegisterController',
            templateUrl: "views/register.html"
        })
        .state('landing', {
            url: "/landing",
            controller: 'LandingController',
            templateUrl: "landing.html"
        })
        .state('welcome', {
            url: "/welcome",
            controller: 'WelcomeController',
            templateUrl: "views/welcome.html"
        })
        .state('app.profile', {
            url: "/profile",
            controller: 'ProfileController',
            templateUrl: "views/profile.html"
        })
        .state('app.home', {
            url: "/home",
            controller: 'HomeController',
            templateUrl: "views/home.html"
        })
        .state('paymentgateway', {
            url: "/paymentgateway?pkgAmount",
            params: {
                pkgAmount: null
            },
            controller: 'Paymentgateway',
            templateUrl: "views/paymentgateway.html"

        })
        .state('app.availabletests', {
            url: "/availabletests",
            controller: 'AvailabletestsController',
            templateUrl: "views/availableTests.html"
        })
        .state('app.newtest', {
            url: "/newtest/",
            controller: 'NewtesttakingController',
            templateUrl: "views/newtesttaking.html"
        })
        .state('app.subscriptionKey', {
            url: "/subscriptionKey",
            controller: 'SubscriptionKeyController',
            templateUrl: "views/subscriptionKey.html"
        })
        .state('app.userManagement', {
            url: "/usermanagement",
            controller: 'userManagement',
            templateUrl: "views/userMgmt.html"
        })
        .state('app.publish', {
            url: "/publish",
            controller: 'publish',
            templateUrl: "views/publish.html"
        })
        .state('app.roleManagement', {
            url: "/rolemanagement",
            controller: 'roleManagement',
            templateUrl: "views/roleMgmt.html"
        })
        .state('app.categoryManagement', {
            url: "/categoryManagement",
            controller: 'categoryManagement',
            templateUrl: "views/categoryManagement.html"
        })
        .state('app.afterTakingtest', {
            url: "/afterTakingtest",
            params: {
                productId: null,
                constructId: null
            },
            controller: 'AfterTakingtestController',
            templateUrl: "views/afterTakingtest.html"
        })
        .state('app.createTest', {
            url: "/createTest",
            params: {
                testId: null,
                version: false,
                readUser: false
            },
            controller: 'CreateTestController',
            templateUrl: "views/createTest.html"
        })
        .state('app.createMasterData', {
            url: "/createMasterData",
            controller: 'createMasterDataController',
            templateUrl: "views/masterData/createMasterData.html"
        })
        .state('app.createQuestions', {
            url: "/createQuestions",
            controller: 'questionsController',
            templateUrl: "views/questions/questionPanel.html"
        })
        .state('app.manageQuestions', {
            url: "/manageQuestions",
            controller: 'questionsController',
            templateUrl: "views/questions/questionUpdatePanel.html"
        })
        .state('app.allTests', {
            url: "/allTest",
            controller: 'AllTestsController',
            templateUrl: "views/allTests.html"
        })
        .state('app.addMedia', {
            url: "/addMedia",
            controller: 'AddMediaController',
            templateUrl: "views/addMedia.html"
        })
        .state('app.bulkSignup', {
            url: "/bulkSignup",
            controller: 'BulkSignupController',
            templateUrl: "views/bulkSignup.html"
        })
        .state('app.school', {
            url: "/school",
            controller: 'SchoolController',
            templateUrl: "views/school.html"
        })
        .state('app.expertAnalysis', {
            url: "/expertAnalysis",
            controller: 'ExpertAnalysisController',
            templateUrl: "views/expertAnalysis.html"
        })
        .state('app.importTest', {
            url: "/importTest",
            controller: 'ImportTestController',
            templateUrl: "views/importTest.html"
        })
        .state('app.testPanel', {
            url: "/testPanel",
            params: {
                testId: null,
                productId: null,
                constructId: null
            },
            controller: 'TestPanelController',
            templateUrl: "views/testPanel/testPanel.html"
        })
        .state('app.userResult', {
            url: "/userResult/:t?/:u?",
            controller: 'UserResultController',
            templateUrl: "views/userResult.html"
        })
        .state('app.scaleReports', {
            url: "/intscaleReports",
            controller: 'reportsController',
            templateUrl: "views/reports/testReport.html"
        })
        .state('app.usertestAssignment', {
            url: "/userTestAssignment",
            controller: 'usertestAssignmentController',
            templateUrl: "views/usertestAssigned.html"
        })
        .state('app.testSequencing', {
            url: "/testSequence",
            controller: 'testSequenceController',
            templateUrl: "views/testSequence.html"
        })
        .state('app.deriveCategory', {
            url: "/deriveCategory",
            controller: 'DeriveCategoryController',
            templateUrl: "views/deriveCategory.html"
        })
        .state('app.tenantTestAllot', {
            url: "/testAllotment",
            controller: 'tenantTestAllotmentController',
            templateUrl: "views/tenatTestAllotment.html"
        })
        .state('app.tag', {
            url: "/tags",
            controller: 'QuestionTagController',
            templateUrl: "views/tags/question_tags.html"
        })
        .state('app.interests', {
            url: "/interests",
            controller: 'InterestsController',
            templateUrl: "views/interests.html"
        })
        .state('app.individualReport', {
            url: "/individualReport/:id?",
            controller: 'IndividualReportController',
            templateUrl: "views/reports/individualReport.html"
        })
        .state('app.individualReportV1', {
            url: "/individualReportV1/:id?",
            controller: 'IndividualReportV1Controller',
            templateUrl: "views/reports/individualReportV1.html"
        })
        .state('app.tenantManagement', {
            url: "/tenantManagement",
            controller: 'TenantManagementController',
            templateUrl: "views/tenantManagement.html"
        })
        .state('app.studentManagement', {
            url: "/studentManagement",
            controller: 'StudentManagementController',
            templateUrl: "views/studentManagement.html"
        })
        .state('app.sessionManagement', {
            url: "/sessionManagement",
            controller: 'SessionManagementController',
            templateUrl: "views/sessionManagement.html"
        })
        .state('app.teacherManagement', {
            url: "/teacherManagement",
            controller: 'TeacherManagementController',
            templateUrl: "views/teacherManagement.html"
        })
        .state('app.studentGradeManagement', {
            url: "/studentGrade",
            controller: 'StudentGradeController',
            templateUrl: "views/studentGrade.html"
        })
        .state('app.counsellorStudentManagement', {
            url: "/counsellorStudentManagement",
            controller: 'counsellorStudentController',
            templateUrl: "views/counsellorStudent.html"
        })
        .state('app.counsellorCalender', {
            url: "/counsellorCalender",
            controller: 'CounsellorCalenderController',
            templateUrl: "views/counsellorCalender.html"
        })
        .state('app.teacherGradeAssignment', {
            url: "/teacherGrade",
            controller: 'TeacherGradeController',
            templateUrl: "views/teacherGrade.html"
        })
        .state('app.pilotManagement', {
            url: "/pilotManagement",
            controller: 'PilotManagementController',
            templateUrl: "views/pilotView.html"
        })
        .state('app.categoryScoring', {
            url: "/categoryScoring",
            controller: 'CategoryScoringController',
            templateUrl: "views/reports/categoryScoring.html"
        })
        .state('app.createProduct', {
            url: "/productManagement",
            controller: 'ProductManagementController',
            templateUrl: "views/createProduct.html"
        })
        .state('app.subjectTeacherManagement', {
            url: "/subjectTeacherManagement",
            controller: 'subjectTeacherManagement',
            templateUrl: "views/subjectTeacherMgmt.html"
        })
        .state('app.testConstruct', {
            url: "/testConstruct",
            controller: 'TestConstructController',
            templateUrl: "views/testConstruct.html"
        })
        .state('app.counsellorReport', {
            url: "/CounsellorReport/:id?",
            controller: 'CounsellorReportController',
            templateUrl: "views/reports/counsellorReport.html"
        })
        .state('app.dashboard', {
            url: "/dashboard",
            controller: 'DashboardController',
            templateUrl: "views/dashboard.html"
        })
        .state('app.product', {
            url: "/product/:id?",
            controller: 'ProductController',
            templateUrl: "views/product.html"
        })
        .state('app.testLeaf', {
            url: "/testLeaf",
            controller: 'TestLeafController',
            templateUrl: "views/testLeaf.html"
        })
        .state('app.teacherDashboard', {
            url: "/teacherDashboard",
            controller: 'TeacherDashboardController',
            templateUrl: "views/teacherDashboard.html"
        })
        .state('app.principalDashboard', {
            url: "/principalDashboard",
            controller: 'PrincipalDashboardController',
            templateUrl: "views/principalDashboard.html"
        })
        .state('app.userTimeline', {
            url: "/timeline",
            controller: 'UserTimelineController',
            templateUrl: "views/userTimeline.html"
        })
        .state('app.mynews', {
            url: "/mynews",
            controller: 'MyNewsController',
            templateUrl: "views/mynews.html"
        })
        .state('app.viewPost', {
            url: "/viewPost/:id?",
            controller: 'ViewPostController',
            templateUrl: "views/viewPost.html"
        })
        .state('app.manageBlogs', {
            url: "/manageBlogs",
            controller: 'ManageBlogsController',
            templateUrl: "views/masterData/manageBlogs.html"
        })
        .state('app.talentReport', {
            url: "/talentReport/:id?",
            controller: 'TalentReportController',
            templateUrl: "views/reports/talentReport.html"
        })
        .state('app.voscaleReport', {
            url: "/voscaleReport/:id?",
            controller: 'VoscaleReportController',
            templateUrl: "views/reports/voscaleReport.html"
        })
        .state('app.360_question', {
            url: "/360_question",
            controller: '360QuestionController',
            templateUrl: "views/threesixty/360question.html"
        })
        .state('app.stakeholders', {
            url: "/stakeholders",
            controller: 'StakeholderController',
            templateUrl: "views/threesixty/stakeholders.html"
        })
        .state('app.upload360Question', {
            url: "/upload360Question",
            controller: 'Upload360QuestionController',
            templateUrl: "views/threesixty/upload360question.html"
        })
        .state('app.stakeholderreport', {
            url: "/stakeholderreport",
            controller: 'StakeholderReportController',
            templateUrl: "views/threesixty/stakeholderreport.html"
        })
        .state('app.createReport', {
            url: "/createReport",
            controller: 'CreateReportController',
            templateUrl: "views/createReport.html"
        })
        .state('app.createReportSection', {
            url: "/createReportSection",
            controller: 'CreateReportSectionController',
            templateUrl: "views/createReportSection.html"
        })
        .state('app.reportV2', {
            url: "/reportV2/:id?",
            controller: 'ReportV2Controller',
            templateUrl: "views/aimReports/reportV2.html"
        })
        .state('app.aimReportMini', {
            url: "/aimReportMini/:id?",
            controller: 'AimReportMiniController',
            templateUrl: "views/aimReports/aimReportMiniMain.html"
        })
        .state('app.counsellorReportV2', {
            url: "/counsellorReportV2/:id?",
            controller: 'CounsellorReportV2Controller',
            templateUrl: "views/counsellorReportV2.html"
        })
        .state('app.aimExpertInventory', {
            url: "/aimExpertInventory",
            controller: 'AimExpertInventoryController',
            templateUrl: "views/reports/aimExpertInventory.html"
        })
        .state('app.userReportStatus', {
            url: "/userReportStatus",
            controller: 'UserReportStatusController',
            templateUrl: "views/reports/userReportStatus.html"
        })
        .state('app.tenantProductAllotment', {
            url: "/tenantProductAllotment",
            controller: 'tenantProductAllotmentController',
            templateUrl: "views/tenantProductAllotment.html"
        })
        .state('app.offeredTest', {
            url: "/offeredTest",
            controller: 'offeredTestController',
            templateUrl: "views/tenantOfferTest.html"
        })
        .state('app.researcher', {
            url: "/researcher",
            controller: 'ResearcherController',
            templateUrl: "views/researcher.html"
        })
        .state('app.marketeer', {
            url: "/marketeer",
            controller: 'MarketeerController',
            templateUrl: "views/marketeer.html"
        })
        .state('app.operations', {
            url: "/operations",
            controller: 'OperationsController',
            templateUrl: "views/operations.html"
        })
        .state('app.leap', {
            url: "/leap",
            controller: 'LeapController',
            templateUrl: "views/leap.html"
        })
        .state('app.counsellorDashboard2', {
            url: "/counsellorDashboard2",
            controller: 'CounsellorDashboard2Controller',
            templateUrl: "views/counsellorDashboard2.html"
        })
        .state('app.counsellorAdmin', {
            url: "/counsellorAdmin",
            controller: 'CounsellorAdminController',
            templateUrl: "views/counsellorAdmin.html"
        })
        .state('app.counsellingSession', {
            url: "/counsellingSession/:id?/:admin?",
            controller: 'CounsellingSessionController',
            templateUrl: "views/counsellingSession.html"
        })
        .state('app.counsellingSessionNotes', {
            url: "/counsellingSessionNotes",
            controller: 'CounsellingSessionNotesController',
            templateUrl: "views/counsellingSessionNotes.html"
        })
        .state('app.leapDashboardManagement', {
            url: "/leap/dashboard/management",
            controller: 'LeapDashboardManagementController',
            templateUrl: "views/leapDashboardManagement.html"
        })
        .state('app.leapDashboardTeacher', {
            url: "/leap/dashboard/teacher/:grade?/:section?/:fromDate?/:toDate?",
            controller: 'LeapDashboardTeacherController',
            templateUrl: "views/leapDashboardTeacher.html"
        })
        .state('app.leapDashboardStudentsPerformance', {
            url: "/leap/dashboard/students/:grade?/:section?",
            controller: 'LeapDashboardStudentsPerformanceController',
            templateUrl: "views/leapDashboardStudentsPerformance.html"
        })
        .state('app.leapAttendanceSheet', {
            url: "/leap/attendanceSheet",
            controller: 'LeapAttendanceSheetController',
            templateUrl: "views/leapAttendanceSheet.html"
        })
        .state('app.syncOfflineData', {
            url: "/syncOfflineData",
            controller: 'SyncOfflineDataController',
            templateUrl: "views/syncOfflineData.html"
        })
        .state('app.manageCouponCode', {
            url: "/manageCouponCode",
            controller: 'ManageCouponCodeController',
            templateUrl: "views/manageCouponCode.html"
        })
        .state('app.refineMasterData', {
            url: "/refineMasterData",
            controller: 'RefineMasterDataController',
            templateUrl: "views/masterData/refineMasterData.html"
        })
        .state('app.loginPanel', {
            url: "/loginPanel",
            controller: 'LoginPanelController',
            templateUrl: "views/loginPanel.html"
        })
        .state('app.careerInterestInventory', {
            url: "/career-interest-inventory",
            controller: 'CareerInterestInventoryController',
            templateUrl: "views/reports/careerInterestInventory.html"
        })
        .state('app.aimReportDemo', {
            url: "/aimReportDemo/:id?",
            controller: 'AimReportDemoController',
            templateUrl: "views/aimReports/aimReportDemo.html"
        })
        .state('app.aimReportDemoConsole', {
            url: "/aimReportDemoConsole",
            controller: 'AimReportDemoConsoleController',
            templateUrl: "views/aimReportDemoConsole.html"
        })
        .state('app.transactionReport', {
            url: "/transactionReport",
            controller: 'TransactionReportController',
            templateUrl: "views/transactionReport.html"
        })
        .state('app.couponCodeDashboard', {
            url: "/couponCodeDashboard",
            controller: 'CouponCodeDashboardController',
            templateUrl: "views/couponCodeDashboard.html"
        })
        .state('app.sponsorManagement', {
            url: "/sponsors",
            controller: 'SponsorController',
            templateUrl: "views/masterData/sponsors.html"
        });


    $httpProvider.interceptors.push(['$q', '$location', '$localStorage', 'jwtHelper',
        function ($q, $location, $localStorage, $rootScope, jwtHelper) {
            return {
                'request': function (config) {
                    config.headers = config.headers || {};
                    if ($localStorage.token) {
                        config.headers.Authorization = 'Bearer ' + $localStorage.token;
                    }
                    return config;
                },
                'responseError': function (response) {
                    //do not redirect on app api rejects @ 403 and they may be due to
                    //autherisation error, not authentication error
                    if (response.status === 401) {
                        delete $localStorage.token;
                        localStorage.clear();
                        // $location.path('/login?pkgAmount='+$location.$$search.pkgAmount);
                        // if ($location.$$search.pkgAmount) {
                        // } else {
                        $location.path('/login');
                        // $state.reload();
                        location.reload(true);
                        // }
                        // $state.go('login');

                    }
                    return $q.reject(response);
                }
            };
        }]);

    socialProvider.setGoogleKey("752743798467-mspqb3qiuct1nj1q20l6u3umtie828e5.apps.googleusercontent.com");
    socialProvider.setFbKey({ appId: "2085602424996910", apiVersion: "v2.11" });

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    window.fbAsyncInit = function () {
        FB.init({
            appId: '969430493216288',
            status: true,
            cookie: true,
            xfbml: true,
            version: 'v3.0'
        });
    };

    var language = (window.navigator.userLanguage || window.navigator.language).toLowerCase();

    $translateProvider.useStaticFilesLoader({
        prefix: '/assets/translations/lang_',
        suffix: '.json'
    });

    $translateProvider.preferredLanguage('en');
    $translateProvider.fallbackLanguage('en');
    $translateProvider.addInterpolation('$translateMessageFormatInterpolation');
    $translateProvider.useSanitizeValueStrategy('escape');
    // $translateProvider.addInterpolation($translateMessageFormatInterpolation);4
});
